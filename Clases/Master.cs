﻿using ByStro.Forms;
using ByStro.PL;
using DevExpress.XtraBars.Docking2010.Customization;
using DevExpress.XtraBars.Docking2010.Views.WindowsUI;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ByStro.Clases
{
   public static  class Master
    {
        public static readonly string DomainPath;
        public static DAL.DBDataContext  db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
        static Master()
        {
            DomainPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "ByStro", "LayoutToXml");
            CurrentSession.UserAccessbileAccounts = db.Acc_Accounts.ToList();
        }
        public static void RefreshAllWindows()
        {

        }
        public static void SetUserSetting(object _SettingName, object _SettingValue)
        {
            //DAL.ERPDataContext db = new DAL.ERPDataContext(Program.setting.con);
            //var value = (from v in db.St_UserSettings
            //             where v.UserID == CurrentSession.user.ID && v.SettingName == _SettingName.ToString()
            //             select v).ToList();

            //if (value.Count > 0)
            //{
            //    value[0].SettingValue = _SettingValue.ToString();
            //}
            //else
            //{
            //    db.St_UserSettings.InsertOnSubmit(new DAL.St_UserSetting() { SettingName = _SettingName.ToString(), UserID = CurrentSession.user.ID, SettingValue = _SettingValue.ToString() });
            //}
            //db.SubmitChanges();


        }

        public static System.Drawing.Image GetImageFromByte(Byte[] ByteArray)
        {
            System.Drawing.Image img;
            try
            {
                Byte[] imgbyte = ByteArray;
                MemoryStream strm = new MemoryStream(imgbyte, false);
                img = System.Drawing.Image.FromStream(strm);
            }
            catch { img = null; }
            return img;
        }
        public static Byte[] GetByteFromImage(System.Drawing.Image image)
        {
            MemoryStream strm = new MemoryStream();
            try
            {

                image.Save(strm, System.Drawing.Imaging.ImageFormat.Jpeg);
                return strm.ToArray();
            }
            catch
            {
                return strm.ToArray();
            }
        }

        //public static bool ValidAsIntNonZero(this object value)
        //{
        //    return (value.IsNumber() && value.ToInt() > 0);
        //}
        public static void OpenByProssess(int ProssessID, int ProssessCode)
        {
            switch (ProssessID)
            {
                //case 1: frm_Main.OpenForm(new frm_OpenBalance(ProssessCode), openNew: true); break;
                //case 2: frm_Main.OpenForm(new frm_ItemDamage(ProssessCode), openNew: true); break;
                //case 3: frm_Main.OpenForm(new frm_ItemAdd(ProssessCode), openNew: true); break;
                //case 4: frm_Main.OpenForm(new frm_ItemTake(ProssessCode), openNew: true); break;
                //case 5: frm_Main.OpenForm(new frm_ItemStoreMove(ProssessCode), openNew: true); break;
               case 6: Main_frm.OpenForm(new frm_Inv_Invoice(ProssessCode), true); break;
                //case 7: frm_Main.OpenForm(new frm_Sl_Requst(ProssessCode), openNew: true); break;
                //case 8: frm_Main.OpenForm(new frm_Sl_Order(ProssessCode), openNew: true); break;
                //case 9: frm_Main.OpenForm(new frm_Sl_SlReturnInvoice(ProssessCode), openNew: true); break;
                ////case 10: frm_Main.OpenForm(new frm_Pr_PrInvoice (ProssessCode), openNew: true); break;
                ////case 11: frm_Main.OpenForm(new frm_Pr_Requst (ProssessCode), openNew: true); break;
                ////case 12: frm_Main.OpenForm(new frm_Pr_Order (ProssessCode), openNew: true); break;
                ////case 13: frm_Main.OpenForm(new frm_Pr_PrReturnInvoice (ProssessCode), openNew: true); break;
                //case 14: frm_Main.OpenForm(new frm_RevExpenEntry(true, ProssessCode), openNew: true); break;
                //case 15: frm_Main.OpenForm(new frm_RevExpenEntry(false, ProssessCode), openNew: true); break;
                //case 16: frm_Main.OpenForm(new frm_CashNote(true, ProssessCode, ""), openNew: true); break;
                //case 17: frm_Main.OpenForm(new frm_CashNote(false, ProssessCode, ""), openNew: true); break;
                //case 18: frm_Main.OpenForm(new frm_AccountTransaction(ProssessCode), openNew: true); break;
                //case 19: frm_Main.OpenForm(new frm_MnfOrder(ProssessCode), openNew: true); break;

                default:
                    break;
            }
        }
        public static void AddRevenueAccount(Form frm)
        {

            FlyoutProperties properties = new FlyoutProperties();
            FlyoutAction action = new FlyoutAction() { Caption = LangResource.AddRevenueAccount, Description = "" };
            FlyoutCommand command1 = new FlyoutCommand() { Text = LangResource.Finish, Result = DialogResult.Yes };
            action.Commands.Add(command1);
            //FlyoutDialog.Show(frm_Main.Instance
            //    , new Forms.UserControls.uc_AddRevenueAccount(), action, properties);

            Master.RefreshAllWindows();
        }
        public static void AddExpencesAccount(Form frm)
        {

            FlyoutProperties properties = new FlyoutProperties();
            FlyoutAction action = new FlyoutAction() { Caption = LangResource.AddExpenceAccount, Description = "" };
            FlyoutCommand command1 = new FlyoutCommand() { Text = LangResource.Finish, Result = DialogResult.Yes };
            action.Commands.Add(command1);
            //FlyoutDialog.Show(frm_Main.Instance, new Forms.UserControls.uc_AddExpenceAccount(), action, properties);

            Master.RefreshAllWindows();
        }
        public enum PayTypes
        {
            Drawer = 1,
            Bank = 2,
            PayCard = 3,
            Account = 4,
            CashNote = 5,
        }
        public enum SystemProssess
        {
            OpenBalance = 0,       //0   D
            ItemOpenBalance = 1,      //1 
            ItemDamage = 2,       //2 
            ItemAdd = 3,           //3
            ItemTake = 4,          //4 
            ItemStoreMove = 5,      //5
            SalesInvoice = 6,       //6   D
            SalesRequest = 7,      //7 
            SalesOrder = 8,      //8
            SalesReturn = 9,    //9   C
            PurchaseInvoice = 10,  //10  C
            PurchaseRequest = 11,    //11
            PurchaseOrder = 12,    //12
            PurchaseReturn = 13,  //13  D
            Revenues = 14,  //14  D
            Expenses = 15,  //15  C
            CashNoteIn = 16,  //16  D
            CashNoteOut = 17,  //17  C
            CashTransfer = 18,  //18  C
            ProductionOrder = 19,  //19  C
            ManualJournal = 20,  //20  
            HR_Pay = 21,         //21
            HR_loan = 22,        //22
            SalesPriceOffer = 23,        //22
            Without = 24,        //24
            DirectPay = 25,        //24
            DirectPostToStore = 26,        //24
            DirectWithdrawFromStore = 27,        //24
            PurchasePriceOffer = 28
        }
        public static string ConvertMoneyToText(string Amount)
        {
            return MasterClass.ConvertMoneyToText(Amount);
        }
        public class NameAndIDDataSource
        {
            public int ID { get; set; }
            public string Name { get; set; }
        }
        public enum PartTypes
        {
            Vendor = 1,
            Customer = 2,
            Employee = 3,
            Account = 4,
        }


        public static DialogResult AskForSaving()
        {
            DialogResult rslt = XtraMessageBox.Show(LangResource.AskForSaving, "", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
            return rslt;
        }
        public static bool AskForDelete(Form frm, bool IsNew, string partName, string PartNumber)
        {

            if (XtraMessageBox.Show(LangResource.ConfrmDelete, "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes) return true;
            else return false;

        }
        public static string GetNewAccountNumber(int parentID)
        {

            var parent = (from a in db.Acc_Accounts where a.ID == parentID select a).FirstOrDefault();
            if (parent == null)
                parent = (from a in db.Acc_Accounts where a.ID == 1 select a).FirstOrDefault();
            var ParentChilds = (from a in db.Acc_Accounts where a.ParentID == parent.ID select a.Number);

            if (ParentChilds.Count() > 0)
            {
                return (Convert.ToInt64(ParentChilds.Max()) + 1).ToString();
            }
            else
            {
                string num = parent.Number.ToString();
                for (int i = 0; i < parent.Level; i++)
                {
                    num += "0";
                }
                num += "1";
                return num;

            }
        }
        public static int GetNewAccountID()
        {
            DAL.DBDataContext dbs = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);

            try
            {
                return (int)dbs.Acc_Accounts.Max(n => n.ID) + 1;

            }
            catch
            {
                return (int)1;


            }
        }
        public static int InsertNewAccount(string Name, int parentID
            , bool CanEdit = false, int secrecy = 0, int level = 2, string notes = "", byte? AccountType = null)
        {
            byte type = AccountType ?? 0;
            if (AccountType == null)
                type = db.Acc_Accounts.Where(x => x.ID == parentID).Select(x => x.AccountType).FirstOrDefault();

            DAL.Acc_Account account = new DAL.Acc_Account()
            {
                ID = GetNewAccountID(),
                Number = GetNewAccountNumber(parentID),
                Name = Name,
                CanEdit = CanEdit,
                Level = level,
                Note = notes,
                ParentID = parentID,
                Secrecy = secrecy,
                InsertUser = CurrentSession.User.ID,
                InsertDate = db.GetSystemDate()
                ,
                AccountType = type
            };
            db.Acc_Accounts.InsertOnSubmit(account);
            db.SubmitChanges();
            return account.ID;
        }
        public static void UpdateAccount(int ID, string Name
            , bool CanEdit = false, int secrecy = 0, int level = 2, string notes = "", byte? AccountType = null)
        {
            DAL.Acc_Account account = db.Acc_Accounts.Where(x => x.ID == ID).First();

            account.LastUpdateUserID = CurrentSession.User.ID;
            account.LastUpdateDate = db.GetSystemDate();
            account.LastUpdateData = account.ToString();
            account.Name = (account.Name != Name) ? Name : account.Name;
            account.Level = (account.Level != level) ? level : account.Level;
            account.Note = (account.Note != notes) ? notes : account.Note;
            account.Secrecy = (account.Secrecy != secrecy) ? secrecy : account.Secrecy;
            db.SubmitChanges();
        }

        

        public static AccountBalance GetAccountBalance(long AccNumber)
        {
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
            double Credit = db.Acc_ActivityDetials.Where(x => x.ACCID == AccNumber).Sum(x => (double?)x.Credit) ?? 0;
            double Debit = db.Acc_ActivityDetials.Where(x => x.ACCID == AccNumber).Sum(x => (double?)x.Debit) ?? 0;

            return new AccountBalance
            {
                Balance = Debit - Credit
            };



        }
        public struct AccountBalance
        {
            public double Balance;
            public string Type
            {
                get
                {
                    if (Balance > 0) return "مدين";
                    else if (Balance < 0) return "دائن";
                    else return string.Empty;
                }

            }

        }
        public struct AccountOpenBalance
        {
            public double Amount;
            public bool IsDebit;
            public DateTime Date;
        }

  

        public static void CreatAccOpenBalance(int accID, string accname
            , AccountOpenBalance openbalance, int capitalAccount = 0)
        {
            DeleteAccountAcctivity("0", accID.ToString());
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
            DAL.Acc_Activity log = new DAL.Acc_Activity();
            DAL.Acc_ActivityDetial logDetail = new DAL.Acc_ActivityDetial();
            DAL.Acc_ActivityDetial logCapital = new DAL.Acc_ActivityDetial();
            log.Type = "0";
            log.TypeID = accID.ToString();
            log.Date = openbalance.Date;
            log.Note = "رصيد افتتاحي " + accname;
            log.Date = DateTime.UtcNow;
            log.InsertDate = DateTime.UtcNow;
            log.LastUpdateDate = DateTime.UtcNow;


            logDetail.ACCID = accID;
            logCapital.ACCID = (capitalAccount == 0) ? CurrentSession.Company.CapitalAccount : capitalAccount;
            logDetail.Note = "رصيد افتتاحي " + accname;
            logCapital.Note = "رصيد افتتاحي " + accname;
            logCapital.DueDate = DateTime.UtcNow;

            logDetail.Debit = (openbalance.IsDebit) ? openbalance.Amount : 0;
            logDetail.Credit = (!openbalance.IsDebit) ? openbalance.Amount : 0;
            logDetail.DueDate = DateTime.UtcNow;

            logCapital.Debit = logDetail.Credit;
            logCapital.Credit = logDetail.Debit;
            
             



            db.Acc_Activities.InsertOnSubmit(log);
            db.SubmitChanges();
            logDetail.AcivityID = log.ID;
            logCapital.AcivityID = log.ID;
            db.Acc_ActivityDetials.InsertOnSubmit(logDetail);
            db.Acc_ActivityDetials.InsertOnSubmit(logCapital);
            db.SubmitChanges();

        }
        public static void DeleteAccountAcctivity(string type, string typeid)
        {

            DeleteAccountAcctivity(type, new List<string>() { typeid });


        }
        internal static void DeleteAccountAcctivity(string type, List<string> ids)
        {
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);

            // from d in db.Acc_Activities where d.Type == type && ids.Contains( d.TypeID)   select d;

            db.Acc_ActivityDetials.DeleteAllOnSubmit(db.Acc_ActivityDetials.Where(d
                => db.Acc_Activities.Where(x => ids.Contains(x.TypeID) && x.Type == type).Select(x => x.ID).Contains(d.AcivityID)));
            db.Acc_Activities.DeleteAllOnSubmit(db.Acc_Activities.Where(x => ids.Contains(x.TypeID) && x.Type == type));
            db.SubmitChanges();

        }
        public static bool CheckForAccountAcctivity(int accID)
        {
            var acctevetylog = from i in db.Acc_ActivityDetials
                               join l in db.Acc_Activities on i.AcivityID equals l.ID
                               where i.ACCID == accID && l.Type != "0"
                               select i.ID;


            return (acctevetylog.Count() > 0);
        }
        public static AccountOpenBalance GetAccountOpenBalance(Int64 accountID)
        {
            var log = from a in db.Acc_Activities
                      join ad in db.Acc_ActivityDetials on a.ID equals ad.AcivityID
                      where ad.ACCID == accountID && a.Type == "0"
                      select new { a.Date, ad.Credit, ad.Debit };
            AccountOpenBalance OpenBalance = new AccountOpenBalance();
            if (log.Count() > 0)
            {
                OpenBalance.Date = (DateTime)log.First().Date;
                OpenBalance.IsDebit = (log.First().Debit > 0);
                OpenBalance.Amount = (log.First().Debit > 0) ? (double)log.First().Debit : (double)log.First().Credit;
            }
            return OpenBalance;
        }

        public readonly static string[] Prossess = new string[]
      {
          "رصيد افتتاحي" ,       //0   D
              "رصيد افتتاحي",    //1 
            "" ,       //2 
      "اضافه صنف",    //LangResource.ItemAdd ,          //3
      "صرف صنف",    //LangResource.ItemTake ,         //4 
      "نقل اصناف",    //LangResource.ItemStoreMove,     //5
      "فاتوره مبيعات",    //LangResource.SalesInvoice,      //6   D
      "طلب بيع",    //LangResource.SalesRequest ,     //7 
      "امر بيع",    //LangResource.SalesOrder   ,     //8
      "مردود مبيعات",    //LangResource.SalesReturn    ,   //9   C
      "فاتوره مشتريات ",    //LangResource.PurchaseInvoice  , //10  C
      "طلب شراء",    //LangResource.PurchaseRequest,   //11
      "امر شراء",    //LangResource.PurchaseOrder  ,   //12
      "مردود مشريات",    //LangResource.PurchaseReturn   , //13  D
      "ايرادات",    //LangResource.Revenues         , //14  D
      "مصروفات",    //LangResource.Expenses         , //15  C
      "سند قبض نقدي",    //LangResource.CashNoteIn       , //16  D
      "سند دفع نقدي",    //LangResource.CashNoteOut      , //17  C
      "تحويل نقديه",    //LangResource.CashTransfer     , //18  C
      "امر انتاج",    //LangResource.ProductionOrder  , //19  C
      "",    //               "قيد تسويه "  , //20  
      "استماره راتب",    //LangResource.HR_Pay    ,        //21
      "سلفه",    //LangResource.HR_loan    ,       //22
      "عرض سعر بيع",    //LangResource.SalesPriceOffer     ,       //23
      "بدون",    //LangResource.Without      ,       //24
      "25",    //LangResource.DirectPay, //         25,       
      "26",    //LangResource.DirectPostToStore, // 26,         
      "27",    //LangResource.DirectWithdrawFromStore ,//=27,    
      "",    //LangResource .PurchasePriceOffer,

      };

        internal static void InsertUserLog(int v1, string callerForm, string v2, string v3)
        {
           // throw new NotImplementedException();
        }
        internal static void RestoreGridLayout(GridView view, Form frm)
        {
            MasterClass.RestoreGridLayout(view, frm);
        }
        internal static void SaveGridLayout(GridView view, Form frm)
        {
            MasterClass.SaveGridLayout(view, frm);
        }
        internal static void TranslateGridColumn(GridView gridView1)
        {
           // throw new NotImplementedException();
        }
        public static void CustomColumn(this GridView grid, string Column, string Caption, bool Visible = true, bool AllowFocus = true, int VisibleIndex = -1, int Width = -1)
        {
            grid.Columns[Column].Caption = Caption;
            grid.Columns[Column].Visible = Visible;
            grid.Columns[Column].OptionsColumn.AllowFocus = AllowFocus;
            if (VisibleIndex > -1)
                grid.Columns[Column].VisibleIndex = VisibleIndex;
            if (Width > -1)
                grid.Columns[Column].Width = Width;
        }
    }
}
