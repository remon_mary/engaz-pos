﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByStro.Clases
{
    public class CustomItem
    {

        public int ID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public CustomUnit  CustomUnit { get; set; }
        public List<CustomUnit> Units { get; set; }
        public int? CategoryID { get; set; }
        public string CategoryName { get; set; }
        public static List<CustomItem> GetCustomItems()
        {
            using (DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String))
            {
                return (from x in db.Prodecuts
                        select new CustomItem
                        {
                            ID = x.ProdecutID,
                            Code = x.FiestUnitBarcode,
                            Name = x.ProdecutName,
                            CategoryID = x.CategoryID,
                            CategoryName = db.Categories.SingleOrDefault(c=>c.CategoryID== x.CategoryID).CategoryName,
                            Units = CustomUnit.GetUnits(x),
                            CustomUnit = CustomUnit.GetCustomBalance(x)
                        }).ToList();
            }
        }
    }
    public class CustomUnit
    {
        public static string GetBalance(DAL.Prodecut x)
        {
            StringBuilder stringBuilder = new StringBuilder();
            using (DAL.DBDataContext dbc = new DAL.DBDataContext(Properties.Settings.Default.Connection_String))
            {
                var logSumIN = dbc.Inv_StoreLogs.Where(s => s.ItemID == x.ProdecutID).Sum(s => s.ItemQuIN) ?? 0;
                var logSumOut = dbc.Inv_StoreLogs.Where(s => s.ItemID == x.ProdecutID).Sum(s => s.ItemQuOut) ?? 0;
                var balance = ((double)logSumIN - (double)logSumOut);
                double ThreeBalance = 0, SecoundBalance = 0, FiestBalance = 0;
                double ThreeBalanceMod = 0, SecoundBalanceMod = 0;
                if (!string.IsNullOrWhiteSpace(x.ThreeUnitOperating))
                {
                    stringBuilder.Append(x.ThreeUnit);
                    ThreeBalance = balance / Convert.ToDouble(x.ThreeUnitOperating);
                    ThreeBalanceMod = balance % Convert.ToDouble(x.ThreeUnitOperating);
                    stringBuilder.Append($"({ ThreeBalance }) / ");
                }
                if (!string.IsNullOrWhiteSpace(x.SecoundUnitOperating))
                {
                    stringBuilder.Append(x.SecoundUnit);
                    if (ThreeBalance == 0)
                    {
                        SecoundBalance = (int)(balance / Convert.ToDouble(x.SecoundUnitOperating));
                        SecoundBalanceMod = balance % Convert.ToDouble(x.SecoundUnitOperating);
                        stringBuilder.Append($"({ SecoundBalance }) / ");
                    }
                    else
                    {
                        SecoundBalance = ThreeBalanceMod / Convert.ToDouble(x.SecoundUnitOperating);
                        SecoundBalanceMod = ThreeBalanceMod % Convert.ToDouble(x.SecoundUnitOperating);
                        stringBuilder.Append($"({ SecoundBalance }) / ");

                    }
                }
                if (!string.IsNullOrWhiteSpace(x.FiestUnitOperating))
                {
                    stringBuilder.Append(x.FiestUnit);
                    if (SecoundBalance == 0)
                    {
                        FiestBalance = balance / Convert.ToDouble(x.FiestUnitOperating);
                        stringBuilder.Append($"({ SecoundBalance }) / ");
                    }
                    else
                    {
                        FiestBalance = SecoundBalanceMod / Convert.ToDouble(x.FiestUnitOperating);
                        stringBuilder.Append($"({ FiestBalance }) / ");
                    }
                }
            }
            return stringBuilder.ToString();
        }
        public static CustomUnit GetCustomBalance2(DAL.Prodecut x)
        {
            CustomUnit customUnit = new CustomUnit();
            StringBuilder stringBuilder = new StringBuilder();
            using (DAL.DBDataContext dbc = new DAL.DBDataContext(Properties.Settings.Default.Connection_String))
            {
                var logSumIN = dbc.Inv_StoreLogs.Where(s => s.ItemID == x.ProdecutID).Sum(s => s.ItemQuIN) ?? 0;
                var logSumOut = dbc.Inv_StoreLogs.Where(s => s.ItemID == x.ProdecutID).Sum(s => s.ItemQuOut) ?? 0;
                var balance = ((double)logSumIN - (double)logSumOut);


                double ThreeBalance = 0, SecoundBalance = 0, FiestBalance = 0;
                double ThreeBalanceMod = 0, SecoundBalanceMod = 0;
                if (!string.IsNullOrWhiteSpace(x.ThreeUnitOperating))
                {
                    stringBuilder.Append(x.ThreeUnit);
                    ThreeBalance = balance / Convert.ToDouble(x.ThreeUnitOperating);
                    ThreeBalanceMod = balance % Convert.ToDouble(x.ThreeUnitOperating);
                    stringBuilder.Append($"({ ThreeBalance })");
                    stringBuilder.Append(" / ");
                }
                if (!string.IsNullOrWhiteSpace(x.SecoundUnitOperating))
                {
                    stringBuilder.Append(x.SecoundUnit);
                    if (ThreeBalance == 0)
                    {
                        SecoundBalance = (int)(balance / Convert.ToDouble(x.SecoundUnitOperating));
                        SecoundBalanceMod = balance % Convert.ToDouble(x.SecoundUnitOperating);
                        stringBuilder.Append($"({ SecoundBalance })");
                    }
                    else
                    {
                        SecoundBalance = ThreeBalanceMod / Convert.ToDouble(x.SecoundUnitOperating);
                        SecoundBalanceMod = ThreeBalanceMod % Convert.ToDouble(x.SecoundUnitOperating);
                        stringBuilder.Append($"({ SecoundBalance })");
                        stringBuilder.Append(" / ");
                    }
                }
                if (!string.IsNullOrWhiteSpace(x.FiestUnitOperating))
                {
                    stringBuilder.Append(x.FiestUnit);
                    if (SecoundBalance == 0)
                    {
                        FiestBalance = balance / Convert.ToDouble(x.FiestUnitOperating);
                        stringBuilder.Append($"({ SecoundBalance })");
                    }
                    else
                    {
                        FiestBalance = SecoundBalanceMod / Convert.ToDouble(x.FiestUnitOperating);
                        stringBuilder.Append($"({ FiestBalance })  ");
                        stringBuilder.Append(" / ");
                    }
                }
                customUnit.Name = x.ProdecutName;
                customUnit.Balance = balance;
                customUnit.BalanceString = stringBuilder.ToString();
                customUnit.logSumIN = logSumIN;
                customUnit.logSumOut = logSumOut;
            }
            return customUnit;
        }

        public static CustomUnit GetCustomBalance(DAL.Prodecut x)
        {
            CustomUnit customUnit = new CustomUnit();
            StringBuilder stringBuilder = new StringBuilder();
            using (DAL.DBDataContext dbc = new DAL.DBDataContext(Properties.Settings.Default.Connection_String))
            {
                var storeLogs = dbc.Inv_StoreLogs.Where(s => s.ItemID == x.ProdecutID).AsQueryable();
                var logSumIN = storeLogs.Sum(s => s.ItemQuIN) ?? 0;
                var logSumOut = storeLogs.Sum(s => s.ItemQuOut) ?? 0;
                var balance = ((double)logSumIN - (double)logSumOut);
                var numSell = storeLogs.Where(s => s.ItemQuIN == 0).Count();
                var avgSell = (storeLogs.Where(s => s.ItemQuIN == 0).Sum(s => s.SellPrice) ?? 0) / (numSell == 0 ? 1 : numSell);
                var numBuy = storeLogs.Where(s => s.ItemQuOut == 0).Count();
                var avgBuy = (storeLogs.Where(s => s.ItemQuOut == 0).Sum(s => s.BuyPrice) ?? 0) / (numBuy == 0 ? 1 : numBuy);

                double ThreeBalance = 0, SecoundBalance = 0, FiestBalance = 0;
                double FiestBalanceMod = 0, SecoundBalanceMod = 0;

                if (!string.IsNullOrWhiteSpace(x.FiestUnitFactor))
                {
                    FiestBalance = balance / Convert.ToDouble(x.FiestUnitFactor);
                    FiestBalanceMod = balance % Convert.ToDouble(x.FiestUnitFactor);

                    if ((int)FiestBalance > 0)
                    {
                        stringBuilder.Append($" {(int)FiestBalance } ");
                        stringBuilder.Append(x.FiestUnit);
                    }
                    customUnit.Name = x.FiestUnit;
                    //customUnit.SellPrice = GetAvgPrice(x.FiestUnitPrice1, x.FiestUnitPrice2, x.FiestUnitPrice3);
                }

                if (!string.IsNullOrWhiteSpace(x.SecoundUnitFactor))
                {
                    if (FiestBalance == 0)
                    {
                        SecoundBalance = (int)(balance / Convert.ToDouble(x.SecoundUnitFactor));
                        SecoundBalanceMod = balance % Convert.ToDouble(x.SecoundUnitFactor);
                    }
                    else
                    {
                        SecoundBalance = FiestBalanceMod / Convert.ToDouble(x.SecoundUnitFactor);
                        SecoundBalanceMod = FiestBalanceMod % Convert.ToDouble(x.SecoundUnitFactor);
                    }
                    if (SecoundBalance > 0)
                    {
                        stringBuilder.Append($" {(int)SecoundBalance } ");
                        stringBuilder.Append(x.SecoundUnit);
                    }
                    customUnit.Name = x.SecoundUnit;
                    //customUnit.SellPrice = GetAvgPrice(x.SecoundUnitPrice1, x.SecoundUnitPrice2, x.SecoundUnitPrice3);
                }

                if (!string.IsNullOrWhiteSpace(x.ThreeUnitFactor))
                {
                    if (SecoundBalanceMod > 0)
                        ThreeBalance = SecoundBalanceMod / Convert.ToDouble(x.ThreeUnitFactor);
                    if (ThreeBalance > 0)
                    {
                        stringBuilder.Append($" { ThreeBalance } ");
                        stringBuilder.Append(x.ThreeUnit);
                    }
                    customUnit.Name = x.ThreeUnit;
                    //customUnit.SellPrice = GetAvgPrice(x.ThreeUnitPrice1, x.ThreeUnitPrice2, x.ThreeUnitPrice3);
                }

                customUnit.SellPrice = avgSell;
                customUnit.BuyPrice = avgBuy;
                customUnit.Balance = balance;
                customUnit.BalanceString = stringBuilder.ToString();
                customUnit.logSumIN = logSumIN;
                customUnit.logSumOut = logSumOut;
            }
            return customUnit;
        }

        public static List<CustomUnit> GetUnits(DAL.Prodecut x)
        {
            List<CustomUnit> customUnits = new List<CustomUnit>();
            using (DAL.DBDataContext dbc = new DAL.DBDataContext(Properties.Settings.Default.Connection_String))
            {
                var logSumIN = dbc.Inv_StoreLogs.Where(s => s.ItemID == x.ProdecutID).Sum(s => s.ItemQuIN) ?? 0;
                var logSumOut = dbc.Inv_StoreLogs.Where(s => s.ItemID == x.ProdecutID).Sum(s => s.ItemQuOut) ?? 0;
                var balance = ((double)logSumIN - (double)logSumOut);

                if (!string.IsNullOrWhiteSpace(x.FiestUnitFactor))
                    customUnits.Add(new CustomUnit { Balance = balance / Convert.ToDouble(x.FiestUnitFactor),
                        logSumIN = logSumIN,
                        logSumOut = logSumOut,
                        Name = x.FiestUnit,
                        BuyPrice = x.ProdecutBayPrice,
                        SellPrice = GetAvgPrice(x.FiestUnitPrice1, x.FiestUnitPrice2, x.FiestUnitPrice3)
                    });
                if (!string.IsNullOrWhiteSpace(x.SecoundUnitFactor))
                    customUnits.Add(new CustomUnit { Balance = balance / Convert.ToDouble(x.SecoundUnitFactor),
                        logSumIN = logSumIN,
                        logSumOut = logSumOut,
                        Name = x.SecoundUnit,
                        BuyPrice = x.ProdecutBayPrice,
                        SellPrice = GetAvgPrice(x.SecoundUnitPrice1, x.SecoundUnitPrice2, x.SecoundUnitPrice3)
                    });
                if (!string.IsNullOrWhiteSpace(x.ThreeUnitFactor))
                    customUnits.Add(new CustomUnit
                    {
                        logSumIN = logSumIN,
                        logSumOut = logSumOut,
                        Balance = balance / Convert.ToDouble(x.ThreeUnitFactor),
                        Name = x.ThreeUnit,
                        BuyPrice = x.ProdecutBayPrice,
                        SellPrice = GetAvgPrice(x.ThreeUnitPrice1, x.ThreeUnitPrice2, x.ThreeUnitPrice3),

                    });
                return customUnits;
            }
        }
        public static double GetAvgPrice(double? Price1, double? Price2, double? Price3)
        {
            double Price = 0;
            int num = 0;
            if (Price1.HasValue && Price1.Value > 0)
            {
                Price += Price1.Value;
                num++;
            }
            if (Price2.HasValue && Price2.Value > 0)
            {
                Price += Price2.Value;
                num++;
            }
            if (Price3.HasValue && Price3.Value > 0)
            {
                Price += Price3.Value;
                num++;
            }
            return Price / num;
        }
      [Display(Name ="اسم الوحدة")]
        public string Name { get; set; }
        [Display(Name = "سعر الشراء")]
        public double? BuyPrice { get; set; }
        [Display(Name = "سعر البيع")]
        public double? SellPrice { get; set; }
        [Display(Name = "وارد")]
        public double logSumIN { get; set; }
        [Display(Name = "صادر")]
        public double logSumOut { get; set; }
        [Display(Name = "الرصيد")]
        public double Balance { get; set; }
        [Display(Name = "الرصيد مقسم")]
        public string BalanceString { get; set; }
    }

}
