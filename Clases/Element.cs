﻿
using ByStro.Forms;
using DevExpress.LookAndFeel;
using DevExpress.Utils.Svg;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ByStro.Clases
{
    public class Element
    {
        public Element()
        {
            Id = MaxId++;
        }
        static int MaxId = 1;
        public Element(Element element)
        {
            Id = MaxId++;
            if (element != null)
            {
                MasterId = element.Id;
            }
        }

        #region Properties

        public int Id { get; set; }
        public int MasterId { get; set; }
        public string Name { get; set; }
        public string Caption { get; set; }
        public SvgImage SvgImage { get; set; }
        public bool IsPanel { get; set; }
        public bool IsShowDialog { get; set; }

        public Image IconImage
        {
            get
            {
                if (SvgImage == null) return default;
                SvgBitmap bm = new SvgBitmap(SvgImage);
                var r = new Rectangle(0, 0, 25, 25);
                var palette = SvgPaletteHelper.GetSvgPalette(UserLookAndFeel.Default,
                    DevExpress.Utils.Drawing.ObjectState.Normal);

                return bm.Render(palette,50);
            }

        }
        //public bool CanShow { get; set; }
        //public bool CanOpen { get; set; }
        //public bool CanAdd { get; set; }
        //public bool CanEdit { get; set; }
        //public bool CanDelete { get; set; }
        //public bool CanPrint { get; set; }
        #endregion
        //public List<HelparClass.UserAction> UserActions { get; set; } = new List<HelparClass.UserAction> {
        //    UserAction.Add,
        //    UserAction.Delete,
        //    UserAction.Edit,
        //    UserAction.Open,
        //    UserAction.Print,
        //    UserAction.Show
        //}; 
    }


    public static class Screens
    {

        #region Accounts
        public static Element Accounts = new Element()
        {
            Caption = "الحسابات",
        };  
        public static Element AccountStatment = new Element(Accounts)
        {
            Name = "frm_AccountStatment",
            Caption = "كشف حساب",
            SvgImage = Properties.Resources.viewmergeddata,
        };
        public static Element AccountTree = new Element(Accounts)
        {
            Name = "frm_AccountTree",
            Caption = "شجرة الحسابات",
            SvgImage = Properties.Resources.inserttreeview,
        };
        #endregion
      
        #region Financial
        public static Element Financial = new Element()
        {
            Caption = "المالية",
        };
        public static Element RevenuEntry = new Element(Financial)
        {
            Name = nameof(Forms.RevenueEntry),
            Caption = "تسجيل ايردات",           
            SvgImage = Properties.Resources.business_cash,
        };
        public static Element RevenuEntryList = new Element(Financial)
        {
            Name = nameof(Forms.RevenueEntryList),
            Caption = "قائمة الايردات",
            SvgImage = Properties.Resources.business_cash,
        };
        
        public static Element ExpenceEntry = new Element(Financial)
        {
            Name = nameof(Forms.ExpenceEntry),
            Caption = "تسجيل مصروفات",
            SvgImage = Properties.Resources.business_cash,
        };
        public static Element ExpenceEntryList = new Element(Financial)
        {
            Name = nameof(Forms.ExpenceEntryList),
            Caption = "قائمة المصروفات",
            SvgImage = Properties.Resources.business_cash,
        };
        
        public static Element NewCashNoteOut = new Element(Financial)
        {
            Name = nameof(Forms.NewCashNoteOut),
            Caption = "سند دفع جديد",
            SvgImage = Properties.Resources.business_cash,
        };
        public static Element CashNoteOutList = new Element(Financial)
        {
            Name = nameof(Forms.CashNoteOutList),
            Caption = "قائمه سندات الدفع ",
            SvgImage = Properties.Resources.financial,
        };
       
        public static Element NewCashNoteIn = new Element(Financial)
        {
            Name = nameof(Forms.NewCashNoteIn),
            Caption = "سند القبض جديد",
            SvgImage = Properties.Resources.business_cash,
        };
        public static Element CashNoteInList = new Element(Financial)
        {
            Name = nameof(Forms.CashNoteInList),
            Caption = "قائمه سندات القبض ",
            SvgImage = Properties.Resources.financial,
        };

        public static Element AccountTransaction = new Element(Financial)
        {
            Name = nameof(Forms.frm_AccountTransaction),
            Caption = "تحويل نقدي جديد",
            SvgImage = Properties.Resources.convertto,
        };
        public static Element AccountTransactionList = new Element(Financial)
        {
            Name = nameof(Forms.frm_AccountTransactionList),
            Caption = "قائمة التحويلات النقدية",
            SvgImage = Properties.Resources.financial,
        };
       
        public static Element DrawerDailyJournal = new Element(Financial)
        {
            Name = nameof(Forms.frm_DrawerDailyJournal),
            Caption = "بيان يومي لحركه الخزينه",
            SvgImage = Properties.Resources.accounting,
        };

        public static Element NewBank = new Element(Financial)
        {
            Name = nameof(Forms.frm_Bank),
            Caption = "اضافة بنوك جديد",
            SvgImage = Properties.Resources.business_bank,
        };
        public static Element BankList = new Element(Financial)
        {
            Name = nameof(Forms.frm_BankList),
            Caption = "قائمة البنوك",
            SvgImage = Properties.Resources.business_bank,
        };
   
        public static Element NewDrawer = new Element(Financial)
        {
            Name = nameof(Forms.frm_Drawer),
            Caption = "اضافة خزنة جديدة",
            SvgImage = Properties.Resources.shipmentreceived,
        }; 
        public static Element DrawerList = new Element(Financial)
        {
            Name = nameof(Forms.DrawerList),
            Caption = "قائمة الخزن",
            SvgImage = Properties.Resources.shipmentreceived,
        };
    
        public static Element NewPayCard = new Element(Financial)
        {
            Name = nameof(Forms.frm_PayCards),
            Caption = "اضافة بطاقة دفع جديدة",
            SvgImage = Properties.Resources.barcode,
        };   
        public static Element PayCardsList = new Element(Financial)
        {
            Name = nameof(Forms.frm_PayCardsList),
            Caption = "قائمة بطاقات الدفع",
            SvgImage = Properties.Resources.barcode,
        };
        #endregion

        #region Store
        public static Element Store = new Element()
        {
            Caption = "المخزن",
        };
        public static Element Item_Form = new Element(Store)
        {
            Name = "Prodecut_frm",
            Caption = "اضافة صنف",
            IsPanel = true,
            SvgImage = Properties.Resources.bo_price_item,
        }; 
        public static Element AddProductsRange = new Element(Store)
        {
            Name = "frm_AddProductsRange",
            Caption = "اضافه / استيردا اصناف",
            SvgImage = Properties.Resources.showdetail,
        };
        public static Element Category = new Element(Store)
        {
            Name = "Category_frm",
            Caption = "المجموعات",
            SvgImage = Properties.Resources.group,
        };
        public static Element Unit = new Element(Store)
        {
            Name = "Unit_frm",
            Caption = "وحدات القياس",
            SvgImage = Properties.Resources.removedataitems,
        };
        public static Element Store_frm = new Element(Store)
        {
            Name =nameof(frm_Stores),
            Caption = "المخازن",
            SvgImage = Properties.Resources.shopping_store,

        };
        public static Element OpenBalanceInvoice = new Element(Store)
        {
            Name = "ItemOpenBalance",
            Caption = "اصناف بدايه المده",
            SvgImage = Properties.Resources.bo_appointment,

        };
        public static Element ItemDamage = new Element(Store)
        {
            Name = "ItemDamage",
            Caption = "سند هالك اصناف",
            SvgImage = Properties.Resources.rotate,

        };
        public static Element frm_ItemStoreMove = new Element(Store)
        {
            Name = "ItemStoreMove",
            Caption = "تحويل بين المخازن",
            SvgImage = Properties.Resources.convertto,

        };
        public static Element frm_SearchItems = new Element(Store)
        {
            Name = "frm_SearchItems",
            Caption = "بحث الاصناف",
            IsPanel = true,
            SvgImage = Properties.Resources.enablesearch,

        };
        public static Element frm_Inv_Color = new Element(Store)
        {
            Name = "frm_Inv_Color",
            Caption = "اضافة لون",
            SvgImage = Properties.Resources.morecolors,

        };
        public static Element frm_Inv_Size = new Element(Store)
        {
            Name = "frm_Inv_Size",
            Caption = "اضافة حجم",
            SvgImage = Properties.Resources.stretch,

        };
        #endregion

        #region Control
        public static Element control = new Element()
        {
            Caption = "التحكم",
        };
        public static Element uc_Administrative_Report1 = new Element(control)
        {
            Name = "uc_Administrative_Report",
            Caption = "المراقبه الاداريه",
            IsPanel = true,
            SvgImage = Properties.Resources.viewmergeddata,
        };
        public static Element User_Permetion_frm = new Element(control)
        {
            Name = "User_Permetion_frm",
            Caption = "صلاحيات المستخدمين",
            IsPanel = true,
            SvgImage = Properties.Resources.bo_user,
        };
        public static Element SettingInvoice_frm = new Element(control)
        {
            Name = "SettingInvoice_frm",
            Caption = "اعداد الفواتير",
            SvgImage = Properties.Resources.properties
        };
        public static Element frm_BackupRestore = new Element(control)
        {
            Name = "frm_BackupRestore",
            Caption = "النسخ الاحتياطي والاسترجاع",
            SvgImage = Properties.Resources.crossdatasourcefiltering
        };
        public static Element ConnectSQLServer_Form = new Element(control)
        {
            Name = "ConnectSQLServer_Form",
            Caption = "اعداد الاتصال",
            SvgImage = Properties.Resources.managedatasource
        };
        public static Element CombanyInformatuon_FormAR = new Element(control)
        {
            Name =  nameof(frm_CompanyInfo),
            Caption = "بيانات الشركة",
            SvgImage = Properties.Resources.bluedatabargradient,
            IsShowDialog = true
        };
        public static Element FontDialog = new Element(control)
        {
            Name = "FontDialog",
            Caption = "تغير خط البرنامج",
            SvgImage = Properties.Resources.changefontstyle,
        };
        public static Element frm_SystemSettings = new Element(control)
        {
            Name = "frm_SystemSettings",
            Caption = "اعداد ميزان الباركود",
            SvgImage = Properties.Resources.barcodeshowtext,
            IsShowDialog = true
        };
        public static Element frm_BarcodeTemplates = new Element(control)
        {
            Name = "frm_BarcodeTemplates",
            Caption = "اعداد طباعه الباركود",
            SvgImage = Properties.Resources.shopping_cashvoucher,
            IsShowDialog = true
        };
        public static Element frm_SelectDefaultPrinter = new Element(control)
        {
            Name = "frm_SelectDefaultPrinter",
            Caption = "تحديد الطابعة الافتراضية",
            SvgImage = Properties.Resources.bo_price_item,
        };
        public static Element frm_LocalSettings = new Element(control)
        {
            Name = "frm_LocalSettings",
            Caption = "اعدادات خاصه",
            SvgImage = Properties.Resources.bo_user,
            IsShowDialog = true
        };
        #endregion

        #region Sales
        public static Element Sales = new Element()
        {
            Caption = "المبيعات",
        };
        public static Element SalesInvoice = new Element(Sales)
        {
            Name = "SalesInvoice",
            Caption = "فاتوره مبيعات",
            IsPanel = true,
            SvgImage = Properties.Resources.fieldlistpanelpivottable,
            
        };
        public static Element SalesInvoiceOther = new Element(Sales)
        {
            Name = "SalesInvoiceOther",
            Caption = "فاتوره مبيعات اخري",
            IsPanel = true,
            SvgImage = Properties.Resources.fieldlistpanelpivottable,
            
        };
        public static Element SalesReturn = new Element(Sales)
        {
            Name = "SalesReturn",
            Caption = "مرتجع المبيعات",
            IsPanel = true,
            SvgImage = Properties.Resources.resetlayoutoptions,
        };
        public static Element BillOfferPricer_frm = new Element(Sales)
        {
            Name = "BillOfferPricer_frm",
            Caption = "فاتوره عرض السعر",
            SvgImage = Properties.Resources.repeatallitemlabelspivottable,
        };
        public static Element AddInvoiceRangeSalesInvoice = new Element(Sales)
        {
            Name = "AddInvoiceRangeSalesInvoice",
            Caption = "اضافه مجموعه من فواتير البيع",
            SvgImage = Properties.Resources.bo_price_item,
        };
      
        public static Element Tables = new Element(Sales)
        {
            Name = "Tables",
            Caption = "ادارة الطاولات",
            SvgImage = Properties.Resources.selecttablecell,
        };
        public static Element NewDiningTables = new Element(Tables)
        {
            Name = nameof(Forms.frm_DiningTables),
            Caption = "الطاولات",
        };
        public static Element NewDiningTablesOrder = new Element(Tables)
        {
            Name = nameof(Forms.frm_DiningTablesOrder),
            Caption = "امر تشغيل طاولة جديد",
        };
        public static Element DiningTablesOrderDoneList = new Element(Tables)
        {
            Name = nameof(Forms.DiningTablesOrderDoneList),
            Caption = "قائمة اوامر التشغيل المنتهية",
        };
        public static Element DiningTablesOrderNotDoneList = new Element(Tables)
        {
            Name = nameof(Forms.DiningTablesOrderNotDoneList),
            Caption = "قائمة اوامر التشغيل غير المنتهية",
        };

        public static Element Delivery = new Element(Sales)
        {
            Name = "Delivery",
            Caption = "الديلفري",
            SvgImage = Properties.Resources.shopping_delivery,
        };
        public static Element frm_Drivers = new Element(Delivery)
        { 
            Name = "frm_Drivers", 
            Caption = "السائقون", 
        };
        public static Element frm_DeliveryOrder = new Element(Delivery)
        { 
            Name = "frm_DeliveryOrder", 
            Caption = "طلب توصيل جديد", 
        };
        public static Element frm_DeliveryOrderListGoing = new Element(Delivery)
        { 
            Name = "frm_DeliveryOrderListGoing", 
            Caption = "اوامر توصيل جارية", 
        };
        public static Element frm_DeliveryOrderListEnd = new Element(Delivery)
        { 
            Name = "frm_DeliveryOrderListEnd", 
            Caption = "اوامر توصيل منتهية", 
        };

        public static Element Salesman = new Element(Sales)
        {
            Name = "Salesman",
            Caption = "المناديب",
            SvgImage = Properties.Resources.bo_employee,
        };
        public static Element NewSalesman = new Element(Salesman)
        {
            Name = nameof(Forms.frm_Salesman),
            Caption = "اضافة مندوب جديد",
        };
        public static Element SalesmanList = new Element(Salesman)
        {
            Name = nameof(Forms.SalesmanList),
            Caption = "قائمة المناديب",
        };

        #endregion

        #region Purchases
        public static Element Purchases = new Element()
        {
            Caption = "المشتريات",
        };
        public static Element PurchaseInvoice = new Element(Purchases)
        {
            Name = "PurchaseInvoice",
            Caption = "فاتورة مشتريات",
            IsPanel = true,
            SvgImage = Properties.Resources.fieldlistpanelpivottable,
        };
        public static Element PurchaseReturn = new Element(Purchases)
        {
            Name = "PurchaseReturn",
            Caption = "مرتجع المشتريات",
            IsPanel = true,
            SvgImage = Properties.Resources.resetlayoutoptions,
        };
        public static Element BillPurchaseOrder_frm = new Element(Purchases)
        {
            Name = "BillPurchaseOrder_frm",
            Caption = "فاتورة طلب شراء",
            SvgImage = Properties.Resources.buynow
        }; public static Element AddInvoiceRangePurchaseInvoice = new Element(Purchases)
        {
            Name = "AddInvoiceRangePurchaseInvoice",
            Caption = "اضافه مجموعه من فواتير الشراء",
            SvgImage = Properties.Resources.bo_price_item,
        };

        #endregion

        #region Customer And Vendor
        public static Element CustomerVendor = new Element()
        {
            Caption = "العملاء والموردين",
        };
        public static Element Customer = new Element(CustomerVendor)
        {
            Name = "frm_Customer",
            Caption = "اضافة عميل جديد",
            SvgImage = Properties.Resources.bo_person,
        };
        public static Element CustomerList = new Element(CustomerVendor)
        {
            Name = "frm_CustomerList",
            Caption = "قائمة العملاء",
            SvgImage = Properties.Resources.bo_person,
        };
        public static Element AddCustomerRange = new Element(CustomerVendor)
        {
            Name = "frm_AddCustomerRange",
            Caption = "اضافه مجموعه من العملاء",
            SvgImage = Properties.Resources.bo_price_item,
        }; 
        public static Element Vendor = new Element(CustomerVendor)
        {
            Name = "frm_Vendor",
            Caption = "اضافة مورد جديد",
            SvgImage = Properties.Resources.bo_person,
        };
        public static Element VendorList = new Element(CustomerVendor)
        {
            Name = "frm_VendorList",
            Caption = "قائمة الموردين",
            SvgImage = Properties.Resources.bo_person,
        };
        public static Element AddVendorRange = new Element(CustomerVendor)
        {
            Name = "frm_AddVendorRange",
            Caption = "اضافه مجموعه من الموردين",
            SvgImage = Properties.Resources.bo_price_item,
        };
        #endregion

        #region PersonnelAffairs
        public static Element PersonnelAffairs = new Element()
        {
            Caption = "شؤون الموظفين",
        };
        public static Element Employee_frm = new Element(PersonnelAffairs)
        {
            Name = "Employee_frm",
            Caption = "قائمة الموظفين",
            SvgImage = Properties.Resources.card,
        };
        public static Element WorkPartCompany_frm = new Element(PersonnelAffairs)
        {
            Name = "WorkPartCompany_frm",
            Caption = "انواع الوظائف في الشركة",
            SvgImage = Properties.Resources.tasks,
        };
        public static Element PartCompany_frm = new Element(PersonnelAffairs)
        {
            Name = "PartCompany_frm",
            Caption = "اقسام العمل في الشركة",
            SvgImage = Properties.Resources.business_report,
        };
        public static Element Holiday_frm = new Element(PersonnelAffairs)
        {
            Name = "Holiday_frm",
            Caption = "انواع الاجازات",
            SvgImage = Properties.Resources.bo_appearance,
        };
        public static Element Employee_AdditionsType_frm = new Element(PersonnelAffairs)
        {
            Name = "Employee_AdditionsType_frm",
            Caption = "انواع الاستحققات",
            SvgImage = Properties.Resources.financial,
        };
        public static Element Employee_DeductionType_frm = new Element(PersonnelAffairs)
        {
            Name = "Employee_DeductionType_frm",
            Caption = "انواع الاستقطعات",
            SvgImage = Properties.Resources.actions_removecircled,
        };
        public static Element EmployeeAttendance_frm = new Element(PersonnelAffairs)
        {
            Name = "EmployeeAttendance_frm",
            Caption = "تسجيل حضور",
            SvgImage = Properties.Resources.next7days,
        };
        public static Element Employee_GOAttendance_frm = new Element(PersonnelAffairs)
        {
            Name = "Employee_GOAttendance_frm",
            Caption = "تسجيل انصراف",
            SvgImage = Properties.Resources.deferred,
        };
        public static Element Employee_Additions_frm = new Element(PersonnelAffairs)
        {
            Name = "Employee_Additions_frm",
            Caption = "تسجيل استحقاق",
            SvgImage = Properties.Resources.currency,
        };
        public static Element Employee_Deduction_frm = new Element(PersonnelAffairs)
        {
            Name = "Employee_Deduction_frm",
            Caption = "تسجيل استقطاع",
            SvgImage = Properties.Resources.actions_removecircled,
        };
        public static Element Employee_Holiday_frm = new Element(PersonnelAffairs)
        {
            Name = "Employee_Holiday_frm",
            Caption = "تسجيل اجازة",
            SvgImage = Properties.Resources.bo_appearance,
        };
        public static Element Employee_Salary_frm = new Element(PersonnelAffairs)
        {
            Name = "Employee_Salary_frm",
            Caption = "دفع مرتبات",
            SvgImage = Properties.Resources.financial,
        };


        #endregion

        #region Depositing and cashing
     
        public static Element Instalment = new Element()
        {
            Caption = "الاقساط",
        };
        //public static Element frm_InstalmentList = new Element(Instalment)
        //{
        //    Name = nameof(ByStro.Forms.frm_InstalmentList),
        //    Caption = "قائمة الاقساط",
        //};
        public static Element frm_Instalment = new Element(Instalment)
        {
            Name = nameof(ByStro.Forms.frm_Instalment),
            Caption = "اضافة قسط جديد",
        };
        public static Element frm_PayInstallmentList = new Element(Instalment)
        {
            Name = "PayInstallmentUnPaidList",
            Caption = "عرض الاقساط المستحقه",
        };
        public static Element frm_PayInstallment = new Element(Instalment)
        {
            Name = "frm_PayInstallment",
            Caption = "سداد قسط",
        };
        public static Element frm_PayInstallmentPaidList = new Element(Instalment)
        {
            Name = "PayInstallmentPaidList",
            Caption = "عرض الاقساط المسدده",
        };
        #endregion

        #region Maintenance
        public static Element Maintenance = new Element()
        {
            Caption = "الصيانه",
        };
        public static Element MaintenanceCompeny = new Element(Maintenance)
        {
            Name = "MaintenanceCompeny_frm",
            Caption = "الشركات المصنعه",
            SvgImage = Properties.Resources.grouptooltips,
        };
        public static Element DeviceType_frm = new Element(Maintenance)
        {
            Name = "DeviceType_frm",
            Caption = "انواع الاجهزة",
            SvgImage = Properties.Resources.office2010,
        };
        public static Element DamagedType_frm = new Element(Maintenance)
        {
            Name = "DamagedType_frm",
            Caption = "انواع الاعطال",
            SvgImage = Properties.Resources.chartdatalabels_none,
        };
        
        public static Element MaintenanceRecived_frm = new Element(Maintenance)
        {
            Name = "MaintenanceRecived_frm",
            Caption = "سند استلام صيانه",
            SvgImage = Properties.Resources.business_presentation,
        };
        public static Element MaintenanceReturn_frm = new Element(Maintenance)
        {
            Name = "MaintenanceReturn_frm",
            Caption = "سند صرف صيانه",
            SvgImage = Properties.Resources.business_presentation,
        };
        public static Element BillMaintenance_frm = new Element(Maintenance)
        {
            Name = "BillMaintenance_frm",
            Caption = "فاتوره صيانه",
            SvgImage = Properties.Resources.repeatallitemlabelspivottable,
        };




        #endregion

        #region ProfitsAndLosses
        public static Element ProfitsAndLosses = new Element()
        {
            Caption = "الارباح والخسائر",
        };
        public static Element frm_ProfitAndLoses = new Element(ProfitsAndLosses)
        {
            Name = "frm_ProfitAndLoses",
            Caption = "صافي الربح والخساره",
            SvgImage = Properties.Resources.currency,
        }; public static Element ProfitAndLosesAnalysis = new Element(ProfitsAndLosses)
        {
            Name = "frm_ProfitAndLosesAnalysis",
            Caption = "صافي الربح والخساره السنوي",
            SvgImage = Properties.Resources.currency,
        };
        public static Element frm_CustomerProfit = new Element(ProfitsAndLosses)
        {
            Name = "frm_CustomerProfit",
            Caption = "الربح من العملاء",
            SvgImage = Properties.Resources.bo_department,
        };
        public static Element frm_ProductsProfit = new Element(ProfitsAndLosses)
        {
            Name = "frm_ProductsProfit",
            Caption = "اكثر الاصناف ربحا",
            IsPanel = true,
            SvgImage = Properties.Resources.business_dollar,
        };
        
        //public static Element capital_frm = new Element(ProfitsAndLosses)
        //{
        //    Name = "capital_frm",
        //    Caption = "راس المال",
        //    SvgImage = Properties.Resources.financial,
        //};
        
        //public static Element RptIncomMany_frm = new Element(ProfitsAndLosses)
        //{
        //    Name = "RptIncomMany_frm",
        //    Caption = "الميزانيه",
        //    SvgImage = Properties.Resources.financial,
        //};
        #endregion

        #region Reports
        public static Element Reports = new Element()
        {
            Caption = "التقارير",
        };
        public static Element invoiceByCustomerChart = new Element(Reports)
        {
            Name = nameof(InvoiceByCustomerChart),
            Caption = "مخطط حركات العملاء السنوي",
        }; 
        public static Element invoiceByVendorChart = new Element(Reports)
        {
            Name = nameof(InvoiceByVendorChart),
            Caption = "مخطط حركات الموردين السنوي",
        }; 
        public static Element invoiceHistory = new Element(Reports)
        {
            Name = nameof(InvoiceHistory),
            Caption = "سجل الفواتير",
        }; 
        public static Element invoiceTraking = new Element(Reports)
        {
            Name = nameof(frm_SalesInvoiceTraking),
            Caption = "المبيعات خلال الساعه",
            SvgImage = Properties.Resources.viewmergeddata,
        }; 
        public static Element customerInvoiceDetails = new Element(Reports)
        {
            Name = nameof(CustomerInvoiceDetails),
            Caption = "تقرير مسحوبات عميل",
            SvgImage = Properties.Resources.viewmergeddata,
        }; 
        public static Element fullInvoiceByCustomer = new Element(Reports)
        {
            Name = nameof(FullInvoiceByCustomer),
            Caption = "كشف تفصيلي عميل",
            SvgImage = Properties.Resources.viewmergeddata,
        }; 
        public static Element fullInvoiceByVendor = new Element(Reports)
        {
            Name = nameof(FullInvoiceByVendor),
            Caption = "كشف تفصيلي مورد",
            SvgImage = Properties.Resources.viewmergeddata,
        }; 
        public static Element ReportStores = new Element(Reports)
        {
            Name = nameof(RPT.frm_reportStores),
            Caption = "تقرير المخازن",
            SvgImage = Properties.Resources.viewmergeddata,
            IsPanel = true,
        }; 
        public static Element ReportTotalGroupProductSells = new Element(Reports)
        {
            Name = nameof(RPT.frm_reportTotalGroupProductSells),
            Caption = "تقرير مبيعات الاصناف",
        };
        public static Element InertProduct = new Element(Reports)
        {
            Name = nameof(RPT.frm_InertProduct),
            Caption = "تقرير الاصناف الخاملة",
        };

     
        public static Element ProductCardWithQty = new Element(Reports)
        {
            Name = "ProductCardWithQty",
            Caption = "كارتة صنف",
            SvgImage = Properties.Resources.bo_note,
        };
        public static Element ProductBalance = new Element(Reports)
        {
            Name = "ProductBalance",
            Caption = "ارصده الاصناف",
            SvgImage = Properties.Resources.bo_note,
        };
        public static Element ProductMovment = new Element(Reports)
        {
            Name = "ProductMovment",
            Caption = "حركة صنف تفصيلي",
            SvgImage = Properties.Resources.bo_opportunity,
        };
        public static Element ProductExpire = new Element(Reports)
        {
            Name = "ProductExpire",
            Caption = "صلاحيه المنتجات",
            SvgImage = Properties.Resources.longdate,
        };
        public static Element ProductReachedReorderLevel = new Element(Reports)
        {
            Name = "ProductReachedReorderLevel",
            Caption = "النواقص",
            IsPanel = true,
            SvgImage = Properties.Resources.actions_removecircled,
        };
        public static Element ReportMaintenance_frm = new Element(Reports)
        {
            Name = "ReportMaintenance_frm",
            Caption = "تقرير الصيانه",
            SvgImage = Properties.Resources.tasks,
        };
        #region HrReports

        public static Element HrReports = new Element(Reports)
        {
            Name = "HrReports",
            Caption = "الموارد البشرية",

        };

        public static Element RPTEmployeeAttendance_frm = new Element(HrReports)
        {
            Name = "RPTEmployeeAttendance_frm",
            Caption = "حضور وانصراف",
            SvgImage = Properties.Resources.security_fingerprint,
        };
        public static Element RPTEmployee_Total_Attendance_frm = new Element(HrReports)
        {
            Name = "RPTEmployee_Total_Attendance_frm",
            Caption = "مجمع الحضور والانصراف",
            SvgImage = Properties.Resources.bo_scheduler,
        };
        public static Element RptEmployee_Salary_frm = new Element(HrReports)
        {
            Name = "RptEmployee_Salary_frm",
            Caption = "تقرير بمرتبات الموظفين",
            SvgImage = Properties.Resources.currency,
        };
        #endregion

        public static Element frm_PrintItemBarcode = new Element(Reports)
        {
            Name = "frm_PrintItemBarcode",
            Caption = "طباعه باركود",
            SvgImage = Properties.Resources.barcode,
        };
        #endregion
     
        #region Support
        public static Element Support = new Element()
        {
            Caption = "الدعم الفني",
        };
        public static Element Connection_Form = new Element(Support)
        {
            Name = "Connection_Form",
            Caption = "الدعم الفني والاستفسارات",
            SvgImage = Properties.Resources.support,
        };
        public static Element Info_Form = new Element(Support)
        {
            Name = "Info_Form",
            Caption = "تفعيل البرنامج",
            SvgImage = Properties.Resources.showallfieldcodes,
        };
        #endregion 
        
        #region Users
        public static Element Users = new Element()
        {
            Caption = "المستخدمين",
        };
        public static Element ChangePassword_frm = new Element(Users)
        {
            Name = "ChangePassword_frm",
            Caption = "تغير كلمة المرور",
            SvgImage = Properties.Resources.security_key,
        }; 
        public static Element LogOutUser = new Element(Users)
        {
            Name = "LogOutUser",
            Caption = "تسجيل خروج من حسابك",
            SvgImage = Properties.Resources.bo_department,
        }; 
        public static Element CloseApp = new Element(Users)
        {
            Name = "CloseApp",
            Caption = "خروج من البرنامج",
            SvgImage = Properties.Resources.close,
        }; 
        #endregion
        public static List<Element> GetScreens
        {
            get
            {
                Type t = typeof(Screens);
                FieldInfo[] fields = t.GetFields(BindingFlags.Public | BindingFlags.Static);
                var list = new List<Element>();
                foreach (var item in fields)
                {
                    var obj = item.GetValue(null);
                    if (obj != null && obj.GetType() == typeof(Element))
                        list.Add((Element)item.GetValue(null));
                }
                return list;
            }
        }

    }
}

