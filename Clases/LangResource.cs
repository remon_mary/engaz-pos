﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByStro
{
   public static  class LangResource
    {
        public static string StatmentType => "نوع البيان";
        public static string Statment => "البيان";
        public static string ErrorCantBeEmpry => "هذا الحقل مطلوب";
        public static string MaxCredit => "حد الائتمان";
        public static string Quantity => "الكمية";
        public static string Price => "السعر";
        public static string TaxValue => "قيمة الضريبة";
        public static string DiscountValue => "قيمة الخصم";
        public static string Code => "الكود";
        public static string BarCode => "باركود";
        public static string CashTransfer => "تحويل نقدي";
        public static string From => "من";
        public static string To => "الي";
        public static string Number => "رقم";
        public static string AskForSaving => "يوجد تغييرات لم يتم حفظها \n هل تريد الحفظ اولا";
        public static string ConfrmDelete => "هل تريد الحذف ؟ ";
        public static string CantDeleteDrawerHasTransactions => "لا يمكن حذف الخزنه يوجد حركات ماليه مرتبطه بها في النظام";
        public static string CantDeleteBankUsedByPayCards => "لا يمكن حذف البنك يوجد حركات ماليه مرتبطه بها في النظام";
        public static string All=> "الكل";
        public static string Account => "حساب";
        public static string Vendor=> "مورد";
        public static string Customer => "عميل";
        public static string CashNoteIn => "سند قبض نقدي";
        public static string CashNoteOut => "سند دفع نقدي";
        public static string ErrorValMustBeGreaterThan0 => "يجب ان تكون القيمه اكبر من الصفر ";
        public static string Discount => "خصم ";
        public static string CashPay => "دفع نقدي ";
        public static string BankTransfer => "تحويل بنكي ";
        public static string PayCards => "بطاقه دفع ";
        public static string OnAccount => "علي حساب ";
        public static string DocumentNotFound => "المستند غير موجود !";
        public static string Debit => "مدين";
        public static string Credit => "دائن";
        public static string ErorrThisNameIsUsedBefore => "هذا الاسم مستخدم مسبقاً";
        public static string Income => "وارد";
        public static string OutCome => "منصرف";
        public static string Amount => "المبلغ";
        public static string RevenuEntry => "تسجيل ايردات";
        public static string ExpenceEntry => "تسجيل مصروفات";
        public static string AddRevenueAccount => "اضافه حساب ايرادات";
        public static string AddExpenceAccount => "اضافه حساب مصروفات";
        public static string Finish => "انهاء";
        public static string ErrorMustInsertOneItemAtLeast => "يجب ادخال عنصر واحد علي الاقل";
        public static string Revenue => "ايرادات";
        public static string Expense => "مصروفات";
        public static string Notes => "الملاحظات";
        public static string FirstInFirstOut => "الوارد اولا يخرج اولا";
        public static string LastInFirstOut => "الوارد اخرا يخرج اولا";
        public static string WeightedAverage => "المتوسط المرجح";
        public static string CantDeleteStoreIsUsedInTheSystem => "لا يمكن حذف الفرع حيث انه مستخدم في النظام";
        public static string Sales => "المبيعات";
        public static string SalesReturn => "مردود المبيعات";
        public static string SalesDiscount => "خصم مسموح به";
        public static string Purchases => "المشتريات";
        public static string purchasesReturn => "حساب المشتريات";
        public static string PurchaseDiscount => "خصم نقدي مكتسب";
        public static string CloseInventory => "نهايه المده";
        public static string OpenInventory => "بدايه المده";
        public static string CostOfSoldGoodsAccount => "تكلفه البضاعه المباعه";
        public static string TheInventory => "المخزون";
        public static string CantDeletevendorIsUsedInTheSystem => "لا يمكن حذف المورد حيث يوجد عمليات مرتبطه به في النظام";
        public static string CantDeletevendorIsParentOfanotherVendor => "لا يمكن حذف المورد حيث انه مرتبط بمورد اخر ";
        public static string AnoymasCustomer => "عميل عام ";
        public static string AnoymasVendor => "مورد عام ";
        public static string MainDrawer => "الخزينه الرئيسيه ";


        public static string Assets => "الاصول";
        public static string FixedAssets => "الاصول الثابته";
        public static string CurrentAssets => "الاصول المتداوله";
        public static string Drawer => "الصناديق";
        public static string Banks => "البنوك";
        public static string Customers => "العملاء";
        public static string NotesReceivable => "اوراق القبض";
        public static string EmployeesDue => "ذمم الموظفين";
        public static string FinancialCustodies => "العهد الماليه";
        public static string PermanentCustodies => "العهد المستديمه";
        public static string TemporaryCustdodies => "العهد المؤقته";
        public static string OtherAssets => "اصول اخري";
        public static string Liabilites_Owners_Equity => "الخصوم وحقوق الملكيه";
        public static string Creditor => "الدائنون";
        public static string Vendors => "الموردين";
        public static string Loans => "اقتراضات نقديه";
        public static string Owner_Equity => "حقوق الملكيه";
        public static string Capital => "راس المال";
        public static string CurrentAccount => "Current Account";
        public static string DueSalerys => "رواتب مستحقة";
        public static string DepreciationComplex => "مجمع الاهلاك";
        public static string Expenses => "المصروفات";
        public static string EmployeeSalaries => "رواتب وأجور الموظفين";
        public static string SalesTax => "ضرائب المبيعات";
        public static string GeneralExpenses => "مصروفات عامه";
        public static string GeneralTaxes => "ضرائب عامه";
        public static string Revenues => "الايرادات";
        public static string GeneralRevenues => "ايرادات عامه";
        public static string Merchandising => "المتاجره";
        public static string NotesPayableAccount => "اوراق الدفع";
        public static string ManufacturingExpAccount => "مصروفات تصنيع";
        public static string SalesDeductTaxAccount => "خصم نقدي مسموح به";
        public static string PurchaseAddTaxAccount => "خصم نقدي مكتسب";
        public static string SalesAddTaxAccount => "ضريبه قيمه مضافه مبيعات";
        public static string PurchaseDeductTaxAccount => "ضريبه قيمه مضافه مشتريات";
        public static string DepreciationAccount => "مجمع الاهلاك";
        public static string RecieveNotesUnderCollectAccount => "اوراق تحت التحصيل";
        public static string ThisAccountDebitWith => "هذا الحساب مدين ب ";
        public static string ThisAccountCreditWith => "هذا الحساب دائن ب ";
        public static string Balance => "الرصيد";
        public static string CantDeleteNoPermission => "لا تمتلك صلاحيه الحذف";
        public static string CantDeleteAccountHasChilds => "لا يمكن حذف الحساب , يوجد حسابات فرعيه مرتبطه به";
        public static string CantDeleteAccountHasAcctiveties => "لا يمكن حذف الحساب يوجد قيود مرتبطه به";
        public static string CashNoteIns => "سندات القبض";
        public static string CashNoteOuts => "سندات الدفع";
        public static string CantDeletecustomerIsUsedInTheSystem => "لا يمكن حذف العميل ,  حسابه مستخدم في النظام";
        public static string CantDeletecustomerIsParentOfanotherCustomer => "لا يمكن حذف العميل , حسابه مرتبط بعميل اخر";
        public static string CantDeleteGroupHasItems => "لا يمكن حذف المجموعه حيث يوجد عناصر بها";
        public static string CantDeleteGroupHasChild => "لا يمكن حذف المجموعه حيث يوجد عناصر بها ";
        public static string ID => "رقم";
        public static string Name => "الاسم";
        public static string Phone => "الهاتف";
        public static string City => "المدينه";
        public static string Address => "العنوان";
        public static string ParentStoreID => "الفرع الرئيسي";
        public static string Group => "المجموعه";
        public static string TaxFileNumber => "رقم الملف الضريبي";
        public static string Employee => "موظف";
        public static string Pay => "دفع";
        public static string Commission => "عموله";
        public static string MarketingExpenses => "مصاريف تسويقية";
        public static string CommissionSalesman => "عموله المناديب";
        public static string Email => "البريد";
        public static string Mobile => "موبيل";
        public static string SalesInvoice => "فاتوره مبيعات";
        public static string PurchaseInvoice => "فاتوره مشتريات";
        public static string AddedTax => "ضريبه القيمه المضافه";
        public static string CostOfSoldGoods => "تكلفه البضاعه المباعه";








    }
}
