﻿using ByStro.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ByStro.Clases
{
    class AccountTemplate
    {

        static int _maxID = 0;
        static int _maxNumber = 0;
        static List<string> ListOfChiledNumbers = new List<string>();

        public static int MaxID
        {
            get
            {
                _maxID++;
                return _maxID;
            }
        }
        public static int MaxNumber
        {
            get
            {
                _maxNumber++;
                return _maxNumber;
            }
        }
        public static string GetNewNumber(int? parentID, string ParentNumber, int ParentLevel)
        {
            if (parentID == null)
                return MaxNumber.ToString();
            else
                return
                    GetNewAccountNumber(ParentNumber, ParentLevel);
        }


        public static string GetNewAccountNumber(string ParentNumber, int ParentLevel)
        {


            var ParentChilds = ListOfChiledNumbers.Where(x => x.StartsWith(ParentNumber) && x != ParentNumber);
            var num = "";
            if (ParentChilds.Count() > 0)
            {

                num = (Convert.ToInt64(ParentChilds.Max()) + 1).ToString();
            }
            else
            {
                num = ParentNumber;
                for (int i = 0; i < ParentLevel; i++)
                {
                    num += "0";
                }
                num += "1";


            }

            ListOfChiledNumbers.Add(num);

            return num;
        }
        public static List<Acc_Account> GetAllAccounts()
        {
            Type t = typeof(AccountTemplate.Accounts);
            FieldInfo[] fields = t.GetFields(BindingFlags.Static | BindingFlags.Public);
            var ls = new List<Acc_Account>();
            foreach (var item in fields)
            {
                ls.Add((Acc_Account)item.GetValue(null));
            }
            return ls;
        }

        public static class Accounts
        {
            public static Acc_Account AssetsAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 1,
                Number = GetNewNumber(null, "", 0),
                Name = LangResource.Assets,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = null
            };
            public static Acc_Account FixedAssetsAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 1,
                Number = GetNewNumber(AssetsAccount.ID, AssetsAccount.Number, AssetsAccount.Level),
                Name = LangResource.FixedAssets,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = AssetsAccount.ID
            };
            public static Acc_Account CurrentAssetsAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 2,
                Number = GetNewNumber(AssetsAccount.ID, AssetsAccount.Number, AssetsAccount.Level),
                Name = LangResource.CurrentAssets,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = AssetsAccount.ID
            };
            public static Acc_Account DrawerAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 3,
                Number = GetNewNumber(CurrentAssetsAccount.ID, CurrentAssetsAccount.Number, CurrentAssetsAccount.Level),
                Name = LangResource.Drawer,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = CurrentAssetsAccount.ID
            };
            public static Acc_Account BanksAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 3,
                Number = GetNewNumber(CurrentAssetsAccount.ID, CurrentAssetsAccount.Number, CurrentAssetsAccount.Level),
                Name = LangResource.Banks,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = CurrentAssetsAccount.ID
            };
            public static Acc_Account CustomersAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 3,
                Number = GetNewNumber(CurrentAssetsAccount.ID, CurrentAssetsAccount.Number, CurrentAssetsAccount.Level),
                Name = LangResource.Customers,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = CurrentAssetsAccount.ID
            };
            public static Acc_Account NotesReceivableAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 3,
                Number = GetNewNumber(CurrentAssetsAccount.ID, CurrentAssetsAccount.Number, CurrentAssetsAccount.Level),
                Name = LangResource.NotesReceivable,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = CurrentAssetsAccount.ID
            };
            public static Acc_Account InventoryAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 3,
                Number = GetNewNumber(CurrentAssetsAccount.ID, CurrentAssetsAccount.Number, CurrentAssetsAccount.Level),
                Name = LangResource.TheInventory,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = CurrentAssetsAccount.ID
            };
            public static Acc_Account EmployeesDueAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 3,
                Number = GetNewNumber(CurrentAssetsAccount.ID, CurrentAssetsAccount.Number, CurrentAssetsAccount.Level),
                Name = LangResource.EmployeesDue,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = CurrentAssetsAccount.ID
            };
            public static Acc_Account FinancialCustodiesAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 2,
                Number = GetNewNumber(CurrentAssetsAccount.ID, CurrentAssetsAccount.Number, CurrentAssetsAccount.Level),
                Name = LangResource.FinancialCustodies,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = CurrentAssetsAccount.ID
            };
            public static Acc_Account PermanentCustodiesAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 4,
                Number = GetNewNumber(FinancialCustodiesAccount.ID, FinancialCustodiesAccount.Number, FinancialCustodiesAccount.Level),
                Name = LangResource.PermanentCustodies,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = FinancialCustodiesAccount.ID
            };
            public static Acc_Account TemporaryCustdodiesAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 4,
                Number = GetNewNumber(FinancialCustodiesAccount.ID, FinancialCustodiesAccount.Number, FinancialCustodiesAccount.Level),
                Name = LangResource.TemporaryCustdodies,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = FinancialCustodiesAccount.ID
            };
            public static Acc_Account OtherAssetsAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 3,
                Number = GetNewNumber(AssetsAccount.ID, AssetsAccount.Number, AssetsAccount.Level),
                Name = LangResource.OtherAssets,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = AssetsAccount.ID
            };
            public static Acc_Account Liabilites_Owners_EquityAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 1,
                Number = GetNewNumber(null, "", 0),
                Name = LangResource.Liabilites_Owners_Equity,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = null
            };
            public static Acc_Account CreditorAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 2,
                Number = GetNewNumber(Liabilites_Owners_EquityAccount.ID, Liabilites_Owners_EquityAccount.Number, Liabilites_Owners_EquityAccount.Level),
                Name = LangResource.Creditor,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = Liabilites_Owners_EquityAccount.ID
            };
            public static Acc_Account VendorsAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 3,
                Number = GetNewNumber(CreditorAccount.ID, CreditorAccount.Number, CreditorAccount.Level),
                Name = LangResource.Vendors,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = CreditorAccount.ID
            };
            public static Acc_Account LoansAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 3,
                Number = GetNewNumber(CreditorAccount.ID, CreditorAccount.Number, CreditorAccount.Level),
                Name = LangResource.Loans,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = CreditorAccount.ID
            };
            public static Acc_Account Owner_EquityAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 2,
                Number = GetNewNumber(Liabilites_Owners_EquityAccount.ID, Liabilites_Owners_EquityAccount.Number, Liabilites_Owners_EquityAccount.Level),
                Name = LangResource.Owner_Equity,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = Liabilites_Owners_EquityAccount.ID
            };
            public static Acc_Account CapitalAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 2,
                Number = GetNewNumber(Owner_EquityAccount.ID, Owner_EquityAccount.Number, Owner_EquityAccount.Level),
                Name = LangResource.Capital,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = Owner_EquityAccount.ID
            };
            public static Acc_Account CurrentAccountAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 2,
                Number = GetNewNumber(Owner_EquityAccount.ID, Owner_EquityAccount.Number, Owner_EquityAccount.Level),
                Name = LangResource.CurrentAccount,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = Owner_EquityAccount.ID

            };
            public static Acc_Account DueSalerysAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 2,
                Number = GetNewNumber(Liabilites_Owners_EquityAccount.ID, Liabilites_Owners_EquityAccount.Number, Liabilites_Owners_EquityAccount.Level),
                Name = LangResource.DueSalerys,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = Liabilites_Owners_EquityAccount.ID
            };
            public static Acc_Account DepreciationComplexAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 2,
                Number = GetNewNumber(Liabilites_Owners_EquityAccount.ID, Liabilites_Owners_EquityAccount.Number, Liabilites_Owners_EquityAccount.Level),
                Name = LangResource.DepreciationComplex,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = Liabilites_Owners_EquityAccount.ID
            };
            public static Acc_Account ExpensesAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 2,
                Number = GetNewNumber(null, "", 0),
                Name = LangResource.Expenses,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = null
            };
            public static Acc_Account WagesAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 2,
                Number = GetNewNumber(ExpensesAccount.ID, ExpensesAccount.Number, ExpensesAccount.Level),
                Name = LangResource.EmployeeSalaries,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = ExpensesAccount.ID
            };     
            public static Acc_Account MarketingAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 2,
                Number = GetNewNumber(ExpensesAccount.ID, ExpensesAccount.Number, ExpensesAccount.Level),
                Name = LangResource.MarketingExpenses,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = ExpensesAccount.ID
            };   
            public static Acc_Account CommissionSalesmanAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 3,
                Number = GetNewNumber(MarketingAccount.ID, MarketingAccount.Number, MarketingAccount.Level),
                Name = LangResource.CommissionSalesman,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = MarketingAccount.ID
            };
            public static Acc_Account TaxAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 2,
                Number = GetNewNumber(ExpensesAccount.ID, ExpensesAccount.Number, ExpensesAccount.Level),
                Name = LangResource.SalesTax,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = ExpensesAccount.ID
            };
            public static Acc_Account GeneralExpensesAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 2,
                Number = GetNewNumber(ExpensesAccount.ID, ExpensesAccount.Number, ExpensesAccount.Level),
                Name = LangResource.GeneralExpenses,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = ExpensesAccount.ID
            };
            public static Acc_Account GeneralTaxesAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 2,
                Number = GetNewNumber(ExpensesAccount.ID, ExpensesAccount.Number, ExpensesAccount.Level),
                Name = LangResource.GeneralTaxes,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = ExpensesAccount.ID
            };
           
            public static Acc_Account RevenueAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 2,
                Number = GetNewNumber(null, "", 0),
                Name = LangResource.Revenues,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = null
            };
            public static Acc_Account GeneralRevenuesAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 2,
                Number = GetNewNumber(RevenueAccount.ID, RevenueAccount.Number, RevenueAccount.Level),
                Name = LangResource.GeneralRevenues,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = RevenueAccount.ID
            };
            public static Acc_Account MerchandisingAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 2,
                Number = GetNewNumber(null, "", 0),
                Name = LangResource.Merchandising,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = null
            };
            public static Acc_Account PurchasesAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 2,
                Number = GetNewNumber(MerchandisingAccount.ID, MerchandisingAccount.Number, MerchandisingAccount.Level),
                Name = LangResource.Purchases,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = MerchandisingAccount.ID
            };
            public static Acc_Account PurchasesReturnAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 2,
                Number = GetNewNumber(MerchandisingAccount.ID, MerchandisingAccount.Number, MerchandisingAccount.Level),
                Name = LangResource.purchasesReturn,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = MerchandisingAccount.ID
            };
            public static Acc_Account SalesAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 2,
                Number = GetNewNumber(MerchandisingAccount.ID, MerchandisingAccount.Number, MerchandisingAccount.Level),
                Name = LangResource.Sales,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = MerchandisingAccount.ID
            };
            public static Acc_Account SalesReturnAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 2,
                Number = GetNewNumber(MerchandisingAccount.ID, MerchandisingAccount.Number, MerchandisingAccount.Level),
                Name = LangResource.SalesReturn,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = MerchandisingAccount.ID
            };
            public static Acc_Account SalesDiscountAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 2,
                Number = GetNewNumber(MerchandisingAccount.ID, MerchandisingAccount.Number, MerchandisingAccount.Level),
                Name = LangResource.SalesDiscount,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = MerchandisingAccount.ID
            };
            public static Acc_Account PurchaseDiscountAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 2,
                Number = GetNewNumber(MerchandisingAccount.ID, MerchandisingAccount.Number, MerchandisingAccount.Level),
                Name = LangResource.PurchaseDiscount,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = MerchandisingAccount.ID
            };
            public static Acc_Account OpenInventoryAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 2,
                Number = GetNewNumber(MerchandisingAccount.ID, MerchandisingAccount.Number, MerchandisingAccount.Level),
                Name = LangResource.OpenInventory,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = MerchandisingAccount.ID
            };
            public static Acc_Account CloseInventoryAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 2,
                Number = GetNewNumber(MerchandisingAccount.ID, MerchandisingAccount.Number, MerchandisingAccount.Level),
                Name = LangResource.CloseInventory,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = MerchandisingAccount.ID
            };
            public static Acc_Account NotesPayableAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 2,
                Number = GetNewNumber(Liabilites_Owners_EquityAccount.ID, Liabilites_Owners_EquityAccount.Number, Liabilites_Owners_EquityAccount.Level),
                Name = LangResource.NotesPayableAccount,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = Liabilites_Owners_EquityAccount.ID
            };
            public static Acc_Account ManufacturingExpAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 2,
                Number = GetNewNumber(ExpensesAccount.ID, ExpensesAccount.Number, ExpensesAccount.Level),
                Name = LangResource.ManufacturingExpAccount,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = ExpensesAccount.ID
            };
            public static Acc_Account SalesDeductTaxAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 2,
                Number = GetNewNumber(ExpensesAccount.ID, ExpensesAccount.Number, ExpensesAccount.Level),
                Name = LangResource.SalesDeductTaxAccount,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = ExpensesAccount.ID
            };
            public static Acc_Account PurchaseAddTaxAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 2,
                Number = GetNewNumber(ExpensesAccount.ID, ExpensesAccount.Number, ExpensesAccount.Level),
                Name = LangResource.PurchaseAddTaxAccount,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = ExpensesAccount.ID
            };
            public static Acc_Account SalesAddTaxAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 2,
                Number = GetNewNumber(ExpensesAccount.ID, ExpensesAccount.Number, ExpensesAccount.Level),
                Name = LangResource.SalesAddTaxAccount,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = ExpensesAccount.ID
            };
            public static Acc_Account PurchaseDeductTaxAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 2,
                Number = GetNewNumber(ExpensesAccount.ID, ExpensesAccount.Number, ExpensesAccount.Level),
                Name = LangResource.PurchaseDeductTaxAccount,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = ExpensesAccount.ID
            };
            public static Acc_Account CostOfSoldGoodsAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 2,
                Number = GetNewNumber(MerchandisingAccount.ID, MerchandisingAccount.Number, MerchandisingAccount.Level),
                Name = LangResource.CostOfSoldGoodsAccount,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = MerchandisingAccount.ID
            };
            public static Acc_Account DepreciationAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 2,
                Number = GetNewNumber(Liabilites_Owners_EquityAccount.ID, Liabilites_Owners_EquityAccount.Number, Liabilites_Owners_EquityAccount.Level),
                Name = LangResource.DepreciationAccount,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = Liabilites_Owners_EquityAccount.ID
            };
            public static Acc_Account RecieveNotesUnderCollectAccount = new Acc_Account()
            {
                ID = MaxID,
                Level = 2,
                Number = GetNewNumber(CurrentAssetsAccount.ID, CurrentAssetsAccount.Number, CurrentAssetsAccount.Level),
                Name = LangResource.RecieveNotesUnderCollectAccount,
                CanEdit = false,
                Secrecy = 0,
                AccountType = 1,
                ParentID = CurrentAssetsAccount.ID
            };




        }
    }
}
