﻿using System;
using System.Data;
class ExpenseType_cls : DataAccessLayer
{
    //MaxID
    public String MaxID_ExpenseType()
    {
        return Execute_SQL("select ISNULL (MAX(ExpenseID)+1,1) from ExpenseType", CommandType.Text);
    }

    // Insert
    public void InsertExpenseType(string ExpenseID, String ExpenseName, string Remarks)
    {
        Execute_SQL("insert into ExpenseType(ExpenseID ,ExpenseName ,Remarks )Values (@ExpenseID ,@ExpenseName ,@Remarks )", CommandType.Text,
        Parameter("@ExpenseID", SqlDbType.Int, ExpenseID),
        Parameter("@ExpenseName", SqlDbType.NVarChar, ExpenseName),
        Parameter("@Remarks", SqlDbType.NText, Remarks));
    }



    //Update
    public void Update_ExpenseType(string ExpenseID, string ExpenseName, string Remarks)
    {
        Execute_SQL("Update ExpenseType Set ExpenseID=@ExpenseID ,ExpenseName=@ExpenseName ,Remarks=@Remarks  where ExpenseID=@ExpenseID", CommandType.Text,
          Parameter("@ExpenseID", SqlDbType.Int, ExpenseID),
        Parameter("@ExpenseName", SqlDbType.NVarChar, ExpenseName),
        Parameter("@Remarks", SqlDbType.NVarChar, Remarks));
    }

    //Delete
    public void Delete_ExpenseType(string ExpenseID)
    {
        Execute_SQL("Delete  From ExpenseType where ExpenseID=@ExpenseID", CommandType.Text,
         Parameter("@ExpenseID", SqlDbType.Int, ExpenseID));
    }
    //Details ID
    public DataTable NODelete_ExpenseType(String ExpenseTypeID)
    {
        return ExecteRader("Select ExpenseTypeID  from Expenses Where ExpenseTypeID=@ExpenseTypeID", CommandType.Text,
        Parameter("@ExpenseTypeID", SqlDbType.Int, ExpenseTypeID));
    }


    //Details ID
    public DataTable Details_ExpenseType(string ExpenseID)
    {
        return ExecteRader("select * From ExpenseType where ExpenseID=@ExpenseID", CommandType.Text,
        Parameter("@ExpenseID", SqlDbType.Int, ExpenseID));
    }

    //Details ID
    public DataTable NameSearch_ExpenseType(String ExpenseName)
    {
        return ExecteRader("select ExpenseName From ExpenseType where ExpenseName=@ExpenseName", CommandType.Text,
        Parameter("@ExpenseName", SqlDbType.NVarChar, ExpenseName));
    }

    public DataTable Search_ExpenseType_Delete(String ExpenseID)
    {
        return ExecteRader("Select ExpenseID  from Prodecuts where ExpenseID=@ExpenseID", CommandType.Text,
        Parameter("@ExpenseID", SqlDbType.NVarChar, ExpenseID));
    }
    
    //Search
    public DataTable Search_ExpenseType(String Search)
    {
        return ExecteRader("Select *  from ExpenseType where convert(nvarchar, ExpenseID) + ExpenseName like '%' + @Search + '%'", CommandType.Text,
        Parameter("@Search", SqlDbType.NVarChar, Search));
    }



}

