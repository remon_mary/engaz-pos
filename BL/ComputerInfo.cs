﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Security.Cryptography;
using System.Text;

namespace ByStro.BL
{
    public static class ComputerInfo
    {
     public static string GetComputerId()
    {
        return ComputerInfo.GetHash("ByStro>>" + "CPU>>" + ComputerInfo.CpuId() + GetMotherBoardID());
    }

    private static string GetHash(string s)
    {
      return ComputerInfo.GetHexString(new MD5CryptoServiceProvider().ComputeHash(new ASCIIEncoding().GetBytes(s)));
    }

    private static string GetHexString(byte[] bt)
    {
      string str1 = string.Empty;
      for (int index = 0; index < bt.Length; ++index)
      {
        int num1 = (int) bt[index];
        int num2 = 15;
        int num3 = num1 & num2;
        int num4 = 4;
        int num5 = num1 >> num4 & 15;
        string str2 = num5 <= 9 ? str1 + num5.ToString() : str1 + ((char) (num5 - 10 + 65)).ToString();
        str1 = num3 <= 9 ? str2 + num3.ToString() : str2 + ((char) (num3 - 10 + 65)).ToString();
        if (index + 1 != bt.Length && (index + 1) % 2 == 0)
          str1 += "";
      }
      return str1;
    }

    private static string identifier(string wmiClass, string wmiProperty, string wmiMustBeTrue)
    {
      string str = "";
      foreach (ManagementObject instance in new ManagementClass(wmiClass).GetInstances())
      {
        if (instance[wmiMustBeTrue].ToString() == "True")
        {
          if (str == "")
          {
            try
            {
              str = instance[wmiProperty].ToString();
              break;
            }
            catch
            {
            }
          }
        }
      }
      return str;
    }

    private static string identifier(string wmiClass, string wmiProperty)
    {
      string str = "";
      foreach (ManagementObject instance in new ManagementClass(wmiClass).GetInstances())
      {
        if (str == "")
        {
          try
          {
            str = instance[wmiProperty].ToString();
            break;
          }
          catch
          {
          }
        }
      }
      return str;
    }

    private static string CpuId()
    {
            return ComputerInfo.identifier("Win32_Processor", "ProcessorId");// + ComputerInfo.identifier("Win32_Processor", "Name");

      //string str1 = ComputerInfo.identifier("Win32_Processor", "UniqueId");
      //if (str1 == "")
      //{
      // str1 = ComputerInfo.identifier("Win32_Processor", "ProcessorId");
      //  if (str1 == "")
      //  {
      //    string str2 = ComputerInfo.identifier("Win32_Processor", "Name");
      //    if (str2 == "")
      //      str2 = ComputerInfo.identifier("Win32_Processor", "Manufacturer");
      //    str1 = str2 + ComputerInfo.identifier("Win32_Processor", "MaxClockSpeed");
      //  }
      //}
     // return str1;
    }

    private static string BiosId()
    {
      return ComputerInfo.identifier("Win32_BIOS", "Manufacturer") + ComputerInfo.identifier("Win32_BIOS", "SMBIOSBIOSVersion") + ComputerInfo.identifier("Win32_BIOS", "IdentificationCode") + ComputerInfo.identifier("Win32_BIOS", "SerialNumber") + ComputerInfo.identifier("Win32_BIOS", "ReleaseDate") + ComputerInfo.identifier("Win32_BIOS", "Version");
    }

    private static string DiskId()
    {
        return ComputerInfo.identifier("win32_PhysicalMedia", "SerialNumber");
 
 //  return ComputerInfo.identifier("Win32_DiskDrive", "Model") + ComputerInfo.identifier("Win32_DiskDrive", "Manufacturer") + ComputerInfo.identifier("Win32_DiskDrive", "Signature") + ComputerInfo.identifier("Win32_DiskDrive", "TotalHeads");
    }

    private static string BaseId()
    {
      return ComputerInfo.identifier("Win32_BaseBoard", "Model") + ComputerInfo.identifier("Win32_BaseBoard", "Manufacturer") + ComputerInfo.identifier("Win32_BaseBoard", "Name") + ComputerInfo.identifier("Win32_BaseBoard", "SerialNumber");
    }

    private static string VideoId()
    {
      return ComputerInfo.identifier("Win32_VideoController", "DriverVersion") + ComputerInfo.identifier("Win32_VideoController", "Name");
    }

    private static string MacId()
    {
      return ComputerInfo.identifier("Win32_NetworkAdapterConfiguration", "MACAddress", "IPEnabled");
    }


        public static String GetMotherBoardID()
        {
            String serial = "";
            try
            {
                ManagementObjectSearcher mos = new ManagementObjectSearcher("SELECT SerialNumber FROM Win32_BaseBoard");
                ManagementObjectCollection moc = mos.Get();

                foreach (ManagementObject mo in moc)
                {
                    serial = mo["SerialNumber"].ToString();
                }
                return serial;
            }
            catch (Exception)
            {
                return serial;
            }
        }






    }
}

