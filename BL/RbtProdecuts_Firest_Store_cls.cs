﻿
using System.Data;

class RbtProdecuts_Firest_Store_cls : DataAccessLayer
    {
    //Details ID
    public DataTable Search_Firist_Details(string StoreID)
    {
        return ExecteRader(@"SELECT        dbo.Firist_Details.ID, dbo.Firist_Details.MainID, dbo.Firist_Main.MyDate, dbo.Firist_Details.ProdecutID, dbo.Prodecuts.ProdecutName, dbo.Firist_Details.Unit, dbo.Firist_Details.UnitFactor, dbo.Firist_Details.UnitOperating, 
                         dbo.Firist_Details.Quantity, dbo.Firist_Details.Price, dbo.Firist_Details.TotalPrice, dbo.Firist_Details.StoreID, dbo.Stores.StoreName, dbo.Firist_Main.UserAdd, dbo.UserPermissions.EmpName
FROM            dbo.Firist_Main INNER JOIN
                         dbo.Firist_Details ON dbo.Firist_Main.MainID = dbo.Firist_Details.MainID INNER JOIN
                         dbo.Prodecuts ON dbo.Firist_Details.ProdecutID = dbo.Prodecuts.ProdecutID INNER JOIN
                         dbo.Stores ON dbo.Firist_Details.StoreID = dbo.Stores.StoreID INNER JOIN
                         dbo.UserPermissions ON dbo.Firist_Main.UserAdd = dbo.UserPermissions.ID where dbo.Firist_Details.StoreID=@StoreID", CommandType.Text,
        Parameter("@StoreID", SqlDbType.NVarChar, StoreID));
    }

    //Details ID
    public DataTable Search_Firist_Details()
    {
        return ExecteRader(@"SELECT        dbo.Firist_Details.ID, dbo.Firist_Details.MainID, dbo.Firist_Main.MyDate, dbo.Firist_Details.ProdecutID, dbo.Prodecuts.ProdecutName, dbo.Firist_Details.Unit, dbo.Firist_Details.UnitFactor, dbo.Firist_Details.UnitOperating, 
                         dbo.Firist_Details.Quantity, dbo.Firist_Details.Price, dbo.Firist_Details.TotalPrice, dbo.Firist_Details.StoreID, dbo.Stores.StoreName, dbo.Firist_Main.UserAdd, dbo.UserPermissions.EmpName
FROM            dbo.Firist_Main INNER JOIN
                         dbo.Firist_Details ON dbo.Firist_Main.MainID = dbo.Firist_Details.MainID INNER JOIN
                         dbo.Prodecuts ON dbo.Firist_Details.ProdecutID = dbo.Prodecuts.ProdecutID INNER JOIN
                         dbo.Stores ON dbo.Firist_Details.StoreID = dbo.Stores.StoreID INNER JOIN
                         dbo.UserPermissions ON dbo.Firist_Main.UserAdd = dbo.UserPermissions.ID", CommandType.Text);
    }
}

