﻿using System;
using System.Data;

class Bay_cls : DataAccessLayer
{

    //MaxID
    public String MaxID_Bay_Main()
    {
        return Execute_SQL("select ISNULL (MAX(MainID)+1,1) from Bay_Main", CommandType.Text);
    }

    // Insert
    public void InsertBay_Main(string MainID, DateTime MyDate, String InvoiceNumber, string SupplierID, String Remarks, String TypeKind, Double TotalInvoice, Double Discount, Double NetInvoice, Double PaidInvoice, Double RestInvoise, string ReturnInvoise, string TransID, string TreasuryID, double Vat, string Expenses, int UserAdd)
    {
        Execute_SQL("insert into Bay_Main(MainID ,MyDate ,InvoiceNumber ,SupplierID ,Remarks ,TypeKind ,TotalInvoice ,Discount ,NetInvoice ,PaidInvoice ,RestInvoise,ReturnInvoise,PaidReturnInvoice,TransID,TreasuryID,Vat,Expenses,UserAdd )Values (@MainID,@MyDate ,@InvoiceNumber ,@SupplierID ,@Remarks ,@TypeKind ,@TotalInvoice ,@Discount ,@NetInvoice ,@PaidInvoice ,@RestInvoise,@ReturnInvoise,@PaidReturnInvoice,@TransID,@TreasuryID,@Vat,@Expenses,@UserAdd )", CommandType.Text,
         Parameter("@MainID", SqlDbType.NVarChar, MainID),
        Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@InvoiceNumber", SqlDbType.NVarChar, InvoiceNumber),
        Parameter("@SupplierID", SqlDbType.BigInt, SupplierID),
        Parameter("@Remarks", SqlDbType.NVarChar, Remarks),
        Parameter("@TypeKind", SqlDbType.NVarChar, TypeKind),
        Parameter("@TotalInvoice", SqlDbType.Float, TotalInvoice),
        Parameter("@Discount", SqlDbType.Float, Discount),
        Parameter("@NetInvoice", SqlDbType.Float, NetInvoice),
        Parameter("@PaidInvoice", SqlDbType.Float, PaidInvoice),
        Parameter("@RestInvoise", SqlDbType.Float, RestInvoise),
        Parameter("@ReturnInvoise", SqlDbType.Float, ReturnInvoise),
          Parameter("@PaidReturnInvoice", SqlDbType.Float, 0),
        Parameter("@TransID", SqlDbType.NVarChar, TransID),
        Parameter("@TreasuryID", SqlDbType.NVarChar, TreasuryID),
        Parameter("@Vat", SqlDbType.Float, Vat),
        Parameter("@Expenses", SqlDbType.Float, Expenses),
        Parameter("@UserAdd", SqlDbType.Int, UserAdd));
    }



    //Update
    public void UpdateBay_Main(string MainID, DateTime MyDate, String InvoiceNumber, string SupplierID, String Remarks, String TypeKind, Double TotalInvoice, Double Discount, Double NetInvoice, Double PaidInvoice, Double RestInvoise, string TransID, string TreasuryID,double Vat, double VatValue, string Expenses)
    {
        Execute_SQL("Update Bay_Main Set MyDate=@MyDate ,InvoiceNumber=@InvoiceNumber ,SupplierID=@SupplierID ,Remarks=@Remarks ,TypeKind=@TypeKind ,TotalInvoice=@TotalInvoice ,Discount=@Discount ,NetInvoice=@NetInvoice ,PaidInvoice=@PaidInvoice ,RestInvoise=@RestInvoise,TransID=@TransID,TreasuryID=@TreasuryID,Vat=@Vat,VatValue=@VatValue, Expenses=@Expenses  where MainID=@MainID", CommandType.Text,
        Parameter("@MainID", SqlDbType.BigInt, MainID),
        Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@InvoiceNumber", SqlDbType.NVarChar, InvoiceNumber),
        Parameter("@SupplierID", SqlDbType.BigInt, SupplierID),
        Parameter("@Remarks", SqlDbType.NVarChar, Remarks),
        Parameter("@TypeKind", SqlDbType.NVarChar, TypeKind),
        Parameter("@TotalInvoice", SqlDbType.Float, TotalInvoice),
        Parameter("@Discount", SqlDbType.Float, Discount),
        Parameter("@NetInvoice", SqlDbType.Float, NetInvoice),
        Parameter("@PaidInvoice", SqlDbType.Float, PaidInvoice),
        Parameter("@RestInvoise", SqlDbType.Float, RestInvoise),
        Parameter("@TransID", SqlDbType.NVarChar, TransID),
        Parameter("@TreasuryID", SqlDbType.NVarChar, TreasuryID),
                Parameter("@Vat", SqlDbType.Float, Vat),
        Parameter("@VatValue", SqlDbType.Float, VatValue),
           Parameter("@Expenses", SqlDbType.Float, Expenses)
        );
    }


    //Update Return
    public void UpdateBay_Main(string MainID, string ReturnInvoise,string PaidReturnInvoice)
    {
        Execute_SQL("Update Bay_Main Set ReturnInvoise=@ReturnInvoise ,PaidReturnInvoice=@PaidReturnInvoice  where MainID=@MainID", CommandType.Text,
        Parameter("@MainID", SqlDbType.BigInt, MainID),
        Parameter("@ReturnInvoise", SqlDbType.Float, ReturnInvoise),
       Parameter("@PaidReturnInvoice", SqlDbType.Float, PaidReturnInvoice) );
    }





    //Delete
    public void Delete_BillStoreMain(string MainID)
    {
        Execute_SQL("delete from Bay_Main where MainID=@MainID", CommandType.Text,
        Parameter("@MainID", SqlDbType.NVarChar, MainID));
    }

    //Details ID
    public DataTable Details_Bay_Main(string MainID)
    {
        return ExecteRader("SELECT  dbo.Bay_Main.*, dbo.Suppliers.SupplierName FROM  dbo.Bay_Main INNER JOIN  dbo.Suppliers ON dbo.Bay_Main.SupplierID = dbo.Suppliers.SupplierID where dbo.Bay_Main.MainID=@MainID ", CommandType.Text,
        Parameter("@MainID", SqlDbType.NVarChar, MainID));
    }


    public DataTable Details_Bay_MainChangeQuentity(string MainID)
    {
        return ExecteRader("SELECT  * from Bay_Main  where MainID=@MainID", CommandType.Text,
        Parameter("@MainID", SqlDbType.NVarChar, MainID));
    }

    //=================================================================================================================================


    //MaxID
    private String MaxID_Bay_Details()
    {
        return Execute_SQL("select ISNULL (MAX(ID)+1,1) from Bay_Details", CommandType.Text);
    }


    // Insert
    //public void InsertBay_Details(string MainID, string ProdecutID, string Unit, string UnitFactor, string UnitOperating, string Quantity, string ReturnQuantity, string Price, string Vat, string MainVat, string TotalPrice, string StoreID, Boolean Recived)
    //{
    //    Execute_SQL("insert into Bay_Details(ID ,MainID ,ProdecutID ,Unit ,UnitFactor ,UnitOperating ,Quantity,ReturnQuantity,Price ,Vat,MainVat,ReturnVat ,TotalPrice,ReturnTotalPrice,StoreID,Recived )Values (@ID ,@MainID ,@ProdecutID ,@Unit ,@UnitFactor ,@UnitOperating ,@Quantity,@ReturnQuantity,@Price ,@Vat,@MainVat,@ReturnVat ,@TotalPrice,@ReturnTotalPrice,@StoreID,@Recived)", CommandType.Text,
    //    Parameter("@ID", SqlDbType.BigInt, MaxID_Bay_Details()),
    //    Parameter("@MainID", SqlDbType.BigInt, MainID),
    //    Parameter("@ProdecutID", SqlDbType.Int, ProdecutID),
    //    Parameter("@Unit", SqlDbType.NVarChar, Unit),
    //    Parameter("@UnitFactor", SqlDbType.Float, UnitFactor),
    //    Parameter("@UnitOperating", SqlDbType.Float, UnitOperating),
    //    Parameter("@Quantity", SqlDbType.Float, Quantity),
    //    Parameter("@ReturnQuantity", SqlDbType.Float, ReturnQuantity),
    //    Parameter("@Price", SqlDbType.Float, Price),
    //    Parameter("@Vat", SqlDbType.Float, Vat),
    //    Parameter("@MainVat", SqlDbType.Float, MainVat),
    //    Parameter("@ReturnVat", SqlDbType.Float, 0),
    //    Parameter("@TotalPrice", SqlDbType.Float, TotalPrice),
    //    Parameter("@ReturnTotalPrice", SqlDbType.Float, 0),
    //    Parameter("@StoreID", SqlDbType.Int, StoreID),
    //    Parameter("@Recived", SqlDbType.Bit, Recived));
    //}

    // Insert
    public void InsertBay_Details(string MainID, string ProdecutID, String Unit, string UnitFactor, string UnitOperating, string Quantity, string ReturnQuantity, string Price, string Vat, string MainVat, string TotalPrice, string StoreID, Boolean Recived, string CurrencyID, string CurrencyRate, string CurrencyPrice,String CurrencyVat, string CurrencyTotal)
    {
        Execute_SQL("insert into Bay_Details(ID ,MainID ,ProdecutID ,Unit ,UnitFactor ,UnitOperating ,Quantity ,ReturnQuantity ,Price ,Vat ,MainVat ,ReturnVat ,TotalPrice ,ReturnTotalPrice,ReturnCurrencyVat,ReturnCurrencyTotal ,StoreID ,Recived ,CurrencyID ,CurrencyRate ,CurrencyPrice,CurrencyVat ,CurrencyTotal )Values (@ID ,@MainID ,@ProdecutID ,@Unit ,@UnitFactor ,@UnitOperating ,@Quantity ,@ReturnQuantity ,@Price ,@Vat ,@MainVat ,@ReturnVat ,@TotalPrice ,@ReturnTotalPrice,@ReturnCurrencyVat,@ReturnCurrencyTotal ,@StoreID ,@Recived ,@CurrencyID ,@CurrencyRate ,@CurrencyPrice ,@CurrencyVat ,@CurrencyTotal )", CommandType.Text,
        Parameter("@ID", SqlDbType.BigInt, MaxID_Bay_Details()),
        Parameter("@MainID", SqlDbType.BigInt, MainID),
        Parameter("@ProdecutID", SqlDbType.Int, ProdecutID),
        Parameter("@Unit", SqlDbType.NVarChar, Unit),
        Parameter("@UnitFactor", SqlDbType.Float, UnitFactor),
        Parameter("@UnitOperating", SqlDbType.Float, UnitOperating),
        Parameter("@Quantity", SqlDbType.Float, Quantity),
        Parameter("@ReturnQuantity", SqlDbType.Float, ReturnQuantity),
        Parameter("@Price", SqlDbType.Float, Price),
        Parameter("@Vat", SqlDbType.Float, Vat),
        Parameter("@MainVat", SqlDbType.Float, MainVat),
        Parameter("@ReturnVat", SqlDbType.Float, 0),
        Parameter("@TotalPrice", SqlDbType.Float, TotalPrice),
        Parameter("@ReturnTotalPrice", SqlDbType.Float, 0),
               Parameter("@ReturnCurrencyVat", SqlDbType.NVarChar, 0),
          Parameter("@ReturnCurrencyTotal", SqlDbType.NVarChar, 0),

        Parameter("@StoreID", SqlDbType.Int, StoreID),
        Parameter("@Recived", SqlDbType.Bit, Recived),
        Parameter("@CurrencyID", SqlDbType.Int, CurrencyID),
        Parameter("@CurrencyRate", SqlDbType.Float, CurrencyRate),
        Parameter("@CurrencyPrice", SqlDbType.Float, CurrencyPrice),
          Parameter("@CurrencyVat", SqlDbType.Float, CurrencyVat),
        Parameter("@CurrencyTotal", SqlDbType.Float, CurrencyTotal));
    }




    //Update
    public void UpdateBay_Details(string ID, Double ReturnQuantity, Double ReturnVat, Double ReturnTotalPrice,string ReturnCurrencyVat,string ReturnCurrencyTotal)
    {
        Execute_SQL("Update Bay_Details Set ReturnQuantity=@ReturnQuantity,ReturnVat=@ReturnVat ,ReturnTotalPrice=@ReturnTotalPrice,ReturnCurrencyVat=@ReturnCurrencyVat,ReturnCurrencyTotal=@ReturnCurrencyTotal  where ID=@ID", CommandType.Text,
        Parameter("@ID", SqlDbType.NVarChar, ID),
        Parameter("@ReturnQuantity", SqlDbType.Float, ReturnQuantity),
         Parameter("@ReturnVat", SqlDbType.Float, ReturnVat),
        Parameter("@ReturnTotalPrice", SqlDbType.Float, ReturnTotalPrice),
           Parameter("@ReturnCurrencyVat", SqlDbType.Float, ReturnCurrencyVat),
              Parameter("@ReturnCurrencyTotal", SqlDbType.Float, ReturnCurrencyTotal));
    }



    //Delete
    public void Delete_Bay_Details(string MainID)
    {
        Execute_SQL("delete from Bay_Details where MainID=@MainID", CommandType.Text,
        Parameter("@MainID", SqlDbType.NVarChar, MainID));
    }
    //Delete Table  Bill Details where ID 
    public void DeleteBay_Details_UseID(string ID)
    {
        Execute_SQL("Delete  From Bay_Details where ID=@ID", CommandType.Text,
        Parameter("@ID", SqlDbType.NVarChar, ID));
    }


    //Details ID

    public DataTable Details_Bay_Details(String MainID)
    {

        string str = @"SELECT        dbo.Bay_Details.*, dbo.Prodecuts.ProdecutName, dbo.Stores.StoreName, dbo.Currency.CurrencyName
FROM            dbo.Bay_Details INNER JOIN
                         dbo.Prodecuts ON dbo.Bay_Details.ProdecutID = dbo.Prodecuts.ProdecutID INNER JOIN
                         dbo.Stores ON dbo.Bay_Details.StoreID = dbo.Stores.StoreID INNER JOIN
                         dbo.Currency ON dbo.Bay_Details.CurrencyID = dbo.Currency.CurrencyID
where  dbo.Bay_Details.MainID=@MainID";
        return ExecteRader(str, CommandType.Text,
            Parameter("@MainID", SqlDbType.NVarChar, MainID));
    }

    //Details ID

    public DataTable Details_Bay_Details(Boolean Recived)
    {

        string str = @"SELECT        dbo.Bay_Details.ID, dbo.Bay_Details.MainID, dbo.Bay_Details.ProdecutID, dbo.Bay_Details.Unit, dbo.Bay_Details.UnitFactor, dbo.Bay_Details.UnitOperating, dbo.Bay_Details.Quantity, dbo.Bay_Details.ReturnQuantity, 
                         dbo.Bay_Details.Price, dbo.Bay_Details.Vat, dbo.Bay_Details.TotalPrice, dbo.Prodecuts.ProdecutName, dbo.Stores.StoreName, dbo.Bay_Details.BayPrice, dbo.Bay_Details.ReturnTotalPrice, dbo.Bay_Details.SumType, 
                         dbo.Bay_Details.StoreID
FROM            dbo.Prodecuts INNER JOIN
                         dbo.Bay_Details ON dbo.Prodecuts.ProdecutID = dbo.Bay_Details.ProdecutID INNER JOIN
                         dbo.Stores ON dbo.Bay_Details.StoreID = dbo.Stores.StoreID where  dbo.Bay_Details.Recived=@Recived";
        return ExecteRader(str, CommandType.Text,
            Parameter("@Recived", SqlDbType.Bit, Recived));
    }

    public DataTable Details_Bay_Details(String MainID, Boolean SumType)
    {

        string str = @"SELECT        dbo.Bay_Details.ID, dbo.Bay_Details.MainID, dbo.Bay_Details.ProdecutID, dbo.Bay_Details.Unit, dbo.Bay_Details.UnitFactor, dbo.Bay_Details.UnitOperating, dbo.Bay_Details.Quantity, dbo.Bay_Details.ReturnQuantity, 
                         dbo.Bay_Details.Price, dbo.Bay_Details.Vat, dbo.Bay_Details.TotalPrice, dbo.Prodecuts.ProdecutName, dbo.Stores.StoreName, dbo.Bay_Details.BayPrice, dbo.Bay_Details.ReturnTotalPrice, dbo.Bay_Details.SumType, 
                         dbo.Bay_Details.StoreID
FROM            dbo.Prodecuts INNER JOIN
                         dbo.Bay_Details ON dbo.Prodecuts.ProdecutID = dbo.Bay_Details.ProdecutID INNER JOIN
                         dbo.Stores ON dbo.Bay_Details.StoreID = dbo.Stores.StoreID where  dbo.Bay_Details.MainID=@MainID and dbo.Bay_Details.SumType=@SumType";
        return ExecteRader(str, CommandType.Text,
            Parameter("@MainID", SqlDbType.NVarChar, MainID),
             Parameter("@SumType", SqlDbType.NVarChar, SumType));
    }



    //=====================================================================================
    // Update 
    public void UpdateBay_Details(string ID, string Quantity, string Price, string Vat, string MainVat, string TotalPrice)
    {
        Execute_SQL("Update Bay_Details Set ID=@ID ,Quantity=@Quantity,Price=@Price ,Vat=@Vat,MainVat=@MainVat ,TotalPrice=@TotalPrice  where ID=@ID", CommandType.Text,
        Parameter("@ID", SqlDbType.BigInt, ID),
        Parameter("@Quantity", SqlDbType.Float, Quantity),
        Parameter("@Price", SqlDbType.Float, Price),
        Parameter("@Vat", SqlDbType.Float, Vat),
         Parameter("@MainVat", SqlDbType.Float, MainVat),
        Parameter("@TotalPrice", SqlDbType.Float, TotalPrice));
    }


    //    //==================== Return=========================================================================================================================
    public DataTable LoadBillSalesWhereCustomerID(string MainID)
    {
        string sql = @"SELECT   dbo.Bay_Main.MyDate, dbo.Bay_Main.TypeKind, dbo.Bay_Main.SupplierID, dbo.Customers.CustomerName, dbo.Customers.Phone, dbo.Bay_Details.ProdecutID, dbo.Prodecuts.ProdecutName, dbo.Bay_Details.Unit, 
                         dbo.Bay_Details.Quantity, dbo.Bay_Details.ReturnQuantity, dbo.Bay_Details.Price, dbo.Bay_Details.Vat, dbo.Bay_Details.TotalPrice, dbo.Stores.StoreName, dbo.Bay_Main.MainID, dbo.Bay_Details.ID, 
                         dbo.Bay_Details.ReturnTotalPrice, dbo.Bay_Details.UnitFactor, dbo.Bay_Details.UnitOperating, dbo.Bay_Details.StoreID, dbo.Bay_Details.Recived
FROM            dbo.Bay_Details INNER JOIN
                         dbo.Bay_Main ON dbo.Bay_Details.MainID = dbo.Bay_Main.MainID INNER JOIN
                         dbo.Customers ON dbo.Bay_Main.SupplierID = dbo.Customers.CustomerID INNER JOIN
                         dbo.Prodecuts ON dbo.Bay_Details.ProdecutID = dbo.Prodecuts.ProdecutID INNER JOIN
                         dbo.Stores ON dbo.Bay_Details.StoreID = dbo.Stores.StoreID
WHERE (dbo.Bay_Main.MainID =@MainID)";
        return ExecteRader(sql, CommandType.Text,
           Parameter("@MainID", SqlDbType.NVarChar, MainID));
    }


    public DataTable BillSalesCustomer(string SupplierID)
    {
        string sql = @"SELECT  MainID, SupplierID FROM  Bay_Main WHERE SupplierID =@SupplierID";
        return ExecteRader(sql, CommandType.Text,
           Parameter("@SupplierID", SqlDbType.NVarChar, SupplierID));
    }



    public DataTable BillChangeQuantity()
    {
        string sql = "SELECT  MainID FROM  Bay_Main ";
        return ExecteRader(sql, CommandType.Text);
    }



    //===Search==============================================================================================

    public DataTable Load_Bay_Main ( DateTime MyDate, DateTime MyDate2, int UserAdd)
    {
         string str = @"SELECT        dbo.Bay_Main.MyDate, dbo.Bay_Main.InvoiceNumber, dbo.Bay_Main.SupplierID, dbo.Suppliers.SupplierName, dbo.Bay_Main.Remarks, dbo.Bay_Main.TypeKind, dbo.Bay_Main.TotalInvoice, dbo.Bay_Main.Discount, 
                         dbo.Bay_Main.NetInvoice, dbo.Bay_Main.PaidInvoice, dbo.Bay_Main.RestInvoise, dbo.Bay_Main.UserAdd, dbo.UserPermissions.EmpName, dbo.Bay_Main.ReturnInvoise, dbo.Bay_Main.TransID, dbo.Bay_Main.TreasuryID, 
                         dbo.Bay_Main.Vat, dbo.Bay_Main.VatValue, dbo.Bay_Main.MainID
FROM            dbo.Bay_Main INNER JOIN
                         dbo.Suppliers ON dbo.Bay_Main.SupplierID = dbo.Suppliers.SupplierID INNER JOIN
                         dbo.UserPermissions ON dbo.Bay_Main.UserAdd = dbo.UserPermissions.ID
WHERE      (dbo.Bay_Main.MyDate >=@MyDate ) AND (dbo.Bay_Main.MyDate <=@MyDate2) AND (dbo.Bay_Main.UserAdd =@UserAdd)";
         return ExecteRader(str, CommandType.Text,
         Parameter("@MyDate", SqlDbType.Date, MyDate),
         Parameter("@MyDate2", SqlDbType.Date, MyDate2),
         Parameter("@UserAdd", SqlDbType.Int, UserAdd));

    }

    public DataTable Search_Bay_Main(DateTime MyDate, DateTime MyDate2, int UserAdd,string Search)
    {
        string str = @"SELECT        dbo.Bay_Main.MyDate, dbo.Bay_Main.InvoiceNumber, dbo.Bay_Main.SupplierID, dbo.Suppliers.SupplierName, dbo.Bay_Main.Remarks, dbo.Bay_Main.TypeKind, dbo.Bay_Main.TotalInvoice, dbo.Bay_Main.Discount, 
                         dbo.Bay_Main.NetInvoice, dbo.Bay_Main.PaidInvoice, dbo.Bay_Main.RestInvoise, dbo.Bay_Main.UserAdd, dbo.UserPermissions.EmpName, dbo.Bay_Main.ReturnInvoise, dbo.Bay_Main.TransID, dbo.Bay_Main.TreasuryID, 
                         dbo.Bay_Main.Vat, dbo.Bay_Main.VatValue, dbo.Bay_Main.MainID
FROM            dbo.Bay_Main INNER JOIN
                         dbo.Suppliers ON dbo.Bay_Main.SupplierID = dbo.Suppliers.SupplierID INNER JOIN
                         dbo.UserPermissions ON dbo.Bay_Main.UserAdd = dbo.UserPermissions.ID
WHERE (dbo.Bay_Main.MyDate >=@MyDate ) AND (dbo.Bay_Main.MyDate <=@MyDate2) AND (dbo.Bay_Main.UserAdd =@UserAdd) and convert(nvarchar,dbo.Bay_Main.MainID)+dbo.Suppliers.SupplierName like '%'+@Search+ '%'";
        return ExecteRader(str, CommandType.Text,
            Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@MyDate2", SqlDbType.Date, MyDate2),
        Parameter("@UserAdd", SqlDbType.Int, UserAdd),
        Parameter("@Search", SqlDbType.NVarChar, Search));

    }



    //Details ID
    public DataTable Search_Firist_Details( string ProdecutID, string UnitFactor, string StoreID)
    {
        return ExecteRader(@"SELECT        dbo.Bay_Main.BillType, dbo.Bay_Details.ProdecutID, dbo.Bay_Details.UnitFactor, dbo.Bay_Details.UnitOperating, dbo.Bay_Details.StoreID
FROM            dbo.Bay_Details INNER JOIN
                         dbo.Bay_Main ON dbo.Bay_Details.MainID = dbo.Bay_Main.MainID where  dbo.Bay_Main.BillType=@BillType and  dbo.Bay_Details.ProdecutID=@ProdecutID and dbo.Bay_Details.UnitFactor=@UnitFactor and dbo.Bay_Details.StoreID=@StoreID", CommandType.Text,
                         Parameter("@ProdecutID", SqlDbType.NVarChar, ProdecutID),
                              Parameter("@UnitFactor", SqlDbType.NVarChar, UnitFactor),
            Parameter("@StoreID", SqlDbType.NVarChar, StoreID));
    }



    //Details ID
    public DataTable Details_Firist_Main(string BillType,string MainID)
    {
        return ExecteRader("Select *  from Bay_Main where BillType=@BillType and MainID=@MainID ", CommandType.Text,
                  Parameter("@BillType", SqlDbType.NVarChar, BillType),
                   Parameter("@MainID", SqlDbType.NVarChar, MainID));

    }

    //Details ID
    public DataTable Details_Firist_Main(string BillType)
    {
        return ExecteRader("Select *  from Bay_Main where BillType=@BillType", CommandType.Text,
                  Parameter("@BillType", SqlDbType.NVarChar, BillType));

    }

    // Bay_Details






}

