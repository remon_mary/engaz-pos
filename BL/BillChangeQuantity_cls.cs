﻿using System;
using System.Data;

class BillChangeQuantity_cls : DataAccessLayer
{

    //MaxID
    public String MaxID_ChangeQuantity_Main()
    {
        return Execute_SQL("select ISNULL (MAX(MainID)+1,1) from ChangeQuantity_Main", CommandType.Text);
    }



    // Insert
    public void InsertChangeQuantity_Main(string MainID, DateTime MyDate, string Remarks, int UserAdd)
    {
        Execute_SQL("insert into ChangeQuantity_Main(MainID ,MyDate ,Remarks ,UserAdd )Values (@MainID ,@MyDate ,@Remarks ,@UserAdd )", CommandType.Text,
        Parameter("@MainID", SqlDbType.BigInt, MainID),
        Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@Remarks", SqlDbType.NText, Remarks),
        Parameter("@UserAdd", SqlDbType.Int, UserAdd));
    }

    //Update
    public void UpdateChangeQuantity_Main(string MainID, DateTime MyDate, string Remarks)
    {
        Execute_SQL("Update ChangeQuantity_Main Set MainID=@MainID ,MyDate=@MyDate ,Remarks=@Remarks  where MainID=@MainID", CommandType.Text,
        Parameter("@MainID", SqlDbType.BigInt, MainID),
        Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@Remarks", SqlDbType.NText, Remarks));
    }

    //Delete
    public void DeleteChangeQuantity_Main(string MainID)
    {
        Execute_SQL("Delete  From ChangeQuantity_Main where MainID=@MainID", CommandType.Text,
        Parameter("@MainID", SqlDbType.NVarChar, MainID));
    }





    //Details ID
    public DataTable Details_ChangeQuantity_Main(string MainID)
    {
        return ExecteRader("Select MainID ,MyDate ,Remarks ,UserAdd  from ChangeQuantity_Main where MainID=@MainID", CommandType.Text,
        Parameter("@MainID", SqlDbType.NVarChar, MainID));
    }


    //Search 
    public DataTable Search_ChangeQuantity_Main()
    {
        return ExecteRader("Select MainID ,MyDate ,Remarks ,UserAdd  from ChangeQuantity_Main", CommandType.Text);
    }

    //=================================================================================================================================


    //MaxID
    private String MaxID_ChangeQuantity_Details()
    {
        return Execute_SQL("select ISNULL (MAX(ID)+1,1) from ChangeQuantity_Details", CommandType.Text);
    }

    // Insert
    public void InsertChangeQuantity_Details(string MainID, string ProdecutID, string Unit, string UnitFactor, string UnitOperating, string Quantity,string Price,string TotalPrice, string StoreID, Boolean SumType, Boolean Recived)
    {
        Execute_SQL("insert into ChangeQuantity_Details(ID ,MainID ,ProdecutID ,Unit ,UnitFactor ,UnitOperating ,Quantity,Price,TotalPrice ,StoreID ,SumType ,Recived )Values (@ID ,@MainID ,@ProdecutID ,@Unit ,@UnitFactor ,@UnitOperating ,@Quantity,@Price,@TotalPrice ,@StoreID ,@SumType ,@Recived )", CommandType.Text,
        Parameter("@ID", SqlDbType.BigInt, MaxID_ChangeQuantity_Details()),
        Parameter("@MainID", SqlDbType.BigInt, MainID),
        Parameter("@ProdecutID", SqlDbType.Int, ProdecutID),
        Parameter("@Unit", SqlDbType.NVarChar, Unit),
        Parameter("@UnitFactor", SqlDbType.Float, UnitFactor),
        Parameter("@UnitOperating", SqlDbType.Float, UnitOperating),
        Parameter("@Quantity", SqlDbType.Float, Quantity),
        Parameter("@Price", SqlDbType.Float, Price),
        Parameter("@TotalPrice", SqlDbType.Float, TotalPrice),
        Parameter("@StoreID", SqlDbType.Int, StoreID),
        Parameter("@SumType", SqlDbType.Bit, SumType),
        Parameter("@Recived", SqlDbType.Bit, Recived));
    }



    //Delete
    public void Delete_ChangeQuantity_Details(string MainID)
    {
        Execute_SQL("delete from ChangeQuantity_Details where MainID=@MainID", CommandType.Text,
        Parameter("@MainID", SqlDbType.NVarChar, MainID));
    }



    //Details ID
    public DataTable Details_ChangeQuantity_Details(string MainID)
    {
        return ExecteRader(@"SELECT  dbo.ChangeQuantity_Details.ID, dbo.ChangeQuantity_Details.MainID, dbo.ChangeQuantity_Details.ProdecutID, dbo.Prodecuts.ProdecutName, dbo.ChangeQuantity_Details.Unit, dbo.ChangeQuantity_Details.UnitFactor, 
                         dbo.ChangeQuantity_Details.UnitOperating, dbo.ChangeQuantity_Details.Quantity, dbo.ChangeQuantity_Details.StoreID, dbo.Stores.StoreName, dbo.ChangeQuantity_Details.SumType, dbo.ChangeQuantity_Details.Recived, 
                         dbo.ChangeQuantity_Details.Price, dbo.ChangeQuantity_Details.TotalPrice
FROM            dbo.ChangeQuantity_Details INNER JOIN
                         dbo.Prodecuts ON dbo.ChangeQuantity_Details.ProdecutID = dbo.Prodecuts.ProdecutID INNER JOIN
                         dbo.Stores ON dbo.ChangeQuantity_Details.StoreID = dbo.Stores.StoreID
                      where dbo.ChangeQuantity_Details.MainID=@MainID", CommandType.Text,
        Parameter("@MainID", SqlDbType.NVarChar, MainID));
    }








}

