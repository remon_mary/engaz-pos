﻿using System;
using System.Data;

class Unit_cls : DataAccessLayer
{
    //MaxID
    public String MaxID_Unit()
    {
        return Execute_SQL("select ISNULL (MAX(UnitID)+1,1) from Unit", CommandType.Text);
    }

    // Insert
    public void Insert_Unit(int UnitID, String UnitName)
    {
        Execute_SQL("insert into Unit(UnitID ,UnitName )Values (@UnitID ,@UnitName )", CommandType.Text,
         Parameter("@UnitID", SqlDbType.Int, UnitID),
        Parameter("@UnitName", SqlDbType.NVarChar, UnitName));
    }

    //Update
    public void Update_Unit(int UnitID, String UnitName)
    {
        Execute_SQL("Update Unit Set UnitID=@UnitID ,UnitName=@UnitName  where UnitID=@UnitID", CommandType.Text,
          Parameter("@UnitID", SqlDbType.Int, UnitID),
        Parameter("@UnitName", SqlDbType.NVarChar, UnitName));
    }

    //Delete
    public void Delete_Unit(int UnitID)
    {
        Execute_SQL("Delete  From Unit where UnitID=@UnitID", CommandType.Text,
        Parameter("@UnitID", SqlDbType.Int, UnitID));

    }

    //Details ID
    public DataTable Details_Unit(int UnitID)
    {
        return ExecteRader("Select *  From Unit where UnitID=@UnitID", CommandType.Text,
        Parameter("@UnitID", SqlDbType.Int, UnitID));
    }

    //Details ID
    public DataTable NameSearch_Unit(String UnitName)
    {
        return ExecteRader("Select UnitName  From Unit where UnitName=@UnitName", CommandType.Text,
        Parameter("@UnitName", SqlDbType.NVarChar, UnitName));
    }

    //Search
    public DataTable Search_Unit(String Search)
    {
        return ExecteRader("Select *  from Unit where convert(nvarchar, UnitID) + UnitName like '%' + @Search + '%'", CommandType.Text,
        Parameter("@Search", SqlDbType.NVarChar, Search));
    }



}

