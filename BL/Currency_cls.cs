﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

class Currency_cls : DataAccessLayer
    {

    //MaxID
    public String MaxID_Currency()
    {
        return Execute_SQL("select ISNULL (MAX(CurrencyID)+1,1) from Currency", CommandType.Text);
    }


    // Insert
    public void Insert_Currency(String CurrencyID, String CurrencyName, String CurrencyShortcut, String CurrencyPartName, String CurrencyRate, String Remark)
    {
        Execute_SQL("insert into Currency(CurrencyID ,CurrencyName ,CurrencyShortcut ,CurrencyPartName ,CurrencyRate ,Remark )Values (@CurrencyID ,@CurrencyName ,@CurrencyShortcut ,@CurrencyPartName ,@CurrencyRate ,@Remark )", CommandType.Text,
        Parameter("@CurrencyID", SqlDbType.Int, CurrencyID),
        Parameter("@CurrencyName", SqlDbType.NVarChar, CurrencyName),
        Parameter("@CurrencyShortcut", SqlDbType.NVarChar, CurrencyShortcut),
        Parameter("@CurrencyPartName", SqlDbType.NVarChar, CurrencyPartName),
        Parameter("@CurrencyRate", SqlDbType.Float, CurrencyRate),
        Parameter("@Remark", SqlDbType.NVarChar, Remark));
    }

    //Update
    public void Update_Currency(String CurrencyID, String CurrencyName, String CurrencyShortcut, String CurrencyPartName, String CurrencyRate, String Remark)
    {
        Execute_SQL("Update Currency Set CurrencyID=@CurrencyID ,CurrencyName=@CurrencyName ,CurrencyShortcut=@CurrencyShortcut ,CurrencyPartName=@CurrencyPartName ,CurrencyRate=@CurrencyRate ,Remark=@Remark  where CurrencyID=@CurrencyID", CommandType.Text,
        Parameter("@CurrencyID", SqlDbType.Int, CurrencyID),
        Parameter("@CurrencyName", SqlDbType.NVarChar, CurrencyName),
        Parameter("@CurrencyShortcut", SqlDbType.NVarChar, CurrencyShortcut),
        Parameter("@CurrencyPartName", SqlDbType.NVarChar, CurrencyPartName),
        Parameter("@CurrencyRate", SqlDbType.Float, CurrencyRate),
        Parameter("@Remark", SqlDbType.NVarChar, Remark));
    }

    //Delete
    public void Delete_Currency(String CurrencyID)
    {
        Execute_SQL("Delete  From Currency where CurrencyID=@CurrencyID", CommandType.Text,
        Parameter("@CurrencyID", SqlDbType.Int, CurrencyID));
    }

    //Details ID
    public DataTable Details_Currency(String CurrencyID)
    {
        return ExecteRader("Select *  from Currency Where CurrencyID=@CurrencyID", CommandType.Text,
        Parameter("@CurrencyID", SqlDbType.Int, CurrencyID));
    }

    //Details ID
    public DataTable NameSearch__Currency(String CurrencyName)
    {
        return ExecteRader("Select CurrencyName  from Currency Where CurrencyName=@CurrencyName", CommandType.Text,
        Parameter("@CurrencyName", SqlDbType.NVarChar, CurrencyName));
    }

    //Search 
    public DataTable Search__Currency(string Search)
    {
        return ExecteRader("Select *  from Currency Where convert(nvarchar,CurrencyID)+CurrencyName+CurrencyShortcut+CurrencyPartName like '%'+@Search+ '%' ", CommandType.Text,
        Parameter("@Search", SqlDbType.NVarChar, Search));
    }


}

