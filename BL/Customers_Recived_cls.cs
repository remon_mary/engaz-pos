﻿using System;
using System.Data;

class Customers_Recived_cls : DataAccessLayer
{

    //MaxID
    public String MaxID_Customers_Recived()
    {
        return Execute_SQL("select ISNULL (MAX(PayID)+1,1) from Customers_Recived", CommandType.Text);
    }

    // Insert
    public void InsertCustomers_Recived(string PayID, DateTime PayDate, Double PayValue, string Remarks, string CustomerID,String TreasuryID, int UserAdd)
    {
        Execute_SQL("insert into Customers_Recived(PayID ,PayDate ,PayValue ,Remarks ,CustomerID,TreasuryID ,UserAdd )Values (@PayID,@PayDate ,@PayValue ,@Remarks ,@CustomerID,@TreasuryID ,@UserAdd )", CommandType.Text,
        Parameter("@PayID", SqlDbType.NVarChar, PayID),
        Parameter("@PayDate", SqlDbType.Date, PayDate),
        Parameter("@PayValue", SqlDbType.Float, PayValue),
        Parameter("@Remarks", SqlDbType.NVarChar, Remarks),
        Parameter("@CustomerID", SqlDbType.Int, CustomerID),
        Parameter("@TreasuryID", SqlDbType.NVarChar, TreasuryID),
        Parameter("@UserAdd", SqlDbType.Int, UserAdd));
    }

    //Update
    public void UpdateCustomers_Recived(string PayID, DateTime PayDate, Double PayValue, string Remarks, string CustomerID)
    {
        Execute_SQL("Update Customers_Recived Set PayID=@PayID,PayDate=@PayDate ,PayValue=@PayValue ,Remarks=@Remarks ,CustomerID=@CustomerID   where PayID=@PayID", CommandType.Text,
        Parameter("@PayID", SqlDbType.NVarChar, PayID),
        Parameter("@PayDate", SqlDbType.Date, PayDate),
        Parameter("@PayValue", SqlDbType.Float, PayValue),
        Parameter("@Remarks", SqlDbType.NText, Remarks),
        Parameter("@CustomerID", SqlDbType.Int, CustomerID));
    }




    //Delete
    public void DeleteCustomers_Recived(string PayID)
    {
        Execute_SQL("Delete  From Customers_Recived where PayID=@PayID", CommandType.Text,
        Parameter("@PayID", SqlDbType.BigInt, PayID));
    }



    //==================================================================================================================================================================




    //Search
    public DataTable Search_Customers_Recived(string Search, DateTime PayDate, DateTime PayDate2)
    {
        string sql = @"SELECT        dbo.Customers_Recived.*, dbo.Customers.CustomerName, dbo.UserPermissions.EmpName
FROM            dbo.Customers_Recived INNER JOIN
                         dbo.Customers ON dbo.Customers_Recived.CustomerID = dbo.Customers.CustomerID INNER JOIN
                         dbo.UserPermissions ON dbo.Customers_Recived.UserAdd = dbo.UserPermissions.ID
        WHERE PayDate>= @PayDate And PayDate<= @PayDate2 And convert(nvarchar,dbo.Customers_Recived.PayID)+dbo.Customers.CustomerName like '%'+ @Search + '%'";
        return ExecteRader(sql, CommandType.Text,
        Parameter("@Search", SqlDbType.NVarChar, Search),
        Parameter("@PayDate", SqlDbType.Date, PayDate),
        Parameter("@PayDate2", SqlDbType.Date, PayDate2));
    }







    //Details   Where id
    public DataTable Details_Customers_Recived(String PayID)
    {
        string sql = @"SELECT        dbo.Customers_Recived.*, dbo.Customers.CustomerName
FROM            dbo.Customers_Recived INNER JOIN
                         dbo.Customers ON dbo.Customers_Recived.CustomerID = dbo.Customers.CustomerID
         WHERE  (dbo.Customers_Recived.PayID=@PayID)";
        return ExecteRader(sql, CommandType.Text,
        Parameter("@PayID", SqlDbType.NVarChar, PayID));
    }




}

