﻿
using System;
using System.Data;

class Store_Prodecut_cls : DataAccessLayer
{




    //MaxID
    private String MaxID_Store_Prodecut()
    {
        return Execute_SQL("select ISNULL (MAX(ID)+1,1) from Store_Prodecut", CommandType.Text);
    }


    // Insert
    public void InsertStore_Prodecut(string ProdecutID, string StoreID, string Balence, string Price, string ProdecutAvg)
    {
        Execute_SQL("insert into Store_Prodecut(ID ,ProdecutID ,StoreID ,Balence ,Price ,ProdecutAvg )Values (@ID ,@ProdecutID ,@StoreID ,@Balence ,@Price ,@ProdecutAvg )", CommandType.Text,
        Parameter("@ID", SqlDbType.BigInt, MaxID_Store_Prodecut()),
        Parameter("@ProdecutID", SqlDbType.Int, ProdecutID),
        Parameter("@StoreID", SqlDbType.Int, StoreID),
        Parameter("@Balence", SqlDbType.Float, Balence),
        Parameter("@Price", SqlDbType.Float, Price),
        Parameter("@ProdecutAvg", SqlDbType.Float, ProdecutAvg));
    }


    //Details ID
    public DataTable Details_Store_Prodecut(string ProdecutID, string StoreID)
    {
        return ExecteRader("Select  *  from Store_Prodecut where ProdecutID=@ProdecutID and StoreID=@StoreID", CommandType.Text,
        Parameter("@ProdecutID", SqlDbType.Int, ProdecutID),
        Parameter("@StoreID", SqlDbType.Int, StoreID));
    }

    //Details ID
    public  DataTable Store_Balence(string ProdecutID, string StoreID)
    {
        return  ExecteRader("Select  Balence ,Price ,ProdecutAvg  from Store_Prodecut where ProdecutID=@ProdecutID and StoreID=@StoreID", CommandType.Text,
        Parameter("@ProdecutID", SqlDbType.Int, ProdecutID),
        Parameter("@StoreID", SqlDbType.Int, StoreID));

    }

    public Double Prodect_Balence(string ProdecutID, string StoreID)
    {
        double xx=0;
        DataTable dt = Store_Balence(ProdecutID, StoreID);
        return xx = Convert.ToDouble(dt.Rows[0]["Balence"]);
    }

    public Double Prodect_Price(string ProdecutID, string StoreID)
    {
        double mm = 0;
        DataTable dt = Store_Balence(ProdecutID, StoreID);
        return  mm = Convert.ToDouble(dt.Rows[0]["Price"]);
    }

   


    //////Update
    //public void Import_Plus(string ProdecutID, string StoreID, Double Balence, Double Price)
    //{
    //    double XBalence = (Prodect_Balence(ProdecutID, StoreID)) + Balence;
    //    double XPrice = (Prodect_Price(ProdecutID, StoreID)) + Price;
    //    Execute_SQL("Update Store_Prodecut Set  Balence=@Balence,Price=@Price Where ProdecutID=@ProdecutID  And StoreID=@StoreID", CommandType.Text,
    //    Parameter("@ProdecutID", SqlDbType.Int, ProdecutID),
    //    Parameter("@StoreID", SqlDbType.Int, StoreID),
    //    Parameter("@Balence", SqlDbType.Float, XBalence),
    //     Parameter("@Price", SqlDbType.Float, XPrice));
    //}


    ////Update
    //public void Export_Mines(string ProdecutID, string StoreID, Double Balence, Double Price)
    //{

    //    double XBalence = (Prodect_Balence(ProdecutID, StoreID)) - Balence;
    //    double XPrice = (Prodect_Price(ProdecutID, StoreID)) - Price;
    //    Execute_SQL("Update Store_Prodecut Set  Balence=@Balence,Price=@Price Where ProdecutID=@ProdecutID  And StoreID=@StoreID", CommandType.Text,
    //    Parameter("@ProdecutID", SqlDbType.Int, ProdecutID),
    //    Parameter("@StoreID", SqlDbType.Int, StoreID),
    //    Parameter("@Balence", SqlDbType.Float, XBalence),
    //     Parameter("@Price", SqlDbType.Float, XPrice));
    //}


    //Update
    //public void AVG_ProdecutAvg(string ProdecutID, string StoreID)
    //{
    //    AVG_cls AVG_cls = new AVG_cls();
    //    Double   ProdecutAvg = AVG_cls.dt_Avg(ProdecutID, StoreID);
    //    Execute_SQL("Update Store_Prodecut Set ProdecutAvg=@ProdecutAvg Where ProdecutID=@ProdecutID  And StoreID=@StoreID", CommandType.Text,
    //    Parameter("@ProdecutID", SqlDbType.Int, ProdecutID),
    //    Parameter("@StoreID", SqlDbType.Int, StoreID),
    //    Parameter("@ProdecutAvg", SqlDbType.Float, ProdecutAvg));
    //}



    #region "جرد المخزن"
    //Details ID
    public DataTable Store_Prodecut(int? StoreID, int? CategoryID, int? ProdecutID)
    {
        return ExecteRader("ProductStores", CommandType.StoredProcedure,

        Parameter("@StoreID", SqlDbType.Int, StoreID),
        Parameter("@CategoryID", SqlDbType.Int, CategoryID),
           Parameter("@ProdecutID", SqlDbType.Int, ProdecutID));
    }


    //Details ID
//    public DataTable Store_Prodecut(string StoreID)
//    {
//        return ExecteRader(@"SELECT        dbo.Store_Prodecut.ProdecutID, dbo.Prodecuts.ProdecutName, dbo.Prodecuts.CategoryID, dbo.Prodecuts.RequestLimit, dbo.Prodecuts.FiestUnit, 
//                         dbo.Prodecuts.FiestUnitOperating, dbo.Prodecuts.FiestUnitFactor, dbo.Prodecuts.FiestUnitBarcode, dbo.Prodecuts.SecoundUnit, 
//                         dbo.Prodecuts.SecoundUnitOperating, dbo.Prodecuts.SecoundUnitFactor, dbo.Prodecuts.SecoundUnitBarcode, dbo.Prodecuts.ThreeUnit, 
//                         dbo.Prodecuts.ThreeUnitOperating, dbo.Prodecuts.ThreeUnitFactor, dbo.Prodecuts.ThreeUnitBarcode, dbo.Store_Prodecut.StoreID, dbo.Store_Prodecut.Balence, 
//                         dbo.Store_Prodecut.Price, dbo.Store_Prodecut.ProdecutAvg, dbo.Prodecuts.FiestUnitPrice1
//FROM            dbo.Store_Prodecut INNER JOIN
//                         dbo.Prodecuts ON dbo.Store_Prodecut.ProdecutID = dbo.Prodecuts.ProdecutID
//WHERE        (dbo.Store_Prodecut.StoreID =@StoreID)", CommandType.Text,
//        Parameter("@StoreID", SqlDbType.NVarChar, StoreID));
//    }


//    public DataTable Store_Prodecut()
//    {
//        return ExecteRader(@"SELECT        dbo.Store_Prodecut.ProdecutID, dbo.Prodecuts.ProdecutName, dbo.Prodecuts.CategoryID, dbo.Prodecuts.RequestLimit, dbo.Prodecuts.FiestUnit, 
//                         dbo.Prodecuts.FiestUnitOperating, dbo.Prodecuts.FiestUnitFactor, dbo.Prodecuts.FiestUnitBarcode, dbo.Prodecuts.SecoundUnit, 
//                         dbo.Prodecuts.SecoundUnitOperating, dbo.Prodecuts.SecoundUnitFactor, dbo.Prodecuts.SecoundUnitBarcode, dbo.Prodecuts.ThreeUnit, 
//                         dbo.Prodecuts.ThreeUnitOperating, dbo.Prodecuts.ThreeUnitFactor, dbo.Prodecuts.ThreeUnitBarcode, dbo.Store_Prodecut.StoreID, dbo.Store_Prodecut.Balence, 
//                         dbo.Store_Prodecut.Price, dbo.Store_Prodecut.ProdecutAvg, dbo.Prodecuts.FiestUnitPrice1
//FROM            dbo.Store_Prodecut INNER JOIN
//                         dbo.Prodecuts ON dbo.Store_Prodecut.ProdecutID = dbo.Prodecuts.ProdecutID", CommandType.Text);
//    }

//    public DataTable Store_ProdecutCategoryID( string CategoryID)
//    {
//        return ExecteRader(@"SELECT        dbo.Store_Prodecut.ProdecutID, dbo.Prodecuts.ProdecutName, dbo.Prodecuts.CategoryID, dbo.Prodecuts.RequestLimit, dbo.Prodecuts.FiestUnit, 
//                         dbo.Prodecuts.FiestUnitOperating, dbo.Prodecuts.FiestUnitFactor, dbo.Prodecuts.FiestUnitBarcode, dbo.Prodecuts.SecoundUnit, 
//                         dbo.Prodecuts.SecoundUnitOperating, dbo.Prodecuts.SecoundUnitFactor, dbo.Prodecuts.SecoundUnitBarcode, dbo.Prodecuts.ThreeUnit, 
//                         dbo.Prodecuts.ThreeUnitOperating, dbo.Prodecuts.ThreeUnitFactor, dbo.Prodecuts.ThreeUnitBarcode, dbo.Store_Prodecut.StoreID, dbo.Store_Prodecut.Balence, 
//                         dbo.Store_Prodecut.Price, dbo.Store_Prodecut.ProdecutAvg, dbo.Prodecuts.FiestUnitPrice1
//FROM            dbo.Store_Prodecut INNER JOIN
//                         dbo.Prodecuts ON dbo.Store_Prodecut.ProdecutID = dbo.Prodecuts.ProdecutID
//WHERE  (dbo.Prodecuts.CategoryID =@CategoryID)", CommandType.Text,
//        Parameter("@CategoryID", SqlDbType.NVarChar, CategoryID));
//    }



//    public DataTable Store_ProdecutID(string ProdecutID, string StoreID)
//    {
//        return ExecteRader(@"SELECT        dbo.Store_Prodecut.ProdecutID, dbo.Prodecuts.ProdecutName, dbo.Prodecuts.CategoryID, dbo.Prodecuts.RequestLimit, dbo.Prodecuts.FiestUnit, 
//                         dbo.Prodecuts.FiestUnitOperating, dbo.Prodecuts.FiestUnitFactor, dbo.Prodecuts.FiestUnitBarcode, dbo.Prodecuts.SecoundUnit, 
//                         dbo.Prodecuts.SecoundUnitOperating, dbo.Prodecuts.SecoundUnitFactor, dbo.Prodecuts.SecoundUnitBarcode, dbo.Prodecuts.ThreeUnit, 
//                         dbo.Prodecuts.ThreeUnitOperating, dbo.Prodecuts.ThreeUnitFactor, dbo.Prodecuts.ThreeUnitBarcode, dbo.Store_Prodecut.StoreID, dbo.Store_Prodecut.Balence, 
//                         dbo.Store_Prodecut.Price, dbo.Store_Prodecut.ProdecutAvg, dbo.Prodecuts.FiestUnitPrice1
//FROM            dbo.Store_Prodecut INNER JOIN
//                         dbo.Prodecuts ON dbo.Store_Prodecut.ProdecutID = dbo.Prodecuts.ProdecutID
//WHERE  (dbo.Prodecuts.ProdecutID =@ProdecutID) and StoreID=@StoreID", CommandType.Text,
//        Parameter("@ProdecutID", SqlDbType.NVarChar, ProdecutID),
//         Parameter("@StoreID", SqlDbType.NVarChar, StoreID));
//    }


//    public DataTable Store_ProdecutID(string ProdecutID)
//    {
//        return ExecteRader(@"SELECT        dbo.Store_Prodecut.ProdecutID, dbo.Prodecuts.ProdecutName, dbo.Prodecuts.CategoryID, dbo.Prodecuts.RequestLimit, dbo.Prodecuts.FiestUnit, 
//                         dbo.Prodecuts.FiestUnitOperating, dbo.Prodecuts.FiestUnitFactor, dbo.Prodecuts.FiestUnitBarcode, dbo.Prodecuts.SecoundUnit, 
//                         dbo.Prodecuts.SecoundUnitOperating, dbo.Prodecuts.SecoundUnitFactor, dbo.Prodecuts.SecoundUnitBarcode, dbo.Prodecuts.ThreeUnit, 
//                         dbo.Prodecuts.ThreeUnitOperating, dbo.Prodecuts.ThreeUnitFactor, dbo.Prodecuts.ThreeUnitBarcode, dbo.Store_Prodecut.StoreID, dbo.Store_Prodecut.Balence, 
//                         dbo.Store_Prodecut.Price, dbo.Store_Prodecut.ProdecutAvg, dbo.Prodecuts.FiestUnitPrice1
//FROM            dbo.Store_Prodecut INNER JOIN
//                         dbo.Prodecuts ON dbo.Store_Prodecut.ProdecutID = dbo.Prodecuts.ProdecutID
//WHERE  (dbo.Prodecuts.ProdecutID =@ProdecutID) ", CommandType.Text,
//        Parameter("@ProdecutID", SqlDbType.NVarChar, ProdecutID));
//    }

    #endregion




    #region "الاصناف التي وصلت حد الطلب"

    public DataTable Store_Prodecut_Minim(string StoreID, string CategoryID)
    {
        return ExecteRader(@"SELECT dbo.Store_Prodecut.ID, dbo.Store_Prodecut.ProdecutID, dbo.Prodecuts.ProdecutName, dbo.Prodecuts.CategoryID, dbo.Prodecuts.RequestLimit, dbo.Prodecuts.FiestUnit, dbo.Prodecuts.FiestUnitOperating,
                         dbo.Prodecuts.FiestUnitFactor, dbo.Prodecuts.FiestUnitBarcode, dbo.Prodecuts.SecoundUnit, dbo.Prodecuts.SecoundUnitOperating, dbo.Prodecuts.SecoundUnitFactor, dbo.Prodecuts.SecoundUnitBarcode,
                         dbo.Prodecuts.ThreeUnit, dbo.Prodecuts.ThreeUnitOperating, dbo.Prodecuts.ThreeUnitFactor, dbo.Prodecuts.ThreeUnitBarcode, dbo.Prodecuts.ProdecutAvg, dbo.Store_Prodecut.StoreID, dbo.Store_Prodecut.Balence,
                         dbo.Store_Prodecut.Price, dbo.Store_Prodecut.ProdecutAvg AS Expr1
FROM            dbo.Prodecuts INNER JOIN
                         dbo.Store_Prodecut ON dbo.Prodecuts.ProdecutID = dbo.Store_Prodecut.ProdecutID
WHERE        (dbo.Store_Prodecut.StoreID =@StoreID) AND(dbo.Prodecuts.CategoryID =@CategoryID)", CommandType.Text,
        Parameter("@StoreID", SqlDbType.NVarChar, StoreID),
        Parameter("@CategoryID", SqlDbType.NVarChar, CategoryID));
    }


    //Details ID
    public DataTable Store_Prodecut_Minim(string StoreID)
    {
        return ExecteRader(@" SELECT dbo.Store_Prodecut.ID, dbo.Store_Prodecut.ProdecutID, dbo.Prodecuts.ProdecutName, dbo.Prodecuts.CategoryID, dbo.Prodecuts.RequestLimit, dbo.Prodecuts.FiestUnit, dbo.Prodecuts.FiestUnitOperating,
                         dbo.Prodecuts.FiestUnitFactor, dbo.Prodecuts.FiestUnitBarcode, dbo.Prodecuts.SecoundUnit, dbo.Prodecuts.SecoundUnitOperating, dbo.Prodecuts.SecoundUnitFactor, dbo.Prodecuts.SecoundUnitBarcode,
                         dbo.Prodecuts.ThreeUnit, dbo.Prodecuts.ThreeUnitOperating, dbo.Prodecuts.ThreeUnitFactor, dbo.Prodecuts.ThreeUnitBarcode, dbo.Prodecuts.ProdecutAvg, dbo.Store_Prodecut.StoreID, dbo.Store_Prodecut.Balence,
                         dbo.Store_Prodecut.Price, dbo.Store_Prodecut.ProdecutAvg AS Expr1
FROM            dbo.Prodecuts INNER JOIN
                         dbo.Store_Prodecut ON dbo.Prodecuts.ProdecutID = dbo.Store_Prodecut.ProdecutID
WHERE        (dbo.Store_Prodecut.StoreID =@StoreID)", CommandType.Text,
        Parameter("@StoreID", SqlDbType.NVarChar, StoreID));
    }


    public DataTable Store_Prodecut_MinimNotifcation()
    {
        return ExecteRader(@"SELECT  dbo.Store_Prodecut.ProdecutID, dbo.Prodecuts.ProdecutName, dbo.Store_Prodecut.StoreID, dbo.Stores.StoreName, dbo.Store_Prodecut.Balence, 
                         dbo.Prodecuts.RequestLimit
FROM            dbo.Stores INNER JOIN
                         dbo.Store_Prodecut ON dbo.Stores.StoreID = dbo.Store_Prodecut.StoreID INNER JOIN
                         dbo.Prodecuts ON dbo.Store_Prodecut.ProdecutID = dbo.Prodecuts.ProdecutID WHERE	dbo.Store_Prodecut.Balence <=  dbo.Prodecuts.RequestLimit", CommandType.Text);
    }

    #endregion



    //#region "Update Quantity , Total prise and  Avg"

    //#endregion



}

