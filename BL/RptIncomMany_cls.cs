﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace ByStro.BL
{
    class RptIncomMany_cls : DataAccessLayer
    {

        public string PayType_trans()
        {
            return Execute_SQL(@"select ISNULL(SUM(Debit),0)  -ISNULL(SUM(Credit),0) as balanse from PayType_trans", CommandType.Text);
        }

        public string Balence_ISCusSupp(string ISCusSupp)
        {
            string sql = "Select isnull (sum(Debit)-sum(Credit),0) AS balence from TreasuryMovement where  ISCusSupp=@ISCusSupp";

            return Execute_SQL(sql, CommandType.Text,
                  Parameter("@ISCusSupp", SqlDbType.NVarChar, ISCusSupp));
        }



        public string Store_trans()
        {
            return Execute_SQL(@"select ISNULL(SUM(Price),0)  as balanse from Store_Prodecut", CommandType.Text);
        }







        ////Details ID
        //public string Search_Sales()
        //{
        //    return Execute_SQL("select isnull( SUM(NetInvoice),0) as NetInvoice, isnull(SUM(ReturnInvoise),0) as ReturnInvoise ,isnull(SUM(TotalBayPrice),0) as TotalBayPrice , isnull(SUM(ReturnBayPrice),0) as ReturnBayPrice  from Sales_Main", CommandType.Text);
        //}



        //public string Search_Bay()
        //{
        //    return Execute_SQL("select isnull (SUM(Discount),0) as Discount from Bay_Main", CommandType.Text);
        //}









        //Details ID
        public DataTable Search_Sales()
        {
            return ExecteRader("select isnull( SUM(NetInvoice),0) as NetInvoice, isnull(SUM(ReturnInvoise),0) as ReturnInvoise ,isnull(SUM(TotalBayPrice),0) as TotalBayPrice , isnull(SUM(ReturnBayPrice),0) as ReturnBayPrice  from Sales_Main", CommandType.Text);
        }


        //Details ID
        public DataTable Search_Bay()
        {
            return ExecteRader("select isnull (SUM(Discount),0) as Discount from Bay_Main", CommandType.Text);
        }


        //Details ID
        public DataTable Search_FirestProdecut()
        {
            return ExecteRader("select isnull (SUM(TotalPrice),0) as TotalPrice from Firist_Details", CommandType.Text);
        }

        // Expenses
        public DataTable Search_Expenses()
        {
            return ExecteRader("select isnull (SUM(ExpenseValue),0) as ExpenseValue from Expenses", CommandType.Text);
        }

        // Expenses
        public DataTable Search_Salary( )
        {
            return ExecteRader("select isnull (SUM(MainSalary),0)+isnull(SUM(Additions),0) as NetSalary from Employee_Salary", CommandType.Text);
        }

        // Expenses
        public DataTable Search_Maintenance( )
        {
            return ExecteRader("select isnull (SUM(PaidInvoice),0) as Paid from Maintenance_Main", CommandType.Text);
        }
        //Details ID
        public DataTable Details_ALL_Prodecuts()
        {
            return ExecteRader("Select ProdecutID  from Prodecuts", CommandType.Text);
        }



        public string Capital()
        {
            return Execute_SQL(" SELECT ISNULL(SUM(Debit),0)-ISNULL(SUM(Credit),0) AS balanse from Capital", CommandType.Text);
        }



       


    }
}
