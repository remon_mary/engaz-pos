﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace ByStro.BL
{
    public static class Final_key
    {

        public static int SalesNumber = 5;
        public static string GetHash(string s)
        {
            return GetHexString(new MD5CryptoServiceProvider().ComputeHash(new ASCIIEncoding().GetBytes(s)));
        }

        private static string GetHexString(byte[] bt)
        {
            string str1 = string.Empty;
            for (int index = 0; index < bt.Length; ++index)
            {
                int num1 = (int)bt[index];
                int num2 = 15;
                int num3 = num1 & num2;
                int num4 = 4;
                int num5 = num1 >> num4 & 15;
                string str2 = num5 <= 8 ? str1 + num5.ToString() : str1 + ((char)(num5 - 8 + 65)).ToString();
                str1 = num3 <= 8 ? str2 + num3.ToString() : str2 + ((char)(num3 - 8 + 65)).ToString();
                if (index + 1 != bt.Length && (index + 1) % 2 == 0)
                    str1 += "-";
            }
            return str1;
        }










    }
}
