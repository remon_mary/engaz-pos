﻿
using System;
using System.Data;

class Money_Customer_cls : DataAccessLayer
{
    //Details ID
    public DataTable Money_Prodects_Sales_Details(DateTime MyDate, DateTime MyDate2)
    {
        return ExecteRader(@"SELECT        dbo.Sales_Main.MyDate, dbo.Sales_Main.CustomerID, dbo.Customers.CustomerName, dbo.Sales_Main.NetInvoice, dbo.Sales_Main.ReturnInvoise, dbo.Sales_Main.TotalProfits, dbo.UserPermissions.EmpName, 
                         dbo.Sales_Main.MainID FROM   dbo.Customers INNER JOIN
                         dbo.Sales_Main ON dbo.Customers.CustomerID = dbo.Sales_Main.CustomerID INNER JOIN
                         dbo.UserPermissions ON dbo.Sales_Main.UserAdd = dbo.UserPermissions.ID
                  Where MyDate>= @MyDate And MyDate<= @MyDate2", CommandType.Text,
        Parameter("@MyDate", SqlDbType.Date, MyDate),
               Parameter("@MyDate2", SqlDbType.Date, MyDate2));
    }




    public DataTable Money_Prodects(DateTime MyDate, DateTime MyDate2, string CustomerID)
    {
        return ExecteRader(@"SELECT        dbo.Sales_Main.MyDate, dbo.Sales_Main.CustomerID, dbo.Customers.CustomerName, dbo.Sales_Main.NetInvoice, dbo.Sales_Main.ReturnInvoise, dbo.Sales_Main.TotalProfits, dbo.UserPermissions.EmpName, 
                         dbo.Sales_Main.MainID
FROM            dbo.Customers INNER JOIN
                         dbo.Sales_Main ON dbo.Customers.CustomerID = dbo.Sales_Main.CustomerID INNER JOIN
                         dbo.UserPermissions ON dbo.Sales_Main.UserAdd = dbo.UserPermissions.ID
                  Where MyDate>= @MyDate And MyDate<= @MyDate2 and dbo.Sales_Main.CustomerID=@CustomerID", CommandType.Text,
        Parameter("@MyDate", SqlDbType.Date, MyDate),
               Parameter("@MyDate2", SqlDbType.Date, MyDate2),
                 Parameter("@CustomerID", SqlDbType.NVarChar, CustomerID));
    }


    






}

