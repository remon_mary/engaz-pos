﻿using System;
using System.Windows.Forms;

class Mass : DataAccessLayer
    {


    public static void Saved()
    {

            MessageBox.Show("تمت عملية الحفظ بنجاح", "حفظ", MessageBoxButtons.OK, MessageBoxIcon.Information);
  
    }

    public static void Update()
    {
      
            MessageBox.Show("تمت عملية التعديل بنجاح", "تعديل", MessageBoxButtons.OK, MessageBoxIcon.Information);
     
    }

    public static Boolean Delete()
        {
            Boolean SurDelete = false;
        
                if (MessageBox.Show("سوف يتم الحذف  ! : هل تريد الاستمرار ؟", "تأكيد", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                {
                    //MessageBox.Show(" تم الحذف بنجاح", "حذف", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    SurDelete = true;
                }

            return SurDelete;
        }

  


    public static Boolean close_frm()
    {
        Boolean SurDelete = false;
            if (MessageBox.Show("هل متأكد مـن الخـروج ؟", "! تنبية", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {

                SurDelete = true;

            }
 
        return SurDelete;
    
}

    public static void NoDelete()
    {
        MessageBox.Show("لا يمكنك الحذف", " مستخدم", MessageBoxButtons.OK, MessageBoxIcon.Error);

    }



    public static void No_Fouind()
    {

       // MessageBox.Show("الصنف المدخل غير موجود في المخزن"+Environment.NewLine+" يمكنك اضافتة من شاشة اصناف أول المدة او من فاتورة الشراء", "المخزن", MessageBoxButtons.OK, MessageBoxIcon.Error);
        MessageBox.Show(string.Format("{0}\n {1} \n{2}\n", "لم يتم العثور علي الصنف المدخل", "  الصنف المدخل غير موجود في المخزن المحدد", "خطأ في ادخال اسم الصنف او الباركود"), "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);

    }

}

