﻿using System;
using System.Data;
using System.Data.SqlClient;

  class Customers_cls :DataAccessLayer
    {



    //MaxID
    public String MaxID_Customers_ID()
        {
            return Execute_SQL("select ISNULL (MAX(CustomerID)+1,1) from Customers", CommandType.Text);
        }




        // Insert
        public void InsertCustomers( string CustomerID, String CustomerName, string SalesLavel, string NationalID, string Address, string TaxFileNumber, string RegistrationNo, string StartContract, string EndContract, string Remarks, string Phone, string Phone2, string AccountNumber, string Fax, string Email, string WebSite, string Maximum, string CreditLimit, Boolean ISCusSupp, string SupplierID, int UserAdd, Boolean Status)
        {
            Execute_SQL("insert into Customers(CustomerID ,CustomerName,SalesLavel ,NationalID ,Address ,TaxFileNumber ,RegistrationNo ,StartContract ,EndContract ,Remarks ,Phone ,Phone2 ,AccountNumber ,Fax ,Email ,WebSite ,Maximum ,CreditLimit,ISCusSupp,SupplierID ,UserAdd ,Status )Values (@CustomerID ,@CustomerName ,@SalesLavel ,@NationalID ,@Address ,@TaxFileNumber ,@RegistrationNo ,@StartContract ,@EndContract ,@Remarks ,@Phone ,@Phone2 ,@AccountNumber ,@Fax ,@Email ,@WebSite ,@Maximum ,@CreditLimit,@ISCusSupp,@SupplierID ,@UserAdd  ,@Status )", CommandType.Text,
            Parameter("@CustomerID", SqlDbType.Int, CustomerID),
            Parameter("@CustomerName", SqlDbType.NVarChar, CustomerName),
            Parameter("@SalesLavel", SqlDbType.NVarChar, SalesLavel),
            Parameter("@NationalID", SqlDbType.NVarChar, NationalID),
            Parameter("@Address", SqlDbType.NVarChar, Address),
            Parameter("@TaxFileNumber", SqlDbType.NVarChar, TaxFileNumber),
            Parameter("@RegistrationNo", SqlDbType.NVarChar, RegistrationNo),
            Parameter("@StartContract", SqlDbType.NVarChar, StartContract),
            Parameter("@EndContract", SqlDbType.NVarChar, EndContract),
            Parameter("@Remarks", SqlDbType.NText, Remarks),
            Parameter("@Phone", SqlDbType.NVarChar, Phone),
            Parameter("@Phone2", SqlDbType.NVarChar, Phone2),
            Parameter("@AccountNumber", SqlDbType.NVarChar, AccountNumber),
            Parameter("@Fax", SqlDbType.NVarChar, Fax),
            Parameter("@Email", SqlDbType.NVarChar, Email),
            Parameter("@WebSite", SqlDbType.NVarChar, WebSite),
            Parameter("@Maximum", SqlDbType.NVarChar, Maximum),
            Parameter("@CreditLimit", SqlDbType.SmallInt, CreditLimit),
        
         Parameter("@ISCusSupp", SqlDbType.Bit, ISCusSupp),  
          Parameter("@SupplierID", SqlDbType.NVarChar, SupplierID),

            Parameter("@UserAdd", SqlDbType.Int, UserAdd),
            Parameter("@Status", SqlDbType.Bit, Status));
        }


    //Update
    public void UpdateCustomers(string CustomerID, string CustomerName, string SalesLavel, string NationalID, string Address, string TaxFileNumber, string RegistrationNo, string StartContract, string EndContract, string Remarks, string Phone, string Phone2, string AccountNumber, string Fax, string Email, string WebSite, string Maximum, string CreditLimit, Boolean ISCusSupp, string SupplierID, Boolean Status)
    {
        Execute_SQL("Update Customers Set CustomerName=@CustomerName ,SalesLavel=@SalesLavel ,NationalID=@NationalID ,Address=@Address ,TaxFileNumber=@TaxFileNumber ,RegistrationNo=@RegistrationNo ,StartContract=@StartContract ,EndContract=@EndContract ,Remarks=@Remarks ,Phone=@Phone ,Phone2=@Phone2 ,AccountNumber=@AccountNumber ,Fax=@Fax ,Email=@Email ,WebSite=@WebSite ,Maximum=@Maximum ,CreditLimit=@CreditLimit,ISCusSupp=@ISCusSupp,SupplierID=@SupplierID,Status=@Status  where CustomerID=@CustomerID", CommandType.Text,
        Parameter("@CustomerID", SqlDbType.Int, CustomerID),
        Parameter("@CustomerName", SqlDbType.NVarChar, CustomerName),
        Parameter("@SalesLavel", SqlDbType.NVarChar, SalesLavel),
        Parameter("@NationalID", SqlDbType.NVarChar, NationalID),
        Parameter("@Address", SqlDbType.NVarChar, Address),
        Parameter("@TaxFileNumber", SqlDbType.NVarChar, TaxFileNumber),
        Parameter("@RegistrationNo", SqlDbType.NVarChar, RegistrationNo),
        Parameter("@StartContract", SqlDbType.NVarChar, StartContract),
        Parameter("@EndContract", SqlDbType.NVarChar, EndContract),
        Parameter("@Remarks", SqlDbType.NText, Remarks),
        Parameter("@Phone", SqlDbType.NVarChar, Phone),
        Parameter("@Phone2", SqlDbType.NVarChar, Phone2),
        Parameter("@AccountNumber", SqlDbType.NVarChar, AccountNumber),
        Parameter("@Fax", SqlDbType.NVarChar, Fax),
        Parameter("@Email", SqlDbType.NVarChar, Email),
        Parameter("@WebSite", SqlDbType.NVarChar, WebSite),
        Parameter("@Maximum", SqlDbType.NVarChar, Maximum),
        Parameter("@CreditLimit", SqlDbType.SmallInt, CreditLimit),
         Parameter("@ISCusSupp", SqlDbType.Bit, ISCusSupp),
          Parameter("@SupplierID", SqlDbType.NVarChar, SupplierID),
        Parameter("@Status", SqlDbType.Bit, Status));
    }






    public DataTable NameSearch_Customers(String CustomerName)
    {
        return ExecteRader("Select CustomerID,CustomerName  from Customers where CustomerName=@CustomerName", CommandType.Text,
        Parameter("@CustomerName", SqlDbType.NVarChar, CustomerName));
    }












    //Delete
    public void DeleteCustomers(string CustomerID)
    {
        Execute_SQL("Delete  From Customers where CustomerID=@CustomerID", CommandType.Text,
        Parameter("@CustomerID", SqlDbType.Int, CustomerID));
    }

    //Details ID
    public DataTable NODeleteCustomers(String CustomerID)
    {
        return ExecteRader("Select CustomerID  from Customers_trans Where CustomerID=@CustomerID", CommandType.Text,
        Parameter("@CustomerID", SqlDbType.Int, CustomerID));
    }




    //Details ID
    public DataTable Details_Customers(string CustomerID)
    {
        return ExecteRader("Select * from Customers where CustomerID=@CustomerID", CommandType.Text,
        Parameter("@CustomerID", SqlDbType.Int, CustomerID));
    }



    //Search
    public DataTable Search_Customers(String Search )
    {
        return ExecteRader("Select *  from Customers where convert(nvarchar,CustomerID)+CustomerName+Phone+Phone2 like '%'+ @Search + '%' ", CommandType.Text,
        Parameter("@Search", SqlDbType.NVarChar, Search));
    }






    //Update
    public void Update_Customers(string CustomerID, Double Income, Double Export)
    {
        Execute_SQL("Update Customers Set Income=@Income ,Export=@Export  where CustomerID=@CustomerID", CommandType.Text,
        Parameter("@CustomerID", SqlDbType.NVarChar, CustomerID),
        Parameter("@Income", SqlDbType.Float, Income),
        Parameter("@Export", SqlDbType.Float, Export));
    }


 //Update
    public DataTable RptDetails_Customers(DateTime MyDate, DateTime MyDate2, string CustomerID)
    {
      return  ExecteRader(@"SELECT        dbo.Sales_Details.MainID, dbo.Sales_Details.ProdecutID, dbo.Prodecuts.ProdecutName, dbo.Sales_Details.Unit, dbo.Sales_Details.Quantity, dbo.Sales_Details.ReturnQuantity, dbo.Sales_Details.BayPrice, 
                         dbo.Sales_Details.TotalBayPrice, dbo.Sales_Details.Price, dbo.Sales_Details.Vat, dbo.Sales_Details.MainVat, dbo.Sales_Details.ReturnVat, dbo.Sales_Details.TotalPrice, dbo.Sales_Details.ReturnTotalPrice, 
                         dbo.Sales_Details.ReturnCurrencyVat, dbo.Sales_Details.ReturnCurrencyTotal, dbo.Sales_Details.StoreID, dbo.Stores.StoreName, dbo.Sales_Details.CurrencyPrice, dbo.Sales_Details.CurrencyVat, 
                         dbo.Sales_Details.CurrencyTotal, dbo.Sales_Main.MyDate, dbo.Sales_Main.CustomerID, dbo.UserPermissions.EmpName
FROM            dbo.Sales_Details INNER JOIN
                         dbo.Sales_Main ON dbo.Sales_Details.MainID = dbo.Sales_Main.MainID INNER JOIN
                         dbo.Prodecuts ON dbo.Sales_Details.ProdecutID = dbo.Prodecuts.ProdecutID INNER JOIN
                         dbo.Stores ON dbo.Sales_Details.StoreID = dbo.Stores.StoreID INNER JOIN
                         dbo.UserPermissions ON dbo.Sales_Main.UserAdd = dbo.UserPermissions.ID
Where dbo.Sales_Main.MyDate>= @MyDate And dbo.Sales_Main.MyDate<= @MyDate2  And dbo.Sales_Main.CustomerID=@CustomerID ", CommandType.Text,
        Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@MyDate2", SqlDbType.Date, MyDate2),
        Parameter("@CustomerID", SqlDbType.NVarChar, CustomerID));
    }







}

