﻿
using System;
using System.Data;

class RbtTreasuryMovement_cls : DataAccessLayer
    {

    //Details ID
    public DataTable TreasuryMovement(DateTime VoucherDate, DateTime VoucherDate2)
    {
        return ExecteRader(@" SELECT dbo.TreasuryMovement.*, dbo.UserPermissions.EmpName FROM  dbo.TreasuryMovement INNER JOIN dbo.UserPermissions ON dbo.TreasuryMovement.UserAdd = dbo.UserPermissions.ID
             Where VoucherDate >= @VoucherDate And VoucherDate <= @VoucherDate2", CommandType.Text,
              Parameter("@VoucherDate", SqlDbType.Date, VoucherDate),
            Parameter("@VoucherDate2", SqlDbType.Date, VoucherDate2));
    }

    public DataTable TreasuryMovement(DateTime VoucherDate, DateTime VoucherDate2, string AccountID, string ISCusSupp)
    {
        return ExecteRader(@" SELECT dbo.TreasuryMovement.*, dbo.UserPermissions.EmpName FROM  dbo.TreasuryMovement INNER JOIN dbo.UserPermissions ON dbo.TreasuryMovement.UserAdd = dbo.UserPermissions.ID
             Where VoucherDate >= @VoucherDate And VoucherDate <= @VoucherDate2 and AccountID=@AccountID and ISCusSupp=@ISCusSupp ", CommandType.Text,
              Parameter("@VoucherDate", SqlDbType.Date, VoucherDate),
            Parameter("@VoucherDate2", SqlDbType.Date, VoucherDate2),
             Parameter("@AccountID", SqlDbType.NVarChar, AccountID),
              Parameter("@ISCusSupp", SqlDbType.NVarChar, ISCusSupp));
    }

    public DataTable TreasuryMovement2(DateTime VoucherDate, string AccountID, string ISCusSupp)
    {
        string sql = "select sum(Debit) as Debit , sum(Credit) as Credit from TreasuryMovement where  VoucherDate < @VoucherDate and AccountID=@AccountID and ISCusSupp=@ISCusSupp";

        return ExecteRader(sql, CommandType.Text,
            Parameter("@VoucherDate", SqlDbType.Date, VoucherDate),
                         Parameter("@AccountID", SqlDbType.NVarChar, AccountID),
              Parameter("@ISCusSupp", SqlDbType.NVarChar, ISCusSupp));
    }


    public DataTable TreasuryMovement2( DateTime VoucherDate)
    {
        string sql = "select sum(income) as income , sum(Export) as Export from TreasuryMovement where  VoucherDate < @VoucherDate";

        return ExecteRader(sql, CommandType.Text,
            Parameter("@VoucherDate", SqlDbType.Date, VoucherDate));
    }




}

