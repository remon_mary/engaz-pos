﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;


    class MonyTransfer_cls : DataAccessLayer
    {
        //MaxID
        public String MaxID_MoneyTransfer()
        {
            return Execute_SQL("select ISNULL (MAX(TransferID)+1,1) from MoneyTransfer", CommandType.Text);
        }


        // Insert
        public void Insert_MoneyTransfer(int TransferID, DateTime MyDate, int AccountID_From, int AccountID_To, Double Amount, int CurrencyID, Double CurrencyRate, string Note )
        {
            Execute_SQL("insert into MoneyTransfer(TransferID ,MyDate ,AccountID_From ,AccountID_To ,Amount ,CurrencyID ,CurrencyRate ,Note )Values (@TransferID ,@MyDate ,@AccountID_From ,@AccountID_To ,@Amount ,@CurrencyID ,@CurrencyRate ,@Note )", CommandType.Text,
            Parameter("@TransferID", SqlDbType.Int, TransferID),
            Parameter("@MyDate", SqlDbType.Date, MyDate),
            Parameter("@AccountID_From", SqlDbType.Int, AccountID_From),
            Parameter("@AccountID_To", SqlDbType.Int, AccountID_To),
            Parameter("@Amount", SqlDbType.Float, Amount),
            Parameter("@CurrencyID", SqlDbType.Int, CurrencyID),
            Parameter("@CurrencyRate", SqlDbType.Float, CurrencyRate), 
            Parameter("@Note", SqlDbType.NVarChar, Note));
        }

        //Update
        public void Update_MoneyTransfer(int TransferID, DateTime MyDate, int AccountID_From, int AccountID_To, Double Amount, int CurrencyID, Double CurrencyRate, string Note)
        {
            Execute_SQL("Update MoneyTransfer Set TransferID=@TransferID ,MyDate=@MyDate ,AccountID_From=@AccountID_From ,AccountID_To=@AccountID_To ,Amount=@Amount ,CurrencyID=@CurrencyID ,CurrencyRate=@CurrencyRate ,Note=@Note  where TransferID=@TransferID", CommandType.Text,
            Parameter("@TransferID", SqlDbType.Int, TransferID),
            Parameter("@MyDate", SqlDbType.Date, MyDate),
            Parameter("@AccountID_From", SqlDbType.Int, AccountID_From),
            Parameter("@AccountID_To", SqlDbType.Int, AccountID_To),
            Parameter("@Amount", SqlDbType.Float, Amount),
            Parameter("@CurrencyID", SqlDbType.Int, CurrencyID),
            Parameter("@CurrencyRate", SqlDbType.Float, CurrencyRate),
            Parameter("@Note", SqlDbType.NVarChar, Note));
        }

        //Delete
        public void Delete_MoneyTransfer(int TransferID)
        {
            Execute_SQL("Delete  From MoneyTransfer where TransferID=@TransferID", CommandType.Text,
            Parameter("@TransferID", SqlDbType.Int, TransferID));
        }

        //Details ID
        public DataTable Details_MoneyTransfer(int TransferID)
        {
            return ExecteRader(@"SELECT        dbo.PayType.PayTypeName AS PayTypeNameFrom, PayType_1.PayTypeName AS PayTypeNameTo, dbo.MoneyTransfer.*, dbo.Currency.CurrencyName
FROM            dbo.PayType INNER JOIN
                         dbo.MoneyTransfer ON dbo.PayType.PayTypeID = dbo.MoneyTransfer.AccountID_From INNER JOIN
                         dbo.PayType AS PayType_1 ON dbo.MoneyTransfer.AccountID_To = PayType_1.PayTypeID INNER JOIN
                         dbo.Currency ON dbo.MoneyTransfer.CurrencyID = dbo.Currency.CurrencyID Where TransferID=@TransferID", CommandType.Text,
            Parameter("@TransferID", SqlDbType.Int, TransferID));
        }




            //Search
        public DataTable Search_MoneyTransfer(DateTime MyDate, DateTime MyDate2)
    {
        string sql = @"SELECT        dbo.PayType.PayTypeName AS PayTypeNameFrom, PayType_1.PayTypeName AS PayTypeNameTo, dbo.MoneyTransfer.*, dbo.Currency.CurrencyName
FROM            dbo.PayType INNER JOIN
                         dbo.MoneyTransfer ON dbo.PayType.PayTypeID = dbo.MoneyTransfer.AccountID_From INNER JOIN
                         dbo.PayType AS PayType_1 ON dbo.MoneyTransfer.AccountID_To = PayType_1.PayTypeID INNER JOIN
                         dbo.Currency ON dbo.MoneyTransfer.CurrencyID = dbo.Currency.CurrencyID
        WHERE MyDate>= @MyDate And MyDate<= @MyDate ";
        return ExecteRader(sql, CommandType.Text,
        Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@MyDate2", SqlDbType.Date, MyDate2));
    }




    }

