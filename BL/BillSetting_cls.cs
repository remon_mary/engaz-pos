﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

class BillSetting_cls : DataAccessLayer
{
    public static int SettingID = 1;
    public static Boolean UseStoreDefult = false;
    public static String StoreID = "";
    public static Boolean UseCustomerDefult = false;
    public static String CustomerID = "";
    public static Boolean UsingFastInput = false;
    public static Boolean ShowMessageQty = false;
    public static Boolean ShowMessageSave = false;
    public static int kindPay = 0;
    public static Boolean UseVat = false;
    public static string Vat = "0";
    public static int PrintSize = 1;

    public static Boolean UseCrrencyDefault = false;
    public static string CurrencyID = "";

    public static Boolean NotificationProdect = false;
    public static Boolean NotificationCustomers = false;
    public static string MaxBalance = "0";

    public static Boolean UsekindPay = false;
    public static string PayTypeID = "0";


    public static Boolean UseCategory = false;
    public static string CategoryId = "0";


    public static Boolean UseUnit = false;
    public static string UnitId = "0";


    public static void BillSetting(int SettingID_, Boolean UseStoreDefult_, String StoreID_, Boolean UseCustomerDefult_, String CustomerID_, Boolean UsingFastInput_, Boolean ShowMessageQty_, Boolean ShowMessageSave_, int kindPay_, Boolean UseVat_, string Vat_, int PrintSize_, Boolean UseCrrencyDefault_, string CurrencyID_, Boolean NotificationProdect_, Boolean NotificationCustomers_, String MaxBalance_, Boolean UsekindPay_, string PayTypeID_, Boolean UseCategory_, String CategoryId_, Boolean UseUnit_, string UnitId_)
    {
        SettingID = SettingID_;
        UseStoreDefult = UseStoreDefult_;
        StoreID = StoreID_;
        UseCustomerDefult = UseCustomerDefult_;
        CustomerID = CustomerID_;
        UsingFastInput = UsingFastInput_;
        ShowMessageQty = ShowMessageQty_;
        ShowMessageSave = ShowMessageSave_;
        kindPay = kindPay_;
        UseVat = UseVat_;
        Vat = Vat_;
        PrintSize = PrintSize_;
        UseCrrencyDefault = UseCrrencyDefault_;
        CurrencyID = CurrencyID_;
        NotificationProdect = NotificationProdect_;
        NotificationCustomers = NotificationCustomers_;
        MaxBalance = MaxBalance_;
        UsekindPay = UsekindPay_;
        PayTypeID = PayTypeID_;
        UseCategory = UseCategory_;
        CategoryId = CategoryId_;
        UseUnit = UseUnit_;
        UnitId = UnitId_;

    }





    //Details ID
    public DataTable BillPrint(string MainID)
    {
        return ExecteRader("select * from Bill_Print where MainID=@MainID", CommandType.Text,
        Parameter("@MainID", SqlDbType.NVarChar, MainID));
    }


    //Details ID
    public DataTable BillPrintPurchaseOrder(string MainID)
    {
        return ExecteRader(@"SELECT        dbo.PurchaseOrder_Details.MainID, dbo.Prodecuts.ProdecutName, dbo.PurchaseOrder_Details.Unit, dbo.PurchaseOrder_Details.Quantity, dbo.PurchaseOrder_Details.Price, dbo.PurchaseOrder_Details.Vat, 
                         dbo.PurchaseOrder_Details.TotalPrice
FROM            dbo.PurchaseOrder_Details INNER JOIN
                         dbo.Prodecuts ON dbo.PurchaseOrder_Details.ProdecutID = dbo.Prodecuts.ProdecutID
WHERE        (dbo.PurchaseOrder_Details.MainID =@MainID)", CommandType.Text,
        Parameter("@MainID", SqlDbType.NVarChar, MainID));
    }




}

