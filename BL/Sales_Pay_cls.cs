﻿using System;
using System.Data;
class PayType_trans_cls : DataAccessLayer
    {

    //MaxID
    public String MaxID_PayType_trans()
    {
        return Execute_SQL("select ISNULL (MAX(PayID)+1,1) from PayType_trans", CommandType.Text);
    }


 

    // Insert
    public void Insert_PayType_trans( string MainID,DateTime VoucherDate, string PayTypeID,string VoucherCode, string VoucherType,string TypeID, Double Debit, Double Credit, string Statement,int UserID, string CurrencyID, string CurrencyRate, string CurrencyPrice)
    {
        Execute_SQL("insert into PayType_trans(PayID ,MainID ,VoucherDate,PayTypeID ,VoucherCode,VoucherType,TypeID ,Debit ,Credit ,Statement,UserID,CurrencyID ,CurrencyRate ,CurrencyPrice )Values (@PayID ,@MainID,@VoucherDate ,@PayTypeID,@VoucherCode ,@VoucherType,@TypeID ,@Debit ,@Credit ,@Statement,@UserID,@CurrencyID ,@CurrencyRate ,@CurrencyPrice)", CommandType.Text,
        Parameter("@PayID", SqlDbType.NVarChar, MaxID_PayType_trans()),
        Parameter("@MainID", SqlDbType.BigInt, MainID),
                Parameter("@VoucherDate", SqlDbType.Date, VoucherDate),
        Parameter("@PayTypeID", SqlDbType.Int, PayTypeID),
        Parameter("@VoucherCode", SqlDbType.NVarChar, VoucherCode),
        Parameter("@VoucherType", SqlDbType.NVarChar, VoucherType),
        Parameter("@TypeID", SqlDbType.NVarChar, TypeID),
        Parameter("@Debit", SqlDbType.Float, Debit),
        Parameter("@Credit", SqlDbType.Float, Credit),
        Parameter("@Statement", SqlDbType.NText, Statement),
         Parameter("@UserID", SqlDbType.NVarChar, UserID),
         Parameter("@CurrencyID", SqlDbType.Int, CurrencyID),
Parameter("@CurrencyRate", SqlDbType.Float, CurrencyRate),
Parameter("@CurrencyPrice", SqlDbType.Float, CurrencyPrice));
    }



    //Delete
    public void Delete_PayType_trans(string MainID,string TypeID)
    {
        Execute_SQL("Delete  From PayType_trans where MainID=@MainID and TypeID=@TypeID", CommandType.Text,
        Parameter("@MainID", SqlDbType.BigInt, MainID),
         Parameter("@TypeID", SqlDbType.NVarChar, TypeID));
    }

    //Details ID
    public DataTable Details_PayType_trans(string MainID,string TypeID)
    {
        return ExecteRader(@"SELECT        dbo.PayType_trans.*, dbo.PayType.PayTypeName, dbo.Currency.CurrencyName
FROM            dbo.PayType INNER JOIN
                         dbo.PayType_trans ON dbo.PayType.PayTypeID = dbo.PayType_trans.PayTypeID INNER JOIN
                         dbo.Currency ON dbo.PayType_trans.CurrencyID = dbo.Currency.CurrencyID
Where MainID=@MainID and dbo.PayType_trans.TypeID=@TypeID ", CommandType.Text,
        Parameter("@MainID", SqlDbType.BigInt, MainID),
        Parameter("@TypeID", SqlDbType.NVarChar, TypeID));
    }


}

