﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace ByStro.BL
{
    class Capital_cls : DataAccessLayer
    {
        //MaxID
        public String MaxID_Capital()
        {
            return Execute_SQL("select ISNULL (MAX(Id)+1,1) from Capital", CommandType.Text);
        }

        // Insert
        public void Insert_Capital(string Id, string Notes, string Debit, string Credit)
        {
            Execute_SQL("insert into Capital(Id ,Notes ,Debit ,Credit )Values (@Id ,@Notes ,@Debit ,@Credit )", CommandType.Text,
            Parameter("@Id", SqlDbType.Int, Id),
            Parameter("@Notes", SqlDbType.NVarChar, Notes),
            Parameter("@Debit", SqlDbType.Float, Debit),
            Parameter("@Credit", SqlDbType.Float, Credit));
        }

        //Update
        public void Update_Capital(string Id, string Notes, string Debit, string Credit)
        {
            Execute_SQL("Update Capital Set Id=@Id ,Notes=@Notes ,Debit=@Debit ,Credit=@Credit  where Id=@Id", CommandType.Text,
            Parameter("@Id", SqlDbType.Int, Id),
            Parameter("@Notes", SqlDbType.NVarChar, Notes),
            Parameter("@Debit", SqlDbType.Float, Debit),
            Parameter("@Credit", SqlDbType.Float, Credit));
        }

        //Delete
        public void Delete_Capital(string Id)
        {
            Execute_SQL("Delete  From Capital where Id=@Id", CommandType.Text,
            Parameter("@Id", SqlDbType.Int, Id));
        }

        //Search 
        public DataTable Search__Capital(string Search)
        {
            return ExecteRader("Select *  from Capital Where Notes like '%'+@Search+ '%' ", CommandType.Text,
            Parameter("@Search", SqlDbType.NVarChar, Search));
        }



        //Details ID
        public DataTable Details_Capital(string Id)
        {
            return ExecteRader("Select *  from Capital Where Id=@Id", CommandType.Text,
            Parameter("@Id", SqlDbType.Int, Id));
        }





    }
}
