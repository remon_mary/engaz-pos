﻿using System;
using System.Data;

class Suppliers_Recived_cls : DataAccessLayer
{
    //MaxID
    public String MaxID_Supplier_Recived()
    {
        return Execute_SQL("select ISNULL (MAX(PayID)+1,1) from Supplier_Recived", CommandType.Text);
    }

    // Insert
    public void InsertSupplier_Recived(string PayID, DateTime PayDate, Double PayValue, string Remarks, string SupplierID,String TreasuryID, int UserAdd)
    {
        Execute_SQL("insert into Supplier_Recived(PayID ,PayDate ,PayValue ,Remarks ,SupplierID,TreasuryID ,UserAdd )Values (@PayID,@PayDate ,@PayValue ,@Remarks ,@SupplierID,@TreasuryID ,@UserAdd )", CommandType.Text,
        Parameter("@PayID", SqlDbType.NVarChar, PayID),
        Parameter("@PayDate", SqlDbType.Date, PayDate),
        Parameter("@PayValue", SqlDbType.Float, PayValue),
        Parameter("@Remarks", SqlDbType.NVarChar, Remarks),
        Parameter("@SupplierID", SqlDbType.Int, SupplierID),
        Parameter("@TreasuryID", SqlDbType.NVarChar, TreasuryID),
        Parameter("@UserAdd", SqlDbType.Int, UserAdd));
    }

    //Update
    public void UpdateSupplier_Recived(string PayID, DateTime PayDate, Double PayValue, string Remarks, string SupplierID)
    {
        Execute_SQL("Update Supplier_Recived Set PayID=@PayID,PayDate=@PayDate ,PayValue=@PayValue ,Remarks=@Remarks ,SupplierID=@SupplierID   where PayID=@PayID", CommandType.Text,
        Parameter("@PayID", SqlDbType.NVarChar, PayID),
        Parameter("@PayDate", SqlDbType.Date, PayDate),
        Parameter("@PayValue", SqlDbType.Float, PayValue),
        Parameter("@Remarks", SqlDbType.NText, Remarks),
        Parameter("@SupplierID", SqlDbType.Int, SupplierID));
    }




    //Delete
    public void DeleteSupplier_Recived(string PayID)
    {
        Execute_SQL("Delete  From Supplier_Recived where PayID=@PayID", CommandType.Text,
        Parameter("@PayID", SqlDbType.BigInt, PayID));
    }



    //==================================================================================================================================================================

    //Search
    public DataTable Search_Supplier_Recived(string Search, DateTime PayDate, DateTime PayDate2)
    {
        string sql = @"SELECT        dbo.Supplier_Recived.*, dbo.Suppliers.SupplierName, dbo.UserPermissions.EmpName
FROM            dbo.Suppliers INNER JOIN
                         dbo.Supplier_Recived ON dbo.Suppliers.SupplierID = dbo.Supplier_Recived.SupplierID INNER JOIN
                         dbo.UserPermissions ON dbo.Supplier_Recived.UserAdd = dbo.UserPermissions.ID
        WHERE  PayDate>= @PayDate And PayDate<= @PayDate2 And convert(nvarchar,dbo.Supplier_Recived.PayID)+dbo.Suppliers.SupplierName like '%'+ @Search + '%'";
        return ExecteRader(sql, CommandType.Text,
        Parameter("@Search", SqlDbType.NVarChar, Search),
        Parameter("@PayDate", SqlDbType.Date, PayDate),
        Parameter("@PayDate2", SqlDbType.Date, PayDate2));
    }

    //Details   Where id
    public DataTable Details_Supplier_Recived(String PayID)
    {
        string sql = @"SELECT        dbo.Supplier_Recived.*, dbo.Suppliers.SupplierName
FROM            dbo.Suppliers INNER JOIN
                         dbo.Supplier_Recived ON dbo.Suppliers.SupplierID = dbo.Supplier_Recived.SupplierID
         WHERE  (dbo.Supplier_Recived.PayID=@PayID)";
        return ExecteRader(sql, CommandType.Text,
        Parameter("@PayID", SqlDbType.NVarChar, PayID));
    }




}

