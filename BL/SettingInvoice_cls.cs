﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;


class SettingInvoice_cls : DataAccessLayer
{


    // Insert
    public void InsertSetting(int SettingID, Boolean UseStoreDefault, String StoreID, Boolean UseCustomerDefault, String CustomerID, Boolean UsingFastInput, Boolean ShowMessageQty, Boolean ShowMessageSave, String kindPay, Boolean UseVat, String Vat, int PrintSize, bool UseCrrencyDefault, string CurrencyID, Boolean NotificationProdect, Boolean NotificationCustomers, String MaxBalance, Boolean UsekindPay, String PayTypeID, Boolean UseCategory, String CategoryId, Boolean UseUnit, string UnitId)
    {
        Execute_SQL("insert into Setting(SettingID ,UseStoreDefault ,StoreID ,UseCustomerDefault ,CustomerID ,UsingFastInput ,ShowMessageQty ,ShowMessageSave ,kindPay ,UseVat ,Vat ,PrintSize,UseCrrencyDefault,CurrencyID,NotificationProdect ,NotificationCustomers ,MaxBalance,UsekindPay ,PayTypeID,UseCategory ,CategoryId,UseUnit ,UnitId  )Values (@SettingID ,@UseStoreDefault ,@StoreID ,@UseCustomerDefault ,@CustomerID ,@UsingFastInput ,@ShowMessageQty ,@ShowMessageSave ,@kindPay ,@UseVat ,@Vat,@PrintSize,@UseCrrencyDefault ,@CurrencyID ,@NotificationProdect ,@NotificationCustomers ,@MaxBalance,@UsekindPay ,@PayTypeID,@UseCategory ,@CategoryId,@UseUnit ,@UnitId  )", CommandType.Text,
        Parameter("@SettingID", SqlDbType.Int, SettingID),
        Parameter("@UseStoreDefault", SqlDbType.Bit, UseStoreDefault),
        Parameter("@StoreID", SqlDbType.NVarChar, StoreID),
        Parameter("@UseCustomerDefault", SqlDbType.Bit, UseCustomerDefault),
        Parameter("@CustomerID", SqlDbType.NVarChar, CustomerID),
        Parameter("@UsingFastInput", SqlDbType.Bit, UsingFastInput),
        Parameter("@ShowMessageQty", SqlDbType.Bit, ShowMessageQty),
        Parameter("@ShowMessageSave", SqlDbType.Bit, ShowMessageSave),
        Parameter("@kindPay", SqlDbType.NVarChar, kindPay),
        Parameter("@UseVat", SqlDbType.Bit, UseVat),
        Parameter("@Vat", SqlDbType.NVarChar, Vat),
        Parameter("@PrintSize", SqlDbType.Int, PrintSize),
        Parameter("@UseCrrencyDefault", SqlDbType.Bit, UseCrrencyDefault),
        Parameter("@CurrencyID", SqlDbType.NVarChar, CurrencyID),
         Parameter("@NotificationProdect", SqlDbType.Bit, NotificationProdect),
       Parameter("@NotificationCustomers", SqlDbType.Bit, NotificationCustomers),
         Parameter("@MaxBalance", SqlDbType.NVarChar, MaxBalance),
         Parameter("@UsekindPay", SqlDbType.Bit, UsekindPay),
       Parameter("@PayTypeID", SqlDbType.NVarChar, PayTypeID),
       Parameter("@UseUnit", SqlDbType.Bit, UseUnit),
       Parameter("@UnitId", SqlDbType.NVarChar, UnitId),
       Parameter("@UseCategory", SqlDbType.Bit, UseCategory),
       Parameter("@CategoryId", SqlDbType.NVarChar, CategoryId));
    }



    //Update
    public void UpdateSetting(int SettingID, Boolean UseStoreDefault, String StoreID, Boolean UseCustomerDefault, String CustomerID, Boolean UsingFastInput, Boolean ShowMessageQty, Boolean ShowMessageSave, String kindPay, Boolean UseVat, String Vat, int PrintSize, bool UseCrrencyDefault, string CurrencyID, Boolean NotificationProdect, Boolean NotificationCustomers, String MaxBalance, Boolean UsekindPay, String PayTypeID, Boolean UseCategory, String CategoryId, Boolean UseUnit, string UnitId)
    {
        Execute_SQL("Update Setting Set SettingID=@SettingID ,UseStoreDefault=@UseStoreDefault ,StoreID=@StoreID ,UseCustomerDefault=@UseCustomerDefault ,CustomerID=@CustomerID ,UsingFastInput=@UsingFastInput ,ShowMessageQty=@ShowMessageQty ,ShowMessageSave=@ShowMessageSave ,kindPay=@kindPay ,UseVat=@UseVat ,Vat=@Vat,PrintSize=@PrintSize,UseCrrencyDefault=@UseCrrencyDefault,CurrencyID=@CurrencyID,NotificationProdect=@NotificationProdect ,NotificationCustomers=@NotificationCustomers ,MaxBalance=@MaxBalance,UsekindPay=@UsekindPay ,PayTypeID=@PayTypeID,UseCategory=@UseCategory ,CategoryId=@CategoryId,UseUnit=@UseUnit ,UnitId=@UnitId     where SettingID=@SettingID", CommandType.Text,
        Parameter("@SettingID", SqlDbType.Int, SettingID),
        Parameter("@UseStoreDefault", SqlDbType.Bit, UseStoreDefault),
        Parameter("@StoreID", SqlDbType.NVarChar, StoreID),
        Parameter("@UseCustomerDefault", SqlDbType.Bit, UseCustomerDefault),
        Parameter("@CustomerID", SqlDbType.NVarChar, CustomerID),
        Parameter("@UsingFastInput", SqlDbType.Bit, UsingFastInput),
        Parameter("@ShowMessageQty", SqlDbType.Bit, ShowMessageQty),
        Parameter("@ShowMessageSave", SqlDbType.Bit, ShowMessageSave),
        Parameter("@kindPay", SqlDbType.NVarChar, kindPay),
        Parameter("@UseVat", SqlDbType.Bit, UseVat),
        Parameter("@Vat", SqlDbType.NVarChar, Vat),
        Parameter("@PrintSize", SqlDbType.Int, PrintSize),
         Parameter("@UseCrrencyDefault", SqlDbType.Bit, UseCrrencyDefault),
        Parameter("@CurrencyID", SqlDbType.NVarChar, CurrencyID),
                 Parameter("@NotificationProdect", SqlDbType.Bit, NotificationProdect),
       Parameter("@NotificationCustomers", SqlDbType.Bit, NotificationCustomers),
         Parameter("@MaxBalance", SqlDbType.NVarChar, MaxBalance),
         Parameter("@UsekindPay", SqlDbType.Bit, UsekindPay),
Parameter("@PayTypeID", SqlDbType.NVarChar, PayTypeID),
       Parameter("@UseUnit", SqlDbType.Bit, UseUnit),
       Parameter("@UnitId", SqlDbType.NVarChar, UnitId),
       Parameter("@UseCategory", SqlDbType.Bit, UseCategory),
       Parameter("@CategoryId", SqlDbType.NVarChar, CategoryId));
    }


    //Details ID
    public DataTable Details_Setting()
    {
        return ExecteRader("Select * from Setting where SettingID ='1'", CommandType.Text);
    }






}

