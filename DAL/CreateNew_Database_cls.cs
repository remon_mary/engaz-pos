﻿
using System;
using System.Data.SqlClient;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;

class CreateNew_Database_cls
{

    public void CreateNew_Database(string sqlConnectionString, string DatabaseName, string Path)
    {
        SqlConnection connection = new SqlConnection(sqlConnectionString);
        try
        {


            string script = @"CREATE DATABASE " + DatabaseName + " On primary (Name = " + DatabaseName + ", FileName =N'" + Path + DatabaseName + ".mdf') LOG ON (Name=" + DatabaseName + "_log, FileName=N'" + Path + DatabaseName + "_log.ldf')" + Environment.NewLine + "Go" + Environment.NewLine + "Use " + DatabaseName + Environment.NewLine;

        ////=========================================================================
        FileInfo file = new FileInfo(Application.StartupPath + "\\script.sql");
        script += file.OpenText().ReadToEnd();

            System.Collections.Generic.IEnumerable<string> commandStrings = Regex.Split(script, @"^\s*GO\s*$",
                                     RegexOptions.Multiline | RegexOptions.IgnoreCase);
            using (connection )
            {
                if (connection.State != System.Data.ConnectionState.Open) connection.Open();
                foreach (string commandString in commandStrings)
                {
                    if (commandString.Trim() != "")
                    {
                        using (var command = new SqlCommand(commandString, connection))
                        {
                            //try
                           // {
                                command.ExecuteNonQuery();
                           // }
                           // catch (SqlException ex)
                           // {
                                //string spError = commandString.Length > 100 ? commandString.Substring(0, 100) + " ...\n..." : commandString;
                                //MessageBox.Show(string.Format("Please check the SqlServer script.\nFile: {0} \nLine: {1} \nError: {2} \nSQL Command: \n{3}", Path, ex.LineNumber, ex.Message, spError), "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                               
                            //}
                        }
                    }
                }
             
            }

            //MessageBox.Show("تم انشاء قاعدة البيانات بنجاح", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }
        catch (Exception ex)
        {
            //string spError = commandString.Length > 100 ? commandString.Substring(0, 100) + " ...\n..." : commandString;
            MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);

        }
        finally
        {
            if (connection.State != System.Data.ConnectionState.Closed) connection.Close();
        }

  }





    public void AttathDatabase(string sqlConnectionString,string path,string FileName)
    {

        SqlConnection connection = new SqlConnection(sqlConnectionString);
       
            //int x = FileName.Length - 4;
            //string sub2 = FileName.Substring(0, x);

            string mdf = string.Format(path + ".mdf");
            string log = string.Format(path + "_log.ldf");
            var sections = FileName.Split('\\');

        var databasename = sections[sections.Length - 1];


        int r = databasename.Length - 4;
        string fileName = databasename.Substring(0, r);

        // MessageBox.Show(fileName, "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
        try
            {


                SqlCommand cmd = new SqlCommand("USE [master]EXEC sp_attach_db @dbname = N'" + FileName + "', @filename1 = '" + mdf + "',@filename2 =  '" + log + "'; ", connection);
            if (connection.State != System.Data.ConnectionState.Open) connection.Open();
            cmd.ExecuteNonQuery();
              
       
 

                MessageBox.Show("تم اضافة قاعدة البيانات بنجاح", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);


            }
            catch (Exception ex)
            {
    
                MessageBox.Show(ex.Message);

            }
        finally { if (connection.State != System.Data.ConnectionState.Closed) connection.Close(); }
        }

}

