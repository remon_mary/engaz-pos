﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
namespace ByStro.PL
{
    public partial class WorkPartCompany_frm : Form
    {
        public WorkPartCompany_frm()
        {
            InitializeComponent();
        }

        WorkPartCompany_cls cls = new WorkPartCompany_cls();
        private void Discound_frm_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingForm(this);
            btnNew_Click(null,null);
        }

      

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                txtID.Text = cls.MaxID_WorkPartCompany();
              txtWorkPartCompanyName.Text = "";
              txtNote.Text = "";
              btnSave.Enabled = true;
              btnUpdate.Enabled = false;
              btnDelete.Enabled = false;
              txtWorkPartCompanyName.Focus();

                if (Application.OpenForms["Employee_frm"] != null)
                {
                    ((Employee_frm)Application.OpenForms["Employee_frm"]).LoadData();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                // condetion
                if (txtWorkPartCompanyName.Text.Trim() == "")
                {
                    txtWorkPartCompanyName.BackColor = Color.Pink;
                    txtWorkPartCompanyName.Focus();
                    return;
                }


                // Search where JOB Name
                DataTable DtSearch = cls.NameSearch__WorkPartCompany( txtWorkPartCompanyName.Text.Trim());
                if (DtSearch.Rows.Count > 0)
                {
                    MessageBox.Show("الاسم المدخل موجود مسبقا", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtWorkPartCompanyName.BackColor = Color.Pink;
                    txtWorkPartCompanyName.Focus();
                    return;
                }
      
                txtID.Text = cls.MaxID_WorkPartCompany();
                cls.Insert_WorkPartCompany(txtID.Text, txtWorkPartCompanyName.Text.Trim(), txtNote.Text);
                btnNew_Click(null,null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                // condetion
                if (txtWorkPartCompanyName.Text.Trim() == "")
                {
                    txtWorkPartCompanyName.BackColor = Color.Pink;
                    txtWorkPartCompanyName.Focus();
                    return;
                }
                cls.Update_WorkPartCompany(txtID.Text, txtWorkPartCompanyName.Text.Trim(), txtNote.Text);
                btnNew_Click(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (Mass.Delete()==true)
                {
                    cls.Delete_WorkPartCompany(Convert.ToInt32(txtID.Text));
                    btnNew_Click(null, null);      
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnGetData_Click(object sender, EventArgs e)
        {
       

            try
            {
                WorkPartCompanySearch_frm frm = new WorkPartCompanySearch_frm();
                frm.ShowDialog(this);
                if (frm.loaddata == true)
                {
                    DataTable Dt = cls.Details_WorkPartCompany( frm.ID_Load);
                    txtID.Text = Dt.Rows[0]["WorkPartCompanyID"].ToString();
                    txtWorkPartCompanyName.Text = Dt.Rows[0]["WorkPartCompanyName"].ToString();
                    txtNote.Text = Dt.Rows[0]["Remarks"].ToString();
                    btnSave.Enabled = false;
                    btnUpdate.Enabled =true ;
                    btnDelete.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void AllKeyPress(object sender, KeyPressEventArgs e)
        {
           DataAccessLayer.UseNamberOnly(e);
        }

        private void Discound_frm_KeyDown(object sender, KeyEventArgs e)
        {
            #region "KeyDown"
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    Close();
                }
                if (e.Control == true && e.KeyCode == Keys.N)
                {
                    btnNew_Click(null, null);
                }
                if (e.KeyCode == Keys.F2)
                {
                    if (btnSave.Enabled == true)
                    {
                        btnSave_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F3)
                {
                    if (btnUpdate.Enabled == true)
                    {
                        btnUpdate_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.Delete)
                {
                    if (btnDelete.Enabled == true)
                    {
                        btnDelete_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F5)
                {
                    btnGetData_Click(null, null);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            #endregion

        }

        private void txtPartCompanyName_TextChanged(object sender, EventArgs e)
        {
            txtWorkPartCompanyName.BackColor = Color.White;
        }

      

    }
}
