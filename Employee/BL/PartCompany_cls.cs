﻿using System;
using System.Data;

class PartCompany_cls : DataAccessLayer
    {
    //MaxID
    public String MaxID_PartCompany()
    {
        return Execute_SQL("select ISNULL (MAX(PartCompanyID)+1,1) from PartCompany", CommandType.Text);
    }

    // Insert
    public void InsertPartCompany(String PartCompanyID, String PartCompanyName, String Remarks)
    {
        Execute_SQL("insert into PartCompany(PartCompanyID ,PartCompanyName ,Remarks )Values (@PartCompanyID ,@PartCompanyName ,@Remarks )", CommandType.Text,
        Parameter("@PartCompanyID", SqlDbType.NVarChar, PartCompanyID),
        Parameter("@PartCompanyName", SqlDbType.NVarChar, PartCompanyName),
        Parameter("@Remarks", SqlDbType.NText, Remarks));
    }



    //Update
    public void UpdatePartCompany(string PartCompanyID, String PartCompanyName, string Remarks)
    {
        Execute_SQL("Update PartCompany Set PartCompanyID=@PartCompanyID ,PartCompanyName=@PartCompanyName ,Remarks=@Remarks  where PartCompanyID=@PartCompanyID", CommandType.Text,
        Parameter("@PartCompanyID", SqlDbType.NVarChar, PartCompanyID),
        Parameter("@PartCompanyName", SqlDbType.NVarChar, PartCompanyName),
        Parameter("@Remarks", SqlDbType.NText, Remarks));
    }


    //Delete
    public void Delete_PartCompany(int PartCompanyID)
    {
        Execute_SQL("Delete  From PartCompany where PartCompanyID=@PartCompanyID", CommandType.Text,
        Parameter("@PartCompanyID", SqlDbType.Int, PartCompanyID));
    }

    //Details ID
    public DataTable Details_PartCompany(int PartCompanyID)
    {
        return ExecteRader("Select *  From PartCompany where PartCompanyID=@PartCompanyID", CommandType.Text,
        Parameter("@PartCompanyID", SqlDbType.Int, PartCompanyID));
    }

    //Details ID
    public DataTable NameSearch_PartCompany(String PartCompanyName)
    {
        return ExecteRader("Select PartCompanyName  from PartCompany where PartCompanyName = @PartCompanyName", CommandType.Text,
        Parameter("@PartCompanyName", SqlDbType.NVarChar, PartCompanyName));
    }

    //Search
    public DataTable Search_PartCompany( String Search)
    {
        return ExecteRader("Select *  from PartCompany where convert(nvarchar, PartCompanyID) + PartCompanyName like '%' + @Search + '%'", CommandType.Text,
        Parameter("@Search", SqlDbType.NVarChar, Search));
    }


}

