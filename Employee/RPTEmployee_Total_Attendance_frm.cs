﻿using ByStro.RPT;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class RPTEmployee_Total_Attendance_frm : XtraForm
    {
        public RPTEmployee_Total_Attendance_frm()
        {
            InitializeComponent();
        }
        /*
         
              this.EmployeeName.DataPropertyName = "EmployeeName";
            this.EmployeeName.FillWeight = 167.7984F;
            this.EmployeeName.HeaderText = "اسم الموظف";
            this.EmployeeName.Name = "EmployeeName";
            this.EmployeeName.ReadOnly = true;
            // 
            // TotalDay
            // 
            this.TotalDay.DataPropertyName = "TotalDay";
            this.TotalDay.HeaderText = "أيام العمل";
            this.TotalDay.Name = "TotalDay";
            this.TotalDay.ReadOnly = true;
            // 
            // Hower_Work
            // 
            this.Hower_Work.DataPropertyName = "Hower_Work";
            this.Hower_Work.HeaderText = "عدد ساعات العمل";
            this.Hower_Work.Name = "Hower_Work";
            this.Hower_Work.ReadOnly = true;
             
             */

        Employee_Attendance_cls cls = new Employee_Attendance_cls();

        private void EmployeeAttendance_frm_Load(object sender, EventArgs e)
        {
            gridView1.OptionsBehavior.Editable = false;
            datefrom.DateTime = DateTime.Now;
        }
        private void EmployeeAttendance_frm_KeyDown(object sender, KeyEventArgs e)
        {
         
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if(datefrom.DateTime .Year > 1950)
            {
                using (var db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String))
                {

                    gridControl1.DataSource = (from att in db.Employee_Attendances
                                               where  att.Hower_Come.Value.Date  == datefrom.DateTime.Date 
                                               select new { att.Employee.EmployeeName, att.Hower_Come, att.Hower_Go, att.Hower_Work }).ToList();
                }
            }
            gridView1.Columns["EmployeeName"].Caption = "اسم الموظف";
            gridView1.Columns["Hower_Come"].Caption = "حضور";
            gridView1.Columns["Hower_Go"].Caption = "انصراف";
            gridView1.Columns["Hower_Work"].Caption = "عدد ساعات العمل ";



            
        }
        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridView1.RowCount == 0)
                barButtonItem1_ItemClick(null, null);
            else
                RPT.rpt_GridReport.Print(gridControl1, this.Text ,   " التاريخ :  " + datefrom.Text  , true);
        }
    }
}
