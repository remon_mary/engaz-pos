﻿using ByStro.RPT;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
namespace ByStro.PL
{
    public partial class RptEmployee_Salary_frm : XtraForm 
    {
        public RptEmployee_Salary_frm()
        {
            InitializeComponent();
        }
        public Boolean loaddata = false;
        public Boolean search = false;
        public String SearchType = "ALL";
        Employee_Salary_cls cls = new Employee_Salary_cls();
        private void AccountEnd_Form_Load(object sender, EventArgs e)
        {


            datefrom.DateTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            datetto.DateTime = DateTime.Now.Date;

            using (var db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String))
            {

                Employee_cls Employee_cls = new Employee_cls();
                LookUpEdit_PartID.Properties.DataSource = db.Employees.Select(x => new { x.EmployeeID, x.EmployeeName }).ToList();
                LookUpEdit_PartID.Properties.DisplayMember = "EmployeeName";
                LookUpEdit_PartID.Properties.ValueMember = "EmployeeID";

            }


            // FILL Combo Store item
            gridView1.Columns.Clear();
            int Index = 0;
            foreach (var item in IntColumns())
            {
                gridView1.Columns.Add(item);
                item.VisibleIndex = Index;
                item.OptionsColumn.AllowEdit = false;
                Index++;
            }
        }
        List<GridColumn> IntColumns()
        {


            GridColumn MyDate = new GridColumn();
            GridColumn SalaryID = new GridColumn();
            GridColumn EmployeeName = new GridColumn();
            GridColumn MainSalary = new GridColumn();
            GridColumn Additions = new GridColumn();
            GridColumn Deduction = new GridColumn();
            GridColumn NetSalary = new GridColumn();
            GridColumn EmpName = new GridColumn();




            EmployeeName.FieldName = "EmployeeName";
            EmployeeName.Caption = "اسم الموظف";
            EmployeeName.Name = "EmployeeName";
            EmpName.FieldName = "EmpName";
            EmpName.Caption = "المستخدم";
            EmpName.Name = "EmpName";
            SalaryID.FieldName = "SalaryID";
            SalaryID.Caption = "كود المرتب";
            SalaryID.Name = "SalaryID";
            MainSalary.FieldName = "MainSalary";
            MainSalary.Caption = "الراتب الشهري";
            MainSalary.Name = "MainSalary";
            Additions.FieldName = "Additions";
            Additions.Caption = "الاستحقاقات";
            Additions.Name = "Additions";
            Deduction.FieldName = "Deduction";
            Deduction.Caption = "صافي المرتب";
            Deduction.Name = "Deduction";
            NetSalary.FieldName = "NetSalary";
            NetSalary.Caption = "اسم الموظف";
            NetSalary.Name = "NetSalary";
            MyDate.FieldName = "MyDate";
            MyDate.Caption = "التاريخ";
            MyDate.Name = "MyDate";



            return new List<GridColumn>()
            { MyDate, SalaryID, EmployeeName, MainSalary  ,Additions,Deduction,NetSalary,EmpName};


        }
       

        private void AccountEnd_Form_KeyDown(object sender, KeyEventArgs e)
        {
        
            if (e.KeyCode == Keys.Escape)
            {
                Close();
            }
        }

      

    

       

        

        private void sumSalary()
        {
           double sum = 0;

            for (int i = 0; i < gridView1.RowCount; i++)
            {
                sum += Convert.ToDouble(gridView1.GetRowCellValue(i, "NetSalary"));
            }
            txt_2 .Caption  = sum.ToString();
        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridView1.RowCount == 0)
                barButtonItem1_ItemClick(null, null);
            else
                RPT.rpt_GridReport.Print(gridControl1, this.Text + " : " + LookUpEdit_PartID.Text,

                 " " + stlb1.Caption + " " + txt_1.Caption +
                 " " + stlb2.Caption + " " + txt_2.Caption +
                 " التاريخ :  " + datefrom.Text+ " الي " + datetto .Text
                    , true);
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();
                if (LookUpEdit_PartID .Text.Trim() == "")
                {
                    dt = cls.Search_Employee_Salary(datefrom .DateTime , datetto .DateTime );
                }
                else
                {
                    dt = cls.Search_Employee_Salary(datefrom.DateTime, datetto.DateTime, LookUpEdit_PartID.EditValue.ToString());

                }
                //Load  
                gridControl1 .DataSource = dt;
                txt_1 .Caption  =gridView1.RowCount .ToString();
                sumSalary();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
