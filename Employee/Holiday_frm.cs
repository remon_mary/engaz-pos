﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class Holiday_frm : Form
    {
        public Holiday_frm()
        {
            InitializeComponent();
        }

        HolidayType_cls cls = new HolidayType_cls();
        private void Discound_frm_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingForm(this);
            btnNew_Click(null, null);
        }



        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                txtID.Text = cls.MaxID_EmpHoliday();
                txtHolidayName.Text = "";
                txtDayNumber.Text = "";
                txtNote.Text = "";
                btnSave.Enabled = true;
                btnUpdate.Enabled = false;
                btnDelete.Enabled = false;
                txtHolidayName.Focus();
                if (Application.OpenForms["Employee_Holiday_frm"] != null)
                {
                    ((Employee_Holiday_frm)Application.OpenForms["Employee_Holiday_frm"]).LoadData();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                // condetion
                if (txtHolidayName.Text.Trim() == "")
                {
                    txtHolidayName.BackColor = Color.Pink;
                    txtHolidayName.Focus();
                    return;
                }
                // condetion
                if (txtDayNumber.Text.Trim() == "")
                {
                    txtDayNumber.BackColor = Color.Pink;
                    txtDayNumber.Focus();
                    return;
                }

                // Search where Part Company Name
                DataTable DtSearch = cls.NameSearch_EmpHoliday(txtHolidayName.Text.Trim());
                if (DtSearch.Rows.Count > 0)
                {
                    MessageBox.Show("الاسم المدخل موجود مسبقا", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtHolidayName.Focus();
                    return;
                }

                txtID.Text = cls.MaxID_EmpHoliday();
                cls.Insert_EmpHoliday(Convert.ToInt32(txtID.Text), txtHolidayName.Text.Trim(), txtDayNumber.Text, txtNote.Text);
                // Mass.Saved();
                btnNew_Click(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                // condetion
                if (txtHolidayName.Text.Trim() == "")
                {
                    txtHolidayName.BackColor = Color.Pink;
                    txtHolidayName.Focus();
                    return;
                }
                // condetion
                if (txtDayNumber.Text.Trim() == "")
                {
                    txtDayNumber.BackColor = Color.Pink;
                    txtDayNumber.Focus();
                    return;
                }
                cls.Update_EmpHoliday(Convert.ToInt32(txtID.Text), txtHolidayName.Text, txtDayNumber.Text, txtNote.Text);
                // Mass.Saved();
                btnNew_Click(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (cls.NODelete_Employee_Holiday(txtID.Text).Rows.Count > 0)
                {
                    Mass.NoDelete();
                    return;
                }


                if (Mass.Delete() == true)
                {
                    cls.Delete_EmpHoliday(Convert.ToInt32(txtID.Text));
                    btnNew_Click(null, null);
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnGetData_Click(object sender, EventArgs e)
        {


            try
            {
                HolidaySearch_frm frm = new HolidaySearch_frm();
                frm.ShowDialog(this);
                if (frm.loaddata == true)
                {
                    DataTable Dt = cls.Details_EmpHoliday(int.Parse(frm.ID_Load));
                    txtID.Text = Dt.Rows[0]["HolidayID"].ToString();
                    txtHolidayName.Text = Dt.Rows[0]["HolidayName"].ToString();
                    txtDayNumber.Text = Dt.Rows[0]["DayNumber"].ToString();
                    txtNote.Text = Dt.Rows[0]["Note"].ToString();
                    btnSave.Enabled = false;
                    btnUpdate.Enabled = true;
                    btnDelete.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void AllKeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

        private void Discound_frm_KeyDown(object sender, KeyEventArgs e)
        {
            #region "KeyDown"
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    Close();
                }
                if (e.Control == true && e.KeyCode == Keys.N)
                {
                    btnNew_Click(null, null);
                }
                if (e.KeyCode == Keys.F2)
                {
                    if (btnSave.Enabled == true)
                    {
                        btnSave_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F3)
                {
                    if (btnUpdate.Enabled == true)
                    {
                        btnUpdate_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.Delete)
                {
                    if (btnDelete.Enabled == true)
                    {
                        btnDelete_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F5)
                {
                    btnGetData_Click(null, null);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            #endregion

        }

        private void txtPartCompanyName_TextChanged(object sender, EventArgs e)
        {
            txtHolidayName.BackColor = Color.White;
        }

        private void txtDayNumber_TextChanged(object sender, EventArgs e)
        {
            txtDayNumber.BackColor = Color.White;

        }

        private void txtDayNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }



    }
}
