﻿using ByStro.RPT;
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class Employee_Salary_frm : Form
    {
        public Employee_Salary_frm()
        {
            InitializeComponent();
        }
        Employee_Salary_cls cls = new Employee_Salary_cls();
        TreasuryMovement_cls cls_Treasury_Movement = new TreasuryMovement_cls();
        private void Employee_Salery_frm_Load(object sender, EventArgs e)
        {
          
            ERB_Setting.SettingForm(this);
            btnNew_Click(null, null);
        }

        private void butEmp_Click(object sender, EventArgs e)
        {

            try
            {
                EmployeeSearch_frm frm = new EmployeeSearch_frm();
                frm.ShowDialog(this);
                if (frm.loaddata == true)
                {
                    txtEmployeeName.BackColor = Color.WhiteSmoke;
                    txtEmployeeID.Text = frm.DGV1.CurrentRow.Cells["EmployeeID"].Value.ToString();
                    txtEmployeeName.Text = frm.DGV1.CurrentRow.Cells["EmployeeName"].Value.ToString();
                    txtSalary.Text = frm.DGV1.CurrentRow.Cells["Salary"].Value.ToString();
                    Additions();
                    Deduction();
                    Salary();
                }
            }
            catch (Exception ex)
            {
                txtEmployeeID.Text = "";
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        DataTable dt_Additions = new DataTable();
        private void Additions()
        {
            DataTable dt = cls.Search_Salary_Employee_Additions(txtEmployeeID.Text, dateTimePicker1.Value);
            dt_Additions = cls.Print_Employee_Additions(txtEmployeeID.Text, dateTimePicker1.Value);
            txtAdditions.Text = dt.Rows[0][0].ToString();

        }


        DataTable dt_Deduction = new DataTable();
        private void Deduction()
        {
            DataTable dt = cls.Search_Salary_Employee_Deduction(txtEmployeeID.Text, dateTimePicker1.Value);
            dt_Deduction = cls.Print_Salary_Employee_Deduction(txtEmployeeID.Text, dateTimePicker1.Value);
            txtDeduction.Text = dt.Rows[0][0].ToString();
        }




        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            Additions();
            Deduction();
            Salary();
        }

        private void Salary()
        {
            Double Additions = Convert.ToDouble(txtAdditions.Text);
            Double Deduction = Convert.ToDouble(txtDeduction.Text);
            Double MainSalary = Convert.ToDouble(txtSalary.Text);
            Double NetSalary = (Additions + MainSalary) - Deduction;
            txtNetSalary.Text = NetSalary.ToString();
        }

     //   private void btnPrint_Click(object sender, EventArgs e)
     //   {
     //    
     //           if (txtNetSalary.Text == "0")
     //           {
     //               return;
     //           }
     //           Employee_cls Emp_cls = new Employee_cls();
     //           DataTable dt_Emp = Emp_cls.Details_Employees(txtEmployeeID.Text);
     //           DataTable dt_Additions_Deduction = cls.Print_Salary_Additions_Deduction();
     //           dt_Additions_Deduction.Clear();

     //           int Additions = dt_Additions.Rows.Count;
     //           int Deduction = dt_Deduction.Rows.Count;

     //           if (Additions >= Deduction)
     //           {
     //               for (int i = 0; i < dt_Additions.Rows.Count; i++)
     //               {
     //                   dynamic DR = dt_Additions_Deduction.NewRow();
     //                   DR["AdditionsType_Name"] = dt_Additions.Rows[i]["AdditionsType_Name"].ToString();
     //                   DR["Addition_Value"] = dt_Additions.Rows[i]["Addition_Value"].ToString();
     //                   if (Deduction - 1 >= i)
     //                   {
     //                       DR["DeductionType_Name"] = dt_Deduction.Rows[i]["DeductionType_Name"].ToString();
     //                       DR["Deduction_Value"] = dt_Deduction.Rows[i]["Deduction_Value"].ToString();
     //                   }
     //                   else
     //                   {
     //                       DR["DeductionType_Name"] = null;
     //                       DR["Deduction_Value"] = DBNull.Value;
     //                   }
     //                   dt_Additions_Deduction.Rows.Add(DR);
     //               }

     //           }
     //           else if (Additions <= Deduction)
     //           {

     //               //**************************************************************************
     //               for (int i = 0; i < dt_Deduction.Rows.Count; i++)
     //               {
     //                   dynamic DR = dt_Additions_Deduction.NewRow();
     //                   DR["DeductionType_Name"] = dt_Deduction.Rows[i]["DeductionType_Name"].ToString();
     //                   DR["Deduction_Value"] = dt_Deduction.Rows[i]["Deduction_Value"].ToString();
     //                   if (Deduction - 1 >= i)
     //                   {

     //                       DR["AdditionsType_Name"] = dt_Additions.Rows[i]["AdditionsType_Name"].ToString();
     //                       DR["Addition_Value"] = dt_Additions.Rows[i]["Addition_Value"].ToString();
     //                   }
     //                   else
     //                   {
     //                       DR["AdditionsType_Name"] = null;
     //                       DR["Addition_Value"] = DBNull.Value;
     //                   }
     //                   dt_Additions_Deduction.Rows.Add(DR);
     //               }


     //           }



     //           string Total_Hower = Total_Hower_Work(txtEmployeeID.Text);




     //           Reportes_Form frm = new Reportes_Form();
     //           CrySalary_Employee report = new CrySalary_Employee();
     //           report.Database.Tables["Additions_Deduction"].SetDataSource(dt_Additions_Deduction);
     //           frm.crystalReportViewer1.ReportSource = report;
     //           report.SetParameterValue(0, dt_Emp.Rows[0]["EmployeeName"].ToString());
     //           report.SetParameterValue(1, dt_Emp.Rows[0]["WorkPartCompanyName"].ToString());
     //           report.SetParameterValue(2, dt_Emp.Rows[0]["PartCompanyName"].ToString());
     //           report.SetParameterValue(3, dt_Emp.Rows[0]["Salary"].ToString());
     //           report.SetParameterValue(4, dateTimePicker1.Text);
     //           report.SetParameterValue(5, txtAdditions.Text);
     //           report.SetParameterValue(6, txtDeduction.Text);
     //           report.SetParameterValue(7, txtNetSalary.Text);
     //           report.SetParameterValue(8, Total_Day.ToString());
     //           report.SetParameterValue(9, Total_Hower);
     //           report.SetParameterValue(10, cls.Search_NumberHoliday(txtEmployeeID.Text, dateTimePicker1.Value).Rows[0][0].ToString());
     //           frm.crystalReportViewer1.Refresh();
     //           frm.ShowDialog();
     //

     //   }


        int Total_Day = 0;
        public string Total_Hower_Work(string EmployeeID)
        {
            Employee_Attendance_cls Emp_Attendance_cls = new Employee_Attendance_cls();

            string Hower_Work = "00:00:00";
            Total_Day = 0;
            DataTable dt = Emp_Attendance_cls.Search_RPT_Employee_Attendance(EmployeeID, dateTimePicker1.Value, false);

            TimeSpan Time_Span = TimeSpan.Parse("00:00:00");
            for (int k = 0; k < dt.Rows.Count; k++)
            {
                Total_Day += 1;
                Time_Span += TimeSpan.Parse(dt.Rows[k]["Hower_Work"].ToString());
            }
            return Hower_Work = Time_Span.ToString();
        }




        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        String TreasuryID = "";
        private void button1_Click(object sender, EventArgs e)
        {

            try
            {
                if (txtEmployeeID.Text.Trim() == "")
                {
                    txtEmployeeName.BackColor = Color.Pink;
                    butEmp.Focus();
                    return;
                }
                else if (txtNetSalary.Text == "" || txtNetSalary.Text == "0")
                {
                    MessageBox.Show("لا يوجد مرتب  ", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }

                txtSalaryID.Text = cls.MaxID_Employee_Salary();
                DataTable dt_Search = cls.Name_EmployeeSalary(txtEmployeeID.Text, dateTimePicker1.Value);
                if (dt_Search.Rows.Count > 0)
                {
                    MessageBox.Show("تم تسجيل المرتب لهذ الموظف ", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                TreasuryID = cls_Treasury_Movement.MaxID_TreasuryMovement();
                cls_Treasury_Movement.InsertTreasuryMovement(TreasuryID,txtEmployeeID.Text,"Emp", txtSalaryID.Text, txtTypeID.Text, this.Text, dateTimePicker1.Value, " دفع راتب : "+txtEmployeeName.Text, "0", txtNetSalary.Text,"","0","0", Properties.Settings.Default.UserID);
                cls.Insert_Employee_Salary(txtSalaryID.Text, dateTimePicker1.Value, txtEmployeeID.Text, txtSalary.Text, txtAdditions.Text, txtDeduction.Text, txtNetSalary.Text, TreasuryID, Properties.Settings.Default.UserID);
                btnNew_Click(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtEmployeeID.Text.Trim() == "")
                {
                    txtEmployeeName.BackColor = Color.Pink;
                    butEmp.Focus();
                    return;
                }
                else if (txtNetSalary.Text == "" || txtNetSalary.Text == "0")
                {
                    MessageBox.Show("لا يوجد مرتب  ", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }

                cls.Update_Employee_Salary(txtSalaryID.Text, dateTimePicker1.Value, txtEmployeeID.Text, txtSalary.Text, txtAdditions.Text, txtDeduction.Text, txtNetSalary.Text);
                cls_Treasury_Movement.UpdateTreasuryMovement(TreasuryID, "", "NO", txtSalaryID.Text, txtTypeID.Text, this.Text, dateTimePicker1.Value, " دفع راتب : " + txtEmployeeName.Text, "0", txtNetSalary.Text, "", "0", "0");
                btnNew_Click(null, null);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (Mass.Delete() == true)
                {
                    cls.Delete_Employee_Salary(txtSalaryID.Text);
                    cls_Treasury_Movement.DeleteTreasuryMovement(TreasuryID);
                    btnNew_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                txtSalaryID.Text = cls.MaxID_Employee_Salary();
                txtEmployeeID.Text = "";
                txtEmployeeName.Text = "";
                txtSalary.Text = "0";
                txtAdditions.Text = "0";
                txtDeduction.Text = "0";
                txtNetSalary.Text = "0";
                dateTimePicker1.Value = DateTime.Now;
                btnSave.Enabled = true;
                btnUpdate.Enabled = false;
                btnDelete.Enabled = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnGetData_Click(object sender, EventArgs e)
        {
            try
            {
                Employee_Salary_Search_frm frm = new Employee_Salary_Search_frm();

                frm.ShowDialog(this);
                if (frm.loaddata == true)
                {
                    DataTable Dt = cls.Details_Employee_Salary(frm.DGV1.CurrentRow.Cells["SalaryID"].Value.ToString());
                    txtSalaryID.Text = Dt.Rows[0]["SalaryID"].ToString();
                    txtEmployeeID.Text = Dt.Rows[0]["EmployeeID"].ToString();
                    txtEmployeeName.Text = Dt.Rows[0]["EmployeeName"].ToString();
                    txtSalary.Text = Dt.Rows[0]["MainSalary"].ToString();
                    txtAdditions.Text = Dt.Rows[0]["Additions"].ToString();
                    txtDeduction.Text = Dt.Rows[0]["Deduction"].ToString();
                    txtNetSalary.Text = Dt.Rows[0]["NetSalary"].ToString();
                    TreasuryID = Dt.Rows[0]["TreasuryID"].ToString();
                    btnSave.Enabled = false;
                    btnUpdate.Enabled = true;
                    btnDelete.Enabled = true;
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



        }
    }
}
