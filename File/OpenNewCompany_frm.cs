﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class OpenNewCompany_frm : Form
    {
        public OpenNewCompany_frm()
        {
            InitializeComponent();
        }
        public Boolean Saved = false;
        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }
        int Nuimber = 1;
        private void CreateNewCompany_frm_Load(object sender, EventArgs e)
        {
            LoadServer(combServerName);
            panel_SelectDatabase.BringToFront();
            btnBake.Enabled = false;
            btnSave.Enabled = false;
            btnNext.Enabled = true;
            comboBox1.SelectedIndex = 0;
        }





        public void LoadServer(ComboBox cmbServer)
        {
            try
            {
                cmbServer.Items.Clear();
                Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Microsoft SQL Server");
                String[] names = (String[])key.GetValue("InstalledInstances");
                if (names.Length > 0)
                {
                    cmbServer.Items.Clear();
                    cmbServer.Items.Add(".");
                    foreach (string item in names)
                    {
                        if (item == "MSSQLSERVER")
                            cmbServer.Items.Add(Environment.MachineName);
                        else
                            cmbServer.Items.Add(Environment.MachineName + "\\" + item);
                    }
                    cmbServer.SelectedIndex = 0;
                }

            }
            catch
            {
                cmbServer.Items.Add(".");
                cmbServer.Items.Add(Environment.MachineName);
                //  MessageBox.Show(ex.Message, "Alert", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                return;
            }

        }








        private void button1_Click(object sender, EventArgs e)
        {



            #region "radioButton1"

            if (Nuimber == 1)
            {
                #region "Condetion"
                if (txtPathAttachDatabase.Text.Trim() == "")
                {
                    MessageBox.Show(" من فضلك حدد ملف قاعدة البيانات ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                #endregion
                panel_LastPage.BringToFront();
                Nuimber = 2;
                btnSave.Enabled = true;
                btnNext.Enabled = false;
                btnBake.Enabled = true;

            }


            #endregion






            //===================================================================================================================================================================================






        }

        private void btnBake_Click(object sender, EventArgs e)
        {


            #region "radioButton1"

            if (Nuimber == 2)
            {
                panel_SelectDatabase.BringToFront();
                Nuimber = 1;
                btnBake.Enabled = false;

            }

            #endregion



            btnSave.Enabled = false;
            btnNext.Enabled = true;


        }






        private void SaveSetting(Boolean Mode, string SQLUserName, string SQLPassword, string ServerName, string Databasename)
        {
            var p = Properties.Settings.Default;
            string Connection_String = "";
            if (Mode == false)
            {
                Connection_String = "Data Source=" + combServerName.Text + ";Initial Catalog=" + Databasename + ";Integrated security=true";
            }
            else
            {
                Connection_String = "Data Source=" + combServerName.Text + ",1433;Network Library=DBMSSOCN;Initial Catalog =" + Databasename + ";User ID='" + txt_name.Text + "' ;Password='" + txt_password.Text + "'";

            }

            p.SQLUserName = SQLUserName;
            p.SQLPassword = SQLPassword;
            p.ServerName = ServerName;
            p.Databasename = Databasename;
            p.Connection_String = Connection_String;
            p.Mode = Mode;
            p.Save();


        }







        private void CreateIfMissing(string path)
        {
            bool folderExists = Directory.Exists(path);
            if (!folderExists)
                Directory.CreateDirectory(path);
        }



        private void lblLoadServer_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

            LoadServer(combServerName);
        }



        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex == 1)
            {
                txt_name.Enabled = true;
                txt_password.Enabled = true;
            }
            else
            {
                txt_name.BackColor = Color.White;
                txt_password.BackColor = Color.White;
                txt_name.Enabled = false;
                txt_password.Enabled = false;
            }
        }

        private void txt_name_TextChanged(object sender, EventArgs e)
        {
            txt_name.BackColor = Color.White;

        }

        private void txt_password_TextChanged(object sender, EventArgs e)
        {
            txt_password.BackColor = Color.White;
        }










        SqlConnection conn = new SqlConnection();
        string SqlConnectionString = "";
        bool Mode;
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {


                CreateNew_Database_cls CreateNew_Database_cls = new CreateNew_Database_cls();
                #region MyRegion
                if (combServerName.Text == "")
                {
                    combServerName.BackColor = Color.Pink;
                    combServerName.Focus();
                    return;
                }


                if (comboBox1.SelectedIndex == 1)
                {
                    if (txt_name.Text == "")
                    {
                        txt_name.BackColor = Color.Pink;
                        txt_name.Focus();
                        return;
                    }
                    if (txt_password.Text == "")
                    {
                        txt_password.BackColor = Color.Pink;
                        txt_password.Focus();
                        return;
                    }
                }
                #endregion


                if (comboBox1.SelectedIndex == 0)
                {
                    SqlConnectionString = "Data Source=" + combServerName.Text + ";Initial Catalog=master;Integrated security=true";
                    Mode = false;
                }
                else
                {
                    SqlConnectionString = "Data Source=" + combServerName.Text + ",1433;Network Library=DBMSSOCN;Initial Catalog =master;User ID='" + txt_name.Text + "' ;Password='" + txt_password.Text + "'";
                    Mode = true;
                }


                conn = new SqlConnection(SqlConnectionString);



                // create database
                string DatabaseName = System.IO.Path.GetFileName(txtPathAttachDatabase.Text);
                AttathDatabase(txtPathAttachDatabase.Text, DatabaseName);


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        public void AttathDatabase(string path, string FileName)
        {

            string mdf = string.Format(path + ".mdf");
            string log = string.Format(path + "_log.ldf");
            try
            {

                SqlCommand cmd = new SqlCommand("USE [master]EXEC sp_attach_db @dbname = N'" + FileName + "', @filename1 = '" + mdf + "',@filename2 =  '" + log + "'; ", conn);
                if (conn.State != System.Data.ConnectionState.Open) conn.Open();
                cmd.ExecuteNonQuery();
                SaveSetting(Mode, txt_name.Text, txt_password.Text, combServerName.Text, FileName);
                MessageBox.Show("تم حفظ الاعدادات بنجاح ", "حفظ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Saved = true;
                this.Close();

            }
            catch (Exception ex)
            {
                Saved = false;
                MessageBox.Show(ex.Message + "\nPemissions for Authenticated Users Security Fille Database \nChange pemissions\n Pemissions for Authenticated Users Is Deny ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            finally
            {
                if (conn.State != System.Data.ConnectionState.Closed) conn.Close();
            }
        }






        private void button6_Click(object sender, EventArgs e)
        {
            try
            {

                OpenFileDialog ofd = new OpenFileDialog();
                ofd.Filter = "Select Database | *.mdf";
                ofd.Title = "اختر قاعدة البيانات";
                if (ofd.ShowDialog() == DialogResult.OK)
                {

                    string input = ofd.FileName;
                    int x = input.Length - 4;
                    string sub2 = input.Substring(0, x);
                    txtPathAttachDatabase.Text = sub2.ToString();
                    string mdf = string.Format(sub2 + ".mdf");
                    string log = string.Format(sub2 + "_log.ldf");
                    var sections = input.Split('\\');
                    var databasename = sections[sections.Length - 1];


                    int r = databasename.Length - 4;
                    string fileName = databasename.Substring(0, r);

                }

            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                Close();
                MessageBox.Show(ex.Message);

            }
        }




    }
}
