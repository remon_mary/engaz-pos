﻿namespace ByStro.PL
{
    partial class OpenNewCompany_frm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OpenNewCompany_frm));
            this.btnNext = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnBake = new System.Windows.Forms.Button();
            this.panel_LastPage = new System.Windows.Forms.Panel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txt_password = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txt_name = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.combServerName = new System.Windows.Forms.ComboBox();
            this.lblLoadServer = new System.Windows.Forms.LinkLabel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel_SelectDatabase = new System.Windows.Forms.Panel();
            this.button6 = new System.Windows.Forms.Button();
            this.txtPathAttachDatabase = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel_LastPage.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel_SelectDatabase.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(97, 314);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(91, 28);
            this.btnNext.TabIndex = 2;
            this.btnNext.Text = "التالي";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(191, 314);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(93, 28);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "حفظ";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnBake
            // 
            this.btnBake.Location = new System.Drawing.Point(4, 314);
            this.btnBake.Name = "btnBake";
            this.btnBake.Size = new System.Drawing.Size(91, 28);
            this.btnBake.TabIndex = 4;
            this.btnBake.Text = "رجوع";
            this.btnBake.UseVisualStyleBackColor = true;
            this.btnBake.Click += new System.EventHandler(this.btnBake_Click);
            // 
            // panel_LastPage
            // 
            this.panel_LastPage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_LastPage.Controls.Add(this.groupBox4);
            this.panel_LastPage.Controls.Add(this.groupBox1);
            this.panel_LastPage.Controls.Add(this.pictureBox1);
            this.panel_LastPage.Location = new System.Drawing.Point(1, 2);
            this.panel_LastPage.Name = "panel_LastPage";
            this.panel_LastPage.Size = new System.Drawing.Size(634, 306);
            this.panel_LastPage.TabIndex = 19;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.comboBox1);
            this.groupBox4.Controls.Add(this.label22);
            this.groupBox4.Controls.Add(this.txt_password);
            this.groupBox4.Controls.Add(this.label24);
            this.groupBox4.Controls.Add(this.txt_name);
            this.groupBox4.Controls.Add(this.label23);
            this.groupBox4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox4.Font = new System.Drawing.Font("Tahoma", 10F);
            this.groupBox4.Location = new System.Drawing.Point(80, 170);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(445, 120);
            this.groupBox4.TabIndex = 456;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "صلاحيات الاتصال بالسيرفر";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Windows Authentication",
            "SQL Server Authentication"});
            this.comboBox1.Location = new System.Drawing.Point(15, 25);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(291, 23);
            this.comboBox1.TabIndex = 28;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label22.Location = new System.Drawing.Point(317, 58);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(110, 17);
            this.label22.TabIndex = 3;
            this.label22.Text = "اسم المستخدم :";
            // 
            // txt_password
            // 
            this.txt_password.Enabled = false;
            this.txt_password.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_password.Location = new System.Drawing.Point(15, 85);
            this.txt_password.Name = "txt_password";
            this.txt_password.PasswordChar = '*';
            this.txt_password.Size = new System.Drawing.Size(291, 24);
            this.txt_password.TabIndex = 9;
            this.txt_password.TextChanged += new System.EventHandler(this.txt_password_TextChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label24.Location = new System.Drawing.Point(317, 28);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(79, 17);
            this.label24.TabIndex = 29;
            this.label24.Text = "نوع الاتصال :";
            // 
            // txt_name
            // 
            this.txt_name.Enabled = false;
            this.txt_name.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_name.Location = new System.Drawing.Point(15, 54);
            this.txt_name.Name = "txt_name";
            this.txt_name.Size = new System.Drawing.Size(291, 24);
            this.txt_name.TabIndex = 8;
            this.txt_name.TextChanged += new System.EventHandler(this.txt_name_TextChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label23.Location = new System.Drawing.Point(317, 88);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(83, 17);
            this.label23.TabIndex = 4;
            this.label23.Text = "كلمة المرور :";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.combServerName);
            this.groupBox1.Controls.Add(this.lblLoadServer);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 10F);
            this.groupBox1.Location = new System.Drawing.Point(80, 91);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(445, 71);
            this.groupBox1.TabIndex = 455;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "اختر السيرفر";
            // 
            // combServerName
            // 
            this.combServerName.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combServerName.FormattingEnabled = true;
            this.combServerName.Location = new System.Drawing.Point(15, 28);
            this.combServerName.Name = "combServerName";
            this.combServerName.Size = new System.Drawing.Size(291, 23);
            this.combServerName.TabIndex = 450;
            this.combServerName.Text = ".";
            // 
            // lblLoadServer
            // 
            this.lblLoadServer.AutoSize = true;
            this.lblLoadServer.Location = new System.Drawing.Point(317, 31);
            this.lblLoadServer.Name = "lblLoadServer";
            this.lblLoadServer.Size = new System.Drawing.Size(93, 17);
            this.lblLoadServer.TabIndex = 454;
            this.lblLoadServer.TabStop = true;
            this.lblLoadServer.Text = "اسم السيرفر :";
            this.lblLoadServer.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblLoadServer_LinkClicked);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::ByStro.Properties.Resources._2e1ax_default_entry_SQLServer_20140326_103734_1;
            this.pictureBox1.Location = new System.Drawing.Point(80, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(445, 79);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 457;
            this.pictureBox1.TabStop = false;
            // 
            // panel_SelectDatabase
            // 
            this.panel_SelectDatabase.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_SelectDatabase.Controls.Add(this.label3);
            this.panel_SelectDatabase.Controls.Add(this.button6);
            this.panel_SelectDatabase.Controls.Add(this.txtPathAttachDatabase);
            this.panel_SelectDatabase.Controls.Add(this.label6);
            this.panel_SelectDatabase.Location = new System.Drawing.Point(1, 2);
            this.panel_SelectDatabase.Name = "panel_SelectDatabase";
            this.panel_SelectDatabase.Size = new System.Drawing.Size(634, 306);
            this.panel_SelectDatabase.TabIndex = 20;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(45, 166);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(31, 26);
            this.button6.TabIndex = 13;
            this.button6.Text = "...";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // txtPathAttachDatabase
            // 
            this.txtPathAttachDatabase.Location = new System.Drawing.Point(78, 168);
            this.txtPathAttachDatabase.Name = "txtPathAttachDatabase";
            this.txtPathAttachDatabase.ReadOnly = true;
            this.txtPathAttachDatabase.Size = new System.Drawing.Size(410, 24);
            this.txtPathAttachDatabase.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(47, 86);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(551, 17);
            this.label6.TabIndex = 11;
            this.label6.Text = "سـوف يقوم البرنامج باستعـراض ملف الكمبيوتر ... من فضلك حدد ملف قاعدة البيانات \r\n";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(492, 171);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(129, 17);
            this.label3.TabIndex = 14;
            this.label3.Text = "مسار قاعدة البيانات :";
            // 
            // OpenNewCompany_frm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(636, 344);
            this.Controls.Add(this.btnBake);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.panel_LastPage);
            this.Controls.Add(this.panel_SelectDatabase);
            this.Font = new System.Drawing.Font("Tahoma", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "OpenNewCompany_frm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "فتــح شــركة موجـودة ";
            this.Load += new System.EventHandler(this.CreateNewCompany_frm_Load);
            this.panel_LastPage.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel_SelectDatabase.ResumeLayout(false);
            this.panel_SelectDatabase.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnBake;
        private System.Windows.Forms.Panel panel_LastPage;
        private System.Windows.Forms.GroupBox groupBox4;
        internal System.Windows.Forms.ComboBox comboBox1;
        internal System.Windows.Forms.Label label22;
        internal System.Windows.Forms.TextBox txt_password;
        internal System.Windows.Forms.Label label24;
        internal System.Windows.Forms.TextBox txt_name;
        internal System.Windows.Forms.Label label23;
        private System.Windows.Forms.GroupBox groupBox1;
        internal System.Windows.Forms.ComboBox combServerName;
        private System.Windows.Forms.LinkLabel lblLoadServer;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel_SelectDatabase;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.TextBox txtPathAttachDatabase;
        private System.Windows.Forms.Label label6;
    }
}