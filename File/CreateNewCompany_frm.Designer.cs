﻿namespace ByStro.PL
{
    partial class CreateNewCompany_frm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreateNewCompany_frm));
            this.btnNext = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnBake = new System.Windows.Forms.Button();
            this.panel_CompanyData = new System.Windows.Forms.Panel();
            this.label27 = new System.Windows.Forms.Label();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.txtCombanyDescriptone = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.Label14 = new System.Windows.Forms.Label();
            this.txtPhoneNamber = new System.Windows.Forms.TextBox();
            this.txtCombanyName = new System.Windows.Forms.TextBox();
            this.Label12 = new System.Windows.Forms.Label();
            this.Label16 = new System.Windows.Forms.Label();
            this.Label13 = new System.Windows.Forms.Label();
            this.txtMyName = new System.Windows.Forms.TextBox();
            this.panel_User = new System.Windows.Forms.Panel();
            this.label26 = new System.Windows.Forms.Label();
            this.txtConfirmPassword = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtEmployee = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtUserPassword = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.panel_PathSave = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.txtDatabaseCreate = new System.Windows.Forms.TextBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.btnBroser = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.txtPathSaveDatabase = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.panel_Start = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel_LastPage = new System.Windows.Forms.Panel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txt_password = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txt_name = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.combServerName = new System.Windows.Forms.ComboBox();
            this.lblLoadServer = new System.Windows.Forms.LinkLabel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel_CompanyData.SuspendLayout();
            this.panel_User.SuspendLayout();
            this.panel_PathSave.SuspendLayout();
            this.panel_Start.SuspendLayout();
            this.panel_LastPage.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(97, 314);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(91, 28);
            this.btnNext.TabIndex = 2;
            this.btnNext.Text = "التالي";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(191, 314);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(93, 28);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "حفظ";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnBake
            // 
            this.btnBake.Location = new System.Drawing.Point(4, 314);
            this.btnBake.Name = "btnBake";
            this.btnBake.Size = new System.Drawing.Size(91, 28);
            this.btnBake.TabIndex = 4;
            this.btnBake.Text = "رجوع";
            this.btnBake.UseVisualStyleBackColor = true;
            this.btnBake.Click += new System.EventHandler(this.btnBake_Click);
            // 
            // panel_CompanyData
            // 
            this.panel_CompanyData.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_CompanyData.Controls.Add(this.label27);
            this.panel_CompanyData.Controls.Add(this.txtTitle);
            this.panel_CompanyData.Controls.Add(this.txtCombanyDescriptone);
            this.panel_CompanyData.Controls.Add(this.label8);
            this.panel_CompanyData.Controls.Add(this.Label14);
            this.panel_CompanyData.Controls.Add(this.txtPhoneNamber);
            this.panel_CompanyData.Controls.Add(this.txtCombanyName);
            this.panel_CompanyData.Controls.Add(this.Label12);
            this.panel_CompanyData.Controls.Add(this.Label16);
            this.panel_CompanyData.Controls.Add(this.Label13);
            this.panel_CompanyData.Controls.Add(this.txtMyName);
            this.panel_CompanyData.Location = new System.Drawing.Point(1, 2);
            this.panel_CompanyData.Name = "panel_CompanyData";
            this.panel_CompanyData.Size = new System.Drawing.Size(634, 306);
            this.panel_CompanyData.TabIndex = 14;
            this.panel_CompanyData.Paint += new System.Windows.Forms.PaintEventHandler(this.panel3_Paint);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.label27.Location = new System.Drawing.Point(281, 14);
            this.label27.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label27.Name = "label27";
            this.label27.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label27.Size = new System.Drawing.Size(407, 21);
            this.label27.TabIndex = 704;
            this.label27.Text = "يرجي ادخال بيانات الشركة حيث تظهر في الفواتير";
            // 
            // txtTitle
            // 
            this.txtTitle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTitle.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtTitle.Location = new System.Drawing.Point(63, 230);
            this.txtTitle.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtTitle.Multiline = true;
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtTitle.Size = new System.Drawing.Size(422, 52);
            this.txtTitle.TabIndex = 698;
            // 
            // txtCombanyDescriptone
            // 
            this.txtCombanyDescriptone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCombanyDescriptone.Location = new System.Drawing.Point(63, 109);
            this.txtCombanyDescriptone.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtCombanyDescriptone.Name = "txtCombanyDescriptone";
            this.txtCombanyDescriptone.Size = new System.Drawing.Size(422, 28);
            this.txtCombanyDescriptone.TabIndex = 695;
            this.txtCombanyDescriptone.TextChanged += new System.EventHandler(this.txtCombanyDescriptone_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(502, 233);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label8.Size = new System.Drawing.Size(78, 21);
            this.label8.TabIndex = 701;
            this.label8.Text = ":العنــــوان";
            // 
            // Label14
            // 
            this.Label14.AutoSize = true;
            this.Label14.Location = new System.Drawing.Point(502, 191);
            this.Label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label14.Name = "Label14";
            this.Label14.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label14.Size = new System.Drawing.Size(94, 21);
            this.Label14.TabIndex = 702;
            this.Label14.Text = ": رقم الهاتف";
            // 
            // txtPhoneNamber
            // 
            this.txtPhoneNamber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPhoneNamber.Location = new System.Drawing.Point(63, 189);
            this.txtPhoneNamber.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtPhoneNamber.Name = "txtPhoneNamber";
            this.txtPhoneNamber.Size = new System.Drawing.Size(422, 28);
            this.txtPhoneNamber.TabIndex = 697;
            // 
            // txtCombanyName
            // 
            this.txtCombanyName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCombanyName.Location = new System.Drawing.Point(63, 65);
            this.txtCombanyName.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtCombanyName.Name = "txtCombanyName";
            this.txtCombanyName.Size = new System.Drawing.Size(422, 28);
            this.txtCombanyName.TabIndex = 694;
            this.txtCombanyName.TextChanged += new System.EventHandler(this.txtCombanyName_TextChanged);
            // 
            // Label12
            // 
            this.Label12.AutoSize = true;
            this.Label12.Location = new System.Drawing.Point(502, 152);
            this.Label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label12.Name = "Label12";
            this.Label12.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label12.Size = new System.Drawing.Size(123, 21);
            this.Label12.TabIndex = 703;
            this.Label12.Text = ": صاحب الشركة";
            // 
            // Label16
            // 
            this.Label16.AutoSize = true;
            this.Label16.Location = new System.Drawing.Point(502, 112);
            this.Label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label16.Name = "Label16";
            this.Label16.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label16.Size = new System.Drawing.Size(122, 21);
            this.Label16.TabIndex = 700;
            this.Label16.Text = ": نشاط الشركة ";
            // 
            // Label13
            // 
            this.Label13.AutoSize = true;
            this.Label13.Location = new System.Drawing.Point(502, 67);
            this.Label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label13.Name = "Label13";
            this.Label13.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label13.Size = new System.Drawing.Size(116, 21);
            this.Label13.TabIndex = 699;
            this.Label13.Text = ": اسم الشركة ";
            // 
            // txtMyName
            // 
            this.txtMyName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMyName.Location = new System.Drawing.Point(63, 148);
            this.txtMyName.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtMyName.Name = "txtMyName";
            this.txtMyName.Size = new System.Drawing.Size(422, 28);
            this.txtMyName.TabIndex = 696;
            // 
            // panel_User
            // 
            this.panel_User.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_User.Controls.Add(this.label26);
            this.panel_User.Controls.Add(this.txtConfirmPassword);
            this.panel_User.Controls.Add(this.label15);
            this.panel_User.Controls.Add(this.txtEmployee);
            this.panel_User.Controls.Add(this.label9);
            this.panel_User.Controls.Add(this.txtUserPassword);
            this.panel_User.Controls.Add(this.label10);
            this.panel_User.Controls.Add(this.label11);
            this.panel_User.Controls.Add(this.txtUserName);
            this.panel_User.Location = new System.Drawing.Point(1, 2);
            this.panel_User.Name = "panel_User";
            this.panel_User.Size = new System.Drawing.Size(634, 306);
            this.panel_User.TabIndex = 15;
            // 
            // label26
            // 
            this.label26.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.label26.Location = new System.Drawing.Point(161, 53);
            this.label26.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(517, 21);
            this.label26.TabIndex = 443;
            this.label26.Text = "اضافة مدير النظام يرجي التأكد من اسم المستخدم وكلمة المرور";
            // 
            // txtConfirmPassword
            // 
            this.txtConfirmPassword.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtConfirmPassword.BackColor = System.Drawing.Color.White;
            this.txtConfirmPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtConfirmPassword.Location = new System.Drawing.Point(138, 220);
            this.txtConfirmPassword.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtConfirmPassword.Name = "txtConfirmPassword";
            this.txtConfirmPassword.PasswordChar = '*';
            this.txtConfirmPassword.Size = new System.Drawing.Size(306, 28);
            this.txtConfirmPassword.TabIndex = 441;
            this.txtConfirmPassword.UseSystemPasswordChar = true;
            this.txtConfirmPassword.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(450, 224);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(137, 21);
            this.label15.TabIndex = 442;
            this.label15.Text = "تأكيد كلمة المرور :";
            // 
            // txtEmployee
            // 
            this.txtEmployee.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEmployee.BackColor = System.Drawing.Color.White;
            this.txtEmployee.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEmployee.Location = new System.Drawing.Point(138, 118);
            this.txtEmployee.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtEmployee.Name = "txtEmployee";
            this.txtEmployee.Size = new System.Drawing.Size(306, 28);
            this.txtEmployee.TabIndex = 435;
            this.txtEmployee.TextChanged += new System.EventHandler(this.txtEmployee_TextChanged);
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(450, 122);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(112, 21);
            this.label9.TabIndex = 439;
            this.label9.Text = "اسم الموظف :";
            // 
            // txtUserPassword
            // 
            this.txtUserPassword.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUserPassword.BackColor = System.Drawing.Color.White;
            this.txtUserPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUserPassword.Location = new System.Drawing.Point(138, 185);
            this.txtUserPassword.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtUserPassword.Name = "txtUserPassword";
            this.txtUserPassword.PasswordChar = '*';
            this.txtUserPassword.Size = new System.Drawing.Size(306, 28);
            this.txtUserPassword.TabIndex = 437;
            this.txtUserPassword.UseSystemPasswordChar = true;
            this.txtUserPassword.TextChanged += new System.EventHandler(this.txtUserPassword_TextChanged);
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(450, 189);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(99, 21);
            this.label10.TabIndex = 438;
            this.label10.Text = "كلمة المرور :";
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(450, 156);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(133, 21);
            this.label11.TabIndex = 440;
            this.label11.Text = "اسم المستخدم :";
            // 
            // txtUserName
            // 
            this.txtUserName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUserName.BackColor = System.Drawing.Color.White;
            this.txtUserName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUserName.Location = new System.Drawing.Point(138, 151);
            this.txtUserName.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(306, 28);
            this.txtUserName.TabIndex = 436;
            this.txtUserName.TextChanged += new System.EventHandler(this.txtUserName_TextChanged);
            // 
            // panel_PathSave
            // 
            this.panel_PathSave.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_PathSave.Controls.Add(this.label19);
            this.panel_PathSave.Controls.Add(this.txtDatabaseCreate);
            this.panel_PathSave.Controls.Add(this.checkBox1);
            this.panel_PathSave.Controls.Add(this.btnBroser);
            this.panel_PathSave.Controls.Add(this.label17);
            this.panel_PathSave.Controls.Add(this.txtPathSaveDatabase);
            this.panel_PathSave.Controls.Add(this.label18);
            this.panel_PathSave.Location = new System.Drawing.Point(1, 2);
            this.panel_PathSave.Name = "panel_PathSave";
            this.panel_PathSave.Size = new System.Drawing.Size(634, 306);
            this.panel_PathSave.TabIndex = 17;
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(494, 144);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(153, 21);
            this.label19.TabIndex = 449;
            this.label19.Text = "اسم قاعدة البيانات :";
            // 
            // txtDatabaseCreate
            // 
            this.txtDatabaseCreate.Location = new System.Drawing.Point(76, 141);
            this.txtDatabaseCreate.Name = "txtDatabaseCreate";
            this.txtDatabaseCreate.Size = new System.Drawing.Size(401, 28);
            this.txtDatabaseCreate.TabIndex = 448;
            this.txtDatabaseCreate.Text = "ByStro";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(482, 220);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(18, 17);
            this.checkBox1.TabIndex = 447;
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // btnBroser
            // 
            this.btnBroser.Location = new System.Drawing.Point(47, 214);
            this.btnBroser.Name = "btnBroser";
            this.btnBroser.Size = new System.Drawing.Size(31, 26);
            this.btnBroser.TabIndex = 443;
            this.btnBroser.Text = "...";
            this.btnBroser.UseVisualStyleBackColor = true;
            this.btnBroser.Click += new System.EventHandler(this.btnBroser_Click);
            // 
            // label17
            // 
            this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(498, 217);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(106, 21);
            this.label17.TabIndex = 442;
            this.label17.Text = "مسار الحفظ :";
            // 
            // txtPathSaveDatabase
            // 
            this.txtPathSaveDatabase.Location = new System.Drawing.Point(80, 215);
            this.txtPathSaveDatabase.Name = "txtPathSaveDatabase";
            this.txtPathSaveDatabase.ReadOnly = true;
            this.txtPathSaveDatabase.Size = new System.Drawing.Size(397, 28);
            this.txtPathSaveDatabase.TabIndex = 441;
            this.txtPathSaveDatabase.Text = "D:\\ByStro\\Data";
            // 
            // label18
            // 
            this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(103, 75);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(532, 21);
            this.label18.TabIndex = 439;
            this.label18.Text = "سوف يتم انشاء قاعدة بيانات جديدة يرجي تحديد مسار بعيد عن :C\r\n";
            // 
            // panel_Start
            // 
            this.panel_Start.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_Start.Controls.Add(this.label7);
            this.panel_Start.Controls.Add(this.label1);
            this.panel_Start.Controls.Add(this.label2);
            this.panel_Start.Location = new System.Drawing.Point(1, 2);
            this.panel_Start.Name = "panel_Start";
            this.panel_Start.Size = new System.Drawing.Size(634, 306);
            this.panel_Start.TabIndex = 18;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 35F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label7.Location = new System.Drawing.Point(221, 139);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(228, 71);
            this.label7.TabIndex = 7;
            this.label7.Text = "ByStro";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(234, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(189, 27);
            this.label1.TabIndex = 6;
            this.label1.Text = "مرحبا بكم في ... ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 12F);
            this.label2.Location = new System.Drawing.Point(47, 254);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(656, 24);
            this.label2.TabIndex = 7;
            this.label2.Text = "يرجي تتبع الخطوات التالية حتي يتم إعداد البرنامج   مع تمنيتنا بقضاء وقت ممتع";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_LastPage
            // 
            this.panel_LastPage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_LastPage.Controls.Add(this.groupBox4);
            this.panel_LastPage.Controls.Add(this.groupBox1);
            this.panel_LastPage.Controls.Add(this.pictureBox1);
            this.panel_LastPage.Location = new System.Drawing.Point(1, 2);
            this.panel_LastPage.Name = "panel_LastPage";
            this.panel_LastPage.Size = new System.Drawing.Size(634, 306);
            this.panel_LastPage.TabIndex = 19;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.comboBox1);
            this.groupBox4.Controls.Add(this.label22);
            this.groupBox4.Controls.Add(this.txt_password);
            this.groupBox4.Controls.Add(this.label24);
            this.groupBox4.Controls.Add(this.txt_name);
            this.groupBox4.Controls.Add(this.label23);
            this.groupBox4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox4.Font = new System.Drawing.Font("Tahoma", 10F);
            this.groupBox4.Location = new System.Drawing.Point(80, 169);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(445, 120);
            this.groupBox4.TabIndex = 456;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "صلاحيات الاتصال بالسيرفر";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Windows Authentication",
            "SQL Server Authentication"});
            this.comboBox1.Location = new System.Drawing.Point(15, 25);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(291, 27);
            this.comboBox1.TabIndex = 28;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label22.Location = new System.Drawing.Point(317, 58);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(133, 21);
            this.label22.TabIndex = 3;
            this.label22.Text = "اسم المستخدم :";
            // 
            // txt_password
            // 
            this.txt_password.Enabled = false;
            this.txt_password.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_password.Location = new System.Drawing.Point(15, 85);
            this.txt_password.Name = "txt_password";
            this.txt_password.PasswordChar = '*';
            this.txt_password.Size = new System.Drawing.Size(291, 29);
            this.txt_password.TabIndex = 9;
            this.txt_password.TextChanged += new System.EventHandler(this.txt_password_TextChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label24.Location = new System.Drawing.Point(317, 28);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(97, 21);
            this.label24.TabIndex = 29;
            this.label24.Text = "نوع الاتصال :";
            // 
            // txt_name
            // 
            this.txt_name.Enabled = false;
            this.txt_name.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_name.Location = new System.Drawing.Point(15, 54);
            this.txt_name.Name = "txt_name";
            this.txt_name.Size = new System.Drawing.Size(291, 29);
            this.txt_name.TabIndex = 8;
            this.txt_name.TextChanged += new System.EventHandler(this.txt_name_TextChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label23.Location = new System.Drawing.Point(317, 88);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(99, 21);
            this.label23.TabIndex = 4;
            this.label23.Text = "كلمة المرور :";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.combServerName);
            this.groupBox1.Controls.Add(this.lblLoadServer);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 10F);
            this.groupBox1.Location = new System.Drawing.Point(80, 90);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(445, 71);
            this.groupBox1.TabIndex = 455;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "اختر السيرفر";
            // 
            // combServerName
            // 
            this.combServerName.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combServerName.FormattingEnabled = true;
            this.combServerName.Location = new System.Drawing.Point(15, 28);
            this.combServerName.Name = "combServerName";
            this.combServerName.Size = new System.Drawing.Size(291, 27);
            this.combServerName.TabIndex = 450;
            // 
            // lblLoadServer
            // 
            this.lblLoadServer.AutoSize = true;
            this.lblLoadServer.Location = new System.Drawing.Point(317, 31);
            this.lblLoadServer.Name = "lblLoadServer";
            this.lblLoadServer.Size = new System.Drawing.Size(113, 21);
            this.lblLoadServer.TabIndex = 454;
            this.lblLoadServer.TabStop = true;
            this.lblLoadServer.Text = "اسم السيرفر :";
            this.lblLoadServer.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblLoadServer_LinkClicked);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::ByStro.Properties.Resources._2e1ax_default_entry_SQLServer_20140326_103734_1;
            this.pictureBox1.Location = new System.Drawing.Point(76, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(449, 79);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 457;
            this.pictureBox1.TabStop = false;
            // 
            // CreateNewCompany_frm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(636, 347);
            this.Controls.Add(this.btnBake);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.panel_LastPage);
            this.Controls.Add(this.panel_Start);
            this.Controls.Add(this.panel_CompanyData);
            this.Controls.Add(this.panel_User);
            this.Controls.Add(this.panel_PathSave);
            this.Font = new System.Drawing.Font("Tahoma", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "CreateNewCompany_frm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "انشـــاء شــــركة جديــدة";
            this.Load += new System.EventHandler(this.CreateNewCompany_frm_Load);
            this.panel_CompanyData.ResumeLayout(false);
            this.panel_CompanyData.PerformLayout();
            this.panel_User.ResumeLayout(false);
            this.panel_User.PerformLayout();
            this.panel_PathSave.ResumeLayout(false);
            this.panel_PathSave.PerformLayout();
            this.panel_Start.ResumeLayout(false);
            this.panel_Start.PerformLayout();
            this.panel_LastPage.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnBake;
        private System.Windows.Forms.Panel panel_CompanyData;
        public System.Windows.Forms.TextBox txtTitle;
        internal System.Windows.Forms.TextBox txtCombanyDescriptone;
        internal System.Windows.Forms.Label label8;
        internal System.Windows.Forms.Label Label14;
        internal System.Windows.Forms.TextBox txtPhoneNamber;
        internal System.Windows.Forms.TextBox txtCombanyName;
        internal System.Windows.Forms.Label Label12;
        internal System.Windows.Forms.Label Label16;
        internal System.Windows.Forms.Label Label13;
        internal System.Windows.Forms.TextBox txtMyName;
        private System.Windows.Forms.Panel panel_User;
        public System.Windows.Forms.TextBox txtConfirmPassword;
        private System.Windows.Forms.Label label15;
        public System.Windows.Forms.TextBox txtEmployee;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.TextBox txtUserPassword;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        public System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Panel panel_PathSave;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtPathSaveDatabase;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button btnBroser;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label26;
        internal System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtDatabaseCreate;
        private System.Windows.Forms.Panel panel_Start;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel_LastPage;
        private System.Windows.Forms.GroupBox groupBox4;
        internal System.Windows.Forms.ComboBox comboBox1;
        internal System.Windows.Forms.Label label22;
        internal System.Windows.Forms.TextBox txt_password;
        internal System.Windows.Forms.Label label24;
        internal System.Windows.Forms.TextBox txt_name;
        internal System.Windows.Forms.Label label23;
        private System.Windows.Forms.GroupBox groupBox1;
        internal System.Windows.Forms.ComboBox combServerName;
        private System.Windows.Forms.LinkLabel lblLoadServer;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}