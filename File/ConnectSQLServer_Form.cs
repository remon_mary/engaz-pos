﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;
using System.Drawing;
using System.Text.RegularExpressions;
using System.IO;
//using Microsoft.SqlServer.Management.Common;
//using Microsoft.SqlServer.Management.Smo;
namespace ByStro.PL
{
    public partial class ConnectSQLServer_Form : Form
    {
        public ConnectSQLServer_Form()
        {
            InitializeComponent();


        }
        public string add = "new";

        private void ConnectSQLServer_Form_Load(object sender, EventArgs e)
        {
            try
            {
                LoadServer(combServerName);
                var p = Properties.Settings.Default;
                if (p.Mode == true)
                {
                    comboBox1.SelectedIndex = 1;
                    txt_name.Text = p.SQLUserName;
                    txt_password.Text = p.SQLPassword;
                    combServerName.Text = p.ServerName;
                    combDatabase.Text = p.Databasename;
                }
                else
                {
                    comboBox1.SelectedIndex = 0;
                    combServerName.Text = p.ServerName;
                    combDatabase.Text = p.Databasename;
                }


            }
            catch
            {


            }



        }





        // جلب السيفرات
        public void LoadServer(ComboBox cmbServer)
        {
            try
            {

                Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Microsoft SQL Server");
                String[] names = (String[])key.GetValue("InstalledInstances");
                if (names.Length > 0)
                {
                    cmbServer.Items.Clear();
                    cmbServer.Items.Add(".");
                    foreach (string item in names)
                    {
                        if (item == "MSSQLSERVER")
                            cmbServer.Items.Add(Environment.MachineName);
                        else
                            cmbServer.Items.Add(Environment.MachineName + "\\" + item);
                    }
                    cmbServer.SelectedIndex = 0;
                }

            }
            catch
            {
                cmbServer.Items.Add(".");
                cmbServer.Items.Add(Environment.MachineName);
                //  MessageBox.Show(ex.Message, "Alert", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                return;
            }

        }


        #region "جلب اسماء السيرفرات"
        //public void LoadServer(ComboBox cmbServer)
        //{
        //    try
        //    {

        //        Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Microsoft SQL Server");
        //        String[] names;
        //        names = (String[])key.GetValue("InstalledInstances");
        //        if (names.ToString() !=DBNull.Value.ToString())
        //        {
        //        cmbServer.Items.Clear();

        //        foreach (string item in names)
        //        {
        //            if (item == "MSSQLSERVER")
        //            {
        //                //cmbServer.Items.Add(".");
        //                cmbServer.Items.Add(Environment.MachineName);
        //            }
        //            else
        //            {
        //                cmbServer.Items.Add(Environment.MachineName + "\\" + item);
        //            }
        //        }
        //        cmbServer.SelectedIndex = 0;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        cmbServer.Items.Add(".");
        //        //cmbServer.Items.Add(Environment.MachineName);
        //        MessageBox.Show(ex.Message, "Alert", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
        //        return;
        //    }

        //}

        #endregion




        //public void select_Database(ComboBox Combo_Server, ComboBox Combo_Database)
        //{
        //    Combo_Database.Items.Clear();
        //    SqlConnection Conn;
        //    SqlDataAdapter da;
        //    if (comboBox1.SelectedIndex == 1)
        //        Conn = new SqlConnection("Data Source='" + Combo_Server.Text + "',1433;Network Library=DBMSSOCN;Initial Catalog =master;User ID='" + txt_name.Text + "' ;Password='" + txt_password.Text + "'");
        //    else
        //        Conn = new SqlConnection("server='" + Combo_Server.Text + "' ;database=master;integrated security=true");

        //    //public SqlConnection connSQLServer=new SqlConnection("Data Source=MOHAMMED;Initial Catalog=ByStro;Integrated security=true");

        //    da = new SqlDataAdapter("select name from sys.databases", Conn);

        //    DataTable dt = new DataTable();
        //    da.Fill(dt);
        //    for (int i = 0; i < dt.Rows.Count; i++)
        //    {
        //        Combo_Database.Items.Add(dt.Rows[i]["name"].ToString());
        //    }

        //    ServerConnection ser = new ServerConnection();
        //    ser = new ServerConnection(Combo_Server.Text, txt_name.Text, txt_password.Text);

        //}




        public void DatabaseDropDown(ComboBox comb, ComboBox cmbServer, TextBox txtUser, TextBox txtPass)
        {
            try
            {
                SqlConnection connSQLServer = new SqlConnection();

                if (comboBox1.SelectedIndex == 0)
                {
                    connSQLServer = new SqlConnection("Data Source=" + cmbServer.Text + ";Initial Catalog=master;Integrated security=true");
                }
                else
                {
                    connSQLServer = new SqlConnection("Data Source=" + cmbServer.Text + ",1433;Network Library=DBMSSOCN;Initial Catalog =master;User ID='" + txtUser.Text + "' ;Password='" + txtPass.Text + "'");
                }


                SqlDataAdapter da = new SqlDataAdapter("select name from sys.databases", connSQLServer);
                DataTable dt = new DataTable();
                da.Fill(dt);
                comb.Items.Clear();
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        comb.Items.Add(dt.Rows[i]["name"]);
                    }

                }

            }
            catch (Exception ex)
            {
                comb.Items.Clear();
                MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }




        private void combDatabase_DropDown(object sender, EventArgs e)
        {

            if (combServerName.Text == "")
            {
                combServerName.BackColor = Color.Pink;
                combServerName.Focus();
                return;
            }


            if (comboBox1.Text == "SQL Server Authentication")
            {
                if (txt_name.Text == "")
                {
                    txt_name.BackColor = Color.Pink;
                    txt_name.Focus();
                    return;
                }

                if (txt_password.Text == "")
                {
                    txt_password.BackColor = Color.Pink;
                    txt_password.Focus();
                    return;
                }




            }

            DatabaseDropDown(combDatabase, combServerName, txt_name, txt_password);
        }





        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex == 1)
            {
                txt_name.Enabled = true;
                txt_password.Enabled = true;
            }
            else
            {
                txt_name.BackColor = Color.White;
                txt_password.BackColor = Color.White;
                txt_name.Text = "";
                txt_password.Text = "";
                txt_name.Enabled = false;
                txt_password.Enabled = false;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }


        private void txt_name_TextChanged(object sender, EventArgs e)
        {
            txt_name.BackColor = Color.White;

        }

        private void txt_password_TextChanged(object sender, EventArgs e)
        {
            txt_password.BackColor = Color.White;

        }

        private void combDatabase_SelectedIndexChanged(object sender, EventArgs e)
        {
            combDatabase.BackColor = Color.White;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (combServerName.Text == "")
            {
                combServerName.BackColor = Color.Pink;
                combServerName.Focus();
                return;
            }

            if (combDatabase.Text == "")
            {
                combDatabase.BackColor = Color.Pink;
                combDatabase.Focus();
                return;
            }

            if (comboBox1.SelectedIndex == 1)
            {
                if (txt_name.Text == "")
                {
                    txt_name.BackColor = Color.Pink;
                    txt_name.Focus();
                    return;
                }
                if (txt_password.Text == "")
                {
                    txt_password.BackColor = Color.Pink;
                    txt_password.Focus();
                    return;
                }
            }

            Save();





            MessageBox.Show("تم حفظ الاعدادات بنجاح", "Massage", MessageBoxButtons.OK, MessageBoxIcon.Information);

            Close();

            try
            {
                Main_frm main = this.Owner as Main_frm;
                if (add == "Main")
                {
                    MessageBox.Show("سوف يتم أعادة تسجيل الدخول  من جديد  ", "Massage", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    //DataAccessLayer.closeMainform = true;
                    //main.Close();
                    //LoginUser_Form f = new LoginUser_Form();
                    //f.Show();
                    Application.Restart();

                }
            }
            catch
            {


            }
        }


        private void Save()
        {

            var p = Properties.Settings.Default;
            if (comboBox1.SelectedIndex == 1)
            {
                p.SQLUserName = txt_name.Text;
                p.SQLPassword = txt_password.Text;
                p.ServerName = combServerName.Text;
                p.Databasename = combDatabase.Text;

                p.Connection_String = "Data Source=" + combServerName.Text + ",1433;Network Library=DBMSSOCN;Initial Catalog =" + combDatabase.Text + ";User ID=" + txt_name.Text + " ;Password=" + txt_password.Text + "";
                p.Mode = true;
                p.Save();
            }
            else
            {
                p.SQLUserName = "";
                p.SQLPassword = "";
                p.ServerName = combServerName.Text;
                p.Databasename = combDatabase.Text;
                p.Connection_String = "Data Source=" + combServerName.Text + ";Initial Catalog=" + combDatabase.Text + ";Integrated security=true";
                p.Mode = false;
                p.Save();
            }

        }
        private void AttachDatabase_Click(object sender, EventArgs e)
        {

            if (combServerName.Text == "")
            {
                combServerName.BackColor = Color.Pink;
                combServerName.Focus();
                return;
            }


            if (comboBox1.SelectedIndex == 1)
            {
                if (txt_name.Text == "")
                {
                    txt_name.BackColor = Color.Pink;
                    txt_name.Focus();
                    return;
                }
                if (txt_password.Text == "")
                {
                    txt_password.BackColor = Color.Pink;
                    txt_password.Focus();
                    return;
                }
            }
            SqlConnection connSQLServer = new SqlConnection(@"Server=" + combServerName.Text + ";  Initial Catalog = master; Integrated Security = True");
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Select Database | *.mdf";
            ofd.Title = "اختر قاعدة البيانات";
            if (ofd.ShowDialog() == DialogResult.OK)
            {

                this.Cursor = Cursors.WaitCursor;


                string input = ofd.FileName;
                int x = input.Length - 4;
                string sub2 = input.Substring(0, x);
                // MessageBox.Show(sub2, "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

                string mdf = string.Format(sub2 + ".mdf");
                string log = string.Format(sub2 + "_log.ldf");
                var sections = input.Split('\\');
                var databasename = sections[sections.Length - 1];


                int r = databasename.Length - 4;
                string fileName = databasename.Substring(0, r);

                // MessageBox.Show(fileName, "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                try
                {


                    SqlCommand cmd = new SqlCommand("USE [master]EXEC sp_attach_db @dbname = N'" + fileName + "', @filename1 = '" + mdf + "',@filename2 =  '" + log + "'; ", connSQLServer);
                    connSQLServer.Open();
                    cmd.ExecuteNonQuery();
                    connSQLServer.Close();
                    combDatabase.Text = fileName;
                    this.Cursor = Cursors.Default;
                    Save();
                    MessageBox.Show("تم اضافة قاعدة البيانات بنجاح", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);


                }
                catch (Exception ex)
                {
                    this.Cursor = Cursors.Default;
                    Close();
                    MessageBox.Show(ex.Message);

                }


            }

        }
        CreateNew_Database_cls CreateNew_Database_cls = new CreateNew_Database_cls();
        private void createNewDatabaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (combServerName.Text == "")
            {
                combServerName.BackColor = Color.Pink;
                combServerName.Focus();
                return;
            }


            if (comboBox1.SelectedIndex == 1)
            {
                if (txt_name.Text == "")
                {
                    txt_name.BackColor = Color.Pink;
                    txt_name.Focus();
                    return;
                }
                if (txt_password.Text == "")
                {
                    txt_password.BackColor = Color.Pink;
                    txt_password.Focus();
                    return;
                }
            }
            string SqlConnectionString;

            if (comboBox1.SelectedIndex == 0)
            {
                SqlConnectionString = "Data Source=" + combServerName.Text + ";Initial Catalog=master;Integrated security=true";
            }
            else
            {
                SqlConnectionString = "Data Source=" + combServerName.Text + ",1433;Network Library=DBMSSOCN;Initial Catalog =master;User ID='" + txt_name.Text + "' ;Password='" + txt_password.Text + "'";
            }



            SaveFileDialog sfd = new SaveFileDialog();
            sfd.FileName = "ByStro";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                //toolStripStatusLabel1.Text = "Plase Wait...";
                File_Path = System.IO.Path.GetFileName(sfd.FileName);
                FileName = sfd.FileName;


                CreateNew_Database(SqlConnectionString, File_Path, sfd.FileName);

                combDatabase.Text = File_Path;
                Save();
            }
        }

        String File_Path; string FileName;


        public void CreateNew_Database(string sqlConnectionString, string DatabaseName, string Path)
        {

            SqlConnection connection = new SqlConnection(sqlConnectionString);
            try
            {


                string script = @"CREATE DATABASE " + DatabaseName + " On primary (Name = " + DatabaseName + ", FileName =N'" + Path + ".mdf') LOG ON (Name=" + DatabaseName + "_log, FileName=N'" + Path + "_log.ldf')" + Environment.NewLine + "Go" + Environment.NewLine + "Use " + DatabaseName + Environment.NewLine;

                ////=========================================================================
                FileInfo file = new FileInfo(Application.StartupPath + "\\script.sql");
                script += file.OpenText().ReadToEnd();

                // split script on GO command
                System.Collections.Generic.IEnumerable<string> commandStrings = Regex.Split(script, @"^\s*GO\s*$",
                                         RegexOptions.Multiline | RegexOptions.IgnoreCase);
                using (connection)
                {
                    if (connection.State != System.Data.ConnectionState.Open) connection.Open();
                    foreach (string commandString in commandStrings)
                    {
                        if (commandString.Trim() != "")
                        {
                            using (var command = new SqlCommand(commandString, connection))
                            {

                                command.ExecuteNonQuery();
                            }
                        }
                    }

                }

                MessageBox.Show("تم انشاء قاعدة البيانات بنجاح", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                toolStripStatusLabel1.Text = "Complate Scussfuly...";
            }
            catch (Exception ex)
            {
                //string spError = commandString.Length > 100 ? commandString.Substring(0, 100) + " ...\n..." : commandString;
                MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                toolStripStatusLabel1.Text = "Error...";
            }
            finally
            {
                if (connection.State != System.Data.ConnectionState.Closed) connection.Close();
            }

        }









        private void ConnectSQLServer_Form_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F10)
            {
                btnSave_Click(null, null);
            }
            //else if (e.Control == true && e.KeyCode == Keys.N)
            //{
            //    createNewDatabaseToolStripMenuItem_Click(null, null);
            //}
            //if (e.Control == true && e.KeyCode == Keys.D)
            //{
            //    AttachDatabase_Click(null, null);
            //}
            if (e.KeyCode == Keys.Escape)
            {
                Close();
            }

        }

        private void combServerName_TextChanged(object sender, EventArgs e)
        {
            combServerName.BackColor = Color.White;
        }

        private void combServerName_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void backgroundWorker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            string SqlConnectionString;

            if (comboBox1.SelectedIndex == 0)
            {
                SqlConnectionString = "Data Source=" + combServerName.Text + ";Initial Catalog=master;Integrated security=true";
            }
            else
            {
                SqlConnectionString = "Data Source=" + combServerName.Text + ",1433;Network Library=DBMSSOCN;Initial Catalog =master;User ID='" + txt_name.Text + "' ;Password='" + txt_password.Text + "'";
            }


            CreateNew_Database(SqlConnectionString, File_Path, FileName);



            SqlConnection connection = new SqlConnection(SqlConnectionString);
            try
            {


                string script = @"CREATE DATABASE " + File_Path + " On primary (Name = " + File_Path + ", FileName =N'" + FileName + ".mdf') LOG ON (Name=" + File_Path + "_log, FileName=N'" + FileName + "_log.ldf')" + Environment.NewLine + "Go" + Environment.NewLine + "Use " + File_Path + Environment.NewLine;

                ////=========================================================================
                FileInfo file = new FileInfo(Application.StartupPath + "\\script.sql");
                script += file.OpenText().ReadToEnd();
                // split script on GO command
                System.Collections.Generic.IEnumerable<string> commandStrings = Regex.Split(script, @"^\s*GO\s*$",
                                         RegexOptions.Multiline | RegexOptions.IgnoreCase);
                using (connection)
                {
                    if (connection.State != System.Data.ConnectionState.Open) connection.Open();
                    foreach (string commandString in commandStrings)
                    {
                        if (commandString.Trim() != "")
                        {
                            using (var command = new SqlCommand(commandString, connection))
                            {

                                command.ExecuteNonQuery();

                            }
                        }
                    }

                }


            }
            catch (Exception ex)
            {
                //string spError = commandString.Length > 100 ? commandString.Substring(0, 100) + " ...\n..." : commandString;
                MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            finally
            {
                if (connection.State != System.Data.ConnectionState.Closed) connection.Close();
            }




        }

        private void backgroundWorker_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {

        }

        private void backgroundWorker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            MessageBox.Show("تم انشاء قاعدة البيانات بنجاح", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

            combDatabase.Text = File_Path;
            Save();
        }
    }
}
