﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByStro.ReportModules
{
  public   class ItemsSoldToCustomers
    {
        public int CustomerID { get; set; }
        public string CustomerName { get; set; }
        public string Phone { get; set; }
        public double ItemCount { get => Products.Count(); }
        public double TotalQty { get => Products.Sum(x => x.Quantity ); }
        public double TotalSellPrice { get => Products.Sum(x => x.TotalSellPrice);}
        public double TotalCost { get => Products.Sum(x => x.TotalCost );}
        public double TotalProductsProfit => Products.Sum(x => x.TotalProfit);
        public List<TotalSoldProducts> Products { get; set; }

        public class TotalSoldProducts
        {
            public string  ProductName { get; set; }
            public string StoreName { get; set; }
            public double Quantity { get; set; }
            public double UnitCost { get; set; }
            public double UnitSellPrice { get; set; }
            public double TotalCost { get => Quantity * UnitCost; }
            public double TotalSellPrice { get => Quantity * UnitSellPrice; }
            public double TotalProfit { get => TotalSellPrice - TotalCost; }

        }

    }
}
