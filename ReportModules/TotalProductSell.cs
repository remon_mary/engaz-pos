﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ByStro.Clases.MasterClass;

namespace ByStro.ReportModules
{
    public class TotalGroupProductSells
    {
        public string Name { get; set; }
        public List<TotalProductSell> TotalProductSells { get; set; }
        public double TotalQty => TotalProductSells.Sum(x => x.Qty);
        public double TotalPrice => TotalProductSells.Sum(x => x.Price);
        public static List<TotalGroupProductSells> GetTotalGroupProductSells() 
        {
            List<TotalGroupProductSells> totalGroupProductSells = new List<TotalGroupProductSells>();
          
            using (DAL.DBDataContext db=new DAL.DBDataContext(Properties.Settings.Default.Connection_String))
            {
                var Categories = db.Categories.Select(x=>new { x.CategoryID,x.CategoryName}).ToList();
                var Prodecuts = db.Prodecuts.Select(x=>new { x.ProdecutID,x.ProdecutName, x.CategoryID ,x.FiestUnitFactor ,x.SecoundUnitFactor,x.ThreeUnitFactor}).ToList();
                var InvoicesIds = db.Inv_Invoices.Where(x=>x.InvoiceType== (byte)InvoiceType.SalesInvoice).Select(x=>x.ID).ToList();

                var InvoiceDetails = db.Inv_InvoiceDetails.Where(x=> InvoicesIds.Contains(x.InvoiceID)).Select(x => new InvoiceDetailsTemp
                {
                    CategoryID = db.Prodecuts.Single(p => p.ProdecutID == x.ItemID).CategoryID,
                    ItemID=  x.ItemID,
                    ItemQty=  x.ItemQty ,
                    ItemUnitID=  x.ItemUnitID,
                    TotalPrice=  x.TotalPrice,
                }).ToList();
                foreach (var item in InvoiceDetails)
                {
                    switch (item.ItemUnitID)
                    {
                        case 2:
                            item.ItemQty = item.ItemQty / double.Parse(Prodecuts.Single(p => p.ProdecutID == item.ItemID).FiestUnitFactor);
                            break;
                        case 3:
                            item.ItemQty = item.ItemQty / double.Parse(Prodecuts.Single(p => p.ProdecutID == item.ItemID).FiestUnitFactor);
                            break;
                        default:
                            break;
                    }
                }
                foreach (var InvoiceCategories in InvoiceDetails.GroupBy(x=>x.CategoryID))
                {
                    TotalGroupProductSells totalGroupProduct = new TotalGroupProductSells();
                    totalGroupProduct.Name = Categories.Single(x => x.CategoryID == InvoiceCategories.Key).CategoryName;
                    totalGroupProduct.TotalProductSells = new List<TotalProductSell>();
                    foreach (var item in InvoiceCategories.GroupBy(x => x.ItemID))
                    {
                        var Prodecut = Prodecuts.Single(p => p.ProdecutID == item.Key);
                        totalGroupProduct.TotalProductSells.Add(new TotalProductSell 
                        {
                            Name = Prodecut.ProdecutName,
                            Qty= item.Sum(x=>x.ItemQty),
                            Price= item.Sum(x=>x.TotalPrice),
                        });
                    }
                    totalGroupProductSells.Add(totalGroupProduct);
                }

            }
            
            
            return totalGroupProductSells;
        }
    }
    class InvoiceDetailsTemp
    { 
        public int? CategoryID { get; set; } 
        public int ItemID { get; set; } 
        public int ItemUnitID { get; set; } 
        public double ItemQty { get; set; }
        public double TotalPrice { get; set; }

    }
    public class TotalProductSell
    {
        public string Name { get; set; }
        public double Qty { get; set; }
        public double Price { get; set; }

    }
}
