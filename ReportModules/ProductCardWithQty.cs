﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByStro.ReportModules
{
   public  class ProductCardWithQty
    {

        public int ID { get; set; }
        public string Name { get; set; }
        public string Company { get; set; }
        public string Group { get; set; } 

        public List<ProductQtyInStore> ProductQtyInStores { get; set; }

        public class ProductQtyInStore
        {
           // public string  ProductID { get; set; }
            public string  StoreName { get; set; }
            public double  Quantity { get; set; }
            public double TotalCost { get; set; }
            public double TotalSellPrice { get; set; }
        }
    }
}
