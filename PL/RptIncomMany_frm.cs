﻿using ByStro.BL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class RptIncomMany_frm : Form
    {
        public RptIncomMany_frm()
        {
            InitializeComponent();
        }
       RptIncomMany_cls cls = new RptIncomMany_cls();

       // Menu_Meny_cls cls = new Menu_Meny_cls();
        private void RptIncomMany_frm_Load(object sender, EventArgs e)
        {
          
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Text = cls.PayType_trans();
            textBox3.Text = cls.Balence_ISCusSupp("Cus");
            textBox4.Text = cls.Store_trans();
            textBox8.Text = (double.Parse(textBox1.Text) + double.Parse(textBox3.Text) + double.Parse(textBox4.Text)).ToString();


            textBox7.Text = cls.Balence_ISCusSupp("Supp");


          double x=  ( Sales() + Maintenance()) -( Expense()+ Salary());
            textBox5.Text = x.ToString();

            textBox6.Text = cls.Capital();
            


            textBox9.Text = (double.Parse(textBox6.Text) + double.Parse(textBox7.Text) + double.Parse(textBox5.Text)).ToString();

        }







        private Double Sales()
        {
           
                DataTable dt = cls.Search_Sales();
                DataTable dt_DiscoundBay = cls.Search_Bay();
                double txtSumSales = double.Parse( dt.Rows[0]["NetInvoice"].ToString());
                double txtSumRSales = double.Parse(dt.Rows[0]["ReturnInvoise"].ToString());

                double  txtSales_NetInvoice = Convert.ToDouble(dt.Rows[0]["NetInvoice"].ToString()) - Convert.ToDouble(dt.Rows[0]["ReturnInvoise"]);

                //Search_Bay
                double x = Convert.ToDouble(dt.Rows[0]["TotalBayPrice"].ToString());
                double y = Convert.ToDouble(dt.Rows[0]["ReturnBayPrice"].ToString());
                double w = Convert.ToDouble(dt_DiscoundBay.Rows[0]["Discount"].ToString());

                return Convert.ToDouble(txtSales_NetInvoice) - Convert.ToDouble((x - y - w).ToString());
          
        }

        private Double Expense()
        {
            
                DataTable dt = cls.Search_Expenses();
                return double.Parse( dt.Rows[0]["ExpenseValue"].ToString());

        

        }

        private Double Salary()
        {
         
                DataTable dt = cls.Search_Salary();
             return double.Parse(dt.Rows[0]["NetSalary"].ToString());


        }
        private Double Maintenance()
        {
        
                DataTable dt = cls.Search_Maintenance();

               return double.Parse(dt.Rows[0][0].ToString());
  

        }












    }
}
