﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class ItemsSearchBill_frm : Form
    {
        public ItemsSearchBill_frm()
        {
            InitializeComponent();
        }

        Prodecut_cls cls = new Prodecut_cls();
        private void ItemsSearchBill_frm_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingForm(this);
            this.MaximizeBox = true;
            DataTable dt = cls.Prodecut_SearchBill(textBox1.Text);
            DGV1.AutoGenerateColumns = false;
            DGV1.DataSource = dt;
            textBox1.Focus();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            DataTable dt = cls.Prodecut_SearchBill(textBox1.Text);
            DGV1.AutoGenerateColumns = false;
            DGV1.DataSource = dt;
        }
        public Boolean loaddata = false;
        private void DGV1_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (DGV1.Rows.Count == 0)
                {
                    return;
                }
              
               
                    loaddata = true;
                    Close();
             
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
