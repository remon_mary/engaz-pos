﻿using DevExpress.XtraEditors;
using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
namespace ByStro.PL
{
    public partial class ChangePassword_frm : XtraForm
    {
        public ChangePassword_frm()
        {
            InitializeComponent();
        }

        private void ChangePasswordForm_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingForm(this);
        }

        private void txtUserName_TextChanged(object sender, EventArgs e)
        {
            txtUserName.BackColor = Color.White;
        }

        private void txtOldPassword_TextChanged(object sender, EventArgs e)
        {
            txtOldPassword.BackColor = Color.White;
        }

        private void txtNewPassword_TextChanged(object sender, EventArgs e)
        {
            txtNewPassword.BackColor = Color.White;
        }

        private void txtConfirmPassword_TextChanged(object sender, EventArgs e)
        {
            txtConfirmPassword.BackColor = Color.White;

        }

        private void button2_Click(object sender, EventArgs e)
        {

            #region " condetion"
              if (txtUserName.Text.Trim()=="")
            {
                txtUserName.BackColor = Color.Pink;
                txtUserName.Focus();
                return;
            }

              if (txtOldPassword.Text.Trim() == "")
              {
                  txtOldPassword.BackColor = Color.Pink;
                  txtOldPassword.Focus();
                  return;
              }
              if (txtNewPassword.Text.Trim() == "")
              {
                  txtNewPassword.BackColor = Color.Pink;
                  txtNewPassword.Focus();
                  return;
              }
              if (txtConfirmPassword.Text.Trim() == "")
              {
                  txtConfirmPassword.BackColor = Color.Pink;
                  txtConfirmPassword.Focus();
                  return;
              }
              if(txtConfirmPassword.Text.Trim() != txtNewPassword.Text.Trim())
            {
                XtraMessageBox.Show("كلمه السر الجديده غير متطابقه  ");

            }
            #endregion
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
            var user = db.UserPermissions.SingleOrDefault(x => x.UserName == txtUserName.Text.Trim() &&
            x.UserPassword == txtOldPassword.Text);
            if(user == null)
            {
                XtraMessageBox.Show("اسم المستخدم او كلمه السر غير صحيحه ");
                return;
            }
            user.UserPassword = txtNewPassword.Text;
            db.SubmitChanges();
            XtraMessageBox.Show("تم تغيير كلمه السر بنجاح");



        }

        private void ChangePasswordForm_KeyDown(object sender, KeyEventArgs e)
        {
            #region "KeyDown"
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    Close();
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            #endregion
        }





















    }
}
