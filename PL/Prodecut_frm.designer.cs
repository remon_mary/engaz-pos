﻿namespace ByStro.PL
{
    partial class Prodecut_frm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.txtBarcode3 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtThreeUnitBarcode = new DevExpress.XtraEditors.ButtonEdit();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.txtProdecutBayPrice = new DevExpress.XtraEditors.SpinEdit();
            this.txtThreeUnitPrice3 = new DevExpress.XtraEditors.SpinEdit();
            this.txtThreeUnitPrice2 = new DevExpress.XtraEditors.SpinEdit();
            this.txtThreeUnitPrice1 = new DevExpress.XtraEditors.SpinEdit();
            this.txtSecoundUnitPrice3 = new DevExpress.XtraEditors.SpinEdit();
            this.txtSecoundUnitPrice2 = new DevExpress.XtraEditors.SpinEdit();
            this.txtSecoundUnitPrice1 = new DevExpress.XtraEditors.SpinEdit();
            this.txtFiestUnitPrice3 = new DevExpress.XtraEditors.SpinEdit();
            this.txtFiestUnitPrice2 = new DevExpress.XtraEditors.SpinEdit();
            this.txtFiestUnitPrice1 = new DevExpress.XtraEditors.SpinEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.RbUnitDefoult3 = new System.Windows.Forms.RadioButton();
            this.txtProdecutBayPrice3 = new System.Windows.Forms.TextBox();
            this.RbUnitDefoult2 = new System.Windows.Forms.RadioButton();
            this.txtSecoundUnitBarcode = new DevExpress.XtraEditors.ButtonEdit();
            this.txtProdecutBayPrice2 = new System.Windows.Forms.TextBox();
            this.spinEdit1 = new DevExpress.XtraEditors.SpinEdit();
            this.txtThreeUnitOperating = new System.Windows.Forms.TextBox();
            this.txtFiestUnitBarcode = new DevExpress.XtraEditors.ButtonEdit();
            this.txtSecoundUnitOperating = new System.Windows.Forms.TextBox();
            this.RbUnitDefoult1 = new System.Windows.Forms.RadioButton();
            this.combThreeUnit = new System.Windows.Forms.ComboBox();
            this.chk_HasSerial = new DevExpress.XtraEditors.CheckEdit();
            this.txtFiestUnitOperating = new System.Windows.Forms.TextBox();
            this.chk_HasExpire = new DevExpress.XtraEditors.CheckEdit();
            this.combSecoundUnit = new System.Windows.Forms.ComboBox();
            this.chk_HasSize = new DevExpress.XtraEditors.CheckEdit();
            this.chk_HasColor = new DevExpress.XtraEditors.CheckEdit();
            this.chProductService = new System.Windows.Forms.CheckBox();
            this.DGV1 = new System.Windows.Forms.DataGridView();
            this.C1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.ProdecutID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProdecutName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chStatus = new System.Windows.Forms.CheckBox();
            this.txtProdecutID = new System.Windows.Forms.TextBox();
            this.txtDiscoundSale = new System.Windows.Forms.TextBox();
            this.txtProdecutName = new System.Windows.Forms.TextBox();
            this.combGroup = new System.Windows.Forms.ComboBox();
            this.txtProdecutLocation = new System.Windows.Forms.TextBox();
            this.combFiestUnit = new System.Windows.Forms.ComboBox();
            this.txtDiscoundBay = new System.Windows.Forms.TextBox();
            this.txtCompany = new System.Windows.Forms.TextBox();
            this.txtRequestLimit = new System.Windows.Forms.TextBox();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem43 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem44 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem45 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem46 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem47 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem48 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem40 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem49 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem50 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem51 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem42 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem41 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnGetData = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.txtThreeUnitBarcode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtProdecutBayPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThreeUnitPrice3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThreeUnitPrice2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThreeUnitPrice1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecoundUnitPrice3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecoundUnitPrice2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecoundUnitPrice1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFiestUnitPrice3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFiestUnitPrice2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFiestUnitPrice1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecoundUnitBarcode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFiestUnitBarcode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_HasSerial.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_HasExpire.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_HasSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_HasColor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGV1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).BeginInit();
            this.SuspendLayout();
            // 
            // txtBarcode3
            // 
            this.txtBarcode3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBarcode3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBarcode3.Location = new System.Drawing.Point(450, 748);
            this.txtBarcode3.MaxLength = 80;
            this.txtBarcode3.Name = "txtBarcode3";
            this.txtBarcode3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtBarcode3.Size = new System.Drawing.Size(234, 23);
            this.txtBarcode3.TabIndex = 516;
            this.txtBarcode3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(690, 750);
            this.label13.Name = "label13";
            this.label13.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label13.Size = new System.Drawing.Size(56, 16);
            this.label13.TabIndex = 517;
            this.label13.Text = "الباركود :";
            // 
            // txtThreeUnitBarcode
            // 
            this.txtThreeUnitBarcode.Location = new System.Drawing.Point(36, 419);
            this.txtThreeUnitBarcode.Name = "txtThreeUnitBarcode";
            this.txtThreeUnitBarcode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Search, "", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "توليد براكود تلقائي", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.txtThreeUnitBarcode.Size = new System.Drawing.Size(161, 20);
            this.txtThreeUnitBarcode.StyleController = this.dataLayoutControl1;
            this.txtThreeUnitBarcode.TabIndex = 538;
            this.txtThreeUnitBarcode.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.txtFiestUnitBarcode_ButtonClick);
            this.txtThreeUnitBarcode.TextChanged += new System.EventHandler(this.txtThreeUnitBarcode_TextChanged);
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.txtProdecutBayPrice);
            this.dataLayoutControl1.Controls.Add(this.txtThreeUnitPrice3);
            this.dataLayoutControl1.Controls.Add(this.txtThreeUnitPrice2);
            this.dataLayoutControl1.Controls.Add(this.txtThreeUnitPrice1);
            this.dataLayoutControl1.Controls.Add(this.txtSecoundUnitPrice3);
            this.dataLayoutControl1.Controls.Add(this.txtSecoundUnitPrice2);
            this.dataLayoutControl1.Controls.Add(this.txtSecoundUnitPrice1);
            this.dataLayoutControl1.Controls.Add(this.txtFiestUnitPrice3);
            this.dataLayoutControl1.Controls.Add(this.txtFiestUnitPrice2);
            this.dataLayoutControl1.Controls.Add(this.txtFiestUnitPrice1);
            this.dataLayoutControl1.Controls.Add(this.simpleButton1);
            this.dataLayoutControl1.Controls.Add(this.gridControl1);
            this.dataLayoutControl1.Controls.Add(this.RbUnitDefoult3);
            this.dataLayoutControl1.Controls.Add(this.txtThreeUnitBarcode);
            this.dataLayoutControl1.Controls.Add(this.txtProdecutBayPrice3);
            this.dataLayoutControl1.Controls.Add(this.RbUnitDefoult2);
            this.dataLayoutControl1.Controls.Add(this.txtSecoundUnitBarcode);
            this.dataLayoutControl1.Controls.Add(this.txtProdecutBayPrice2);
            this.dataLayoutControl1.Controls.Add(this.spinEdit1);
            this.dataLayoutControl1.Controls.Add(this.txtThreeUnitOperating);
            this.dataLayoutControl1.Controls.Add(this.txtFiestUnitBarcode);
            this.dataLayoutControl1.Controls.Add(this.txtSecoundUnitOperating);
            this.dataLayoutControl1.Controls.Add(this.RbUnitDefoult1);
            this.dataLayoutControl1.Controls.Add(this.combThreeUnit);
            this.dataLayoutControl1.Controls.Add(this.chk_HasSerial);
            this.dataLayoutControl1.Controls.Add(this.txtFiestUnitOperating);
            this.dataLayoutControl1.Controls.Add(this.chk_HasExpire);
            this.dataLayoutControl1.Controls.Add(this.combSecoundUnit);
            this.dataLayoutControl1.Controls.Add(this.chk_HasSize);
            this.dataLayoutControl1.Controls.Add(this.chk_HasColor);
            this.dataLayoutControl1.Controls.Add(this.chProductService);
            this.dataLayoutControl1.Controls.Add(this.DGV1);
            this.dataLayoutControl1.Controls.Add(this.chStatus);
            this.dataLayoutControl1.Controls.Add(this.txtProdecutID);
            this.dataLayoutControl1.Controls.Add(this.txtDiscoundSale);
            this.dataLayoutControl1.Controls.Add(this.txtProdecutName);
            this.dataLayoutControl1.Controls.Add(this.combGroup);
            this.dataLayoutControl1.Controls.Add(this.txtProdecutLocation);
            this.dataLayoutControl1.Controls.Add(this.combFiestUnit);
            this.dataLayoutControl1.Controls.Add(this.txtDiscoundBay);
            this.dataLayoutControl1.Controls.Add(this.txtCompany);
            this.dataLayoutControl1.Controls.Add(this.txtRequestLimit);
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 24);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsView.RightToLeftMirroringApplied = true;
            this.dataLayoutControl1.Root = this.Root;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1267, 696);
            this.dataLayoutControl1.TabIndex = 518;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // txtProdecutBayPrice
            // 
            this.txtProdecutBayPrice.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtProdecutBayPrice.Location = new System.Drawing.Point(622, 467);
            this.txtProdecutBayPrice.MenuManager = this.barManager1;
            this.txtProdecutBayPrice.Name = "txtProdecutBayPrice";
            this.txtProdecutBayPrice.Properties.Appearance.Options.UseTextOptions = true;
            this.txtProdecutBayPrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtProdecutBayPrice.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtProdecutBayPrice.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtProdecutBayPrice.Size = new System.Drawing.Size(135, 20);
            this.txtProdecutBayPrice.StyleController = this.dataLayoutControl1;
            this.txtProdecutBayPrice.TabIndex = 550;
            this.txtProdecutBayPrice.EditValueChanged += new System.EventHandler(this.txtProdecutBayPrice_EditValueChanged);
            // 
            // txtThreeUnitPrice3
            // 
            this.txtThreeUnitPrice3.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtThreeUnitPrice3.Location = new System.Drawing.Point(36, 515);
            this.txtThreeUnitPrice3.MenuManager = this.barManager1;
            this.txtThreeUnitPrice3.Name = "txtThreeUnitPrice3";
            this.txtThreeUnitPrice3.Properties.Appearance.Options.UseTextOptions = true;
            this.txtThreeUnitPrice3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtThreeUnitPrice3.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtThreeUnitPrice3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtThreeUnitPrice3.Size = new System.Drawing.Size(161, 20);
            this.txtThreeUnitPrice3.StyleController = this.dataLayoutControl1;
            this.txtThreeUnitPrice3.TabIndex = 549;
            // 
            // txtThreeUnitPrice2
            // 
            this.txtThreeUnitPrice2.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtThreeUnitPrice2.Location = new System.Drawing.Point(36, 491);
            this.txtThreeUnitPrice2.MenuManager = this.barManager1;
            this.txtThreeUnitPrice2.Name = "txtThreeUnitPrice2";
            this.txtThreeUnitPrice2.Properties.Appearance.Options.UseTextOptions = true;
            this.txtThreeUnitPrice2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtThreeUnitPrice2.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtThreeUnitPrice2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtThreeUnitPrice2.Size = new System.Drawing.Size(161, 20);
            this.txtThreeUnitPrice2.StyleController = this.dataLayoutControl1;
            this.txtThreeUnitPrice2.TabIndex = 548;
            // 
            // txtThreeUnitPrice1
            // 
            this.txtThreeUnitPrice1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtThreeUnitPrice1.Location = new System.Drawing.Point(36, 467);
            this.txtThreeUnitPrice1.MenuManager = this.barManager1;
            this.txtThreeUnitPrice1.Name = "txtThreeUnitPrice1";
            this.txtThreeUnitPrice1.Properties.Appearance.Options.UseTextOptions = true;
            this.txtThreeUnitPrice1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtThreeUnitPrice1.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtThreeUnitPrice1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtThreeUnitPrice1.Size = new System.Drawing.Size(161, 20);
            this.txtThreeUnitPrice1.StyleController = this.dataLayoutControl1;
            this.txtThreeUnitPrice1.TabIndex = 547;
            // 
            // txtSecoundUnitPrice3
            // 
            this.txtSecoundUnitPrice3.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSecoundUnitPrice3.Location = new System.Drawing.Point(326, 515);
            this.txtSecoundUnitPrice3.MenuManager = this.barManager1;
            this.txtSecoundUnitPrice3.Name = "txtSecoundUnitPrice3";
            this.txtSecoundUnitPrice3.Properties.Appearance.Options.UseTextOptions = true;
            this.txtSecoundUnitPrice3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtSecoundUnitPrice3.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtSecoundUnitPrice3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtSecoundUnitPrice3.Size = new System.Drawing.Size(167, 20);
            this.txtSecoundUnitPrice3.StyleController = this.dataLayoutControl1;
            this.txtSecoundUnitPrice3.TabIndex = 546;
            // 
            // txtSecoundUnitPrice2
            // 
            this.txtSecoundUnitPrice2.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSecoundUnitPrice2.Location = new System.Drawing.Point(326, 491);
            this.txtSecoundUnitPrice2.MenuManager = this.barManager1;
            this.txtSecoundUnitPrice2.Name = "txtSecoundUnitPrice2";
            this.txtSecoundUnitPrice2.Properties.Appearance.Options.UseTextOptions = true;
            this.txtSecoundUnitPrice2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtSecoundUnitPrice2.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtSecoundUnitPrice2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtSecoundUnitPrice2.Size = new System.Drawing.Size(167, 20);
            this.txtSecoundUnitPrice2.StyleController = this.dataLayoutControl1;
            this.txtSecoundUnitPrice2.TabIndex = 545;
            // 
            // txtSecoundUnitPrice1
            // 
            this.txtSecoundUnitPrice1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSecoundUnitPrice1.Location = new System.Drawing.Point(326, 467);
            this.txtSecoundUnitPrice1.MenuManager = this.barManager1;
            this.txtSecoundUnitPrice1.Name = "txtSecoundUnitPrice1";
            this.txtSecoundUnitPrice1.Properties.Appearance.Options.UseTextOptions = true;
            this.txtSecoundUnitPrice1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtSecoundUnitPrice1.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtSecoundUnitPrice1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtSecoundUnitPrice1.Size = new System.Drawing.Size(167, 20);
            this.txtSecoundUnitPrice1.StyleController = this.dataLayoutControl1;
            this.txtSecoundUnitPrice1.TabIndex = 544;
            // 
            // txtFiestUnitPrice3
            // 
            this.txtFiestUnitPrice3.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtFiestUnitPrice3.Location = new System.Drawing.Point(622, 539);
            this.txtFiestUnitPrice3.MenuManager = this.barManager1;
            this.txtFiestUnitPrice3.Name = "txtFiestUnitPrice3";
            this.txtFiestUnitPrice3.Properties.Appearance.Options.UseTextOptions = true;
            this.txtFiestUnitPrice3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtFiestUnitPrice3.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtFiestUnitPrice3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtFiestUnitPrice3.Size = new System.Drawing.Size(135, 20);
            this.txtFiestUnitPrice3.StyleController = this.dataLayoutControl1;
            this.txtFiestUnitPrice3.TabIndex = 543;
            // 
            // txtFiestUnitPrice2
            // 
            this.txtFiestUnitPrice2.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtFiestUnitPrice2.Location = new System.Drawing.Point(622, 515);
            this.txtFiestUnitPrice2.MenuManager = this.barManager1;
            this.txtFiestUnitPrice2.Name = "txtFiestUnitPrice2";
            this.txtFiestUnitPrice2.Properties.Appearance.Options.UseTextOptions = true;
            this.txtFiestUnitPrice2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtFiestUnitPrice2.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtFiestUnitPrice2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtFiestUnitPrice2.Size = new System.Drawing.Size(135, 20);
            this.txtFiestUnitPrice2.StyleController = this.dataLayoutControl1;
            this.txtFiestUnitPrice2.TabIndex = 542;
            // 
            // txtFiestUnitPrice1
            // 
            this.txtFiestUnitPrice1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtFiestUnitPrice1.Location = new System.Drawing.Point(622, 491);
            this.txtFiestUnitPrice1.MenuManager = this.barManager1;
            this.txtFiestUnitPrice1.Name = "txtFiestUnitPrice1";
            this.txtFiestUnitPrice1.Properties.Appearance.Options.UseTextOptions = true;
            this.txtFiestUnitPrice1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtFiestUnitPrice1.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtFiestUnitPrice1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtFiestUnitPrice1.Size = new System.Drawing.Size(135, 20);
            this.txtFiestUnitPrice1.StyleController = this.dataLayoutControl1;
            this.txtFiestUnitPrice1.TabIndex = 541;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(390, 117);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(142, 22);
            this.simpleButton1.StyleController = this.dataLayoutControl1;
            this.simpleButton1.TabIndex = 540;
            this.simpleButton1.Text = "اضافة مجموعة";
            this.simpleButton1.Click += new System.EventHandler(this.button1_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControl1.Location = new System.Drawing.Point(898, 405);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(345, 267);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsBehavior.ReadOnly = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // RbUnitDefoult3
            // 
            this.RbUnitDefoult3.Location = new System.Drawing.Point(36, 341);
            this.RbUnitDefoult3.Name = "RbUnitDefoult3";
            this.RbUnitDefoult3.Size = new System.Drawing.Size(262, 25);
            this.RbUnitDefoult3.TabIndex = 537;
            this.RbUnitDefoult3.TabStop = true;
            this.RbUnitDefoult3.Text = "افتراضي";
            this.RbUnitDefoult3.UseVisualStyleBackColor = true;
            this.RbUnitDefoult3.CheckedChanged += new System.EventHandler(this.RbUnitDefoult3_CheckedChanged);
            // 
            // txtProdecutBayPrice3
            // 
            this.txtProdecutBayPrice3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtProdecutBayPrice3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProdecutBayPrice3.Location = new System.Drawing.Point(36, 443);
            this.txtProdecutBayPrice3.MaxLength = 80;
            this.txtProdecutBayPrice3.Name = "txtProdecutBayPrice3";
            this.txtProdecutBayPrice3.ReadOnly = true;
            this.txtProdecutBayPrice3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtProdecutBayPrice3.Size = new System.Drawing.Size(161, 20);
            this.txtProdecutBayPrice3.TabIndex = 531;
            this.txtProdecutBayPrice3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtProdecutBayPrice3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Allprice);
            // 
            // RbUnitDefoult2
            // 
            this.RbUnitDefoult2.Location = new System.Drawing.Point(326, 341);
            this.RbUnitDefoult2.Name = "RbUnitDefoult2";
            this.RbUnitDefoult2.Size = new System.Drawing.Size(268, 25);
            this.RbUnitDefoult2.TabIndex = 537;
            this.RbUnitDefoult2.TabStop = true;
            this.RbUnitDefoult2.Text = "افتراضي";
            this.RbUnitDefoult2.UseVisualStyleBackColor = true;
            this.RbUnitDefoult2.CheckedChanged += new System.EventHandler(this.RbUnitDefoult2_CheckedChanged);
            // 
            // txtSecoundUnitBarcode
            // 
            this.txtSecoundUnitBarcode.Location = new System.Drawing.Point(326, 419);
            this.txtSecoundUnitBarcode.Name = "txtSecoundUnitBarcode";
            this.txtSecoundUnitBarcode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Search, "", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "توليد براكود تلقائي", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.txtSecoundUnitBarcode.Size = new System.Drawing.Size(167, 20);
            this.txtSecoundUnitBarcode.StyleController = this.dataLayoutControl1;
            this.txtSecoundUnitBarcode.TabIndex = 538;
            this.txtSecoundUnitBarcode.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.txtFiestUnitBarcode_ButtonClick);
            this.txtSecoundUnitBarcode.TextChanged += new System.EventHandler(this.txtSecoundUnitBarcode_TextChanged);
            // 
            // txtProdecutBayPrice2
            // 
            this.txtProdecutBayPrice2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtProdecutBayPrice2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProdecutBayPrice2.Location = new System.Drawing.Point(326, 443);
            this.txtProdecutBayPrice2.MaxLength = 80;
            this.txtProdecutBayPrice2.Name = "txtProdecutBayPrice2";
            this.txtProdecutBayPrice2.ReadOnly = true;
            this.txtProdecutBayPrice2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtProdecutBayPrice2.Size = new System.Drawing.Size(167, 20);
            this.txtProdecutBayPrice2.TabIndex = 3;
            this.txtProdecutBayPrice2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtProdecutBayPrice2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Allprice);
            // 
            // spinEdit1
            // 
            this.spinEdit1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit1.Location = new System.Drawing.Point(622, 443);
            this.spinEdit1.Name = "spinEdit1";
            this.spinEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEdit1.Size = new System.Drawing.Size(135, 20);
            this.spinEdit1.StyleController = this.dataLayoutControl1;
            this.spinEdit1.TabIndex = 539;
            // 
            // txtThreeUnitOperating
            // 
            this.txtThreeUnitOperating.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtThreeUnitOperating.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThreeUnitOperating.Location = new System.Drawing.Point(36, 395);
            this.txtThreeUnitOperating.MaxLength = 10;
            this.txtThreeUnitOperating.Name = "txtThreeUnitOperating";
            this.txtThreeUnitOperating.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtThreeUnitOperating.Size = new System.Drawing.Size(161, 20);
            this.txtThreeUnitOperating.TabIndex = 1;
            this.txtThreeUnitOperating.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtThreeUnitOperating.TextChanged += new System.EventHandler(this.txtThreeUnitOperating_TextChanged);
            this.txtThreeUnitOperating.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Allprice);
            // 
            // txtFiestUnitBarcode
            // 
            this.txtFiestUnitBarcode.Location = new System.Drawing.Point(622, 419);
            this.txtFiestUnitBarcode.Name = "txtFiestUnitBarcode";
            this.txtFiestUnitBarcode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Search, "", -1, true, true, false, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "توليد براكود تلقائي", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.txtFiestUnitBarcode.Size = new System.Drawing.Size(135, 20);
            this.txtFiestUnitBarcode.StyleController = this.dataLayoutControl1;
            this.txtFiestUnitBarcode.TabIndex = 538;
            this.txtFiestUnitBarcode.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.txtFiestUnitBarcode_ButtonClick);
            this.txtFiestUnitBarcode.TextChanged += new System.EventHandler(this.txtFiestUnitBarcode_TextChanged);
            // 
            // txtSecoundUnitOperating
            // 
            this.txtSecoundUnitOperating.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSecoundUnitOperating.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSecoundUnitOperating.Location = new System.Drawing.Point(326, 395);
            this.txtSecoundUnitOperating.MaxLength = 10;
            this.txtSecoundUnitOperating.Name = "txtSecoundUnitOperating";
            this.txtSecoundUnitOperating.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtSecoundUnitOperating.Size = new System.Drawing.Size(167, 20);
            this.txtSecoundUnitOperating.TabIndex = 1;
            this.txtSecoundUnitOperating.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSecoundUnitOperating.TextChanged += new System.EventHandler(this.txtSecoundUnitOperating_TextChanged);
            this.txtSecoundUnitOperating.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Allprice);
            // 
            // RbUnitDefoult1
            // 
            this.RbUnitDefoult1.Checked = true;
            this.RbUnitDefoult1.Location = new System.Drawing.Point(622, 341);
            this.RbUnitDefoult1.Name = "RbUnitDefoult1";
            this.RbUnitDefoult1.Size = new System.Drawing.Size(236, 25);
            this.RbUnitDefoult1.TabIndex = 537;
            this.RbUnitDefoult1.TabStop = true;
            this.RbUnitDefoult1.Text = "افتراضي";
            this.RbUnitDefoult1.UseVisualStyleBackColor = true;
            this.RbUnitDefoult1.CheckedChanged += new System.EventHandler(this.RbUnitDefoult1_CheckedChanged);
            // 
            // combThreeUnit
            // 
            this.combThreeUnit.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.combThreeUnit.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.combThreeUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combThreeUnit.FormattingEnabled = true;
            this.combThreeUnit.Location = new System.Drawing.Point(36, 370);
            this.combThreeUnit.Name = "combThreeUnit";
            this.combThreeUnit.Size = new System.Drawing.Size(161, 24);
            this.combThreeUnit.TabIndex = 0;
            this.combThreeUnit.TextChanged += new System.EventHandler(this.combThreeUnit_TextChanged);
            // 
            // chk_HasSerial
            // 
            this.chk_HasSerial.Location = new System.Drawing.Point(36, 102);
            this.chk_HasSerial.Name = "chk_HasSerial";
            this.chk_HasSerial.Properties.Caption = "له رقم تسلسلي";
            this.chk_HasSerial.Size = new System.Drawing.Size(245, 20);
            this.chk_HasSerial.StyleController = this.dataLayoutControl1;
            this.chk_HasSerial.TabIndex = 15;
            // 
            // txtFiestUnitOperating
            // 
            this.txtFiestUnitOperating.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFiestUnitOperating.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFiestUnitOperating.Location = new System.Drawing.Point(622, 395);
            this.txtFiestUnitOperating.MaxLength = 10;
            this.txtFiestUnitOperating.Name = "txtFiestUnitOperating";
            this.txtFiestUnitOperating.ReadOnly = true;
            this.txtFiestUnitOperating.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtFiestUnitOperating.Size = new System.Drawing.Size(135, 20);
            this.txtFiestUnitOperating.TabIndex = 529;
            this.txtFiestUnitOperating.Text = "1";
            this.txtFiestUnitOperating.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFiestUnitOperating.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Allprice);
            // 
            // chk_HasExpire
            // 
            this.chk_HasExpire.Location = new System.Drawing.Point(285, 102);
            this.chk_HasExpire.Name = "chk_HasExpire";
            this.chk_HasExpire.Properties.Caption = "له صلاحيه";
            this.chk_HasExpire.Size = new System.Drawing.Size(89, 20);
            this.chk_HasExpire.StyleController = this.dataLayoutControl1;
            this.chk_HasExpire.TabIndex = 14;
            // 
            // combSecoundUnit
            // 
            this.combSecoundUnit.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.combSecoundUnit.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.combSecoundUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combSecoundUnit.FormattingEnabled = true;
            this.combSecoundUnit.Location = new System.Drawing.Point(326, 370);
            this.combSecoundUnit.Name = "combSecoundUnit";
            this.combSecoundUnit.Size = new System.Drawing.Size(167, 24);
            this.combSecoundUnit.TabIndex = 0;
            this.combSecoundUnit.TextChanged += new System.EventHandler(this.combSecoundUnit_TextChanged);
            // 
            // chk_HasSize
            // 
            this.chk_HasSize.Location = new System.Drawing.Point(36, 78);
            this.chk_HasSize.Name = "chk_HasSize";
            this.chk_HasSize.Properties.Caption = "له حجم";
            this.chk_HasSize.Size = new System.Drawing.Size(245, 20);
            this.chk_HasSize.StyleController = this.dataLayoutControl1;
            this.chk_HasSize.TabIndex = 13;
            // 
            // chk_HasColor
            // 
            this.chk_HasColor.Location = new System.Drawing.Point(285, 78);
            this.chk_HasColor.Name = "chk_HasColor";
            this.chk_HasColor.Properties.Caption = "له لون";
            this.chk_HasColor.Size = new System.Drawing.Size(89, 20);
            this.chk_HasColor.StyleController = this.dataLayoutControl1;
            this.chk_HasColor.TabIndex = 12;
            // 
            // chProductService
            // 
            this.chProductService.Location = new System.Drawing.Point(390, 93);
            this.chProductService.Name = "chProductService";
            this.chProductService.Size = new System.Drawing.Size(257, 20);
            this.chProductService.TabIndex = 5;
            this.chProductService.Text = "الصنف خدمي";
            this.chProductService.UseVisualStyleBackColor = true;
            // 
            // DGV1
            // 
            this.DGV1.AllowUserToAddRows = false;
            this.DGV1.AllowUserToDeleteRows = false;
            this.DGV1.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FloralWhite;
            this.DGV1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.DGV1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGV1.BackgroundColor = System.Drawing.Color.White;
            this.DGV1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(146)))), ((int)(((byte)(222)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 10F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGV1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.DGV1.ColumnHeadersHeight = 24;
            this.DGV1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.DGV1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.C1,
            this.ProdecutID,
            this.ProdecutName});
            this.DGV1.Cursor = System.Windows.Forms.Cursors.Hand;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Tahoma", 10F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGV1.DefaultCellStyle = dataGridViewCellStyle3;
            this.DGV1.EnableHeadersVisualStyles = false;
            this.DGV1.GridColor = System.Drawing.Color.Silver;
            this.DGV1.Location = new System.Drawing.Point(898, 45);
            this.DGV1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.DGV1.MultiSelect = false;
            this.DGV1.Name = "DGV1";
            this.DGV1.ReadOnly = true;
            this.DGV1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Tahoma", 10F);
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGV1.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.DGV1.RowHeadersWidth = 25;
            this.DGV1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.DGV1.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.DGV1.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.DGV1.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Tahoma", 9F);
            this.DGV1.RowTemplate.Height = 20;
            this.DGV1.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.DGV1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGV1.Size = new System.Drawing.Size(345, 311);
            this.DGV1.TabIndex = 0;
            // 
            // C1
            // 
            this.C1.FillWeight = 50.48077F;
            this.C1.HeaderText = "التفاصيل";
            this.C1.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.C1.Name = "C1";
            this.C1.ReadOnly = true;
            // 
            // ProdecutID
            // 
            this.ProdecutID.DataPropertyName = "ProdecutID";
            this.ProdecutID.FillWeight = 104.431F;
            this.ProdecutID.HeaderText = "كود الصنف";
            this.ProdecutID.Name = "ProdecutID";
            this.ProdecutID.ReadOnly = true;
            this.ProdecutID.Visible = false;
            // 
            // ProdecutName
            // 
            this.ProdecutName.DataPropertyName = "ProdecutName";
            this.ProdecutName.FillWeight = 167.7984F;
            this.ProdecutName.HeaderText = "اسم الصنف";
            this.ProdecutName.Name = "ProdecutName";
            this.ProdecutName.ReadOnly = true;
            // 
            // chStatus
            // 
            this.chStatus.Checked = true;
            this.chStatus.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chStatus.Location = new System.Drawing.Point(651, 93);
            this.chStatus.Name = "chStatus";
            this.chStatus.Size = new System.Drawing.Size(219, 20);
            this.chStatus.TabIndex = 4;
            this.chStatus.Text = "الصنف نشط";
            this.chStatus.UseVisualStyleBackColor = true;
            // 
            // txtProdecutID
            // 
            this.txtProdecutID.BackColor = System.Drawing.Color.White;
            this.txtProdecutID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtProdecutID.Enabled = false;
            this.txtProdecutID.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProdecutID.Location = new System.Drawing.Point(390, 45);
            this.txtProdecutID.Margin = new System.Windows.Forms.Padding(4);
            this.txtProdecutID.Name = "txtProdecutID";
            this.txtProdecutID.ReadOnly = true;
            this.txtProdecutID.Size = new System.Drawing.Size(379, 20);
            this.txtProdecutID.TabIndex = 2;
            this.txtProdecutID.TextChanged += new System.EventHandler(this.txtProdecutID_TextChanged);
            // 
            // txtDiscoundSale
            // 
            this.txtDiscoundSale.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDiscoundSale.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiscoundSale.Location = new System.Drawing.Point(390, 239);
            this.txtDiscoundSale.MaxLength = 10;
            this.txtDiscoundSale.Name = "txtDiscoundSale";
            this.txtDiscoundSale.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtDiscoundSale.Size = new System.Drawing.Size(379, 20);
            this.txtDiscoundSale.TabIndex = 11;
            this.txtDiscoundSale.Text = "0";
            this.txtDiscoundSale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtProdecutName
            // 
            this.txtProdecutName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtProdecutName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProdecutName.Location = new System.Drawing.Point(390, 69);
            this.txtProdecutName.Margin = new System.Windows.Forms.Padding(4);
            this.txtProdecutName.Name = "txtProdecutName";
            this.txtProdecutName.Size = new System.Drawing.Size(379, 20);
            this.txtProdecutName.TabIndex = 3;
            this.txtProdecutName.TextChanged += new System.EventHandler(this.txtItemName_TextChanged);
            // 
            // combGroup
            // 
            this.combGroup.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.combGroup.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.combGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combGroup.FormattingEnabled = true;
            this.combGroup.Location = new System.Drawing.Point(536, 117);
            this.combGroup.Name = "combGroup";
            this.combGroup.Size = new System.Drawing.Size(233, 24);
            this.combGroup.TabIndex = 6;
            this.combGroup.SelectedIndexChanged += new System.EventHandler(this.combGroup_SelectedIndexChanged);
            this.combGroup.TextChanged += new System.EventHandler(this.combGroup_TextChanged);
            // 
            // txtProdecutLocation
            // 
            this.txtProdecutLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtProdecutLocation.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProdecutLocation.Location = new System.Drawing.Point(390, 143);
            this.txtProdecutLocation.MaxLength = 80;
            this.txtProdecutLocation.Name = "txtProdecutLocation";
            this.txtProdecutLocation.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtProdecutLocation.Size = new System.Drawing.Size(379, 20);
            this.txtProdecutLocation.TabIndex = 7;
            this.txtProdecutLocation.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // combFiestUnit
            // 
            this.combFiestUnit.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.combFiestUnit.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.combFiestUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combFiestUnit.FormattingEnabled = true;
            this.combFiestUnit.Location = new System.Drawing.Point(622, 370);
            this.combFiestUnit.Name = "combFiestUnit";
            this.combFiestUnit.Size = new System.Drawing.Size(135, 24);
            this.combFiestUnit.TabIndex = 0;
            this.combFiestUnit.TextChanged += new System.EventHandler(this.combFiestUnit_TextChanged);
            this.combFiestUnit.VisibleChanged += new System.EventHandler(this.combFiestUnit_VisibleChanged);
            // 
            // txtDiscoundBay
            // 
            this.txtDiscoundBay.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDiscoundBay.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiscoundBay.Location = new System.Drawing.Point(390, 215);
            this.txtDiscoundBay.MaxLength = 10;
            this.txtDiscoundBay.Name = "txtDiscoundBay";
            this.txtDiscoundBay.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtDiscoundBay.Size = new System.Drawing.Size(379, 20);
            this.txtDiscoundBay.TabIndex = 10;
            this.txtDiscoundBay.Text = "0";
            this.txtDiscoundBay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtCompany
            // 
            this.txtCompany.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCompany.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCompany.Location = new System.Drawing.Point(390, 167);
            this.txtCompany.MaxLength = 80;
            this.txtCompany.Name = "txtCompany";
            this.txtCompany.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtCompany.Size = new System.Drawing.Size(379, 20);
            this.txtCompany.TabIndex = 8;
            this.txtCompany.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtRequestLimit
            // 
            this.txtRequestLimit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRequestLimit.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRequestLimit.Location = new System.Drawing.Point(390, 191);
            this.txtRequestLimit.MaxLength = 80;
            this.txtRequestLimit.Name = "txtRequestLimit";
            this.txtRequestLimit.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtRequestLimit.Size = new System.Drawing.Size(379, 20);
            this.txtRequestLimit.TabIndex = 9;
            this.txtRequestLimit.Text = "0";
            this.txtRequestLimit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup1,
            this.layoutControlGroup8,
            this.layoutControlGroup2,
            this.layoutControlGroup7});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(1267, 696);
            this.Root.TextVisible = false;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(874, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(373, 360);
            this.layoutControlGroup1.Text = "الاصناف";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.DGV1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(349, 315);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup4,
            this.layoutControlGroup5,
            this.layoutControlGroup6});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 263);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Size = new System.Drawing.Size(874, 413);
            this.layoutControlGroup8.Text = "وحدات الصنف واسعارها";
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem16,
            this.layoutControlItem18,
            this.layoutControlItem19,
            this.layoutControlItem20,
            this.layoutControlItem17,
            this.layoutControlItem43,
            this.layoutControlItem44,
            this.layoutControlItem45,
            this.layoutControlItem21});
            this.layoutControlGroup4.Location = new System.Drawing.Point(586, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(264, 368);
            this.layoutControlGroup4.Text = "الوحدة الأولي";
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.combFiestUnit;
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 29);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(240, 25);
            this.layoutControlItem16.Text = "وحدة القياس";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.txtFiestUnitOperating;
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 54);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(240, 24);
            this.layoutControlItem18.Text = "معدل التحويل";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.txtFiestUnitBarcode;
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 78);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(240, 24);
            this.layoutControlItem19.Text = "الباركود";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.spinEdit1;
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 102);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(240, 24);
            this.layoutControlItem20.Text = "الرصيد الافتتاحي";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.RbUnitDefoult1;
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(240, 29);
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextVisible = false;
            // 
            // layoutControlItem43
            // 
            this.layoutControlItem43.Control = this.txtFiestUnitPrice1;
            this.layoutControlItem43.Location = new System.Drawing.Point(0, 150);
            this.layoutControlItem43.Name = "layoutControlItem43";
            this.layoutControlItem43.Size = new System.Drawing.Size(240, 24);
            this.layoutControlItem43.Text = "سعر بيع 1";
            this.layoutControlItem43.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem44
            // 
            this.layoutControlItem44.Control = this.txtFiestUnitPrice2;
            this.layoutControlItem44.Location = new System.Drawing.Point(0, 174);
            this.layoutControlItem44.Name = "layoutControlItem44";
            this.layoutControlItem44.Size = new System.Drawing.Size(240, 24);
            this.layoutControlItem44.Text = "سعر بيع 2";
            this.layoutControlItem44.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem45
            // 
            this.layoutControlItem45.Control = this.txtFiestUnitPrice3;
            this.layoutControlItem45.Location = new System.Drawing.Point(0, 198);
            this.layoutControlItem45.Name = "layoutControlItem45";
            this.layoutControlItem45.Size = new System.Drawing.Size(240, 125);
            this.layoutControlItem45.Text = "سعر بيع 3";
            this.layoutControlItem45.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.txtProdecutBayPrice;
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 126);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(240, 24);
            this.layoutControlItem21.Text = "سعر الشراء";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem25,
            this.layoutControlItem26,
            this.layoutControlItem27,
            this.layoutControlItem28,
            this.layoutControlItem32,
            this.layoutControlItem46,
            this.layoutControlItem47,
            this.layoutControlItem48});
            this.layoutControlGroup5.Location = new System.Drawing.Point(290, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(296, 368);
            this.layoutControlGroup5.Text = "الوحدة الثانية";
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.Control = this.combSecoundUnit;
            this.layoutControlItem25.Location = new System.Drawing.Point(0, 29);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(272, 25);
            this.layoutControlItem25.Text = "وحدة القياس";
            this.layoutControlItem25.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.Control = this.txtSecoundUnitOperating;
            this.layoutControlItem26.Location = new System.Drawing.Point(0, 54);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(272, 24);
            this.layoutControlItem26.Text = "معدل التحويل";
            this.layoutControlItem26.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.Control = this.txtSecoundUnitBarcode;
            this.layoutControlItem27.Location = new System.Drawing.Point(0, 78);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(272, 24);
            this.layoutControlItem27.Text = "الباركود";
            this.layoutControlItem27.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.Control = this.txtProdecutBayPrice2;
            this.layoutControlItem28.Location = new System.Drawing.Point(0, 102);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(272, 24);
            this.layoutControlItem28.Text = "سعر الشراء";
            this.layoutControlItem28.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.Control = this.RbUnitDefoult2;
            this.layoutControlItem32.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(272, 29);
            this.layoutControlItem32.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem32.TextVisible = false;
            // 
            // layoutControlItem46
            // 
            this.layoutControlItem46.Control = this.txtSecoundUnitPrice1;
            this.layoutControlItem46.Location = new System.Drawing.Point(0, 126);
            this.layoutControlItem46.Name = "layoutControlItem46";
            this.layoutControlItem46.Size = new System.Drawing.Size(272, 24);
            this.layoutControlItem46.Text = "سعر بيع 1";
            this.layoutControlItem46.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem47
            // 
            this.layoutControlItem47.Control = this.txtSecoundUnitPrice2;
            this.layoutControlItem47.Location = new System.Drawing.Point(0, 150);
            this.layoutControlItem47.Name = "layoutControlItem47";
            this.layoutControlItem47.Size = new System.Drawing.Size(272, 24);
            this.layoutControlItem47.Text = "سعر بيع 2";
            this.layoutControlItem47.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem48
            // 
            this.layoutControlItem48.Control = this.txtSecoundUnitPrice3;
            this.layoutControlItem48.Location = new System.Drawing.Point(0, 174);
            this.layoutControlItem48.Name = "layoutControlItem48";
            this.layoutControlItem48.Size = new System.Drawing.Size(272, 149);
            this.layoutControlItem48.Text = "سعر بيع 3";
            this.layoutControlItem48.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem33,
            this.layoutControlItem34,
            this.layoutControlItem35,
            this.layoutControlItem36,
            this.layoutControlItem40,
            this.layoutControlItem49,
            this.layoutControlItem50,
            this.layoutControlItem51});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(290, 368);
            this.layoutControlGroup6.Text = "الوحدة الثالثه";
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.Control = this.combThreeUnit;
            this.layoutControlItem33.Location = new System.Drawing.Point(0, 29);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(266, 25);
            this.layoutControlItem33.Text = "وحدة القياس";
            this.layoutControlItem33.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.Control = this.txtThreeUnitOperating;
            this.layoutControlItem34.Location = new System.Drawing.Point(0, 54);
            this.layoutControlItem34.Name = "layoutControlItem34";
            this.layoutControlItem34.Size = new System.Drawing.Size(266, 24);
            this.layoutControlItem34.Text = "معدل التحويل";
            this.layoutControlItem34.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.Control = this.txtThreeUnitBarcode;
            this.layoutControlItem35.Location = new System.Drawing.Point(0, 78);
            this.layoutControlItem35.Name = "layoutControlItem35";
            this.layoutControlItem35.Size = new System.Drawing.Size(266, 24);
            this.layoutControlItem35.Text = "الباركود";
            this.layoutControlItem35.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.Control = this.txtProdecutBayPrice3;
            this.layoutControlItem36.Location = new System.Drawing.Point(0, 102);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(266, 24);
            this.layoutControlItem36.Text = "سعر الشراء";
            this.layoutControlItem36.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem40
            // 
            this.layoutControlItem40.Control = this.RbUnitDefoult3;
            this.layoutControlItem40.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem40.Name = "layoutControlItem40";
            this.layoutControlItem40.Size = new System.Drawing.Size(266, 29);
            this.layoutControlItem40.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem40.TextVisible = false;
            // 
            // layoutControlItem49
            // 
            this.layoutControlItem49.Control = this.txtThreeUnitPrice1;
            this.layoutControlItem49.Location = new System.Drawing.Point(0, 126);
            this.layoutControlItem49.Name = "layoutControlItem49";
            this.layoutControlItem49.Size = new System.Drawing.Size(266, 24);
            this.layoutControlItem49.Text = "سعر بيع 1";
            this.layoutControlItem49.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem50
            // 
            this.layoutControlItem50.Control = this.txtThreeUnitPrice2;
            this.layoutControlItem50.Location = new System.Drawing.Point(0, 150);
            this.layoutControlItem50.Name = "layoutControlItem50";
            this.layoutControlItem50.Size = new System.Drawing.Size(266, 24);
            this.layoutControlItem50.Text = "سعر بيع 2";
            this.layoutControlItem50.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem51
            // 
            this.layoutControlItem51.Control = this.txtThreeUnitPrice3;
            this.layoutControlItem51.Location = new System.Drawing.Point(0, 174);
            this.layoutControlItem51.Name = "layoutControlItem51";
            this.layoutControlItem51.Size = new System.Drawing.Size(266, 149);
            this.layoutControlItem51.Text = "سعر بيع 3";
            this.layoutControlItem51.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem42,
            this.layoutControlGroup3});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(874, 263);
            this.layoutControlGroup2.Text = "البيانات الاساسية";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txtProdecutID;
            this.layoutControlItem2.Location = new System.Drawing.Point(366, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(484, 24);
            this.layoutControlItem2.Text = "رقم الصنف :";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txtProdecutName;
            this.layoutControlItem3.Location = new System.Drawing.Point(366, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(484, 24);
            this.layoutControlItem3.Text = "اسم الصنف :";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.combGroup;
            this.layoutControlItem4.Location = new System.Drawing.Point(512, 72);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(338, 26);
            this.layoutControlItem4.Text = "مجموعة  :";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.txtProdecutLocation;
            this.layoutControlItem5.Location = new System.Drawing.Point(366, 98);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(484, 24);
            this.layoutControlItem5.Text = "مكان التواجد :";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.txtCompany;
            this.layoutControlItem6.Location = new System.Drawing.Point(366, 122);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(484, 24);
            this.layoutControlItem6.Text = "الشركة المصنعة :";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.txtRequestLimit;
            this.layoutControlItem7.Location = new System.Drawing.Point(366, 146);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(484, 24);
            this.layoutControlItem7.Text = "حد الطلب :";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.txtDiscoundBay;
            this.layoutControlItem8.Location = new System.Drawing.Point(366, 170);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(484, 24);
            this.layoutControlItem8.Text = "خصم عند الشراء % :";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.txtDiscoundSale;
            this.layoutControlItem9.Location = new System.Drawing.Point(366, 194);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(484, 24);
            this.layoutControlItem9.Text = "خصم عند البيع % :";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.chStatus;
            this.layoutControlItem10.Location = new System.Drawing.Point(627, 48);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(223, 24);
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.chProductService;
            this.layoutControlItem11.Location = new System.Drawing.Point(366, 48);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(261, 24);
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlItem42
            // 
            this.layoutControlItem42.Control = this.simpleButton1;
            this.layoutControlItem42.Location = new System.Drawing.Point(366, 72);
            this.layoutControlItem42.Name = "layoutControlItem42";
            this.layoutControlItem42.Size = new System.Drawing.Size(146, 26);
            this.layoutControlItem42.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem42.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem12,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem15});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(366, 218);
            this.layoutControlGroup3.Text = "خصائص";
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.chk_HasColor;
            this.layoutControlItem12.Location = new System.Drawing.Point(249, 0);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(93, 24);
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.chk_HasSize;
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(249, 24);
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextVisible = false;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.chk_HasExpire;
            this.layoutControlItem14.Location = new System.Drawing.Point(249, 24);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(93, 149);
            this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem14.TextVisible = false;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.chk_HasSerial;
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(249, 149);
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextVisible = false;
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem41});
            this.layoutControlGroup7.Location = new System.Drawing.Point(874, 360);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(373, 316);
            this.layoutControlGroup7.Text = "رصيد الصنف في المخازن";
            // 
            // layoutControlItem41
            // 
            this.layoutControlItem41.Control = this.gridControl1;
            this.layoutControlItem41.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem41.Name = "layoutControlItem41";
            this.layoutControlItem41.Size = new System.Drawing.Size(349, 271);
            this.layoutControlItem41.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem41.TextVisible = false;
            // 
            // btnGetData
            // 
            this.btnGetData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(160)))), ((int)(((byte)(124)))));
            this.btnGetData.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGetData.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnGetData.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGetData.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGetData.ForeColor = System.Drawing.Color.White;
            this.btnGetData.Location = new System.Drawing.Point(1, 443);
            this.btnGetData.Name = "btnGetData";
            this.btnGetData.Size = new System.Drawing.Size(91, 31);
            this.btnGetData.TabIndex = 506;
            this.btnGetData.Text = "بحث";
            this.btnGetData.UseVisualStyleBackColor = false;
            this.btnGetData.Visible = false;
            this.btnGetData.Click += new System.EventHandler(this.btnGetData_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(160)))), ((int)(((byte)(124)))));
            this.btnDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDelete.Enabled = false;
            this.btnDelete.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ForeColor = System.Drawing.Color.White;
            this.btnDelete.Location = new System.Drawing.Point(110, 406);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(91, 31);
            this.btnDelete.TabIndex = 505;
            this.btnDelete.Text = "حذف";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Visible = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(160)))), ((int)(((byte)(124)))));
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(110, 443);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(91, 31);
            this.btnClose.TabIndex = 507;
            this.btnClose.Text = "اغلاق";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Visible = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(160)))), ((int)(((byte)(124)))));
            this.btnSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSave.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(1, 360);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(91, 31);
            this.btnSave.TabIndex = 503;
            this.btnSave.Text = "حفظ";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Visible = false;
            this.btnSave.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(160)))), ((int)(((byte)(124)))));
            this.btnUpdate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUpdate.Enabled = false;
            this.btnUpdate.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.ForeColor = System.Drawing.Color.White;
            this.btnUpdate.Location = new System.Drawing.Point(110, 360);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(91, 31);
            this.btnUpdate.TabIndex = 504;
            this.btnUpdate.Text = "تعديل";
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Visible = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnNew
            // 
            this.btnNew.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(160)))), ((int)(((byte)(124)))));
            this.btnNew.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNew.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNew.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNew.ForeColor = System.Drawing.Color.White;
            this.btnNew.Location = new System.Drawing.Point(1, 406);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(91, 31);
            this.btnNew.TabIndex = 502;
            this.btnNew.Text = "جديد";
            this.btnNew.UseVisualStyleBackColor = false;
            this.btnNew.Visible = false;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // Prodecut_frm
            // 
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1267, 720);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.txtBarcode3);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.btnGetData);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnNew);
            this.Font = new System.Drawing.Font("Tahoma", 10F);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Prodecut_frm";
            this.Text = "شاشة الاصناف";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Prodecut_frm_FormClosing);
            this.Load += new System.EventHandler(this.ITEM_FRM_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Prodecut_frm_KeyDown);
            this.Controls.SetChildIndex(this.btnNew, 0);
            this.Controls.SetChildIndex(this.btnUpdate, 0);
            this.Controls.SetChildIndex(this.btnSave, 0);
            this.Controls.SetChildIndex(this.btnClose, 0);
            this.Controls.SetChildIndex(this.btnDelete, 0);
            this.Controls.SetChildIndex(this.btnGetData, 0);
            this.Controls.SetChildIndex(this.label13, 0);
            this.Controls.SetChildIndex(this.txtBarcode3, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.txtThreeUnitBarcode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtProdecutBayPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThreeUnitPrice3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThreeUnitPrice2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThreeUnitPrice1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecoundUnitPrice3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecoundUnitPrice2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecoundUnitPrice1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFiestUnitPrice3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFiestUnitPrice2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFiestUnitPrice1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecoundUnitBarcode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFiestUnitBarcode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_HasSerial.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_HasExpire.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_HasSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_HasColor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGV1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.TextBox txtBarcode3;
        private System.Windows.Forms.Label label13;
        public System.Windows.Forms.TextBox txtProdecutName;
        public System.Windows.Forms.TextBox txtProdecutLocation;
        public System.Windows.Forms.TextBox txtProdecutID;
        private System.Windows.Forms.ComboBox combGroup;
        public System.Windows.Forms.TextBox txtCompany;
        internal System.Windows.Forms.Button btnGetData;
        internal System.Windows.Forms.Button btnDelete;
        internal System.Windows.Forms.Button btnClose;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.Button btnUpdate;
        internal System.Windows.Forms.Button btnNew;
        public System.Windows.Forms.TextBox txtRequestLimit;
        public System.Windows.Forms.TextBox txtProdecutBayPrice3;
        public System.Windows.Forms.TextBox txtThreeUnitOperating;
        private System.Windows.Forms.ComboBox combThreeUnit;
        public System.Windows.Forms.TextBox txtFiestUnitOperating;
        private System.Windows.Forms.ComboBox combFiestUnit;
        public System.Windows.Forms.TextBox txtProdecutBayPrice2;
        public System.Windows.Forms.TextBox txtSecoundUnitOperating;
        private System.Windows.Forms.ComboBox combSecoundUnit;
        private System.Windows.Forms.CheckBox chStatus;
        private System.Windows.Forms.ToolTip toolTip1;
        public System.Windows.Forms.TextBox txtDiscoundBay;
        public System.Windows.Forms.TextBox txtDiscoundSale;
        private System.Windows.Forms.RadioButton RbUnitDefoult3;
        private System.Windows.Forms.RadioButton RbUnitDefoult1;
        private System.Windows.Forms.RadioButton RbUnitDefoult2;
        private System.Windows.Forms.CheckBox chProductService;
        private DevExpress.XtraEditors.ButtonEdit txtFiestUnitBarcode;
        private DevExpress.XtraEditors.ButtonEdit txtThreeUnitBarcode;
        private DevExpress.XtraEditors.ButtonEdit txtSecoundUnitBarcode;
        private DevExpress.XtraEditors.SpinEdit spinEdit1;
        private DevExpress.XtraEditors.CheckEdit chk_HasSize;
        private DevExpress.XtraEditors.CheckEdit chk_HasExpire;
        private DevExpress.XtraEditors.CheckEdit chk_HasColor;
        private DevExpress.XtraEditors.CheckEdit chk_HasSerial;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        public System.Windows.Forms.DataGridView DGV1;
        private System.Windows.Forms.DataGridViewImageColumn C1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProdecutID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProdecutName;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem41;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem40;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem42;
        private DevExpress.XtraEditors.SpinEdit txtProdecutBayPrice;
        private DevExpress.XtraEditors.SpinEdit txtThreeUnitPrice3;
        private DevExpress.XtraEditors.SpinEdit txtThreeUnitPrice2;
        private DevExpress.XtraEditors.SpinEdit txtThreeUnitPrice1;
        private DevExpress.XtraEditors.SpinEdit txtSecoundUnitPrice3;
        private DevExpress.XtraEditors.SpinEdit txtSecoundUnitPrice2;
        private DevExpress.XtraEditors.SpinEdit txtSecoundUnitPrice1;
        private DevExpress.XtraEditors.SpinEdit txtFiestUnitPrice3;
        private DevExpress.XtraEditors.SpinEdit txtFiestUnitPrice2;
        private DevExpress.XtraEditors.SpinEdit txtFiestUnitPrice1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem43;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem44;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem45;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem46;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem47;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem48;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem49;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem50;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem51;
    }
}