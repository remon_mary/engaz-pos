﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class Money_Customers_frm : Form
    {
        public Money_Customers_frm()
        {
            InitializeComponent();
        }
        Money_Customer_cls cls = new Money_Customer_cls();

        private void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();
                if (txtCustomerID.Text.Trim() == "")
                {
                    dt = cls.Money_Prodects_Sales_Details(D1.Value, D2.Value);
                }
                else 
                {
                    dt = cls.Money_Prodects(D1.Value, D2.Value, txtCustomerID.Text);
                }

                DGV1.AutoGenerateColumns = false;
                DGV1.DataSource = dt;
                lblCount.Text = DGV1.Rows.Count.ToString();
                sum();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void Money_Prodects_frm_Load(object sender, EventArgs e)
        {
            try
            {
                ERB_Setting.SettingToolStrip(toolStrip1);
                ERB_Setting.SettingForm(this);
                ERB_Setting.SettingDGV(DGV1);
                // FILL Combo Group item
                Customers_cls Customers_cls = new Customers_cls();
                combCustomers.DataSource = Customers_cls.Search_Customers("");
                combCustomers.DisplayMember = "CustomerName";
                combCustomers.ValueMember = "CustomerID";
                combCustomers.Text = "";
                txtCustomerID.Text = "";



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void DGV1_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
           
        }

      private void sum()
        {
            double sum = 0;
            for (int i = 0; i < DGV1.Rows.Count; i++)
            {

                sum += Convert.ToDouble(DGV1.Rows[i].Cells["TotalProfits"].Value);
            }

            lblSum.Text = sum.ToString("0.00");
        }
       
        private void combProdect_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                combCustomers.BackColor = Color.White;
                txtCustomerID.Text = combCustomers.SelectedValue.ToString();

            }
            catch
            {

                txtCustomerID.Text = "";

            }

        }

        private void combProdect_TextChanged(object sender, EventArgs e)
        {
            try
            {
                combCustomers.BackColor = Color.White;

                if (combCustomers.Text == "")
                {
                    txtCustomerID.Text = "";
                }
                else
                {
                    txtCustomerID.Text = combCustomers.SelectedValue.ToString();
                }
            }
            catch
            {
                txtCustomerID.Text = "";
            }
        }
    }
}
