﻿namespace ByStro.PL
{
    partial class BillSales_frm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BillSales_frm));
            this.txtMainID = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.D1 = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.txtStoreName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.txtStoreID = new System.Windows.Forms.TextBox();
            this.txtProdecutID = new System.Windows.Forms.TextBox();
            this.txtUnitFactor = new System.Windows.Forms.TextBox();
            this.txtUnitOperating = new System.Windows.Forms.TextBox();
            this.txtBillTypeID = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripDropDownButton2 = new System.Windows.Forms.ToolStripDropDownButton();
            this.CS_44 = new System.Windows.Forms.ToolStripMenuItem();
            this.CustomerTrsnse = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.اعادةتحميلالاصنافToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.مستوياسعارToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SalesLevel1 = new System.Windows.Forms.ToolStripMenuItem();
            this.SalesLevel2 = new System.Windows.Forms.ToolStripMenuItem();
            this.SalesLevel3 = new System.Windows.Forms.ToolStripMenuItem();
            this.تحميلالاصنافمنفاتورةعرضسعرToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnNew = new System.Windows.Forms.ToolStripButton();
            this.btnSave = new System.Windows.Forms.ToolStripButton();
            this.btnSave_Print = new System.Windows.Forms.ToolStripButton();
            this.btnUpdate = new System.Windows.Forms.ToolStripButton();
            this.btnDelete = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.txtAccountID = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtRestInvoise = new System.Windows.Forms.TextBox();
            this.txtpaid_Invoice = new System.Windows.Forms.TextBox();
            this.txtNetInvoice = new System.Windows.Forms.TextBox();
            this.txtTotalDescound = new System.Windows.Forms.TextBox();
            this.txtCustomersBlanse = new System.Windows.Forms.TextBox();
            this.txtTotalInvoice = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.combPayKind = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtProdecutAvg = new System.Windows.Forms.TextBox();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.txtBalence = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.txtTotalBayPrice = new System.Windows.Forms.TextBox();
            this.combAccount = new System.Windows.Forms.ComboBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.bunViewQuentityStore_frm = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.txtVatValue = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.statusStrip2 = new System.Windows.Forms.StatusStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.txtSumPayType = new System.Windows.Forms.ToolStripStatusLabel();
            this.DGVPayType = new System.Windows.Forms.DataGridView();
            this.PayTypeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PayTypeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrencyPrice2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Statement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrencyID2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrencyName2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrencyRate2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PayValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtStatement = new System.Windows.Forms.TextBox();
            this.txtPayValue = new System.Windows.Forms.TextBox();
            this.combPayType = new System.Windows.Forms.ComboBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.txtCurrencyRate = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.combCurrency = new System.Windows.Forms.ComboBox();
            this.txtExtra = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.gridLookUpEdit1 = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.button2 = new System.Windows.Forms.Button();
            this.GridControl_Items = new DevExpress.XtraGrid.GridControl();
            this.GridView_Items = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.toolStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.statusStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVPayType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_Items)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_Items)).BeginInit();
            this.SuspendLayout();
            // 
            // txtMainID
            // 
            this.txtMainID.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtMainID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMainID.Location = new System.Drawing.Point(93, 55);
            this.txtMainID.Name = "txtMainID";
            this.txtMainID.ReadOnly = true;
            this.txtMainID.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtMainID.Size = new System.Drawing.Size(310, 24);
            this.txtMainID.TabIndex = 606;
            this.txtMainID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMainID.TextChanged += new System.EventHandler(this.txtMainID_TextChanged);
            this.txtMainID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AllKeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 17);
            this.label4.TabIndex = 605;
            this.label4.Text = "رقم السند :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 141);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 17);
            this.label2.TabIndex = 604;
            this.label2.Text = "البيـــان :";
            // 
            // txtNote
            // 
            this.txtNote.BackColor = System.Drawing.Color.White;
            this.txtNote.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNote.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNote.Location = new System.Drawing.Point(94, 141);
            this.txtNote.Name = "txtNote";
            this.txtNote.Size = new System.Drawing.Size(309, 23);
            this.txtNote.TabIndex = 601;
            // 
            // D1
            // 
            this.D1.CalendarMonthBackground = System.Drawing.Color.WhiteSmoke;
            this.D1.CustomFormat = "dd/MM/yyyy";
            this.D1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.D1.Location = new System.Drawing.Point(93, 28);
            this.D1.Name = "D1";
            this.D1.Size = new System.Drawing.Size(154, 24);
            this.D1.TabIndex = 603;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 17);
            this.label1.TabIndex = 602;
            this.label1.Text = "تاريخ السند :";
            // 
            // txtStoreName
            // 
            this.txtStoreName.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtStoreName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtStoreName.Location = new System.Drawing.Point(634, 28);
            this.txtStoreName.Name = "txtStoreName";
            this.txtStoreName.ReadOnly = true;
            this.txtStoreName.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtStoreName.Size = new System.Drawing.Size(268, 24);
            this.txtStoreName.TabIndex = 624;
            this.txtStoreName.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtStoreName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtStoreName_KeyDown);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(569, 33);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 17);
            this.label6.TabIndex = 623;
            this.label6.Text = "المخزن :";
            // 
            // button1
            // 
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.Location = new System.Drawing.Point(904, 28);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(29, 25);
            this.button1.TabIndex = 625;
            this.button1.Text = "...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtStoreID
            // 
            this.txtStoreID.Location = new System.Drawing.Point(869, 1);
            this.txtStoreID.Name = "txtStoreID";
            this.txtStoreID.ReadOnly = true;
            this.txtStoreID.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtStoreID.Size = new System.Drawing.Size(10, 24);
            this.txtStoreID.TabIndex = 626;
            this.txtStoreID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtStoreID.Visible = false;
            this.txtStoreID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AllKeyPress);
            // 
            // txtProdecutID
            // 
            this.txtProdecutID.Location = new System.Drawing.Point(869, 1);
            this.txtProdecutID.Name = "txtProdecutID";
            this.txtProdecutID.Size = new System.Drawing.Size(10, 24);
            this.txtProdecutID.TabIndex = 645;
            this.txtProdecutID.Visible = false;
            // 
            // txtUnitFactor
            // 
            this.txtUnitFactor.Location = new System.Drawing.Point(869, 1);
            this.txtUnitFactor.Name = "txtUnitFactor";
            this.txtUnitFactor.Size = new System.Drawing.Size(10, 24);
            this.txtUnitFactor.TabIndex = 657;
            this.txtUnitFactor.Visible = false;
            // 
            // txtUnitOperating
            // 
            this.txtUnitOperating.Location = new System.Drawing.Point(869, 1);
            this.txtUnitOperating.Name = "txtUnitOperating";
            this.txtUnitOperating.Size = new System.Drawing.Size(10, 24);
            this.txtUnitOperating.TabIndex = 658;
            this.txtUnitOperating.Visible = false;
            // 
            // txtBillTypeID
            // 
            this.txtBillTypeID.Location = new System.Drawing.Point(869, -2);
            this.txtBillTypeID.Name = "txtBillTypeID";
            this.txtBillTypeID.ReadOnly = true;
            this.txtBillTypeID.Size = new System.Drawing.Size(10, 24);
            this.txtBillTypeID.TabIndex = 659;
            this.txtBillTypeID.Text = "Sales";
            this.txtBillTypeID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBillTypeID.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(33, 86);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 17);
            this.label9.TabIndex = 687;
            this.label9.Text = "العميل :";
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.Tan;
            this.toolStrip1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDropDownButton2,
            this.btnNew,
            this.btnSave,
            this.btnSave_Print,
            this.btnUpdate,
            this.btnDelete,
            this.toolStripButton2});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1360, 27);
            this.toolStrip1.TabIndex = 690;
            this.toolStrip1.Text = "toolStrip1";
            this.toolStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStrip1_ItemClicked);
            // 
            // toolStripDropDownButton2
            // 
            this.toolStripDropDownButton2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CS_44,
            this.CustomerTrsnse,
            this.toolStripSeparator4,
            this.اعادةتحميلالاصنافToolStripMenuItem,
            this.toolStripSeparator1,
            this.مستوياسعارToolStripMenuItem,
            this.تحميلالاصنافمنفاتورةعرضسعرToolStripMenuItem});
            this.toolStripDropDownButton2.Image = global::ByStro.Properties.Resources.Tools;
            this.toolStripDropDownButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton2.Name = "toolStripDropDownButton2";
            this.toolStripDropDownButton2.Size = new System.Drawing.Size(78, 24);
            this.toolStripDropDownButton2.Text = "خيارات";
            // 
            // CS_44
            // 
            this.CS_44.Name = "CS_44";
            this.CS_44.Size = new System.Drawing.Size(289, 26);
            this.CS_44.Text = "عرض صافي ربح الفاتورة";
            this.CS_44.Click += new System.EventHandler(this.CS_44_Click);
            // 
            // CustomerTrsnse
            // 
            this.CustomerTrsnse.Image = global::ByStro.Properties.Resources.customerss;
            this.CustomerTrsnse.Name = "CustomerTrsnse";
            this.CustomerTrsnse.Size = new System.Drawing.Size(289, 26);
            this.CustomerTrsnse.Text = "كشف حساب عميل";
            this.CustomerTrsnse.Click += new System.EventHandler(this.CustomerTrsnse_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(286, 6);
            // 
            // اعادةتحميلالاصنافToolStripMenuItem
            // 
            this.اعادةتحميلالاصنافToolStripMenuItem.Image = global::ByStro.Properties.Resources.Ingredients_48px;
            this.اعادةتحميلالاصنافToolStripMenuItem.Name = "اعادةتحميلالاصنافToolStripMenuItem";
            this.اعادةتحميلالاصنافToolStripMenuItem.Size = new System.Drawing.Size(289, 26);
            this.اعادةتحميلالاصنافToolStripMenuItem.Text = "اعادة تحميل الاصناف";
            this.اعادةتحميلالاصنافToolStripMenuItem.Click += new System.EventHandler(this.اعادةتحميلالاصنافToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(286, 6);
            // 
            // مستوياسعارToolStripMenuItem
            // 
            this.مستوياسعارToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SalesLevel1,
            this.SalesLevel2,
            this.SalesLevel3});
            this.مستوياسعارToolStripMenuItem.Image = global::ByStro.Properties.Resources.Euro_Price_Tag_48px;
            this.مستوياسعارToolStripMenuItem.Name = "مستوياسعارToolStripMenuItem";
            this.مستوياسعارToolStripMenuItem.Size = new System.Drawing.Size(289, 26);
            this.مستوياسعارToolStripMenuItem.Text = "مستوي اسعار";
            // 
            // SalesLevel1
            // 
            this.SalesLevel1.Checked = true;
            this.SalesLevel1.CheckOnClick = true;
            this.SalesLevel1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.SalesLevel1.Name = "SalesLevel1";
            this.SalesLevel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.SalesLevel1.Size = new System.Drawing.Size(160, 22);
            this.SalesLevel1.Text = "سعر بيــــــع 1";
            this.SalesLevel1.Click += new System.EventHandler(this.SalesLevel1_Click);
            // 
            // SalesLevel2
            // 
            this.SalesLevel2.CheckOnClick = true;
            this.SalesLevel2.Name = "SalesLevel2";
            this.SalesLevel2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.SalesLevel2.Size = new System.Drawing.Size(160, 22);
            this.SalesLevel2.Text = "سعر بيــــــع 2";
            this.SalesLevel2.Click += new System.EventHandler(this.SalesLevel2_Click);
            // 
            // SalesLevel3
            // 
            this.SalesLevel3.CheckOnClick = true;
            this.SalesLevel3.Name = "SalesLevel3";
            this.SalesLevel3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.SalesLevel3.Size = new System.Drawing.Size(160, 22);
            this.SalesLevel3.Text = "سعر بيــــــع 3";
            this.SalesLevel3.Click += new System.EventHandler(this.SalesLevel3_Click);
            // 
            // تحميلالاصنافمنفاتورةعرضسعرToolStripMenuItem
            // 
            this.تحميلالاصنافمنفاتورةعرضسعرToolStripMenuItem.Image = global::ByStro.Properties.Resources.xbuy;
            this.تحميلالاصنافمنفاتورةعرضسعرToolStripMenuItem.Name = "تحميلالاصنافمنفاتورةعرضسعرToolStripMenuItem";
            this.تحميلالاصنافمنفاتورةعرضسعرToolStripMenuItem.Size = new System.Drawing.Size(289, 26);
            this.تحميلالاصنافمنفاتورةعرضسعرToolStripMenuItem.Text = "تحميل الاصناف من فاتورة عرض سعر";
            this.تحميلالاصنافمنفاتورةعرضسعرToolStripMenuItem.Click += new System.EventHandler(this.تحميلالاصنافمنفاتورةعرضسعرToolStripMenuItem_Click);
            // 
            // btnNew
            // 
            this.btnNew.Image = global::ByStro.Properties.Resources.New;
            this.btnNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(65, 24);
            this.btnNew.Text = "جديد ";
            this.btnNew.ToolTipText = "جديد Ctrl + N ";
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnSave
            // 
            this.btnSave.Image = global::ByStro.Properties.Resources.Save_and_New;
            this.btnSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(85, 24);
            this.btnSave.Text = "حفظ F10";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnSave_Print
            // 
            this.btnSave_Print.Image = global::ByStro.Properties.Resources.Print_25;
            this.btnSave_Print.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSave_Print.Name = "btnSave_Print";
            this.btnSave_Print.Size = new System.Drawing.Size(125, 24);
            this.btnSave_Print.Text = "حفظ وطباعة F2 ";
            this.btnSave_Print.Click += new System.EventHandler(this.btnSave_Print_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Image = global::ByStro.Properties.Resources.edit;
            this.btnUpdate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(84, 24);
            this.btnUpdate.Text = "تعديل F3";
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Image = global::ByStro.Properties.Resources.Pic030;
            this.btnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(105, 24);
            this.btnDelete.Text = "حذف الفاتورة";
            this.btnDelete.Click += new System.EventHandler(this.toolStripButton5_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Image = global::ByStro.Properties.Resources.SearchRead_25;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(107, 24);
            this.toolStripButton2.Text = "بحث الاصناف";
            // 
            // txtAccountID
            // 
            this.txtAccountID.Location = new System.Drawing.Point(444, 79);
            this.txtAccountID.Name = "txtAccountID";
            this.txtAccountID.Size = new System.Drawing.Size(10, 24);
            this.txtAccountID.TabIndex = 691;
            this.txtAccountID.Visible = false;
            this.txtAccountID.TextChanged += new System.EventHandler(this.txtAccountID_TextChanged);
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label19.BackColor = System.Drawing.SystemColors.Control;
            this.label19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label19.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(1158, 233);
            this.label19.Name = "label19";
            this.label19.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label19.Size = new System.Drawing.Size(196, 24);
            this.label19.TabIndex = 767;
            this.label19.Text = "خصم";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtRestInvoise
            // 
            this.txtRestInvoise.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtRestInvoise.BackColor = System.Drawing.Color.White;
            this.txtRestInvoise.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRestInvoise.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold);
            this.txtRestInvoise.ForeColor = System.Drawing.Color.Blue;
            this.txtRestInvoise.Location = new System.Drawing.Point(1159, 600);
            this.txtRestInvoise.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtRestInvoise.Name = "txtRestInvoise";
            this.txtRestInvoise.ReadOnly = true;
            this.txtRestInvoise.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtRestInvoise.Size = new System.Drawing.Size(197, 24);
            this.txtRestInvoise.TabIndex = 765;
            this.txtRestInvoise.Text = "0.00";
            this.txtRestInvoise.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtpaid_Invoice
            // 
            this.txtpaid_Invoice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtpaid_Invoice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtpaid_Invoice.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold);
            this.txtpaid_Invoice.ForeColor = System.Drawing.Color.Green;
            this.txtpaid_Invoice.Location = new System.Drawing.Point(1159, 550);
            this.txtpaid_Invoice.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtpaid_Invoice.Name = "txtpaid_Invoice";
            this.txtpaid_Invoice.ReadOnly = true;
            this.txtpaid_Invoice.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtpaid_Invoice.Size = new System.Drawing.Size(197, 24);
            this.txtpaid_Invoice.TabIndex = 766;
            this.txtpaid_Invoice.Text = "0";
            this.txtpaid_Invoice.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtpaid_Invoice.TextChanged += new System.EventHandler(this.txtpaid_Invoice_TextChanged);
            // 
            // txtNetInvoice
            // 
            this.txtNetInvoice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNetInvoice.BackColor = System.Drawing.Color.White;
            this.txtNetInvoice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNetInvoice.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold);
            this.txtNetInvoice.ForeColor = System.Drawing.Color.Red;
            this.txtNetInvoice.Location = new System.Drawing.Point(1158, 409);
            this.txtNetInvoice.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtNetInvoice.Name = "txtNetInvoice";
            this.txtNetInvoice.ReadOnly = true;
            this.txtNetInvoice.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtNetInvoice.Size = new System.Drawing.Size(197, 24);
            this.txtNetInvoice.TabIndex = 764;
            this.txtNetInvoice.Text = "0.00";
            this.txtNetInvoice.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtNetInvoice.TextChanged += new System.EventHandler(this.txtNetInvoice_TextChanged);
            // 
            // txtTotalDescound
            // 
            this.txtTotalDescound.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTotalDescound.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotalDescound.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold);
            this.txtTotalDescound.Location = new System.Drawing.Point(1158, 258);
            this.txtTotalDescound.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTotalDescound.Name = "txtTotalDescound";
            this.txtTotalDescound.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtTotalDescound.Size = new System.Drawing.Size(197, 24);
            this.txtTotalDescound.TabIndex = 760;
            this.txtTotalDescound.Text = "0";
            this.txtTotalDescound.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.toolTip1.SetToolTip(this.txtTotalDescound, "خصم مسموح بة");
            this.txtTotalDescound.TextChanged += new System.EventHandler(this.txtChange);
            // 
            // txtCustomersBlanse
            // 
            this.txtCustomersBlanse.BackColor = System.Drawing.Color.White;
            this.txtCustomersBlanse.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCustomersBlanse.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCustomersBlanse.Location = new System.Drawing.Point(634, 56);
            this.txtCustomersBlanse.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCustomersBlanse.Name = "txtCustomersBlanse";
            this.txtCustomersBlanse.ReadOnly = true;
            this.txtCustomersBlanse.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtCustomersBlanse.Size = new System.Drawing.Size(298, 24);
            this.txtCustomersBlanse.TabIndex = 761;
            this.txtCustomersBlanse.Text = "0";
            this.txtCustomersBlanse.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtTotalInvoice
            // 
            this.txtTotalInvoice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTotalInvoice.BackColor = System.Drawing.Color.White;
            this.txtTotalInvoice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotalInvoice.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalInvoice.Location = new System.Drawing.Point(1158, 208);
            this.txtTotalInvoice.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTotalInvoice.Name = "txtTotalInvoice";
            this.txtTotalInvoice.ReadOnly = true;
            this.txtTotalInvoice.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtTotalInvoice.Size = new System.Drawing.Size(197, 24);
            this.txtTotalInvoice.TabIndex = 759;
            this.txtTotalInvoice.Text = "0.00";
            this.txtTotalInvoice.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtTotalInvoice.TextChanged += new System.EventHandler(this.txtChange);
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.BackColor = System.Drawing.SystemColors.Control;
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label7.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(1158, 434);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label7.Size = new System.Drawing.Size(196, 24);
            this.label7.TabIndex = 762;
            this.label7.Text = "نوع الدفع";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label24
            // 
            this.label24.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label24.BackColor = System.Drawing.SystemColors.Control;
            this.label24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label24.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(1159, 575);
            this.label24.Name = "label24";
            this.label24.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label24.Size = new System.Drawing.Size(196, 24);
            this.label24.TabIndex = 755;
            this.label24.Text = "رجع للعميل ";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label15.BackColor = System.Drawing.SystemColors.Control;
            this.label15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label15.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(1158, 384);
            this.label15.Name = "label15";
            this.label15.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label15.Size = new System.Drawing.Size(196, 24);
            this.label15.TabIndex = 758;
            this.label15.Text = "صافي الفاتورة";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label18.BackColor = System.Drawing.SystemColors.Control;
            this.label18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label18.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(1159, 525);
            this.label18.Name = "label18";
            this.label18.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label18.Size = new System.Drawing.Size(196, 24);
            this.label18.TabIndex = 757;
            this.label18.Text = "المبلغ المسدد";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label20
            // 
            this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label20.BackColor = System.Drawing.SystemColors.Control;
            this.label20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label20.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(1158, 183);
            this.label20.Name = "label20";
            this.label20.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label20.Size = new System.Drawing.Size(196, 24);
            this.label20.TabIndex = 754;
            this.label20.Text = "اجمالي الفاتورة";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // combPayKind
            // 
            this.combPayKind.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.combPayKind.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combPayKind.FormattingEnabled = true;
            this.combPayKind.Items.AddRange(new object[] {
            "نقدي",
            "أجل"});
            this.combPayKind.Location = new System.Drawing.Point(1158, 459);
            this.combPayKind.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.combPayKind.Name = "combPayKind";
            this.combPayKind.Size = new System.Drawing.Size(197, 24);
            this.combPayKind.TabIndex = 763;
            this.combPayKind.SelectedIndexChanged += new System.EventHandler(this.combPayKind_SelectedIndexChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(539, 58);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(88, 17);
            this.label13.TabIndex = 769;
            this.label13.Text = "رصيد العميل :";
            // 
            // txtProdecutAvg
            // 
            this.txtProdecutAvg.Location = new System.Drawing.Point(869, -2);
            this.txtProdecutAvg.Name = "txtProdecutAvg";
            this.txtProdecutAvg.ReadOnly = true;
            this.txtProdecutAvg.Size = new System.Drawing.Size(10, 24);
            this.txtProdecutAvg.TabIndex = 770;
            this.txtProdecutAvg.Visible = false;
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(469, 80);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(10, 24);
            this.txtPhone.TabIndex = 772;
            this.txtPhone.Visible = false;
            // 
            // txtBalence
            // 
            this.txtBalence.Location = new System.Drawing.Point(488, 33);
            this.txtBalence.Name = "txtBalence";
            this.txtBalence.ReadOnly = true;
            this.txtBalence.Size = new System.Drawing.Size(10, 24);
            this.txtBalence.TabIndex = 773;
            this.txtBalence.Visible = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(528, 89);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(99, 17);
            this.label14.TabIndex = 775;
            this.label14.Text = "الكمية المتاحة :";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.White;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBox1.Location = new System.Drawing.Point(634, 84);
            this.textBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textBox1.Size = new System.Drawing.Size(268, 24);
            this.textBox1.TabIndex = 774;
            this.textBox1.Text = "0";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtTotalBayPrice
            // 
            this.txtTotalBayPrice.Location = new System.Drawing.Point(512, 33);
            this.txtTotalBayPrice.Name = "txtTotalBayPrice";
            this.txtTotalBayPrice.ReadOnly = true;
            this.txtTotalBayPrice.Size = new System.Drawing.Size(10, 24);
            this.txtTotalBayPrice.TabIndex = 776;
            this.txtTotalBayPrice.Visible = false;
            // 
            // combAccount
            // 
            this.combAccount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.combAccount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.combAccount.FormattingEnabled = true;
            this.combAccount.Location = new System.Drawing.Point(93, 83);
            this.combAccount.Name = "combAccount";
            this.combAccount.Size = new System.Drawing.Size(309, 24);
            this.combAccount.TabIndex = 777;
            this.combAccount.SelectedIndexChanged += new System.EventHandler(this.combAccount_SelectedIndexChanged);
            this.combAccount.TextChanged += new System.EventHandler(this.combAccount_TextChanged);
            this.combAccount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.combAccount_KeyDown_1);
            // 
            // bunViewQuentityStore_frm
            // 
            this.bunViewQuentityStore_frm.BackColor = System.Drawing.Color.WhiteSmoke;
            this.bunViewQuentityStore_frm.BackgroundImage = global::ByStro.Properties.Resources.Search1_52px;
            this.bunViewQuentityStore_frm.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunViewQuentityStore_frm.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunViewQuentityStore_frm.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.bunViewQuentityStore_frm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bunViewQuentityStore_frm.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunViewQuentityStore_frm.Location = new System.Drawing.Point(904, 84);
            this.bunViewQuentityStore_frm.Name = "bunViewQuentityStore_frm";
            this.bunViewQuentityStore_frm.Size = new System.Drawing.Size(29, 25);
            this.bunViewQuentityStore_frm.TabIndex = 778;
            this.toolTip1.SetToolTip(this.bunViewQuentityStore_frm, "الكمية المتاحة في المخازن Shift + A");
            this.bunViewQuentityStore_frm.UseVisualStyleBackColor = false;
            this.bunViewQuentityStore_frm.Click += new System.EventHandler(this.bunViewQuentityStore_frm_Click);
            // 
            // button3
            // 
            this.button3.BackgroundImage = global::ByStro.Properties.Resources.icons8_Plus_30;
            this.button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Location = new System.Drawing.Point(409, 83);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(29, 25);
            this.button3.TabIndex = 689;
            this.toolTip1.SetToolTip(this.button3, "اضافة عميل جديد");
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button5
            // 
            this.button5.BackgroundImage = global::ByStro.Properties.Resources.Search_20;
            this.button5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button5.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(409, 54);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(29, 25);
            this.button5.TabIndex = 607;
            this.toolTip1.SetToolTip(this.button5, "البحث في الفواتير السابقة F5");
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // txtVatValue
            // 
            this.txtVatValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtVatValue.BackColor = System.Drawing.Color.White;
            this.txtVatValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVatValue.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold);
            this.txtVatValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.txtVatValue.Location = new System.Drawing.Point(1158, 359);
            this.txtVatValue.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtVatValue.Name = "txtVatValue";
            this.txtVatValue.ReadOnly = true;
            this.txtVatValue.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtVatValue.Size = new System.Drawing.Size(197, 24);
            this.txtVatValue.TabIndex = 780;
            this.txtVatValue.Text = "0";
            this.txtVatValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label16.BackColor = System.Drawing.SystemColors.Control;
            this.label16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label16.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(1158, 334);
            this.label16.Name = "label16";
            this.label16.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label16.Size = new System.Drawing.Size(196, 24);
            this.label16.TabIndex = 779;
            this.label16.Text = "اجمالي الضريبة";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.button4);
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.label25);
            this.groupBox2.Controls.Add(this.statusStrip2);
            this.groupBox2.Controls.Add(this.DGVPayType);
            this.groupBox2.Controls.Add(this.txtStatement);
            this.groupBox2.Controls.Add(this.txtPayValue);
            this.groupBox2.Controls.Add(this.combPayType);
            this.groupBox2.Location = new System.Drawing.Point(10, 490);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1143, 217);
            this.groupBox2.TabIndex = 781;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "=";
            // 
            // button4
            // 
            this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button4.Location = new System.Drawing.Point(269, 11);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(88, 52);
            this.button4.TabIndex = 813;
            this.button4.Text = "ادخال ";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label23
            // 
            this.label23.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label23.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label23.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label23.Location = new System.Drawing.Point(894, 11);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(242, 24);
            this.label23.TabIndex = 788;
            this.label23.Text = "نوع الدفع";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label22
            // 
            this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label22.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label22.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label22.Location = new System.Drawing.Point(756, 11);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(138, 24);
            this.label22.TabIndex = 787;
            this.label22.Text = "المبلغ";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label22.Click += new System.EventHandler(this.label22_Click);
            // 
            // label25
            // 
            this.label25.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label25.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label25.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label25.Location = new System.Drawing.Point(363, 11);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(395, 24);
            this.label25.TabIndex = 786;
            this.label25.Text = "البيان";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // statusStrip2
            // 
            this.statusStrip2.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusStrip2.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripStatusLabel2,
            this.txtSumPayType});
            this.statusStrip2.Location = new System.Drawing.Point(3, 188);
            this.statusStrip2.Name = "statusStrip2";
            this.statusStrip2.Size = new System.Drawing.Size(1137, 26);
            this.statusStrip2.TabIndex = 785;
            this.statusStrip2.Text = "statusStrip2";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = global::ByStro.Properties.Resources.Deletex;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(118, 24);
            this.toolStripButton1.Text = "حذف  المحدد  ";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(69, 21);
            this.toolStripStatusLabel2.Text = "الاجمالي :";
            // 
            // txtSumPayType
            // 
            this.txtSumPayType.BackColor = System.Drawing.Color.Transparent;
            this.txtSumPayType.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSumPayType.ForeColor = System.Drawing.Color.Red;
            this.txtSumPayType.Name = "txtSumPayType";
            this.txtSumPayType.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtSumPayType.Size = new System.Drawing.Size(17, 21);
            this.txtSumPayType.Text = "0";
            // 
            // DGVPayType
            // 
            this.DGVPayType.AllowUserToAddRows = false;
            this.DGVPayType.AllowUserToDeleteRows = false;
            this.DGVPayType.AllowUserToResizeColumns = false;
            this.DGVPayType.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FloralWhite;
            this.DGVPayType.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.DGVPayType.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGVPayType.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(146)))), ((int)(((byte)(222)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 10F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVPayType.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.DGVPayType.ColumnHeadersHeight = 24;
            this.DGVPayType.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.DGVPayType.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PayTypeName,
            this.PayTypeID,
            this.CurrencyPrice2,
            this.Statement,
            this.CurrencyID2,
            this.CurrencyName2,
            this.CurrencyRate2,
            this.PayValue});
            this.DGVPayType.Cursor = System.Windows.Forms.Cursors.Default;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Tahoma", 10F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGVPayType.DefaultCellStyle = dataGridViewCellStyle3;
            this.DGVPayType.EnableHeadersVisualStyles = false;
            this.DGVPayType.GridColor = System.Drawing.Color.Silver;
            this.DGVPayType.Location = new System.Drawing.Point(3, 69);
            this.DGVPayType.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.DGVPayType.MultiSelect = false;
            this.DGVPayType.Name = "DGVPayType";
            this.DGVPayType.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.DGVPayType.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Tahoma", 10F);
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGVPayType.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.DGVPayType.RowHeadersVisible = false;
            this.DGVPayType.RowHeadersWidth = 55;
            this.DGVPayType.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.DGVPayType.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.DGVPayType.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.DGVPayType.RowTemplate.Height = 23;
            this.DGVPayType.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.DGVPayType.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGVPayType.Size = new System.Drawing.Size(1137, 118);
            this.DGVPayType.TabIndex = 784;
            this.DGVPayType.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGVPayType_CellEndEdit);
            // 
            // PayTypeName
            // 
            this.PayTypeName.DataPropertyName = "PayTypeName";
            this.PayTypeName.HeaderText = "نوع الدفع";
            this.PayTypeName.MinimumWidth = 6;
            this.PayTypeName.Name = "PayTypeName";
            this.PayTypeName.ReadOnly = true;
            this.PayTypeName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PayTypeName.Width = 252;
            // 
            // PayTypeID
            // 
            this.PayTypeID.DataPropertyName = "PayTypeID";
            this.PayTypeID.HeaderText = "PayTypeID";
            this.PayTypeID.MinimumWidth = 6;
            this.PayTypeID.Name = "PayTypeID";
            this.PayTypeID.ReadOnly = true;
            this.PayTypeID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PayTypeID.Visible = false;
            this.PayTypeID.Width = 125;
            // 
            // CurrencyPrice2
            // 
            this.CurrencyPrice2.HeaderText = "السعر بالعملة";
            this.CurrencyPrice2.MinimumWidth = 6;
            this.CurrencyPrice2.Name = "CurrencyPrice2";
            this.CurrencyPrice2.Width = 135;
            // 
            // Statement
            // 
            this.Statement.DataPropertyName = "Statement";
            this.Statement.HeaderText = "البيان";
            this.Statement.MinimumWidth = 6;
            this.Statement.Name = "Statement";
            this.Statement.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Statement.Width = 382;
            // 
            // CurrencyID2
            // 
            this.CurrencyID2.HeaderText = "CurrencyID2";
            this.CurrencyID2.MinimumWidth = 6;
            this.CurrencyID2.Name = "CurrencyID2";
            this.CurrencyID2.Visible = false;
            this.CurrencyID2.Width = 125;
            // 
            // CurrencyName2
            // 
            this.CurrencyName2.HeaderText = "العملة";
            this.CurrencyName2.MinimumWidth = 6;
            this.CurrencyName2.Name = "CurrencyName2";
            this.CurrencyName2.Width = 125;
            // 
            // CurrencyRate2
            // 
            this.CurrencyRate2.HeaderText = "سعر التعادل";
            this.CurrencyRate2.MinimumWidth = 6;
            this.CurrencyRate2.Name = "CurrencyRate2";
            this.CurrencyRate2.Width = 125;
            // 
            // PayValue
            // 
            this.PayValue.DataPropertyName = "Debit";
            this.PayValue.HeaderText = "السعر بعد التحويل";
            this.PayValue.MinimumWidth = 6;
            this.PayValue.Name = "PayValue";
            this.PayValue.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PayValue.Width = 135;
            // 
            // txtStatement
            // 
            this.txtStatement.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStatement.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtStatement.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold);
            this.txtStatement.ForeColor = System.Drawing.Color.Black;
            this.txtStatement.Location = new System.Drawing.Point(363, 39);
            this.txtStatement.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtStatement.Name = "txtStatement";
            this.txtStatement.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtStatement.Size = new System.Drawing.Size(396, 24);
            this.txtStatement.TabIndex = 783;
            this.txtStatement.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtStatement.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtStatement_KeyDown);
            // 
            // txtPayValue
            // 
            this.txtPayValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPayValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPayValue.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold);
            this.txtPayValue.ForeColor = System.Drawing.Color.Black;
            this.txtPayValue.Location = new System.Drawing.Point(746, 39);
            this.txtPayValue.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtPayValue.Name = "txtPayValue";
            this.txtPayValue.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtPayValue.Size = new System.Drawing.Size(149, 24);
            this.txtPayValue.TabIndex = 780;
            this.txtPayValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtPayValue.TextChanged += new System.EventHandler(this.txtPayValue_TextChanged);
            this.txtPayValue.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtValue_KeyDown);
            this.txtPayValue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPayValue_KeyPress);
            // 
            // combPayType
            // 
            this.combPayType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.combPayType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.combPayType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.combPayType.FormattingEnabled = true;
            this.combPayType.Location = new System.Drawing.Point(882, 39);
            this.combPayType.Name = "combPayType";
            this.combPayType.Size = new System.Drawing.Size(253, 24);
            this.combPayType.TabIndex = 779;
            this.combPayType.TextChanged += new System.EventHandler(this.combPayType_TextChanged);
            this.combPayType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.combPayType_KeyDown);
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // txtCurrencyRate
            // 
            this.txtCurrencyRate.BackColor = System.Drawing.Color.White;
            this.txtCurrencyRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrencyRate.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrencyRate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtCurrencyRate.Location = new System.Drawing.Point(634, 112);
            this.txtCurrencyRate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCurrencyRate.Name = "txtCurrencyRate";
            this.txtCurrencyRate.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtCurrencyRate.Size = new System.Drawing.Size(297, 24);
            this.txtCurrencyRate.TabIndex = 783;
            this.txtCurrencyRate.Text = "1";
            this.txtCurrencyRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCurrencyRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCurrencyRate_KeyPress);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(542, 115);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(84, 17);
            this.label21.TabIndex = 787;
            this.label21.Text = "سعر الصرف :";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(32, 115);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(53, 17);
            this.label17.TabIndex = 784;
            this.label17.Text = "العملة :";
            // 
            // combCurrency
            // 
            this.combCurrency.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.combCurrency.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.combCurrency.FormattingEnabled = true;
            this.combCurrency.Location = new System.Drawing.Point(93, 111);
            this.combCurrency.Name = "combCurrency";
            this.combCurrency.Size = new System.Drawing.Size(309, 24);
            this.combCurrency.TabIndex = 809;
            this.combCurrency.SelectedIndexChanged += new System.EventHandler(this.combCurrency_SelectedIndexChanged);
            // 
            // txtExtra
            // 
            this.txtExtra.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtExtra.BackColor = System.Drawing.Color.White;
            this.txtExtra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtExtra.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold);
            this.txtExtra.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.txtExtra.Location = new System.Drawing.Point(1159, 308);
            this.txtExtra.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtExtra.Name = "txtExtra";
            this.txtExtra.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtExtra.Size = new System.Drawing.Size(197, 24);
            this.txtExtra.TabIndex = 811;
            this.txtExtra.Text = "0";
            this.txtExtra.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtExtra.TextChanged += new System.EventHandler(this.txtChange);
            // 
            // label26
            // 
            this.label26.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label26.BackColor = System.Drawing.SystemColors.Control;
            this.label26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label26.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(1159, 283);
            this.label26.Name = "label26";
            this.label26.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label26.Size = new System.Drawing.Size(196, 24);
            this.label26.TabIndex = 810;
            this.label26.Text = "اضافة ع الفاتورة";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(567, 143);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(59, 17);
            this.label27.TabIndex = 787;
            this.label27.Text = "السائق :";
            // 
            // gridLookUpEdit1
            // 
            this.gridLookUpEdit1.Location = new System.Drawing.Point(634, 141);
            this.gridLookUpEdit1.Name = "gridLookUpEdit1";
            this.gridLookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)});
            this.gridLookUpEdit1.Properties.NullText = "";
            this.gridLookUpEdit1.Properties.PopupView = this.gridLookUpEdit1View;
            this.gridLookUpEdit1.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.gridLookUpEdit1_Properties_ButtonClick);
            this.gridLookUpEdit1.Size = new System.Drawing.Size(297, 20);
            this.gridLookUpEdit1.TabIndex = 812;
            this.gridLookUpEdit1.EditValueChanged += new System.EventHandler(this.gridLookUpEdit1_EditValueChanged);
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Location = new System.Drawing.Point(1158, 490);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(197, 30);
            this.button2.TabIndex = 813;
            this.button2.Text = "ادخال المبلغ";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // GridControl_Items
            // 
            this.GridControl_Items.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GridControl_Items.Location = new System.Drawing.Point(13, 183);
            this.GridControl_Items.MainView = this.GridView_Items;
            this.GridControl_Items.Name = "GridControl_Items";
            this.GridControl_Items.Size = new System.Drawing.Size(1140, 301);
            this.GridControl_Items.TabIndex = 814;
            this.GridControl_Items.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.GridView_Items});
            // 
            // GridView_Items
            // 
            this.GridView_Items.GridControl = this.GridControl_Items;
            this.GridView_Items.Name = "GridView_Items";
            // 
            // BillSales_frm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(246)))));
            this.ClientSize = new System.Drawing.Size(1360, 719);
            this.Controls.Add(this.GridControl_Items);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.gridLookUpEdit1);
            this.Controls.Add(this.txtExtra);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.combCurrency);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.txtCurrencyRate);
            this.Controls.Add(this.txtVatValue);
            this.Controls.Add(this.bunViewQuentityStore_frm);
            this.Controls.Add(this.combAccount);
            this.Controls.Add(this.txtTotalBayPrice);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.txtBalence);
            this.Controls.Add(this.txtPhone);
            this.Controls.Add(this.txtProdecutAvg);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.txtRestInvoise);
            this.Controls.Add(this.txtpaid_Invoice);
            this.Controls.Add(this.txtNetInvoice);
            this.Controls.Add(this.txtTotalDescound);
            this.Controls.Add(this.txtCustomersBlanse);
            this.Controls.Add(this.txtTotalInvoice);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.combPayKind);
            this.Controls.Add(this.D1);
            this.Controls.Add(this.txtAccountID);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtBillTypeID);
            this.Controls.Add(this.txtUnitOperating);
            this.Controls.Add(this.txtUnitFactor);
            this.Controls.Add(this.txtProdecutID);
            this.Controls.Add(this.txtStoreID);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtStoreName);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.txtMainID);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtNote);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label16);
            this.Font = new System.Drawing.Font("Tahoma", 10F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "BillSales_frm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "فاتورة مبيعات";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OrderProudect_frm_FormClosing);
            this.Load += new System.EventHandler(this.PureItemToStoreForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.BillSales_frm_KeyDown);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.statusStrip2.ResumeLayout(false);
            this.statusStrip2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVPayType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_Items)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_Items)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TextBox txtMainID;
        private System.Windows.Forms.Label label4;
        internal System.Windows.Forms.Label label2;
        internal System.Windows.Forms.TextBox txtNote;
        private System.Windows.Forms.DateTimePicker D1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtStoreName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtStoreID;
        private System.Windows.Forms.TextBox txtProdecutID;
        private System.Windows.Forms.TextBox txtUnitFactor;
        private System.Windows.Forms.TextBox txtUnitOperating;
        private System.Windows.Forms.TextBox txtBillTypeID;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton2;
        private System.Windows.Forms.ToolStripMenuItem CS_44;
        private System.Windows.Forms.ToolStripMenuItem CustomerTrsnse;
        private System.Windows.Forms.ToolStripMenuItem اعادةتحميلالاصنافToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.TextBox txtAccountID;
        private System.Windows.Forms.ToolStripMenuItem مستوياسعارToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SalesLevel1;
        private System.Windows.Forms.ToolStripMenuItem SalesLevel2;
        private System.Windows.Forms.ToolStripMenuItem SalesLevel3;
        public System.Windows.Forms.Label label19;
        public System.Windows.Forms.TextBox txtRestInvoise;
        public System.Windows.Forms.TextBox txtpaid_Invoice;
        public System.Windows.Forms.TextBox txtNetInvoice;
        public System.Windows.Forms.TextBox txtTotalDescound;
        public System.Windows.Forms.TextBox txtCustomersBlanse;
        public System.Windows.Forms.TextBox txtTotalInvoice;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.Label label24;
        public System.Windows.Forms.Label label15;
        public System.Windows.Forms.Label label18;
        public System.Windows.Forms.Label label20;
        public System.Windows.Forms.ComboBox combPayKind;
        private System.Windows.Forms.ToolStripButton btnNew;
        private System.Windows.Forms.ToolStripButton btnSave;
        private System.Windows.Forms.ToolStripButton btnDelete;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ToolStripButton btnUpdate;
        private System.Windows.Forms.TextBox txtProdecutAvg;
        public System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.TextBox txtBalence;
        private System.Windows.Forms.Label label14;
        public System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox txtTotalBayPrice;
        private System.Windows.Forms.ComboBox combAccount;
        private System.Windows.Forms.ToolStripButton btnSave_Print;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button bunViewQuentityStore_frm;
        private System.Windows.Forms.Button button3;
        public System.Windows.Forms.TextBox txtVatValue;
        public System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox combPayType;
        public System.Windows.Forms.TextBox txtStatement;
        public System.Windows.Forms.TextBox txtPayValue;
        private System.Windows.Forms.StatusStrip statusStrip2;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel txtSumPayType;
        public System.Windows.Forms.DataGridView DGVPayType;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label25;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        public System.Windows.Forms.TextBox txtCurrencyRate;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.DataGridViewTextBoxColumn PayTypeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn PayTypeID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrencyPrice2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Statement;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrencyID2;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrencyName2;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrencyRate2;
        private System.Windows.Forms.DataGridViewTextBoxColumn PayValue;
        private System.Windows.Forms.ComboBox combCurrency;
        private System.Windows.Forms.ToolStripMenuItem تحميلالاصنافمنفاتورةعرضسعرToolStripMenuItem;
        public System.Windows.Forms.TextBox txtExtra;
        public System.Windows.Forms.Label label26;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.Label label27;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button4;
        private DevExpress.XtraGrid.GridControl GridControl_Items;
        private DevExpress.XtraGrid.Views.Grid.GridView GridView_Items;
    }
}