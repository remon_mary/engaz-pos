﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
namespace ByStro.PL
{
    public partial class PayType_frm : Form
    {
        public PayType_frm()
        {
            InitializeComponent();
        }
        PayType_cls cls = new PayType_cls();
        public Boolean LoadData = false;
        private void AccountEndAdd_Form_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingForm(this);
            btnNew_Click(null, null);

        }


        private void AccountEndAdd_Form_KeyDown(object sender, KeyEventArgs e)
        {
            #region "KeyDown"
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    Close();
                }
                if (e.Control == true && e.KeyCode == Keys.N)
                {
                    btnNew_Click(null, null);
                }
                if (e.KeyCode == Keys.F2)
                {
                    if (btnSave.Enabled == true)
                    {
                        btnSave_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F3)
                {
                    if (btnUpdate.Enabled == true)
                    {
                        btnUpdate_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.Delete)
                {
                    if (btnDelete.Enabled == true)
                    {
                        btnDelete_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F5)
                {
                    btnGetData_Click(null, null);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            #endregion

        }

        private void txtAccountName_TextChanged(object sender, EventArgs e)
        {
            txtPayTypeName.BackColor = Color.White;
        }

        private void txtCurrencyVal_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }



        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                //"Conditional"
                if (txtPayTypeName.Text.Trim() == "")
                {
                    txtPayTypeName.BackColor = Color.Pink;
                    txtPayTypeName.Focus();
                    return;
                }

                // " Search TextBox "
                DataTable DtSearch = cls.NameSearch__PayType(txtPayTypeName.Text.Trim());
                if (DtSearch.Rows.Count > 0)
                {
                    MessageBox.Show("الاسم المدخل موجود مسبقا", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtPayTypeName.BackColor = Color.Pink;
                    txtPayTypeName.Focus();
                    return;
                }

                txtPayTypeID.Text = cls.MaxID_PayType();
                cls.Insert_PayType(txtPayTypeID.Text, txtPayTypeName.Text.Trim(), txtAccountNumber.Text, txtAddress.Text, txtPhone.Text, txtFax.Text, txtRemark.Text);
                btnNew_Click(null, null);
                LoadData = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                // "Conditional"
                if (txtPayTypeName.Text.Trim() == "")
                {
                    txtPayTypeName.BackColor = Color.Pink;
                    txtPayTypeName.Focus();
                    return;
                }

                //"Conditional"
                cls.Update_PayType(txtPayTypeID.Text, txtPayTypeName.Text.Trim(), txtAccountNumber.Text, txtAddress.Text, txtPhone.Text, txtFax.Text, txtRemark.Text);
                btnNew_Click(null, null);
                LoadData = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {

                // fill combBox combParent

                //=============================================================================================

                txtPayTypeID.Text = cls.MaxID_PayType();
                txtPayTypeName.Text = "";
                txtAccountNumber.Text = "";
                txtAddress.Text = "";
                txtPhone.Text = "";
                txtFax.Text = "";
                txtRemark.Text = "";
                btnSave.Enabled = true;
                btnUpdate.Enabled = false;
                btnDelete.Enabled = false;

                if (Application.OpenForms["BillSales_frm"] != null)
                {
                    //((BillSales_frm)Application.OpenForms["BillSales_frm"]).FillPayType();

                }






                txtPayTypeName.Focus();



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {





                #region " Search TextBox "


                #endregion

                if (Mass.Delete() == true)
                {
                    cls.Delete_PayType(txtPayTypeID.Text);
                    btnNew_Click(null, null);
                    LoadData = true;
                }






            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnGetData_Click(object sender, EventArgs e)
        {


            try
            {
                PayTypeSearch_frm frm = new PayTypeSearch_frm();
                frm.ShowDialog(this);
                if (frm.loaddata == false)
                {
                    return;
                }

                DataTable dt = cls.Details_PayType(frm.DGV1.CurrentRow.Cells["PayTypeID"].Value.ToString());
                DataRow Dr = dt.Rows[0];
                txtPayTypeID.Text = Dr["PayTypeID"].ToString();
                txtPayTypeName.Text = Dr["PayTypeName"].ToString();
                txtAccountNumber.Text = Dr["AccountNumber"].ToString();
                txtAddress.Text = Dr["Address"].ToString();
                txtPhone.Text = Dr["Phone"].ToString();
                txtFax.Text = Dr["Fax"].ToString();
                txtRemark.Text = Dr["Remark"].ToString();

                btnSave.Enabled = false;
                btnUpdate.Enabled = true;
                btnDelete.Enabled = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }


        }

        private void txtRemark_TextChanged(object sender, EventArgs e)
        {


        }
    }
}
