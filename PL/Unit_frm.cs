﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Drawing;
namespace ByStro.PL
{
    public partial class Unit_frm : Form
    {
        public Unit_frm()
        {
            InitializeComponent();
        }
        public Boolean LoadData = false;
        Unit_cls cls  = new Unit_cls();
        private void AccountEndAdd_Form_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingForm(this);
            btnNew_Click(null,null);

        }

      



        private void AccountEndAdd_Form_KeyDown(object sender, KeyEventArgs e)
        {
            #region "KeyDown"
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    Close();
                }
                if (e.Control == true && e.KeyCode == Keys.N)
                {
                    btnNew_Click(null, null);
                }
                if (e.KeyCode == Keys.F2)
                {
                    if (btnSave.Enabled == true)
                    {
                        btnSave_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F3)
                {
                    if (btnUpdate.Enabled == true)
                    {
                        btnUpdate_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.Delete)
                {
                    if (btnDelete.Enabled == true)
                    {
                        btnDelete_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F5)
                {
                    btnGetData_Click(null, null);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            #endregion
          
        }

        private void txtAccountName_TextChanged(object sender, EventArgs e)
        {
            txtUnitName.BackColor = Color.White;
        }

        private void txtCurrencyVal_KeyPress(object sender, KeyPressEventArgs e)
        {
         DataAccessLayer.UseNamberOnly(e);
        }

  

        private void btnSave_Click(object sender, EventArgs e)
        {
           // "Conditional"
            if (txtUnitName.Text.Trim() == "")
            {
                txtUnitName.BackColor = Color.Pink;
                txtUnitName.Focus();
                return;
            }
      
           //" Search TextBox "
            DataTable DtSearch = cls.NameSearch_Unit( txtUnitName.Text.Trim());
            if (DtSearch.Rows.Count > 0)
            {
                MessageBox.Show("الاسم المدخل موجود مسبقا", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtUnitName.BackColor = Color.Pink;
                txtUnitName.Focus();
                return;
            }
      


            txtUnitID.Text = cls.MaxID_Unit();
            cls.Insert_Unit(int.Parse( txtUnitID.Text),txtUnitName.Text.Trim());
            btnNew_Click(null,null);
            LoadData = true;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
          // "Conditional"
            if (txtUnitName.Text.Trim() == "")
            {
                txtUnitName.BackColor = Color.Pink;
                txtUnitName.Focus();
                return;
            }

      

            cls.Update_Unit(int.Parse(txtUnitID.Text), txtUnitName.Text.Trim());
            btnNew_Click(null,null);
            LoadData = true;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                txtUnitID.Text = cls.MaxID_Unit();
                txtUnitName.Text = "";
                btnSave.Enabled = true;
                btnUpdate.Enabled = false;
                btnDelete.Enabled = false;
                txtUnitName.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
   
   



                if (Mass.Delete() == true)
                {
                    cls.Delete_Unit(int.Parse(txtUnitID.Text));
                    btnNew_Click(null,null);
                    LoadData = true;
                }  
             
            
            



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnGetData_Click(object sender, EventArgs e)
        {
           

            try
            {
                Unit1Search_Form frm = new Unit1Search_Form();
                frm.ShowDialog(this);
                if (frm.loaddata==false)
                {
                    return;
                }

                DataTable dt = cls.Details_Unit(int.Parse(frm.DGV1.CurrentRow.Cells["UnitID"].Value.ToString()));
                DataRow Dr = dt.Rows[0];
                txtUnitID.Text = Dr["UnitID"].ToString();
                txtUnitName.Text = Dr["UnitName"].ToString();        
                btnSave.Enabled = false;
                btnUpdate.Enabled = true;
                btnDelete.Enabled = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }


        }

  

       












    }
}
