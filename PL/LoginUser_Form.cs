﻿using ByStro.BL;
using ByStro.Clases;
using ByStro.DAL;
using ByStro.Forms;
using DevExpress.XtraEditors;
using Microsoft.Win32;
using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class LoginUser_Form : XtraForm
    {
        public LoginUser_Form()
        {

            InitializeComponent();
            try
            {
                backgroundWorker1.RunWorkerAsync();
                DataAccessLayer.Finull_Sarial = BL.Final_key.GetHash(Properties.Settings.Default.computerID);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

        }
        // DataAccessLayer DataAccessLayer = new DataAccessLayer();


        public void LoginUser_Form_Load(object sender, EventArgs e)
        {
            if(CheckIfConnectionUp() == false)
            {
                ConnectSQLServer_Form frm = new ConnectSQLServer_Form();
              //  frm.add = "Main";
                frm.ShowDialog(this);
            }

            //CreateNewCompany_frm frm = new CreateNewCompany_frm();

            //frm.Show();
            simpleButton1.Visible = CheckIfConnectionUp();

            timer1.Start();
           // lblHora.Text = DateTime.Now.ToLongTimeString();
            lblVersion.Text = DataAccessLayer.Version;


             #region"User name and password and chb"
            if (Properties.Settings.Default.RemmberPassword == true)
            {

                txtUserName.Text = Properties.Settings.Default.UserName; 
                txtPassword.Text = Properties.Settings.Default.UserPassword;
                chRememberMe.Checked = true;
            }

            #endregion

            if (Properties.Settings.Default.Finull_Sarial == DataAccessLayer.Finull_Sarial)
            {
                butActivationtheprogram.Enabled = false;
            }


            txtUserName.Focus();


        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                Properties.Settings.Default.computerID = BL.ComputerInfo.GetComputerId();
                Properties.Settings.Default.Save();
                //MessageBox.Show("", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void sQLConnectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConnectSQLServer_Form frm = new ConnectSQLServer_Form();
            frm.ShowDialog();

        }

        private void sERUELToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SerilFull_FORM FRM = new SerilFull_FORM();
            FRM.ShowDialog(this);
        }



        private void LoginUser_Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void LoginUser_Form_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void LoginUser_Form_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
               // button1.PerformClick(); ;
            }
        }



        private void button1_MouseHover(object sender, EventArgs e)
        {
            //button1.Size = new Size(100, 92);

        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            // button1.BackgroundImageLayout = ImageLayout.Stretch;
            //button1.Size = new Size(90, 85);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (CheckIfConnectionUp() == false)
            {
                XtraMessageBox.Show("لا يمكن الاتصال بالخادم , تاكد من اعدادات الاتصال", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }if(CheckIfDataBaseExist() == false)
            {
                if (XtraMessageBox.Show("تم الاتصال بالخادم بنجاح ولاكن قاعده البيانات غير موجوده ويجب تهئتها اولا قبل استخدام البرنامج  \n هل تريد انشاء قاعده بيانات جديده ؟"
                    , "", MessageBoxButtons.YesNo , MessageBoxIcon.Error) == DialogResult.Yes) {
                    simpleButton1.PerformClick();
                };
                return;
            }

            var p = Properties.Settings.Default;

             



            #region"تفعيل البرنامج "
            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);

            Trail_cls sales_CLS = new Trail_cls();
                Double countSales = Convert.ToDouble(db.Inv_Invoices.Where(x=>x.InvoiceType == (int)MasterClass.InvoiceType .SalesInvoice ).Count());
                try
                {
                    if (p.Finull_Sarial != DataAccessLayer.Finull_Sarial)
                    {
                    //C0GH-EB6D-04D7-51GB-HF6F-2HHF-812G-BH6F // Mohammed
                    p.LicenseType = "Trail";
                        p.Finull_Sarial = "1E01-192B-FBD3-75EE-A005-9910-E120-6A30";
                        p.Save();
                        if (countSales >= Final_key.SalesNumber)
                        {
                            this.Hide();
                            SerilFull_FORM FrmAc = new SerilFull_FORM();
                            FrmAc.ShowDialog();
                            Application.Exit();
                            return;
                        }

                    }
                    else
                    {
                        RegistryKey regkey = default(RegistryKey);
                        regkey = Registry.CurrentUser.OpenSubKey("SOFTWARE\\ByStor", true);
                        string code = "";
                        code = regkey.GetValue("System_Activation").ToString();
                        if (DataAccessLayer.DecryptData(code) == DataAccessLayer.Finull_Sarial)
                        {
                            p.LicenseType = "Full";
                            p.Save();
                        }
                        else
                        {
                            if (countSales >= Final_key.SalesNumber)
                            {
                                this.Hide();
                                SerilFull_FORM FrmAc = new SerilFull_FORM();
                                FrmAc.ShowDialog();
                                Application.Exit();
                                return;
                            }

                        }

                    }
                }
                catch (Exception)
                {

                    p.LicenseType = "Trail";
                    p.Finull_Sarial = "1E01-192B-FBD3-75EE-A005-9910-E120-6A30";
                    p.Save();
                    if (countSales >= Final_key.SalesNumber)
                    {
                        this.Hide();
                        SerilFull_FORM FrmAc = new SerilFull_FORM();
                        FrmAc.ShowDialog();
                        Application.Exit();
                        return;
                    }
                }
                #endregion



                if (txtUserName.Text.Trim() == "")
                {
                    MessageBox.Show("يرجي ادخال اسم المستخدم");
                    txtUserName.BackColor = Color.Pink;
                    txtUserName.Focus();
                    return;
                }
                if (txtPassword.Text.Trim() == "")
                {
                    MessageBox.Show("يرجي ادخال كلمة المرور");
                    txtPassword.BackColor = Color.Pink;
                    txtPassword.Focus();
                    return;
                }

                String Admin = "User";
                if (txtUserName.Text == "STARTECH")
                {
                    txtPassword.Text = txtPassword.Text.ToUpper();
                }

                if (txtUserName.Text == "STARTECH" && txtPassword.Text.ToUpper() == "ARS1130S1032SS")
                {
                    Admin = "OK";
                    //DataAccessLayer.UserAddEdit = "1";
                    DataAccessLayer.UserNameLogIn = "مهندس محمد ";
                    Properties.Settings.Default.Save();
                    Main_frm frm = new Main_frm();
             
                    txtUserName.Text = ""; txtPassword.Text = "";
                    this.Hide();
                    frm.Show();
               

                }


                if (Admin == "User")
                {
                    #region "User"
                    UserPermissions_CLS cls = new UserPermissions_CLS();
                    DataTable Dt_NewLogIn = cls.Search_NewLogIn_UserPermissions();

                    if (Dt_NewLogIn.Rows.Count == 0)
                    {
                        User_Permetion_frm f_user = new User_Permetion_frm();
                        f_user.ShowDialog(this);
                        return;
                    }

                    //================================================================================================

                    DataTable dt = cls.Search_LogIn_UserPermissions(txtUserName.Text, txtPassword.Text, true);

                    if (dt.Rows.Count > 0)
                    {
                        Main_frm frm = new Main_frm();
                        dynamic Dr = dt.Rows[0];

                        DataAccessLayer.TypeUser = Dr["TypeUser"];
                     
                        DataAccessLayer.CS_12 = Dr["CS12"];
                        DataAccessLayer.CS_13 = Dr["CS13"];
                         DataAccessLayer.CS_16 = Dr["CS16"];
                        DataAccessLayer.CS_17 = Dr["CS17"];
                         DataAccessLayer.CS_44 = Dr["CS44"];
                    Properties.Settings.Default.UserID = Dr["ID"];
                        Properties.Settings.Default.UserName = Dr["UserName"];
                        Properties.Settings.Default.myDate = DateTime.Now.ToString("yyy/MM/dd");
                        Properties.Settings.Default.Save();
                        // xxxxxxxxx قفل السنة 





                   








                        if (chRememberMe.Checked == true)
                        {

                            Properties.Settings.Default.UserName = txtUserName.Text;
                            Properties.Settings.Default.UserPassword = txtPassword.Text;
                            Properties.Settings.Default.RemmberPassword = true;
                            Properties.Settings.Default.Save();
                        }

                        else
                        {

                            Properties.Settings.Default.UserName = "";
                            Properties.Settings.Default.UserPassword = "";
                            Properties.Settings.Default.RemmberPassword = false;
                            Properties.Settings.Default.Save();
                        }


                        SettingInvoice_cls SettingInvoice_cls = new SettingInvoice_cls();
                        DataTable dt_Setting = SettingInvoice_cls.Details_Setting();
                    // if (dt_Setting.Rows.Count > 0)
                    // {
                    //     Store_cls store_Cls = new Store_cls();
                    //     DataTable dt_storeUser = store_Cls.Search_Stores("");
                    ////     BillSetting_cls.BillSetting(int.Parse(dt_Setting.Rows[0]["SettingID"].ToString()), (bool)dt_Setting.Rows[0]["UseStoreDefault"], dt_storeUser.Rows[0]["StoreID"].ToString(), (bool)dt_Setting.Rows[0]["UseCustomerDefault"], dt_Setting.Rows[0]["CustomerID"].ToString(), (bool)dt_Setting.Rows[0]["UsingFastInput"], (bool)dt_Setting.Rows[0]["ShowMessageQty"], (bool)dt_Setting.Rows[0]["ShowMessageSave"], int.Parse(dt_Setting.Rows[0]["kindPay"].ToString()), (bool)dt_Setting.Rows[0]["UseVat"], dt_Setting.Rows[0]["Vat"].ToString(), int.Parse(dt_Setting.Rows[0]["PrintSize"].ToString()), (bool)dt_Setting.Rows[0]["UseCrrencyDefault"], dt_Setting.Rows[0]["CurrencyID"].ToString(), (bool)dt_Setting.Rows[0]["NotificationProdect"], (bool)dt_Setting.Rows[0]["NotificationCustomers"], dt_Setting.Rows[0]["MaxBalance"].ToString(), (bool)dt_Setting.Rows[0]["UsekindPay"], dt_Setting.Rows[0]["PayTypeID"].ToString(),   (bool)dt_Setting.Rows[0]["UseCategory"], dt_Setting.Rows[0]["CategoryId"].ToString(), (bool)dt_Setting.Rows[0]["UseUnit"], dt_Setting.Rows[0]["UnitId"].ToString());
                    // }

                    DataAccessLayer.UserNameLogIn = txtUserName.Text;

                        timer1.Stop();
                        this.Hide();
                        frm.Show();
                    using (DAL.DBDataContext DB = new DAL.DBDataContext(Properties.Settings.Default.Connection_String))
                    {
                        CurrentSession.User.ID = Convert.ToInt32(dt.Rows[0]["ID"].ToString());
                        CurrentSession.UserAccessbileAccounts = DB.Acc_Accounts.ToList();
                        CurrentSession.UserAccessbileStores = DB.Inv_Stores.ToList();
                        CurrentSession.UserAccessbileCustomers = DB.Sl_Customers.ToList();
                        CurrentSession.UserAccessbileCustomergroup = DB.Sl_CustomerGroups.ToList();
                        CurrentSession.UserAccessbileVendors = DB.Pr_Vendors.ToList();
                        CurrentSession.UserAccessbileVendorgroup = DB.Pr_VendorGroups.ToList();
                        CurrentSession.UserAccessibleDrawer = DB.Acc_Drawers.ToList();
                        CurrentSession.UserAccessibleBanks = DB.Acc_Banks.ToList();
                        CurrentSession.UserAccessiblePayCards = DB.Acc_PayCards.ToList();
                        CurrentSession.UserAccessbileSalesman = DB.Salesmans.ToList();
                    }
                    }
                    else
                    {
                        MessageBox.Show("ليس لديك صلاحية للعمل علي هذ البرنامج", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Properties.Settings.Default.UserName = "";
                        Properties.Settings.Default.UserPassword = "";
                        Properties.Settings.Default.RemmberPassword = false;
                        Properties.Settings.Default.Save();
                    }
                    #endregion
                }
        }

        

        private void btnmin_Click(object sender, EventArgs e)
        {         
            WindowState = FormWindowState.Minimized;
        }

   
        private void btncerrar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("سوف يتم الخروج من البرنامج : هل تريد الاستمرار", "◄ Message | Star Tech ►", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
          //  lblHora.Text = dt.ToString("HH:MM:ss");
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtUserName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F2)
            {

                //txtUserName.PasswordChar = "*";
             
                
            }
        }

        private void txtUserName_TextChanged(object sender, EventArgs e)
        {
            txtUserName.BackColor = Color.White;
            if (txtUserName.Text == "startech" || txtUserName.Text == "STARTECH")
            { 
                txtUserName.Text = txtUserName.Text.ToUpper();
                txtPassword.Focus();
            }
            else
            { 
            }
        }

        private void txtPassword_TextChanged(object sender, EventArgs e)
        {
            txtPassword.BackColor = Color.White;
        }

        private void lblHora_Click(object sender, EventArgs e)
        {

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            new frm_ResetAdminPassWord().ShowDialog();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            new frm_CreatNewCompany().ShowDialog();
        }
        public static  bool CheckIfConnectionUp()
        {
            var sqlbulder = new SqlConnectionStringBuilder(Properties.Settings.Default.Connection_String);
            sqlbulder.InitialCatalog = "master";
            var sqlcon = new SqlConnection(sqlbulder.ConnectionString ); 
            try
            {
                sqlcon.Open();
                sqlcon.Close();
                return true;
            }
            catch ( Exception ex)
            {
                return false;
            }
            
        }
        public static bool CheckIfDataBaseExist()
        {
            using (var db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String))
            {
                return db.DatabaseExists();
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            ConnectSQLServer_Form frm = new ConnectSQLServer_Form();
            //  frm.add = "Main";
            frm.ShowDialog(this);
        }
    }
}
