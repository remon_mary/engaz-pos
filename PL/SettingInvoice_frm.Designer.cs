﻿namespace ByStro.PL
{
    partial class SettingInvoice_frm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingInvoice_frm));
            this.chUsingFastInput = new System.Windows.Forms.CheckBox();
            this.chShowMessageQty = new System.Windows.Forms.CheckBox();
            this.chShowMessageSave = new System.Windows.Forms.CheckBox();
            this.rbkindPay = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnSave = new System.Windows.Forms.ToolStripButton();
            this.chUseStoreDefault = new System.Windows.Forms.CheckBox();
            this.chUseCustomerDefault = new System.Windows.Forms.CheckBox();
            this.txtVat = new System.Windows.Forms.TextBox();
            this.chUseVat = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.lookUpEdit1 = new DevExpress.XtraEditors.LookUpEdit();
            this.chCategory = new System.Windows.Forms.CheckBox();
            this.txtCategoryId = new System.Windows.Forms.TextBox();
            this.txtUnitId = new System.Windows.Forms.TextBox();
            this.txtUnitName = new System.Windows.Forms.TextBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.btnUnit = new System.Windows.Forms.Button();
            this.btnCategory = new System.Windows.Forms.Button();
            this.txtCategoryName = new System.Windows.Forms.TextBox();
            this.checkNotToSellWhenItemBalanceIsNotAvailable = new System.Windows.Forms.CheckBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.RBPrintSize3 = new System.Windows.Forms.RadioButton();
            this.RBPrintSize2 = new System.Windows.Forms.RadioButton();
            this.RBPrintSize1 = new System.Windows.Forms.RadioButton();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.chNotificationCustomers = new System.Windows.Forms.CheckBox();
            this.chNotificationProdect = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMaxBalance = new System.Windows.Forms.TextBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.lookUpEditCustomer = new DevExpress.XtraEditors.LookUpEdit();
            this.toolStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditCustomer.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // chUsingFastInput
            // 
            this.chUsingFastInput.AutoSize = true;
            this.chUsingFastInput.Location = new System.Drawing.Point(441, 144);
            this.chUsingFastInput.Name = "chUsingFastInput";
            this.chUsingFastInput.Size = new System.Drawing.Size(179, 21);
            this.chUsingFastInput.TabIndex = 781;
            this.chUsingFastInput.Text = "استخدام الادخال السريـــع";
            this.chUsingFastInput.UseVisualStyleBackColor = true;
            // 
            // chShowMessageQty
            // 
            this.chShowMessageQty.AutoSize = true;
            this.chShowMessageQty.Location = new System.Drawing.Point(390, 171);
            this.chShowMessageQty.Name = "chShowMessageQty";
            this.chShowMessageQty.Size = new System.Drawing.Size(230, 21);
            this.chShowMessageQty.TabIndex = 782;
            this.chShowMessageQty.Text = "عدم ظهور رساله تأكيد اضافة الكمية";
            this.chShowMessageQty.UseVisualStyleBackColor = true;
            // 
            // chShowMessageSave
            // 
            this.chShowMessageSave.AutoSize = true;
            this.chShowMessageSave.Location = new System.Drawing.Point(428, 198);
            this.chShowMessageSave.Name = "chShowMessageSave";
            this.chShowMessageSave.Size = new System.Drawing.Size(192, 21);
            this.chShowMessageSave.TabIndex = 784;
            this.chShowMessageSave.Text = "عدم ظهور رساله تأكيد الحفظ";
            this.chShowMessageSave.UseVisualStyleBackColor = true;
            // 
            // rbkindPay
            // 
            this.rbkindPay.AutoSize = true;
            this.rbkindPay.Checked = true;
            this.rbkindPay.Location = new System.Drawing.Point(396, 225);
            this.rbkindPay.Name = "rbkindPay";
            this.rbkindPay.Size = new System.Drawing.Size(224, 21);
            this.rbkindPay.TabIndex = 785;
            this.rbkindPay.TabStop = true;
            this.rbkindPay.Text = "اعتماد نوع الدفع< كاش>ا افتراضي";
            this.rbkindPay.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(160, 225);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(218, 21);
            this.radioButton2.TabIndex = 786;
            this.radioButton2.Text = "اعتماد نوع الدفع< اجل>ا افتراضي";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnSave});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(676, 25);
            this.toolStrip1.TabIndex = 787;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnSave
            // 
            this.btnSave.Image = global::ByStro.Properties.Resources.Save_and_New;
            this.btnSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(73, 22);
            this.btnSave.Text = "حفظ F2";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // chUseStoreDefault
            // 
            this.chUseStoreDefault.AutoSize = true;
            this.chUseStoreDefault.Location = new System.Drawing.Point(454, 21);
            this.chUseStoreDefault.Name = "chUseStoreDefault";
            this.chUseStoreDefault.Size = new System.Drawing.Size(166, 21);
            this.chUseStoreDefault.TabIndex = 788;
            this.chUseStoreDefault.Text = "استخدام مخزن افتراضي";
            this.chUseStoreDefault.UseVisualStyleBackColor = true;
            this.chUseStoreDefault.CheckedChanged += new System.EventHandler(this.chUseStoreDefault_CheckedChanged);
            // 
            // chUseCustomerDefault
            // 
            this.chUseCustomerDefault.AutoSize = true;
            this.chUseCustomerDefault.Location = new System.Drawing.Point(454, 77);
            this.chUseCustomerDefault.Name = "chUseCustomerDefault";
            this.chUseCustomerDefault.Size = new System.Drawing.Size(166, 21);
            this.chUseCustomerDefault.TabIndex = 789;
            this.chUseCustomerDefault.Text = "استخدام عميل افتراضي";
            this.chUseCustomerDefault.UseVisualStyleBackColor = true;
            this.chUseCustomerDefault.CheckedChanged += new System.EventHandler(this.chUseCustomerDefault_CheckedChanged);
            // 
            // txtVat
            // 
            this.txtVat.BackColor = System.Drawing.Color.White;
            this.txtVat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVat.Location = new System.Drawing.Point(236, 279);
            this.txtVat.Name = "txtVat";
            this.txtVat.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtVat.Size = new System.Drawing.Size(384, 24);
            this.txtVat.TabIndex = 799;
            this.txtVat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtVat.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVat_KeyPress);
            // 
            // chUseVat
            // 
            this.chUseVat.AutoSize = true;
            this.chUseVat.Location = new System.Drawing.Point(412, 252);
            this.chUseVat.Name = "chUseVat";
            this.chUseVat.Size = new System.Drawing.Size(205, 21);
            this.chUseVat.TabIndex = 800;
            this.chUseVat.Text = "استخدام ضريبة القيمة المضافة";
            this.chUseVat.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(208, 281);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(22, 17);
            this.label1.TabIndex = 801;
            this.label1.Text = "%";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Location = new System.Drawing.Point(0, 24);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.RightToLeftLayout = true;
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(676, 400);
            this.tabControl1.TabIndex = 802;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.lookUpEditCustomer);
            this.tabPage1.Controls.Add(this.lookUpEdit1);
            this.tabPage1.Controls.Add(this.chCategory);
            this.tabPage1.Controls.Add(this.txtCategoryId);
            this.tabPage1.Controls.Add(this.txtUnitId);
            this.tabPage1.Controls.Add(this.txtUnitName);
            this.tabPage1.Controls.Add(this.checkBox2);
            this.tabPage1.Controls.Add(this.btnUnit);
            this.tabPage1.Controls.Add(this.btnCategory);
            this.tabPage1.Controls.Add(this.txtCategoryName);
            this.tabPage1.Controls.Add(this.chUseStoreDefault);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.chUsingFastInput);
            this.tabPage1.Controls.Add(this.checkNotToSellWhenItemBalanceIsNotAvailable);
            this.tabPage1.Controls.Add(this.chUseVat);
            this.tabPage1.Controls.Add(this.chShowMessageQty);
            this.tabPage1.Controls.Add(this.txtVat);
            this.tabPage1.Controls.Add(this.chShowMessageSave);
            this.tabPage1.Controls.Add(this.rbkindPay);
            this.tabPage1.Controls.Add(this.radioButton2);
            this.tabPage1.Controls.Add(this.chUseCustomerDefault);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(668, 371);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "اعدادات رئيسية";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // lookUpEdit1
            // 
            this.lookUpEdit1.Location = new System.Drawing.Point(334, 51);
            this.lookUpEdit1.Name = "lookUpEdit1";
            this.lookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit1.Properties.NullText = "";
            this.lookUpEdit1.Size = new System.Drawing.Size(286, 20);
            this.lookUpEdit1.TabIndex = 810;
            // 
            // chCategory
            // 
            this.chCategory.AutoSize = true;
            this.chCategory.Location = new System.Drawing.Point(87, 23);
            this.chCategory.Name = "chCategory";
            this.chCategory.Size = new System.Drawing.Size(181, 21);
            this.chCategory.TabIndex = 802;
            this.chCategory.Text = "استخدام مجموعة افتراضية";
            this.chCategory.UseVisualStyleBackColor = true;
            this.chCategory.CheckedChanged += new System.EventHandler(this.chCategory_CheckedChanged);
            // 
            // txtCategoryId
            // 
            this.txtCategoryId.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtCategoryId.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCategoryId.Location = new System.Drawing.Point(23, 47);
            this.txtCategoryId.Name = "txtCategoryId";
            this.txtCategoryId.ReadOnly = true;
            this.txtCategoryId.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtCategoryId.Size = new System.Drawing.Size(10, 24);
            this.txtCategoryId.TabIndex = 809;
            this.txtCategoryId.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCategoryId.Visible = false;
            // 
            // txtUnitId
            // 
            this.txtUnitId.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtUnitId.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUnitId.Location = new System.Drawing.Point(23, 104);
            this.txtUnitId.Name = "txtUnitId";
            this.txtUnitId.ReadOnly = true;
            this.txtUnitId.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtUnitId.Size = new System.Drawing.Size(10, 24);
            this.txtUnitId.TabIndex = 808;
            this.txtUnitId.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtUnitId.Visible = false;
            // 
            // txtUnitName
            // 
            this.txtUnitName.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtUnitName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUnitName.Location = new System.Drawing.Point(39, 105);
            this.txtUnitName.Name = "txtUnitName";
            this.txtUnitName.ReadOnly = true;
            this.txtUnitName.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtUnitName.Size = new System.Drawing.Size(232, 24);
            this.txtUnitName.TabIndex = 806;
            this.txtUnitName.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(105, 79);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(163, 21);
            this.checkBox2.TabIndex = 803;
            this.checkBox2.Text = "استخدام وحدة افتراضية";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // btnUnit
            // 
            this.btnUnit.Enabled = false;
            this.btnUnit.Location = new System.Drawing.Point(4, 103);
            this.btnUnit.Name = "btnUnit";
            this.btnUnit.Size = new System.Drawing.Size(29, 25);
            this.btnUnit.TabIndex = 807;
            this.btnUnit.Text = "...";
            this.btnUnit.UseVisualStyleBackColor = true;
            this.btnUnit.Click += new System.EventHandler(this.btnUnit_Click);
            // 
            // btnCategory
            // 
            this.btnCategory.Enabled = false;
            this.btnCategory.Location = new System.Drawing.Point(4, 48);
            this.btnCategory.Name = "btnCategory";
            this.btnCategory.Size = new System.Drawing.Size(29, 25);
            this.btnCategory.TabIndex = 805;
            this.btnCategory.Text = "...";
            this.btnCategory.UseVisualStyleBackColor = true;
            this.btnCategory.Click += new System.EventHandler(this.btnCategory_Click);
            // 
            // txtCategoryName
            // 
            this.txtCategoryName.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtCategoryName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCategoryName.Location = new System.Drawing.Point(39, 50);
            this.txtCategoryName.Name = "txtCategoryName";
            this.txtCategoryName.ReadOnly = true;
            this.txtCategoryName.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtCategoryName.Size = new System.Drawing.Size(232, 24);
            this.txtCategoryName.TabIndex = 804;
            this.txtCategoryName.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // checkNotToSellWhenItemBalanceIsNotAvailable
            // 
            this.checkNotToSellWhenItemBalanceIsNotAvailable.AutoSize = true;
            this.checkNotToSellWhenItemBalanceIsNotAvailable.Location = new System.Drawing.Point(385, 309);
            this.checkNotToSellWhenItemBalanceIsNotAvailable.Name = "checkNotToSellWhenItemBalanceIsNotAvailable";
            this.checkNotToSellWhenItemBalanceIsNotAvailable.Size = new System.Drawing.Size(232, 21);
            this.checkNotToSellWhenItemBalanceIsNotAvailable.TabIndex = 800;
            this.checkNotToSellWhenItemBalanceIsNotAvailable.Text = "عدم البيع عند عدم توفر رصيد الصنف";
            this.checkNotToSellWhenItemBalanceIsNotAvailable.UseVisualStyleBackColor = true;
            this.checkNotToSellWhenItemBalanceIsNotAvailable.CheckedChanged += new System.EventHandler(this.checkNotToSellWhenItemBalanceIsNotAvailable_CheckedChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(668, 371);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "اعداد الطباعة";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.RBPrintSize3);
            this.groupBox1.Controls.Add(this.RBPrintSize2);
            this.groupBox1.Controls.Add(this.RBPrintSize1);
            this.groupBox1.Location = new System.Drawing.Point(25, 34);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(635, 86);
            this.groupBox1.TabIndex = 790;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "تحديد نوع طباعة الفواتير ";
            // 
            // RBPrintSize3
            // 
            this.RBPrintSize3.AutoSize = true;
            this.RBPrintSize3.Location = new System.Drawing.Point(70, 38);
            this.RBPrintSize3.Name = "RBPrintSize3";
            this.RBPrintSize3.Size = new System.Drawing.Size(117, 21);
            this.RBPrintSize3.TabIndex = 789;
            this.RBPrintSize3.Text = "الطابعة الحراري ";
            this.RBPrintSize3.UseVisualStyleBackColor = true;
            // 
            // RBPrintSize2
            // 
            this.RBPrintSize2.AutoSize = true;
            this.RBPrintSize2.Location = new System.Drawing.Point(286, 38);
            this.RBPrintSize2.Name = "RBPrintSize2";
            this.RBPrintSize2.Size = new System.Drawing.Size(74, 21);
            this.RBPrintSize2.TabIndex = 788;
            this.RBPrintSize2.Text = "نصف A4";
            this.RBPrintSize2.UseVisualStyleBackColor = true;
            // 
            // RBPrintSize1
            // 
            this.RBPrintSize1.AutoSize = true;
            this.RBPrintSize1.Checked = true;
            this.RBPrintSize1.Location = new System.Drawing.Point(459, 38);
            this.RBPrintSize1.Name = "RBPrintSize1";
            this.RBPrintSize1.Size = new System.Drawing.Size(42, 21);
            this.RBPrintSize1.TabIndex = 786;
            this.RBPrintSize1.TabStop = true;
            this.RBPrintSize1.Text = "A4";
            this.RBPrintSize1.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.chNotificationCustomers);
            this.tabPage4.Controls.Add(this.chNotificationProdect);
            this.tabPage4.Controls.Add(this.groupBox2);
            this.tabPage4.Location = new System.Drawing.Point(4, 25);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(668, 371);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "الاشعارات";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // chNotificationCustomers
            // 
            this.chNotificationCustomers.AutoSize = true;
            this.chNotificationCustomers.Location = new System.Drawing.Point(274, 67);
            this.chNotificationCustomers.Name = "chNotificationCustomers";
            this.chNotificationCustomers.Size = new System.Drawing.Size(332, 21);
            this.chNotificationCustomers.TabIndex = 3;
            this.chNotificationCustomers.Text = "عرض اشعارات العملاء الذين تجاوزا الحد الاقصي للدين";
            this.chNotificationCustomers.UseVisualStyleBackColor = true;
            // 
            // chNotificationProdect
            // 
            this.chNotificationProdect.AutoSize = true;
            this.chNotificationProdect.Location = new System.Drawing.Point(321, 38);
            this.chNotificationProdect.Name = "chNotificationProdect";
            this.chNotificationProdect.Size = new System.Drawing.Size(285, 21);
            this.chNotificationProdect.TabIndex = 1;
            this.chNotificationProdect.Text = "عرض اشعارات الاصناف التي وصلت حد الطلب";
            this.chNotificationProdect.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtMaxBalance);
            this.groupBox2.Location = new System.Drawing.Point(6, 108);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(654, 83);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "العملاء";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(475, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(116, 17);
            this.label2.TabIndex = 801;
            this.label2.Text = "الحد الاقصي للدين";
            // 
            // txtMaxBalance
            // 
            this.txtMaxBalance.BackColor = System.Drawing.Color.White;
            this.txtMaxBalance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMaxBalance.Location = new System.Drawing.Point(228, 36);
            this.txtMaxBalance.Name = "txtMaxBalance";
            this.txtMaxBalance.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtMaxBalance.Size = new System.Drawing.Size(237, 24);
            this.txtMaxBalance.TabIndex = 800;
            this.txtMaxBalance.Text = "0";
            this.txtMaxBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMaxBalance.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMaxBalance_KeyPress);
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.simpleButton2);
            this.tabPage5.Controls.Add(this.simpleButton1);
            this.tabPage5.Location = new System.Drawing.Point(4, 25);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(668, 371);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "التصميمات";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(311, 20);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(165, 47);
            this.simpleButton2.TabIndex = 0;
            this.simpleButton2.Text = "اعاده تصميم ملف الاقساط";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(482, 20);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(165, 47);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "اعاده تصميم فاتوره البيع";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // lookUpEditCustomer
            // 
            this.lookUpEditCustomer.Location = new System.Drawing.Point(334, 101);
            this.lookUpEditCustomer.Name = "lookUpEditCustomer";
            this.lookUpEditCustomer.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditCustomer.Properties.NullText = "";
            this.lookUpEditCustomer.Size = new System.Drawing.Size(286, 20);
            this.lookUpEditCustomer.TabIndex = 811;
            // 
            // SettingInvoice_frm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(676, 436);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.toolStrip1);
            this.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "SettingInvoice_frm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "اعداد الفواتير";
            this.Load += new System.EventHandler(this.SettingInvoice_frm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SettingInvoice_frm_KeyDown);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditCustomer.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.CheckBox chUsingFastInput;
        private System.Windows.Forms.CheckBox chShowMessageQty;
        private System.Windows.Forms.CheckBox chShowMessageSave;
        private System.Windows.Forms.RadioButton rbkindPay;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnSave;
        private System.Windows.Forms.CheckBox chUseStoreDefault;
        private System.Windows.Forms.CheckBox chUseCustomerDefault;
        private System.Windows.Forms.TextBox txtVat;
        private System.Windows.Forms.CheckBox chUseVat;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.RadioButton RBPrintSize1;
        private System.Windows.Forms.RadioButton RBPrintSize2;
        private System.Windows.Forms.RadioButton RBPrintSize3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox chNotificationProdect;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtMaxBalance;
        private System.Windows.Forms.CheckBox chNotificationCustomers;
        private System.Windows.Forms.CheckBox chCategory;
        private System.Windows.Forms.TextBox txtCategoryId;
        private System.Windows.Forms.TextBox txtUnitId;
        private System.Windows.Forms.TextBox txtUnitName;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.Button btnUnit;
        private System.Windows.Forms.Button btnCategory;
        private System.Windows.Forms.TextBox txtCategoryName;
        private System.Windows.Forms.TabPage tabPage5;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit1;
        private System.Windows.Forms.CheckBox checkNotToSellWhenItemBalanceIsNotAvailable;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditCustomer;
    }
}