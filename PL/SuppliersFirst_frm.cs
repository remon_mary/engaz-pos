﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class SuppliersFirst_frm : Form
    {
        public SuppliersFirst_frm()
        {
            InitializeComponent();
        }


        TreasuryMovement_cls cls_Treasury_Movement = new TreasuryMovement_cls();

        private void CustomersPayment_FormAdd_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingForm(this);
            FillCombAccount();
            btnNew_Click(null, null);

        }


        public void FillCombAccount()
        {
            string cusName = combSuppliers.Text;
            string cusID = txtSupplierID.Text;
            Suppliers_cls Supplier_cls = new Suppliers_cls();
            combSuppliers.DataSource = Supplier_cls.Search_Suppliers("");
            combSuppliers.DisplayMember = "SupplierName";
            combSuppliers.ValueMember = "SupplierID";
            combSuppliers.Text = cusName;
            txtSupplierID.Text = cusID;

        }



        private void txtStatement_TextChanged(object sender, EventArgs e)
        {
            txtRemarks.BackColor = System.Drawing.Color.White;
        }

        private void txtPaymentValue_KeyPress(object sender, KeyPressEventArgs e)
        {

            DataAccessLayer.UseNamberOnly(e);

        }


        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                txtPayID.Text = cls_Treasury_Movement.MaxID_TreasuryMovement();
                D1.Value = DateTime.Now;
                txtCredit.Text = "";
                txtRemarks.Text = "";
                txtDebit.Text = "";
                label10.Text = "";
                txtSupplierID.Text = "";
                combSuppliers.Text = "";
                combSuppliers.BackColor = System.Drawing.Color.White;
                txtCredit.BackColor = System.Drawing.Color.White;
                txtRemarks.BackColor = System.Drawing.Color.White;

                btnSave.Enabled = true;
                btnUpdate.Enabled = false;
                btnDelete.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnSave_Click(object sender, EventArgs e)
        {

            if (txtPayID.Text == "")
            {
                MessageBox.Show("حدث خطأ يرجي المحاولة لاحقا", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if (txtSupplierID.Text == "" || combSuppliers.Text == "")
            {
                combSuppliers.BackColor = System.Drawing.Color.Pink;
                combSuppliers.Focus();
                return;
            }


            if (txtDebit.Text.Trim() == "" && txtCredit.Text.Trim() == "")
            {
                txtDebit.BackColor = System.Drawing.Color.Pink;
                txtCredit.BackColor = System.Drawing.Color.Pink;
                txtDebit.Focus();
                return;
            }
            else if (txtDebit.Text.Trim() == "0" && txtCredit.Text.Trim() == "")
            {
                txtDebit.BackColor = System.Drawing.Color.Pink;
                txtCredit.BackColor = System.Drawing.Color.Pink;
                txtDebit.Focus();
                return;
            }
            else if (txtDebit.Text.Trim() == "" && txtCredit.Text.Trim() == "0")
            {
                txtDebit.BackColor = System.Drawing.Color.Pink;
                txtCredit.BackColor = System.Drawing.Color.Pink;
                txtDebit.Focus();
                return;
            }
            else if (txtDebit.Text.Trim() == "0" && txtCredit.Text.Trim() == "0")
            {
                txtDebit.BackColor = System.Drawing.Color.Pink;
                txtCredit.BackColor = System.Drawing.Color.Pink;
                txtDebit.Focus();
                return;
            }
            if (txtRemarks.Text.Trim() == "")
            {
                txtRemarks.BackColor = System.Drawing.Color.Pink;
                txtRemarks.Focus();
                return;
            }

            string Debit = "0";
            string Credit = "0";
            if (txtDebit.Text.Trim() !="")
            {
                Debit= txtDebit.Text;
            }
            if (txtCredit.Text.Trim() != "")
            {
                Credit = txtCredit.Text;
            }

            try
            {
                txtPayID.Text = cls_Treasury_Movement.MaxID_TreasuryMovement();
                cls_Treasury_Movement.InsertTreasuryMovement(txtPayID.Text, txtSupplierID.Text, "Supp", txtPayID.Text, txtTypeID.Text, this.Text, D1.Value, combSuppliers.Text, "0", "0", txtRemarks.Text, Debit.ToString(), Credit.ToString(), Properties.Settings.Default.UserID, false);
                MessageBox.Show("تم الحفظ بنجاح", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                btnNew_Click(null, null);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void txtPaymentValue_TextChanged(object sender, EventArgs e)
        {
            txtCredit.BackColor = System.Drawing.Color.White;


            if (txtCredit.Text == "0" || txtCredit.Text == "")
            {
                txtPaymentValueArbic.Text = "";
            }
            else
            {
                txtPaymentValueArbic.Text = DataAccessLayer.horof.تحويل_الأرقام_الى_حروف(Convert.ToDecimal(txtCredit.Text), 3, Properties.Settings.Default.TopCurrency, Properties.Settings.Default.MinyCurrency, true, true);
            }
        }



        private void txtCustomersBlanse_TextChanged(object sender, EventArgs e)
        {
            txtDebit.BackColor = Color.White;
            if (txtDebit.Text == "0" || txtDebit.Text == "")
            {
                label10.Text = "";
            }
            else
            {
                label10.Text = DataAccessLayer.horof.تحويل_الأرقام_الى_حروف(Convert.ToDecimal(txtDebit.Text), 3, Properties.Settings.Default.TopCurrency, Properties.Settings.Default.MinyCurrency, true, true);
            }
        }



        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (txtPayID.Text == "")
            {
                MessageBox.Show("حدث خطأ يرجي المحاولة لاحقا", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if (txtSupplierID.Text == "" || combSuppliers.Text == "")
            {
                combSuppliers.BackColor = System.Drawing.Color.Pink;
                combSuppliers.Focus();
                return;
            }





            if (txtDebit.Text.Trim() == "" && txtCredit.Text.Trim() == "")
            {
                txtDebit.BackColor = System.Drawing.Color.Pink;
                txtCredit.BackColor = System.Drawing.Color.Pink;
                txtDebit.Focus();
                return;
            }
            else if (txtDebit.Text.Trim() == "0" && txtCredit.Text.Trim() == "")
            {
                txtDebit.BackColor = System.Drawing.Color.Pink;
                txtCredit.BackColor = System.Drawing.Color.Pink;
                txtDebit.Focus();
                return;
            }
            else if (txtDebit.Text.Trim() == "" && txtCredit.Text.Trim() == "0")
            {
                txtDebit.BackColor = System.Drawing.Color.Pink;
                txtCredit.BackColor = System.Drawing.Color.Pink;
                txtDebit.Focus();
                return;
            }
            else if (txtDebit.Text.Trim() == "0" && txtCredit.Text.Trim() == "0")
            {
                txtDebit.BackColor = System.Drawing.Color.Pink;
                txtCredit.BackColor = System.Drawing.Color.Pink;
                txtDebit.Focus();
                return;
            }

            if (txtRemarks.Text.Trim() == "")
            {
                txtRemarks.BackColor = System.Drawing.Color.Pink;
                txtRemarks.Focus();
                return;
            }

           
            string Debit = "0";
            string Credit = "0";
            if (txtDebit.Text.Trim() != "")
            {
                Debit = txtDebit.Text;
            }
            if (txtCredit.Text.Trim() != "")
            {
                Credit = txtCredit.Text;
            }

            try
            {

                cls_Treasury_Movement.UpdateTreasuryMovement(txtPayID.Text, txtSupplierID.Text, "Supp", txtPayID.Text, txtTypeID.Text, this.Text, D1.Value, combSuppliers.Text, "0", "0", txtRemarks.Text, Debit.ToString(), Credit.ToString());

                Mass.Update();
                btnNew_Click(null, null);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (Mass.Delete() == true)
                {
                    cls_Treasury_Movement.DeleteTreasuryMovement(txtPayID.Text);

                    btnNew_Click(null, null);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }





        private void txtBillTypeID_TextChanged(object sender, EventArgs e)
        {

        }

        private void combCustomers_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                combSuppliers.BackColor = Color.White;
                txtSupplierID.Text = combSuppliers.SelectedValue.ToString();

            }
            catch
            {
                txtSupplierID.Text = "";
            }
        }

        private void combCustomers_TextChanged(object sender, EventArgs e)
        {
            try
            {
                combSuppliers.BackColor = Color.White;
                if (combSuppliers.Text.Trim() == "")
                {
                    txtSupplierID.Text = "";
                }
                else
                {
                    txtSupplierID.Text = combSuppliers.SelectedValue.ToString();
                }
            }
            catch
            {
                txtSupplierID.Text = "";
            }
        }


        private void BalenceCustomer(string CustomerID)
        {
            try
            {
                if (CustomerID.Trim() == "" || CustomerID == "System.Data.DataRowView")
                {
                    return;
                }
                DataTable dt = cls_Treasury_Movement.First_Balence_trans(txtTypeID.Text, "Supp", CustomerID);
                if (dt.Rows.Count == 0)
                {
                    txtRemarks.Text = "";
                    txtDebit.Text = "";
                    txtCredit.Text = "";
                    btnSave.Enabled = true;
                    btnUpdate.Enabled = false;
                    btnDelete.Enabled = false;
                    return;
                }
                DataRow Dr = dt.Rows[0];
                txtPayID.Text = Dr["ID"].ToString();
                txtPayID.Text = Dr["VoucherID"].ToString();
                D1.Value = (DateTime)Dr["VoucherDate"];
                txtRemarks.Text = Dr["Remark"].ToString();
                txtDebit.Text = Dr["Debit"].ToString();
                txtCredit.Text = Dr["Credit"].ToString();
                btnSave.Enabled = false;
                btnUpdate.Enabled = true;
                btnDelete.Enabled = true;

            }
            catch
            {

            }


        }

        private void txtCustomerID_TextChanged(object sender, EventArgs e)
        {
            BalenceCustomer(txtSupplierID.Text);
        }
    }
}
