﻿using System;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class Money_Form : Form
    {
        public Money_Form()
        {
            InitializeComponent();
        }
        Menu_Meny_cls cls = new Menu_Meny_cls();
        private void Money_Form_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingToolStrip(toolStrip1);
            ERB_Setting.SettingForm(this);



        }













        DataTable dt_Prodect = new DataTable();
        private void btnSave_Click(object sender, EventArgs e)
        {
            Sales();
            Bay();
            Firest();
             //ProdectStore();

            dt_Prodect = cls.Details_ALL_Prodecuts();
            if (!backgroundWorker1.IsBusy)
            {
                backgroundWorker1.RunWorkerAsync();
            }
            Expense();
            Salary();
            NetGood();
        }

        private void Sales()
        {
            try
            {
                DataTable dt = cls.Search_Sales(dateTimePicker1.Value);
                txtSales_TotalInvoice.Text = dt.Rows[0]["TotalInvoice"].ToString();
                txtSales_Discount.Text = dt.Rows[0]["Discount"].ToString();
                txtSales_ReturnInvoise.Text = dt.Rows[0]["ReturnInvoise"].ToString();
                txtSales_NetInvoice.Text = (Convert.ToDouble(dt.Rows[0]["NetInvoice"].ToString()) - Convert.ToDouble(dt.Rows[0]["ReturnInvoise"])).ToString();
                //DataTable dt_byprice = cls.Search_TotalBayPrice(dateTimePicker1.Value);
                //textBox5.Text = dt_byprice.Rows[0]["TotalBayPrice"].ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }


        private void Bay()
        {
            try
            {
                DataTable dt = cls.Search_Bay(dateTimePicker1.Value);
                txtBay_TotalInvoice.Text = dt.Rows[0]["TotalInvoice"].ToString();
                txtBay_Discount.Text = dt.Rows[0]["Discount"].ToString();
                txtBay_ReturnInvoise.Text = dt.Rows[0]["ReturnInvoise"].ToString();
                txtBay_NetInvoice.Text = (Convert.ToDouble(dt.Rows[0]["NetInvoice"].ToString()) - Convert.ToDouble(dt.Rows[0]["ReturnInvoise"])).ToString();
                textBox4.Text = txtBay_NetInvoice.Text;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void Firest()
        {
            try
            {
                DataTable dt = cls.Search_FirestProdecut();
                txtTotalFirest.Text = dt.Rows[0]["TotalPrice"].ToString();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }


        private void ProdectStore()
        {
            Double sumProdect = 0;
            DataTable dt = cls.Details_ALL_Prodecuts();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                sumProdect += cls.QuantityNow_Avg(dt.Rows[i]["ProdecutID"].ToString(), dateTimePicker1.Value);
            }
            textBox1.Text = sumProdect.ToString();
            textBox3.Text = ((Convert.ToDouble(textBox4.Text) + Convert.ToDouble(txtTotalFirest.Text)) - sumProdect).ToString();
        }

        private void Expense()
        {
            try
            {
                DataTable dt = cls.Search_Expenses(dateTimePicker1.Value);
                textBox6.Text = dt.Rows[0]["ExpenseValue"].ToString();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void Salary()
        {
            try
            {
                DataTable dt = cls.Search_Salary(dateTimePicker1.Value);
                txtTotalSalary.Text = dt.Rows[0]["NetSalary"].ToString();
                txtTotalExpenses.Text = (Convert.ToDouble(txtTotalSalary.Text) + Convert.ToDouble(textBox6.Text)).ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        // مجمل اللربح او الخسارة

        private void NetGood()
        {
            textBox2.Text = (Convert.ToDouble(txtSales_NetInvoice.Text) - Convert.ToDouble(textBox3.Text) - Convert.ToDouble(txtTotalExpenses.Text)).ToString("0.00");
        }
        Double sumProdect = 0;
        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            try
            {
                sumProdect = 0;

                for (int i = 0; i < dt_Prodect.Rows.Count; i++)
            {
                sumProdect += cls.QuantityNow_Avg(dt_Prodect.Rows[i]["ProdecutID"].ToString(), dateTimePicker1.Value);
            }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            textBox1.Text = sumProdect.ToString();
            textBox3.Text = ((Convert.ToDouble(textBox4.Text) + Convert.ToDouble(txtTotalFirest.Text)) - sumProdect).ToString();
        }
    }
}
