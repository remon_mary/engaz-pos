﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class MaintenanceDatabase_frm : Form
    {
        public MaintenanceDatabase_frm()
        {
            InitializeComponent();
        }
        DataAccessLayer dal = new DataAccessLayer();
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtRun.Text))
                {
                    dal.Execute_SQL(txtRun.Text.Trim(), CommandType.Text);
                    MessageBox.Show("Save Successfully", "Successfully", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



        }
    }
}
