﻿namespace ByStro.PL
{
    partial class BillMaintenance_frm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BillMaintenance_frm));
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.D1 = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.DGV1 = new System.Windows.Forms.DataGridView();
            this.DamagedTypeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DamagedTypeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrencyPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrencyTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrencyID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrencyName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrencyRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtStoreID = new System.Windows.Forms.TextBox();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.txtQuantity = new System.Windows.Forms.TextBox();
            this.txtTotalPrice = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtProdecutID = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.txtUnitFactor = new System.Windows.Forms.TextBox();
            this.txtUnitOperating = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripDropDownButton2 = new System.Windows.Forms.ToolStripDropDownButton();
            this.ToolStripMenuItemSupplers = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.اعادةتحميلالاصنافToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnNew = new System.Windows.Forms.ToolStripButton();
            this.btnSave = new System.Windows.Forms.ToolStripButton();
            this.btnUpdate = new System.Windows.Forms.ToolStripButton();
            this.btnDelete = new System.Windows.Forms.ToolStripButton();
            this.txtAccountID = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtRestInvoise = new System.Windows.Forms.TextBox();
            this.txtpaid_Invoice = new System.Windows.Forms.TextBox();
            this.txtNetInvoice = new System.Windows.Forms.TextBox();
            this.txtTotalDescound = new System.Windows.Forms.TextBox();
            this.txtTotalInvoice = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.combPayKind = new System.Windows.Forms.ComboBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblCount = new System.Windows.Forms.ToolStripStatusLabel();
            this.txtProdecutAvg = new System.Windows.Forms.TextBox();
            this.txtBillTypeID = new System.Windows.Forms.TextBox();
            this.txtMainID = new System.Windows.Forms.TextBox();
            this.combAccount = new System.Windows.Forms.ComboBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.button5 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.statusStrip2 = new System.Windows.Forms.StatusStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.txtSumPayType = new System.Windows.Forms.ToolStripStatusLabel();
            this.DGVPayType = new System.Windows.Forms.DataGridView();
            this.PayTypeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PayTypeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrencyPrice2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Statement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrencyID2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrencyName2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrencyRate2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PayValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtStatement = new System.Windows.Forms.TextBox();
            this.txtPayValue = new System.Windows.Forms.TextBox();
            this.combPayType = new System.Windows.Forms.ComboBox();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.combCurrency = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtCurrencyRate = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.DGV1)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.statusStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVPayType)).BeginInit();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 57);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 17);
            this.label4.TabIndex = 605;
            this.label4.Text = "رقم السند :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 115);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 17);
            this.label2.TabIndex = 604;
            this.label2.Text = "البيـــان :";
            // 
            // txtRemarks
            // 
            this.txtRemarks.BackColor = System.Drawing.Color.White;
            this.txtRemarks.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRemarks.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRemarks.Location = new System.Drawing.Point(92, 112);
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(784, 23);
            this.txtRemarks.TabIndex = 601;
            // 
            // D1
            // 
            this.D1.CalendarMonthBackground = System.Drawing.Color.WhiteSmoke;
            this.D1.CustomFormat = "";
            this.D1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.D1.Location = new System.Drawing.Point(92, 27);
            this.D1.Name = "D1";
            this.D1.Size = new System.Drawing.Size(154, 24);
            this.D1.TabIndex = 603;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 17);
            this.label1.TabIndex = 602;
            this.label1.Text = "تاريخ السند :";
            // 
            // DGV1
            // 
            this.DGV1.AllowUserToAddRows = false;
            this.DGV1.AllowUserToDeleteRows = false;
            this.DGV1.AllowUserToResizeColumns = false;
            this.DGV1.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FloralWhite;
            this.DGV1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.DGV1.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(146)))), ((int)(((byte)(222)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 10F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGV1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.DGV1.ColumnHeadersHeight = 24;
            this.DGV1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.DGV1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DamagedTypeID,
            this.DamagedTypeName,
            this.Quantity,
            this.CurrencyPrice,
            this.CurrencyTotal,
            this.Price,
            this.TotalPrice,
            this.CurrencyID,
            this.CurrencyName,
            this.CurrencyRate});
            this.DGV1.Cursor = System.Windows.Forms.Cursors.Default;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Tahoma", 10F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGV1.DefaultCellStyle = dataGridViewCellStyle3;
            this.DGV1.EnableHeadersVisualStyles = false;
            this.DGV1.GridColor = System.Drawing.Color.Silver;
            this.DGV1.Location = new System.Drawing.Point(3, 55);
            this.DGV1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.DGV1.MultiSelect = false;
            this.DGV1.Name = "DGV1";
            this.DGV1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.DGV1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Tahoma", 10F);
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGV1.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.DGV1.RowHeadersVisible = false;
            this.DGV1.RowHeadersWidth = 55;
            this.DGV1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.DGV1.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.DGV1.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.DGV1.RowTemplate.Height = 23;
            this.DGV1.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.DGV1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGV1.Size = new System.Drawing.Size(706, 223);
            this.DGV1.TabIndex = 609;
            this.DGV1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGV1_CellEndEdit);
            this.DGV1.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.DGV1_EditingControlShowing);
            this.DGV1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DGV1_KeyDown);
            // 
            // DamagedTypeID
            // 
            this.DamagedTypeID.DataPropertyName = "DamagedTypeID";
            this.DamagedTypeID.HeaderText = "كود الصنف";
            this.DamagedTypeID.Name = "DamagedTypeID";
            this.DamagedTypeID.ReadOnly = true;
            this.DamagedTypeID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.DamagedTypeID.Visible = false;
            // 
            // DamagedTypeName
            // 
            this.DamagedTypeName.DataPropertyName = "DamagedTypeName";
            this.DamagedTypeName.FillWeight = 256.7848F;
            this.DamagedTypeName.HeaderText = "اسم القطعة - الصيانة";
            this.DamagedTypeName.Name = "DamagedTypeName";
            this.DamagedTypeName.ReadOnly = true;
            this.DamagedTypeName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.DamagedTypeName.Width = 303;
            // 
            // Quantity
            // 
            this.Quantity.DataPropertyName = "Quantity";
            this.Quantity.FillWeight = 79.34152F;
            this.Quantity.HeaderText = "الكمية";
            this.Quantity.Name = "Quantity";
            this.Quantity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Quantity.Width = 130;
            // 
            // CurrencyPrice
            // 
            this.CurrencyPrice.DataPropertyName = "CurrencyPrice";
            this.CurrencyPrice.HeaderText = "السعر بالعملة";
            this.CurrencyPrice.Name = "CurrencyPrice";
            this.CurrencyPrice.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CurrencyPrice.Width = 120;
            // 
            // CurrencyTotal
            // 
            this.CurrencyTotal.DataPropertyName = "CurrencyTotal";
            this.CurrencyTotal.HeaderText = "الاجمالي بالعملة";
            this.CurrencyTotal.Name = "CurrencyTotal";
            this.CurrencyTotal.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CurrencyTotal.Width = 150;
            // 
            // Price
            // 
            this.Price.DataPropertyName = "Price";
            this.Price.FillWeight = 116.319F;
            this.Price.HeaderText = "سعر الوحدة";
            this.Price.Name = "Price";
            this.Price.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Price.Width = 120;
            // 
            // TotalPrice
            // 
            this.TotalPrice.DataPropertyName = "TotalPrice";
            this.TotalPrice.HeaderText = "الاجمالي";
            this.TotalPrice.Name = "TotalPrice";
            this.TotalPrice.ReadOnly = true;
            this.TotalPrice.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.TotalPrice.Width = 150;
            // 
            // CurrencyID
            // 
            this.CurrencyID.DataPropertyName = "CurrencyID";
            this.CurrencyID.HeaderText = "CurrencyID";
            this.CurrencyID.Name = "CurrencyID";
            this.CurrencyID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CurrencyID.Visible = false;
            // 
            // CurrencyName
            // 
            this.CurrencyName.DataPropertyName = "CurrencyName";
            this.CurrencyName.HeaderText = "العملة";
            this.CurrencyName.Name = "CurrencyName";
            this.CurrencyName.ReadOnly = true;
            this.CurrencyName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // CurrencyRate
            // 
            this.CurrencyRate.DataPropertyName = "CurrencyRate";
            this.CurrencyRate.HeaderText = "سعر التعادل";
            this.CurrencyRate.Name = "CurrencyRate";
            this.CurrencyRate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // txtStoreID
            // 
            this.txtStoreID.Location = new System.Drawing.Point(454, 60);
            this.txtStoreID.Name = "txtStoreID";
            this.txtStoreID.ReadOnly = true;
            this.txtStoreID.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtStoreID.Size = new System.Drawing.Size(10, 24);
            this.txtStoreID.TabIndex = 626;
            this.txtStoreID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtStoreID.Visible = false;
            this.txtStoreID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AllKeyPress);
            // 
            // txtSearch
            // 
            this.txtSearch.BackColor = System.Drawing.Color.White;
            this.txtSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSearch.Location = new System.Drawing.Point(404, 30);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtSearch.Size = new System.Drawing.Size(304, 24);
            this.txtSearch.TabIndex = 635;
            this.txtSearch.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyDown);
            // 
            // txtQuantity
            // 
            this.txtQuantity.BackColor = System.Drawing.Color.White;
            this.txtQuantity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtQuantity.Location = new System.Drawing.Point(273, 30);
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtQuantity.Size = new System.Drawing.Size(130, 24);
            this.txtQuantity.TabIndex = 636;
            this.txtQuantity.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtQuantity.TextChanged += new System.EventHandler(this.txtQty_TextChanged);
            this.txtQuantity.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtQty_KeyDown);
            this.txtQuantity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AllKeyPress);
            // 
            // txtTotalPrice
            // 
            this.txtTotalPrice.BackColor = System.Drawing.Color.White;
            this.txtTotalPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotalPrice.Location = new System.Drawing.Point(5, 30);
            this.txtTotalPrice.Name = "txtTotalPrice";
            this.txtTotalPrice.ReadOnly = true;
            this.txtTotalPrice.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtTotalPrice.Size = new System.Drawing.Size(147, 24);
            this.txtTotalPrice.TabIndex = 637;
            this.txtTotalPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtTotalPrice.TextChanged += new System.EventHandler(this.txtPrise_TextChanged);
            this.txtTotalPrice.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPrise_KeyDown);
            this.txtTotalPrice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AllKeyPress);
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label10.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label10.Location = new System.Drawing.Point(404, 5);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(304, 24);
            this.label10.TabIndex = 640;
            this.label10.Text = "اسم القطعة - الصيانة";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label12.Location = new System.Drawing.Point(273, 5);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(130, 24);
            this.label12.TabIndex = 642;
            this.label12.Text = "الكمية";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtProdecutID
            // 
            this.txtProdecutID.Location = new System.Drawing.Point(454, 60);
            this.txtProdecutID.Name = "txtProdecutID";
            this.txtProdecutID.Size = new System.Drawing.Size(10, 24);
            this.txtProdecutID.TabIndex = 645;
            this.txtProdecutID.Visible = false;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Location = new System.Drawing.Point(153, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 24);
            this.label3.TabIndex = 648;
            this.label3.Text = "سعر الوحدة";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label5.Location = new System.Drawing.Point(5, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(147, 24);
            this.label5.TabIndex = 656;
            this.label5.Text = "الاجمالي";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtPrice
            // 
            this.txtPrice.BackColor = System.Drawing.Color.White;
            this.txtPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPrice.Location = new System.Drawing.Point(153, 30);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtPrice.Size = new System.Drawing.Size(119, 24);
            this.txtPrice.TabIndex = 655;
            this.txtPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtPrice.TextChanged += new System.EventHandler(this.txtPrice_TextChanged);
            this.txtPrice.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPrice_KeyDown);
            this.txtPrice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AllKeyPress);
            // 
            // txtUnitFactor
            // 
            this.txtUnitFactor.Location = new System.Drawing.Point(454, 60);
            this.txtUnitFactor.Name = "txtUnitFactor";
            this.txtUnitFactor.Size = new System.Drawing.Size(10, 24);
            this.txtUnitFactor.TabIndex = 657;
            this.txtUnitFactor.Visible = false;
            // 
            // txtUnitOperating
            // 
            this.txtUnitOperating.Location = new System.Drawing.Point(454, 60);
            this.txtUnitOperating.Name = "txtUnitOperating";
            this.txtUnitOperating.Size = new System.Drawing.Size(10, 24);
            this.txtUnitOperating.TabIndex = 658;
            this.txtUnitOperating.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(30, 86);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 17);
            this.label9.TabIndex = 687;
            this.label9.Text = "العميل :";
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDropDownButton2,
            this.btnNew,
            this.btnSave,
            this.btnUpdate,
            this.btnDelete});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(881, 25);
            this.toolStrip1.TabIndex = 690;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripDropDownButton2
            // 
            this.toolStripDropDownButton2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItemSupplers,
            this.toolStripSeparator4,
            this.اعادةتحميلالاصنافToolStripMenuItem,
            this.toolStripSeparator1});
            this.toolStripDropDownButton2.Image = global::ByStro.Properties.Resources.Tools;
            this.toolStripDropDownButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton2.Name = "toolStripDropDownButton2";
            this.toolStripDropDownButton2.Size = new System.Drawing.Size(74, 22);
            this.toolStripDropDownButton2.Text = "خيارات";
            // 
            // ToolStripMenuItemSupplers
            // 
            this.ToolStripMenuItemSupplers.Image = global::ByStro.Properties.Resources.supp;
            this.ToolStripMenuItemSupplers.Name = "ToolStripMenuItemSupplers";
            this.ToolStripMenuItemSupplers.Size = new System.Drawing.Size(195, 22);
            this.ToolStripMenuItemSupplers.Text = "كشف حساب عميل";
            this.ToolStripMenuItemSupplers.Click += new System.EventHandler(this.ToolStripMenuItemSupplers_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(192, 6);
            // 
            // اعادةتحميلالاصنافToolStripMenuItem
            // 
            this.اعادةتحميلالاصنافToolStripMenuItem.Image = global::ByStro.Properties.Resources.Ingredients_48px;
            this.اعادةتحميلالاصنافToolStripMenuItem.Name = "اعادةتحميلالاصنافToolStripMenuItem";
            this.اعادةتحميلالاصنافToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.اعادةتحميلالاصنافToolStripMenuItem.Text = "اعادة تحميل الاصناف";
            this.اعادةتحميلالاصنافToolStripMenuItem.Click += new System.EventHandler(this.اعادةتحميلالاصنافToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(192, 6);
            // 
            // btnNew
            // 
            this.btnNew.Image = global::ByStro.Properties.Resources.New;
            this.btnNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(61, 22);
            this.btnNew.Text = "جديد ";
            this.btnNew.ToolTipText = "جديد Ctrl+N";
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnSave
            // 
            this.btnSave.Image = global::ByStro.Properties.Resources.Save_and_New;
            this.btnSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(81, 22);
            this.btnSave.Text = "حفظ F10";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Image = global::ByStro.Properties.Resources.edit;
            this.btnUpdate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(84, 22);
            this.btnUpdate.Text = "تعديل F3 ";
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Image = global::ByStro.Properties.Resources.Pic030;
            this.btnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(61, 22);
            this.btnDelete.Text = "حذف ";
            this.btnDelete.Click += new System.EventHandler(this.toolStripButton5_Click);
            // 
            // txtAccountID
            // 
            this.txtAccountID.Location = new System.Drawing.Point(454, 23);
            this.txtAccountID.Name = "txtAccountID";
            this.txtAccountID.Size = new System.Drawing.Size(10, 24);
            this.txtAccountID.TabIndex = 691;
            this.txtAccountID.Visible = false;
            this.txtAccountID.TextChanged += new System.EventHandler(this.txtAccountID_TextChanged);
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label19.BackColor = System.Drawing.SystemColors.Control;
            this.label19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label19.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(726, 211);
            this.label19.Name = "label19";
            this.label19.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label19.Size = new System.Drawing.Size(150, 24);
            this.label19.TabIndex = 767;
            this.label19.Text = "خصم";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtRestInvoise
            // 
            this.txtRestInvoise.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtRestInvoise.BackColor = System.Drawing.Color.White;
            this.txtRestInvoise.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRestInvoise.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold);
            this.txtRestInvoise.ForeColor = System.Drawing.Color.Blue;
            this.txtRestInvoise.Location = new System.Drawing.Point(726, 441);
            this.txtRestInvoise.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtRestInvoise.Name = "txtRestInvoise";
            this.txtRestInvoise.ReadOnly = true;
            this.txtRestInvoise.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtRestInvoise.Size = new System.Drawing.Size(150, 24);
            this.txtRestInvoise.TabIndex = 765;
            this.txtRestInvoise.Text = "0.00";
            this.txtRestInvoise.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtpaid_Invoice
            // 
            this.txtpaid_Invoice.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtpaid_Invoice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtpaid_Invoice.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold);
            this.txtpaid_Invoice.ForeColor = System.Drawing.Color.Green;
            this.txtpaid_Invoice.Location = new System.Drawing.Point(726, 388);
            this.txtpaid_Invoice.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtpaid_Invoice.Name = "txtpaid_Invoice";
            this.txtpaid_Invoice.ReadOnly = true;
            this.txtpaid_Invoice.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtpaid_Invoice.Size = new System.Drawing.Size(150, 24);
            this.txtpaid_Invoice.TabIndex = 766;
            this.txtpaid_Invoice.Text = "0";
            this.txtpaid_Invoice.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtpaid_Invoice.TextChanged += new System.EventHandler(this.txtpaid_Invoice_TextChanged);
            // 
            // txtNetInvoice
            // 
            this.txtNetInvoice.BackColor = System.Drawing.Color.White;
            this.txtNetInvoice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNetInvoice.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold);
            this.txtNetInvoice.ForeColor = System.Drawing.Color.Red;
            this.txtNetInvoice.Location = new System.Drawing.Point(726, 288);
            this.txtNetInvoice.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtNetInvoice.Name = "txtNetInvoice";
            this.txtNetInvoice.ReadOnly = true;
            this.txtNetInvoice.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtNetInvoice.Size = new System.Drawing.Size(150, 24);
            this.txtNetInvoice.TabIndex = 764;
            this.txtNetInvoice.Text = "0.00";
            this.txtNetInvoice.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtTotalDescound
            // 
            this.txtTotalDescound.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTotalDescound.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotalDescound.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold);
            this.txtTotalDescound.Location = new System.Drawing.Point(726, 236);
            this.txtTotalDescound.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTotalDescound.Name = "txtTotalDescound";
            this.txtTotalDescound.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtTotalDescound.Size = new System.Drawing.Size(150, 24);
            this.txtTotalDescound.TabIndex = 760;
            this.txtTotalDescound.Text = "0";
            this.txtTotalDescound.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtTotalDescound.TextChanged += new System.EventHandler(this.txtChange);
            // 
            // txtTotalInvoice
            // 
            this.txtTotalInvoice.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTotalInvoice.BackColor = System.Drawing.Color.White;
            this.txtTotalInvoice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotalInvoice.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalInvoice.Location = new System.Drawing.Point(726, 186);
            this.txtTotalInvoice.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTotalInvoice.Name = "txtTotalInvoice";
            this.txtTotalInvoice.ReadOnly = true;
            this.txtTotalInvoice.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtTotalInvoice.Size = new System.Drawing.Size(150, 24);
            this.txtTotalInvoice.TabIndex = 759;
            this.txtTotalInvoice.Text = "0.00";
            this.txtTotalInvoice.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtTotalInvoice.TextChanged += new System.EventHandler(this.txtChange);
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.BackColor = System.Drawing.SystemColors.Control;
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label7.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(726, 313);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label7.Size = new System.Drawing.Size(150, 24);
            this.label7.TabIndex = 762;
            this.label7.Text = "نوع الدفع";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label24
            // 
            this.label24.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label24.BackColor = System.Drawing.SystemColors.Control;
            this.label24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label24.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(726, 414);
            this.label24.Name = "label24";
            this.label24.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label24.Size = new System.Drawing.Size(150, 24);
            this.label24.TabIndex = 755;
            this.label24.Text = "المبلغ الباقي";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label15.BackColor = System.Drawing.SystemColors.Control;
            this.label15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label15.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(726, 263);
            this.label15.Name = "label15";
            this.label15.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label15.Size = new System.Drawing.Size(150, 24);
            this.label15.TabIndex = 758;
            this.label15.Text = "صافي الفاتورة";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label18.BackColor = System.Drawing.SystemColors.Control;
            this.label18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label18.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(726, 363);
            this.label18.Name = "label18";
            this.label18.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label18.Size = new System.Drawing.Size(150, 24);
            this.label18.TabIndex = 757;
            this.label18.Text = "المبلغ المسدد";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label20
            // 
            this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label20.BackColor = System.Drawing.SystemColors.Control;
            this.label20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label20.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(726, 161);
            this.label20.Name = "label20";
            this.label20.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label20.Size = new System.Drawing.Size(150, 24);
            this.label20.TabIndex = 754;
            this.label20.Text = "اجمالي الفاتورة";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // combPayKind
            // 
            this.combPayKind.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.combPayKind.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combPayKind.FormattingEnabled = true;
            this.combPayKind.Items.AddRange(new object[] {
            "نقدي",
            "أجل"});
            this.combPayKind.Location = new System.Drawing.Point(726, 338);
            this.combPayKind.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.combPayKind.Name = "combPayKind";
            this.combPayKind.Size = new System.Drawing.Size(150, 24);
            this.combPayKind.TabIndex = 763;
            this.combPayKind.SelectedIndexChanged += new System.EventHandler(this.combPayKind_SelectedIndexChanged);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton3,
            this.toolStripStatusLabel1,
            this.lblCount});
            this.statusStrip1.Location = new System.Drawing.Point(3, 276);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(706, 23);
            this.statusStrip1.TabIndex = 768;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.Image = global::ByStro.Properties.Resources.Deletex;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(150, 21);
            this.toolStripButton3.Text = "حذف الصنف المحدد  ";
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(88, 18);
            this.toolStripStatusLabel1.Text = "عدد الاصناف :";
            // 
            // lblCount
            // 
            this.lblCount.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCount.ForeColor = System.Drawing.Color.Red;
            this.lblCount.Name = "lblCount";
            this.lblCount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblCount.Size = new System.Drawing.Size(17, 18);
            this.lblCount.Text = "0";
            // 
            // txtProdecutAvg
            // 
            this.txtProdecutAvg.Location = new System.Drawing.Point(454, 60);
            this.txtProdecutAvg.Name = "txtProdecutAvg";
            this.txtProdecutAvg.ReadOnly = true;
            this.txtProdecutAvg.Size = new System.Drawing.Size(10, 24);
            this.txtProdecutAvg.TabIndex = 770;
            this.txtProdecutAvg.Visible = false;
            // 
            // txtBillTypeID
            // 
            this.txtBillTypeID.Location = new System.Drawing.Point(528, 23);
            this.txtBillTypeID.Name = "txtBillTypeID";
            this.txtBillTypeID.ReadOnly = true;
            this.txtBillTypeID.Size = new System.Drawing.Size(20, 24);
            this.txtBillTypeID.TabIndex = 773;
            this.txtBillTypeID.Text = "Maintenance";
            this.txtBillTypeID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBillTypeID.Visible = false;
            // 
            // txtMainID
            // 
            this.txtMainID.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtMainID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMainID.Location = new System.Drawing.Point(92, 54);
            this.txtMainID.Name = "txtMainID";
            this.txtMainID.ReadOnly = true;
            this.txtMainID.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtMainID.Size = new System.Drawing.Size(310, 24);
            this.txtMainID.TabIndex = 774;
            this.txtMainID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // combAccount
            // 
            this.combAccount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.combAccount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.combAccount.FormattingEnabled = true;
            this.combAccount.Location = new System.Drawing.Point(92, 83);
            this.combAccount.Name = "combAccount";
            this.combAccount.Size = new System.Drawing.Size(309, 24);
            this.combAccount.TabIndex = 775;
            this.combAccount.SelectedIndexChanged += new System.EventHandler(this.combAccount_SelectedIndexChanged);
            this.combAccount.TextChanged += new System.EventHandler(this.combAccount_TextChanged);
            this.combAccount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.combAccount_KeyDown);
            // 
            // button5
            // 
            this.button5.BackgroundImage = global::ByStro.Properties.Resources.Search_20;
            this.button5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button5.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(408, 53);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(29, 25);
            this.button5.TabIndex = 607;
            this.toolTip1.SetToolTip(this.button5, "البحث في الفواتير السابقة F5");
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button1
            // 
            this.button1.BackgroundImage = global::ByStro.Properties.Resources.icons8_Plus_30;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(408, 81);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(29, 25);
            this.button1.TabIndex = 785;
            this.toolTip1.SetToolTip(this.button1, "اضافة عميل جديد");
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(2, 135);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.RightToLeftLayout = true;
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(720, 331);
            this.tabControl1.TabIndex = 784;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.DGV1);
            this.tabPage1.Controls.Add(this.txtPrice);
            this.tabPage1.Controls.Add(this.txtTotalPrice);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.txtQuantity);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.statusStrip1);
            this.tabPage1.Controls.Add(this.txtSearch);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(712, 302);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "الاصناف";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox2);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(712, 302);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "انواع الدفع F4";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.label25);
            this.groupBox2.Controls.Add(this.statusStrip2);
            this.groupBox2.Controls.Add(this.DGVPayType);
            this.groupBox2.Controls.Add(this.txtStatement);
            this.groupBox2.Controls.Add(this.txtPayValue);
            this.groupBox2.Controls.Add(this.combPayType);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox2.Location = new System.Drawing.Point(3, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(706, 293);
            this.groupBox2.TabIndex = 781;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "انواع الدفع ";
            // 
            // label23
            // 
            this.label23.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label23.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label23.Location = new System.Drawing.Point(448, 19);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(255, 24);
            this.label23.TabIndex = 788;
            this.label23.Text = "نوع الدفع";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label22
            // 
            this.label22.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label22.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label22.Location = new System.Drawing.Point(315, 19);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(132, 24);
            this.label22.TabIndex = 787;
            this.label22.Text = "المبلغ";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label25
            // 
            this.label25.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label25.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label25.Location = new System.Drawing.Point(3, 19);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(311, 24);
            this.label25.TabIndex = 786;
            this.label25.Text = "البيان";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // statusStrip2
            // 
            this.statusStrip2.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripStatusLabel2,
            this.txtSumPayType});
            this.statusStrip2.Location = new System.Drawing.Point(3, 267);
            this.statusStrip2.Name = "statusStrip2";
            this.statusStrip2.Size = new System.Drawing.Size(700, 23);
            this.statusStrip2.TabIndex = 785;
            this.statusStrip2.Text = "statusStrip2";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = global::ByStro.Properties.Resources.Deletex;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(114, 21);
            this.toolStripButton1.Text = "حذف  المحدد  ";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(69, 18);
            this.toolStripStatusLabel2.Text = "الاجمالي :";
            // 
            // txtSumPayType
            // 
            this.txtSumPayType.BackColor = System.Drawing.Color.Transparent;
            this.txtSumPayType.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSumPayType.ForeColor = System.Drawing.Color.Red;
            this.txtSumPayType.Name = "txtSumPayType";
            this.txtSumPayType.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtSumPayType.Size = new System.Drawing.Size(17, 18);
            this.txtSumPayType.Text = "0";
            // 
            // DGVPayType
            // 
            this.DGVPayType.AllowUserToAddRows = false;
            this.DGVPayType.AllowUserToDeleteRows = false;
            this.DGVPayType.AllowUserToResizeColumns = false;
            this.DGVPayType.AllowUserToResizeRows = false;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FloralWhite;
            this.DGVPayType.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle6;
            this.DGVPayType.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGVPayType.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(146)))), ((int)(((byte)(222)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Tahoma", 10F);
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVPayType.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.DGVPayType.ColumnHeadersHeight = 24;
            this.DGVPayType.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.DGVPayType.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PayTypeName,
            this.PayTypeID,
            this.CurrencyPrice2,
            this.Statement,
            this.CurrencyID2,
            this.CurrencyName2,
            this.CurrencyRate2,
            this.PayValue});
            this.DGVPayType.Cursor = System.Windows.Forms.Cursors.Default;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Tahoma", 10F);
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGVPayType.DefaultCellStyle = dataGridViewCellStyle8;
            this.DGVPayType.EnableHeadersVisualStyles = false;
            this.DGVPayType.GridColor = System.Drawing.Color.Silver;
            this.DGVPayType.Location = new System.Drawing.Point(3, 69);
            this.DGVPayType.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.DGVPayType.MultiSelect = false;
            this.DGVPayType.Name = "DGVPayType";
            this.DGVPayType.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.DGVPayType.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Tahoma", 10F);
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGVPayType.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.DGVPayType.RowHeadersVisible = false;
            this.DGVPayType.RowHeadersWidth = 55;
            this.DGVPayType.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black;
            this.DGVPayType.RowsDefaultCellStyle = dataGridViewCellStyle10;
            this.DGVPayType.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.DGVPayType.RowTemplate.Height = 23;
            this.DGVPayType.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.DGVPayType.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGVPayType.Size = new System.Drawing.Size(700, 197);
            this.DGVPayType.TabIndex = 784;
            this.DGVPayType.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGVPayType_CellEndEdit);
            // 
            // PayTypeName
            // 
            this.PayTypeName.DataPropertyName = "PayTypeName";
            this.PayTypeName.HeaderText = "نوع الدفع";
            this.PayTypeName.Name = "PayTypeName";
            this.PayTypeName.ReadOnly = true;
            this.PayTypeName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PayTypeName.Width = 252;
            // 
            // PayTypeID
            // 
            this.PayTypeID.DataPropertyName = "PayTypeID";
            this.PayTypeID.HeaderText = "PayTypeID";
            this.PayTypeID.Name = "PayTypeID";
            this.PayTypeID.ReadOnly = true;
            this.PayTypeID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PayTypeID.Visible = false;
            // 
            // CurrencyPrice2
            // 
            this.CurrencyPrice2.HeaderText = "المبلغ بالعملة";
            this.CurrencyPrice2.Name = "CurrencyPrice2";
            this.CurrencyPrice2.Width = 130;
            // 
            // Statement
            // 
            this.Statement.DataPropertyName = "Statement";
            this.Statement.HeaderText = "البيان";
            this.Statement.Name = "Statement";
            this.Statement.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Statement.Width = 386;
            // 
            // CurrencyID2
            // 
            this.CurrencyID2.HeaderText = "CurrencyID2";
            this.CurrencyID2.Name = "CurrencyID2";
            this.CurrencyID2.Visible = false;
            // 
            // CurrencyName2
            // 
            this.CurrencyName2.HeaderText = "العملة";
            this.CurrencyName2.Name = "CurrencyName2";
            this.CurrencyName2.ReadOnly = true;
            // 
            // CurrencyRate2
            // 
            this.CurrencyRate2.HeaderText = "سعر التعادل";
            this.CurrencyRate2.Name = "CurrencyRate2";
            // 
            // PayValue
            // 
            this.PayValue.DataPropertyName = "Debit";
            this.PayValue.HeaderText = "المبلغ بعد التحويل";
            this.PayValue.Name = "PayValue";
            this.PayValue.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.PayValue.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PayValue.Width = 135;
            // 
            // txtStatement
            // 
            this.txtStatement.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStatement.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtStatement.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold);
            this.txtStatement.ForeColor = System.Drawing.Color.Black;
            this.txtStatement.Location = new System.Drawing.Point(3, 44);
            this.txtStatement.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtStatement.Name = "txtStatement";
            this.txtStatement.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtStatement.Size = new System.Drawing.Size(311, 24);
            this.txtStatement.TabIndex = 783;
            this.txtStatement.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtStatement.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtStatement_KeyDown);
            // 
            // txtPayValue
            // 
            this.txtPayValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPayValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPayValue.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold);
            this.txtPayValue.ForeColor = System.Drawing.Color.Black;
            this.txtPayValue.Location = new System.Drawing.Point(315, 44);
            this.txtPayValue.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtPayValue.Name = "txtPayValue";
            this.txtPayValue.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtPayValue.Size = new System.Drawing.Size(132, 24);
            this.txtPayValue.TabIndex = 780;
            this.txtPayValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtPayValue.TextChanged += new System.EventHandler(this.txtPayValue_TextChanged);
            this.txtPayValue.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPayValue_KeyDown);
            this.txtPayValue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPayValue_KeyPress);
            // 
            // combPayType
            // 
            this.combPayType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.combPayType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.combPayType.FormattingEnabled = true;
            this.combPayType.Location = new System.Drawing.Point(448, 44);
            this.combPayType.Name = "combPayType";
            this.combPayType.Size = new System.Drawing.Size(255, 24);
            this.combPayType.TabIndex = 779;
            this.combPayType.TextChanged += new System.EventHandler(this.combPayType_TextChanged);
            this.combPayType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.combPayType_KeyDown);
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(492, 23);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.ReadOnly = true;
            this.txtPhone.Size = new System.Drawing.Size(11, 24);
            this.txtPhone.TabIndex = 786;
            this.txtPhone.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPhone.Visible = false;
            // 
            // combCurrency
            // 
            this.combCurrency.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.combCurrency.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.combCurrency.FormattingEnabled = true;
            this.combCurrency.Location = new System.Drawing.Point(566, 53);
            this.combCurrency.Name = "combCurrency";
            this.combCurrency.Size = new System.Drawing.Size(310, 24);
            this.combCurrency.TabIndex = 807;
            this.combCurrency.SelectedIndexChanged += new System.EventHandler(this.combCurrency_SelectedIndexChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(476, 86);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(84, 17);
            this.label21.TabIndex = 806;
            this.label21.Text = "سعر الصرف :";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(507, 56);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(53, 17);
            this.label17.TabIndex = 805;
            this.label17.Text = "العملة :";
            // 
            // txtCurrencyRate
            // 
            this.txtCurrencyRate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCurrencyRate.BackColor = System.Drawing.Color.White;
            this.txtCurrencyRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrencyRate.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrencyRate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtCurrencyRate.Location = new System.Drawing.Point(566, 83);
            this.txtCurrencyRate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCurrencyRate.Name = "txtCurrencyRate";
            this.txtCurrencyRate.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtCurrencyRate.Size = new System.Drawing.Size(310, 24);
            this.txtCurrencyRate.TabIndex = 804;
            this.txtCurrencyRate.Text = "1";
            this.txtCurrencyRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCurrencyRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCurrencyRate_KeyPress);
            // 
            // BillMaintenance_frm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(881, 469);
            this.Controls.Add(this.combCurrency);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.txtCurrencyRate);
            this.Controls.Add(this.txtPhone);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.txtMainID);
            this.Controls.Add(this.txtBillTypeID);
            this.Controls.Add(this.txtProdecutAvg);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.txtRestInvoise);
            this.Controls.Add(this.txtpaid_Invoice);
            this.Controls.Add(this.txtNetInvoice);
            this.Controls.Add(this.txtTotalDescound);
            this.Controls.Add(this.txtTotalInvoice);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.combPayKind);
            this.Controls.Add(this.D1);
            this.Controls.Add(this.txtAccountID);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtUnitOperating);
            this.Controls.Add(this.txtUnitFactor);
            this.Controls.Add(this.txtProdecutID);
            this.Controls.Add(this.txtStoreID);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtRemarks);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.combAccount);
            this.Font = new System.Drawing.Font("Tahoma", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "BillMaintenance_frm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "فاتورة صيانة";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OrderProudect_frm_FormClosing);
            this.Load += new System.EventHandler(this.PureItemToStoreForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.BillBay_frm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.DGV1)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.statusStrip2.ResumeLayout(false);
            this.statusStrip2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVPayType)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label4;
        internal System.Windows.Forms.Label label2;
        internal System.Windows.Forms.TextBox txtRemarks;
        private System.Windows.Forms.DateTimePicker D1;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.DataGridView DGV1;
        private System.Windows.Forms.TextBox txtStoreID;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.TextBox txtQuantity;
        private System.Windows.Forms.TextBox txtTotalPrice;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtProdecutID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtPrice;
        private System.Windows.Forms.TextBox txtUnitFactor;
        private System.Windows.Forms.TextBox txtUnitOperating;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton2;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemSupplers;
        private System.Windows.Forms.ToolStripMenuItem اعادةتحميلالاصنافToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.TextBox txtAccountID;
        public System.Windows.Forms.Label label19;
        public System.Windows.Forms.TextBox txtRestInvoise;
        public System.Windows.Forms.TextBox txtpaid_Invoice;
        public System.Windows.Forms.TextBox txtNetInvoice;
        public System.Windows.Forms.TextBox txtTotalDescound;
        public System.Windows.Forms.TextBox txtTotalInvoice;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.Label label24;
        public System.Windows.Forms.Label label15;
        public System.Windows.Forms.Label label18;
        public System.Windows.Forms.Label label20;
        public System.Windows.Forms.ComboBox combPayKind;
        private System.Windows.Forms.ToolStripButton btnNew;
        private System.Windows.Forms.ToolStripButton btnSave;
        private System.Windows.Forms.ToolStripButton btnDelete;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel lblCount;
        private System.Windows.Forms.ToolStripButton btnUpdate;
        private System.Windows.Forms.TextBox txtProdecutAvg;
        private System.Windows.Forms.TextBox txtBillTypeID;
        private System.Windows.Forms.TextBox txtMainID;
        private System.Windows.Forms.ComboBox combAccount;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.StatusStrip statusStrip2;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel txtSumPayType;
        public System.Windows.Forms.DataGridView DGVPayType;
        public System.Windows.Forms.TextBox txtStatement;
        public System.Windows.Forms.TextBox txtPayValue;
        private System.Windows.Forms.ComboBox combPayType;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.ComboBox combCurrency;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label17;
        public System.Windows.Forms.TextBox txtCurrencyRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn PayTypeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn PayTypeID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrencyPrice2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Statement;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrencyID2;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrencyName2;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrencyRate2;
        private System.Windows.Forms.DataGridViewTextBoxColumn PayValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn DamagedTypeID;
        private System.Windows.Forms.DataGridViewTextBoxColumn DamagedTypeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrencyPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrencyTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn Price;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrencyID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrencyName;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrencyRate;
    }
}