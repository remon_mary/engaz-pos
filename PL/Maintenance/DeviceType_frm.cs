﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
namespace ByStro.PL
{
    public partial class DeviceType_frm : Form
    {
        public DeviceType_frm()
        {
            InitializeComponent();
        }
        DeviceType_cls cls = new DeviceType_cls();
        public Boolean LoadData = false;
        private void AccountEndAdd_Form_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingForm(this);
            btnNew_Click(null, null);
            FillCombMaintenanceCompeny();
        }

        public void FillCombMaintenanceCompeny()
        {

            MaintenanceCompeny_cls Compeny_cls = new MaintenanceCompeny_cls();
            combMaintenanceCompeny.DataSource = Compeny_cls.Search_MaintenanceCompeny("");
            combMaintenanceCompeny.DisplayMember = "CompenyName";
            combMaintenanceCompeny.ValueMember = "CompenyID";
            combMaintenanceCompeny.Text = null;

        }
        private void AccountEndAdd_Form_KeyDown(object sender, KeyEventArgs e)
        {
            #region "KeyDown"
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    Close();
                }
                if (e.Control == true && e.KeyCode == Keys.N)
                {
                    btnNew_Click(null, null);
                }
                if (e.KeyCode == Keys.F2)
                {
                    if (btnSave.Enabled == true)
                    {
                        btnSave_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F3)
                {
                    if (btnUpdate.Enabled == true)
                    {
                        btnUpdate_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.Delete)
                {
                    if (btnDelete.Enabled == true)
                    {
                        btnDelete_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F5)
                {
                    btnGetData_Click(null, null);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            #endregion
          
        }

        private void txtAccountName_TextChanged(object sender, EventArgs e)
        {
            txtDeviceTypeName.BackColor = Color.White;
        }

        private void txtCurrencyVal_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

  

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                //"Conditional"
                if (txtDeviceTypeName.Text.Trim() == "")
                {
                    txtDeviceTypeName.BackColor = Color.Pink;
                    txtDeviceTypeName.Focus();
                    return;
                }
                //"Conditional"
                if (combMaintenanceCompeny.SelectedIndex < 0)
                {
                    combMaintenanceCompeny.BackColor = Color.Pink;
                    combMaintenanceCompeny.Focus();
                    return;
                }


                // " Search TextBox "
                DataTable DtSearch = cls.NameSearch__DeviceType(txtDeviceTypeName.Text.Trim());
                if (DtSearch.Rows.Count > 0)
                {
                    MessageBox.Show("الاسم المدخل موجود مسبقا", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtDeviceTypeName.BackColor = Color.Pink;
                    txtDeviceTypeName.Focus();
                    return;
                }




                txtDeviceTypeID.Text = cls.MaxID_DeviceType();
                cls.Insert_DeviceType(txtDeviceTypeID.Text, txtDeviceTypeName.Text.Trim(),combMaintenanceCompeny.SelectedValue.ToString(),txtRemark.Text);
                btnNew_Click(null, null);
                LoadData = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                // "Conditional"
                if (txtDeviceTypeName.Text.Trim() == "")
                {
                    txtDeviceTypeName.BackColor = Color.Pink;
                    txtDeviceTypeName.Focus();
                    return;
                }

                //"Conditional"
                if (combMaintenanceCompeny.SelectedIndex<0)
                {
                    combMaintenanceCompeny.BackColor = Color.Pink;
                    combMaintenanceCompeny.Focus();
                    return;
                }



                cls.Update_DeviceType(txtDeviceTypeID.Text, txtDeviceTypeName.Text.Trim(), combMaintenanceCompeny.SelectedValue.ToString(), txtRemark.Text);
                btnNew_Click(null, null);
                LoadData = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {

                                // fill combBox combParent
            
                //=============================================================================================

                txtDeviceTypeID.Text = cls.MaxID_DeviceType();
                txtDeviceTypeName.Text = "";
                combMaintenanceCompeny.Text = null;
                txtRemark.Text ="";
                btnSave.Enabled = true;
                btnUpdate.Enabled = false;
                btnDelete.Enabled = false;
                txtDeviceTypeName.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {





                #region " Search TextBox "



                //DataTable DtItem = cls.Search_Category_Delete( txtDeviceTypeID.Text);
                //if (DtItem.Rows.Count > 0)
                //{
                //    MessageBox.Show("لا يمكنك حذف مجموعة يوجد بداخلها اصناف ", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //    return;
                //}

                #endregion

                if (Mass.Delete() == true)
                {
                    cls.Delete_DeviceType( txtDeviceTypeID.Text);
                    btnNew_Click(null, null); 
                    LoadData = true;
                }  
             
            
            



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnGetData_Click(object sender, EventArgs e)
        {
           

            try
            {
                DeviceTypeSearch_frm frm = new DeviceTypeSearch_frm();
                frm.ShowDialog(this);
                if (frm.loaddata==false)
                {
                    return;
                }

                DataTable dt = cls.Details_DeviceType( frm.DGV1.CurrentRow.Cells["DeviceTypeID"].Value.ToString());
                DataRow Dr = dt.Rows[0];
                txtDeviceTypeID.Text = Dr["DeviceTypeID"].ToString();
                txtDeviceTypeName.Text = Dr["DeviceTypeName"].ToString();
                combMaintenanceCompeny.Text = Dr["CompenyName"].ToString();
                txtRemark.Text =Dr["Remark"].ToString();
     
                btnSave.Enabled = false;
                btnUpdate.Enabled = true;
                btnDelete.Enabled = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }


        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            MaintenanceCompeny_frm frm = new MaintenanceCompeny_frm();
            frm.txtCompenyName.Text = combMaintenanceCompeny.Text;
            frm.ShowDialog(this);

        }

        private void combMaintenanceCompeny_SelectedIndexChanged(object sender, EventArgs e)
        {
            combMaintenanceCompeny.BackColor = Color.White;

        }

     

      



  

       












    }
}
