﻿using System;
using System.Data;

  class MaintenanceCompeny_cls : DataAccessLayer
    {
        //MaxID
        public String MaxID_MaintenanceCompeny()
        {
            return Execute_SQL("select ISNULL (MAX(CompenyID)+1,1) from MaintenanceCompeny", CommandType.Text);
        }


        // Insert
        public void Insert_MaintenanceCompeny(string CompenyID, String CompenyName, string Remark)
        {
            Execute_SQL("insert into MaintenanceCompeny(CompenyID ,CompenyName ,Remark )Values (@CompenyID ,@CompenyName ,@Remark )", CommandType.Text,
            Parameter("@CompenyID", SqlDbType.NVarChar, CompenyID),
            Parameter("@CompenyName", SqlDbType.NVarChar, CompenyName),
            Parameter("@Remark", SqlDbType.NText, Remark));
        }

        //Update
        public void Update_MaintenanceCompeny(string CompenyID, String CompenyName, string Remark)
        {
            Execute_SQL("Update MaintenanceCompeny Set CompenyID=@CompenyID ,CompenyName=@CompenyName ,Remark=@Remark  where CompenyID=@CompenyID", CommandType.Text,
            Parameter("@CompenyID", SqlDbType.NVarChar, CompenyID),
            Parameter("@CompenyName", SqlDbType.NVarChar, CompenyName),
            Parameter("@Remark", SqlDbType.NText, Remark));
        }

        //Delete
        public void Delete_MaintenanceCompeny(string CompenyID)
        {
            Execute_SQL("Delete  From MaintenanceCompeny where CompenyID=@CompenyID", CommandType.Text,
            Parameter("@CompenyID", SqlDbType.NVarChar, CompenyID));
        }

        //Details ID
        public DataTable Details_MaintenanceCompeny(string CompenyID)
        {
            return ExecteRader("Select *  from MaintenanceCompeny Where CompenyID=@CompenyID", CommandType.Text,
            Parameter("@CompenyID", SqlDbType.NVarChar, CompenyID));
        }

        //Details ID
        public DataTable NameSearch_MaintenanceCompeny(String CompenyName)
        {
            return ExecteRader("Select *  from MaintenanceCompeny Where CompenyName=@CompenyName", CommandType.Text,
            Parameter("@CompenyName", SqlDbType.NVarChar, CompenyName));
        }

        //Search 
        public DataTable Search_MaintenanceCompeny(string Search)
        {
            return ExecteRader("Select *  from MaintenanceCompeny Where convert(nvarchar,CompenyID)+CompenyName+convert(nvarchar,Remark) like '%'+@Search+ '%' ", CommandType.Text,
            Parameter("@Search", SqlDbType.NVarChar, Search));
        }


  
}

