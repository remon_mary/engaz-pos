﻿using System;
using System.Data;

class DamagedType_cls : DataAccessLayer
    {
    //MaxID
    public String MaxID_DamagedType()
    {
        return Execute_SQL("select ISNULL (MAX(DamagedTypeID)+1,1) from DamagedType", CommandType.Text);
    }


    // Insert
    public void Insert_DamagedType(string DamagedTypeID, String DamagedTypeName, string Remark)
    {
        Execute_SQL("insert into DamagedType(DamagedTypeID ,DamagedTypeName ,Remark )Values (@DamagedTypeID ,@DamagedTypeName ,@Remark )", CommandType.Text,
        Parameter("@DamagedTypeID", SqlDbType.NVarChar, DamagedTypeID),
        Parameter("@DamagedTypeName", SqlDbType.NVarChar, DamagedTypeName),
        Parameter("@Remark", SqlDbType.NText, Remark));
    }

    //Update
    public void Update_DamagedType(string DamagedTypeID, String DamagedTypeName, string Remark)
    {
        Execute_SQL("Update DamagedType Set DamagedTypeID=@DamagedTypeID ,DamagedTypeName=@DamagedTypeName ,Remark=@Remark  where DamagedTypeID=@DamagedTypeID", CommandType.Text,
        Parameter("@DamagedTypeID", SqlDbType.NVarChar, DamagedTypeID),
        Parameter("@DamagedTypeName", SqlDbType.NVarChar, DamagedTypeName),
        Parameter("@Remark", SqlDbType.NText, Remark));
    }

    //Delete
    public void Delete_DamagedType(string DamagedTypeID)
    {
        Execute_SQL("Delete  From DamagedType where DamagedTypeID=@DamagedTypeID", CommandType.Text,
        Parameter("@DamagedTypeID", SqlDbType.NVarChar, DamagedTypeID));
    }

    //Details ID
    public DataTable Details_DamagedType(string DamagedTypeID)
    {
        return ExecteRader("Select *  from DamagedType Where DamagedTypeID=@DamagedTypeID", CommandType.Text,
        Parameter("@DamagedTypeID", SqlDbType.NVarChar, DamagedTypeID));
    }

    //Details ID
    public DataTable NameSearch__DamagedType(String DamagedTypeName)
    {
        return ExecteRader("Select *  from DamagedType Where DamagedTypeName=@DamagedTypeName", CommandType.Text,
        Parameter("@DamagedTypeName", SqlDbType.NVarChar, DamagedTypeName));
    }

    //Search 
    public DataTable Search__DamagedType(string Search)
    {
        return ExecteRader("Select *  from DamagedType Where convert(nvarchar,DamagedTypeID)+DamagedTypeName like '%'+@Search+ '%' ", CommandType.Text,
        Parameter("@Search", SqlDbType.NVarChar, Search));
    }


}

