﻿using ByStro.DAL;
using ByStro.Forms;
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Linq;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraEditors.Controls;
using ByStro.Clases;
using System.Collections.Generic;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.Utils;
using System.Linq.Expressions;
using System.Collections.ObjectModel;

namespace ByStro.PL
{
    public partial class BillBay_frm : Form
    {
        public BillBay_frm()
        {
            InitializeComponent();
        }
        Bay_cls cls = new Bay_cls();
        TreasuryMovement_cls TreasuryMovement_CLS = new TreasuryMovement_cls();
        Store_Prodecut_cls Store_Prodecut_cls = new Store_Prodecut_cls();
        AVG_cls AVG_cls = new AVG_cls();
        PayType_trans_cls PayType_trans_cls = new PayType_trans_cls();

        Boolean Recived = true;
        string TransID = "0";
        string TreasuryID = "0";
        private void PureItemToStoreForm_Load(object sender, EventArgs e)
        {


            ERB_Setting.SettingToolStrip(toolStrip1);
            ERB_Setting.SettingForm(this);

            FormBorderStyle = FormBorderStyle.Sizable;
            MaximizeBox = true;

            FillCombAccount();
            FillProdecut();
            FillCurrency();
            FillPayType();
            btnNew_Click(null, null);


        }
        DAL.DBDataContext DetailsDataContexst;
        bool IsGridIntialized;
        RepositoryItemGridLookUpEdit repoItems = new RepositoryItemGridLookUpEdit();
        RepositoryItemLookUpEdit repoUOM = new RepositoryItemLookUpEdit();
        RepositoryItemLookUpEdit repoSize = new RepositoryItemLookUpEdit();
        RepositoryItemLookUpEdit repoColor = new RepositoryItemLookUpEdit();
        RepositoryItemDateEdit repoDate = new RepositoryItemDateEdit();
        RepositoryItemLookUpEdit Revenuerepo = new RepositoryItemLookUpEdit();
        RepositoryItemLookUpEdit PaySourceRepo = new RepositoryItemLookUpEdit();
        RepositoryItemLookUpEdit repoUsers = new RepositoryItemLookUpEdit();
        RepositoryItemLookUpEdit repoCurrency = new RepositoryItemLookUpEdit();
        RepositoryItemLookUpEdit repoStore = new RepositoryItemLookUpEdit();
        MasterClass.InvoicesType invoicesType { get => MasterClass.InvoicesType.Purchase ; }
        DAL.Sales_Detail InvoDInsta = new DAL.Sales_Detail();
        void GetData()
        {
            //  if (DetailsDataContexst == null)
            DetailsDataContexst = new DBDataContext(Properties.Settings.Default.Connection_String);

            IQueryable<DAL.Sales_Detail> InvoiceDetails =
                DetailsDataContexst.Sales_Details.Select<DAL.Sales_Detail, DAL.Sales_Detail>((Expression<Func<DAL.Sales_Detail, DAL.Sales_Detail>>)
                (x => x)).Where(x => x.InvoiceID == Convert.ToInt64(txtMainID.Text) && x.InvoiceType == (int)invoicesType);
            GridControl_Items.DataSource = InvoiceDetails;
            //GridView_Items_RowCountChanged(null, null);
            if (IsGridIntialized == false)
            {
                repoStore.NullText = repoItems.NullText = repoUOM.NullText = repoColor.NullText = repoSize.NullText = Revenuerepo.NullText = "";
                repoStore.ValueMember = "ID";
                repoStore.DisplayMember = "Name";
                GridView_Items.Columns[nameof(InvoDInsta.StoreID)].ColumnEdit = repoStore;

                // repoItems.CloseUp += glkp_Customer_CloseUp;
                repoItems.ValidateOnEnterKey = true;
                repoItems.AllowNullInput = DefaultBoolean.False;
                repoItems.BestFitMode = BestFitMode.BestFitResizePopup;
                repoItems.ImmediatePopup = true;
                repoItems.TextEditStyle = TextEditStyles.DisableTextEditor;
                repoItems.ValueMember = "ID";
                repoItems.DisplayMember = "Name";
                repoItems.Buttons.Add(new EditorButton("Add", ButtonPredefines.Plus));
                repoItems.ButtonClick += RepoItems_ButtonClick;
                GridView_Items.Columns[nameof(InvoDInsta.ItemID)].ColumnEdit = repoItems;







                var repositoryItemGridLookUpEdit1View = repoItems.View as GridView;
                repositoryItemGridLookUpEdit1View.Appearance.HeaderPanel.Options.UseTextOptions = true;
                repositoryItemGridLookUpEdit1View.Appearance.HeaderPanel.TextOptions.HAlignment = HorzAlignment.Far;
                repositoryItemGridLookUpEdit1View.Appearance.Row.Options.UseTextOptions = true;
                repositoryItemGridLookUpEdit1View.Appearance.Row.TextOptions.HAlignment = HorzAlignment.Near;
                repositoryItemGridLookUpEdit1View.FocusRectStyle = DrawFocusRectStyle.RowFocus;
                repositoryItemGridLookUpEdit1View.OptionsBehavior.AllowAddRows = DefaultBoolean.False;
                repositoryItemGridLookUpEdit1View.OptionsBehavior.AllowDeleteRows = DefaultBoolean.False;
                repositoryItemGridLookUpEdit1View.OptionsBehavior.AutoSelectAllInEditor = false;
                repositoryItemGridLookUpEdit1View.OptionsBehavior.AutoUpdateTotalSummary = false;
                repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
                repositoryItemGridLookUpEdit1View.OptionsSelection.UseIndicatorForSelection = true;
                repositoryItemGridLookUpEdit1View.OptionsView.BestFitMaxRowCount = 10;
                repositoryItemGridLookUpEdit1View.OptionsView.ShowAutoFilterRow = true;
                repositoryItemGridLookUpEdit1View.OptionsView.ShowDetailButtons = false;
                repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
                repositoryItemGridLookUpEdit1View.OptionsView.ShowIndicator = false;


                GridView_Items.Columns[nameof(InvoDInsta.ItemUnitID)].ColumnEdit = repoUOM;




                RepositoryItemSpinEdit repospin = new RepositoryItemSpinEdit();
                RepositoryItemSpinEdit repospinPrecintage = new RepositoryItemSpinEdit();

                repospinPrecintage.Increment = 0.01M;
                repospinPrecintage.Mask.EditMask = "p";
                repospinPrecintage.Mask.UseMaskAsDisplayFormat = true;
                repospinPrecintage.MaxValue = 1;
                GridView_Items.Columns[nameof(InvoDInsta.Price)].ColumnEdit = repospin;
                GridView_Items.Columns[nameof(InvoDInsta.Discount)].ColumnEdit = repospinPrecintage;
                GridView_Items.Columns[nameof(InvoDInsta.DiscountValue)].ColumnEdit = repospin;

                GridView_Items.Columns[nameof(InvoDInsta.Vat)].ColumnEdit = repospinPrecintage;
                GridView_Items.Columns[nameof(InvoDInsta.VatValue)].ColumnEdit = repospin;

                GridView_Items.Columns[nameof(InvoDInsta.ItemQty)].ColumnEdit = repospin;

                GridView_Items.Columns[nameof(InvoDInsta.Color)].Visible =
                      GridView_Items.Columns[nameof(InvoDInsta.Color)].OptionsColumn.ShowInCustomizationForm =
                GridView_Items.Columns[nameof(InvoDInsta.Size)].Visible =
                      GridView_Items.Columns[nameof(InvoDInsta.Size)].OptionsColumn.ShowInCustomizationForm =
                       GridView_Items.Columns[nameof(InvoDInsta.InvoiceType)].OptionsColumn.ShowInCustomizationForm =
                GridView_Items.Columns[nameof(InvoDInsta.InvoiceType)].Visible =
                  GridView_Items.Columns[nameof(InvoDInsta.CurrencyID)].Visible =
                    GridView_Items.Columns[nameof(InvoDInsta.CurrencyID)].OptionsColumn.ShowInCustomizationForm =
                  GridView_Items.Columns[nameof(InvoDInsta.CurrencyRate)].Visible =
                  GridView_Items.Columns[nameof(InvoDInsta.PurchasePrice)].Visible =
                  GridView_Items.Columns[nameof(InvoDInsta.PurchasePrice)].OptionsColumn.ShowInCustomizationForm =
                    GridView_Items.Columns[nameof(InvoDInsta.CurrencyRate)].OptionsColumn.ShowInCustomizationForm = false;


                GridView_Items.Columns[nameof(InvoDInsta.ExpDate)].ColumnEdit = repoDate;


                GridControl_Items.RepositoryItems.AddRange(new RepositoryItem[]
                {
                    repoStore,repoItems , repoUOM, repospin, /*repoColor,*/  /*repoSize, */repoDate,repospinPrecintage
                });

               
                GridView_Items.Columns["ID"].OptionsColumn.AllowShowHide = GridView_Items.Columns["InvoiceID"].OptionsColumn.AllowShowHide =
                GridView_Items.Columns["ID"].Visible = GridView_Items.Columns["InvoiceID"].Visible = false;

                this.GridView_Items.ColumnPanelRowHeight = 35;
                this.GridView_Items.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
                this.GridView_Items.GridControl = this.GridControl_Items;
                this.GridView_Items.Name = "GridView_Items";
                this.GridView_Items.OptionsBehavior.FocusLeaveOnTab = true;
                this.GridView_Items.OptionsBehavior.KeepFocusedRowOnUpdate = false;
                this.GridView_Items.OptionsMenu.ShowConditionalFormattingItem = true;
                this.GridView_Items.OptionsNavigation.AutoFocusNewRow = true;
                this.GridView_Items.OptionsPrint.ExpandAllDetails = true;
                this.GridView_Items.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top;
                this.GridView_Items.OptionsView.ShowFooter = true;
                this.GridView_Items.OptionsView.ShowGroupPanel = false;

                GridView_Items.CustomRowCellEditForEditing += GridView_Items_CustomRowCellEditForEditing;
                GridView_Items.FocusedRowChanged += GridView_Items_FocusedRowChanged;
                GridView_Items.FocusedColumnChanged += GridView_Items_FocusedColumnChanged;
                GridView_Items.CellValueChanged += GridView_Items_CellValueChanged;
                GridView_Items.InvalidRowException += GridView_Items_InvalidRowException;
                GridView_Items.ValidateRow += GridView_Items_ValidateRow;
                GridView_Items.RowUpdated += GridView_Items_RowUpdated;
                GridView_Items.RowCountChanged += GridView_Items_RowCountChanged;
                GridView_Items.CustomColumnDisplayText += GridView_Items_CustomColumnDisplayText;
                GridView_Items.CustomDrawCell += GridView_Items_CustomDrawCell;
                GridView_Items.CustomUnboundColumnData += GridView_Items_CustomUnboundColumnData;
                GridView_Items.InitNewRow += GridView_Items_InitNewRow;
                GridView_Items.Columns[nameof(InvoDInsta.CurrencyRate)].Caption = "";
                GridView_Items.Columns[nameof(InvoDInsta.CurrencyID)].Caption = "العمله";
                GridView_Items.Columns[nameof(InvoDInsta.Color)].Caption = "اللون";
                GridView_Items.Columns[nameof(InvoDInsta.Size)].Caption = "الحجم";



                GridView_Items.Columns[nameof(InvoDInsta.ItemID)].Caption = "الصنف";
                GridView_Items.Columns[nameof(InvoDInsta.ItemUnitID)].Caption = "الوحده";
                GridView_Items.Columns[nameof(InvoDInsta.ItemQty)].Caption = "الكميه";
                GridView_Items.Columns[nameof(InvoDInsta.Price)].Caption = "السعر";
                GridView_Items.Columns[nameof(InvoDInsta.TotalPrice)].Caption = "الاجمالي";
                GridView_Items.Columns[nameof(InvoDInsta.Discount)].Caption = "ن خصم";
                GridView_Items.Columns[nameof(InvoDInsta.DiscountValue)].Caption = "ق خصم";
                GridView_Items.Columns[nameof(InvoDInsta.Vat)].Caption = "VAT %";
                GridView_Items.Columns[nameof(InvoDInsta.VatValue)].Caption = "VAT";
                GridView_Items.Columns[nameof(InvoDInsta.NetPrice)].Caption = "الصافي";
                GridView_Items.Columns[nameof(InvoDInsta.CurrencyNet)].Caption = "الصافي بسعر العمله";
                GridView_Items.Columns[nameof(InvoDInsta.ExpDate)].Caption = "الصلاحيه";
                GridView_Items.Columns[nameof(InvoDInsta.StoreID)].Caption = "المخزن";
                GridView_Items.Columns[nameof(InvoDInsta.PurchasePrice)].Caption = "سعر الشراء";
                GridView_Items.Columns[nameof(InvoDInsta.TotalPurcasePrice)].Caption = "الاجمالي بسعر الشراء";

                GridView_Items.Columns[nameof(InvoDInsta.TotalPrice)].OptionsColumn.AllowFocus =
                GridView_Items.Columns[nameof(InvoDInsta.NetPrice)].OptionsColumn.AllowFocus =
                GridView_Items.Columns[nameof(InvoDInsta.PurchasePrice)].OptionsColumn.AllowFocus =
                GridView_Items.Columns[nameof(InvoDInsta.TotalPurcasePrice)].OptionsColumn.AllowFocus =
                GridView_Items.Columns[nameof(InvoDInsta.CurrencyNet)].OptionsColumn.AllowFocus = false;


                GridView_Items.Columns.Add(new GridColumn() { Name = "clmIndex", FieldName = "Index", Caption = "م", UnboundType = DevExpress.Data.UnboundColumnType.Integer, MaxWidth = 25 });
                GridView_Items.Columns["Index"].OptionsColumn.AllowFocus = false;
                GridView_Items.Columns.Add(new GridColumn() { Name = "clmCode", FieldName = "Code", Caption = "باركود", UnboundType = DevExpress.Data.UnboundColumnType.String });

                RepositoryItemButtonEdit buttonEdit = new RepositoryItemButtonEdit();
                GridControl_Items.RepositoryItems.Add(buttonEdit);
                buttonEdit.Buttons.Clear();
                buttonEdit.Buttons.Add(new EditorButton(ButtonPredefines.Delete));
                buttonEdit.ButtonClick += ButtonEdit_ButtonClick;
                GridColumn clmnDelete = new GridColumn() { Name = "Delete", Caption = " ", FieldName = "Delete", ColumnEdit = buttonEdit, VisibleIndex = 25, Width = 15 };
                buttonEdit.TextEditStyle = TextEditStyles.HideTextEditor;
                GridView_Items.Columns.Add(clmnDelete);

                GridView_Items.Columns["Index"].VisibleIndex = 0;
                GridView_Items.Columns["Code"].VisibleIndex = 1;
                GridView_Items.Columns[nameof(InvoDInsta.ItemID)].VisibleIndex = 2;
                GridView_Items.Columns[nameof(InvoDInsta.ItemUnitID)].VisibleIndex = 3;
                GridView_Items.Columns[nameof(InvoDInsta.ItemQty)].VisibleIndex = 4;
                GridView_Items.Columns[nameof(InvoDInsta.Price)].VisibleIndex = 5;
                GridView_Items.Columns[nameof(InvoDInsta.TotalPrice)].VisibleIndex = 6;
                GridView_Items.Columns[nameof(InvoDInsta.Discount)].VisibleIndex = 7;
                GridView_Items.Columns[nameof(InvoDInsta.DiscountValue)].VisibleIndex = 8;
                GridView_Items.Columns[nameof(InvoDInsta.Vat)].VisibleIndex = 9;
                GridView_Items.Columns[nameof(InvoDInsta.VatValue)].VisibleIndex = 10;
                GridView_Items.Columns[nameof(InvoDInsta.NetPrice)].VisibleIndex = 11;
                GridView_Items.Columns[nameof(InvoDInsta.CurrencyNet)].VisibleIndex = 12;
                GridView_Items.Columns[nameof(InvoDInsta.ExpDate)].VisibleIndex = 13;
                GridView_Items.Columns[nameof(InvoDInsta.StoreID)].VisibleIndex = 14;

                //GridView_Items.Columns[nameof(InvoDInsta.Vat)].Summary.Clear();
                GridView_Items.Columns[nameof(InvoDInsta.ItemID)].Summary.Add(DevExpress.Data.SummaryItemType.Count, nameof(InvoDInsta.ItemID), "{0}عدد ");
                GridView_Items.Columns[nameof(InvoDInsta.ItemQty)].Summary.Add(DevExpress.Data.SummaryItemType.Count, nameof(InvoDInsta.ItemQty), "{0} قطع");

                GridView_Items.Columns[nameof(InvoDInsta.VatValue)].Summary.Add(DevExpress.Data.SummaryItemType.Sum, nameof(InvoDInsta.VatValue), "{0}");
                GridView_Items.Columns[nameof(InvoDInsta.CurrencyNet)].Summary.Add(DevExpress.Data.SummaryItemType.Sum, nameof(InvoDInsta.CurrencyNet), "{0}");
                GridView_Items.Columns[nameof(InvoDInsta.NetPrice)].Summary.Add(DevExpress.Data.SummaryItemType.Sum, nameof(InvoDInsta.NetPrice), "{0}");
                GridView_Items.Columns[nameof(InvoDInsta.TotalPrice)].Summary.Add(DevExpress.Data.SummaryItemType.Sum, nameof(InvoDInsta.TotalPrice), "{0}");


                IsGridIntialized = true;
            }
            RefreshData();
        }
        private void GridView_Items_InitNewRow(object sender, InitNewRowEventArgs e)
        {
            GridView view = GridView_Items as GridView;
            int index = e.RowHandle;
            DAL.Sales_Detail detail = view.GetRow(index) as DAL.Sales_Detail;
            detail.CurrencyID = combCurrency.SelectedValue.ToInt();
            detail.CurrencyRate = Convert.ToDouble(txtCurrencyRate.Text);
            if (BillSetting_cls.UseVat == true)
            {
                detail.Vat = Convert.ToDouble(BillSetting_cls.Vat) / 100;
            }

        }
        private void ButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            GridView view = ((GridControl)((ButtonEdit)sender).Parent).MainView as GridView;

            if (view.FocusedRowHandle >= 0)
            {
                view.DeleteSelectedRows();
            }

        }
        Dictionary<string, string> CustomUOMNamesForItems = new Dictionary<string, string>();
        List<DAL.Prodecut> prodecuts;
        private void GridView_Items_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.FieldName == nameof(InvoDInsta.ItemUnitID))
            {
                GridView view = GridView_Items as GridView;
                int index = view.GetRowHandle(e.ListSourceRowIndex);
                DAL.Sales_Detail detail = view.GetRow(index) as DAL.Sales_Detail;
                if (detail == null) return;
                var db = new DBDataContext(Properties.Settings.Default.Connection_String);
                if (prodecuts == null)
                    prodecuts = db.Prodecuts.ToList();
                var product = prodecuts.SingleOrDefault(x => x.ProdecutID == detail.ItemID);
                if (prodecuts == null) return;
                if (detail.ItemUnitID == 1)
                    e.DisplayText = product.FiestUnit;
                else if (detail.ItemUnitID == 2)
                    e.DisplayText = product.SecoundUnit;
                else if (detail.ItemUnitID == 3)
                    e.DisplayText = product.ThreeUnit;
            }
        }
        void SaveDetails()
        {
            var db = new DBDataContext(Properties.Settings.Default.Connection_String);

            if (GridView_Items.FocusedRowHandle < 0)
            {
                GridView_Items_ValidateRow(GridView_Items, new ValidateRowEventArgs(GridView_Items.FocusedRowHandle, GridView_Items.GetRow(GridView_Items.FocusedRowHandle)));

            }
            var invoiceDetails = ((Collection<DAL.Sales_Detail>)GridView_Items.DataSource);
            foreach (var item in invoiceDetails)
            {
                item.InvoiceID = Convert.ToInt64(txtMainID.Text);
                item.InvoiceType = (byte)invoicesType;
            }
            DetailsDataContexst.SubmitChanges();
            var invoice = db.Sales_Mains.SingleOrDefault(x => x.MainID == Convert.ToInt64(txtMainID.Text));

            db.Inv_StoreLogs.DeleteAllOnSubmit(db.Inv_StoreLogs.Where(x => x.Type == (int)invoicesType && invoiceDetails.Select(d => d.ID).Contains(x.TypeID ?? 0)));
            db.SubmitChanges();
            foreach (var item in invoiceDetails)
            {
                var product = db.Prodecuts.Single(x => x.ProdecutID == item.ItemID);
                bool IsStockOut;
                double Factor = 1;
                var MainMessage = "";

                switch (item.ItemUnitID)
                {
                    case 1:
                        Factor = Convert.ToDouble(product.FiestUnitFactor);
                        break;
                    case 2:
                        Factor = Convert.ToDouble(product.SecoundUnitFactor);

                        break;
                    case 3:
                        Factor = Convert.ToDouble(product.ThreeUnitFactor);

                        break;
                }
                switch (invoicesType)
                {
                    case MasterClass.InvoicesType.Sales:
                        IsStockOut = true;
                        MainMessage = string.Format("فاتوره مبيعات رقم {0}", invoice.MainID);

                        break;
                    case MasterClass.InvoicesType.Purchase:
                        IsStockOut = false;
                        MainMessage = string.Format("فاتوره مشتريات رقم {0}", invoice.MainID);

                        break;
                    default:
                        throw new NotImplementedException();
                }

                db.Inv_StoreLogs.InsertOnSubmit(new Inv_StoreLog()
                {
                    Type = item.InvoiceType,
                    TypeID = item.ID,
                    Batch = "",
                    BuyPrice = item.NetPrice / item.ItemQty,
                    Color = item.Color,
                    date = invoice.MyDate,
                    ExpDate = item.ExpDate,
                    ItemID = item.ItemID,
                    ItemQuIN = IsStockOut ? 0 : item.ItemQty * Factor,
                    ItemQuOut = IsStockOut ? item.ItemQty * Factor : 0,
                    Note = MainMessage,
                    SellPrice = item.NetPrice / item.ItemQty,
                    Serial = "",
                    Size = item.Size,
                    StoreID = item.StoreID
                });
            }
            db.SubmitChanges();
        }
        void RefreshData()
        {
            var db = new DBDataContext(Properties.Settings.Default.Connection_String);
            repoStore.DataSource = db.Stores.Select(x => new { ID = x.StoreID, Name = x.StoreName }).ToList();
            repoItems.DataSource = db.Prodecuts.Select(x => new {
                ID = x.ProdecutID,
                Name = x.ProdecutName,
                Category = db.Categories.SingleOrDefault(c => c.CategoryID == x.CategoryID).CategoryName ?? ""
            }).ToList();
            repoUOM.DataSource = db.Units.Select(x => new { ID = x.UnitID, Name = x.UnitName }).ToList();
        }
        public static void GridView_Items_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            ColumnView columnView = sender as ColumnView;
            int h = e.RowHandle;
            GridView view = sender as GridView;
            DAL.Sales_Detail detail = view.GetRow(h) as DAL.Sales_Detail;
            if (e.RowHandle < 0) return;
            if (detail.ExpDate.HasValue)
            {
                if (Convert.ToDateTime(view.GetRowCellValue(e.RowHandle, "ExpDate")) <= DateTime.Now)
                    e.Appearance.BackColor = System.Drawing.Color.Red;
                else if (Convert.ToDateTime(view.GetRowCellValue(e.RowHandle, "ExpDate")).AddDays(-15) <= DateTime.Now)
                    e.Appearance.BackColor = System.Drawing.Color.DarkOrange;
                else if (Convert.ToDateTime(view.GetRowCellValue(e.RowHandle, "ExpDate")).AddDays(-30) <= DateTime.Now)
                    e.Appearance.BackColor = System.Drawing.Color.Orange;
                else if (Convert.ToDateTime(view.GetRowCellValue(e.RowHandle, "ExpDate")).AddDays(-45) <= DateTime.Now)
                    e.Appearance.BackColor = System.Drawing.Color.Yellow;
                else if (Convert.ToDateTime(view.GetRowCellValue(e.RowHandle, "ExpDate")).AddDays(-60) <= DateTime.Now)
                    e.Appearance.BackColor = System.Drawing.Color.LightYellow;
            }

        }
        private void GridView_Items_RowUpdated(object sender, RowObjectEventArgs e)
        {
            var items = GridView_Items.DataSource as Collection<DAL.Sales_Detail>;
            if (items == null)
                txtTotalInvoice.Text = txtVatValue.Text = "0";
            else
            {
                txtTotalInvoice.Text = items.Sum(x => x.NetPrice).ToString();
                txtVatValue.Text = items.Sum(x => x.VatValue).ToString();
               
            }

        }
        private void GridView_Items_CustomRowCellEditForEditing(object sender, CustomRowCellEditEventArgs e)
        {
            var dbc = new DBDataContext(Properties.Settings.Default.Connection_String);

            GridView Senderview = sender as GridView;

            if (e.Column.FieldName == "ItemUnitID")
            {
                RepositoryItemGridLookUpEdit UOMRepoEXP = new RepositoryItemGridLookUpEdit();
                GridView view = new GridView();
                UOMRepoEXP.View = view;
                UOMRepoEXP.NullText = "";

                DAL.Sales_Detail detail = Senderview.GetRow(e.RowHandle) as DAL.Sales_Detail;

                if (detail.ItemID.ValidAsIntNonZero() == false)
                { e.RepositoryItem = new RepositoryItem(); return; }
                var item = dbc.Prodecuts.Where(x => x.ProdecutID == Convert.ToInt32(Senderview.GetRowCellValue(e.RowHandle, "ItemID"))).Single();
                var datasource = new[] { new { Name = item.FiestUnit, ID = 1 }, new { Name = item.SecoundUnit, ID = 2 }, new { Name = item.ThreeUnit, ID = 3 } };


                var itemunit = datasource.Where(x => string.IsNullOrEmpty(x.Name) == false).ToList();

                UOMRepoEXP.DataSource = itemunit;
                UOMRepoEXP.DisplayMember = "Name";
                UOMRepoEXP.ValueMember = "ID";
                UOMRepoEXP.View.PopulateColumns(itemunit.ToList());
                view.Columns["ID"].Visible = false;
                e.RepositoryItem = UOMRepoEXP;
            }


        }
        MasterClass.ItemLog SharedLog;
        private void GridView_Items_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            if (e.RowHandle > GridView_Items.RowCount - 1) return;
            GridControl gridControl = sender as GridControl;
            GridView View = GridView_Items as GridView;
            var dbc = new DBDataContext(Properties.Settings.Default.Connection_String);

            int index = e.RowHandle;
            DAL.Sales_Detail detail = View.GetRow(index) as DAL.Sales_Detail;
            if (detail == null)
                return;
            var item = dbc.Prodecuts.SingleOrDefault(x => x.ProdecutID == detail.ItemID);
            if (e.Column == null) return;
            switch (e.Column.FieldName)
            {
                case "Code":
                    if (e.Value == null || e.Value == DBNull.Value || e.Value.ToString().Trim() == string.Empty) View.DeleteRow(index);
                    if (CurrentSession.SystemSettings.UseScalBarcode && e.Value.ToString().Length == CurrentSession.SystemSettings.WeightCodeLength + CurrentSession.SystemSettings.ItemCodeLength)
                    {
                        var itemCode = e.Value.ToString().Substring(0, CurrentSession.SystemSettings.ItemCodeLength);
                        var weightText = e.Value.ToString().Substring(CurrentSession.SystemSettings.ItemCodeLength, CurrentSession.SystemSettings.WeightCodeLength);

                        double weight = 0;
                        double.TryParse(weightText, out weight);
                        SharedLog = MasterClass.SearchForItem(itemCode, (string.IsNullOrEmpty(txtStoreID.Text)) ? 0 : Convert.ToInt32(txtStoreID.Text));
                        if (weight > 0)
                        {
                            weight = weight / (double)CurrentSession.SystemSettings.DivideWeightBy;
                            detail.ItemQty = weight;
                        }
                    }
                    else
                    {
                        SharedLog = MasterClass.SearchForItem(e.Value.ToString(), (string.IsNullOrEmpty(txtStoreID.Text)) ? 0 : Convert.ToInt32(txtStoreID.Text));
                    }
                    if (SharedLog.ItemID == 0)
                    {
                        View.DeleteRow(index);
                        break;
                    }
                    detail.ItemID = SharedLog.ItemID;
                    if (CodesDictionary.ContainsKey(index))
                        CodesDictionary.Remove(index);
                    GridView_Items_CellValueChanged(sender, new CellValueChangedEventArgs(index, View.Columns[nameof(InvoDInsta.ItemID)], detail.ItemID));

                    break;
                case nameof(InvoDInsta.ItemID):
                    if (detail.ItemID == 0)   //e.Value == null || e.Value == DBNull.Value || e.Value.ToString().Trim() == string.Empty)
                    {
                        View.DeleteRow(index);
                        break;
                    }
                    if (item == null) return;
                    if (SharedLog.ItemID == 0)
                        SharedLog = MasterClass.GetNextItemForSell(detail.ItemID, Convert.ToInt32(txtStoreID.Text));

                    detail.ItemUnitID = SharedLog.UnitID;

                    if (detail.ItemUnitID.ValidAsIntNonZero() == false)
                        detail.ItemUnitID = item.UnitDefoult ?? 1;

                    detail.StoreID = SharedLog.StoreID == 0 ? Convert.ToInt32(txtStoreID.Text) : SharedLog.StoreID;
                    if (detail.ItemQty <= 0)
                        detail.ItemQty = 1;
                    detail.Color = SharedLog.Color;
                    detail.ExpDate = SharedLog.ExpirDate;
                    detail.Size = SharedLog.Size;
                    GridView_Items_CellValueChanged(sender, new CellValueChangedEventArgs(index, View.Columns[nameof(InvoDInsta.ItemUnitID)], detail.ItemUnitID));
                    GridView_Items_CellValueChanged(sender, new CellValueChangedEventArgs(index, View.Columns[nameof(InvoDInsta.ItemQty)], detail.ItemQty));
                    break;
                case nameof(InvoDInsta.ItemUnitID):
                    if (detail.ItemUnitID <= 0) break;
                    if (item == null) return;

                    if (detail.ItemUnitID == 1)
                    { 
                        detail.Price = (item.ProdecutBayPrice ?? 0 / Convert.ToDouble(item.FiestUnitFactor)) * Convert.ToDouble(item.FiestUnitFactor);
                    }
                    else if (detail.ItemUnitID == 2)
                    {
                        detail.Price = (item.ProdecutBayPrice ?? 0 / Convert.ToDouble(item.FiestUnitFactor)) * Convert.ToDouble(item.SecoundUnitFactor);
                         
                    }
                    else if (detail.ItemUnitID == 3)
                    {
                        detail.Price = (item.ProdecutBayPrice ?? 0 / Convert.ToDouble(item.FiestUnitFactor)) * Convert.ToDouble(item.ThreeUnitFactor);
                         
                    }
                    GridView_Items_CellValueChanged(sender, new CellValueChangedEventArgs(index, View.Columns[nameof(InvoDInsta.Price)], detail.Price));
                    GridView_Items_CellValueChanged(sender, new CellValueChangedEventArgs(index, View.Columns[nameof(InvoDInsta.ItemQty)], detail.ItemQty));
                    break;
                case nameof(InvoDInsta.ItemQty):
                    if (detail.ItemID == 0) break;
                    GridView_Items_CellValueChanged(sender, new CellValueChangedEventArgs(index, View.Columns[nameof(InvoDInsta.Price)], View.GetRowCellValue(index, nameof(InvoDInsta.Price))));
                    break;
                case nameof(InvoDInsta.Price):
                case nameof(InvoDInsta.Discount):
                case nameof(InvoDInsta.Vat):
                    // if (View.FocusedColumn.FieldName == "DiscountVal") break;
                    detail.DiscountValue = detail.Discount * (detail.ItemQty * detail.Price);
                    detail.VatValue = detail.Vat * (detail.ItemQty * detail.Price);

                    GridView_Items_CellValueChanged(sender, new CellValueChangedEventArgs(index, View.Columns[nameof(InvoDInsta.DiscountValue)], detail.DiscountValue));
                    break;
                case nameof(InvoDInsta.DiscountValue):
                case nameof(InvoDInsta.VatValue):
                    detail.TotalPrice = (detail.ItemQty * detail.Price);

                    detail.TotalPurcasePrice = (detail.ItemQty * detail.PurchasePrice);
                    if (View.FocusedColumn.FieldName == nameof(InvoDInsta.DiscountValue))
                    {
                        detail.Discount = detail.DiscountValue / detail.TotalPrice;
                    }
                    if (View.FocusedColumn.FieldName == nameof(InvoDInsta.VatValue))
                    {
                        detail.Vat = detail.VatValue / detail.TotalPrice;
                    }
                    detail.NetPrice = detail.TotalPrice + detail.VatValue - detail.DiscountValue;
                    detail.CurrencyNet = detail.NetPrice * detail.CurrencyRate;

                    break;
            }
            SharedLog.ItemID = 0;

        }
        Dictionary<int, string> CodesDictionary = new Dictionary<int, string>();
        private void GridView_Items_CustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
        {

            if (e.Column.FieldName == "Index")
            {
                if (e.IsGetData)
                    e.Value = e.ListSourceRowIndex + 1;
            }
            else if (e.Column.FieldName == "Code")
            {
                if (e.IsGetData)
                {

                    var code = "";
                    CodesDictionary.TryGetValue(GridView_Items.GetRowHandle(e.ListSourceRowIndex), out code);
                    e.Value = code;

                }
                else
                {
                    CodesDictionary.Remove(GridView_Items.GetRowHandle(e.ListSourceRowIndex));
                    if (e.Value != null)
                        CodesDictionary.Add(GridView_Items.GetRowHandle(e.ListSourceRowIndex), e.Value.ToString());
                }
            }


        }
        private void GridView_Items_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            GridView view = sender as GridView;
            if (view == null || view.Columns.Count == 0) return;
            DAL.Sales_Detail detail = view.GetRow(e.FocusedRowHandle) as DAL.Sales_Detail;
            //if (detail == null)
            //    return;
            //Inv_Item item = CurrentSession.Products.Where(x => x.ID == detail.ItemID).SingleOrDefault();
            //if (item == null) return;

            //int h = e.FocusedRowHandle;
            //view.Columns[nameof(InvoDInsta.Size)].OptionsColumn.AllowEdit = item.Size;
            //view.Columns[nameof(InvoDInsta.ExpDate)].OptionsColumn.AllowEdit = item.Expier;
            //view.Columns[nameof(InvoDInsta.Color)].OptionsColumn.AllowEdit = item.Color;
            //view.Columns[nameof(InvoDInsta.Serial)].OptionsColumn.AllowEdit = item.Serial;
            //view.Columns[nameof(InvoDInsta.ItemQty)].OptionsColumn.AllowEdit = !item.Serial;
            //if (!view.Columns[nameof(InvoDInsta.ItemQty)].OptionsColumn.AllowEdit)
            //    view.SetRowCellValue(h, nameof(InvoDInsta.ItemQty), 1);


        }


        private void GridView_Items_FocusedColumnChanged(object sender, FocusedColumnChangedEventArgs e)
        {
            GridView_Items_FocusedRowChanged(sender, new FocusedRowChangedEventArgs(0, GridView_Items.FocusedRowHandle));

        }
        private void GridView_Items_ValidateRow(object sender, ValidateRowEventArgs e)
        {
            GridView view = sender as GridView;
            ColumnView columnView = sender as ColumnView;
            int index = e.RowHandle;
            DAL.Sales_Detail detail = e.Row as DAL.Sales_Detail;
            if (detail == null || detail.ItemID <= 0)
            {
                e.Valid = false;
                return;
            }
            if (detail.ItemID.ValidAsIntNonZero() == false)
            {
                e.Valid = false;
                view.SetColumnError(view.Columns[nameof(detail.ItemID)], "يجب اختيار الصنف ");
            }
            // if (columnView.get)
            if (detail.ItemQty <= 0)
            {
                e.Valid = false;
                view.SetColumnError(view.Columns[nameof(detail.ItemQty)], "يجب ان تكون الكميه اكبر من صفر ");
            }
            //if (detail.PurchasePrice > detail.Price)
            //{
            //    if (MessageBox.Show("سعر البيـع اقل من سعر الشـراء بمقدار :  " + (detail.Price - detail.PurchasePrice).ToString() + Environment.NewLine + "هل تريد الاستمــرار ؟؟ ", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Error, MessageBoxDefaultButton.Button2) == DialogResult.No)
            //    {
            //        e.Valid = false;
            //        view.SetColumnError(view.Columns[nameof(detail.Price)], "سعر البيع غير مقبول ");

            //    }

            //}
            //else if (detail.PurchasePrice == detail.Price)
            //{
            //    if (MessageBox.Show("سعر البيع يساوي سعر الشراء : هل تريد البيع بسعر التكلفة", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Error, MessageBoxDefaultButton.Button2) == DialogResult.No)
            //    {
            //        e.Valid = false;
            //        view.SetColumnError(view.Columns[nameof(detail.Price)], "سعر البيع غير مقبول ");

            //    }
            //}

            //if (detail.TotalPrice < detail.TotalCostValue)
            //{
            //    if (CurrentSession.user.SellLowerPriceThanBuy == 1)
            //    {
            //        e.Valid = false;
            //        view.SetColumnError(view.Columns[nameof(detail.Price)], LangResource.ErrorCantSellLowerPriceThanBuy);

            //    }
            //    else if (CurrentSession.user.SellLowerPriceThanBuy == 0)
            //    {

            //        view.SetColumnError(view.Columns[nameof(detail.Price)], LangResource.ItemPriceIsLowerThanCostPrice + "-" + LangResource.PressESCToDismess, DevExpress.XtraEditors.DXErrorProvider.ErrorType.Warning);
            //        //var dialogResult  = XtraMessageBox.Show(text: LangResource.DoYouWantToSellWithPriceLowerThanCost, caption: LangResource.Warning, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            //        //if(dialogResult == DialogResult.No)
            //        //{
            //        //    e.Valid = false;
            //        //    view.SetColumnError(view.Columns[nameof(detail.Price)], LangResource.ErrorCantSellLowerPriceThanBuy, DevExpress.XtraEditors.DXErrorProvider.ErrorType.Warning );
            //        //}
            //    }
            //}


            //if (item.Color && detail.Color.ValidAsIntNonZero() == false)
            //{
            //    e.Valid = false;
            //    view.SetColumnError(view.Columns[nameof(detail.Color)], LangResource.ErrorCantBeEmpry);
            //    view.Columns[nameof(detail.Color)].Visible = true;

            //}
            //if (item.Size && detail.Size.ValidAsIntNonZero() == false)
            //{
            //    e.Valid = false;
            //    view.SetColumnError(view.Columns[nameof(detail.Size)], LangResource.ErrorCantBeEmpry);
            //    view.Columns[nameof(detail.Size)].Visible = true;

            //}
            //if (item.Expier && detail.ExpDate.HasValue == false)
            //{
            //    view.Columns[nameof(detail.ExpDate)].Visible = true;
            //    e.Valid = false;
            //    view.SetColumnError(view.Columns[nameof(detail.ExpDate)], LangResource.ErrorCantBeEmpry);
            //}

            //if (detail.ExpDate.HasValue)
            //{
            //    int relationship = DateTime.Compare(detail.ExpDate.Value, DateTime.Now.Date);
            //    if (relationship <= 0)
            //    {
            //        //if (CurrentSession.user.WhenSelllingItemHasExpired == 1)
            //        //{
            //        //    if (XtraMessageBox.Show(LangResource.WarningItemHasExpired, "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            //        //    {
            //        //        view.DeleteRow(e.RowHandle);
            //        //    }
            //        //}
            //        //if (CurrentSession.user.WhenSelllingItemHasExpired == 2)
            //        //{
            //            view.Columns[nameof(detail.ExpDate)].Visible = true;
            //            e.Valid = false;
            //          //  view.SetColumnError(view.Columns[nameof(detail.ExpDate)], LangResource.ErrorCantBeEmpry);
            //        //} 
            //    }
            //}

            //if (detail.ItemQty > 0 && IsNew && Master.IsItemReachedReorderLevel(item.ID, detail.StoreID.ToInt()))
            //{
            //    if (CurrentSession.user.WhenSellingQtyLessThanReorder == 1)
            //    {
            //        if (XtraMessageBox.Show(LangResource.WarningItemTakeRechedReorderLevel, "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            //        {
            //            view.DeleteRow(e.RowHandle);
            //        }
            //    }
            //    if (CurrentSession.user.WhenSellingQtyLessThanReorder == 2)
            //    {
            //        XtraMessageBox.Show(LangResource.ErorrCantTakeSellItemReachedReorderLevel, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //        view.DeleteRow(e.RowHandle);
            //    }


            //}



        }
        private void GridView_Items_InvalidRowException(object sender, InvalidRowExceptionEventArgs e)
        {
            if ((e.Row as DAL.Sales_Detail).ItemID == 0)
                e.ExceptionMode = ExceptionMode.Ignore;
            else
                e.ExceptionMode = ExceptionMode.NoAction;

        }

        private void GridView_Items_RowCountChanged(object sender, EventArgs e)
        {

            GridView_Items_RowUpdated(sender, new RowObjectEventArgs(0, "Index"));

        }
        private void RepoItems_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.GetType() != typeof(EditorButton) || e.Button.Tag == null)
                return;

            string btnName = e.Button.Tag.ToString();
            if (btnName == "Add")
            {
                // frm_Main.OpenForm(new frm_Item());
                RefreshData();
            }
        }
        private void gridControl1_ProcessGridKey(object sender, KeyEventArgs e)
        {
            try
            {

                GridControl gridControl = sender as GridControl;
                GridView focusedView = gridControl.FocusedView as GridView;
                if (e.KeyCode == Keys.Return)
                {
                    GridColumn focusedColumn = (gridControl.FocusedView as ColumnView).FocusedColumn;
                    int focusedRowHandle = (gridControl.FocusedView as ColumnView).FocusedRowHandle;

                    var FocusedColumnName = "";
                    if (focusedView.FocusedColumn == focusedView.Columns["Code"] || focusedView.FocusedColumn == focusedView.Columns["ItemID"])
                    {
                        FocusedColumnName = focusedView.FocusedColumn.FieldName;
                        gridControl1_ProcessGridKey(sender, new KeyEventArgs(Keys.Tab));
                    }
                    if (focusedView.FocusedRowHandle < 0)
                    {
                        focusedView.AddNewRow();
                        if (FocusedColumnName != "")
                            focusedView.FocusedColumn = focusedView.Columns[FocusedColumnName];
                        else
                            focusedView.FocusedColumn = focusedView.Columns["Code"];
                    }
                    else
                    {
                        focusedView.FocusedRowHandle = focusedRowHandle + 1;
                        if (FocusedColumnName != "")
                            focusedView.FocusedColumn = focusedView.Columns[FocusedColumnName];
                        else
                            focusedView.FocusedColumn = focusedView.Columns["Code"];
                    }
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.Tab && e.Modifiers != Keys.Shift)
                {
                    if (focusedView.FocusedColumn.VisibleIndex == 0)
                        focusedView.FocusedColumn = focusedView.VisibleColumns[focusedView.VisibleColumns.Count - 1];
                    else
                        focusedView.FocusedColumn = focusedView.VisibleColumns[focusedView.FocusedColumn.VisibleIndex - 1];
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.Delete && e.Modifiers == Keys.Control)
                    focusedView.DeleteSelectedRows();
                //else if (e.KeyCode == Keys.Tab && e.Modifiers == Keys.Shift)
                //{
                //    if (focusedView.FocusedColumn.VisibleIndex == focusedView.VisibleColumns.Count)
                //        focusedView.FocusedColumn = focusedView.VisibleColumns[0];
                //    else
                //        focusedView.FocusedColumn = focusedView.VisibleColumns[focusedView.FocusedColumn.VisibleIndex + 1];
                //    e.Handled = true;
                //}
                //else
                //{
                //    //if (e.KeyCode != Keys.Up || focusedView.GetFocusedRow() == null || !(focusedView.GetFocusedRow() as DataRowView).IsNew || focusedView.GetFocusedRowCellValue("ItemId") != null && !(focusedView.GetFocusedRowCellValue("ItemId").ToString() == string.Empty))
                //    //    return;
                //    //focusedView.DeleteRow(focusedView.FocusedRowHandle);
                //}
            }
            catch
            {
            }
        }
        public void FillCurrency()
        {
            string cusName = combCurrency.Text;
            Currency_cls Currency_cls = new Currency_cls();
            combCurrency.DataSource = Currency_cls.Search__Currency("");
            combCurrency.DisplayMember = "CurrencyName";
            combCurrency.ValueMember = "CurrencyID";
            combCurrency.Text = cusName;

        }

        public void FillPayType()
        {
            //string cusName = combPayType.Text;
            PayType_cls PayType_cls = new PayType_cls();
            combPayType.DataSource = PayType_cls.Search__PayType("");
            combPayType.DisplayMember = "PayTypeName";
            combPayType.ValueMember = "PayTypeID";
            //combPayType.Text = cusName;

        }

        public void FillProdecut()
        {
            RefreshData();

        }
        // fill comboxe All Suppliers
        public void FillCombAccount()
        {
            Suppliers_cls Suppliers_cls = new Suppliers_cls();
            string supName = combAccount.Text;
            string supID = txtAccountID.Text;
            combAccount.DataSource = Suppliers_cls.Search_Suppliers("");
            combAccount.DisplayMember = "SupplierName";
            combAccount.ValueMember = "SupplierID";
            combAccount.Text = supName;
            txtAccountID.Text = supID;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                StoreSearch_frm frm = new StoreSearch_frm();
                frm.ShowDialog();
                if (frm.loaddata == true)
                {
                    txtStoreName.BackColor = Color.WhiteSmoke;
                    txtStoreID.Text = frm.DGV1.CurrentRow.Cells["StoreID"].Value.ToString();
                    txtStoreName.Text = frm.DGV1.CurrentRow.Cells["StoreName"].Value.ToString();
                 
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        Prodecut_cls Prodecut_CLS = new Prodecut_cls();
        DataTable dt_Prodecut = new DataTable();
       
         
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                if (GridView_Items.RowCount <= 0)
                {
                    MessageBox.Show("لا يمكن حفظ فاتورة خالية من الاصناف", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else if (combAccount.Text.Trim() == "" || txtAccountID.Text.Trim() == "")
                {
                    combAccount.BackColor = Color.Pink;
                    combAccount.Focus();
                    return;
                }
                else if (txtInvoiceNumber.Text.Trim() == "")
                {
                    txtInvoiceNumber.BackColor = Color.Pink;
                    txtInvoiceNumber.Focus();
                    return;
                }





               

                if (combPayKind.SelectedIndex == 0)
                {
                    if (DGVPayType.Rows.Count == 0)
                    {
                        AddDgv_PayType(DGVPayType, combPayType.SelectedValue.ToString(), combPayType.Text, (double.Parse(txtNetInvoice.Text) * double.Parse(txtCurrencyRate.Text)).ToString(), txtStatement.Text, combCurrency.SelectedValue.ToString(), combCurrency.Text, txtCurrencyRate.Text, txtNetInvoice.Text);
                        ClearPayType();
                        SumPayType();
                    }
                    if (double.Parse(txtpaid_Invoice.Text) < double.Parse(txtNetInvoice.Text))
                    {
                        MessageBox.Show(string.Format("{0}\n{1}\n", "المبلغ المدفوع اقل من صافي الفاتورة ", "في حالة نوع الدفع كاش لا يمكنك دفع اقل من صافي الفاتورة"), "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                }



                txtMainID.Text = cls.MaxID_Bay_Main();
                //===Suppliers_trans============================================================================================================================================================================================================================================================
                //TransID = Sup_Trans_cls.MaxID_Suppliers_transID();
                //Sup_Trans_cls.InsertSuppliers_trans(TransID, txtMainID.Text, txtBillTypeID.Text, this.Text, D1.Value, this.Text + " : " + combPayKind.Text, txtpaid_Invoice.Text, txtNetInvoice.Text, txtAccountID.Text, Properties.Settings.Default.UserID);
                //===Treasury Movement_============================================================================================================================================================================================================================================================

                string paidInvoice = txtNetInvoice.Text;
                if (combPayKind.SelectedIndex == 1)
                {
                    paidInvoice = txtpaid_Invoice.Text;
                }

                TreasuryID = TreasuryMovement_CLS.MaxID_TreasuryMovement();
                TreasuryMovement_CLS.InsertTreasuryMovement(TreasuryID, txtAccountID.Text, "Supp", txtMainID.Text, txtBillTypeID.Text, this.Text, D1.Value, this.Text + " : " + combAccount.Text, "0", txtpaid_Invoice.Text, this.Text + " : " + combPayKind.Text, paidInvoice, txtNetInvoice.Text, Properties.Settings.Default.UserID);

                //====Bay_Main===========================================================================================================================================================================================================================================================



                cls.InsertBay_Main(txtMainID.Text, D1.Value, txtInvoiceNumber.Text, txtAccountID.Text, txtRemarks.Text, combPayKind.Text, Convert.ToDouble(txtTotalInvoice.Text), Convert.ToDouble(txtTotalDescound.Text), Convert.ToDouble(txtNetInvoice.Text), Convert.ToDouble(paidInvoice), Convert.ToDouble(txtRestInvoise.Text), "0", TransID, TreasuryID, double.Parse(txtVatValue.Text),txtExpenses.Text, Properties.Settings.Default.UserID);
                //====Bay_Main===========================================================================================================================================================================================================================================================
               

                #region // Pay
                for (int i = 0; i < DGVPayType.Rows.Count; i++)
                {
                    string Statement = "مورد" + " / " + combAccount.Text;
                    if (!string.IsNullOrEmpty(DGVPayType.Rows[i].Cells["Statement"].Value.ToString()))
                    {
                        Statement = DGVPayType.Rows[i].Cells["Statement"].Value.ToString();
                    }
                    PayType_trans_cls.Insert_PayType_trans(txtMainID.Text, D1.Value, DGVPayType.Rows[i].Cells["PayTypeID"].Value.ToString(), txtMainID.Text, this.Text, txtBillTypeID.Text, 0, double.Parse(DGVPayType.Rows[i].Cells["PayValue"].Value.ToString()), Statement, Properties.Settings.Default.UserID, DGVPayType.Rows[i].Cells["CurrencyID2"].Value.ToString(), DGVPayType.Rows[i].Cells["CurrencyRate2"].Value.ToString(), DGVPayType.Rows[i].Cells["CurrencyPrice2"].Value.ToString());

                }
                #endregion 
              
                SaveDetails();

                if (BillSetting_cls.ShowMessageSave == false)
                {
                    Mass.Saved();
                }
                btnNew_Click(null, null);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {

            if (DataAccessLayer.CS_16 == false)
            {
                MessageBox.Show("ليس لديك صلاحية", "صلاحيات ", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            if (GridView_Items.RowCount <= 0)
            {
                MessageBox.Show("لا يمكن حفظ فاتورة خالية من الاصناف", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (combAccount.Text.Trim() == "" || txtAccountID.Text.Trim() == "")
            {
                combAccount.BackColor = Color.Pink;
                combAccount.Focus();
                return;
            }
            if (txtInvoiceNumber.Text.Trim() == "")
            {
                txtInvoiceNumber.BackColor = Color.Pink;
                txtInvoiceNumber.Focus();
                return;
            }





            if (combPayKind.SelectedIndex == 0)
            {
                if (double.Parse(txtpaid_Invoice.Text) < double.Parse(txtNetInvoice.Text))
                {
                    MessageBox.Show(string.Format("{0}\n{1}\n", "المبلغ المدفوع اقل من صافي الفاتورة ", "في حالة نوع الدفع كاش لا يمكنك دفع اقل من صافي الفاتورة"), "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

            }

            //===Treasury Movement_============================================================================================================================================================================================================================================================
            double paidInvoice = Convert.ToDouble(txtNetInvoice.Text);
            if (combPayKind.SelectedIndex == 1)
            {
                paidInvoice = Convert.ToDouble(txtpaid_Invoice.Text);
            }

            TreasuryMovement_CLS.UpdateTreasuryMovement(TreasuryID, txtAccountID.Text, "Supp", txtMainID.Text, txtBillTypeID.Text, this.Text, D1.Value, this.Text + " : " + combAccount.Text, "0", txtpaid_Invoice.Text, txtRemarks.Text, paidInvoice.ToString(), txtNetInvoice.Text);


            //====UpdateBay_Main========================================================================================================================================================================================================================================================

            cls.UpdateBay_Main(txtMainID.Text, D1.Value, txtInvoiceNumber.Text, txtAccountID.Text, txtRemarks.Text, combPayKind.Text, Convert.ToDouble(txtTotalInvoice.Text), Convert.ToDouble(txtTotalDescound.Text), Convert.ToDouble(txtNetInvoice.Text), paidInvoice, Convert.ToDouble(txtRestInvoise.Text), TransID, TreasuryID, /*double.Parse(txtMainVat.Text)*/ 0 , double.Parse(txtVatValue.Text), txtExpenses.Text);
            //====Update Suppliers_trans========================================================================================================================================================================================================================================================
            //====Delete Quentity ========================================================================================================================================================================================================================================================

           
            PayType_trans_cls.Delete_PayType_trans(txtMainID.Text, txtBillTypeID.Text);
            for (int i = 0; i < DGVPayType.Rows.Count; i++)
            {
                string Statement = "مورد" + " / " + combAccount.Text;
                if (!string.IsNullOrEmpty(DGVPayType.Rows[i].Cells["Statement"].Value.ToString()))
                {
                    Statement = DGVPayType.Rows[i].Cells["Statement"].Value.ToString();
                }
                PayType_trans_cls.Insert_PayType_trans(txtMainID.Text, D1.Value, DGVPayType.Rows[i].Cells["PayTypeID"].Value.ToString(), txtMainID.Text, this.Text, txtBillTypeID.Text, 0, double.Parse(DGVPayType.Rows[i].Cells["PayValue"].Value.ToString()), Statement, Properties.Settings.Default.UserID, DGVPayType.Rows[i].Cells["CurrencyID2"].Value.ToString(), DGVPayType.Rows[i].Cells["CurrencyRate2"].Value.ToString(), DGVPayType.Rows[i].Cells["CurrencyPrice2"].Value.ToString());
            }
             
            DataTable dt = cls.Details_Bay_Details(txtMainID.Text);


            var items = GridView_Items.DataSource as Collection<DAL.Sales_Detail>;
            items.ToList().ForEach(i =>
            {
                AVG_cls.QuantityNow_Avg(i.ItemID.ToString(), i.StoreID.ToString(), false);

            });


            SaveDetails();
            //====clear ========================================================================================================================================================================================================================================================
            if (BillSetting_cls.ShowMessageSave == false)
            {
                Mass.Update();
            }



        }
        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                BillBay_Search_frm frm = new BillBay_Search_frm();
                frm.Icon = this.Icon;
                frm.ShowDialog(this);
                if (frm.LoadData == false)
                {
                    return;
                }

                btnSave.Enabled = false;
                btnUpdate.Enabled = true;
                btnDelete.Enabled = true;
                DataTable dt = cls.Details_Bay_Main(frm.DGV2.CurrentRow.Cells["MainID"].Value.ToString());
                DataRow Dr = dt.Rows[0];
                txtMainID.Text = Dr["MainID"].ToString();
                D1.Value = Convert.ToDateTime(Dr["MyDate"]);
                txtInvoiceNumber.Text = Dr["InvoiceNumber"].ToString();
                txtAccountID.Text = Dr["SupplierID"].ToString();
                // Balense==========================================================================================
                BalenceSuppliers(txtAccountID.Text);
                combAccount.Text = Dr["SupplierName"].ToString();
                txtRemarks.Text = Dr["Remarks"].ToString();
                combPayKind.Text = Dr["TypeKind"].ToString();
                txtTotalInvoice.Text = Dr["TotalInvoice"].ToString(); 
                txtTotalDescound.Text = Dr["Discount"].ToString();
                txtNetInvoice.Text = Dr["NetInvoice"].ToString();
                txtpaid_Invoice.Text = Dr["PaidInvoice"].ToString();
                txtRestInvoise.Text = Dr["RestInvoise"].ToString();
                TransID = Dr["TransID"].ToString();
                TreasuryID = Dr["TreasuryID"].ToString();
                txtExpenses .Text = Dr["Expenses"].ToString();
                //=========================================================================================================================================================================
              


                // PyeType
                DGVPayType.DataSource = null;
                DGVPayType.Rows.Clear();
                DataTable dt_paytype_trans = PayType_trans_cls.Details_PayType_trans(frm.DGV2.CurrentRow.Cells["MainID"].Value.ToString(), txtBillTypeID.Text);
                for (int i = 0; i < dt_paytype_trans.Rows.Count; i++)
                {
                    AddDgv_PayType(DGVPayType, dt_paytype_trans.Rows[i]["PayTypeID"].ToString(), dt_paytype_trans.Rows[i]["PayTypeName"].ToString(), dt_paytype_trans.Rows[i]["Credit"].ToString(), dt_paytype_trans.Rows[i]["Statement"].ToString(), dt_paytype_trans.Rows[i]["CurrencyID"].ToString(), dt_paytype_trans.Rows[i]["CurrencyName"].ToString(), dt_paytype_trans.Rows[i]["CurrencyRate"].ToString(), dt_paytype_trans.Rows[i]["CurrencyPrice"].ToString());
                }
                SumPayType();








            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        private void AllKeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }
        private void OrderProudect_frm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //if (DGV1.Rows.Count > 0)
            //{
            //    if (Mass.close_frm() == false)
            //    {
            //        e.Cancel = true;
            //    }
            //}
        }
        private void button3_Click(object sender, EventArgs e)
        {
            FillCombAccount();
        }
        private void BalenceSuppliers(string SuppliersID)
        {
            txtCustomersBlanse.Text = "0";
            if (SuppliersID.Trim() != "" && SuppliersID != "System.Data.DataRowView")
            {
                DataTable dt = TreasuryMovement_CLS.Balence_ISCusSuppBay(SuppliersID, "Supp");// Sup_Trans_cls.Sup_Balence(SuppliersID);
                txtCustomersBlanse.Text = "0";
                if (dt.Rows[0]["balence"] == DBNull.Value == false)
                {
                    txtCustomersBlanse.Text = dt.Rows[0]["balence"].ToString();
                }
            }

        }
        private void toolStripButton3_Click(object sender, EventArgs e)
        {
           
        }

        private void txtChange(object sender, EventArgs e)
        {
            try
            {

                Double total = Convert.ToDouble(txtTotalInvoice.Text);
                //  txtVatValue.Text = (total / 100).ToString();


                txtNetInvoice.Text = (Convert.ToDouble(txtTotalInvoice.Text) + Convert.ToDouble(txtExpenses.Text) - Convert.ToDouble(txtTotalDescound.Text)).ToString();


                //if (combPayKind.Text == "نقدي")
                //{
                //    txtpaid_Invoice.Text = txtNetInvoice.Text;

                //}

                txtRestInvoise.Text = (Convert.ToDouble(txtNetInvoice.Text) + Convert.ToDouble(txtExpenses.Text) - Convert.ToDouble(txtpaid_Invoice.Text)).ToString();
                //if (btnSave.Enabled)
                //{
                //    if (!string.IsNullOrEmpty(txtCurrencyRate.Text.Trim()))
                //    {
                //        txtPayValue.Text = (double.Parse(txtNetInvoice.Text) / double.Parse(txtCurrencyRate.Text)).ToString();

                //    }
                //    else
                //    {
                //        txtPayValue.Text = txtNetInvoice.Text;
                //    }
                //}
            }
            catch
            {
                return;
            }

        }

        private void txtpaid_Invoice_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtRestInvoise.Text = (Convert.ToDouble(txtNetInvoice.Text)  - Convert.ToDouble(txtpaid_Invoice.Text)).ToString();
            }
            catch
            {
                return;
            }
        }

        private void combPayKind_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (combPayKind.SelectedIndex == 0)
            //{
            //    // txtpaid_Invoice.ReadOnly = true;
            //    txtpaid_Invoice.Text = txtNetInvoice.Text;

            //}
            //else
            //{
            //    //txtpaid_Invoice.ReadOnly = false;
            //    txtpaid_Invoice.Text = "0";
            //}
        }

      
        private void toolStripButton5_Click(object sender, EventArgs e)
        {

            try
            {
                if (DataAccessLayer.CS_17 == false)
                {
                    MessageBox.Show("ليس لديك صلاحية", "صلاحيات ", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                if (Mass.Delete() == true)
                {
                    //====Delete Quentity ========================================================================================================================================================================================================================================================
                    DataTable dt = cls.Details_Bay_Details(txtMainID.Text);
                    var db = new DBDataContext(Properties.Settings.Default.Connection_String);
                    var invoiceDetails = ((Collection<DAL.Sales_Detail>)GridView_Items.DataSource);
                    db.Inv_StoreLogs.DeleteAllOnSubmit(db.Inv_StoreLogs.Where(x => x.Type == (int)invoicesType && invoiceDetails.Select(d => d.ID).Contains(x.TypeID ?? 0)));
                    db.Sales_Details.DeleteAllOnSubmit(db.Sales_Details.Where(x => x.InvoiceType == (int)invoicesType && x.InvoiceID == Convert.ToInt32(txtMainID.Text)));
                    db.SubmitChanges();
                    cls.Delete_BillStoreMain(txtMainID.Text);
                    //cls.Delete_Bay_Details(txtMainID.Text);
                    // Pay
                    PayType_trans_cls.Delete_PayType_trans(txtMainID.Text, txtBillTypeID.Text);
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        AVG_cls.QuantityNow_Avg(dt.Rows[i]["ProdecutID"].ToString(), dt.Rows[i]["StoreID"].ToString(), true);
                    }
                    if (TreasuryID != "0")
                    {
                        TreasuryMovement_CLS.DeleteTreasuryMovement(TreasuryID);
                    }
                
                    btnNew_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
               // Use Fast Inpout
                //QuationQty.Checked = Properties.Settings.Default.HidenMessameQuentity;
                //UseSearchFast.Checked = Properties.Settings.Default.UseQuick_Inpout;
                //SHow_message_save.Checked = Properties.Settings.Default.SHow_message_save;
                //===============================================================================================================================================================
                txtMainID.Text = cls.MaxID_Bay_Main();
                txtRemarks.Text = "";
                txtProdecutID.Text = "";
                txtTotalInvoice.Text = "0";
             
                combPayKind.SelectedIndex = BillSetting_cls.kindPay;
                combAccount.Text = "";
                txtAccountID.Text = "";
                txtpaid_Invoice.Text = "0";
                txtCustomersBlanse.Text = "0";
                txtInvoiceNumber.Text = "";
                TransID = "0";
                TreasuryID = "0";
                // store defult
                txtVatValue.Text = "0";
                UseVatSuppliers = false;
                txtExpenses.Text = "0";
                if (BillSetting_cls.UseStoreDefult == true)
                {
                    Store_cls Store_cls = new Store_cls();
                    DataTable dt_store = Store_cls.Details_Stores(int.Parse(BillSetting_cls.StoreID));
                    txtStoreID.Text = dt_store.Rows[0]["StoreID"].ToString();
                    txtStoreName.Text = dt_store.Rows[0]["StoreName"].ToString();
                }
                else
                {
                    txtStoreID.Text = "";
                    txtStoreName.Text = "";
                }

                 

                if (BillSetting_cls.UseCrrencyDefault == true)
                {
                    Currency_cls Currency = new Currency_cls();
                    DataTable DtCurrency = Currency.Details_Currency(BillSetting_cls.CurrencyID.ToString());
                    combCurrency.Text = DtCurrency.Rows[0]["CurrencyName"].ToString();
                    txtCurrencyRate.Text = DtCurrency.Rows[0]["CurrencyRate"].ToString();
                }
                else
                {
                    combCurrency.Text = null;
                    txtCurrencyRate.Text = "";

                }

                btnSave.Enabled = true;
                btnUpdate.Enabled = false;
                btnDelete.Enabled = false;
                ClearPayType();
                #region UsekindPay

                //===============================================================================================================================================================


                if (BillSetting_cls.UsekindPay == true)
                {
                    PayType_cls PayType_cls = new PayType_cls();
                    DataTable dtPayType = PayType_cls.Details_PayType(BillSetting_cls.PayTypeID);
                    if (dtPayType.Rows.Count > 0)
                    {
                        combPayType.Text = dtPayType.Rows[0]["PayTypeName"].ToString();
                    }
                }
                else
                {
                    combPayType.Text = null;
                }



                //===============================================================================================================================================================

                #endregion

                DGVPayType.DataSource = null;
                DGVPayType.Rows.Clear();
                txtSumPayType.Text = "0";
                GetData();
                 
       
        }

        private void txtInvoiceNumber_TextChanged(object sender, EventArgs e)
        {
            txtInvoiceNumber.BackColor = Color.White;

        }

     

        private void BillBay_frm_KeyDown(object sender, KeyEventArgs e)
        {


            #region "KeyDown"
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    Close();
                }
                else if (e.Control == true && e.KeyCode == Keys.N)
                {
                    btnNew_Click(null, null);
                }
                else if (e.KeyCode == Keys.F2)
                {
                    if (btnSave.Enabled == true)
                    {
                        btnSave_Click(null, null);
                    }
                }
                else if (e.KeyCode == Keys.F3)
                {
                    if (btnUpdate.Enabled == true)
                    {
                        btnUpdate_Click(null, null);
                    }
                }

                else if (e.KeyCode == Keys.F5)
                {
                    button5_Click(null, null);
                }
                if (e.KeyCode == Keys.F4)
                {
                    if (tabControl1.SelectedIndex == 0)
                    {
                        tabControl1.SelectedIndex = 1;
                        txtPayValue.Focus();
                    }
                    else
                    {
                        tabControl1.SelectedIndex = 0;
                    //    txtSearch.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            #endregion

        }



        private void اعادةتحميلالاصنافToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RefreshData();

        }

        private void ToolStripMenuItemSupplers_Click(object sender, EventArgs e)
        {
            try
            {
                if (combAccount.Text.Trim() == "")
                {
                    combAccount.BackColor = Color.Pink;
                    button3_Click(null, null);
                    return;
                }

                SuppliersTrans_frm frm = new SuppliersTrans_frm();
                frm.txtAccountID.Text = txtAccountID.Text;
                frm.txtAccountName.Text = combAccount.Text;
                frm.AddEdit = "Bay";
                frm.ShowDialog(this);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }


        private void txtStoreName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                txtStoreID.Text = "";
                txtStoreName.Text = "";
            }
        }

        Boolean UseVatSuppliers = false;
        private void combAccount_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (combAccount.SelectedIndex > -1)
                {
                    combAccount.BackColor = Color.White;
                    txtAccountID.Text = combAccount.SelectedValue.ToString();

                    Suppliers_cls Suppliers_cls = new Suppliers_cls();

                    DataTable dt = Suppliers_cls.Details_Suppliers(txtAccountID.Text);
                    //if (dt.Rows.Count > 0)
                    //{
                    //    if (!string.IsNullOrEmpty(dt.Rows[0]["UseVat"].ToString()))
                    //    {


                    //        if (Convert.ToBoolean(dt.Rows[0]["UseVat"].ToString()) == true)
                    //        {
                    //            UseVatSuppliers = Convert.ToBoolean(dt.Rows[0]["UseVat"].ToString());

                    //            if (BillSetting_cls.UseVat == true)
                    //            {
                    //                txtMainVat.Text = BillSetting_cls.Vat;
                    //            }
                    //            else
                    //            {
                    //                txtMainVat.Text = "0";
                    //            }
                    //        }
                    //        else
                    //        {
                    //            UseVatSuppliers = false;
                    //            txtMainVat.Text = "0";
                    //            txtChange(null, null);
                    //        }
                    //    }
                    //    else
                    //    {
                    //        UseVatSuppliers = false;
                    //        txtMainVat.Text = "0";
                    //        txtChange(null, null);
                    //    } 
                    //}

                }


            }
            catch
            {
                txtAccountID.Text = "";
            }
        }

        private void combAccount_TextChanged(object sender, EventArgs e)
        {
            try
            {
                combAccount.BackColor = Color.White;
                if (combAccount.Text.Trim() == "")
                {
                    txtAccountID.Text = "";
                }
                else
                {
                    txtAccountID.Text = combAccount.SelectedValue.ToString();
                }
            }
            catch
            {
                txtAccountID.Text = "";
            }
        }

        private void txtAccountID_TextChanged(object sender, EventArgs e)
        {
            BalenceSuppliers(txtAccountID.Text);
        } 
        private void combPayType_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (combPayType.SelectedIndex >= 0)
                {
                    txtPayValue.Focus();
                }

            }
        }

        private void combPayType_TextChanged(object sender, EventArgs e)
        {
            combPayType.BackColor = Color.White;

        }

        private void txtPayValue_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (!string.IsNullOrEmpty(txtPayValue.Text.Trim()))
                {
                    txtStatement.Focus();
                }

            }
        }

        private void txtPayValue_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

        private void txtPayValue_TextChanged(object sender, EventArgs e)
        {
            txtPayValue.BackColor = Color.White;
        }

        private void txtStatement_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {

                if (e.KeyCode == Keys.Enter)
                {

                    if (combCurrency.SelectedIndex < 0)
                    {
                        MessageBox.Show("يرجي تحديد العملة", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        combCurrency.Focus();
                        return;
                    }
                    if (string.IsNullOrEmpty(txtCurrencyRate.Text.Trim()))
                    {
                        MessageBox.Show("يرجي تحديد سعر التعادل", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        txtCurrencyRate.Focus();
                        return;
                    }


                    if (combPayType.SelectedIndex < 0)
                    {
                        combPayType.BackColor = Color.Pink;
                        combPayType.Focus();
                        return;
                    }
                    if (txtPayValue.Text.Trim() == "")
                    {
                        txtPayValue.BackColor = Color.Pink;
                        txtPayValue.Focus();
                        return;
                    }
                    if (double.Parse(txtPayValue.Text) == 0)
                    {
                        txtPayValue.BackColor = Color.Pink;
                        txtPayValue.Focus();
                        return;
                    }
                    for (int i = 0; i < DGVPayType.Rows.Count; i++)
                    {
                        if (DGVPayType.Rows[i].Cells["PayTypeID"].Value.ToString() == combPayType.SelectedValue.ToString())
                        {
                            combPayType.BackColor = Color.Pink;
                            combPayType.Focus();
                            return;
                        }
                    }
                    AddDgv_PayType(DGVPayType, combPayType.SelectedValue.ToString(), combPayType.Text, (double.Parse(txtPayValue.Text) * double.Parse(txtCurrencyRate.Text)).ToString(), txtStatement.Text, combCurrency.SelectedValue.ToString(), combCurrency.Text, txtCurrencyRate.Text, txtPayValue.Text);
                    ClearPayType();
                    SumPayType();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }



        private void AddDgv_PayType(DataGridView dgv, string PayTypeID, string PayTypeName, string PayValue, string Statement, string CurrencyID2, string CurrencyName2, string CurrencyRate2, string CurrencyPrice2)
        {
            try
            {
                dgv.Rows.Add();
                dgv.ClearSelection();
                dgv.Rows[dgv.Rows.Count - 1].Selected = true;
                var lastRows = dgv.Rows.Count - 1;
                dgv.Rows[lastRows].Cells["PayTypeID"].Value = PayTypeID;
                dgv.Rows[lastRows].Cells["PayTypeName"].Value = PayTypeName;
                dgv.Rows[lastRows].Cells["PayValue"].Value = PayValue;
                dgv.Rows[lastRows].Cells["Statement"].Value = Statement;

                dgv.Rows[lastRows].Cells["CurrencyID2"].Value = CurrencyID2;
                dgv.Rows[lastRows].Cells["CurrencyName2"].Value = CurrencyName2;
                dgv.Rows[lastRows].Cells["CurrencyRate2"].Value = CurrencyRate2;
                dgv.Rows[lastRows].Cells["CurrencyPrice2"].Value = CurrencyPrice2;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }


        private void ClearPayType()
        {
            combPayType.Text = null;
            txtPayValue.Text = "";
            txtStatement.Text = "";
            combPayType.Focus();
        }

        private void SumPayType()
        {
            try
            {

                double Sum = 0;
                for (int i = 0; i < DGVPayType.Rows.Count; i++)
                {
                    Sum += Convert.ToDouble(DGVPayType.Rows[i].Cells["PayValue"].Value.ToString());
                }
                txtSumPayType.Text = Sum.ToString();
                txtpaid_Invoice.Text = txtSumPayType.Text;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            try
            {
                if (DGVPayType.SelectedRows.Count == 0)
                {
                    return;
                }
                if (Mass.Delete() == true)
                {
                    foreach (DataGridViewRow r in DGVPayType.SelectedRows)
                    {

                        DGVPayType.Rows.Remove(r);

                    }
                    SumPayType();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void DGVPayType_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (DGVPayType.Rows.Count == 0)
                {
                    return;
                }
                double pay = double.Parse(DGVPayType.CurrentRow.Cells["CurrencyPrice2"].Value.ToString());
                double Rat = double.Parse(DGVPayType.CurrentRow.Cells["CurrencyRate2"].Value.ToString());
                DGVPayType.CurrentRow.Cells["PayValue"].Value = pay * Rat;
                SumPayType();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }



        private void combCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {

            try
            {
                if (combCurrency.SelectedIndex > -1)
                {
                    Currency_cls crrcls = new Currency_cls();
                    DataTable dt = crrcls.Details_Currency(combCurrency.SelectedValue.ToString());
                    if (dt.Rows.Count > 0)
                    {
                        txtCurrencyRate.Text = dt.Rows[0]["CurrencyRate"].ToString();
                    }
                }
            }
            catch
            {

                return;
            }
        }

        private void txtCurrencyRate_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }
        PurchaseOrder_cls purchaseOrder_cls = new PurchaseOrder_cls();
        private void تحميلالاصنافمنفاتورةطلبالشراءToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {

                BillPurchaseOrder_Search_frm frm = new BillPurchaseOrder_Search_frm();
                frm.Icon = this.Icon;
                frm.ShowDialog(this);
                if (frm.LoadData == false)
                {
                    return;
                }
                btnNew_Click(null, null);
                //btnSave.Enabled = false;
                //btnUpdate.Enabled = true;
                //btnDelete.Enabled = true;
                //DataTable dt = purchaseOrder_cls.Details_PurchaseOrder_Main(long.Parse(frm.DGV2.CurrentRow.Cells["MainID"].Value.ToString()));
                //DataRow Dr = dt.Rows[0];
                //txtMainID.Text = Dr["MainID"].ToString();
                //D1.Value = Convert.ToDateTime(Dr["MyDate"]);

                //// Balense==========================================================================================
                //txtRemarks.Text = Dr["Remarks"].ToString();
                //txtTotalInvoice.Text = Dr["TotalInvoice"].ToString();
                //txtMainVat.Text = Dr["vat"].ToString();
                //txtTotalDescound.Text = Dr["Discount"].ToString();
                //txtNetInvoice.Text = Dr["NetInvoice"].ToString();

                //=========================================================================================================================================================================
                //dt.Clear();
                //DGV1.DataSource = null;
                //DGV1.Rows.Clear();
                ////=========================================================================================================================================================================
                //DataTable dt = purchaseOrder_cls.Details_PurchaseOrder_Details(long.Parse(frm.DGV2.CurrentRow.Cells["MainID"].Value.ToString()));

                //for (int i = 0; i < dt.Rows.Count; i++)
                //{
                //    AddRowDgv(dt.Rows[i]["ProdecutID"].ToString(), dt.Rows[i]["ProdecutName"].ToString(), dt.Rows[i]["Unit"].ToString(), dt.Rows[i]["UnitFactor"].ToString(), dt.Rows[i]["UnitOperating"].ToString(), dt.Rows[i]["Quantity"].ToString(), combCurrency.SelectedValue.ToString(), txtCurrencyRate.Text, (double.Parse(dt.Rows[i]["Price"].ToString()) * double.Parse(txtCurrencyRate.Text)).ToString(), (double.Parse(dt.Rows[i]["TotalPrice"].ToString()) * double.Parse(txtCurrencyRate.Text)).ToString(), combCurrency.Text, txtCurrencyRate.Text, dt.Rows[i]["Price"].ToString(), dt.Rows[i]["Vat"].ToString(), dt.Rows[i]["MainVat"].ToString(), dt.Rows[i]["TotalPrice"].ToString(), dt.Rows[i]["StoreID"].ToString(), dt.Rows[i]["StoreName"].ToString());

                //}

                //lblCount.Text = DGV1.Rows.Count.ToString();
                //SumDgv();










            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtVatValue_TextChanged(object sender, EventArgs e)
        {

        }
        private void txtMainID_TextChanged(object sender, EventArgs e)
        {
            GetData();
        }

    }
}
