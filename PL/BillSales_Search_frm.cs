﻿using ByStro.RPT;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class BillSales_Search_frm : Form
    {
        public BillSales_Search_frm()
        {
            InitializeComponent();
        }
        Sales_CLS cls = new Sales_CLS();
        public Boolean LoadData = false;
        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                DGV1.DataSource = null;
                DGV1.Rows.Clear();
                DataTable dt = new DataTable();
                if (DataAccessLayer.TypeUser == true)
                {
                    dt = cls.Search_Sales_Main(D1.Value, D2.Value, txtSearch.Text);
                }
                else
                {
                    dt = cls.Search_Sales_Main(D1.Value, D2.Value, Properties.Settings.Default.UserID, txtSearch.Text);
                }


                DGV2.AutoGenerateColumns = false;
                DGV2.DataSource = dt;
                sumbills();
            }
            catch
            {
                return;
            }

        }

        private void SalesReturn_Form_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingToolStrip(toolStrip1);
            ERB_Setting.SettingForm(this);
            ERB_Setting.SettingDGV(DGV2);


            LoadBill();
            ERB_Setting.SettingDGV(DGV1);


        }





        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            LoadBill();
        }


        private void LoadBill()
        {
            try
            {

                DataTable dt = new DataTable();
                if (DataAccessLayer.TypeUser == true)
                {
                    dt = cls.Load_Sales_Main(D1.Value, D2.Value);
                }
                else
                {
                    dt = cls.Load_Sales_Main(D1.Value, D2.Value, Properties.Settings.Default.UserID);
                }

                DGV2.AutoGenerateColumns = false;
                DGV2.DataSource = dt;
                sumbills();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            LoadBill();
        }


        public void sumbills()
        {


            try
            {


                Decimal NetInvoice = 0;
                Decimal ReturnInvoise = 0;
                Decimal paid_Invoice = 0;
                Decimal RestInvoise = 0;
                lblCount.Text = "0";

                for (int i = 0; i < DGV2.Rows.Count; i++)
                {
                    ReturnInvoise += Convert.ToDecimal(DGV2.Rows[i].Cells["ReturnInvoise"].Value);
                    NetInvoice += Convert.ToDecimal(DGV2.Rows[i].Cells["NetInvoice"].Value);
                    paid_Invoice += Convert.ToDecimal(DGV2.Rows[i].Cells["PaidInvoice"].Value);
                    RestInvoise += Convert.ToDecimal(DGV2.Rows[i].Cells["RestInvoise"].Value);
                }
                lblRetrunInvoice.Text = ReturnInvoise.ToString();
                lblSum.Text = NetInvoice.ToString();
                lblPay.Text = paid_Invoice.ToString();
                lblRest.Text = RestInvoise.ToString();
                lblCount.Text = DGV2.Rows.Count.ToString(); ;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }




        }



        private void DGV2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (DGV2.Rows.Count == 0)
                {
                    return;
                }
                DGV1.DataSource = null;
                DGV1.Rows.Clear();
                DataTable dt = cls.Details_Sales_Details(DGV2.CurrentRow.Cells["MainID"].Value.ToString());
                DGV1.AutoGenerateColumns = false;
                DGV1.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        public Boolean ReturnInvoice = false;
        private void btnView_Click(object sender, EventArgs e)
        {

            if (DGV2.Rows.Count == 0)
            {
                return;
            }
            if (ReturnInvoice == false)
            {
                if (Convert.ToDouble(DGV2.CurrentRow.Cells["ReturnInvoise"].Value) != 0)
                {
                    MessageBox.Show("لا يمكنك عرض الفاتورةبسبب تم  عمل مرتجع علي الفاتورة", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            LoadData = true;




            Close();
        }

        private void BillBay_Search_frm_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.WindowState == FormWindowState.Maximized)
                {
                    DGV2.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                }
                else
                {
                    DGV2.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
                    DGV2.Columns[0].Width = 150;
                    DGV2.Columns[1].Width = 150;
                    DGV2.Columns[2].Width = 250;
                    DGV2.Columns[3].Width = 150;
                    DGV2.Columns[4].Width = 150;
                    DGV2.Columns[5].Width = 100;
                    DGV2.Columns[6].Width = 150;
                    DGV2.Columns[7].Width = 100;
                    DGV2.Columns[8].Width = 150;
                    DGV2.Columns[9].Width = 150;
                    DGV2.Columns[10].Width = 300;
                    DGV2.Columns[11].Width = 200;
                }

                panel3.Height = this.Height / 2;
            }
            catch
            {

            }

        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            PrintBill();

        }


        private void PrintBill()
        {


            
            BillSales_frm.PrintBill(Convert.ToInt32(DGV2.CurrentRow.Cells["MainID"].Value.ToString()));

        }




         
        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            PrintBill();
        }









    }
}