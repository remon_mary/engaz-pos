﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class Info_Form : Form
    {
        public Info_Form()
        {
            InitializeComponent();
        }

        private void Info_Form_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingForm(this);
            label2.Text=DataAccessLayer.Version;
            txtProductId.Text = Properties.Settings.Default.computerID ;
            txtproductKey.Text = Properties.Settings.Default.Finull_Sarial;
            txtproductType.Text = Properties.Settings.Default.LicenseType;
            if (Properties.Settings.Default.LicenseType== "Trail")
            {
                lblRegister.Visible = true;
            }
            else
            {
                lblRegister.Visible = false;
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

            TextBox txtCopy=new TextBox();
            txtCopy.Text= txtProductId.Text;
            txtCopy.SelectAll();
            txtCopy.Copy();
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            SerilFull_FORM frm = new SerilFull_FORM();
            frm.close = false;
            frm.ShowDialog();
        }
    }
}
