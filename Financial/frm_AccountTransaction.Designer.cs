﻿namespace ByStro.Forms
{
    partial class frm_AccountTransaction
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AccountTransaction));
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txt_ID = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.dateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.spinEdit1 = new DevExpress.XtraEditors.SpinEdit();
            this.lkp_Store = new DevExpress.XtraEditors.LookUpEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_DrawerToBalance = new DevExpress.XtraEditors.TextEdit();
            this.lkp_DrawerTo = new DevExpress.XtraEditors.LookUpEdit();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_DrawerFromBalance = new DevExpress.XtraEditors.TextEdit();
            this.lkp_DrawerFrom = new DevExpress.XtraEditors.LookUpEdit();
            ((System.ComponentModel.ISupportInitialize)(this.lkp_List)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt_ID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkp_Store.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DrawerToBalance.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkp_DrawerTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DrawerFromBalance.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkp_DrawerFrom.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lkp_List
            // 
            resources.ApplyResources(this.lkp_List, "lkp_List");
            // 
            // SubItem_ConvertTo
            // 
            resources.ApplyResources(this.SubItem_ConvertTo, "SubItem_ConvertTo");
            this.SubItem_ConvertTo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("SubItem_ConvertTo.ImageOptions.Image")));
            this.SubItem_ConvertTo.ImageOptions.ImageIndex = ((int)(resources.GetObject("SubItem_ConvertTo.ImageOptions.ImageIndex")));
            this.SubItem_ConvertTo.ImageOptions.LargeImageIndex = ((int)(resources.GetObject("SubItem_ConvertTo.ImageOptions.LargeImageIndex")));
            this.SubItem_ConvertTo.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("SubItem_ConvertTo.ImageOptions.SvgImage")));
            this.SubItem_ConvertTo.MenuAppearance.HeaderItemAppearance.FontSizeDelta = ((int)(resources.GetObject("SubItem_ConvertTo.MenuAppearance.HeaderItemAppearance.FontSizeDelta")));
            this.SubItem_ConvertTo.MenuAppearance.HeaderItemAppearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("SubItem_ConvertTo.MenuAppearance.HeaderItemAppearance.FontStyleDelta")));
            this.SubItem_ConvertTo.MenuAppearance.HeaderItemAppearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("SubItem_ConvertTo.MenuAppearance.HeaderItemAppearance.GradientMode")));
            this.SubItem_ConvertTo.MenuAppearance.HeaderItemAppearance.Image = ((System.Drawing.Image)(resources.GetObject("SubItem_ConvertTo.MenuAppearance.HeaderItemAppearance.Image")));
            // 
            // SubItem_Printbills
            // 
            resources.ApplyResources(this.SubItem_Printbills, "SubItem_Printbills");
            this.SubItem_Printbills.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("SubItem_Printbills.ImageOptions.Image")));
            this.SubItem_Printbills.ImageOptions.ImageIndex = ((int)(resources.GetObject("SubItem_Printbills.ImageOptions.ImageIndex")));
            this.SubItem_Printbills.ImageOptions.LargeImageIndex = ((int)(resources.GetObject("SubItem_Printbills.ImageOptions.LargeImageIndex")));
            this.SubItem_Printbills.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("SubItem_Printbills.ImageOptions.SvgImage")));
            this.SubItem_Printbills.MenuAppearance.HeaderItemAppearance.FontSizeDelta = ((int)(resources.GetObject("SubItem_Printbills.MenuAppearance.HeaderItemAppearance.FontSizeDelta")));
            this.SubItem_Printbills.MenuAppearance.HeaderItemAppearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("SubItem_Printbills.MenuAppearance.HeaderItemAppearance.FontStyleDelta")));
            this.SubItem_Printbills.MenuAppearance.HeaderItemAppearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("SubItem_Printbills.MenuAppearance.HeaderItemAppearance.GradientMode")));
            this.SubItem_Printbills.MenuAppearance.HeaderItemAppearance.Image = ((System.Drawing.Image)(resources.GetObject("SubItem_Printbills.MenuAppearance.HeaderItemAppearance.Image")));
            // 
            // groupControl3
            // 
            resources.ApplyResources(this.groupControl3, "groupControl3");
            this.groupControl3.AppearanceCaption.FontSizeDelta = ((int)(resources.GetObject("groupControl3.AppearanceCaption.FontSizeDelta")));
            this.groupControl3.AppearanceCaption.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("groupControl3.AppearanceCaption.FontStyleDelta")));
            this.groupControl3.AppearanceCaption.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("groupControl3.AppearanceCaption.GradientMode")));
            this.groupControl3.AppearanceCaption.Image = ((System.Drawing.Image)(resources.GetObject("groupControl3.AppearanceCaption.Image")));
            this.groupControl3.AppearanceCaption.Options.UseTextOptions = true;
            this.groupControl3.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.groupControl3.Controls.Add(this.label8);
            this.groupControl3.Controls.Add(this.label7);
            this.groupControl3.Controls.Add(this.txt_ID);
            this.groupControl3.Controls.Add(this.label6);
            this.groupControl3.Controls.Add(this.label9);
            this.groupControl3.Controls.Add(this.label5);
            this.groupControl3.Controls.Add(this.memoEdit1);
            this.groupControl3.Controls.Add(this.dateEdit1);
            this.groupControl3.Controls.Add(this.spinEdit1);
            this.groupControl3.Controls.Add(this.lkp_Store);
            this.groupControl3.Name = "groupControl3";
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // txt_ID
            // 
            resources.ApplyResources(this.txt_ID, "txt_ID");
            this.txt_ID.Name = "txt_ID";
            this.txt_ID.Properties.AccessibleDescription = resources.GetString("txt_ID.Properties.AccessibleDescription");
            this.txt_ID.Properties.AccessibleName = resources.GetString("txt_ID.Properties.AccessibleName");
            this.txt_ID.Properties.AutoHeight = ((bool)(resources.GetObject("txt_ID.Properties.AutoHeight")));
            this.txt_ID.Properties.Mask.AutoComplete = ((DevExpress.XtraEditors.Mask.AutoCompleteType)(resources.GetObject("txt_ID.Properties.Mask.AutoComplete")));
            this.txt_ID.Properties.Mask.BeepOnError = ((bool)(resources.GetObject("txt_ID.Properties.Mask.BeepOnError")));
            this.txt_ID.Properties.Mask.EditMask = resources.GetString("txt_ID.Properties.Mask.EditMask");
            this.txt_ID.Properties.Mask.IgnoreMaskBlank = ((bool)(resources.GetObject("txt_ID.Properties.Mask.IgnoreMaskBlank")));
            this.txt_ID.Properties.Mask.MaskType = ((DevExpress.XtraEditors.Mask.MaskType)(resources.GetObject("txt_ID.Properties.Mask.MaskType")));
            this.txt_ID.Properties.Mask.PlaceHolder = ((char)(resources.GetObject("txt_ID.Properties.Mask.PlaceHolder")));
            this.txt_ID.Properties.Mask.SaveLiteral = ((bool)(resources.GetObject("txt_ID.Properties.Mask.SaveLiteral")));
            this.txt_ID.Properties.Mask.ShowPlaceHolders = ((bool)(resources.GetObject("txt_ID.Properties.Mask.ShowPlaceHolders")));
            this.txt_ID.Properties.Mask.UseMaskAsDisplayFormat = ((bool)(resources.GetObject("txt_ID.Properties.Mask.UseMaskAsDisplayFormat")));
            this.txt_ID.Properties.NullValuePrompt = resources.GetString("txt_ID.Properties.NullValuePrompt");
            this.txt_ID.Properties.NullValuePromptShowForEmptyValue = ((bool)(resources.GetObject("txt_ID.Properties.NullValuePromptShowForEmptyValue")));
            this.txt_ID.Properties.ReadOnly = true;
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // memoEdit1
            // 
            resources.ApplyResources(this.memoEdit1, "memoEdit1");
            this.memoEdit1.MenuManager = this.barManager1;
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Properties.AccessibleDescription = resources.GetString("memoEdit1.Properties.AccessibleDescription");
            this.memoEdit1.Properties.AccessibleName = resources.GetString("memoEdit1.Properties.AccessibleName");
            this.memoEdit1.Properties.NullValuePrompt = resources.GetString("memoEdit1.Properties.NullValuePrompt");
            this.memoEdit1.Properties.NullValuePromptShowForEmptyValue = ((bool)(resources.GetObject("memoEdit1.Properties.NullValuePromptShowForEmptyValue")));
            // 
            // dateEdit1
            // 
            resources.ApplyResources(this.dateEdit1, "dateEdit1");
            this.dateEdit1.MenuManager = this.barManager1;
            this.dateEdit1.Name = "dateEdit1";
            this.dateEdit1.Properties.AccessibleDescription = resources.GetString("dateEdit1.Properties.AccessibleDescription");
            this.dateEdit1.Properties.AccessibleName = resources.GetString("dateEdit1.Properties.AccessibleName");
            this.dateEdit1.Properties.AutoHeight = ((bool)(resources.GetObject("dateEdit1.Properties.AutoHeight")));
            this.dateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("dateEdit1.Properties.Buttons"))))});
            this.dateEdit1.Properties.CalendarTimeProperties.AccessibleDescription = resources.GetString("dateEdit1.Properties.CalendarTimeProperties.AccessibleDescription");
            this.dateEdit1.Properties.CalendarTimeProperties.AccessibleName = resources.GetString("dateEdit1.Properties.CalendarTimeProperties.AccessibleName");
            this.dateEdit1.Properties.CalendarTimeProperties.AutoHeight = ((bool)(resources.GetObject("dateEdit1.Properties.CalendarTimeProperties.AutoHeight")));
            this.dateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("dateEdit1.Properties.CalendarTimeProperties.Buttons"))))});
            this.dateEdit1.Properties.CalendarTimeProperties.Mask.AutoComplete = ((DevExpress.XtraEditors.Mask.AutoCompleteType)(resources.GetObject("dateEdit1.Properties.CalendarTimeProperties.Mask.AutoComplete")));
            this.dateEdit1.Properties.CalendarTimeProperties.Mask.BeepOnError = ((bool)(resources.GetObject("dateEdit1.Properties.CalendarTimeProperties.Mask.BeepOnError")));
            this.dateEdit1.Properties.CalendarTimeProperties.Mask.EditMask = resources.GetString("dateEdit1.Properties.CalendarTimeProperties.Mask.EditMask");
            this.dateEdit1.Properties.CalendarTimeProperties.Mask.IgnoreMaskBlank = ((bool)(resources.GetObject("dateEdit1.Properties.CalendarTimeProperties.Mask.IgnoreMaskBlank")));
            this.dateEdit1.Properties.CalendarTimeProperties.Mask.MaskType = ((DevExpress.XtraEditors.Mask.MaskType)(resources.GetObject("dateEdit1.Properties.CalendarTimeProperties.Mask.MaskType")));
            this.dateEdit1.Properties.CalendarTimeProperties.Mask.PlaceHolder = ((char)(resources.GetObject("dateEdit1.Properties.CalendarTimeProperties.Mask.PlaceHolder")));
            this.dateEdit1.Properties.CalendarTimeProperties.Mask.SaveLiteral = ((bool)(resources.GetObject("dateEdit1.Properties.CalendarTimeProperties.Mask.SaveLiteral")));
            this.dateEdit1.Properties.CalendarTimeProperties.Mask.ShowPlaceHolders = ((bool)(resources.GetObject("dateEdit1.Properties.CalendarTimeProperties.Mask.ShowPlaceHolders")));
            this.dateEdit1.Properties.CalendarTimeProperties.Mask.UseMaskAsDisplayFormat = ((bool)(resources.GetObject("dateEdit1.Properties.CalendarTimeProperties.Mask.UseMaskAsDisplayFormat")));
            this.dateEdit1.Properties.CalendarTimeProperties.NullValuePrompt = resources.GetString("dateEdit1.Properties.CalendarTimeProperties.NullValuePrompt");
            this.dateEdit1.Properties.CalendarTimeProperties.NullValuePromptShowForEmptyValue = ((bool)(resources.GetObject("dateEdit1.Properties.CalendarTimeProperties.NullValuePromptShowForEmptyValue")));
            this.dateEdit1.Properties.Mask.AutoComplete = ((DevExpress.XtraEditors.Mask.AutoCompleteType)(resources.GetObject("dateEdit1.Properties.Mask.AutoComplete")));
            this.dateEdit1.Properties.Mask.BeepOnError = ((bool)(resources.GetObject("dateEdit1.Properties.Mask.BeepOnError")));
            this.dateEdit1.Properties.Mask.EditMask = resources.GetString("dateEdit1.Properties.Mask.EditMask");
            this.dateEdit1.Properties.Mask.IgnoreMaskBlank = ((bool)(resources.GetObject("dateEdit1.Properties.Mask.IgnoreMaskBlank")));
            this.dateEdit1.Properties.Mask.MaskType = ((DevExpress.XtraEditors.Mask.MaskType)(resources.GetObject("dateEdit1.Properties.Mask.MaskType")));
            this.dateEdit1.Properties.Mask.PlaceHolder = ((char)(resources.GetObject("dateEdit1.Properties.Mask.PlaceHolder")));
            this.dateEdit1.Properties.Mask.SaveLiteral = ((bool)(resources.GetObject("dateEdit1.Properties.Mask.SaveLiteral")));
            this.dateEdit1.Properties.Mask.ShowPlaceHolders = ((bool)(resources.GetObject("dateEdit1.Properties.Mask.ShowPlaceHolders")));
            this.dateEdit1.Properties.Mask.UseMaskAsDisplayFormat = ((bool)(resources.GetObject("dateEdit1.Properties.Mask.UseMaskAsDisplayFormat")));
            this.dateEdit1.Properties.NullValuePrompt = resources.GetString("dateEdit1.Properties.NullValuePrompt");
            this.dateEdit1.Properties.NullValuePromptShowForEmptyValue = ((bool)(resources.GetObject("dateEdit1.Properties.NullValuePromptShowForEmptyValue")));
            // 
            // spinEdit1
            // 
            resources.ApplyResources(this.spinEdit1, "spinEdit1");
            this.spinEdit1.MenuManager = this.barManager1;
            this.spinEdit1.Name = "spinEdit1";
            this.spinEdit1.Properties.AccessibleDescription = resources.GetString("spinEdit1.Properties.AccessibleDescription");
            this.spinEdit1.Properties.AccessibleName = resources.GetString("spinEdit1.Properties.AccessibleName");
            this.spinEdit1.Properties.AutoHeight = ((bool)(resources.GetObject("spinEdit1.Properties.AutoHeight")));
            this.spinEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("spinEdit1.Properties.Buttons"))))});
            this.spinEdit1.Properties.Mask.AutoComplete = ((DevExpress.XtraEditors.Mask.AutoCompleteType)(resources.GetObject("spinEdit1.Properties.Mask.AutoComplete")));
            this.spinEdit1.Properties.Mask.BeepOnError = ((bool)(resources.GetObject("spinEdit1.Properties.Mask.BeepOnError")));
            this.spinEdit1.Properties.Mask.EditMask = resources.GetString("spinEdit1.Properties.Mask.EditMask");
            this.spinEdit1.Properties.Mask.IgnoreMaskBlank = ((bool)(resources.GetObject("spinEdit1.Properties.Mask.IgnoreMaskBlank")));
            this.spinEdit1.Properties.Mask.MaskType = ((DevExpress.XtraEditors.Mask.MaskType)(resources.GetObject("spinEdit1.Properties.Mask.MaskType")));
            this.spinEdit1.Properties.Mask.PlaceHolder = ((char)(resources.GetObject("spinEdit1.Properties.Mask.PlaceHolder")));
            this.spinEdit1.Properties.Mask.SaveLiteral = ((bool)(resources.GetObject("spinEdit1.Properties.Mask.SaveLiteral")));
            this.spinEdit1.Properties.Mask.ShowPlaceHolders = ((bool)(resources.GetObject("spinEdit1.Properties.Mask.ShowPlaceHolders")));
            this.spinEdit1.Properties.Mask.UseMaskAsDisplayFormat = ((bool)(resources.GetObject("spinEdit1.Properties.Mask.UseMaskAsDisplayFormat")));
            this.spinEdit1.Properties.NullValuePrompt = resources.GetString("spinEdit1.Properties.NullValuePrompt");
            this.spinEdit1.Properties.NullValuePromptShowForEmptyValue = ((bool)(resources.GetObject("spinEdit1.Properties.NullValuePromptShowForEmptyValue")));
            // 
            // lkp_Store
            // 
            resources.ApplyResources(this.lkp_Store, "lkp_Store");
            this.lkp_Store.Name = "lkp_Store";
            this.lkp_Store.Properties.AccessibleDescription = resources.GetString("lkp_Store.Properties.AccessibleDescription");
            this.lkp_Store.Properties.AccessibleName = resources.GetString("lkp_Store.Properties.AccessibleName");
            this.lkp_Store.Properties.AutoHeight = ((bool)(resources.GetObject("lkp_Store.Properties.AutoHeight")));
            this.lkp_Store.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("lkp_Store.Properties.Buttons"))))});
            this.lkp_Store.Properties.NullText = resources.GetString("lkp_Store.Properties.NullText");
            this.lkp_Store.Properties.NullValuePrompt = resources.GetString("lkp_Store.Properties.NullValuePrompt");
            this.lkp_Store.Properties.NullValuePromptShowForEmptyValue = ((bool)(resources.GetObject("lkp_Store.Properties.NullValuePromptShowForEmptyValue")));
            // 
            // groupControl1
            // 
            resources.ApplyResources(this.groupControl1, "groupControl1");
            this.groupControl1.AppearanceCaption.FontSizeDelta = ((int)(resources.GetObject("groupControl1.AppearanceCaption.FontSizeDelta")));
            this.groupControl1.AppearanceCaption.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("groupControl1.AppearanceCaption.FontStyleDelta")));
            this.groupControl1.AppearanceCaption.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("groupControl1.AppearanceCaption.GradientMode")));
            this.groupControl1.AppearanceCaption.Image = ((System.Drawing.Image)(resources.GetObject("groupControl1.AppearanceCaption.Image")));
            this.groupControl1.AppearanceCaption.Options.UseTextOptions = true;
            this.groupControl1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.groupControl1.Controls.Add(this.label4);
            this.groupControl1.Controls.Add(this.label3);
            this.groupControl1.Controls.Add(this.txt_DrawerToBalance);
            this.groupControl1.Controls.Add(this.lkp_DrawerTo);
            this.groupControl1.Name = "groupControl1";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // txt_DrawerToBalance
            // 
            resources.ApplyResources(this.txt_DrawerToBalance, "txt_DrawerToBalance");
            this.txt_DrawerToBalance.Name = "txt_DrawerToBalance";
            this.txt_DrawerToBalance.Properties.AccessibleDescription = resources.GetString("txt_DrawerToBalance.Properties.AccessibleDescription");
            this.txt_DrawerToBalance.Properties.AccessibleName = resources.GetString("txt_DrawerToBalance.Properties.AccessibleName");
            this.txt_DrawerToBalance.Properties.AutoHeight = ((bool)(resources.GetObject("txt_DrawerToBalance.Properties.AutoHeight")));
            this.txt_DrawerToBalance.Properties.Mask.AutoComplete = ((DevExpress.XtraEditors.Mask.AutoCompleteType)(resources.GetObject("txt_DrawerToBalance.Properties.Mask.AutoComplete")));
            this.txt_DrawerToBalance.Properties.Mask.BeepOnError = ((bool)(resources.GetObject("txt_DrawerToBalance.Properties.Mask.BeepOnError")));
            this.txt_DrawerToBalance.Properties.Mask.EditMask = resources.GetString("txt_DrawerToBalance.Properties.Mask.EditMask");
            this.txt_DrawerToBalance.Properties.Mask.IgnoreMaskBlank = ((bool)(resources.GetObject("txt_DrawerToBalance.Properties.Mask.IgnoreMaskBlank")));
            this.txt_DrawerToBalance.Properties.Mask.MaskType = ((DevExpress.XtraEditors.Mask.MaskType)(resources.GetObject("txt_DrawerToBalance.Properties.Mask.MaskType")));
            this.txt_DrawerToBalance.Properties.Mask.PlaceHolder = ((char)(resources.GetObject("txt_DrawerToBalance.Properties.Mask.PlaceHolder")));
            this.txt_DrawerToBalance.Properties.Mask.SaveLiteral = ((bool)(resources.GetObject("txt_DrawerToBalance.Properties.Mask.SaveLiteral")));
            this.txt_DrawerToBalance.Properties.Mask.ShowPlaceHolders = ((bool)(resources.GetObject("txt_DrawerToBalance.Properties.Mask.ShowPlaceHolders")));
            this.txt_DrawerToBalance.Properties.Mask.UseMaskAsDisplayFormat = ((bool)(resources.GetObject("txt_DrawerToBalance.Properties.Mask.UseMaskAsDisplayFormat")));
            this.txt_DrawerToBalance.Properties.NullValuePrompt = resources.GetString("txt_DrawerToBalance.Properties.NullValuePrompt");
            this.txt_DrawerToBalance.Properties.NullValuePromptShowForEmptyValue = ((bool)(resources.GetObject("txt_DrawerToBalance.Properties.NullValuePromptShowForEmptyValue")));
            this.txt_DrawerToBalance.Properties.ReadOnly = true;
            // 
            // lkp_DrawerTo
            // 
            resources.ApplyResources(this.lkp_DrawerTo, "lkp_DrawerTo");
            this.lkp_DrawerTo.Name = "lkp_DrawerTo";
            this.lkp_DrawerTo.Properties.AccessibleDescription = resources.GetString("lkp_DrawerTo.Properties.AccessibleDescription");
            this.lkp_DrawerTo.Properties.AccessibleName = resources.GetString("lkp_DrawerTo.Properties.AccessibleName");
            this.lkp_DrawerTo.Properties.AutoHeight = ((bool)(resources.GetObject("lkp_DrawerTo.Properties.AutoHeight")));
            this.lkp_DrawerTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("lkp_DrawerTo.Properties.Buttons"))))});
            this.lkp_DrawerTo.Properties.NullText = resources.GetString("lkp_DrawerTo.Properties.NullText");
            this.lkp_DrawerTo.Properties.NullValuePrompt = resources.GetString("lkp_DrawerTo.Properties.NullValuePrompt");
            this.lkp_DrawerTo.Properties.NullValuePromptShowForEmptyValue = ((bool)(resources.GetObject("lkp_DrawerTo.Properties.NullValuePromptShowForEmptyValue")));
            this.lkp_DrawerTo.EditValueChanged += new System.EventHandler(this.lkp_DrawerFrom_EditValueChanged);
            // 
            // groupControl2
            // 
            resources.ApplyResources(this.groupControl2, "groupControl2");
            this.groupControl2.AppearanceCaption.FontSizeDelta = ((int)(resources.GetObject("groupControl2.AppearanceCaption.FontSizeDelta")));
            this.groupControl2.AppearanceCaption.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("groupControl2.AppearanceCaption.FontStyleDelta")));
            this.groupControl2.AppearanceCaption.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("groupControl2.AppearanceCaption.GradientMode")));
            this.groupControl2.AppearanceCaption.Image = ((System.Drawing.Image)(resources.GetObject("groupControl2.AppearanceCaption.Image")));
            this.groupControl2.AppearanceCaption.Options.UseTextOptions = true;
            this.groupControl2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.groupControl2.Controls.Add(this.label1);
            this.groupControl2.Controls.Add(this.label2);
            this.groupControl2.Controls.Add(this.txt_DrawerFromBalance);
            this.groupControl2.Controls.Add(this.lkp_DrawerFrom);
            this.groupControl2.Name = "groupControl2";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // txt_DrawerFromBalance
            // 
            resources.ApplyResources(this.txt_DrawerFromBalance, "txt_DrawerFromBalance");
            this.txt_DrawerFromBalance.Name = "txt_DrawerFromBalance";
            this.txt_DrawerFromBalance.Properties.AccessibleDescription = resources.GetString("txt_DrawerFromBalance.Properties.AccessibleDescription");
            this.txt_DrawerFromBalance.Properties.AccessibleName = resources.GetString("txt_DrawerFromBalance.Properties.AccessibleName");
            this.txt_DrawerFromBalance.Properties.AutoHeight = ((bool)(resources.GetObject("txt_DrawerFromBalance.Properties.AutoHeight")));
            this.txt_DrawerFromBalance.Properties.Mask.AutoComplete = ((DevExpress.XtraEditors.Mask.AutoCompleteType)(resources.GetObject("txt_DrawerFromBalance.Properties.Mask.AutoComplete")));
            this.txt_DrawerFromBalance.Properties.Mask.BeepOnError = ((bool)(resources.GetObject("txt_DrawerFromBalance.Properties.Mask.BeepOnError")));
            this.txt_DrawerFromBalance.Properties.Mask.EditMask = resources.GetString("txt_DrawerFromBalance.Properties.Mask.EditMask");
            this.txt_DrawerFromBalance.Properties.Mask.IgnoreMaskBlank = ((bool)(resources.GetObject("txt_DrawerFromBalance.Properties.Mask.IgnoreMaskBlank")));
            this.txt_DrawerFromBalance.Properties.Mask.MaskType = ((DevExpress.XtraEditors.Mask.MaskType)(resources.GetObject("txt_DrawerFromBalance.Properties.Mask.MaskType")));
            this.txt_DrawerFromBalance.Properties.Mask.PlaceHolder = ((char)(resources.GetObject("txt_DrawerFromBalance.Properties.Mask.PlaceHolder")));
            this.txt_DrawerFromBalance.Properties.Mask.SaveLiteral = ((bool)(resources.GetObject("txt_DrawerFromBalance.Properties.Mask.SaveLiteral")));
            this.txt_DrawerFromBalance.Properties.Mask.ShowPlaceHolders = ((bool)(resources.GetObject("txt_DrawerFromBalance.Properties.Mask.ShowPlaceHolders")));
            this.txt_DrawerFromBalance.Properties.Mask.UseMaskAsDisplayFormat = ((bool)(resources.GetObject("txt_DrawerFromBalance.Properties.Mask.UseMaskAsDisplayFormat")));
            this.txt_DrawerFromBalance.Properties.NullValuePrompt = resources.GetString("txt_DrawerFromBalance.Properties.NullValuePrompt");
            this.txt_DrawerFromBalance.Properties.NullValuePromptShowForEmptyValue = ((bool)(resources.GetObject("txt_DrawerFromBalance.Properties.NullValuePromptShowForEmptyValue")));
            this.txt_DrawerFromBalance.Properties.ReadOnly = true;
            // 
            // lkp_DrawerFrom
            // 
            resources.ApplyResources(this.lkp_DrawerFrom, "lkp_DrawerFrom");
            this.lkp_DrawerFrom.Name = "lkp_DrawerFrom";
            this.lkp_DrawerFrom.Properties.AccessibleDescription = resources.GetString("lkp_DrawerFrom.Properties.AccessibleDescription");
            this.lkp_DrawerFrom.Properties.AccessibleName = resources.GetString("lkp_DrawerFrom.Properties.AccessibleName");
            this.lkp_DrawerFrom.Properties.AutoHeight = ((bool)(resources.GetObject("lkp_DrawerFrom.Properties.AutoHeight")));
            this.lkp_DrawerFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("lkp_DrawerFrom.Properties.Buttons"))))});
            this.lkp_DrawerFrom.Properties.NullText = resources.GetString("lkp_DrawerFrom.Properties.NullText");
            this.lkp_DrawerFrom.Properties.NullValuePrompt = resources.GetString("lkp_DrawerFrom.Properties.NullValuePrompt");
            this.lkp_DrawerFrom.Properties.NullValuePromptShowForEmptyValue = ((bool)(resources.GetObject("lkp_DrawerFrom.Properties.NullValuePromptShowForEmptyValue")));
            this.lkp_DrawerFrom.EditValueChanged += new System.EventHandler(this.lkp_DrawerFrom_EditValueChanged);
            // 
            // frm_AccountTransaction
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Name = "frm_AccountTransaction";
            this.Controls.SetChildIndex(this.groupControl1, 0);
            this.Controls.SetChildIndex(this.groupControl2, 0);
            this.Controls.SetChildIndex(this.groupControl3, 0);
            ((System.ComponentModel.ISupportInitialize)(this.lkp_List)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt_ID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkp_Store.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DrawerToBalance.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkp_DrawerTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DrawerFromBalance.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkp_DrawerFrom.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraEditors.DateEdit dateEdit1;
        private DevExpress.XtraEditors.SpinEdit spinEdit1;
        private DevExpress.XtraEditors.LookUpEdit lkp_Store;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.TextEdit txt_DrawerToBalance;
        private DevExpress.XtraEditors.LookUpEdit lkp_DrawerTo;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit txt_DrawerFromBalance;
        private DevExpress.XtraEditors.LookUpEdit lkp_DrawerFrom;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.TextEdit txt_ID;
        private System.Windows.Forms.Label label9;
    }
}