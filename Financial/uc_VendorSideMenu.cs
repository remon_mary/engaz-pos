﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace ByStro.Forms.UserControls
{
    public partial class uc_VendorSideMenu : DevExpress.XtraEditors.XtraUserControl
    {
        private int _VendorID;
        public int VendorID
        {
            get
            {
                return _VendorID;
            }
            set
            {
                _VendorID = value;
                ViewBalance();
            }

        }
        void ViewBalance()
        {
            DAL.ERPDataContext db = new DAL.ERPDataContext(Program.setting.con);
            var cus = (from c in db.Pr_Vendors 
                       where c.ID == VendorID 
                       select c).FirstOrDefault();
            if (cus == null) return;
            
          var   VendorBalance = Master.GetAccountBalance(cus.Account);
            accordionControlElement3.Text = LangResource.Balance +": " +  Math.Abs(VendorBalance.Balance).ToString();
            accordionControlElement3.Text  +=" "+( (VendorBalance.Balance >= 0) ? LangResource.Debit : LangResource.Credit);
            

        }
        public uc_VendorSideMenu()
        {
            InitializeComponent(); 
        }


        private void accordionControl1_ExpandStateChanged(object sender, DevExpress.XtraBars.Navigation.ExpandStateChangedEventArgs e)
        {
         
        }
        int LastWidth;

        private void accordionControl1_StateChanged(object sender, EventArgs e)
        {
            if(accordionControl1.OptionsMinimizing.State == DevExpress.XtraBars.Navigation.AccordionControlState.Minimized)
            {
                LastWidth = this.Width; 
                this.Width  = accordionControl1.Width ;
            }
            else
            {
            this.Width    =LastWidth ;

            }
        }

        private void uc_VendorSideMenu_SizeChanged(object sender, EventArgs e)
        {
            if (accordionControl1.OptionsMinimizing.State == DevExpress.XtraBars.Navigation.AccordionControlState.Normal)
            {
                accordionControl1.Width = this.Width; 
            }
        }

        private void accordionControlElement11_Click(object sender, EventArgs e)
        {
            // عرض فواتير المبيعات 
          //  frm_Main.OpenForm(new frm_Pr_PrInvoiceList(VendorID));

        }

        private void accordionControlElement13_Click(object sender, EventArgs e)
        {
           // frm_Main.OpenForm(new frm_Pr_PrInvoice(VendorID, TypeToLoad: "cus"));
        }

        private void accordionControlElement12_Click(object sender, EventArgs e)
        {
            DAL.ERPDataContext db = new DAL.ERPDataContext(Program.setting.con);
            var cus = (from c in db.Pr_Vendors
                       where c.ID == VendorID
                       select c).FirstOrDefault();
            if (cus == null) return;
            frm_Main.OpenForm(new frm_AccountStatment(cus.Account));
            
        }

        private void accordionControlElement7_Click(object sender, EventArgs e)
        {
            frm_Main.OpenForm(new frm_CashNote(false ,VendorID, "Vendor"));

        }

        private void accordionControlElement4_Click(object sender, EventArgs e)
        {
            frm_Main.OpenForm(new frm_CashNote(true, VendorID, "Vendor"));

        }

        private void accordionControlElement6_Click(object sender, EventArgs e)
        {
          //  frm_Main.OpenForm(new frm_Pr_PrInvoiceList(VendorID));

        }

        private void accordionControlElement8_Click(object sender, EventArgs e)
        {
            //frm_Main.OpenForm(new frm_Pr_PrReturnInvoice(VendorID, TypeToLoad: "cus"));

        }
    }
}
