﻿using DevExpress.Data;
using DevExpress.XtraCharts;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace ByStro.Forms
{
    public partial class frm_AccountStatment : frm_Master 
    {
        public frm_AccountStatment(Int64  AccountID = 0 )
        {
            InitializeComponent();
            if (AccountID > 0)
            {
                DAL.ERPDataContext db = new DAL.ERPDataContext(Program.setting.con); 
                lkp_Account.EditValue = db.Acc_Accounts.Where(x=>x.ID == AccountID).Select(x=>x.Number).Single ();

            }

        }
        public override void frm_Load(object sender, EventArgs e)
        {
            base.frm_Load(sender, e);
            btn_Delete.Visibility = btn_New.Visibility = btn_Refresh.Visibility = btn_Save.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            lkp_CostCenter .Properties.DisplayMember = "ID";
            lkp_CostCenter.Properties.ValueMember = "Name";
            Master.RestoreGridLayout(this.gridView_Main, this);
        }
        public override void RunUserPrivilage()
        {
            lkp_Account.Properties.DataSource = CurrentSession.UserAccessbileAccounts.Select(x => new {x.ID , x.ParentID, x.Number, x.Name }).ToList();
            lkp_Account.Properties.ValueMember = "Number";
            lkp_Account.Properties.DisplayMember = "Name";
            lkp_Account.Properties.TreeList.ParentFieldName = "ParentID";
            lkp_Account.Properties.TreeList.KeyFieldName = "ID";
                simpleButton1.PerformClick();


        }
        public override void RefreshData()
        {
            DAL.ERPDataContext db = new DAL.ERPDataContext(Program.setting.con);
            lkp_Account.Properties.DataSource = CurrentSession.UserAccessbileAccounts.Select(x => new { x.ID, x.ParentID, x.Number, x.Name });
        
        }
        public override void Print()
        {
            if (CanPerformPrint() == false) return;
            var reportName = LangResource.AccountStatment + " " + lkp_Account.Text;
            if (dt_From.EditValue != null)
                reportName += " " + LangResource.From + " " + dt_From.Text;
            if (dt_To.EditValue != null)
                reportName += " " + LangResource.To + " " + dt_To.Text;

            Reporting.rpt_GridReport.Print(gridControl_Main, this.Text,
              reportName, (this.RightToLeft == RightToLeft.Yes) ? true : false);
            base.Print();




        }
        void ViewStatment()
        {
            if (lkp_Account.EditValue == null) return;
            DAL.ERPDataContext db = new DAL.ERPDataContext(Program.setting.con);
            
            var list = from l in db.Acc_Activities
                       from d in db.Acc_ActivityDetials.Where(x => x.AcivityID == l.ID)
                       from ac in db.Acc_Accounts .Where(x=>x.ID  == d.ACCID )
                       from cs in db.Acc_CostCenters .Where(x=>x.ID == l.CostCEnter ).DefaultIfEmpty()
                     
                       orderby l.Date  ascending
                       select new
                       {
                           ACCID = ac.Number,
                           AccountName = ac.Name ,
                           l.Date ,
                           Source = l.Type,
                           SourceID= l.TypeID,
                           l.Note,
                           CoastCenter =  cs.Name,
                           d.Debit,
                           d.Credit,
                           Balance =
                           (from sa in db.Acc_Activities
                            from sd in db.Acc_ActivityDetials.Where(x => x.AcivityID == sa.ID)
                            where sa.Date <= l.Date  && sd.ACCID == d.ACCID 
                            select sd.Debit - sd.Credit).ToList().Sum()
                       } ;
            if (dt_From.EditValue != null)
                list = list.Where(x => x.Date >= dt_From.DateTime.Date);
            if (dt_To .EditValue != null)
                list = list.Where(x => x.Date <= dt_To.DateTime.Date);
             
                list = list.Where(x => x.ACCID.ToString().StartsWith(lkp_Account.EditValue.ToString()));
          
            var _DataSource = list.ToList();

            if (dt_From.EditValue != null)
            {
                var OpenBalanceFromDay = new
                {
                    ACCID =  "0",
                    AccountName = "",
                    Date = dt_From.DateTime.Date,
                    Source = "",
                    SourceID = "",
                    Note = LangResource.OpenBalance,
                    CoastCenter = "",
                    Debit = (double)0,
                    Credit = (double)0,
                    Balance = (double)
              (from a in db.Acc_Activities
               from d in db.Acc_ActivityDetials.Where(x => x.AcivityID == a.ID)
               where a.Date.Date <= dt_From.DateTime.Date && d.ACCID.ToString() == lkp_Account.EditValue.ToString()
               select d.Debit - d.Credit).ToList().Sum()
                };

                _DataSource.Add(OpenBalanceFromDay); 
            }
            gridControl_Main.DataSource = _DataSource.OrderBy(x => x.Date).ToList(); 

          
            gridView_Main.Columns["AccountName"].Caption = LangResource.AccountName;
            gridView_Main.Columns["Credit"].Caption = LangResource.Credit ;
            gridView_Main.Columns["Debit"].Caption = LangResource.Debit ;
            gridView_Main.Columns["Date"].Caption = LangResource.Date ;
            gridView_Main.Columns["Source"].Caption = LangResource.Source ;
            gridView_Main.Columns["SourceID"].Caption = LangResource.SourceCode;
            gridView_Main.Columns["Note"].Caption = LangResource.Statment ;
            gridView_Main.Columns["CoastCenter"].Caption = LangResource.CostCenter ;
            gridView_Main.Columns["Balance"].Caption = LangResource.Balance ;



            RepositoryItemLookUpEdit StatmentRepo = new RepositoryItemLookUpEdit();
            RepositoryItemMemoEdit RepoMemo = new RepositoryItemMemoEdit();

            DataTable ProssessDT = new DataTable();
            ProssessDT.Columns.Add("ID");
            ProssessDT.Columns.Add("Name");

            for (int i = 0; i < Master.Prossess.Count(); i++)
                ProssessDT.Rows.Add(i, Master.Prossess[i]);
            StatmentRepo.DataSource = ProssessDT;
            StatmentRepo.DisplayMember = "Name";
            StatmentRepo.ValueMember = "ID";


            gridView_Main.Columns["ACCID"].Visible = 
            gridView_Main.Columns["ACCID"].OptionsColumn.ShowInCustomizationForm = false; 
            gridView_Main.Columns["Source"].ColumnEdit = StatmentRepo; 
            gridView_Main.Columns["Note"].ColumnEdit = RepoMemo; 
            gridView_Main.Columns["Date"].DisplayFormat.FormatString = "yyyy-MM-dd hh:mm tt";
            gridView_Main.Columns["Debit"].Summary.Clear();
            gridView_Main.Columns["Credit"].Summary.Clear();
            gridView_Main.Columns["Note"].Summary.Clear(); 
            gridView_Main.Columns["Debit"].Summary.Add(SummaryItemType.Sum, "Debit", "{0:n2}");
            gridView_Main.Columns["Credit"].Summary.Add(SummaryItemType.Sum, "Credit", "{0:n2}");
       
            var balance = Master.GetAccountBalance(Convert.ToInt64(lkp_Account.EditValue));
            if (balance.Balance  >= 0)
                gridView_Main.Columns["Note"].Summary.Add(SummaryItemType.Sum, "Note", LangResource.ThisAccountDebitWith +  " "+Math.Abs(balance.Balance));
            else
                gridView_Main.Columns["Note"].Summary.Add(SummaryItemType.Sum, "Note", LangResource.ThisAccountCreditWith + " " + Math.Abs(balance.Balance));

            //
            var History = (from l in db.Acc_Activities
                           join d in db.Acc_ActivityDetials on l.ID equals d.AcivityID
                           where d.ACCID.ToString() == lkp_Account .EditValue.ToString()  
                           group l  by l.Date .Date into g  
                           select new { Date = g.Key.Date.Date,
                               Balance = (from a in db.Acc_Activities
                                                                      from sd in db.Acc_ActivityDetials.Where(x => x.AcivityID == a.ID)
                                                                      where a.Date.Date <= g.Key.Date.Date && sd.ACCID.ToString() == lkp_Account.EditValue.ToString()
                                                                      select sd.Debit - sd.Credit).ToList().Sum()
                           });

            Series DebitSeries = new Series(LangResource.Balance , ViewType.Line);
            DebitSeries.ArgumentScaleType = ScaleType.DateTime;
            DebitSeries.ArgumentDataMember = "Date";
            DebitSeries.ValueScaleType = ScaleType.Numerical;
            DebitSeries.ValueDataMembers.AddRange(new string[] { "Balance" });
            DebitSeries.DataSource = History.ToList();
             
            chartControl1.Series.Clear();
            chartControl1.Series.Add(DebitSeries);
          
            ((XYDiagram)chartControl1.Diagram).AxisY.Visibility = DevExpress.Utils.DefaultBoolean.True;
            ((XYDiagram)chartControl1.Diagram).AxisX.Visibility = DevExpress.Utils.DefaultBoolean.True;
            chartControl1.Legend.Visibility = DevExpress.Utils.DefaultBoolean.True;
            chartControl1.AnimationStartMode = ChartAnimationMode.OnDataChanged;
            XYDiagram diagram = (XYDiagram)chartControl1.Diagram;
            diagram.AxisX.VisualRange.Auto = false;
            diagram.AxisX.VisualRange.SetMinMaxValues(((DateTime)diagram.AxisX.WholeRange.MaxValue).AddDays(-30), diagram.AxisX.WholeRange.MaxValue);
            diagram.EnableAxisXScrolling = true;
            diagram.EnableAxisXZooming = true;
            Master.RestoreGridLayout(this.gridView_Main, this);

            //


        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            ViewStatment(); 
        }

        private void gridView1_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.FieldName == "Balance")
            {
                if (Convert.ToDouble(e.Value) < 0)
                    e.DisplayText =Math.Abs(Convert.ToDouble(e.Value)).ToString()+ " " +   LangResource.Credit;
                else
                    e.DisplayText =  Math.Abs( Convert.ToDouble(e.Value)).ToString()+ " " + LangResource.Debit;
            }
            if (e.Column.FieldName == "Debit" || e.Column.FieldName == "Credit")
            {

                if (e.Value.GetType() == typeof(double) && Convert.ToDouble(e.Value) == 0)
                {
                    e.DisplayText = ""; 
                }
            }
        }

        private void gridView_Main_DoubleClick(object sender, EventArgs e)
        {
            GridView view = sender as GridView;
            if (view == null || view.FocusedRowHandle < 0 ||
                view.GetFocusedRowCellValue("SourceID") == null ||
                view.GetFocusedRowCellValue("SourceID") == DBNull.Value) return;
            Master.OpenByProssess(Convert.ToInt32(view.GetFocusedRowCellValue("Source")), Convert.ToInt32(view.GetFocusedRowCellValue("SourceID")));

        }

   
        private void frm_AccountStatment_FormClosing(object sender, FormClosingEventArgs e)
        {
            Master.SaveGridLayout (this.gridView_Main, this);
        }

        private void gridView_Main_CustomSummaryCalculate(object sender, CustomSummaryEventArgs e)
        { 
        }

        private void gridView_Main_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            if (e.Column.FieldName == "Debit" || e.Column.FieldName == "Credit")
            {
               

                ColumnView columnView = sender as ColumnView;
                int h = e.RowHandle;
                GridView view = sender as GridView; 
                if (e.RowHandle < 0) return;

                if (view.GetRowCellValue(e.RowHandle, "Debit") != null &&
                  view.GetRowCellValue(e.RowHandle, "Debit") != DBNull.Value &&
                  view.GetRowCellValue(e.RowHandle, "Credit") != null &&
                 view.GetRowCellValue(e.RowHandle, "Credit") != DBNull.Value &&
                 Convert.ToDouble(view.GetRowCellValue(e.RowHandle, "Debit")) == 0 &&
                 Convert.ToDouble(view.GetRowCellValue(e.RowHandle, "Credit")) == 0)

                    return;
                if (view.GetRowCellValue(e.RowHandle, "Debit") != null &&
                    view.GetRowCellValue(e.RowHandle, "Debit") != DBNull.Value)
                {
                    e.Appearance.ForeColor = Color.WhiteSmoke; 
                    if (Convert.ToDouble(view.GetRowCellValue(e.RowHandle, "Debit")) > 0)
                        e.Appearance.BackColor = Color.FromArgb(76, 175, 80);
                    else
                        e.Appearance.BackColor = Color.FromArgb(239, 83, 80); 
                }
                else if (view.GetRowCellValue(e.RowHandle, "Credit") != null &&
                 view.GetRowCellValue(e.RowHandle, "Credit") != DBNull.Value)
                {

                    if (Convert.ToDouble(view.GetRowCellValue(e.RowHandle, "Credit")) > 0)
                    {
                        e.Appearance.BackColor = Color.FromArgb(239, 83, 80);
                        e.Appearance.ForeColor = Color.WhiteSmoke; 
                    }
                }
            }
            else if (e.Column.FieldName == "Balance")
            {
                e.Appearance.ForeColor = Color.WhiteSmoke;
                if (Convert.ToDouble(e.CellValue) > 0)
                    e.Appearance.BackColor = Color.FromArgb(76, 175, 80);
                else if (Convert.ToDouble(e.CellValue) < 0)
                    e.Appearance.BackColor = Color.FromArgb(239, 83, 80);

            }

        }

        private void gridView_Main_ColumnWidthChanged(object sender, ColumnEventArgs e)
        {
            Master.SaveGridLayout(this.gridView_Main, this);

        }

        private void gridView_Main_ColumnPositionChanged(object sender, EventArgs e)
        {
            Master.SaveGridLayout(this.gridView_Main, this);

        }
    }
}
