﻿namespace ByStro.Forms
{
    partial class frm_AccountStatment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AccountStatment));
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.dt_To = new DevExpress.XtraEditors.DateEdit();
            this.dt_From = new DevExpress.XtraEditors.DateEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.lkp_CustomeMenu = new DevExpress.XtraEditors.LookUpEdit();
            this.lkp_CostCenter = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.gridControl_Main = new DevExpress.XtraGrid.GridControl();
            this.gridView_Main = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.chartControl1 = new DevExpress.XtraCharts.ChartControl();
            this.lkp_Account = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeListLookUpEdit1TreeList = new DevExpress.XtraTreeList.TreeList();
            ((System.ComponentModel.ISupportInitialize)(this.lkp_List)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt_To.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt_To.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt_From.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt_From.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkp_CustomeMenu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkp_CostCenter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl_Main)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView_Main)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkp_Account.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).BeginInit();
            this.SuspendLayout();
            // 
            // lkp_List
            // 
            resources.ApplyResources(this.lkp_List, "lkp_List");
            // 
            // SubItem_ConvertTo
            // 
            resources.ApplyResources(this.SubItem_ConvertTo, "SubItem_ConvertTo");
            this.SubItem_ConvertTo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("SubItem_ConvertTo.ImageOptions.Image")));
            this.SubItem_ConvertTo.ImageOptions.ImageIndex = ((int)(resources.GetObject("SubItem_ConvertTo.ImageOptions.ImageIndex")));
            this.SubItem_ConvertTo.ImageOptions.LargeImageIndex = ((int)(resources.GetObject("SubItem_ConvertTo.ImageOptions.LargeImageIndex")));
            this.SubItem_ConvertTo.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("SubItem_ConvertTo.ImageOptions.SvgImage")));
            this.SubItem_ConvertTo.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btn_ConvertToOrder),
            new DevExpress.XtraBars.LinkPersistInfo(this.btn_ConvertToOrder),
            new DevExpress.XtraBars.LinkPersistInfo(this.btn_ConvertToOrder),
            new DevExpress.XtraBars.LinkPersistInfo(this.btn_ConvertToOrder),
            new DevExpress.XtraBars.LinkPersistInfo(this.btn_ConvertToOrder),
            new DevExpress.XtraBars.LinkPersistInfo(this.btn_ConvertToOrder),
            new DevExpress.XtraBars.LinkPersistInfo(this.btn_ConvertToOrder),
            new DevExpress.XtraBars.LinkPersistInfo(this.btn_ConvertToOrder)});
            // 
            // SubItem_Printbills
            // 
            resources.ApplyResources(this.SubItem_Printbills, "SubItem_Printbills");
            this.SubItem_Printbills.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("SubItem_Printbills.ImageOptions.Image")));
            this.SubItem_Printbills.ImageOptions.ImageIndex = ((int)(resources.GetObject("SubItem_Printbills.ImageOptions.ImageIndex")));
            this.SubItem_Printbills.ImageOptions.LargeImageIndex = ((int)(resources.GetObject("SubItem_Printbills.ImageOptions.LargeImageIndex")));
            this.SubItem_Printbills.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("SubItem_Printbills.ImageOptions.SvgImage")));
            // 
            // labelControl1
            // 
            resources.ApplyResources(this.labelControl1, "labelControl1");
            this.labelControl1.Name = "labelControl1";
            // 
            // labelControl2
            // 
            resources.ApplyResources(this.labelControl2, "labelControl2");
            this.labelControl2.Name = "labelControl2";
            // 
            // labelControl3
            // 
            resources.ApplyResources(this.labelControl3, "labelControl3");
            this.labelControl3.Name = "labelControl3";
            // 
            // dt_To
            // 
            resources.ApplyResources(this.dt_To, "dt_To");
            this.dt_To.MenuManager = this.barManager1;
            this.dt_To.Name = "dt_To";
            this.dt_To.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("dt_To.Properties.Buttons"))))});
            this.dt_To.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("dt_To.Properties.CalendarTimeProperties.Buttons"))))});
            this.dt_To.Properties.CalendarTimeProperties.Mask.EditMask = resources.GetString("dt_To.Properties.CalendarTimeProperties.Mask.EditMask");
            this.dt_To.Properties.CalendarTimeProperties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.dt_To.Properties.Mask.EditMask = resources.GetString("dt_To.Properties.Mask.EditMask");
            this.dt_To.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            // 
            // dt_From
            // 
            resources.ApplyResources(this.dt_From, "dt_From");
            this.dt_From.MenuManager = this.barManager1;
            this.dt_From.Name = "dt_From";
            this.dt_From.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("dt_From.Properties.Buttons"))))});
            this.dt_From.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("dt_From.Properties.CalendarTimeProperties.Buttons"))))});
            this.dt_From.Properties.CalendarTimeProperties.Mask.EditMask = resources.GetString("dt_From.Properties.CalendarTimeProperties.Mask.EditMask");
            this.dt_From.Properties.CalendarTimeProperties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.dt_From.Properties.Mask.EditMask = resources.GetString("dt_From.Properties.Mask.EditMask");
            this.dt_From.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            // 
            // simpleButton1
            // 
            resources.ApplyResources(this.simpleButton1, "simpleButton1");
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // lkp_CustomeMenu
            // 
            resources.ApplyResources(this.lkp_CustomeMenu, "lkp_CustomeMenu");
            this.lkp_CustomeMenu.Name = "lkp_CustomeMenu";
            this.lkp_CustomeMenu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("lkp_CustomeMenu.Properties.Buttons"))))});
            this.lkp_CustomeMenu.Properties.NullText = resources.GetString("lkp_CustomeMenu.Properties.NullText");
            this.lkp_CustomeMenu.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            // 
            // lkp_CostCenter
            // 
            resources.ApplyResources(this.lkp_CostCenter, "lkp_CostCenter");
            this.lkp_CostCenter.Name = "lkp_CostCenter";
            this.lkp_CostCenter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("lkp_CostCenter.Properties.Buttons"))))});
            this.lkp_CostCenter.Properties.NullText = resources.GetString("lkp_CostCenter.Properties.NullText");
            this.lkp_CostCenter.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            // 
            // labelControl5
            // 
            resources.ApplyResources(this.labelControl5, "labelControl5");
            this.labelControl5.Name = "labelControl5";
            // 
            // labelControl7
            // 
            resources.ApplyResources(this.labelControl7, "labelControl7");
            this.labelControl7.Name = "labelControl7";
            // 
            // gridControl_Main
            // 
            resources.ApplyResources(this.gridControl_Main, "gridControl_Main");
            this.gridControl_Main.EmbeddedNavigator.AccessibleDescription = resources.GetString("gridControl_Main.EmbeddedNavigator.AccessibleDescription");
            this.gridControl_Main.EmbeddedNavigator.AccessibleName = resources.GetString("gridControl_Main.EmbeddedNavigator.AccessibleName");
            this.gridControl_Main.EmbeddedNavigator.AllowHtmlTextInToolTip = ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("gridControl_Main.EmbeddedNavigator.AllowHtmlTextInToolTip")));
            this.gridControl_Main.EmbeddedNavigator.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("gridControl_Main.EmbeddedNavigator.Anchor")));
            this.gridControl_Main.EmbeddedNavigator.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("gridControl_Main.EmbeddedNavigator.BackgroundImage")));
            this.gridControl_Main.EmbeddedNavigator.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("gridControl_Main.EmbeddedNavigator.BackgroundImageLayout")));
            this.gridControl_Main.EmbeddedNavigator.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("gridControl_Main.EmbeddedNavigator.ImeMode")));
            this.gridControl_Main.EmbeddedNavigator.MaximumSize = ((System.Drawing.Size)(resources.GetObject("gridControl_Main.EmbeddedNavigator.MaximumSize")));
            this.gridControl_Main.EmbeddedNavigator.TextLocation = ((DevExpress.XtraEditors.NavigatorButtonsTextLocation)(resources.GetObject("gridControl_Main.EmbeddedNavigator.TextLocation")));
            this.gridControl_Main.EmbeddedNavigator.ToolTip = resources.GetString("gridControl_Main.EmbeddedNavigator.ToolTip");
            this.gridControl_Main.EmbeddedNavigator.ToolTipIconType = ((DevExpress.Utils.ToolTipIconType)(resources.GetObject("gridControl_Main.EmbeddedNavigator.ToolTipIconType")));
            this.gridControl_Main.EmbeddedNavigator.ToolTipTitle = resources.GetString("gridControl_Main.EmbeddedNavigator.ToolTipTitle");
            this.gridControl_Main.MainView = this.gridView_Main;
            this.gridControl_Main.MenuManager = this.barManager1;
            this.gridControl_Main.Name = "gridControl_Main";
            this.gridControl_Main.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView_Main});
            // 
            // gridView_Main
            // 
            this.gridView_Main.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView_Main.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView_Main.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView_Main.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView_Main.AppearancePrint.FooterPanel.Options.UseTextOptions = true;
            this.gridView_Main.AppearancePrint.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.gridView_Main.AppearancePrint.HeaderPanel.Options.UseTextOptions = true;
            this.gridView_Main.AppearancePrint.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            resources.ApplyResources(this.gridView_Main, "gridView_Main");
            this.gridView_Main.FooterPanelHeight = 45;
            this.gridView_Main.GridControl = this.gridControl_Main;
            this.gridView_Main.Name = "gridView_Main";
            this.gridView_Main.OptionsBehavior.Editable = false;
            this.gridView_Main.OptionsPrint.AllowMultilineHeaders = true;
            this.gridView_Main.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.gridView_Main.OptionsView.RowAutoHeight = true;
            this.gridView_Main.OptionsView.ShowFooter = true;
            this.gridView_Main.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView_Main_RowCellStyle);
            this.gridView_Main.ColumnWidthChanged += new DevExpress.XtraGrid.Views.Base.ColumnEventHandler(this.gridView_Main_ColumnWidthChanged);
            this.gridView_Main.CustomSummaryCalculate += new DevExpress.Data.CustomSummaryEventHandler(this.gridView_Main_CustomSummaryCalculate);
            this.gridView_Main.ColumnPositionChanged += new System.EventHandler(this.gridView_Main_ColumnPositionChanged);
            this.gridView_Main.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(this.gridView1_CustomColumnDisplayText);
            this.gridView_Main.DoubleClick += new System.EventHandler(this.gridView_Main_DoubleClick);
            // 
            // chartControl1
            // 
            resources.ApplyResources(this.chartControl1, "chartControl1");
            this.chartControl1.BorderOptions.Visibility = DevExpress.Utils.DefaultBoolean.False;
            this.chartControl1.Legend.Name = "Default Legend";
            this.chartControl1.Name = "chartControl1";
            this.chartControl1.SeriesSerializable = new DevExpress.XtraCharts.Series[0];
            // 
            // lkp_Account
            // 
            resources.ApplyResources(this.lkp_Account, "lkp_Account");
            this.lkp_Account.MenuManager = this.barManager1;
            this.lkp_Account.Name = "lkp_Account";
            this.lkp_Account.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("lkp_Account.Properties.Buttons"))))});
            this.lkp_Account.Properties.NullText = resources.GetString("lkp_Account.Properties.NullText");
            this.lkp_Account.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.lkp_Account.Properties.TreeList = this.treeListLookUpEdit1TreeList;
            // 
            // treeListLookUpEdit1TreeList
            // 
            resources.ApplyResources(this.treeListLookUpEdit1TreeList, "treeListLookUpEdit1TreeList");
            this.treeListLookUpEdit1TreeList.Name = "treeListLookUpEdit1TreeList";
            this.treeListLookUpEdit1TreeList.OptionsView.ShowIndentAsRowStyle = true;
            // 
            // frm_AccountStatment
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lkp_Account);
            this.Controls.Add(this.chartControl1);
            this.Controls.Add(this.gridControl_Main);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.dt_From);
            this.Controls.Add(this.dt_To);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.lkp_CostCenter);
            this.Controls.Add(this.lkp_CustomeMenu);
            this.Name = "frm_AccountStatment";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_AccountStatment_FormClosing);
            this.Controls.SetChildIndex(this.lkp_CustomeMenu, 0);
            this.Controls.SetChildIndex(this.lkp_CostCenter, 0);
            this.Controls.SetChildIndex(this.labelControl1, 0);
            this.Controls.SetChildIndex(this.labelControl2, 0);
            this.Controls.SetChildIndex(this.labelControl5, 0);
            this.Controls.SetChildIndex(this.labelControl7, 0);
            this.Controls.SetChildIndex(this.labelControl3, 0);
            this.Controls.SetChildIndex(this.dt_To, 0);
            this.Controls.SetChildIndex(this.dt_From, 0);
            this.Controls.SetChildIndex(this.simpleButton1, 0);
            this.Controls.SetChildIndex(this.gridControl_Main, 0);
            this.Controls.SetChildIndex(this.chartControl1, 0);
            this.Controls.SetChildIndex(this.lkp_Account, 0);
            ((System.ComponentModel.ISupportInitialize)(this.lkp_List)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt_To.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt_To.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt_From.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt_From.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkp_CustomeMenu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkp_CostCenter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl_Main)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView_Main)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkp_Account.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.DateEdit dt_To;
        private DevExpress.XtraEditors.DateEdit dt_From;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.LookUpEdit lkp_CustomeMenu;
        private DevExpress.XtraEditors.LookUpEdit lkp_CostCenter;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraGrid.GridControl gridControl_Main;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView_Main;
        private DevExpress.XtraCharts.ChartControl chartControl1;
        private DevExpress.XtraEditors.TreeListLookUpEdit lkp_Account;
        private DevExpress.XtraTreeList.TreeList treeListLookUpEdit1TreeList;
    }
}