﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Filtering.Templates;
using DevExpress.Utils.Svg; 
using Microsoft.SqlServer.Management.Sdk.Sfc;
using ByStro.DAL;
using ByStro.Forms;

namespace ByStro.UControle 
{
    public partial class uc_Administrative_Report : DevExpress.XtraEditors.XtraUserControl
    {
        public uc_Administrative_Report()
        {
            InitializeComponent();

        }

        void RefreshTiles()
        {
            TileItemInstallmentsDue.Text = GetItemsExudedMaxLevelCount().ToString();
            TileItemProductsWithLowBalance.Text  = ProductReachedReorderLevelCount().ToString();
        }
        // ;
        public static int GetItemsExudedMaxLevelCount()
        {


            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
            var instalDetails = db.Instalment_Details.Where(x => x.IsPaid == false);
            var dateDilter = DateTime.Now;
            dateDilter = DateTime.Now.AddDays(Convert.ToInt32(30));
            instalDetails = instalDetails.Where(x => x.DueDate <= dateDilter);
            var count = instalDetails.Count();
            return count;

        }
        public static int ProductReachedReorderLevelCount()
        {


            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
            var instalDetails = db.Inv_StoreLogs.OrderBy(isl => isl.date).GroupBy(isl => new { isl.ItemID, isl.Color, isl.Size, isl.StoreID })
                .Select(isl => new { 
                    Balance = isl.Sum(i => i.ItemQuIN) - isl.Sum(i => i.ItemQuOut),
                    ReOrderLevel = (db.Prodecuts.Single(p => p.ProdecutID == isl.Key.ItemID).RequestLimit ?? 0), 
                }).Where(x => x.Balance <= x.ReOrderLevel); 
            var count = instalDetails.Count();
            return count;

        }



        TileItemElement TileItemInstallmentsDue; 
        TileItemElement TileItemProductsWithLowBalance;
        private void uc_Administrative_Report_Load(object sender, EventArgs e)
        {
            TileItemInstallmentsDue = AddTileItem(tileGroup2, "اقساط مستحقه خلال 30 يوم ", "قسط", ColorFromHexa("#388E3C"),
            null, (ss, ee) => { new Forms.frm_PayInstallmentList(false, 30).Show(); });
            TileItemProductsWithLowBalance = AddTileItem(tileGroup2, "اصناف وصلت لحد الطلب", "صنف", ColorFromHexa("#283593"),
            null, (ss, ee) => {
              var frm =   new frm_EditReportFilters(frm_Report.ReportType.ProductReachedReorderLevel);
                frm.Show();
                frm.simpleButton1.PerformClick();
                frm.Close();
            });

            RefreshTiles();
        }

        private void bbiRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RefreshTiles();
        } 

        int tileID = 0;
        TileItemElement AddTileItem(TileGroup group, string Titel, string SubCaption, Color color, SvgImage svg, TileItemClickEventHandler args)
        {
            TileItem tile = new TileItem();
            TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            tile.AppearanceItem.Normal.BackColor = color;// System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(163)))), ((int)(((byte)(10)))));
            tile.AppearanceItem.Normal.Options.UseBackColor = true;
            tileItemElement1.AnchorIndent = 5;
            tileItemElement1.Appearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement1.Appearance.Hovered.Options.UseFont = true;
            tileItemElement1.Appearance.Hovered.Options.UseTextOptions = true;
            tileItemElement1.Appearance.Hovered.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement1.Appearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            tileItemElement1.Appearance.Normal.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileItemElement1.Appearance.Normal.Options.UseFont = true;
            tileItemElement1.Appearance.Normal.Options.UseTextOptions = true;
            tileItemElement1.Appearance.Normal.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement1.Appearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            tileItemElement1.Appearance.Selected.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement1.Appearance.Selected.Options.UseFont = true;
            tileItemElement1.Appearance.Selected.Options.UseTextOptions = true;
            tileItemElement1.Appearance.Selected.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement1.Appearance.Selected.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            tileItemElement1.ImageOptions.ImageSize = new System.Drawing.Size(120, 120);
            tileItemElement1.ImageOptions.SvgImage = svg;
            tileItemElement1.ImageOptions.SvgImageColorizationMode = DevExpress.Utils.SvgImageColorizationMode.CommonPalette;
            tileItemElement1.ImageOptions.SvgImageSize = new System.Drawing.Size(64, 64);
            tileItemElement1.MaxWidth = 145;
            tileItemElement1.Text = Titel;
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.TopLeft;
            tileItemElement1.TextLocation = new System.Drawing.Point(5, 5);
            tileItemElement1.Width = 250;
            tileItemElement2.Appearance.Hovered.Font = new System.Drawing.Font("Segoe UI Light", 49F);
            tileItemElement2.Appearance.Hovered.Options.UseFont = true;
            tileItemElement2.Appearance.Hovered.Options.UseTextOptions = true;
            tileItemElement2.Appearance.Hovered.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            tileItemElement2.Appearance.Hovered.TextOptions.Trimming = DevExpress.Utils.Trimming.Character;
            tileItemElement2.Appearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement2.Appearance.Normal.Font = new System.Drawing.Font("Segoe UI Light", 49F);
            tileItemElement2.Appearance.Normal.Options.UseFont = true;
            tileItemElement2.Appearance.Normal.Options.UseTextOptions = true;
            tileItemElement2.Appearance.Normal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            tileItemElement2.Appearance.Normal.TextOptions.Trimming = DevExpress.Utils.Trimming.Character;
            tileItemElement2.Appearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement2.Appearance.Selected.Font = new System.Drawing.Font("Segoe UI Light", 49F);
            tileItemElement2.Appearance.Selected.Options.UseFont = true;
            tileItemElement2.Appearance.Selected.Options.UseTextOptions = true;
            tileItemElement2.Appearance.Selected.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            tileItemElement2.Appearance.Selected.TextOptions.Trimming = DevExpress.Utils.Trimming.Character;
            tileItemElement2.Appearance.Selected.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement2.MaxWidth = 100;
            tileItemElement2.Text = "0";
            tileItemElement2.TextAlignment = TileItemContentAlignment.TopRight;
            tileItemElement2.TextLocation = new System.Drawing.Point(0, -15);
            tileItemElement3.Appearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement3.Appearance.Hovered.Options.UseFont = true;
            tileItemElement3.Appearance.Hovered.Options.UseTextOptions = true;
            tileItemElement3.Appearance.Hovered.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            tileItemElement3.Appearance.Hovered.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement3.Appearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement3.Appearance.Normal.Font = new Font("Segoe UI", 9F);
            tileItemElement3.Appearance.Normal.Options.UseFont = true;
            tileItemElement3.Appearance.Normal.Options.UseTextOptions = true;
            tileItemElement3.Appearance.Normal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            tileItemElement3.Appearance.Normal.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement3.Appearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement3.Appearance.Selected.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement3.Appearance.Selected.Options.UseFont = true;
            tileItemElement3.Appearance.Selected.Options.UseTextOptions = true;
            tileItemElement3.Appearance.Selected.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            tileItemElement3.Appearance.Selected.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement3.Appearance.Selected.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement3.MaxWidth = 70;
            tileItemElement3.Text = SubCaption;
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleRight;
            tileItemElement3.TextLocation = new System.Drawing.Point(-3, 15);
            tile.Elements.Add(tileItemElement1);
            tile.Elements.Add(tileItemElement2);
            tile.Elements.Add(tileItemElement3);
            tile.Id = tileID;
            tile.ItemSize = TileItemSize.Wide;
            tile.Name = "tileItem" + tileID;
            tile.ItemClick += args;

            group.Items.Add(tile);
            return tileItemElement2;


        }
        public static Color ColorFromHexa(string hexCode) =>   System.Drawing.ColorTranslator.FromHtml(hexCode);

        private void uc_Administrative_Report_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void uc_Administrative_Report_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.F5)
            {
                RefreshTiles();
            }
        }
    }
}
