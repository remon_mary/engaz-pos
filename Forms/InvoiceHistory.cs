﻿using ByStro.Clases;
using ByStro.PL;
using DevExpress.Utils;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByStro.Forms
{
    public class InvoiceHistory : FormLayoutList
    {
        public InvoiceHistory() : base("ID") { }
        DAL.Acc_Drawer Acc_Drawer;
        public override void PopulateColumns()
        {
            base.PopulateColumns();
            //gridView1.CustomColumn(nameof(Acc_Drawer.ID), "م", true, false);
            //gridView1.CustomColumn(nameof(Acc_Drawer.Name), "الاسم", true, false);
            //gridView1.CustomColumn(nameof(Acc_Drawer.ACCID), "رقم الحساب", true, false);
            //gridView1.CustomColumn(nameof(Acc_Drawer.Notes), "ملاحظات", true, false);
        }
        List<Element> screens = new List<Element>();
        public override void RefreshData()
        {
            string[] names = 
                {
                MasterClass.InvoiceType.SalesInvoice.ToString() ,
                MasterClass.InvoiceType.SalesReturn.ToString() ,
                MasterClass.InvoiceType.PurchaseInvoice.ToString() ,
                MasterClass.InvoiceType.PurchaseReturn.ToString() ,
            };
            screens = Screens.GetScreens.Where(x => names.Contains( x.Name)).ToList();
            if (screens == null) return;
            using (DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String))
            {
                var data = db.UserLogs.Where(x => screens.Select(s => s.Id).Contains(x.ScreenID)).OrderByDescending(x => x.Createdat);
                this.gridControl1.DataSource  = (from d in data
                                                  join u in db.UserPermissions on d.UserID equals u.ID
                                                  select new
                                                  {
                                                      d.PartID,
                                                      d.ScreenID,
                                                      //   ,
                                                      d.PartName,                                                      
                                                      u.UserName,
                                                      d.Createdat,
                                                      Action = d.ActionType == (byte)ActionType.Add ? "اضافة" :
                                                 d.ActionType == (byte)ActionType.Edite ? "تعديل" :
                                                 d.ActionType == (byte)ActionType.Delete ? "حذف" : "طباعة"
                                                  }).ToList();
            }
            this.gridView1.PopulateColumns();
            if (this.gridView1.Columns.Count == 6)
            {
                gridView1.Columns["PartID"].Caption = "م";
                gridView1.Columns["ScreenID"].Caption = "الشاشة";
                gridView1.Columns["PartName"].Caption = "طرف التعامل";
                gridView1.Columns["UserName"].Caption = "الاسم";
                gridView1.Columns["Createdat"].Caption = "التاريخ والوقت";
                gridView1.Columns["Createdat"].DisplayFormat.FormatType = FormatType.Custom;
                gridView1.Columns["Createdat"].DisplayFormat.FormatString = "yyyy-MM-dd hh:mm tt";
                gridView1.Columns["Action"].Caption = "الحركة";
                gridView1.RowStyle += GridViewUserLogs_RowStyle;
                gridView1.CustomColumnDisplayText += GridView1_CustomColumnDisplayText;
            }
            
            base.RefreshData();
        }

        private void GridView1_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.FieldName == "ScreenID"&& e.Value!=null)
            {
                e.DisplayText = screens.Single(s => s.Id == (int)e.Value).Caption;
            }
        }

        private void GridViewUserLogs_RowStyle(object sender, RowStyleEventArgs e)
        {
            var view = sender as GridView;
            string Action = view.GetRowCellValue(e.RowHandle, "Action")?.ToString();
            if (string.IsNullOrEmpty(Action)) return;
            switch (Action)
            {
                case "اضافة":
                    e.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Success;
                    break;
                case "تعديل":
                    e.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Warning;
                    break;
                case "حذف":
                    e.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Danger;
                    break;
                case "طباعة":
                    e.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Primary;
                    break;
            }
        }

        public override void OpenForm(int id)
        {
            //Main_frm.OpenForm(new frm_Drawer(id), true);
            //base.OpenForm(id);
            //RefreshData();
        }
        public override void New()
        {
            //Main_frm.OpenForm(new frm_Drawer(), true);
            //base.New();
            //RefreshData();
        }
    }
}
