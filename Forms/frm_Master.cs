﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ByStro.Clases;
using DevExpress.Utils;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;

namespace ByStro.Forms
{
    public partial class frm_Master : XtraForm 
    {
        internal virtual bool IsNew { get; set; }
        internal bool ChangesMade;
        public frm_Master()
        {
            InitializeComponent();
          
        }
        public bool CanSave() => true;
        public bool CanPerformDelete() => true;
        public bool AllowDelete => true;
        public bool CanAdd => true;
        public string PartNumber;
        public string PartName;
        public int PartID;

        public virtual Master.SystemProssess ProcessType { get; }
        public enum ActionType
        {
            Add,Edite, Delete, Print
        }
        public virtual void Save()
        {
            InsertUserLog(IsNew? ActionType.Add: ActionType.Edite,this.Name,this.PartID,this.PartName );
            MasterClass.Saved();
            ChangesMade = false;
            IsNew = false;
            RefreshData();
            if (PrintAfterSave == true)
                Print();
            GetHistory();

        }
        public static void InsertUserLog(ActionType actionType,string Screen, int partID,string partName)
        {
            if (partID > 0)
            {
                var screen = Screens.GetScreens.SingleOrDefault(x=>x.Name== Screen);
                if (screen == null) return;
                using (DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String))
                {
                    db.UserLogs.InsertOnSubmit(new DAL.UserLog
                    {
                        PartID = partID,
                        PartName = partName,
                        UserID = CurrentSession.User.ID,
                        ActionType = (byte)actionType,
                        Createdat = DateTime.UtcNow,
                        ScreenID = screen.Id,

                    });
                    db.SubmitChanges();
}
            }
        }
        public virtual void GetHistory()
        {

        }
        public virtual void GetData()
        {
            GetHistory();
        }
        public virtual void SetData()
        {

        }  
        public virtual void New()
        {
            IsNew = true;

        }
        public virtual void Delete()
        {
            InsertUserLog(ActionType.Delete, this.Name, this.PartID, this.PartName);
            GetHistory();
        }
        public virtual void OpenList()
        {

        }
        public virtual void Print()
        {
            InsertUserLog( ActionType.Print, this.Name, this.PartID, this.PartName);
            GetHistory();
        }

        public virtual void frm_Load(object sender, EventArgs e)
        {
            RefreshData();
        }
        protected bool CheckErrorText(params object[] objs)
        {
            List<bool> status = new List<bool>();
            foreach (var obj in objs)
            {

                if (obj is BaseEdit baseEdit)
                {
                    if (string.IsNullOrWhiteSpace(baseEdit.Text.Trim()))
                    {
                        baseEdit.ErrorText = LangResource.ErrorCantBeEmpry;
                        status.Add(false);
                    }
                    continue;
                }
            }
            return status.Where(x => x == false).Count() == 0 ? true : false;
        }
        public virtual void RefreshData()
        {

        }
        public bool SkipMessageBox = false;
        public bool SaveChangedData()
        {
            if(SkipMessageBox) 
                return true;
            switch (XtraMessageBox.Show("يوجد بعض التعديلات التي تمت ولم يتم حفظها , هل تريد حفظها اولا ؟ ", "", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
            {

                case DialogResult.Cancel: return false;
                case DialogResult.Yes: Save(); return true;
                case DialogResult.No: return true;
                default: return false;


            }
        }
        public virtual void DataChanged(object sender, EventArgs e)
        {
            ChangesMade = true;

        }
        bool ProssingKey;
        /* 
        If the form is currently prossing a keyDown event 
        used to prevent  The Form.KeyDown event is raised twice when a key is pressed in
        a grid and the Form.KeyPreview option is enabled  
        */

        public virtual void frm_Master_KeyDown(object sender, KeyEventArgs e)
        {
            if (ProssingKey) return;
            ProssingKey = true;
            if (e.KeyCode == Keys.S && e.Modifiers == Keys.Alt)
            {

            }
            if (e.KeyCode == Keys.I && e.Modifiers == Keys.Alt)
            {

            }
            if (e.KeyCode == Keys.S && e.Modifiers == Keys.Control)
            {

                Save();

            }
            if (e.KeyCode == Keys.Delete && e.Modifiers == Keys.Control)
            {

                Delete();
            }
            if (e.KeyCode == Keys.N && e.Modifiers == Keys.Control)
            {
                New();
            }
            if (e.KeyCode == Keys.P && e.Modifiers == Keys.Control)
            {
                if (IsNew || ChangesMade)
                    SaveAndPrint();
                else
                    Print();
            }
            if (e.KeyCode == Keys.R && e.Modifiers == Keys.Control)
            {
                RefreshData();
            }
           
            ProssingKey = false;
        }
        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (IsNew || ChangesMade)
                SaveAndPrint();
            else
                Print();
        }

        public void SaveAndPrint()
        {
            if (XtraMessageBox.Show("يجب حفظ التغييرات اولاً , هل تريد الحفظ ؟", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                PrintAfterSave = true;
                Save();
              //  Print();
                PrintAfterSave = false;

            }
        }
        public void frm_Master_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (ChangesMade && !SaveChangedData()) e.Cancel = true;

        }
        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RefreshData();
        }

        private void btn_New_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            New();
        }
        private void bt_Save_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Save();
        }
        private void btn_Delete_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Delete();
        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OpenList();
        }

        private void btn_Refresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RefreshData();
        }
        bool PrintAfterSave;
        private void btn_SaveAndPrint_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            PrintAfterSave = true;
            Save();
            PrintAfterSave = false;
        }
        protected bool IsDoubleClickInRow(object sender, EventArgs e)
        {
            DXMouseEventArgs eventArgs = e as DXMouseEventArgs;
            GridView view = sender as GridView;
            GridHitInfo info = view.CalcHitInfo(eventArgs.Location);
            return (info.InRow || info.InRowCell);
        }
        
    }
}