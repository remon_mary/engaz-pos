﻿using ByStro.Clases;
using DevExpress.DataProcessing;
using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ByStro.Forms
{
    public partial class frm_SearchItems : DevExpress.XtraBars.FluentDesignSystem.FluentDesignForm
    {
        public static frm_SearchItems instance;
        public static frm_SearchItems Instance
        {
            get
            {
                if (instance == null || instance.IsDisposed == true)
                    instance = new frm_SearchItems();
                return instance;
            }
            set => instance = value;
        }
        public static void ShowSearchWindow(object sender, EventArgs e)
        {
            frm_SearchItems.Instance.ShowDialog();
        }
        public frm_SearchItems()
        {
            InitializeComponent();

            this.Load += new System.EventHandler(this.frm_SearchItems_Load);
            this.TopMost = false; // To keep the window allways on top 

            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;  // 
            this.StartPosition = FormStartPosition.CenterScreen;
            gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsDetail.ShowDetailTabs = false;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsFind.FindDelay = 500;
            this.gridView1.OptionsFind.FindNullPrompt = "ابحث عن الصنف";
            this.gridView1.OptionsFind.ShowCloseButton = false;
            this.gridView1.OptionsFind.ShowFindButton = false;
            this.gridView1.OptionsPrint.EnableAppearanceOddRow = true;
            this.gridView1.OptionsView.EnableAppearanceOddRow = true;
            this.gridView1.OptionsView.ShowIndicator = false;


            gridControl1.ViewRegistered += GridControl1_ViewRegistered;


        }
        private void frm_SearchItems_Load(object sender, EventArgs e)
        {
            RefreshData();
            gridView1.Columns["ID"].Caption = "رقم";
            gridView1.Columns["Code"].Caption = "باركود";
            gridView1.Columns["Name"].Caption = "اسم الصنف";
            gridView1.Columns["CategoryName"].Caption = "التصنيف";
            gridView1.Columns["CustomUnit"].Visible =false;
            gridView1.Columns["CategoryID"].Visible =false;
        }

        private void GridControl1_ViewRegistered(object sender, DevExpress.XtraGrid.ViewOperationEventArgs e)
        {

            var view = e.View as GridView;
            if (view != null && view.LevelName == "Units")
            {
                view.ViewCaption = "تفاصيل وحدات الصنف";
                view.OptionsView.ShowViewCaption = true;
                view.Columns["Name"].Caption = "اسم الوحدة";
                view.Columns["BuyPrice"].Caption = "سعر الشراء";
                view.Columns["SellPrice"].Caption = "سعر البيع";
                view.Columns["Balance"].Caption = "رصيد الوحدة";
                view.Columns["logSumIN"].Visible = false;
                view.Columns["logSumOut"].Visible = false;
                view.Columns["BalanceString"].Visible = false;
            }
        }

        void RefreshData()
        {
            gridControl1.DataSource = CustomItem.GetCustomItems();
            gridView1.Columns.ForEach(x => x.OptionsColumn.AllowEdit = false);
        }
    }
}
