﻿using System; 
using DevExpress.XtraEditors;
using ByStro.DAL;
using System.Data.SqlClient;
using System.Collections.Generic;
using ByStro.Clases;

namespace ByStro.Forms
{
    public partial class frm_CreatNewCompany : DevExpress.XtraEditors.XtraForm
    {
        public frm_CreatNewCompany()
        {
            InitializeComponent();
        }
        DBDataContext db;
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(Properties.Settings.Default.Connection_String);
            builder.InitialCatalog = textEdit6.Text;
            Properties.Settings.Default.Connection_String = builder.ConnectionString;
            Properties.Settings.Default.Save();
            db = new DBDataContext(builder.ConnectionString);
            if (db.DatabaseExists())
            {
                XtraMessageBox.Show("قاعده البيانات موجوده مسبقا , يرجي تغيير اسم قاعده البيانات ");
                return;

            }
            simpleButton1.Enabled = false; 
            layoutControlItem8.Visibility =  DevExpress.XtraLayout.Utils.LayoutVisibility.Always ;
            progressBar1.Value = 10;
            this.Invalidate(true);
            db.CreateDatabase();
            progressBar1.Value += 10;
            this.Invalidate(true);
            db.UserPermissions.InsertOnSubmit(new UserPermission()
            {
                UserPassword = "admin",
                UserName = "admin",
                TypeUser = true,
                Status = true,
                ID = 1,
                EmpName = "adminstrator",
                CS1 = true,
                CS2 = true,
                CS3 = true,
                CS4 = true,
                CS5 = true,
                CS6 = true,
                CS7 = true,
                CS8 = true,
                CS9 = true,
                CS10 = true,
                CS11 = true,
                CS12 = true,
                CS13 = true,
                CS14 = true,
                CS15 = true,
                CS16 = true,
                CS17 = true,
                CS18 = true,
                CS19 = true,
                CS20 = true,
                CS21 = true,
                CS22 = true,
                CS23 = true,
                CS24 = true,
                CS25 = true,
                CS26 = true,
                CS27 = true,
                CS28 = true,
                CS29 = true,
                CS30 = true,
                CS31 = true,
                CS32 = true,
                CS33 = true,
                CS34 = true,
                CS35 = true,
                CS36 = true,
                CS37 = true,
                CS38 = true,
                CS39 = true,
                CS40 = true,
                CS41 = true,
                CS42 = true,
                CS43 = true,
                CS44 = true,
                CS45 = true,
                CS46 = true,
                CS47 = true,
                CS48 = true,
                CS49 = true,
                CS50 = true,
                CS51 = true,
                CS52 = true,
                CS53 = true,
                CS54 = true,
                CS55 = true,
                CS56 = true,
                CS57 = true,
                CS58 = true,
                CS59 = true,
                CS60 = true,
                CS61 = true,
                CS62 = true,
                CS63 = true,
                CS64 = true,
                CS65 = true,
                CS66 = true,
                CS67 = true,
                CS68 = true,
                CS69 = true,
                CS70 = true,
                CS71 = true,
                CS72 = true,
                CS73 = true,
                CS74 = true,
                CS75 = true,
                CS76 = true,
                CS77 = true,
                CS78 = true,
                CS79 = true,
                CS80 = true,
                CS81 = true, 
                
            });
            //TODO InsertOnSubmit Stores
            //db.Stores.InsertOnSubmit(new Store()
            //{
            //    StoreID = 1,
            //    StoreName = "المخزن الافتراضي", 
            //    Note="",
            //    phone= "",
            //    phone2 = "",
            //    Place= "",


            //});
            progressBar1.Value += 10;
            this.Invalidate(true);
         
            progressBar1.Value += 10;
            this.Invalidate(true);
            //TODO InsertOnSubmit StoreUsers
            //db.StoreUsers.InsertOnSubmit(new StoreUser()
            //{
            //    ID = 1,
            //    StoreID = 1,
            //    UserID = 1 
            //});
            progressBar1.Value += 10;
            this.Invalidate(true);
            db.Categories.InsertOnSubmit(new Category()
            {
                CategoryID = 1,
                CategoryName = "مجموعه افتراضيه",
                Remark ="", 
                
            });
            progressBar1.Value += 10;
            this.Invalidate(true);
            db.Suppliers.InsertOnSubmit(new Supplier()
            {
                SupplierID = 1,
                SupplierName = "مورد افتراضي",
                Status = true,
                UserAdd = 1,
                CreditLimit = 1,
            });
            progressBar1.Value += 10;
            this.Invalidate(true);
            db.Customers.InsertOnSubmit(new Customer()
            {
                CustomerID = 1,
                CustomerName = "عميل افتراضي",
                CreditLimit = 1,
                SalesLavel = "1",
                ISCusSupp = false,
                Status = true,
                UserAdd = 1,
                AccountNumber="",
                Address="",
                Email="",
                EndContract="",
                Fax="",
                NationalID="",
                WebSite = "",
                Maximum = "",
                Phone = "",
                Phone2 = "",
                Remarks="",
                TaxFileNumber ="",
                SupplierID="",


            });
            progressBar1.Value += 10;
            this.Invalidate(true);
            db.Units.InsertOnSubmit(new Unit()
            {
                UnitID = 1,
                UnitName = "قطعه"
            });
            progressBar1.Value += 10;
            this.Invalidate(true);
          
            db.SystemSettings.InsertOnSubmit(new SystemSetting() { DivideWeightBy = 2, ItemCodeLength = 7, UseScalBarcode = true, WeightCodeLength = 6 });
            db.Settings.InsertOnSubmit(new Setting()
            {
                CategoryId = "1",
                CustomerID = "1",
                SettingID = 1,
                UseVat = false,
                StoreID = "1",
                UnitId = "1",
                UseCategory = true,
                UseCustomerDefault = true,
                UseStoreDefault = true,
                UseUnit = true,
                Vat = 0,
                PrintSize = 0,
                ShowMessageSave = true,
                ShowMessageQty = true,
                kindPay = "0",
                UsekindPay = false,
                UsingFastInput =true,
                UseCrrencyDefault = false,
                NotificationCustomers =true,
                NotificationProdect=true,
                MaxBalance = "10000",
                

            });
            progressBar1.Value += 10;
            this.Invalidate(true);
            CreateDB();
            XtraMessageBox.Show("تم انشاء قاعده البيانات بنجاح"); 
            db.SubmitChanges();
            Microsoft.AppCenter.Analytics.Analytics.TrackEvent("CreatDataBase", new Dictionary<string, string> { { "Database Name", textEdit6.Text } });
        }
        public  void CreateDB()
        {
            #region CreatDB
      

                // Company 
                DAL.St_CompanyInfo info = new DAL.St_CompanyInfo
                {
                    CompanyName = "CompanyName",
                    ID = 1,
                    MoneyToTextMode = true


             ,
                    EmployeesDueAccount = AccountTemplate.Accounts.EmployeesDueAccount.ID
             ,
                    WagesAccount = AccountTemplate.Accounts.WagesAccount.ID
             ,
                    DueSalerysAccount = AccountTemplate.Accounts.DueSalerysAccount.ID
             ,
                    DrawerAccount = AccountTemplate.Accounts.DrawerAccount.ID
             ,
                    BanksAccount = AccountTemplate.Accounts.BanksAccount.ID
             ,
                    CustomersAccount = AccountTemplate.Accounts.CustomersAccount.ID
             ,
                    NotesReceivableAccount = AccountTemplate.Accounts.NotesReceivableAccount.ID
             ,
                    InventoryAccount = AccountTemplate.Accounts.InventoryAccount.ID
             ,
                    VendorsAccount = AccountTemplate.Accounts.VendorsAccount.ID
             ,
                    CapitalAccount = AccountTemplate.Accounts.CapitalAccount.ID
             ,
                    NotesPayableAccount = AccountTemplate.Accounts.NotesPayableAccount.ID
             ,
                    TaxAccount = AccountTemplate.Accounts.TaxAccount.ID
             ,
                    ManufacturingExpAccount = AccountTemplate.Accounts.ManufacturingExpAccount.ID
             ,
                    MerchandisingAccount = AccountTemplate.Accounts.MerchandisingAccount.ID
             ,
                    PurchasesAccount = AccountTemplate.Accounts.PurchasesAccount.ID
             ,
                    PurchasesReturnAccount = AccountTemplate.Accounts.PurchasesReturnAccount.ID
             ,
                    SalesAccount = AccountTemplate.Accounts.SalesAccount.ID
             ,
                    SalesReturnAccount = AccountTemplate.Accounts.SalesReturnAccount.ID
             ,
                    OpenInventoryAccount = AccountTemplate.Accounts.OpenInventoryAccount.ID
             ,
                    CloseInventoryAccount = AccountTemplate.Accounts.CloseInventoryAccount.ID
             ,
                    PurchaseDiscountAccount = AccountTemplate.Accounts.PurchaseDiscountAccount.ID
             ,
                    SalesDiscountAccount = AccountTemplate.Accounts.SalesDiscountAccount.ID
             ,
                    FixedAssetsAccount = AccountTemplate.Accounts.FixedAssetsAccount.ID
             ,
                    SalesDeductTaxAccount = AccountTemplate.Accounts.SalesDeductTaxAccount.ID
             ,
                    PurchaseDeductTaxAccount = AccountTemplate.Accounts.PurchaseDeductTaxAccount.ID
             ,
                    CostOfSoldGoodsAccount = AccountTemplate.Accounts.CostOfSoldGoodsAccount.ID
             ,
                    DepreciationAccount = AccountTemplate.Accounts.DepreciationAccount.ID
             ,
                    ExpensesAccount = AccountTemplate.Accounts.ExpensesAccount.ID
             ,
                    RevenueAccount = AccountTemplate.Accounts.RevenueAccount.ID
             ,
                    PurchaseAddTaxAccount = AccountTemplate.Accounts.PurchaseAddTaxAccount.ID
             ,
                    SalesAddTaxAccount = AccountTemplate.Accounts.SalesAddTaxAccount.ID
                ,
                    StockIsPeriodic = true,
                    BackupPath = "",
                    PrimaryReportingFolderName = "PrintTemplets",
                    SecondaryReportingPath = "",
                    CompanyFYearEnd = new DateTime(DateTime.Now.Year, 12, 30),
                    CompanyFYearStart = new DateTime(DateTime.Now.Year, 1, 1),

                };
                db.St_CompanyInfos.InsertOnSubmit(info);

                List<DAL.Acc_Account> Accounts = AccountTemplate.GetAllAccounts();
                // account tree  
                db.Acc_Accounts.InsertAllOnSubmit(Accounts);
             
                 db.SubmitChanges();
                DAL.Inv_Store store = new DAL.Inv_Store
                {
                    Name = "المخزن الرئيسي",
                    ID = 1,
                    SellAccountID = (int)Master.InsertNewAccount("المخزن الرئيسي" + " - " + LangResource.Sales, info.SalesAccount),
                    SellReturnAccountID = (int)Master.InsertNewAccount("المخزن الرئيسي" + " - " + LangResource.SalesReturn, info.SalesReturnAccount),
                    SalesDiscountAccountID = (int)Master.InsertNewAccount("المخزن الرئيسي" + " - " + LangResource.SalesDiscount, info.SalesDiscountAccount),
                    PurchaseAccountID = (int)Master.InsertNewAccount("المخزن الرئيسي" + " - " + LangResource.Purchases, info.PurchasesAccount),
                    PurchaseReturnAccountID = (int)Master.InsertNewAccount("المخزن الرئيسي" + " - " + LangResource.purchasesReturn, info.PurchasesReturnAccount),
                    PurchaseDiscountAccountID = (int)Master.InsertNewAccount("المخزن الرئيسي" + " - " + LangResource.PurchaseDiscount, info.PurchaseDiscountAccount),
                    OpenInventoryAccount = (int)Master.InsertNewAccount("المخزن الرئيسي" + " - " + LangResource.OpenInventory, info.OpenInventoryAccount),
                    CloseInventoryAccount = (int)Master.InsertNewAccount("المخزن الرئيسي" + " - " + LangResource.CloseInventory, info.CloseInventoryAccount),
                    CostMethod = 0
                };
                db.Inv_Stores.InsertOnSubmit(store);
                // CustomerGroup 
                DAL.Sl_CustomerGroup customergroup = new DAL.Sl_CustomerGroup { Name = LangResource.All, Number = 1 };
                db.Sl_CustomerGroups.InsertOnSubmit(customergroup);
                // Vendor Group 
                DAL.Pr_VendorGroup Vendorgroup = new DAL.Pr_VendorGroup { Name = LangResource.All, Number = 1 };
                db.Pr_VendorGroups.InsertOnSubmit(Vendorgroup);


                //Customer 

                DAL.Sl_Customer customer = new DAL.Sl_Customer { Name = LangResource.AnoymasCustomer, ID = 1, GroupID = 1, Account = Master.InsertNewAccount(LangResource.AnoymasCustomer,info.CustomersAccount) };
                db.Sl_Customers.InsertOnSubmit(customer);
                //Vendor 
                DAL.Pr_Vendor Vendor = new DAL.Pr_Vendor { Name = LangResource.AnoymasVendor, ID = 1, GroupID = 1, Account = Master.InsertNewAccount(LangResource.AnoymasVendor,info.VendorsAccount) };
                db.Pr_Vendors.InsertOnSubmit(Vendor);
                //Drawer 
                DAL.Acc_Drawer drawer = new DAL.Acc_Drawer() { ID = 1, Name = LangResource.MainDrawer, ACCID = (int)Master.InsertNewAccount(LangResource.MainDrawer,info.DrawerAccount) };
                db.Acc_Drawers.InsertOnSubmit(drawer);

                Master.AccountOpenBalance OP = new Master.AccountOpenBalance();
                OP.IsDebit = true;
                OP.Amount = 0;
                OP.Date = DateTime.Now;
                Master.CreatAccOpenBalance((int)drawer.ACCID, drawer.Name, OP);


                db.SubmitChanges();
        
            #endregion
        }
    }
}