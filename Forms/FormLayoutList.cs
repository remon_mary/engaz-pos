﻿using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ByStro.Forms
{
    public partial class FormLayoutList : frm_Master
    {
        public FormLayoutList(string baseId= "BaseId")
        {
            InitializeComponent();

            BaseId = baseId;
            btn_Refresh.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
            btn_Print.Visibility =  btn_Delete.Visibility = btn_SaveAndPrint.Visibility = btn_Save.Visibility = btn_List.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            gridView1.DoubleClick += GridView1_DoubleClick;        
            gridView1.Appearance.EvenRow.BackColor = Color.FromArgb(200, 255, 240, 196);
            gridView1.OptionsView.EnableAppearanceEvenRow = true;
            gridView1.Appearance.OddRow.BackColor = Color.WhiteSmoke;
            gridView1.OptionsView.EnableAppearanceOddRow = true;
           
            //----  PaintAppearance   ----//
            gridView1.OptionsPrint.UsePrintStyles = true;
            gridView1.AppearancePrint.EvenRow.BackColor = Color.LightGray;
            gridView1.AppearancePrint.OddRow.BackColor = Color.WhiteSmoke;
            gridView1.OptionsPrint.EnableAppearanceEvenRow = true;
            gridView1.OptionsPrint.EnableAppearanceOddRow = true;

            gridView1.AppearancePrint.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            gridView1.AppearancePrint.HeaderPanel.BackColor = Color.FromArgb(43, 87, 151);
            gridView1.AppearancePrint.HeaderPanel.Font = new Font(gridView1.AppearancePrint.HeaderPanel.Font,FontStyle.Bold);
            gridView1.AppearancePrint.HeaderPanel.Options.UseBackColor = true;
            gridView1.AppearancePrint.HeaderPanel.Options.UseFont = true;
            gridView1.AppearancePrint.HeaderPanel.Options.UseForeColor = true;
            gridView1.AppearancePrint.Lines.BackColor = Color.LightBlue;
            gridView1.OptionsPrint.AllowMultilineHeaders= true;
            gridView1.OptionsPrint.AutoWidth= true;
            gridView1.ColumnPanelRowHeight = 40;

            gridView1.OptionsBehavior.AlignGroupSummaryInGroupRow = DevExpress.Utils.DefaultBoolean.True;
            PopulateColumns();
            //gridView1.ReLodeCustomContol(this.Name);
            //this.FormClosing += (ss, ee) => {                 
            //    gridView1.SaveCustomContol(this.Name);
            //};
        }
        public virtual void PopulateColumns() 
        {
            RefreshData();
            gridView1.PopulateColumns();
        }

        public virtual void OpenForm(int id)
        {
        }
        
        private string baseId;
        public string BaseId
        {
            get { return string.IsNullOrWhiteSpace(baseId) ? "baseId" : baseId; }
            set { baseId = value; }
        }

        private void GridView1_DoubleClick(object sender, EventArgs e)
        {
            if (IsDoubleClickInRow(sender, e))
            {
                int id;
                if (int.TryParse(gridView1.GetFocusedRowCellValue(BaseId).ToString(), out id) && id != 0)
                    OpenForm(id);
            }
        }
    }
}
