﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ByStro.DAL;
using DevExpress.XtraCharts;
using ByStro.Clases;

namespace ByStro.Forms
{
    public partial class frm_ProductsProfit : DevExpress.XtraEditors.XtraForm
    {
        public frm_ProductsProfit()
        {
            InitializeComponent();
        }
        private void dateEdit2_EditValueChanged(object sender, EventArgs e)
        {
            if (dateEdit1.DateTime.Year < 1950 || dateEdit2.DateTime.Year < 1950 || dateEdit2.DateTime < dateEdit1.DateTime)
            {
                // XtraMessageBox.Show("يرجي اختيار التاريخ بشكل صحيح");
                return;
            }
            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);

            var SalesDetails = db.Inv_InvoiceDetails.
               Where(sd => 
               db.Inv_Invoices.Where(s => s.Date.Date >= dateEdit1.DateTime.Date && s.Date.Date <= dateEdit2.DateTime.Date &&(
                 s.InvoiceType == (int)MasterClass.InvoiceType.SalesInvoice)).Select(s => s.ID).Contains(sd.InvoiceID));

            var SalesReturnDetails = db.Inv_InvoiceDetails.
              Where(sd =>
              db.Inv_Invoices.Where(s => s.Date.Date >= dateEdit1.DateTime.Date && s.Date.Date <= dateEdit2.DateTime.Date && 
                s.InvoiceType == (int)MasterClass.InvoiceType.SalesReturn).Select(s => s.ID).Contains(sd.InvoiceID));

            var salesDate = SalesDetails.Select(d => new
            {
                d.ItemID,
                ProfitIn = d.TotalPrice - d.TotalCostValue,
                ProfitOut = (double)0
            }).ToList();
            salesDate.AddRange(

                SalesReturnDetails.Select(d => new
                {
                    d.ItemID,
                    ProfitIn = (double)0,
                    ProfitOut = d.TotalPrice - d.TotalCostValue,
                }).ToList()

                );

            var data = salesDate.GroupBy(d => d.ItemID).Select(d => new
            {
                Product =db.Prodecuts.Single(p=> p.ProdecutID ==  d.Key).ProdecutName ,
                Sum= d.Sum(x=>(double?)x.ProfitIn ) ?? 0 - d.Sum(X=>(double?)X.ProfitOut )??0
            }).OrderByDescending(x=>x.Sum).ToList();


            Series MostSellingProductSeries = new Series("", ViewType.Doughnut);


            MostSellingProductSeries.DataSource = data;

            gridControl1.DataSource = data;

            MostSellingProductSeries.ArgumentDataMember = "Product";
            MostSellingProductSeries.ValueScaleType = ScaleType.Numerical;
            MostSellingProductSeries.ValueDataMembers.AddRange(new string[] { "Sum" });
            MostSellingProductSeries.Label.TextPattern = "{A} = {V} \n ({VP:P0})";
            MostSellingProductSeries.LegendTextPattern = "{A}";
            MostSellingProductSeries.TopNOptions.ShowOthers = true;
            MostSellingProductSeries.TopNOptions.Enabled = true;
            MostSellingProductSeries.TopNOptions.Count = 5;

            ((DoughnutSeriesLabel)MostSellingProductSeries.Label).Position = PieSeriesLabelPosition.TwoColumns;
            ((DoughnutSeriesLabel)MostSellingProductSeries.Label).ResolveOverlappingMode = ResolveOverlappingMode.Default;
            ((DoughnutSeriesLabel)MostSellingProductSeries.Label).ResolveOverlappingMinIndent = 5;
            MostSellingProductSeries.ShowInLegend = true;

          //  chartControl1 = new ChartControl();
            chartControl1.AnimationStartMode = ChartAnimationMode.OnDataChanged;
            chartControl1.Series.Clear();
            chartControl1.Series.Add(MostSellingProductSeries);
        }

        private void frm_ProductsProfit_Load(object sender, EventArgs e)
        {
            this.dateEdit1.EditValueChanged += new System.EventHandler(this.dateEdit2_EditValueChanged);
            this.dateEdit2.EditValueChanged += new System.EventHandler(this.dateEdit2_EditValueChanged);
            dateEdit1.DateTime = new DateTime(DateTime.Now.Year, 1, 1);
            dateEdit2.DateTime = DateTime.Now.Date;
        }
    }
}