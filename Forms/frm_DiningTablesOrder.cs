﻿using System;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Linq.Expressions;
using DevExpress.XtraBars;
using ByStro.DAL;
using ByStro.Clases;
using System.Collections.ObjectModel;
using System.ComponentModel;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraEditors.Repository;
using ByStro.PL;

namespace ByStro.Forms
{
    public partial class frm_DiningTablesOrder : frm_Master 
    {
        DiningTablesOrder  tablesOrder;
        internal override bool IsNew
        {
            get => base.IsNew; set
            {
                base.IsNew = value;


                if (value == true)
                    this.Name = "امر تشغيل طاولة جديد";
                else
                    this.Name = string.Format("تعديل امر تشغيل طاولة رقم {0}  - {1} ", tablesOrder.ID , gridLookUpEdit1 .Text);
                btn_Delete.Visibility = value ? BarItemVisibility.Never : BarItemVisibility.Always;
            }
        }

        public frm_DiningTablesOrder()
        {
            InitializeComponent();

            New();

            GetData();
        }
        public frm_DiningTablesOrder(int ID)
        {
            InitializeComponent();
            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
            tablesOrder = db.DiningTablesOrders.Where(x => x.ID == ID).SingleOrDefault();
            if (tablesOrder == null)
                New();
            GetData();
        }
        BindingList<TempOrderInvoicesObject> OrderInvoices;

        class TempOrderInvoicesObject
        {
            public int InvoiceID { get; set; }
            public int ItemCount { get; set; }
            public double Amount { get; set; }
            public bool  Selected { get; set; }

        }
        
        private bool ValidData()
        {

            if (gridLookUpEdit1.EditValue.ValidAsIntNonZero() == false )
            {
                gridLookUpEdit1.ErrorText = "";
                gridLookUpEdit1.Focus();
                return false;
            }

            if ((gridView1.DataSource as BindingList<TempOrderInvoicesObject>).Where(x => x.Selected)
              .Count() <= 0)
            {
                XtraMessageBox.Show("يجب اختيار  فاتوره واحده علي الاقل ");
                return false;
            }
            return true;
        }
        public override void OpenList()
        {
            Main_frm.OpenForm(new frm_DiningTablesOrderList(true), true);
            base.OpenList();
        }
        public override void Save()
        {

            if (!ValidData()) { return; }
            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
            if (IsNew)
            {
                tablesOrder = new  DiningTablesOrder ();
                db.DiningTablesOrders.InsertOnSubmit(tablesOrder);
            }
            else
            {
                tablesOrder = db.DiningTablesOrders.Where(s => s.ID == tablesOrder.ID).First();
            }
            SetData();
            db.SubmitChanges();


            if(tablesOrder.HasReturned)
            {
                var Orders = db.DiningTablesOrders.FirstOrDefault(d => d.ID == tablesOrder.ID && d.DiningTableID == Convert.ToInt32(gridLookUpEdit1.EditValue));
                var stringList = "";
                if (Orders != null)
                    stringList = Orders.Orders; 
                var ordersInvoicesList = stringList.Split(new char[] { Convert.ToChar(",") }, options: StringSplitOptions.RemoveEmptyEntries);
                var invoices = db.Inv_Invoices.Where(x =>
                  x.InvoiceType == (int)MasterClass.InvoiceType.SalesInvoice &&
               ordersInvoicesList.Contains(x.ID.ToString()));
            }
            db.SubmitChanges();
            base.Save();
        }
        public override void Delete()
        {
            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
            if (IsNew) return;
         
            if (XtraMessageBox.Show(text: "تأكيد الحذف", caption: "هل تريد الحذف ؟", buttons: MessageBoxButtons.YesNo, icon: MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                db.SubmitChanges();

                tablesOrder = db.DiningTablesOrders.Where(c => c.ID == tablesOrder.ID).First();
                db.DiningTablesOrders.DeleteOnSubmit(tablesOrder);
                db.SubmitChanges();
                base.Delete();
                New();
            }
        }
        public override void New()
        {
            tablesOrder = new DiningTablesOrder();
            GetData();
            base.New();
            IsNew = true;
            ChangesMade = false;
        }


        bool IsLoadingData;


        private void GetData()
        {
            IsLoadingData = true;
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);


            gridLookUpEdit1.EditValue = tablesOrder.DiningTableID;
            spinEdit1.EditValue = tablesOrder.OrdersAmount;
            textEdit2.Text = tablesOrder.OrdersCount.ToString();
            textEdit1.Text = tablesOrder.ID .ToString();
            spinEdit2.EditValue = tablesOrder.RecivedAmount;
            checkEdit1.Checked  = tablesOrder.HasReturned ;
            IsLoadingData = false;
        }

        void SetData()
        {
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
            tablesOrder.DiningTableID = gridLookUpEdit1.EditValue.ToInt();
            tablesOrder.HasReturned = checkEdit1.Checked;
            var ordersIDs = "";
            (gridView1.DataSource as BindingList<TempOrderInvoicesObject>).ToList().Where (x=>x.Selected ).ToList().ForEach(i =>
            {
                ordersIDs += i.InvoiceID + ",";
            });

            tablesOrder.Orders = ordersIDs;
            tablesOrder.OrdersAmount = Convert.ToDouble(spinEdit1.EditValue);
            tablesOrder.OrdersCount = (gridView1.DataSource as BindingList<TempOrderInvoicesObject>).Where(x => x.Selected).Count();
            tablesOrder.RecivedAmount = Convert.ToDouble(spinEdit2.EditValue);
        }
        public override void RefreshData()
        {
            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
            gridLookUpEdit1.Properties.DataSource = db.DiningTables.Select(x => new { x.Name, x.ID, State = (db.DiningTablesOrders.Where(d => d.HasReturned == false  && d.DiningTableID == x.ID).Count() > 0) }).ToList();
            gridLookUpEdit1.Properties.ValueMember = "ID";
            gridLookUpEdit1.Properties.DisplayMember = "Name";
            var view = gridLookUpEdit1.Properties.View;
            view.PopulateColumns(gridLookUpEdit1.Properties.DataSource);
            view.Columns["State"].Caption = "الحاله";
            view.Columns["State"].ColumnEdit = new RepositoryItemTextEdit();
            view.Columns["ID"].Visible = false;
            view.Columns["Name"].Caption = "الاسم";
            view.RowCellStyle += View_RowCellStyle;
            view.CustomColumnDisplayText += View_CustomColumnDisplayText;
            base.RefreshData();
        }
        private void View_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.FieldName == "State")
            {
                e.DisplayText = (e.Value is bool d && d == true) ? "مشغول" : "متاح";
            }
        }

        private void View_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            if (e.Column.FieldName == "State")
            {
                if (e.CellValue is bool d && d == true)
                {
                    e.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Warning;
                }
                else
                {
                    e.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Success;

                }
            }
        }
        public override void frm_Load(object sender, EventArgs e)
        {
            base.frm_Load(sender, e);
            layoutControl1.AllowCustomization = false;
            btn_Print.Visibility = BarItemVisibility.Never;

         
            gridView1.CellValueChanged += GridView1_CellValueChanged;
            gridView1.RowCountChanged += GridView1_RowCountChanged;
            #region DataChanged
            spinEdit1 .EditValueChanged += DataChanged;
            spinEdit2.EditValueChanged += DataChanged;
            #endregion 
        }

        private void GridView1_RowCountChanged(object sender, EventArgs e)
        {
            GridView1_CellValueChanged(sender, null);
        }

        private void GridView1_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            if(gridView1.DataSource != null)
            {

                spinEdit1 .Text = (gridView1.DataSource as BindingList<TempOrderInvoicesObject>).Where(x => x.Selected)
                .Sum(x => x.Amount).ToString();
            textEdit2.Text = (gridView1.DataSource as BindingList<TempOrderInvoicesObject>).Where(x => x.Selected)
              .Count().ToString();
            }
            else
            {
                spinEdit1.Text = textEdit2.Text = "0";
            }
        }

        private void gridLookUpEdit1_EditValueChanged(object sender, EventArgs e)
        {
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
           if(gridLookUpEdit1.EditValue.ValidAsIntNonZero() == false)
            {
                gridControl1.DataSource = null;
                return;
            }
            if (IsNew)
            {
                var oldOrders = db.DiningTablesOrders.FirstOrDefault(d => d.DiningTableID == Convert.ToInt32(gridLookUpEdit1.EditValue));
                var stringList = "";
                if (oldOrders != null)
                    stringList = oldOrders.Orders;
                var ordersInvoicesList = stringList.Split(new char[] {Convert.ToChar( ",") },options: StringSplitOptions.RemoveEmptyEntries);
                gridControl1.DataSource = db.Inv_Invoices .Where(x =>
                x.InvoiceType == (int)MasterClass.InvoiceType.SalesInvoice&&
                x.DiningTableID  == Convert.ToInt32(gridLookUpEdit1.EditValue) &&
               !ordersInvoicesList.Contains(x.ID .ToString())
               ).Select(x=>new TempOrderInvoicesObject 
               {
                   Amount = x.Net ,
                   InvoiceID =(int)x.ID,
                   ItemCount = db.Inv_InvoiceDetails  .Where(d=>d.InvoiceID  == x.ID   ).Count()
               });
            }
            else
            {
                var oldOrders = db.DiningTablesOrders.FirstOrDefault(d => d.ID == tablesOrder.ID && d.DiningTableID == Convert.ToInt32(gridLookUpEdit1.EditValue));
                var stringList = "";
                if (oldOrders != null)
                    stringList = oldOrders.Orders;


                var ordersInvoicesList = stringList.Split(new char[] { Convert.ToChar(",") }, options: StringSplitOptions.RemoveEmptyEntries);
                gridControl1.DataSource = db.Inv_Invoices.Where(x =>
                  x.InvoiceType == (int)MasterClass.InvoiceType.SalesInvoice &&
               ordersInvoicesList.Contains(x.ID.ToString())
               ).Select(x => new TempOrderInvoicesObject
               {
                   Amount = x.Net ,
                   InvoiceID = (int)x.ID ,
                   ItemCount = db.Inv_InvoiceDetails   .Where(d => d.InvoiceID    == x.ID  ).Count() ,
                   Selected =true 
               });
            }
            gridView1.Columns["Selected"].OptionsColumn.AllowEdit = (IsNew);
            gridView1.Columns["Amount"].Caption = "قيمه الفاتوره";
            gridView1.Columns["Selected"].Caption = "توصيل";
            gridView1.Columns["InvoiceID"].Caption = "الفاتوره";
            gridView1.Columns["ItemCount"].Caption = "عدد الاصناف";
            gridView1.Columns["Amount"].OptionsColumn.AllowEdit =
            gridView1.Columns["InvoiceID"].OptionsColumn.AllowEdit =
            gridView1.Columns["ItemCount"].OptionsColumn.AllowEdit = false;
        }
        public override void Print()
        {
           
        }

        private void textEdit2_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {
            spinEdit2.Enabled = checkEdit1.Checked;
            if(IsLoadingData == false && checkEdit1.Checked)
            {
                spinEdit2.EditValue = spinEdit1.EditValue;

            }
        }
    }
}
