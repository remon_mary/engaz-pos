﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;

using System.Windows.Forms;
using DevExpress.XtraSplashScreen;

namespace ByStro.Forms
{
    public partial class SplashScreen1 : SplashScreen
    {
        public SplashScreen1()
        {
            InitializeComponent();
            this.labelCopyright.Text = "Copyright © 1998-" + DateTime.Now.Year.ToString();
        }

        #region Overrides

        public override void ProcessCommand(Enum cmd, object arg)
        {
            base.ProcessCommand(cmd, arg);
        }

        #endregion

        public enum SplashScreenCommand
        {
        }

        private void SplashScreen1_Load(object sender, EventArgs e)
        {
            Timer timer = new Timer() { Interval = 3000 };
            timer.Tick += (ts, te) =>
            {
                while (this.Opacity > 0)
                {
                    this.Opacity -= 0.01;
                    System.Threading.Thread.Sleep(5);
                    Invalidate(true);
                } 
                this.Close(); 
            };
            timer.Start();
        }

        private void progressBarControl_EditValueChanged(object sender, EventArgs e)
        {

        }
    }
}