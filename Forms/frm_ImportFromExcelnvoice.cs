﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.DataAccess.Excel;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraSplashScreen;
using DevExpress.SpreadsheetSource;
using System.Threading;
using DevExpress.XtraWaitForm;
using ByStro.UControle;
using static ByStro.Forms.frm_AddInvoiceRange;

namespace ByStro.Forms
{
    public partial class frm_ImportFromExcelnvoice : XtraForm
    {
        string DefualtUnit, DefualtCompany;
        int DefualtGroup;
        BindingList<frm_AddInvoiceRange.Invoice> InvoiceList;
        public frm_ImportFromExcelnvoice(ref BindingList<frm_AddInvoiceRange.Invoice> _InvoiceList)
        {
            InitializeComponent();
            //DefualtUnit = _DefualtUnit;
            //DefualtCompany  = _DefualtCompany;
            //DefualtGroup = _DefualtGroup;
            InvoiceList = _InvoiceList;
        }

        #region Sheets

        private void textEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            XtraOpenFileDialog dialog = new XtraOpenFileDialog();
            dialog.Filter = "Excel File(*.xls)|*.xls|Excel File(*.xlsx)|*.xlsx";
            if (dialog.ShowDialog() != DialogResult.OK)
                return;
            textEdit1.Text = dialog.FileName;
        }

        private void cb_Sheets_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cb_Sheets.SelectedIndex >= 0 && cb_Sheets.Text != string.Empty)
            {
                try
                {
                    gridControl1.DataSource = null;
                    gridView1.Columns.Clear();

                    ExcelDataSource ds = new ExcelDataSource();
                    ds.FileName = textEdit1.Text;
                    DevExpress.DataAccess.Excel.ExcelSourceOptions excelSourceOptions1 = new DevExpress.DataAccess.Excel.ExcelSourceOptions();
                    DevExpress.DataAccess.Excel.ExcelWorksheetSettings excelWorksheetSettings1 = new DevExpress.DataAccess.Excel.ExcelWorksheetSettings();
                    excelWorksheetSettings1.WorksheetName = cb_Sheets.Text;
                    excelSourceOptions1.ImportSettings = excelWorksheetSettings1;
                    ds.SourceOptions = excelSourceOptions1;
                    ds.Fill();

                    gridControl1.DataSource = ds;
                    gridView1.PopulateColumns();

                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message);
                }
            }
        }

        private void textEdit1_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                cb_Sheets.Properties.Items.Clear();
                cb_Sheets.Properties.Items.AddRange(GetExcelSheetNames(textEdit1.Text));

            }
            catch
            {

            }


        }

        public static string[] GetExcelSheetNames(string filelocation)
        {  
            using (ISpreadsheetSource spreadsheetSource = SpreadsheetSourceFactory.CreateSource(filelocation))
            {
                IWorksheetCollection worksheetCollection = spreadsheetSource.Worksheets;
                return worksheetCollection.Select(x => x.Name).ToArray();
            } 
        }
        #endregion

        private void gridControl1_DataSourceChanged(object sender, EventArgs e)
        {
            cb_Id.Properties.Items.Clear();
            cb_Code.Properties.Items.Clear();
            cb_Barcode.Properties.Items.Clear();
            cb_Price.Properties.Items.Clear();
            cb_Qty.Properties.Items.Clear();
            cb_Tax.Properties.Items.Clear();
            cb_Discount.Properties.Items.Clear();

            cb_Id.Properties.Items.Add("");
            cb_Code.Properties.Items.Add("");
            cb_Barcode.Properties.Items.Add("");
            cb_Price.Properties.Items.Add("");
            cb_Qty.Properties.Items.Add("");
            cb_Tax.Properties.Items.Add("");
            cb_Discount.Properties.Items.Add("");
            foreach (GridColumn column in gridView1.Columns)
            {
                cb_Id.Properties.Items.Add(column.FieldName);
                cb_Code.Properties.Items.Add(column.FieldName);
                cb_Barcode.Properties.Items.Add(column.FieldName);
                cb_Price.Properties.Items.Add(column.FieldName);
                cb_Qty.Properties.Items.Add(column.FieldName);
                cb_Tax.Properties.Items.Add(column.FieldName);
                cb_Discount.Properties.Items.Add(column.FieldName);
            }
        }
 
        void StartImporting()
        {
            SplashScreenManager.ShowForm(this, typeof(WaitForm1), true, true, false);
            if (cb_Id.SelectedIndex <= 0)
            {
                cb_Id.ErrorText = "هذا الحقل مطلوب";
                return;
            }

            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
            for (int i = Convert.ToInt32(spn_Start.EditValue) - 1; i < gridView1.RowCount; i++)
            {
                if (gridView1.GetRowCellValue(i, cb_Id.Text) != null && gridView1.GetRowCellValue(i, cb_Id.Text) != DBNull.Value &&
                        gridView1.GetRowCellValue(i, cb_Id.Text).ToString().Trim() != "")
                {
                    SplashScreenManager.Default.SetWaitFormDescription(i.ToString() + "%");
                    int Id = 0;
                    double Price = 0;
                    double Qty = 0;
                    double TaxValue = 0;
                    double DiscountValue = 0;

                    if (cb_Id.SelectedIndex > 0 && gridView1.GetRowCellValue(i, cb_Id.Text) != null)
                        int.TryParse(gridView1.GetRowCellValue(i, cb_Id.Text).ToString(), out Id);
 
                    if (cb_Qty.SelectedIndex > 0 && gridView1.GetRowCellValue(i, cb_Qty.Text) != null)
                        double.TryParse(gridView1.GetRowCellValue(i, cb_Qty.Text).ToString(), out Qty);

                    if (cb_Price.SelectedIndex > 0 && gridView1.GetRowCellValue(i, cb_Price.Text) != null)
                        double.TryParse(gridView1.GetRowCellValue(i, cb_Price.Text).ToString(), out Price);

                    if (cb_Tax.SelectedIndex > 0 && gridView1.GetRowCellValue(i, cb_Tax.Text) != null)
                        double.TryParse(gridView1.GetRowCellValue(i, cb_Tax.Text).ToString(), out TaxValue);

                    if (cb_Discount.SelectedIndex > 0 && gridView1.GetRowCellValue(i, cb_Discount.Text) != null)
                        double.TryParse(gridView1.GetRowCellValue(i, cb_Discount.Text).ToString(), out DiscountValue);

                    Invoice invoice = new Invoice()
                    {
                        Id = Id,
                        Code = gridView1.GetRowCellValue(i, cb_Code.Text).ToString(),
                        BarCodeItem = gridView1.GetRowCellValue(i, cb_Barcode.Text).ToString(),
                        DiscountValue= DiscountValue,
                        TaxValue= TaxValue,
                        ItemQty=Qty,
                        Price= Price
                    };
                    InvoiceList.Add(invoice);
                }
            }
            SplashScreenManager.CloseForm(false);
            this.Close();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
             StartImporting();
        }
    }
}