﻿using ByStro.Clases;
using ByStro.PL;
using DevExpress.DataProcessing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Windows.Forms;

namespace ByStro.Forms
{
    public partial class frm_AddVendorCustomerRange : XtraForm
    {
        BackgroundWorker worker = new BackgroundWorker();
        BindingList<VendorCustomer> vendorCustomers;
        BindingList<NameAndID> Groups;
        List<DAL.Inv_InvoiceDetail> OpenBalancelist = new List<DAL.Inv_InvoiceDetail>();
        RepositoryItemComboBox repoUOM = new RepositoryItemComboBox();
        bool IsCustomer = true;
        public frm_AddVendorCustomerRange(bool IsCustomer)
        {
            InitializeComponent();
            if (!IsCustomer)
            {
                this.IsCustomer = IsCustomer;
                this.Text = "اضافه مجموعه من الموردين";
            }
            gridView1.InitNewRow += GridView1_InitNewRow;
            gridView1.ValidateRow += GridView1_ValidateRow;
            gridView1.InvalidRowException += GridView1_InvalidRowException;
            this.FormClosing += Frm_AddProductsRange_FormClosing;
        }
        private void Frm_AddProductsRange_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(worker != null && worker.IsBusy)
            {
                e.Cancel = true;
            }
        }
        private void GridView1_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
        {
            e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
        }
        private void GridView1_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            var row = gridView1.GetRow(e.RowHandle) as VendorCustomer;
            if (row == null) return;
            if (string.IsNullOrEmpty(row.Name))
            {
                gridView1.SetColumnError(gridView1.Columns[nameof(row.Name)],"يجب ادخال اسم الصنف");
                e.Valid = false;
            }
            if (row.Group == 0)
            {
                gridView1.SetColumnError(gridView1.Columns[nameof(row.Group)], "يجب اختيار المجموعة");
                e.Valid = false;
            }
        }
        private void GridView1_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            var row = gridView1.GetRow(e.RowHandle) as VendorCustomer; 
            if (row == null) return;
            row.Group = Convert.ToInt32(Treelkp_Group.EditValue);
        }
        private void btn_New_Click(object sender, EventArgs e)
        {
            vendorCustomers = new BindingList<VendorCustomer>();
            btn_Save.Enabled = gridControl1.Enabled = true;
            memoEdit1.Text = "";
            progressBarControl1.Position = 0;

        }
        void RefreshData()
        {
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
            if (Groups == null)
                Groups = new BindingList<NameAndID>();
            Groups.Clear();
            if (IsCustomer)
                Groups.AddRange(db.Sl_CustomerGroups.Select(x => new NameAndID { ID = x.Number, Name = x.Name }).ToList());
            else
                Groups.AddRange(db.Pr_VendorGroups.Select(x => new NameAndID { ID = x.Number, Name = x.Name }).ToList());

            repoUOM.Items.Clear();

        }
        void BindDataToControls()
        {
            gridControl1.DataSource = vendorCustomers;

            var ins = new VendorCustomer();
            if (IsCustomer)
                gridView1.Columns[nameof(ins.Tax)].Visible = false;

            gridView1.Columns[nameof(ins.Name)].Caption = LangResource.Name;
            gridView1.Columns[nameof(ins.Tax)].Caption = LangResource.TaxFileNumber;
            gridView1.Columns[nameof(ins.Address)].Caption = LangResource.Address;
            gridView1.Columns[nameof(ins.City)].Caption = LangResource.City;
            gridView1.Columns[nameof(ins.Email)].Caption = LangResource.Email;
            gridView1.Columns[nameof(ins.Phone)].Caption = LangResource.Phone;
            gridView1.Columns[nameof(ins.Mobile)].Caption = LangResource.Mobile;
            gridView1.Columns[nameof(ins.Group)].Caption = LangResource.Group;
            gridView1.Columns[nameof(ins.MaxCredit)].Caption = LangResource.MaxCredit;
            gridView1.Columns[nameof(ins.Debit)].Caption = LangResource.Debit;
            gridView1.Columns[nameof(ins.Credit)].Caption = LangResource.Credit;


            Treelkp_Group.Properties.DataSource = Groups;
            Treelkp_Group.Properties.ValueMember = "ID";
            Treelkp_Group.Properties.DisplayMember = "Name";

            RepositoryItemLookUpEdit repoGroup = new RepositoryItemLookUpEdit() { DataSource = Groups, DisplayMember = "Name", ValueMember = "ID" };
            gridControl1.RepositoryItems.Add(repoGroup);
            gridView1.Columns[nameof(ins.Group)].ColumnEdit = repoGroup;
            if (Groups.Count > 0)
                Treelkp_Group.EditValue = Groups[0].ID;
        }
        private void frm_AddProductsRange_Load(object sender, EventArgs e)
        {
            btn_New.PerformClick();
            RefreshData();
            BindDataToControls();
        }
        private void simpleButton3_Click(object sender, EventArgs e)
        {
            Form form = IsCustomer ? new frm_CustomerGroup() : new frm_CustomerGroup();
            Main_frm.OpenForm(form, true);
            RefreshData();
        }
        private void btn_Save_Click(object sender, EventArgs e)
        {
            if (XtraMessageBox.Show(text: "سيتم اضافه البيانات الي قاعده البيانات وانشاء رصيد افتتاحي \n  هل تريد المتابعه ؟",
               caption: "تاكيد عمليه التسجيل", icon: MessageBoxIcon.Question, buttons: MessageBoxButtons.YesNo) != DialogResult.Yes)
                return;

            var data = gridView1.DataSource as BindingList<VendorCustomer>;
            if (data is null || data.Count() == 0)
            {
                if (IsCustomer) XtraMessageBox.Show("يرجي مراجعه العملاء بالجدول والتاكد من تسجيل صنف واحد علي الاقل");
                else
                    XtraMessageBox.Show("يرجي مراجعه الموردين  بالجدول والتاكد من تسجيل صنف واحد علي الاقل");

            }
            btn_Save.Enabled = btn_New.Enabled =gridControl1.Enabled = false;

            worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.DoWork += Worker_DoWork;
            worker.ProgressChanged += Worker_ProgressChanged;
            worker.RunWorkerCompleted += Worker_RunWorkerCompleted;
            worker.WorkerSupportsCancellation = false;
            worker.RunWorkerAsync();
        }
        private void Worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            btn_New.Enabled = true;


            //if (OpenBalancelist.Count() > 0)
            //{
            //    var frm = new frm_Inv_Invoice(MasterClass.InvoiceType.ItemOpenBalance, OpenBalancelist);
            //    frm.Show();
            //    frm.btn_Save.PerformClick();
            //    frm.Close();
            //}
        }
        private void Worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBarControl1.Position = e.ProgressPercentage;
            if(e.UserState is string && (string .IsNullOrEmpty( e.UserState.ToString() ) == false))
            {
                memoEdit1.Text += Environment.NewLine +e.UserState.ToString();
            }
        }

        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            int precint = 0;
            var data = gridView1.DataSource as BindingList<VendorCustomer>; 
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String); 
            //OpenBalancelist = new List<DAL.Inv_InvoiceDetail>();

            foreach (var item in data)
            {
                if (IsCustomer)
                {
                    var Customer = new DAL.Sl_Customer()
                    {
                        ID= item.Id,
                        Address= item.Address,
                        Name= item.Name,
                        City= item.City,
                        EMail= item.Email,
                        GroupID= item.Group,
                        Mobile= item.Mobile ,
                        Phone= item.Phone,
                        MaxCredit = item.MaxCredit
                       
                    };
                   
                    if (db.Sl_Customers.Where(x => x.Name == item.Name).Count() > 0)
                    {
                        worker.ReportProgress(precint, "الاسم موجود بالفعل " + item.Name);
                        Customer = db.Sl_Customers.Where(x => x.Name == item.Name).FirstOrDefault();
                        continue;
                    }
                    else
                    {
                        Customer.Account = Master.InsertNewAccount(Customer.Name, CurrentSession.Company.CustomersAccount);
                        db.Sl_Customers.InsertOnSubmit(Customer);
                        if (item.Debit > 0 || item.Credit > 0)
                            AddBalance(Customer.Account,item);

                    }
                }
                else
                {
                    var Vendor = new DAL.Pr_Vendor()
                    {
                        ID = item.Id,
                        Address = item.Address,
                        Name = item.Name,
                        City = item.City,
                        EMail = item.Email,
                        GroupID = item.Group,
                        Mobile = item.Mobile,
                        Phone = item.Phone,
                        MaxCredit = item.MaxCredit,
                    };
                    
                    if (db.Pr_Vendors.Where(x => x.Name == item.Name).Count() > 0)
                    {
                        worker.ReportProgress(precint, "اسم الصنف موجود بالفعل" + item.Name);
                        Vendor = db.Pr_Vendors.Where(x => x.Name == item.Name).FirstOrDefault();
                        continue;
                    }
                    else
                    {
                        Vendor.Account = Master.InsertNewAccount(Vendor.Name, CurrentSession.Company.CustomersAccount);
                        db.Pr_Vendors.InsertOnSubmit(Vendor);
                        if (item.Debit > 0 || item.Credit > 0)
                            AddBalance(Vendor.Account, item);
                    }
                }
               
                db.SubmitChanges();

               
                worker.ReportProgress(precint);
            }
            string msg1 = "جاري رفع العملاء الي قاعده البيانات";
            string msg2 = "تم حفظ اضافه العملاء بنجاح";
            if (!IsCustomer)
            {
                msg1 = "جاري رفع الموردين الي قاعده البيانات";
                msg2 = "تم حفظ اضافه الموردين بنجاح";
            }
            worker.ReportProgress(100, msg1);
            worker.ReportProgress(100, msg2);
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if ((Treelkp_Group.EditValue is int id && id > 0) != true)
            {
                Treelkp_Group.ErrorText = "يجب اختيار فئه افتراضيه";
                return;
            }
            new frm_ImportFromExcelVendorCustomer(IsCustomer, ref vendorCustomers, Convert.ToInt32(Treelkp_Group.EditValue)).ShowDialog();
            RefreshData();
        }
  
        void AddBalance(int AccountId, VendorCustomer vendorCustomer)
        {
            Master.AccountOpenBalance OP = new Master.AccountOpenBalance();
            OP.IsDebit = (vendorCustomer.Debit > 0);
            OP.Amount = (vendorCustomer.Credit > 0) ? vendorCustomer.Credit : vendorCustomer.Debit;
            Master.CreatAccOpenBalance(AccountId, vendorCustomer.Name, OP);
           
        }
        public class VendorCustomer
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public int Group { get; set; }
            public string Tax { get; set; }

            public string City { get; set; }
            public string Address { get; set; }
            public string Phone { get; set; }
            public string Mobile { get; set; }
            public string Email { get; set; }
            public int MaxCredit { get; set; }
            public double Credit { get; set; }
            public double Debit { get; set; }
        }
        public class NameAndID
        {
            public int ID { get; set; }
            public string Name { get; set; }
        }

    }
}
