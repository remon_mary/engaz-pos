﻿using ByStro.Clases;
using ByStro.DAL;
using ByStro.PL;
using CustomControls.Models;
using DevExpress.DataProcessing;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace ByStro.Forms
{
    public partial class frm_AddInvoiceRange : XtraForm
    {
        BackgroundWorker worker = new BackgroundWorker();
        BindingList<Invoice>  invoices;
        bool IsSales = true;
        public frm_AddInvoiceRange(bool _IsSales)
        {
            InitializeComponent();
            if (!_IsSales)
            {
                this.IsSales = _IsSales;
                this.Text = "اضافه مجموعه من فواتير الشراء";
                layoutControlItem24.Text = "المورد";
            }
            gridView1.InitNewRow += GridView1_InitNewRow;
            gridView1.ValidateRow += GridView1_ValidateRow;
            gridView1.InvalidRowException += GridView1_InvalidRowException;
            this.FormClosing += Frm_AddProductsRange_FormClosing;
        }
        private void Frm_AddProductsRange_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(worker != null && worker.IsBusy)
            {
                e.Cancel = true;
            }
        }
        private void GridView1_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
        {
            e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
        }
        private void GridView1_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            var row = gridView1.GetRow(e.RowHandle) as Invoice;
            if (row == null) return;
            if (string.IsNullOrEmpty(row.BarCodeItem))
            {
                gridView1.SetColumnError(gridView1.Columns[nameof(row.BarCodeItem)], "يجب ادخال باركود الصنف");
                e.Valid = false;
            }

        }
        private void GridView1_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            var row = gridView1.GetRow(e.RowHandle) as Invoice;
            if (row == null) return;
        }
        private void btn_New_Click(object sender, EventArgs e)
        {
            invoices = new BindingList<Invoice>();
            btn_Save.Enabled = gridControl1.Enabled = true;
            memoEdit1.Text = "";
            progressBarControl1.Position = 0;

        }

        void BindDataToControls()
        {
            gridControl1.DataSource = invoices;

            var ins = new Invoice();


            gridView1.Columns[nameof(ins.Id)].Caption = LangResource.ID;
            gridView1.Columns[nameof(ins.ItemQty)].Caption = LangResource.Quantity;
            gridView1.Columns[nameof(ins.Price)].Caption = LangResource.Price;
            gridView1.Columns[nameof(ins.TaxValue)].Caption = LangResource.TaxValue;
            gridView1.Columns[nameof(ins.DiscountValue)].Caption = LangResource.DiscountValue;
            gridView1.Columns[nameof(ins.Code)].Caption = LangResource.Code;
            gridView1.Columns[nameof(ins.BarCodeItem)].Caption = LangResource.BarCode;

        }
        private void frm_AddProductsRange_Load(object sender, EventArgs e)
        {

            using (DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String))
            {
                var store = (from s in db.Inv_Stores select new { ID = s.ID, Name = s.Name }).ToList();
                LookUpEdit_StoreId.Properties.DataSource = store;
                LookUpEdit_StoreId.Properties.ValueMember = "ID";
                LookUpEdit_StoreId.Properties.DisplayMember = "Name";

                if (IsSales)
                    LookUpEdit_PartID.Properties.DataSource = (from s in db.Sl_Customers select new { ID = s.ID, Name = s.Name }).ToList();
                else
                    LookUpEdit_PartID.Properties.DataSource = (from s in db.Pr_Vendors select new { ID = s.ID, Name = s.Name }).ToList();

                LookUpEdit_PartID.Properties.ValueMember = "ID";
                LookUpEdit_PartID.Properties.DisplayMember = "Name";
            }
            


            btn_New.PerformClick();
            BindDataToControls();
        }
        #region Parts

        private void PartChanged()
        {
            using (DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String))
            {
                if (IsSales)
                {
                    LookUpEdit_PartID.Properties.DataSource = db.Customers.Select(c => new { ID = c.CustomerID, Name = c.CustomerName, c.Address, c.Phone }).ToList();
                    LookUpEdit_PartID.EditValue = db.Customers.Select(x => ((int?)x.CustomerID) ?? 0).FirstOrDefault();
                }
                else
                {

                    LookUpEdit_PartID.Properties.DataSource = db.Suppliers.Select(c => new { ID = c.SupplierID, Name = c.SupplierName, c.Address, c.Phone }).ToList();
                    LookUpEdit_PartID.EditValue = db.Suppliers.Select(x => ((int?)x.SupplierID) ?? 0).FirstOrDefault();
                }
            }
        }
        private void simpleButton2_Click(object sender, EventArgs e)
        {
            Main_frm.OpenForm(new frm_Stores(), true);
        }
        private void simpleButton3_Click(object sender, EventArgs e)
        {
            Form form=null;
            if(IsSales) 
                    form = new frm_Customer();
            else
                form = new frm_Vendor();

            if (form != null)
                Main_frm.OpenForm(form, true);
            PartChanged();
        }
        int PartId(string Name, bool IsCustomer)
        {
            using (DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String))
            {

                if (IsCustomer)
                {
                    var Customer = new DAL.Sl_Customer()
                    {
                        Name = Name,
                        GroupID = db.Sl_CustomerGroups.FirstOrDefault().Number
                    };

                    if (db.Sl_Customers.Where(x => x.Name == Name).Count() > 0)
                    {
                        return db.Sl_Customers.Where(x => x.Name == Name).FirstOrDefault().ID;
                    }
                    else
                    {
                        Customer.Account = Master.InsertNewAccount(Customer.Name, CurrentSession.Company.CustomersAccount);
                        db.Sl_Customers.InsertOnSubmit(Customer);
                        db.SubmitChanges();
                        return Customer.ID;
                    }
                }
                else
                {
                    var Vendor = new DAL.Pr_Vendor()
                    {
                        Name = Name,
                        
                    };
                    Vendor.GroupID = db.Pr_VendorGroups.FirstOrDefault().Number;
                    if (db.Pr_Vendors.Where(x => x.Name == Name).Count() > 0)
                    {
                        return db.Pr_Vendors.Where(x => x.Name == Name).FirstOrDefault().ID;
                    }
                    else
                    {
                        Vendor.Account = Master.InsertNewAccount(Vendor.Name, CurrentSession.Company.VendorsAccount);
                        db.Pr_Vendors.InsertOnSubmit(Vendor);
                        db.SubmitChanges();
                        return Vendor.ID;
                    }
                }

            }


        }

        #endregion
        
        private void btn_Save_Click(object sender, EventArgs e)
        {
            if (!IsSelectDefaultData()) return;
            if (XtraMessageBox.Show(text: "سيتم اضافه البيانات الي قاعده البيانات  \n  هل تريد المتابعه ؟",
               caption: "تاكيد عمليه التسجيل", icon: MessageBoxIcon.Question, buttons: MessageBoxButtons.YesNo) != DialogResult.Yes)
                return;

            var data = gridView1.DataSource as BindingList<Invoice>;
            if (data is null || data.Count() == 0)
            {
                if (IsSales) XtraMessageBox.Show("يرجي مراجعه العملاء بالجدول والتاكد من تسجيل صنف واحد علي الاقل");
                else
                    XtraMessageBox.Show("يرجي مراجعه الموردين  بالجدول والتاكد من تسجيل صنف واحد علي الاقل");

            }
            btn_Save.Enabled = btn_New.Enabled =gridControl1.Enabled = false;

            worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.DoWork += Worker_DoWork;
            worker.ProgressChanged += Worker_ProgressChanged;
            worker.RunWorkerCompleted += Worker_RunWorkerCompleted;
            worker.WorkerSupportsCancellation = false;
            worker.RunWorkerAsync();
        }
        private void Worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            btn_New.Enabled = true;
        }
        private void Worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBarControl1.Position = e.ProgressPercentage;
            if(e.UserState is string && (string .IsNullOrEmpty( e.UserState.ToString() ) == false))
            {
                memoEdit1.Text += Environment.NewLine +e.UserState.ToString();
            }
        }
       
        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            int precint = 0;
            var data = gridView1.DataSource as BindingList<Invoice>;
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);

            var Invoices = data.GroupBy(x => x.Id);

            foreach (var Invoice in Invoices)
            {
                Collection<Inv_InvoiceDetail> invoiceDetails = new Collection<Inv_InvoiceDetail>();
                foreach (var item in Invoice)
                {
                    ByStro.DAL.Prodecut product = db.Prodecuts.SingleOrDefault(x => x.FiestUnitBarcode == item.BarCodeItem || x.SecoundUnitBarcode == item.BarCodeItem || x.ThreeUnitBarcode == item.BarCodeItem);
                    if (product == null) continue;
                    DAL.Inv_InvoiceDetail inv_InvoiceDetail = new Inv_InvoiceDetail
                    {
                        StoreID = LookUpEdit_StoreId.EditValue.ToInt(),
                        ItemQty= item.ItemQty,
                        ItemID= product.ProdecutID,
                        ItemUnitID = (product.FiestUnitBarcode == item.BarCodeItem) ? 1 : (product.SecoundUnitBarcode == item.BarCodeItem) ? 2 : 3,
                        DiscountValue= item.DiscountValue,
                        AddTaxVal= item.TaxValue,
                        TotalPrice = ((item.Price * item.ItemQty) + item.TaxValue) - item.DiscountValue,
                        Price = item.Price, 
                    };
                    inv_InvoiceDetail.AddTax = inv_InvoiceDetail.AddTaxVal / (item.Price * item.ItemQty);
                    inv_InvoiceDetail.Discount = inv_InvoiceDetail.DiscountValue / (item.Price * item.ItemQty);
                    invoiceDetails.Add(inv_InvoiceDetail);
                }
                var inv_Invoice = new DAL.Inv_Invoice()
                {
                    Code = Invoice.FirstOrDefault().Code,
                    Date = DateTime.UtcNow,
                    LastUpdateDate = DateTime.UtcNow,
                    PartType = (byte)(IsSales ? 1 : 2),
                    StoreID = LookUpEdit_StoreId.EditValue.ToInt(),
                    PartID = LookUpEdit_PartID.EditValue.ToInt(),
                    //DiscountRatio = invoiceDetails.Sum(x=>(double?)x.Discount??0),
                    //DiscountValue = invoiceDetails.Sum(x => (double?)x.DiscountValue ?? 0),
                    //TotalRevenue = 0,
                    Total = invoiceDetails.Sum(x => (double?)x.TotalPrice ?? 0),
                    Net = invoiceDetails.Sum(x => (double?)x.TotalPrice ?? 0),
                    Paid = invoiceDetails.Sum(x => (double?)x.TotalPrice ?? 0),
                    InvoiceType = (byte?)(IsSales ? MasterClass.InvoiceType.SalesInvoice : MasterClass.InvoiceType.PurchaseInvoice),
                    UserID = CurrentSession.User.ID
                };
                frm_Inv_Invoice.SaveInvoice(true, inv_Invoice, invoiceDetails,true);

                worker.ReportProgress(precint);
            }
            string msg1 = "جاري رفع الفواتير الي قاعده البيانات";
            string msg2 = "تم حفظ الفواتير بنجاح";
            
            worker.ReportProgress(100, msg1);
            worker.ReportProgress(100, msg2);
        }
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (IsSelectDefaultData())
                new frm_ImportFromExcelnvoice(ref invoices).ShowDialog();
        }
        bool IsSelectDefaultData()
        {
            if ((LookUpEdit_PartID.EditValue is int PartID && PartID > 0) != true)
            {
                LookUpEdit_PartID.ErrorText = "يجب اختيار طرف التعامل الافتراضي";
                return false;
            }
            if ((LookUpEdit_StoreId.EditValue is int StoreId && StoreId > 0) != true)
            {
                LookUpEdit_StoreId.ErrorText = "يجب اختيار المخزن الافتراضي";
                return false;
            }
            return true;

        }
        public class Invoice
        {
            public int Id { get; set; }
            public string Code { get; set; }
            public string BarCodeItem { get; set; }
            public double ItemQty { get; set; }
            public double Price { get; set; }
            public double DiscountValue { get; set; }
            public double TaxValue { get; set; }
        }
        public class NameAndID
        {
            public int ID { get; set; }
            public string Name { get; set; }
        }

       
    }
}
