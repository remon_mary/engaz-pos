﻿using ByStro.Clases;
using ByStro.DAL;
using ByStro.QueryViews;
using ByStro.ReportModules;
using ByStro.RPT;
using DevExpress.Data;
using DevExpress.DataProcessing;
using DevExpress.LookAndFeel;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraLayout;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks; 
using System.Windows.Forms;
using static ByStro.Clases.MasterClass;
using static ByStro.Forms.frm_Report;
using static ByStro.ReportModules.ItemsSoldToCustomers;

namespace ByStro.Forms
{
    public partial class frm_EditReportFilters : XtraForm 
    {
        ReportType reportType;
        PartTypes  partTypes;

        public frm_EditReportFilters(ReportType _reportType, PartTypes _partTypes=PartTypes.Customer)
        {
            InitializeComponent();
            cb_DateFilterType.SelectedIndex = 4;
            reportType = _reportType;
            partTypes = _partTypes;
            lyc_Products.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            lyc_Stores.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            lyc_DatefilterType.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            lyc_Customers.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;

            switch (partTypes)
            {
                case PartTypes.Vendor:
                    this.lyc_Customers.Text = "الموردين";
                    break;
                case PartTypes.Customer:
                    this.lyc_Customers.Text = "العملاء";
                    break;
                case PartTypes.Employee:
                    this.lyc_Customers.Text = "الموظفين";
                    break;
                case PartTypes.Account:
                    this.lyc_Customers.Text = "الحسابات";
                    break;
                default:
                    break;
            }
            switch (reportType)
            {
                case ReportType.ProductMovment:
                    lyc_Products.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    lyc_Stores.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    lyc_DatefilterType.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                
                    break;
                case ReportType.ProductBalance :
                    lyc_Products.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    lyc_Stores.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    lyc_DatefilterType.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    cb_DateFilterType.SelectedIndex = 2;
                    cb_DateFilterType.ReadOnly = true;
                    lyc_EndDate.Text = "التاريخ";
                    dt_End.DateTime = DateTime.Now;
                    break;
                case ReportType.ProductReachedReorderLevel:
                    lyc_Products.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    lyc_Stores.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    lyc_DatefilterType.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    cb_DateFilterType.SelectedIndex = 2;
                    cb_DateFilterType.ReadOnly = true;
                    lyc_EndDate.Text = "التاريخ";
                    dt_End.DateTime = DateTime.Now;
                    break;
                case ReportType.ProductExpire:
                    lyc_Products.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    lyc_Stores.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    lyc_DatefilterType.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never ;
                    break;

                case ReportType.ProductCardWithQty :
                    lyc_Products.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                   // lyc_Stores.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    lyc_DatefilterType.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;

                    break;
                case ReportType.TotalSoldItemsToCustomer:
                    lyc_DatefilterType.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    lyc_Customers.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always ;
                    lyc_Products.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;


                    break;
                case ReportType.CustomerInvoiceDetails:
                    lyc_DatefilterType.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    lyc_Customers.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    break;
                case ReportType.FullInvoiceBy:
                    lyc_DatefilterType.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    lyc_Customers.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    break; 
                 
                default:
                    throw new NotImplementedException();
            }

            GetData();
        }
         DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String); 
        void  GetData()
        { 
            cb_Products.Properties.DataSource =  db.Prodecuts.Select(x => new { ID = x.ProdecutID, Name = x.ProdecutName }).ToList();
            cb_Stores.Properties.DataSource = db.Inv_Stores.Select(x => new { ID = x.ID, Name = x.Name }).ToList();
            switch (partTypes)
            {
                case PartTypes.Vendor:
                    lkp_Customers.Properties.DataSource = db.Pr_Vendors.Select(x =>
                              new CustomerView
                              {
                                  ID = x.ID,
                                  Name = x.Name,
                                  Phone = x.Phone,
                                  Address = x.Address,
                              }); break;
                case PartTypes.Customer:
                    lkp_Customers.Properties.DataSource = db.Sl_Customers.Select(x =>
                              new CustomerView
                              {
                                  ID = x.ID,
                                  Name = x.Name,
                                  Phone = x.Phone,
                                  Address = x.Address,
                              });
                    break;
                default:
                    break;
            }
          
            lkp_Customers.Properties.DisplayMember = "Name";
            lkp_Customers.Properties.ValueMember = "ID";
            lkp_Customers.Properties.NullText = "";
            lkp_Customers.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;

            lkp_Customers.Properties.ValidateOnEnterKey = true;
            lkp_Customers.Properties.AllowNullInput = DefaultBoolean.False;
            lkp_Customers.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            lkp_Customers.Properties.ImmediatePopup = true;
            lkp_Customers.Closed += Lkp_Customers_Closed;
            var repoView = lkp_Customers.Properties.View;


            lkp_Customers.CustomDisplayText += Lkp_Customers_CustomDisplayText;
            repoView.FocusRectStyle = DrawFocusRectStyle.RowFullFocus;
            repoView.OptionsSelection.UseIndicatorForSelection = true;
            repoView.OptionsView.ShowAutoFilterRow = true;
            repoView.OptionsView.ShowFilterPanelMode = ShowFilterPanelMode.ShowAlways;
            repoView.PopulateColumns(lkp_Customers.Properties.DataSource);
           
            repoView.Columns["ID"].Caption = "كود";
            repoView.Columns["Name"].Caption = "الاسم";
            repoView.Columns["Phone"].Caption = "الهاتف";
            repoView.Columns["Phone1"].Caption = "موبيل";
            repoView.Columns["Address"].Caption = "العنوان";
            if (reportType != ReportType.CustomerInvoiceDetails && reportType != ReportType.FullInvoiceBy)
            {
                repoView.OptionsSelection.MultiSelectMode = GridMultiSelectMode.CheckBoxRowSelect;
                repoView.OptionsSelection.MultiSelect = true;
            }
        }

        private void Lkp_Customers_Closed(object sender, ClosedEventArgs e)
        {
            lkp_Customers.EditValue = "";
            lkp_Customers.EditValue = null;
        }

        private void Lkp_Customers_CustomDisplayText(object sender, CustomDisplayTextEventArgs e)
        {
            e.DisplayText = "";
            var repoView = lkp_Customers.Properties.View;
            var datasource = repoView.DataSource as IList <CustomerView>;
            if (datasource == null)
                return;
            var selectedRowsHandels = repoView.GetSelectedRows();
            selectedRowsHandels.ToList().ForEach(x =>
            {
                e.DisplayText +=((CustomerView)repoView.GetRow(x)).Name +" , ";
            });

        }

        private int[] GetSelectedCustomer()
        {
            var repoView = lkp_Customers.Properties.View;
            var datasource = repoView.DataSource as IList<CustomerView>;
            if (datasource == null)
                return null ;
            var selectedRowsHandels = repoView.GetSelectedRows();
            var customerArray = new int[selectedRowsHandels.Count()];
            int index = 0;
            selectedRowsHandels.ToList().ForEach(x =>
            {
                customerArray[index] = ((CustomerView)repoView.GetRow(x)).ID;
                index++;
            });
            return customerArray;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            var products = cb_Products.Properties.Items.Where(x => x.CheckState == CheckState.Checked).Select(x => Convert.ToInt32(x.Value)).ToList();
            var stores = cb_Stores.Properties.Items.Where(x => x.CheckState == CheckState.Checked).Select(x => Convert.ToInt32(x.Value)).ToList();
            var customers = GetSelectedCustomer();

            var filterString = "";
            filterString += lyc_Products.Text + ":" + cb_Products.Text;
            filterString += " ;" + lyc_Stores.Text + ":" + cb_Products.Text;
            if (lyc_StartDate .Visible  && dt_Start .DateTime .Year < 1950)
            {
                dt_Start.ErrorText = "يجب اختيار التاريخ";
                return;
            }
            if (lyc_EndDate .Visible && dt_End.DateTime.Year < 1950)
            {
                dt_End.ErrorText = "يجب اختيار التاريخ";
                return;
            }
            if (lyc_OnDate.Visible && dt_OnDate .DateTime.Year < 1950)
            {
                dt_OnDate .ErrorText = "يجب اختيار التاريخ";
                return;
            }

            var ReportForm =  new frm_Report(reportType);
            switch (reportType)
            {
                 
                case ReportType.CustomerInvoiceDetails:
                    CustomerInvoiceDetails(ReportForm);
                    break;  
                case ReportType.FullInvoiceBy:
                    FullInvoiceBy(ReportForm);
                    break;
              
                    #region ProductMovment
                case ReportType.ProductMovment:
                    ReportForm.btn_Refresh.ItemClick += (bs, be) =>
                    {
                        var source = db.Inv_StoreLogs.Select(x => x);
                        if(products.Count > 0)
                            source = source.Where(x => products.Contains(x.ItemID.Value));
                        if (stores.Count > 0)
                            source = source.Where(x => stores.Contains(x.StoreID.Value));
                        if (lyc_EndDate.Visible)
                            source = source.Where(x => x.date.Value.Date <= dt_OnDate.DateTime.Date);
                        if (lyc_StartDate.Visible)
                            source = source.Where(x => x.date.Value.Date >= dt_Start.DateTime.Date);
                        if (lyc_OnDate.Visible)
                            source = source.Where(x => x.date.Value.Date == dt_OnDate.DateTime.Date);




                        ReportForm.gridControl1.DataSource = source.OrderBy(isl => isl.date).Select(s=>new { 
                        ProductName = db.Prodecuts.Single  (x=>x.ProdecutID == s.ItemID ).ProdecutName ,
                        StoreName = db.Inv_Stores.Single(x=>x.ID == s.StoreID.Value ).Name ,
                        Date = s.date, 
                        s.ItemQuIN ,
                        s.ItemQuOut ,
                       BalanceToDate = db.Inv_StoreLogs.Where(isl => isl.ItemID == s.ItemID && isl.Color.GetValueOrDefault() == s.Color.GetValueOrDefault() &&
                                                         isl.Size.GetValueOrDefault() == s.Size.GetValueOrDefault() && isl.date <= s.date).Sum(isl => isl.ItemQuIN)
                                     - db.Inv_StoreLogs.Where(isl => isl.ItemID == s.ItemID && isl.Color.GetValueOrDefault() == s.Color.GetValueOrDefault() && 
                                                        isl.Size.GetValueOrDefault() == s.Size.GetValueOrDefault() && isl.date <= s.date).Sum(isl => isl.ItemQuOut)
,
                        s.BuyPrice, 
                        TotalCost= s.BuyPrice* (s.ItemQuIN + s.ItemQuOut),
                        TotalPrice = s.SellPrice  * (s.ItemQuIN + s.ItemQuOut),

                            s.SellPrice ,
                        s.Serial ,
                        Color = db.Inv_Colors .SingleOrDefault(x=>x.ID ==s.Color).Name ??"",
                        Size =db.Inv_Sizes .SingleOrDefault(x => x.ID == s.Size ).Name ?? "",
                        s.ExpDate ,
                        s.Note ,
                        s.Type ,
                        s.TypeID ,

                        }).ToList();
                  
                   };
                    ReportForm.gridView1.RowCellStyle += (rs ,re) => { 
                        if(re.RowHandle < 0)return ;

                        if (re.Column.FieldName == "ItemQuIN" || re.Column.FieldName == "ItemQuOut")
                        {
                            if (Convert.ToDouble( ReportForm.gridView1.GetRowCellValue(re.RowHandle , "ItemQuIN")) > 0 )
                                re.Appearance.BackColor = DXSkinColors.FillColors.Success;
                            else
                                re.Appearance.BackColor = DXSkinColors.FillColors.Warning; 
                        }else if (re.Column.FieldName == "BalanceToDate"  )
                            {
                            if(re.CellValue is double d && d < 0)
                            {
                                re.Appearance.BackColor = DXSkinColors.FillColors.Warning;

                            }
                            else if (re.CellValue is double dd && dd > 0)
                            {
                                re.Appearance.BackColor = DXSkinColors.FillColors.Success;

                            }
                        }
                        };
                    ReportForm.gridView1.CustomColumnDisplayText += (rs, re) =>
                    {
                        if(re.ListSourceRowIndex<0)return ;
                        if (re.Column.FieldName == "Type")
                            re.DisplayText =
                            MasterClass.SystemProssessCaption[ReportForm.gridView1.GetRowCellValue(ReportForm.gridView1.GetRowHandle( re.ListSourceRowIndex ),"Type").ToInt()];
                    };
                    
                    ReportForm.btn_Refresh.PerformClick();
                    ReportForm.Text = ReportForm.gridView1.ViewCaption = "حركه الاصناف بالمخازن";
                    ReportForm.gridView1.Columns["ProductName"].Caption = "اسم الصنف";
                    ReportForm.gridView1.Columns["StoreName"].Caption = "المخزن";
                    ReportForm.gridView1.Columns["Date"].Caption = "التاريخ";
                    ReportForm.gridView1.Columns["ItemQuIN"].Caption = " وارد ";
                    ReportForm.gridView1.Columns["ItemQuOut"].Caption = "منصرف";
                    ReportForm.gridView1.Columns["BalanceToDate"].Caption = "الرصيد حتي التاريخ";
                    ReportForm.gridView1.Columns["TotalCost"].Caption = "اجمالي التكلفه";
                    ReportForm.gridView1.Columns["TotalPrice"].Caption = "اجمالي السعر";
                    ReportForm.gridView1.Columns["BuyPrice"].Caption = "سعر التكلفه";
                    ReportForm.gridView1.Columns["SellPrice"].Caption = "السعر";
                    ReportForm.gridView1.Columns["Serial"].Caption = "السريال";
                    ReportForm.gridView1.Columns["Color"].Caption = "اللون";
                    ReportForm.gridView1.Columns["Size"].Caption = "الحجم";
                    ReportForm.gridView1.Columns["ExpDate"].Caption = "الصلاحيه";
                    ReportForm.gridView1.Columns["Note"].Caption = "البيان";
                    ReportForm.gridView1.Columns["Type"].Caption = "المصدر";
                    ReportForm.gridView1.Columns["TypeID"].Caption = "كود المصدر";

                    GridColumnSummaryItem BuyPriceAvg = new GridColumnSummaryItem();
                    BuyPriceAvg.SummaryType = SummaryItemType.Average;
                    BuyPriceAvg.FieldName = "BuyPrice";
                    BuyPriceAvg.DisplayFormat = "متوسط التكلفه: {0:#.#}";
                    ReportForm.gridView1.Columns["BuyPrice"].Summary.Clear();
                    ReportForm.gridView1.Columns["BuyPrice"].Summary.Add(BuyPriceAvg);
                    GridColumnSummaryItem SellPriceAvg = new GridColumnSummaryItem();
                    SellPriceAvg.SummaryType = SummaryItemType.Average;
                    SellPriceAvg.FieldName = "SellPrice";
                    SellPriceAvg.DisplayFormat = " متوسط السعر: {0:#.#}";
                    ReportForm.gridView1.Columns["SellPrice"].Summary.Clear();
                    ReportForm.gridView1.Columns["SellPrice"].Summary.Add(SellPriceAvg);
                    GridColumnSummaryItem TottalCostSum = new GridColumnSummaryItem();
                    TottalCostSum.SummaryType = SummaryItemType.Sum;
                    TottalCostSum.FieldName = "TotalCost";
                    TottalCostSum.DisplayFormat = "الاجمالي: {0:#.#}";
                    ReportForm.gridView1.Columns["TotalCost"].Summary.Clear();
                    ReportForm.gridView1.Columns["TotalCost"].Summary.Add(TottalCostSum);
                    GridColumnSummaryItem TotalPriceSum = new GridColumnSummaryItem();
                    TotalPriceSum.SummaryType = SummaryItemType.Sum;
                    TotalPriceSum.FieldName = "TotalPrice";
                    TotalPriceSum.DisplayFormat = "الاجمالي: {0:#.#}";
                    ReportForm.gridView1.Columns["TotalPrice"].Summary.Clear();
                    ReportForm.gridView1.Columns["TotalPrice"].Summary.Add(TotalPriceSum); 
                    break;
                #endregion

            
                    #region ProductBalance
                case ReportType.ProductBalance:
                    ReportForm.btn_Refresh.ItemClick += (bs, be) =>
                    {
                        var source = db.Inv_StoreLogs.Select(x => x);
                        if (products.Count > 0)
                            source = source.Where(x => products.Contains(x.ItemID.Value));
                        if (stores.Count > 0)
                            source = source.Where(x => stores.Contains(x.StoreID.Value));
                        if (lyc_EndDate.Visible)
                            source = source.Where(x => x.date.Value.Date <= dt_End .DateTime.Date);
                        if (lyc_StartDate.Visible)
                            source = source.Where(x => x.date.Value.Date >= dt_Start.DateTime.Date);
                        if (lyc_OnDate.Visible)
                            source = source.Where(x => x.date.Value.Date == dt_OnDate.DateTime.Date);
                        var rowdata =
                           source.OrderBy(isl => isl.date)
                           .GroupBy(isl => new { isl.ItemID, isl.Color, isl.Size, isl.StoreID })
                           .Select(isl => new {
                               Prodecut = db.Prodecuts.Single(x => x.ProdecutID == isl.Key.ItemID),
                               Store = db.Inv_Stores.Single(x => x.ID == isl.Key.StoreID.Value),
                               Color = db.Inv_Colors.SingleOrDefault(x => x.ID == isl.Key.Color).Name ?? "",
                               Size = db.Inv_Sizes.SingleOrDefault(x => x.ID == isl.Key.Size).Name ?? "",
                               Balance = isl.Sum(i => i.ItemQuIN) - isl.Sum(i => i.ItemQuOut),
                               Date = dt_End.DateTime.Date
                           }).ToList();

                        var rowdata2 = rowdata.Select(r => new
                        {
                            ProductName = r.Prodecut.ProdecutName,
                            //TODO Get Balance for Unit 
                            Units = CustomUnit.GetUnits(r.Prodecut),
                            StoreName = r.Store.Name,
                            CostValue = GetCostOfNextProduct(r.Prodecut.ProdecutID, r.Store.ID) * r.Balance,
                            BuyPrice = (r.Prodecut.ProdecutBayPrice) * r.Balance,
                            SellPrice = (r.Prodecut.FiestUnitFactor == "1") ? r.Prodecut.FiestUnitPrice1 * r.Balance
                           : (r.Prodecut.SecoundUnitFactor == "1") ? r.Prodecut.SecoundUnitPrice1 * r.Balance
                           : r.Prodecut.ThreeUnitPrice1 * r.Balance,
                            r.Color,
                            r.Size,
                            r.Balance,
                            UnitBalance = CustomUnit.GetBalance(r.Prodecut),
                            r.Date
                        });

                        ReportForm.gridControl1.ViewRegistered += (ss,ee)=> {
                            var view = ee.View as GridView;
                            if (view != null && view.LevelName == "Units")
                            {
                                view.ViewCaption = "تفاصيل وحدات الصنف";
                                view.OptionsView.ShowViewCaption = true;
                                if (view.Columns["BalanceString"] != null)
                                    view.Columns["BalanceString"].Visible = false;
                            }
                        };

                        ReportForm.gridControl1.DataSource = rowdata2.Select(r => new
                        {
                            r.ProductName ,
                            r.Units ,
                            r.StoreName,
                            r.CostValue ,
                            r.BuyPrice ,
                            r.SellPrice ,
                            ProfitEst = r.SellPrice - r.CostValue,
                            r.Color,
                            r.Size,
                            r.Balance,
                            r.UnitBalance,
                            r.Date ,
                        }).ToList();
                    };
                    ReportForm.gridView1.RowCellStyle += (rs, re) => {
                        if (re.RowHandle < 0) return; 
                        else if (re.Column.FieldName == "Balance")
                        {
                            if (re.CellValue is double d && d < 0)
                                re.Appearance.BackColor = DXSkinColors.FillColors.Warning;
                            else if (re.CellValue is double dd && dd > 0)
                                re.Appearance.BackColor = DXSkinColors.FillColors.Success;
                        }
                    }; 
                    ReportForm.btn_Refresh.PerformClick();
                    ReportForm.gridView1.PopulateColumns(ReportForm.gridControl1.DataSource);
                    ReportForm.Text = ReportForm.gridView1.ViewCaption = "ارصده الاصناف بالمخازن";
                    ReportForm.gridView1.Columns["ProductName"].Caption = "اسم الصنف";
                    ReportForm.gridView1.Columns["StoreName"].Caption = "المخزن";
                    ReportForm.gridView1.Columns["Date"].Caption = "التاريخ"; 
                    ReportForm.gridView1.Columns["Balance"].Caption = "الرصيد ";
                    ReportForm.gridView1.Columns["UnitBalance"].Caption = "الرصيد مقسم";
                    ReportForm.gridView1.Columns["CostValue"].Caption = "التكلفه";
                    ReportForm.gridView1.Columns["SellPrice"].Caption = "بيع";
                    ReportForm.gridView1.Columns["BuyPrice"].Caption = "شراء";
                    ReportForm.gridView1.Columns["ProfitEst"].Caption = "ارباح مقدره";

                    ReportForm.gridView1.Columns["Color"].Caption = "اللون";
                    ReportForm.gridView1.Columns["Size"].Caption = "الحجم";

                    GridColumnSummaryItem BalanceSum = new GridColumnSummaryItem();
                    BalanceSum.SummaryType = SummaryItemType.Sum ;
                    BalanceSum.FieldName = "Balance";
                    BalanceSum.DisplayFormat = "{0#.#}";
                    ReportForm.gridView1.Columns["Balance"].Summary.Clear();
                    ReportForm.gridView1.Columns["Balance"].Summary.Add(BalanceSum); 

                    TotalPriceSum = new GridColumnSummaryItem();
                    TotalPriceSum.SummaryType = SummaryItemType.Sum;
                    TotalPriceSum.FieldName = "SellPrice";
                    TotalPriceSum.DisplayFormat = "{0#.#}";
                    ReportForm.gridView1.Columns["SellPrice"].Summary.Clear();
                    ReportForm.gridView1.Columns["SellPrice"].Summary.Add(TotalPriceSum);

                    GridColumnSummaryItem TotalCostValue = new GridColumnSummaryItem();
                    TotalCostValue.SummaryType = SummaryItemType.Sum;
                    TotalCostValue.FieldName = "CostValue";
                    TotalCostValue.DisplayFormat = "{0#.#}";
                    ReportForm.gridView1.Columns["CostValue"].Summary.Clear();
                    ReportForm.gridView1.Columns["CostValue"].Summary.Add(TotalCostValue);

                    GridColumnSummaryItem TotalBuyPrice = new GridColumnSummaryItem();
                    TotalBuyPrice.SummaryType = SummaryItemType.Sum;
                    TotalBuyPrice.FieldName = "BuyPrice";
                    TotalBuyPrice.DisplayFormat = "{0#.#}";
                    ReportForm.gridView1.Columns["BuyPrice"].Summary.Clear();
                    ReportForm.gridView1.Columns["BuyPrice"].Summary.Add(TotalBuyPrice);


                    GridColumnSummaryItem ProfitEst = new GridColumnSummaryItem();
                    ProfitEst.SummaryType = SummaryItemType.Sum;
                    ProfitEst.FieldName = "ProfitEst";
                    ProfitEst.DisplayFormat = "{0#.#}";
                    ReportForm.gridView1.Columns["ProfitEst"].Summary.Clear();
                    ReportForm.gridView1.Columns["ProfitEst"].Summary.Add(ProfitEst);

                    break;
                #endregion

            
                    #region ProductExpire
                case ReportType.ProductExpire :
                    ReportForm.btn_Refresh.ItemClick += (bs, be) =>
                    {
                        var source = db.Inv_StoreLogs.Select(x => x);
                        if (products.Count > 0)
                            source = source.Where(x => products.Contains(x.ItemID.Value));
                        if (stores.Count > 0)
                            source = source.Where(x => stores.Contains(x.StoreID.Value));
                        source = source.Where(s => s.ExpDate.HasValue);
                        ReportForm.gridControl1.DataSource = source.OrderBy(isl => isl.date).GroupBy(isl => new { isl.ItemID,ExpireDate = isl.ExpDate .Value.Date , isl.StoreID })
                        .Select(isl => new {
                            ProductName = db.Prodecuts.Single(x => x.ProdecutID == isl.Key.ItemID).ProdecutName,
                            StoreName = db.Inv_Stores.Single(x => x.ID == isl.Key.StoreID.Value).Name,
                            isl.Key.ExpireDate,
                            Balance = isl.Sum(i => i.ItemQuIN) - isl.Sum(i => i.ItemQuOut),
                        }).ToList();
                    };
                    ReportForm.gridView1.RowCellStyle += (rs, re) => {
                        if (re.RowHandle < 0) return;
                        var view = rs as GridView;
                        if (view == null) return;

                        var ExpireDate = view.GetRowCellValue(re.RowHandle, view.Columns["ExpireDate"]) as DateTime?;
                        if (ExpireDate.HasValue == false) return;

                        var Balance = view.GetRowCellValue(re.RowHandle, view.Columns["Balance"]).ToInt();

                        if(Balance == 0)
                        {
                            re.Appearance.BackColor = Color.LightGray ;

                        }else if (Balance < 0)
                        {
                            re.Appearance.BackColor = DXSkinColors.FillColors.Question ;

                        }
                        else
                        {
                            if (ExpireDate.Value  <= DateTime.Now)
                                re.Appearance.BackColor = DXSkinColors.FillColors.Danger;
                            else if (ExpireDate.Value.AddDays(-15) <= DateTime.Now)
                                re.Appearance.BackColor = System.Drawing.Color.DarkOrange;
                            else if (ExpireDate.Value.AddDays(-30) <= DateTime.Now)
                                re.Appearance.BackColor = System.Drawing.Color.Orange;
                            else if (ExpireDate.Value.AddDays(-45) <= DateTime.Now)
                                re.Appearance.BackColor = System.Drawing.Color.Yellow;
                            else if (ExpireDate.Value.AddDays(-60) <= DateTime.Now)
                                re.Appearance.BackColor = System.Drawing.Color.LightYellow;
                        }
                        
                    };
                    ReportForm.btn_Refresh.PerformClick();
                    ReportForm.Text = ReportForm.gridView1.ViewCaption = "صلاحيات الاصناف بالمخازن";
                    ReportForm.gridView1.Columns["ProductName"].Caption = "اسم الصنف";
                    ReportForm.gridView1.Columns["StoreName"].Caption = "المخزن";
                    ReportForm.gridView1.Columns["Balance"].Caption = "الرصيد ";
                    ReportForm.gridView1.Columns["ExpireDate"].Caption = "الصلاحيه";

                    //GridColumnSummaryItem BalanceSum = new GridColumnSummaryItem();
                    //BalanceSum.SummaryType = SummaryItemType.Average;
                    //BalanceSum.FieldName = "Balance";
                    //BalanceSum.DisplayFormat = "الاجمالي: {0:#.#}";
                    //ReportForm.gridView1.Columns["Balance"].Summary.Clear();
                    //ReportForm.gridView1.Columns["Balance"].Summary.Add(BalanceSum);
                    //TotalPriceSum = new GridColumnSummaryItem();
                    //TotalPriceSum.SummaryType = SummaryItemType.Sum;
                    //TotalPriceSum.FieldName = "TotalPrice";
                    //TotalPriceSum.DisplayFormat = "الاجمالي: {0:#.#}";
                    //ReportForm.gridView1.Columns["TotalPrice"].Summary.Clear();
                    //ReportForm.gridView1.Columns["TotalPrice"].Summary.Add(TotalPriceSum);
                    break;
                #endregion
              
                    #region ProductReachedReorderLevel
                case ReportType.ProductReachedReorderLevel:
                    ReportForm.btn_Refresh.ItemClick += (bs, be) =>
                    {
                        var source = db.Inv_StoreLogs.Select(x => x);
                        if (products.Count > 0)
                            source = source.Where(x => products.Contains(x.ItemID.Value));
                        if (stores.Count > 0)
                            source = source.Where(x => stores.Contains(x.StoreID.Value));
                        if (lyc_EndDate.Visible)
                            source = source.Where(x => x.date.Value.Date <= dt_End.DateTime.Date);
                        if (lyc_StartDate.Visible)
                            source = source.Where(x => x.date.Value.Date >= dt_Start.DateTime.Date);
                        if (lyc_OnDate.Visible)
                            source = source.Where(x => x.date.Value.Date == dt_OnDate.DateTime.Date);
                        ReportForm.gridControl1.DataSource = source.OrderBy(isl => isl.date).GroupBy(isl => new { isl.ItemID, isl.Color, isl.Size, isl.StoreID })
                        .Select(isl => new {
                            ProductID = isl.Key.ItemID,
                            ProductName = db.Prodecuts.Single(x => x.ProdecutID == isl.Key.ItemID).ProdecutName,
                            StoreName = db.Inv_Stores.Single(x => x.ID == isl.Key.StoreID.Value).Name,
                            SellPrice = db.Prodecuts.Single(x => x.ProdecutID == isl.Key.ItemID).FiestUnitPrice1,
                            TotalPrice = (isl.Sum(i => i.ItemQuIN) - isl.Sum(i => i.ItemQuOut)) * db.Prodecuts.Single(x => x.ProdecutID == isl.Key.ItemID).FiestUnitPrice1,
                            Color = db.Inv_Colors.SingleOrDefault(x => x.ID == isl.Key.Color).Name ?? "",
                            Size = db.Inv_Sizes.SingleOrDefault(x => x.ID == isl.Key.Size).Name ?? "",
                            Balance = isl.Sum(i => i.ItemQuIN) - isl.Sum(i => i.ItemQuOut),
                            ReOrderLevel = (db.Prodecuts.Single(p=>p.ProdecutID == isl.Key.ItemID).RequestLimit??0),
                            Date = dt_End.DateTime.Date
                        }).Where(x=> x.Balance <=  x.ReOrderLevel).ToList();
                    };
                    ReportForm.gridView1.RowCellStyle += (rs, re) => {
                        if (re.RowHandle < 0) return;
                        else if (re.Column.FieldName == "Balance")
                        {
                            if (re.CellValue is double d && d < 0)
                            {
                                re.Appearance.BackColor = DXSkinColors.FillColors.Warning;

                            }
                            else if (re.CellValue is double dd && dd > 0)
                            {
                                re.Appearance.BackColor = DXSkinColors.FillColors.Success;

                            }
                        }
                    };
                    ReportForm.btn_Refresh.PerformClick();
                    ReportForm.Text = ReportForm.gridView1.ViewCaption = "اصناف وصلت الي حد الطلب";
                    ReportForm.gridView1.Columns["ProductName"].Caption = "اسم الصنف"; 
                    ReportForm.gridView1.Columns["StoreName"].Caption = "المخزن";
                    ReportForm.gridView1.Columns["Date"].Caption = "التاريخ";
                    ReportForm.gridView1.Columns["Balance"].Caption = "الرصيد ";
                    ReportForm.gridView1.Columns["ReOrderLevel"].Caption = "حد الطلب ";
                    ReportForm.gridView1.Columns["TotalPrice"].Caption = "اجمالي السعر";
                    ReportForm.gridView1.Columns["SellPrice"].Caption = "السعر";
                    ReportForm.gridView1.Columns["Color"].Caption = "اللون";
                    ReportForm.gridView1.Columns["Size"].Caption = "الحجم";

                    //GridColumnSummaryItem BalanceSum = new GridColumnSummaryItem();
                    //BalanceSum.SummaryType = SummaryItemType.Average;
                    //BalanceSum.FieldName = "Balance";
                    //BalanceSum.DisplayFormat = "الاجمالي: {0:#.#}";
                    //ReportForm.gridView1.Columns["Balance"].Summary.Clear();
                    //ReportForm.gridView1.Columns["Balance"].Summary.Add(BalanceSum);
                    //TotalPriceSum = new GridColumnSummaryItem();
                    //TotalPriceSum.SummaryType = SummaryItemType.Sum;
                    //TotalPriceSum.FieldName = "TotalPrice";
                    //TotalPriceSum.DisplayFormat = "الاجمالي: {0:#.#}";
                    //ReportForm.gridView1.Columns["TotalPrice"].Summary.Clear();
                    //ReportForm.gridView1.Columns["TotalPrice"].Summary.Add(TotalPriceSum);
                    break;
                #endregion

            
                    #region ProductCardWithQty
                case ReportType.ProductCardWithQty:
                    var DataSource = db.Inv_StoreLogs.Select(x => x);
                    if (products.Count > 0)
                        DataSource = DataSource.Where(x => products.Contains(x.ItemID.Value));
                    var rowBalanceData =( from stlog in DataSource
                                  group stlog by new { stlog.ItemID, stlog.StoreID } into g
                                         join product in db.Prodecuts on g.Key.ItemID equals product.ProdecutID
                                         join cat in db.Categories on product.CategoryID equals cat.CategoryID 
                                         join store in db.Inv_Stores on g.Key.StoreID equals store.ID
                                         select new
                                  {
                                             ProductID = g.Key.ItemID .Value ,
                                             StoreID = g.Key.StoreID ,
                                             product.ProdecutName,
                                             Category = cat.CategoryName  ,
                                             Company = product.Company ,
                                             store.Name,
                                             Product = product,
                                             Balance = (g.Sum(x => (double?)x.ItemQuIN) ?? 0 - g.Sum(x => (double?)x.ItemQuIN) ?? 0)
                                  }).ToList();

                    var rowData = from data in rowBalanceData
                                  group data by  data.ProductID   into g
                                  select new ProductCardWithQty
                                  {
                                      ID = g.Key , 
                                      Name = g.First().ProdecutName ,
                                      Company = g.First().Company ,
                                      Group = g.First().Category ,
                                      ProductQtyInStores = 
                                      (rowBalanceData.Where(x=>x.ProductID == g.Key )
                                      .Select(x=> new ProductCardWithQty.ProductQtyInStore 
                                      { 
                                          StoreName =x.Name ,
                                          TotalCost  = GetCostOfNextProduct(g.Key , x.StoreID.Value  ) * x.Balance ,
                                          Quantity = x.Balance ,
                                          
                                           TotalSellPrice  = 
                                           ((g.First().Product .FiestUnitFactor == "1") ? g.First().Product.FiestUnitPrice1.Value  * x.Balance
                                           :(g.First().Product.SecoundUnitFactor == "1") ? g.First().Product.SecoundUnitPrice1.Value * x.Balance
                                           :g.First().Product.ThreeUnitPrice1.Value * x.Balance)
                                      }).ToList())
                                  };
                    rpt_ItemInfo.ShowReport(rowData.ToList());
                    return;
                #endregion

            
                    #region TotalSoldItemsToCustomer
                case ReportType.TotalSoldItemsToCustomer :
                    var CustomerInvoicesDataSource = db.Inv_Invoices .Where (x => x.InvoiceType == (byte)MasterClass .InvoiceType.SalesInvoice && x.PartType == (byte)MasterClass.PartTypes .Customer  );
                    if (customers != null && customers .Count() > 0)
                        CustomerInvoicesDataSource = CustomerInvoicesDataSource.Where(s => customers.Contains(s.PartID));
                    if (lyc_EndDate.Visible)
                        CustomerInvoicesDataSource = CustomerInvoicesDataSource.Where(x => x.Date  .Date <= dt_OnDate.DateTime.Date);
                    if (lyc_StartDate.Visible)
                        CustomerInvoicesDataSource = CustomerInvoicesDataSource.Where(x => x.Date .Date >= dt_Start.DateTime.Date);
                    if (lyc_OnDate.Visible)
                        CustomerInvoicesDataSource = CustomerInvoicesDataSource.Where(x => x.Date .Date == dt_OnDate.DateTime.Date);

                    var CustomerInvoicesDetailsDataSource = db.Inv_InvoiceDetails.Where(x => CustomerInvoicesDataSource.Select(s => s.ID).Contains(x.InvoiceID));
                    if (products.Count > 0)
                        CustomerInvoicesDetailsDataSource = CustomerInvoicesDetailsDataSource.Where(x => products.Contains(x.ItemID));

                    var TotalSoldItemsToCustomerDataSource = //from sld in CustomerInvoicesDetailsDataSource
                                                             from sl in CustomerInvoicesDataSource //on sld.InvoiceID equals  sld.InvoiceID 
                                                             group sl by sl.PartID into g
                                                             join cus in db.Customers on g.Key equals cus.CustomerID
                                                             select new ReportModules.ItemsSoldToCustomers 
                                                             {
                                                                 CustomerID = cus.CustomerID ,
                                                                 CustomerName = cus.CustomerName , 
                                                                 Phone = cus.Phone +","+cus.Phone2 ,
                                                                 Products = (
                                                                 from log in db.Inv_StoreLogs.Where (x=>x.Type == (byte)MasterClass.InvoiceType.SalesInvoice
                                                                 &&
                                                                 CustomerInvoicesDetailsDataSource
                                                                 .Where(ss=> g.Select(sl=>sl.ID ).ToList().Contains( ss.InvoiceID ))
                                                                 .Select (ss=>ss.ID ).ToList().Contains(x.TypeID.Value  ))
                                                                // from sld in CustomerInvoicesDetailsDataSource 
                                                                             group log by new { log.ItemID , log.StoreID  } into proG 
                                                                             join pro in db.Prodecuts on proG.Key.ItemID equals pro.ProdecutID 
                                                                             join str in db.Inv_Stores on proG.Key.StoreID equals str.ID 
                                                                             select new TotalSoldProducts { 
                                                                             StoreName =str.Name ,
                                                                             ProductName = pro.ProdecutName ,
                                                                             Quantity =proG.Sum(dd=>(double?)dd.ItemQuOut  )??0,
                                                                                 UnitSellPrice  
                                                                                 = (pro.FiestUnitFactor == "1") ? (pro.FiestUnitPrice1??0)
                                                                                 : (pro.SecoundUnitFactor == "1") ? (pro.SecoundUnitPrice1 ?? 0)
                                                                                 :( pro.ThreeUnitPrice1 ?? 0),
                                                                                 UnitCost = (proG.Average (pp => (pp.ItemQuOut  * pp.BuyPrice) ??0)/ (proG.Sum(dd => (double?)dd.ItemQuOut) ?? 0))
                                                                             } ).ToList()

                                                             };


                   
                    rpt_TotalSoldItemsToCustomer .ShowReport(TotalSoldItemsToCustomerDataSource.ToList());
                    return;
                #endregion
                default:
                    break;
            }
            ReportForm.FilterString = filterString;
            ReportForm.Show();
        }

        public void CustomerInvoiceDetails(frm_Report ReportForm)
        {  
            ReportForm.btn_Refresh.ItemClick += (bs, be) =>
            {
                var customers = GetSelectedCustomer();

                var source = db.Inv_Invoices.Where(x => x.InvoiceType == (byte)InvoiceType.SalesInvoice && customers.Contains(x.PartID) && x.PartType == (byte)PartTypes.Customer);

                if (lyc_EndDate.Visible)
                    source = source.Where(x => x.Date.Date <= dt_End.DateTime.Date);
                if (lyc_StartDate.Visible)
                    source = source.Where(x => x.Date.Date >= dt_Start.DateTime.Date);
                if (lyc_OnDate.Visible)
                    source = source.Where(x => x.Date.Date == dt_OnDate.DateTime.Date);


                var rowdata =
                           db.Inv_InvoiceDetails.Where(x => source.Select(s => s.ID).ToList().Contains(x.InvoiceID))
                           .Select(x => new
                           {
                               db.Inv_Invoices.SingleOrDefault(s => s.ID == x.InvoiceID).Date,
                               ProdecutName = db.Prodecuts.SingleOrDefault(p => p.ProdecutID == x.ItemID).ProdecutName,
                               Unit = x.ItemUnitID == 1 ? db.Prodecuts.SingleOrDefault(p => p.ProdecutID == x.ItemID).FiestUnit : x.ItemUnitID == 2 ? db.Prodecuts.SingleOrDefault(p => p.ProdecutID == x.ItemID).SecoundUnit : db.Prodecuts.SingleOrDefault(p => p.ProdecutID == x.ItemID).ThreeUnit,
                               x.ItemQty,
                               x.Price,
                               x.CostValue,
                               x.TotalPrice,
                               x.TotalCostValue
                           }).ToList();

                ReportForm.gridControl1.DataSource = rowdata;//.Select(r => new{}).ToList();
            };

            ReportForm.btn_Refresh.PerformClick();
            ReportForm.gridView1.PopulateColumns(ReportForm.gridControl1.DataSource);
            ReportForm.Text = ReportForm.gridView1.ViewCaption = "";
            ReportForm.gridView1.Columns["Date"].Caption = "التاريخ";
            ReportForm.gridView1.Columns["ProdecutName"].Caption = "اسم الصنف";
            ReportForm.gridView1.Columns["Unit"].Caption = "الوحدة";
            ReportForm.gridView1.Columns["ItemQty"].Caption = "الكمية";
            ReportForm.gridView1.Columns["Price"].Caption = "السعر";
            ReportForm.gridView1.Columns["CostValue"].Caption = "التكلفه";
            ReportForm.gridView1.Columns["TotalPrice"].Caption = "اجمالي السعر";
            ReportForm.gridView1.Columns["TotalCostValue"].Caption = "اجمالي التكلفه";

            GridColumnSummaryItem ItemQty = new GridColumnSummaryItem();
            ItemQty.SummaryType = SummaryItemType.Sum;
            ItemQty.FieldName = "ItemQty";
            ItemQty.DisplayFormat = "{0#.#}";
            ReportForm.gridView1.Columns["ItemQty"].Summary.Clear();
            ReportForm.gridView1.Columns["ItemQty"].Summary.Add(ItemQty);

            GridColumnSummaryItem TotalPrice = new GridColumnSummaryItem();
            TotalPrice.SummaryType = SummaryItemType.Sum;
            TotalPrice.FieldName = "TotalPrice";
            TotalPrice.DisplayFormat = "{0#.#}";
            ReportForm.gridView1.Columns["TotalPrice"].Summary.Clear();
            ReportForm.gridView1.Columns["TotalPrice"].Summary.Add(TotalPrice);

            GridColumnSummaryItem TotalCostValue = new GridColumnSummaryItem();
            TotalCostValue.SummaryType = SummaryItemType.Sum;
            TotalCostValue.FieldName = "TotalCostValue";
            TotalCostValue.DisplayFormat = "{0#.#}";
            ReportForm.gridView1.Columns["TotalCostValue"].Summary.Clear();
            ReportForm.gridView1.Columns["TotalCostValue"].Summary.Add(TotalCostValue);
        }
        public void FullInvoiceBy(frm_Report ReportForm)
        {//
            ReportForm.btn_Refresh.ItemClick += (bs, be) =>
            {
                var customers = GetSelectedCustomer();
                var source = db.Inv_Invoices.Where(x => customers.Contains(x.PartID) && x.PartType == (byte)partTypes);
                if (lyc_EndDate.Visible)
                    source = source.Where(x => x.Date.Date <= dt_End.DateTime.Date);
                if (lyc_StartDate.Visible)
                    source = source.Where(x => x.Date.Date >= dt_Start.DateTime.Date);
                if (lyc_OnDate.Visible)
                    source = source.Where(x => x.Date.Date == dt_OnDate.DateTime.Date);
                var rowdata =
                source.Select(inv => new
                                {
                    inv.Code,
                    inv.Date,
                    Type = inv.InvoiceType == (byte)InvoiceType.SalesInvoice ? "شراء" : inv.InvoiceType == (byte)InvoiceType.SalesReturn ? "مرتحع شراء" : inv.InvoiceType == (byte)InvoiceType.PurchaseInvoice ? "بيع" : inv.InvoiceType == (byte)InvoiceType.PurchaseReturn ? "مرتجع بيع" : "اخري",
                    inv.Total,
                    inv.DiscountValue,
                    inv.AddTaxValue,
                    inv.Net,
                    inv.Paid,
                    inv.TotalRevenue,
                    InvoiceDetails = db.Inv_InvoiceDetails.Where(x => x.InvoiceID== inv.ID)
                           .Select(x => new
                           {
                               ProdecutName = db.Prodecuts.SingleOrDefault(p => p.ProdecutID == x.ItemID).ProdecutName,
                               Unit = x.ItemUnitID == 1 ? db.Prodecuts.SingleOrDefault(p => p.ProdecutID == x.ItemID).FiestUnit : x.ItemUnitID == 2 ? db.Prodecuts.SingleOrDefault(p => p.ProdecutID == x.ItemID).SecoundUnit : db.Prodecuts.SingleOrDefault(p => p.ProdecutID == x.ItemID).ThreeUnit,
                               x.ItemQty,
                               x.Price,
                               x.CostValue,
                               x.TotalPrice,
                               x.TotalCostValue
                           }).ToList()
                }).ToList();
                ReportForm.gridControl1.DataSource = rowdata;//.Select(r => new{}).ToList();
            };
            ReportForm.gridControl1.ViewRegistered += (ss, ee) => {
                var view = ee.View as GridView;
                if (view != null && view.LevelName == "InvoiceDetails")
                {
                    view.ViewCaption = "تفاصيل الفاتورة";
                    view.OptionsView.ShowViewCaption = true;
                    view.Columns["ProdecutName"].Caption = "اسم الصنف";
                    view.Columns["Unit"].Caption = "الوحدة";
                    view.Columns["ItemQty"].Caption = "الكمية";
                    view.Columns["Price"].Caption = "السعر";
                    view.Columns["CostValue"].Caption = "التكلفه";
                    view.Columns["TotalPrice"].Caption = "اجمالي السعر";
                    view.Columns["TotalCostValue"].Caption = "اجمالي التكلفه";
                }
            };
            ReportForm.btn_Refresh.PerformClick();
            ReportForm.gridView1.PopulateColumns(ReportForm.gridControl1.DataSource);
            ReportForm.Text = ReportForm.gridView1.ViewCaption = "";
            ReportForm.gridView1.Columns["Code"].Caption = "الكود";
            ReportForm.gridView1.Columns["Date"].Caption = "التاريخ";
            ReportForm.gridView1.Columns["Type"].Caption = "النوع";
            ReportForm.gridView1.Columns["Total"].Caption = "الاجمالي";
            ReportForm.gridView1.Columns["DiscountValue"].Caption = "ق الخصم";
            ReportForm.gridView1.Columns["AddTaxValue"].Caption = "ق الضريبة";
            ReportForm.gridView1.Columns["Net"].Caption = "الصافي";
            ReportForm.gridView1.Columns["Paid"].Caption = "المدفوع";
            ReportForm.gridView1.Columns["TotalRevenue"].Caption = "اجمالي";

 
            GridColumnSummaryItem SumTotal = new GridColumnSummaryItem();
            SumTotal.SummaryType = SummaryItemType.Sum;
            SumTotal.FieldName = "Total";
            SumTotal.DisplayFormat = "{0#.#}";
            ReportForm.gridView1.Columns["Total"].Summary.Clear();
            ReportForm.gridView1.Columns["Total"].Summary.Add(SumTotal);
          
            GridColumnSummaryItem SumNet = new GridColumnSummaryItem();
            SumNet.SummaryType = SummaryItemType.Sum;
            SumNet.FieldName = "Net";
            SumNet.DisplayFormat = "{0#.#}";
            ReportForm.gridView1.Columns["Net"].Summary.Clear();
            ReportForm.gridView1.Columns["Net"].Summary.Add(SumNet);
           
            GridColumnSummaryItem SumPaid = new GridColumnSummaryItem();
            SumPaid.SummaryType = SummaryItemType.Sum;
            SumPaid.FieldName = "Paid";
            SumTotal.DisplayFormat = "{0#.#}";
            ReportForm.gridView1.Columns["Paid"].Summary.Clear();
            ReportForm.gridView1.Columns["Paid"].Summary.Add(SumPaid);

        }
        class MyClass
        {
            public double Total { get; set; }
        }
  
        private void cb_DateFilterType_SelectedIndexChanged(object sender, EventArgs e)
        {
            lyc_OnDate.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            lyc_EndDate.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            lyc_StartDate.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;

            if (cb_DateFilterType.SelectedIndex  == 0)
            {
                lyc_EndDate.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                lyc_StartDate.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always; 
            }
            else if (cb_DateFilterType.SelectedIndex == 1)
                lyc_StartDate.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always; 
            else if (cb_DateFilterType.SelectedIndex == 2)
                lyc_EndDate.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always ; 
            else if (cb_DateFilterType.SelectedIndex == 3)
                lyc_OnDate .Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;

            
        }


        public static double GetCostOfNextProduct(int productID, int storeID)
        {
            using (DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String))
            {

                var query = db.Inv_StoreLogs.Where(sl => sl.ItemID  == productID && sl.StoreID == storeID)
                 .OrderBy(sl => sl.date);

                if (query.Count() == 0)
                {
                    return 0;
                }

                double TotalQtyOut = query.Where(q => q.ItemQuIN == 0).Sum(q => (double?)q.ItemQuOut ) ?? 0;
                double Balance = GetProductBalanceInStore(productID, storeID);
                if (Balance <= 0)
                {
                    return 0;
                }
                var subQurey = query
                .Where(q => query.Where(q1 => q.ItemQuOut == 0 && q1.date  <= q.date )
                .Sum(q1 => q1.ItemQuIN ) > TotalQtyOut && q.ItemQuOut == 0).ToList();

                var subQureyBalance = subQurey.Where(q => q.ItemQuOut == 0).Sum(q => q.ItemQuIN) - subQurey.Where(q => q.ItemQuIN == 0).Sum(q => q.ItemQuOut);
                if (subQureyBalance > Balance)
                {
                    var diff = subQureyBalance - Balance;
                    subQurey[0].ItemQuIN  -= diff;
                }

                double WAC;
                WAC = subQurey.Select(q => q.ItemQuIN.Value  * q.BuyPrice.Value  ).Sum(q => q) / Balance;
                return WAC;
              


            }
        }

        public static double GetProductBalanceInStore(int productID, int storeID)
        {
            using (DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String))
            {
                var query = db.Inv_StoreLogs .Where(sl => sl.ItemID  == productID && sl.StoreID == storeID);
                double TotalQtyOut = query.Where(q => q.ItemQuIN  == 0).Sum(q => (double?)q.ItemQuOut) ?? 0;
                double TotalQtyIn = query.Where(q => q.ItemQuOut  == 0).Sum(q => (double?)q.ItemQuIN) ?? 0;
                double Balance = TotalQtyIn - TotalQtyOut;
                return Balance;
            }

        }
    }
      
}
