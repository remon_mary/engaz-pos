﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Drawing.Printing;
using DevExpress.DataProcessing;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.Utils.Serializing;

namespace ByStro.Forms
{
    public partial class frm_SelectDefaultPrinter : DevExpress.XtraEditors.XtraForm
    {
        public frm_SelectDefaultPrinter()
        {
            InitializeComponent();
            
            gridView1.CellValueChanged += GridView1_CellValueChanged;

        }

        private void GridView1_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == "IsDefault")
            {
                var row = gridView1.GetRow(e.RowHandle) as Printer;
                if (row == null) return;
                row.IsDefault = true;

                for (int i = 0; i < printers.Count; i++)
                    if (i != e.RowHandle)
                        printers[i].IsDefault = false;
                gridView1.RefreshData();
            }
        }

       


        class Printer
        {
            public string Name { get; set; }
            public bool IsDefault { get; set; }
        }
        BindingList<Printer> printers;
        private void frm_SelectDefaultPrinter_Load(object sender, EventArgs e)
        {
            printers = new BindingList<Printer>();
            foreach (string item in PrinterSettings.InstalledPrinters)
                printers.Add(new Printer { Name = item });
            var printer = printers.SingleOrDefault(x => x.Name == Properties.Settings.Default.DefaultPrinter);
            if(printer !=null ) 
                printer .IsDefault = true;
            gridControl1.DataSource = printers;
            gridView1.PopulateColumns();
            gridView1.Columns["Name"].OptionsColumn.AllowEdit = false;
            gridView1.Columns["Name"].Caption = "الاسم";
            gridView1.Columns["IsDefault"].Caption = "الافتراضية";
        }
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.DefaultPrinter = printers.FirstOrDefault(x => x.IsDefault).Name;
            Properties.Settings.Default.Save();
            MessageBox.Show("تم الحفظ");
        }
        private void simpleButton2_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}