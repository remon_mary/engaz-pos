﻿
using ByStro.Clases;
using ByStro.DAL;
using DevExpress.XtraCharts;
using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static ByStro.Clases.MasterClass;

namespace ByStro.Forms
{
    public partial class frm_SalesInvoiceTraking : XtraForm
    {
        DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
        public frm_SalesInvoiceTraking()
        {
            InitializeComponent();
            InvoiceTraking();
        }

        private void btn_Refresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            InvoiceTraking();
        }
        public void InvoiceTraking()
        {
            var source = db.Inv_Invoices.Where(x => x.Date.Date == DateTime.Now.Date && x.InvoiceType == (byte)InvoiceType.SalesInvoice);
            var rowdata = source.GroupBy(h => h.Date.Hour).Select(x => new
            {
                Hour = x.Key,
                TotalNet = x.Sum(t => t.Net)
            }).ToList();
            gridControl1.DataSource = rowdata;
            gridView1.PopulateColumns(gridControl1.DataSource);
            gridView1.Columns["Hour"].Caption = "الساعة";
            gridView1.Columns["TotalNet"].Caption = "اجمالي المبيعات";
            Series series = new Series(" ", ViewType.Bar);
            series.LabelsVisibility = DevExpress.Utils.DefaultBoolean.True;
            for (int i = 1; i < 25; i++)
            {
                SeriesPoint seriesPoint = new SeriesPoint(i.ToString(),0);
                if(rowdata.SingleOrDefault(x => x.Hour == i) != null)
                    seriesPoint = new SeriesPoint(i.ToString(), rowdata.SingleOrDefault(x => x.Hour == i).TotalNet); 
                series.Points.Add(seriesPoint);
            }
            //foreach (var item in rowdata)
            //    series.Points.Add(new SeriesPoint(item.Hour, item.TotalNet));             
            this.chartControl1.Series.Clear();
            this.chartControl1.Series.Add(series);
        }

    }


}
