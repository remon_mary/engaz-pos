﻿using ByStro.Clases;
using ByStro.PL;
using DevExpress.DataProcessing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Windows.Forms;

namespace ByStro.Forms
{
    public partial class frm_AddProductsRange : XtraForm
    {
        BindingList<Product> ProductsList;
        BindingList<string > UOMs;
        BindingList<NameAndID> Categories;

        public frm_AddProductsRange()
        {
            InitializeComponent();
            gridView1.InitNewRow += GridView1_InitNewRow;
            gridView1.ValidateRow += GridView1_ValidateRow;
            gridView1.InvalidRowException += GridView1_InvalidRowException;
            this.FormClosing += Frm_AddProductsRange_FormClosing;
        }

        private void Frm_AddProductsRange_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(worker != null && worker.IsBusy)
            {
                e.Cancel = true;
            }
        }

        private void GridView1_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
        {
            e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
        }

        private void GridView1_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            var row = gridView1.GetRow(e.RowHandle) as Product;
            if (row == null) return;
            if (string.IsNullOrEmpty(row.Name))
            {
                gridView1.SetColumnError(gridView1.Columns[nameof(row.Name)],"يجب ادخال اسم الصنف");
                e.Valid = false;
            }
            if (string.IsNullOrEmpty(row.Unit ))
            {
                gridView1.SetColumnError(gridView1.Columns[nameof(row.Unit)], "يجب اختيار الوحده");
                e.Valid = false;
            }
            if (row.Category == 0 )
            {
                gridView1.SetColumnError(gridView1.Columns[nameof(row.Category)], "يجب اختيار الفئه");
                e.Valid = false;
            }



        }

        private void GridView1_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            var row = gridView1.GetRow(e.RowHandle) as Product ; 
            if (row == null) return;

            row.HasColor = chk_Color.Checked;
            row.HasSize = chk_Size.Checked;
            row.HasExpire = chk_Expire.Checked;
            row.HasSerial = chk_Serial.Checked;
            row.Category = Convert.ToInt32(Treelkp_Group.EditValue);
            row.Unit  =lkp_UOM  .Text ;
            row.Company = lkp_Company.Text;
        }

        public class Product
        {
            public string Name { get; set; }
            public int Category { get; set; }
            public string Company { get; set; }
            public string Unit { get; set; }
            public double Price { get; set; }
            public double BuyPrice { get; set; }
            public double OpenBalance { get; set; }
            public double  ReorderLevel { get; set; }
            public string Barcode { get; set; }
            public bool HasColor { get; set; }
            public bool HasSize { get; set; }
            public bool HasExpire { get; set; }
            public bool HasSerial { get; set; }
        }
        public class NameAndID
        {
            public int ID { get; set; }
            public string  Name { get; set; }
        }
        private void btn_New_Click(object sender, EventArgs e)
        {
            ProductsList = new BindingList<Product>();
            btn_Save.Enabled =   gridControl1.Enabled = true;
            memoEdit1.Text = "";
            progressBarControl1.Position = 0;

        }
        void RefreshData()
        {
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
            UOMs = new BindingList<string >(db.Units.Select(x=>    x.UnitName ) .ToList());
            if (Categories == null)
                Categories = new BindingList<NameAndID>();
            Categories.Clear();
            Categories.AddRange(db.Categories .Select(x => new NameAndID { ID = x.CategoryID , Name = x.CategoryName  }).ToList());
            
            
            
            lkp_UOM.Properties.Items.Clear();
            lkp_UOM .Properties.Items .AddRange( UOMs);

            repoUOM.Items.Clear();
            repoUOM.Items.AddRange(UOMs);

        }
        RepositoryItemComboBox repoUOM= new RepositoryItemComboBox();
        void BindDataToControls()
        {
            gridControl1.DataSource = ProductsList;

            var ins = new Product();
            gridView1.Columns[nameof(ins.Name)].Caption = "الاسم";
            gridView1.Columns[nameof(ins.Barcode)].Caption = "الباركود";
            gridView1.Columns[nameof(ins.BuyPrice)].Caption = "سعر الشراء";
            gridView1.Columns[nameof(ins.HasSerial)].Caption = "سيريال";
            gridView1.Columns[nameof(ins.Price)].Caption = "السعر";
            gridView1.Columns[nameof(ins.Company)].Caption = "الشركه";
            gridView1.Columns[nameof(ins.HasColor)].Caption = "لون";
            gridView1.Columns[nameof(ins.Category)].Caption = "فئه";
            gridView1.Columns[nameof(ins.HasExpire)].Caption = "صلاحيه";
            gridView1.Columns[nameof(ins.HasSize)].Caption = "حجم";
            gridView1.Columns[nameof(ins.OpenBalance)].Caption = "رصيد افتتاحي"; 
            gridView1.Columns[nameof(ins.ReorderLevel)].Caption = "حد الطلب";
            gridView1.Columns[nameof(ins.Unit)].Caption = "الوحده"; 

            Treelkp_Group.Properties.DataSource = Categories;
            Treelkp_Group.Properties.ValueMember = "ID";
            Treelkp_Group.Properties.DisplayMember  = "Name";

          
            RepositoryItemLookUpEdit repoCategory = new RepositoryItemLookUpEdit() { DataSource = Categories, DisplayMember = "Name", ValueMember = "ID" }; ;

            gridControl1.RepositoryItems.AddRange(new RepositoryItem[] { repoUOM, repoCategory });
             
            gridView1.Columns[nameof(ins.Category)].ColumnEdit = repoCategory;
            gridView1.Columns[nameof(ins.Unit)].ColumnEdit = repoUOM;


            if (UOMs.Count > 0)
                lkp_UOM.EditValue = UOMs[0];
            if (Categories.Count > 0)
                Treelkp_Group.EditValue = Categories[0].ID;



        }
        private void frm_AddProductsRange_Load(object sender, EventArgs e)
        {
            btn_New.PerformClick();
            RefreshData();
            BindDataToControls();
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            Category_frm frm = new Category_frm();
            frm.ShowDialog();
            RefreshData();
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            Unit_frm frm = new Unit_frm();
            frm.ShowDialog();
            RefreshData();

        }
        BackgroundWorker worker = new BackgroundWorker();

        private void btn_Save_Click(object sender, EventArgs e)
        {
            if (XtraMessageBox.Show(text: "سيتم اضافه البيانات الي قاعده البيانات وانشاء رصيد افتتاحي \n  هل تريد المتابعه ؟",
               caption: "تاكيد عمليه التسجيل", icon: MessageBoxIcon.Question, buttons: MessageBoxButtons.YesNo) != DialogResult.Yes)
                return;

            var data = gridView1.DataSource as BindingList<Product>;
            if (data is null || data.Count() == 0)
            {
                XtraMessageBox.Show("يرجي مراجعه الاصناف بالجدول والتاكد من تسجيل صنف واحد علي الاقل");

            }
            btn_Save.Enabled = btn_New.Enabled =gridControl1.Enabled = false;

            worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.DoWork += Worker_DoWork;
            worker.ProgressChanged += Worker_ProgressChanged;
            worker.RunWorkerCompleted += Worker_RunWorkerCompleted;
            worker.WorkerSupportsCancellation = false;
            worker.RunWorkerAsync();


          
           
        }

        private void Worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            btn_New.Enabled = true;


            if (OpenBalancelist.Count() > 0)
            {
                var frm = new frm_Inv_Invoice(MasterClass.InvoiceType.ItemOpenBalance, OpenBalancelist);
                frm.Show();
                frm.btn_Save.PerformClick();
                frm.Close();
            }
        }

        private void Worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBarControl1.Position = e.ProgressPercentage;
            if(e.UserState is string && (string .IsNullOrEmpty( e.UserState.ToString() ) == false))
            {
                memoEdit1.Text += Environment.NewLine +e.UserState.ToString();
            }
        }

        List<DAL.Inv_InvoiceDetail> OpenBalancelist = new List<DAL.Inv_InvoiceDetail>();
        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            int precint = 0;
            var data = gridView1.DataSource as BindingList<Product>; 
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String); 
            OpenBalancelist = new List<DAL.Inv_InvoiceDetail>();

            int index = 1;
            foreach (var item in data)
            {

                precint =Convert.ToInt32( (((double)index / (double)data.Count()) * 100));
                item.Barcode =  (string.IsNullOrEmpty(item.Barcode.Trim())) ? GetNextBarcode() : item.Barcode.Trim();
                var dbProduct = new DAL.Prodecut()
                {
                    CategoryID = item.Category,
                    Company = item.Company,
                    DiscoundBay = 0,
                    ProdecutAvg = null,
                    DiscoundSale = 0,
                    FiestUnit = item.Unit,
                    FiestUnitBarcode = item.Barcode,
                    FiestUnitFactor = "1",
                    FiestUnitOperating = "1",
                    FiestUnitPrice1 = item.Price,
                    FiestUnitPrice2 = item.Price,
                    FiestUnitPrice3 = item.Price,
                    HasColor = item.HasColor,
                    HasExpire = item.HasExpire,
                    HasSerial = item.HasSerial,
                    HasSize = item.HasSize,
                    ProdecutBayPrice = item.BuyPrice,
                    LastUpdate = DateTime.Now,
                    ProdecutID = GetNextProductID(),
                    ProdecutLocation = "",
                    ProdecutName = item.Name,
                    ProductService = false,
                    RequestLimit = item.ReorderLevel,
                    SecoundUnit = "",
                    SecoundUnitBarcode = "",
                    SecoundUnitFactor = "",
                    SecoundUnitOperating = "",
                    SecoundUnitPrice1 = 0,
                    SecoundUnitPrice2 = 0,
                    SecoundUnitPrice3 = 0,
                    Status = true,
                    ThreeUnit = "",
                    ThreeUnitBarcode = "",
                    ThreeUnitFactor = "",
                    ThreeUnitOperating = "",
                    ThreeUnitPrice1 = 0,
                    ThreeUnitPrice2 = 0,
                    ThreeUnitPrice3 = 0,
                    UnitDefoult = 1


                };
              //  if (db.Prodecuts.Where(x => x.FiestUnitBarcode == item.Barcode ||
              //x.SecoundUnitBarcode == item.Barcode || x.SecoundUnitBarcode == item.Barcode).Count() > 0)
              //  {
              //      dbProduct = db.Prodecuts.Where(x => x.FiestUnitBarcode == item.Barcode ||
              //  x.SecoundUnitBarcode == item.Barcode || x.SecoundUnitBarcode == item.Barcode).FirstOrDefault();
              //      worker.ReportProgress(precint, "البراكود مسجل بالفعل" + ":" + dbProduct.ProdecutName);
              //  }

                if (db.Prodecuts.Where(x => x.ProdecutName == item.Name).Count() > 0)
                { 
                    worker.ReportProgress(precint, "اسم الصنف موجود بالفعل" + item.Name);
                    dbProduct = db.Prodecuts.Where(x => x.ProdecutName == item.Name).FirstOrDefault();
                } 
                else
                { 
                     db.Prodecuts.InsertOnSubmit(dbProduct);
                } 

                if (item.OpenBalance > 0)
                {
                    OpenBalancelist.Add(new DAL.Inv_InvoiceDetail()
                    {
                        ItemID = Convert.ToInt32(dbProduct.ProdecutID),
                        ItemQty = item.OpenBalance,
                    });
                }
                worker.ReportProgress(precint);
            }
            worker.ReportProgress(100, "جاري رفع الاصناف الي قاعده البيانات");

            db.SubmitChanges();
            worker.ReportProgress(100 , "تم حفظ اضافه الاصناف بنجاح");
        }

        int CurrentID = 0;
        int GetNextProductID()
        {
            if(CurrentID == 0)
            using (DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String))
                    CurrentID = db.Prodecuts.Max(x => (int?)x.ProdecutID) ?? 0;

            CurrentID++;
            return  CurrentID;


        }
        string CurrentBarcode = string .Empty ;
        string GetNextBarcode()
        {
           
            if (CurrentBarcode == string .Empty)
            {
                var list = new List<string>();

                DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
                db.Prodecuts.Select(x => x.FiestUnitBarcode);
                list.AddRange(db.Prodecuts.Select(x => x.FiestUnitBarcode).ToList());
                list.AddRange(db.Prodecuts.Select(x => x.SecoundUnitBarcode).ToList());
                list.AddRange(db.Prodecuts.Select(x => x.ThreeUnitBarcode).ToList());

                var data = gridView1.DataSource as BindingList<Product>;
                if (data is null == false && data.Count() > 0)
                {
                    list.AddRange(data.Select(x => x.Barcode).Distinct().ToList());
                }
                 list = list.Distinct().ToList();
                CurrentBarcode = list.OrderByDescending(x=>x.Length).FirstOrDefault(); 
            }
            CurrentBarcode =  MasterClass.GetNextNumberInString(CurrentBarcode);
            return  CurrentBarcode;


        }
        private void simpleButton1_Click(object sender, EventArgs e)
        {


            if ((Treelkp_Group.EditValue is int id && id > 0) != true)
            {
                Treelkp_Group.ErrorText = "يجب اختيار فئه افتراضيه";
                return;
            }
            if (string.IsNullOrEmpty(lkp_UOM.Text))
            {
                lkp_UOM.ErrorText = "يجب اختيار وحده افتراضيه";
                return;
            } 



            new frm_ImportFromExcel(ref ProductsList, lkp_UOM.Text, lkp_Company.Text,Convert.ToInt32(Treelkp_Group.EditValue)).ShowDialog();
                RefreshData();
          
           
        }
    }
}
