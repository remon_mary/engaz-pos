﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace ByStro.Forms
{
    public partial class frm_LocalSettings : DevExpress.XtraEditors.XtraForm
    {
        public frm_LocalSettings()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Currancy1 = textEdit1.Text;
            Properties.Settings.Default.Currancy2 = textEdit2.Text;
            Properties.Settings.Default.Currancy3 = textEdit3.Text;
            Properties.Settings.Default.SubCurrancy1 = textEdit4.Text;
            Properties.Settings.Default.SubCurrancy2 = textEdit5.Text;
            Properties.Settings.Default.SubCurrancy3 = textEdit6.Text;
            Properties.Settings.Default.CurrancyChar = textEdit7.Text;
            Properties.Settings.Default.ShowPrintPreview = checkEdit1.Checked;
           Properties.Settings.Default.ShowSellPriceLowerThanCostWarning  =  chk_SellPriceLowerThanCostWarnning.Checked  ;

            Properties.Settings.Default.Save();
        }

        private void frm_LocalSettings_Load(object sender, EventArgs e)
        {
          textEdit1.Text=  Properties.Settings.Default.Currancy1    ; 
          textEdit2.Text=  Properties.Settings.Default.Currancy2    ; 
          textEdit3.Text=  Properties.Settings.Default.Currancy3    ; 
          textEdit4.Text=  Properties.Settings.Default.SubCurrancy1 ; 
          textEdit5.Text=  Properties.Settings.Default.SubCurrancy2 ; 
          textEdit6.Text =  Properties.Settings.Default.SubCurrancy3 ; 
          textEdit7.Text=  Properties.Settings.Default.CurrancyChar;
            checkEdit1.Checked = Properties.Settings.Default.ShowPrintPreview;
            chk_SellPriceLowerThanCostWarnning.Checked = Properties.Settings.Default.ShowSellPriceLowerThanCostWarning;
        }
    }
}