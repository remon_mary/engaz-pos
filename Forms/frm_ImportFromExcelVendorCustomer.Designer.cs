﻿namespace ByStro.Forms
{
    partial class frm_ImportFromExcelVendorCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.cb_Tax = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cb_Group = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cb_City = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cb_Address = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cb_Phone = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cb_Name = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cb_Mobile = new DevExpress.XtraEditors.ComboBoxEdit();
            this.spn_Start = new DevExpress.XtraEditors.SpinEdit();
            this.cb_Sheets = new DevExpress.XtraEditors.ComboBoxEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.textEdit1 = new DevExpress.XtraEditors.ButtonEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.cb_Email = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cb_Credit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cb_MaxCredit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cb_Debit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciTax = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Tax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Group.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_City.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Address.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Phone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Mobile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_Start.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Sheets.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Email.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Credit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_MaxCredit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Debit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.cb_Tax);
            this.layoutControl1.Controls.Add(this.cb_Group);
            this.layoutControl1.Controls.Add(this.cb_City);
            this.layoutControl1.Controls.Add(this.cb_Address);
            this.layoutControl1.Controls.Add(this.cb_Phone);
            this.layoutControl1.Controls.Add(this.cb_Name);
            this.layoutControl1.Controls.Add(this.cb_Mobile);
            this.layoutControl1.Controls.Add(this.spn_Start);
            this.layoutControl1.Controls.Add(this.cb_Sheets);
            this.layoutControl1.Controls.Add(this.gridControl1);
            this.layoutControl1.Controls.Add(this.textEdit1);
            this.layoutControl1.Controls.Add(this.simpleButton1);
            this.layoutControl1.Controls.Add(this.cb_Email);
            this.layoutControl1.Controls.Add(this.cb_Credit);
            this.layoutControl1.Controls.Add(this.cb_MaxCredit);
            this.layoutControl1.Controls.Add(this.cb_Debit);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsView.RightToLeftMirroringApplied = true;
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(760, 467);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // cb_Tax
            // 
            this.cb_Tax.Location = new System.Drawing.Point(507, 162);
            this.cb_Tax.Name = "cb_Tax";
            this.cb_Tax.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cb_Tax.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.cb_Tax.Size = new System.Drawing.Size(161, 20);
            this.cb_Tax.StyleController = this.layoutControl1;
            this.cb_Tax.TabIndex = 7;
            // 
            // cb_Group
            // 
            this.cb_Group.Location = new System.Drawing.Point(507, 186);
            this.cb_Group.Name = "cb_Group";
            this.cb_Group.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cb_Group.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.cb_Group.Size = new System.Drawing.Size(161, 20);
            this.cb_Group.StyleController = this.layoutControl1;
            this.cb_Group.TabIndex = 9;
            // 
            // cb_City
            // 
            this.cb_City.Location = new System.Drawing.Point(519, 243);
            this.cb_City.Name = "cb_City";
            this.cb_City.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cb_City.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.cb_City.Size = new System.Drawing.Size(137, 20);
            this.cb_City.StyleController = this.layoutControl1;
            this.cb_City.TabIndex = 10;
            // 
            // cb_Address
            // 
            this.cb_Address.Location = new System.Drawing.Point(519, 267);
            this.cb_Address.Name = "cb_Address";
            this.cb_Address.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cb_Address.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.cb_Address.Size = new System.Drawing.Size(137, 20);
            this.cb_Address.StyleController = this.layoutControl1;
            this.cb_Address.TabIndex = 11;
            // 
            // cb_Phone
            // 
            this.cb_Phone.Location = new System.Drawing.Point(519, 291);
            this.cb_Phone.Name = "cb_Phone";
            this.cb_Phone.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cb_Phone.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.cb_Phone.Size = new System.Drawing.Size(137, 20);
            this.cb_Phone.StyleController = this.layoutControl1;
            this.cb_Phone.TabIndex = 12;
            // 
            // cb_Name
            // 
            this.cb_Name.Location = new System.Drawing.Point(507, 138);
            this.cb_Name.Name = "cb_Name";
            this.cb_Name.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cb_Name.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.cb_Name.Size = new System.Drawing.Size(161, 20);
            this.cb_Name.StyleController = this.layoutControl1;
            this.cb_Name.TabIndex = 13;
            // 
            // cb_Mobile
            // 
            this.cb_Mobile.Location = new System.Drawing.Point(519, 315);
            this.cb_Mobile.Name = "cb_Mobile";
            this.cb_Mobile.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cb_Mobile.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.cb_Mobile.Size = new System.Drawing.Size(137, 20);
            this.cb_Mobile.StyleController = this.layoutControl1;
            this.cb_Mobile.TabIndex = 14;
            // 
            // spn_Start
            // 
            this.spn_Start.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spn_Start.Location = new System.Drawing.Point(558, 492);
            this.spn_Start.Name = "spn_Start";
            this.spn_Start.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spn_Start.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.spn_Start.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.spn_Start.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.spn_Start.Size = new System.Drawing.Size(110, 20);
            this.spn_Start.StyleController = this.layoutControl1;
            this.spn_Start.TabIndex = 15;
            // 
            // cb_Sheets
            // 
            this.cb_Sheets.Location = new System.Drawing.Point(507, 69);
            this.cb_Sheets.Name = "cb_Sheets";
            this.cb_Sheets.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cb_Sheets.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.cb_Sheets.Size = new System.Drawing.Size(161, 20);
            this.cb_Sheets.StyleController = this.layoutControl1;
            this.cb_Sheets.TabIndex = 6;
            this.cb_Sheets.SelectedIndexChanged += new System.EventHandler(this.cb_Sheets_SelectedIndexChanged);
            // 
            // gridControl1
            // 
            this.gridControl1.Location = new System.Drawing.Point(41, 45);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(438, 479);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            this.gridControl1.DataSourceChanged += new System.EventHandler(this.gridControl1_DataSourceChanged);
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(507, 45);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.textEdit1.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.textEdit1.Size = new System.Drawing.Size(161, 20);
            this.textEdit1.StyleController = this.layoutControl1;
            this.textEdit1.TabIndex = 5;
            this.textEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.textEdit1_ButtonClick);
            this.textEdit1.EditValueChanged += new System.EventHandler(this.textEdit1_EditValueChanged);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(507, 492);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(47, 22);
            this.simpleButton1.StyleController = this.layoutControl1;
            this.simpleButton1.TabIndex = 16;
            this.simpleButton1.Text = "متابعه";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // cb_Email
            // 
            this.cb_Email.Location = new System.Drawing.Point(519, 339);
            this.cb_Email.Name = "cb_Email";
            this.cb_Email.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cb_Email.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.cb_Email.Size = new System.Drawing.Size(137, 20);
            this.cb_Email.StyleController = this.layoutControl1;
            this.cb_Email.TabIndex = 14;
            // 
            // cb_Credit
            // 
            this.cb_Credit.Location = new System.Drawing.Point(519, 432);
            this.cb_Credit.Name = "cb_Credit";
            this.cb_Credit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cb_Credit.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.cb_Credit.Size = new System.Drawing.Size(137, 20);
            this.cb_Credit.StyleController = this.layoutControl1;
            this.cb_Credit.TabIndex = 12;
            // 
            // cb_MaxCredit
            // 
            this.cb_MaxCredit.Location = new System.Drawing.Point(519, 408);
            this.cb_MaxCredit.Name = "cb_MaxCredit";
            this.cb_MaxCredit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cb_MaxCredit.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.cb_MaxCredit.Size = new System.Drawing.Size(137, 20);
            this.cb_MaxCredit.StyleController = this.layoutControl1;
            this.cb_MaxCredit.TabIndex = 14;
            // 
            // cb_Debit
            // 
            this.cb_Debit.Location = new System.Drawing.Point(519, 456);
            this.cb_Debit.Name = "cb_Debit";
            this.cb_Debit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cb_Debit.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.cb_Debit.Size = new System.Drawing.Size(137, 20);
            this.cb_Debit.StyleController = this.layoutControl1;
            this.cb_Debit.TabIndex = 14;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3,
            this.layoutControlGroup1});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(743, 548);
            this.Root.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem3});
            this.layoutControlGroup2.Location = new System.Drawing.Point(466, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(257, 93);
            this.layoutControlGroup2.Text = "الملف";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.textEdit1;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(233, 24);
            this.layoutControlItem2.Text = "الملف";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(65, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.cb_Sheets;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(233, 24);
            this.layoutControlItem3.Text = "الصفحه";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(65, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciTax,
            this.layoutControlItem6,
            this.layoutControlItem13,
            this.layoutControlItem10,
            this.layoutControlItem12,
            this.emptySpaceItem1,
            this.layoutControlGroup4,
            this.layoutControlGroup6});
            this.layoutControlGroup3.Location = new System.Drawing.Point(466, 93);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(257, 435);
            this.layoutControlGroup3.Text = "ربط الحقول";
            // 
            // lciTax
            // 
            this.lciTax.Control = this.cb_Tax;
            this.lciTax.Location = new System.Drawing.Point(0, 24);
            this.lciTax.Name = "lciTax";
            this.lciTax.Size = new System.Drawing.Size(233, 24);
            this.lciTax.Text = "الرقم الضريبي";
            this.lciTax.TextSize = new System.Drawing.Size(65, 13);
            this.lciTax.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.cb_Group;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(233, 24);
            this.layoutControlItem6.Text = "المجموعة";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(65, 13);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.simpleButton1;
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 354);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(51, 26);
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.cb_Name;
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(233, 24);
            this.layoutControlItem10.Text = "الاسم";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(65, 13);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.spn_Start;
            this.layoutControlItem12.Location = new System.Drawing.Point(51, 354);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(182, 26);
            this.layoutControlItem12.Text = "بدأ من الصف";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(65, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 380);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(233, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(233, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(233, 10);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem11,
            this.layoutControlItem4});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 72);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(233, 165);
            this.layoutControlGroup4.Text = "بيانات الاتصال";
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.cb_City;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(209, 24);
            this.layoutControlItem7.Text = "المدينه ";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(65, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.cb_Address;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(209, 24);
            this.layoutControlItem8.Text = "العنوان";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(65, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.cb_Phone;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(209, 24);
            this.layoutControlItem9.Text = "المحمول ";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(65, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.cb_Mobile;
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(209, 24);
            this.layoutControlItem11.Text = "موبيل";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(65, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.cb_Email;
            this.layoutControlItem4.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem4.CustomizationFormText = "موبيل";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(209, 24);
            this.layoutControlItem4.Text = "الايميل";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(65, 13);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "بيانات الاتصال";
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem15,
            this.layoutControlItem17,
            this.layoutControlItem16});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 237);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(233, 117);
            this.layoutControlGroup6.Text = "الارصدة";
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.cb_Credit;
            this.layoutControlItem15.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem15.CustomizationFormText = "المحمول ";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(209, 24);
            this.layoutControlItem15.Text = "المدين";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(65, 13);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.cb_Debit;
            this.layoutControlItem17.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem17.CustomizationFormText = "موبيل";
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(209, 24);
            this.layoutControlItem17.Text = "الدائن";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(65, 13);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.cb_MaxCredit;
            this.layoutControlItem16.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem16.CustomizationFormText = "موبيل";
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(209, 24);
            this.layoutControlItem16.Text = "حد الاتمان";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(65, 13);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(466, 528);
            this.layoutControlGroup1.Text = "البيانات";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridControl1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(442, 483);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // frm_ImportFromExcelVendorCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(760, 467);
            this.Controls.Add(this.layoutControl1);
            this.IconOptions.LargeImage = global::ByStro.Properties.Resources.exporttoxls_32x32;
            this.Name = "frm_ImportFromExcelVendorCustomer";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "استيراد عملاء من ملف اكسيل";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cb_Tax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Group.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_City.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Address.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Phone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Mobile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_Start.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Sheets.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Email.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Credit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_MaxCredit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Debit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraEditors.ComboBoxEdit cb_Tax;
        private DevExpress.XtraEditors.ComboBoxEdit cb_Group;
        private DevExpress.XtraEditors.ComboBoxEdit cb_City;
        private DevExpress.XtraEditors.ComboBoxEdit cb_Address;
        private DevExpress.XtraEditors.ComboBoxEdit cb_Phone;
        private DevExpress.XtraEditors.ComboBoxEdit cb_Name;
        private DevExpress.XtraEditors.ComboBoxEdit cb_Mobile;
        private DevExpress.XtraEditors.SpinEdit spn_Start;
        private DevExpress.XtraEditors.ComboBoxEdit cb_Sheets;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem lciTax;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.ButtonEdit textEdit1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraEditors.ComboBoxEdit cb_Email;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.ComboBoxEdit cb_Credit;
        private DevExpress.XtraEditors.ComboBoxEdit cb_MaxCredit;
        private DevExpress.XtraEditors.ComboBoxEdit cb_Debit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
    }
}