﻿namespace ByStro.Forms
{
    partial class frm_ImportFromExcelnvoice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.cb_Code = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cb_Barcode = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cb_Price = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cb_Qty = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cb_Discount = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cb_Tax = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cb_Id = new DevExpress.XtraEditors.ComboBoxEdit();
            this.spn_Start = new DevExpress.XtraEditors.SpinEdit();
            this.cb_Sheets = new DevExpress.XtraEditors.ComboBoxEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.textEdit1 = new DevExpress.XtraEditors.ButtonEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.الباركود = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.الرصيد = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Barcode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Price.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Qty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Discount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Tax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Id.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_Start.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Sheets.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.الباركود)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.الرصيد)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.cb_Code);
            this.layoutControl1.Controls.Add(this.cb_Barcode);
            this.layoutControl1.Controls.Add(this.cb_Price);
            this.layoutControl1.Controls.Add(this.cb_Qty);
            this.layoutControl1.Controls.Add(this.cb_Discount);
            this.layoutControl1.Controls.Add(this.cb_Tax);
            this.layoutControl1.Controls.Add(this.cb_Id);
            this.layoutControl1.Controls.Add(this.spn_Start);
            this.layoutControl1.Controls.Add(this.cb_Sheets);
            this.layoutControl1.Controls.Add(this.gridControl1);
            this.layoutControl1.Controls.Add(this.textEdit1);
            this.layoutControl1.Controls.Add(this.simpleButton1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsView.RightToLeftMirroringApplied = true;
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(870, 423);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // cb_Code
            // 
            this.cb_Code.Location = new System.Drawing.Point(617, 162);
            this.cb_Code.Name = "cb_Code";
            this.cb_Code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cb_Code.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.cb_Code.Size = new System.Drawing.Size(169, 20);
            this.cb_Code.StyleController = this.layoutControl1;
            this.cb_Code.TabIndex = 7;
            // 
            // cb_Barcode
            // 
            this.cb_Barcode.Location = new System.Drawing.Point(617, 186);
            this.cb_Barcode.Name = "cb_Barcode";
            this.cb_Barcode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cb_Barcode.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.cb_Barcode.Size = new System.Drawing.Size(169, 20);
            this.cb_Barcode.StyleController = this.layoutControl1;
            this.cb_Barcode.TabIndex = 8;
            // 
            // cb_Price
            // 
            this.cb_Price.Location = new System.Drawing.Point(617, 210);
            this.cb_Price.Name = "cb_Price";
            this.cb_Price.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cb_Price.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.cb_Price.Size = new System.Drawing.Size(169, 20);
            this.cb_Price.StyleController = this.layoutControl1;
            this.cb_Price.TabIndex = 9;
            // 
            // cb_Qty
            // 
            this.cb_Qty.Location = new System.Drawing.Point(617, 234);
            this.cb_Qty.Name = "cb_Qty";
            this.cb_Qty.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cb_Qty.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.cb_Qty.Size = new System.Drawing.Size(169, 20);
            this.cb_Qty.StyleController = this.layoutControl1;
            this.cb_Qty.TabIndex = 10;
            // 
            // cb_Discount
            // 
            this.cb_Discount.Location = new System.Drawing.Point(617, 282);
            this.cb_Discount.Name = "cb_Discount";
            this.cb_Discount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cb_Discount.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.cb_Discount.Size = new System.Drawing.Size(169, 20);
            this.cb_Discount.StyleController = this.layoutControl1;
            this.cb_Discount.TabIndex = 11;
            // 
            // cb_Tax
            // 
            this.cb_Tax.Location = new System.Drawing.Point(617, 258);
            this.cb_Tax.Name = "cb_Tax";
            this.cb_Tax.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cb_Tax.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.cb_Tax.Size = new System.Drawing.Size(169, 20);
            this.cb_Tax.StyleController = this.layoutControl1;
            this.cb_Tax.TabIndex = 12;
            // 
            // cb_Id
            // 
            this.cb_Id.Location = new System.Drawing.Point(617, 138);
            this.cb_Id.Name = "cb_Id";
            this.cb_Id.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cb_Id.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.cb_Id.Size = new System.Drawing.Size(169, 20);
            this.cb_Id.StyleController = this.layoutControl1;
            this.cb_Id.TabIndex = 13;
            // 
            // spn_Start
            // 
            this.spn_Start.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spn_Start.Location = new System.Drawing.Point(668, 306);
            this.spn_Start.Name = "spn_Start";
            this.spn_Start.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spn_Start.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.spn_Start.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.spn_Start.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.spn_Start.Size = new System.Drawing.Size(118, 20);
            this.spn_Start.StyleController = this.layoutControl1;
            this.spn_Start.TabIndex = 15;
            // 
            // cb_Sheets
            // 
            this.cb_Sheets.Location = new System.Drawing.Point(617, 69);
            this.cb_Sheets.Name = "cb_Sheets";
            this.cb_Sheets.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cb_Sheets.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.cb_Sheets.Size = new System.Drawing.Size(169, 20);
            this.cb_Sheets.StyleController = this.layoutControl1;
            this.cb_Sheets.TabIndex = 6;
            this.cb_Sheets.SelectedIndexChanged += new System.EventHandler(this.cb_Sheets_SelectedIndexChanged);
            // 
            // gridControl1
            // 
            this.gridControl1.Location = new System.Drawing.Point(24, 45);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(565, 354);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            this.gridControl1.DataSourceChanged += new System.EventHandler(this.gridControl1_DataSourceChanged);
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(617, 45);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.textEdit1.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.textEdit1.Size = new System.Drawing.Size(169, 20);
            this.textEdit1.StyleController = this.layoutControl1;
            this.textEdit1.TabIndex = 5;
            this.textEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.textEdit1_ButtonClick);
            this.textEdit1.EditValueChanged += new System.EventHandler(this.textEdit1_EditValueChanged);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(617, 306);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(47, 22);
            this.simpleButton1.StyleController = this.layoutControl1;
            this.simpleButton1.TabIndex = 16;
            this.simpleButton1.Text = "متابعه";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3,
            this.layoutControlGroup1});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(870, 423);
            this.Root.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem3});
            this.layoutControlGroup2.Location = new System.Drawing.Point(593, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(257, 93);
            this.layoutControlGroup2.Text = "الملف";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.textEdit1;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(233, 24);
            this.layoutControlItem2.Text = "الملف";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(57, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.cb_Sheets;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(233, 24);
            this.layoutControlItem3.Text = "الصفحه";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(57, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem8,
            this.layoutControlItem7,
            this.layoutControlItem6,
            this.layoutControlItem13,
            this.layoutControlItem10,
            this.الباركود,
            this.layoutControlItem12,
            this.emptySpaceItem1,
            this.الرصيد,
            this.layoutControlItem9});
            this.layoutControlGroup3.Location = new System.Drawing.Point(593, 93);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(257, 310);
            this.layoutControlGroup3.Text = "ربط الحقول";
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.cb_Discount;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 144);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(233, 24);
            this.layoutControlItem8.Text = "الخصم";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(57, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.cb_Qty;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(233, 24);
            this.layoutControlItem7.Text = "الكمية";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(57, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.cb_Price;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(233, 24);
            this.layoutControlItem6.Text = "السعر";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(57, 13);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.simpleButton1;
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 168);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(51, 26);
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.cb_Id;
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(233, 24);
            this.layoutControlItem10.Text = "م";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(57, 13);
            // 
            // الباركود
            // 
            this.الباركود.Control = this.cb_Barcode;
            this.الباركود.Location = new System.Drawing.Point(0, 48);
            this.الباركود.Name = "الباركود";
            this.الباركود.Size = new System.Drawing.Size(233, 24);
            this.الباركود.TextSize = new System.Drawing.Size(57, 13);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.spn_Start;
            this.layoutControlItem12.Location = new System.Drawing.Point(51, 168);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(182, 26);
            this.layoutControlItem12.Text = "بدأ من الصف";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(57, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 194);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(233, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(233, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(233, 71);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // الرصيد
            // 
            this.الرصيد.Control = this.cb_Code;
            this.الرصيد.Location = new System.Drawing.Point(0, 24);
            this.الرصيد.Name = "الرصيد";
            this.الرصيد.Size = new System.Drawing.Size(233, 24);
            this.الرصيد.Text = "الكود";
            this.الرصيد.TextSize = new System.Drawing.Size(57, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.cb_Tax;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(233, 24);
            this.layoutControlItem9.Text = "الضريبة";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(57, 13);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(593, 403);
            this.layoutControlGroup1.Text = "البيانات";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridControl1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(569, 358);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // frm_ImportFromExcelnvoice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(870, 423);
            this.Controls.Add(this.layoutControl1);
            this.IconOptions.LargeImage = global::ByStro.Properties.Resources.exporttoxls_32x32;
            this.Name = "frm_ImportFromExcelnvoice";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "استيراد الفواتير من ملف اكسيل";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cb_Code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Barcode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Price.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Qty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Discount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Tax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Id.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_Start.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Sheets.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.الباركود)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.الرصيد)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraEditors.ComboBoxEdit cb_Code;
        private DevExpress.XtraEditors.ComboBoxEdit cb_Barcode;
        private DevExpress.XtraEditors.ComboBoxEdit cb_Price;
        private DevExpress.XtraEditors.ComboBoxEdit cb_Qty;
        private DevExpress.XtraEditors.ComboBoxEdit cb_Discount;
        private DevExpress.XtraEditors.ComboBoxEdit cb_Tax;
        private DevExpress.XtraEditors.ComboBoxEdit cb_Id;
        private DevExpress.XtraEditors.SpinEdit spn_Start;
        private DevExpress.XtraEditors.ComboBoxEdit cb_Sheets;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem الرصيد;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem الباركود;
        private DevExpress.XtraEditors.ButtonEdit textEdit1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
    }
}