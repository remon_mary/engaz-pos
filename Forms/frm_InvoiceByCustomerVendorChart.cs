﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using ByStro.DAL;
using static ByStro.Clases.MasterClass;
using DevExpress.XtraCharts;
using System.Globalization;
using ByStro.QueryViews;
using DevExpress.Utils;

namespace ByStro.Forms
{
    public partial class frm_InvoiceByCustomerVendorChart : DevExpress.XtraEditors.XtraForm
    {
        DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
        PartTypes partTypes;
        public frm_InvoiceByCustomerVendorChart(PartTypes _partTypes)
        {
            InitializeComponent(); 
            partTypes = _partTypes;
            switch (partTypes)
            {
                case PartTypes.Vendor:
                    lkp_Customers.Properties.DataSource = db.Pr_Vendors.Select(x =>
                              new CustomerView
                              {
                                  ID = x.ID,
                                  Name = x.Name,
                                  Phone = x.Phone,
                                  Address = x.Address,
                              }); break;
                case PartTypes.Customer:
                    lkp_Customers.Properties.DataSource = db.Sl_Customers.Select(x =>
                              new CustomerView
                              {
                                  ID = x.ID,
                                  Name = x.Name,
                                  Phone = x.Phone,
                                  Address = x.Address,
                              });
                    break;
                default:
                    break;
            }

            lkp_Customers.Properties.DisplayMember = "Name";
            lkp_Customers.Properties.ValueMember = "ID";
            lkp_Customers.Properties.NullText = "";
            lkp_Customers.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;

            lkp_Customers.Properties.ValidateOnEnterKey = true;
            lkp_Customers.Properties.AllowNullInput = DefaultBoolean.False;
            lkp_Customers.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            lkp_Customers.Properties.ImmediatePopup = true;
            var repoView = lkp_Customers.Properties.View;


            repoView.OptionsSelection.UseIndicatorForSelection = true;
            repoView.OptionsView.ShowAutoFilterRow = true;
            repoView.PopulateColumns(lkp_Customers.Properties.DataSource);

            repoView.Columns["ID"].Caption = "كود";
            repoView.Columns["Name"].Caption = "الاسم";
            repoView.Columns["Phone"].Caption = "الهاتف";
            repoView.Columns["Phone1"].Caption = "موبيل";
            repoView.Columns["Address"].Caption = "العنوان";
        }
        class temp
        {
            public temp(string month)
            {
                Month = month;
            }    
            [DisplayName("الشهر")]
            public string Month { get; set; }
            [DisplayName("مشتريات")]
            public double SalesInvoice { get; set; }
            [DisplayName("مرتجع شراء")]
            public double SalesReturn { get; set; }
            [DisplayName("مبيعات")]
            public double PurchaseInvoice { get; set; }
            [DisplayName("مرتجع بيع")]
            public double PurchaseReturn { get; set; }
        }
        private void gridLookUpEdit1_EditValueChanged(object sender, EventArgs e)
        {
            List<temp> temps = new List<temp>();
            var Inv_Invoices = db.Inv_Invoices.Where(x => x.Date.Year == DateTime.Now.Year && x.PartID == (int)lkp_Customers.EditValue && x.PartType == (byte)partTypes).GroupBy(x=>x.Date.Month).Select(x=>new { Month=x.Key , Invoices = x}).ToList();
            this.chartControl1.Series.Clear();
            for (int i = 1; i < 13; i++)
            {
                Series series = new Series(DateTimeFormatInfo.CurrentInfo.GetMonthName(i), ViewType.Bar);
                var temp = new temp(DateTimeFormatInfo.CurrentInfo.GetMonthName(i));
                var source = Inv_Invoices.SingleOrDefault(x => x.Month == i);
                if (source!=null)
                {
                    var SalesInvoice = source.Invoices.Where(inv => inv.InvoiceType == (byte)InvoiceType.SalesInvoice).Sum(x => x.Net);
                    var SalesReturn = source.Invoices.Where(inv => inv.InvoiceType == (byte)InvoiceType.SalesReturn).Sum(x => x.Net);
                    var PurchaseInvoice = source.Invoices.Where(inv => inv.InvoiceType == (byte)InvoiceType.PurchaseInvoice).Sum(x => x.Net);
                    var PurchaseReturn = source.Invoices.Where(inv => inv.InvoiceType == (byte)InvoiceType.PurchaseReturn).Sum(x => x.Net);
                    series.LabelsVisibility = DevExpress.Utils.DefaultBoolean.True;
                    series.Points.Add(new SeriesPoint("المشتريات", SalesInvoice));
                    series.Points.Add(new SeriesPoint("مرتجع شراء", SalesReturn));
                    series.Points.Add(new SeriesPoint("مبيعات", PurchaseInvoice));
                    series.Points.Add(new SeriesPoint("مرتجع بيع", PurchaseReturn));
                    temp.SalesInvoice = SalesInvoice;
                    temp.SalesReturn = SalesReturn;
                    temp.PurchaseInvoice = PurchaseInvoice;
                    temp.PurchaseReturn = PurchaseReturn;
                }
                temps.Add(temp);
                this.chartControl1.Series.Add(series);
            }
            gridControl1.DataSource = temps;
        }
    }
}