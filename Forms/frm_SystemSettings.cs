﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars;
using ByStro.DAL;
using ByStro.Clases; 


namespace ByStro.Forms
{
    public partial class frm_SystemSettings : frm_Master
    {
        SystemSetting SystemSetting;
        public frm_SystemSettings()
        {
            InitializeComponent(); 
        }
       
        private bool ValidData()
        {

            if(checkEdit1.Checked == true)
            {

                if (spinEdit1.EditValue.ValidAsIntNonZero() == false)
                {
                    spinEdit1.ErrorText = "يجب ان تكون القيمه اكبر من 1";
                    spinEdit1.Focus();
                    return false;
                }
                if (spinEdit11 .EditValue.ValidAsIntNonZero() == false)
                {
                    spinEdit11.ErrorText = "يجب ان تكون القيمه اكبر من 1";
                    spinEdit11.Focus();
                    return false;
                }
                if (comboBoxEdit1 .SelectedIndex < 0 )
                {
                    comboBoxEdit1.ErrorText = "*";
                    comboBoxEdit1.Focus();
                    return false;
                }
            }
            return true;


        }
        public override void Save()
        {

            if (!ValidData()) { return; }
            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
            SystemSetting = db.SystemSettings.First();
            if(SystemSetting == null)
            {
                SystemSetting = new SystemSetting();
                db.SystemSettings.InsertOnSubmit(SystemSetting);
            }
            SystemSetting.ItemCodeLength = spinEdit1.EditValue.ToInt();
            SystemSetting.WeightCodeLength = spinEdit11.EditValue.ToInt();
            SystemSetting.DivideWeightBy = (int)Math.Pow(10.0, Convert.ToDouble(comboBoxEdit1.SelectedIndex + 1)) / 10;
            SystemSetting.UseScalBarcode = checkEdit1.Checked;
            db.SubmitChanges();
            base.Save();
            RefreshData();
        }

        public override void Delete()
        {
 
        }
        public override void New()
        {

        
        }
        public override void RefreshData()
        {
        }
        public override void frm_Load(object sender, EventArgs e)
        {
            base.frm_Load(sender, e);
            layoutControl1.AllowCustomization = false;
            btn_Print.Visibility = btn_Delete.Visibility = btn_Print.Visibility =btn_New.Visibility  = btn_List.Visibility = BarItemVisibility.Never;
            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
            SystemSetting = db.SystemSettings.First();
            if (SystemSetting == null)
            {
                SystemSetting = new SystemSetting(); 
            }

            spinEdit1.EditValue = SystemSetting.ItemCodeLength ;
            spinEdit11.EditValue=SystemSetting.WeightCodeLength; 
            checkEdit1.Checked= SystemSetting.UseScalBarcode  ;
            comboBoxEdit1.SelectedIndex = (SystemSetting.DivideWeightBy  ).ToString().Length - 1;
            #region DataChanged

            spinEdit1.EditValueChanged += DataChanged;
            spinEdit11.EditValueChanged += DataChanged;
            checkEdit1.EditValueChanged += DataChanged;

            #endregion
        }
        public override void Print()
        {


        }
    }
}
