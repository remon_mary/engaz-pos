﻿using ByStro.Clases;
using ByStro.DAL;
using DevExpress.LookAndFeel;
using DevExpress.XtraCharts;
using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ByStro.Forms
{
    public partial class frm_ProfitAndLosesAnalysis : XtraForm
    {
        public frm_ProfitAndLosesAnalysis()
        {
            InitializeComponent();
             ValueChanged();
        }
        private void  ValueChanged()
        { 
            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
           
            var GroupSales = db.Inv_Invoices.Where(s =>s.Date.Year==DateTime.Now.Year&& s.InvoiceType == (int)MasterClass.InvoiceType.SalesInvoice).GroupBy(s => s.Date.Month).Select(s=>new { Month=s.Key, IDs=s.Select(a=>a.ID) }).OrderBy(s=>s.Month).ToList();
            var GroupReturn = db.Inv_Invoices.Where(s =>s.Date.Year==DateTime.Now.Year&& s.InvoiceType == (int)MasterClass.InvoiceType.SalesReturn).GroupBy(s => s.Date.Month).Select(s=>new { Month=s.Key, IDs=s.Select(a=>a.ID) }).OrderBy(s=>s.Month).ToList();
            var GroupExpenses = db.Acc_RevExpEntries.Where(s =>s.EntryDate.Year==DateTime.Now.Year&& s.IsRevenue==false).GroupBy(s => s.EntryDate.Month).Select(s=>new { Month=s.Key, IDs=s.Select(a=>a.ID) }).OrderBy(s=>s.Month).ToList();
            this.chartControl1.Series.Clear();
            List < ProfitAndLosesAnalysis > profitAndLosesAnalyses=new List<ProfitAndLosesAnalysis>();
            for (int i = 1; i < 13; i++)
            {
                Series series = new Series(DateTimeFormatInfo.CurrentInfo.GetAbbreviatedMonthName(i), ViewType.Bar);
                series.LabelsVisibility = DevExpress.Utils.DefaultBoolean.True;
                ProfitAndLosesAnalysis profitAndLosesAnalysis = new ProfitAndLosesAnalysis(DateTimeFormatInfo.CurrentInfo.GetAbbreviatedMonthName(i));
                //double SalesProfite =0;
                //double TotalSoldItems = 0;
                //double TotalReturnItems = 0; 
                //double TotalSoldCost = 0; 
                //double TotalReturnCost = 0;
               
                var SalesMonth = GroupSales.FirstOrDefault(x => x.Month == i);
                if (SalesMonth!=null)
                {
                    var SalesDetails = db.Inv_InvoiceDetails.Where(sd => SalesMonth.IDs.Contains(sd.InvoiceID));
                    profitAndLosesAnalysis.TotalSoldItems =Math.Round( SalesDetails.Sum(sd => (double?)sd.TotalPrice) ?? 0,2,MidpointRounding.AwayFromZero);
                    profitAndLosesAnalysis.TotalSoldCost = Math.Round(SalesDetails.Sum(sd => (double?)sd.TotalCostValue) ?? 0, 2, MidpointRounding.AwayFromZero);

                }
                var ReturnMonth = GroupReturn.FirstOrDefault(x => x.Month == i);
                if (ReturnMonth != null)
                {
                    var SalesRetirnDetails = db.Inv_InvoiceDetails.Where(sd => ReturnMonth.IDs.Contains(sd.InvoiceID));
                    profitAndLosesAnalysis.TotalReturnItems = Math.Round(SalesRetirnDetails.Sum(sd => (double?)sd.TotalPrice) ?? 0,2,MidpointRounding.AwayFromZero);
                    profitAndLosesAnalysis.TotalReturnCost = Math.Round(SalesRetirnDetails.Sum(sd => (double?)sd.TotalCostValue) ?? 0, 2, MidpointRounding.AwayFromZero);

                }
                var ExpensesMonth = GroupExpenses.FirstOrDefault(x => x.Month == i);
                if (ExpensesMonth != null)
                {
                    var ExpensesDetails = db.Acc_RevExpEntryDetails.Where(sd => ExpensesMonth.IDs.Contains(sd.RevExpEntryID));
                    profitAndLosesAnalysis.TotalExpenses = Math.Round(ExpensesDetails.Sum(sd => (double?)sd.Amount) ?? 0, 2, MidpointRounding.AwayFromZero);

                }
                //profitAndLosesAnalysis.TotalExpenses = Math.Round(db.exp.Where(s => s.MyDate.HasValue && s.MyDate.Value.Year == DateTime.Now.Year && s.MyDate.Value.Month == i).Sum(sd => sd.ExpenseValue) ?? 0, 2, MidpointRounding.AwayFromZero);

                SeriesPoint TotalSoldItemsPoint = new SeriesPoint("اجمالي المبيعات", profitAndLosesAnalysis.TotalSoldItems);
                series.Points.Add(TotalSoldItemsPoint);

                SeriesPoint TotalSoldCostPoint = new SeriesPoint("اجمالي مردود المبيعات", profitAndLosesAnalysis.TotalSoldCost);
                series.Points.Add(TotalSoldCostPoint);

                SeriesPoint TotalReturnItemsPoint = new SeriesPoint("تكلفه البضاعه المباعه", profitAndLosesAnalysis.TotalReturnItems);
                series.Points.Add(TotalReturnItemsPoint);

                SeriesPoint TotalReturnCostPoint = new SeriesPoint("تكلفه البضاعه المردوده", profitAndLosesAnalysis.TotalReturnCost);
                series.Points.Add(TotalReturnCostPoint);

                SeriesPoint SalesProfitePoint = new SeriesPoint("الربح من المبيعات", profitAndLosesAnalysis.SalesProfite);
                series.Points.Add(SalesProfitePoint);

                SeriesPoint TotalExpensesPoint = new SeriesPoint("المصروفات", profitAndLosesAnalysis.TotalExpenses);
                series.Points.Add(TotalExpensesPoint);
                
                SeriesPoint NetProfitePoint = new SeriesPoint("صافي الربح", (Math.Abs(profitAndLosesAnalysis.NetProfite)));
                NetProfitePoint.Color = (profitAndLosesAnalysis.NetProfite >= 0) ? DXSkinColors.FillColors.Success : DXSkinColors.FillColors.Danger;
                NetProfitePoint.Tag = profitAndLosesAnalysis.NetProfiteState;
                series.Points.Add(NetProfitePoint);
               
                this.chartControl1.Series.Add(series);
                
                profitAndLosesAnalyses.Add(profitAndLosesAnalysis);
                
                gridControl1.DataSource = profitAndLosesAnalyses;
            }




            //textEdit1.Text = TotalSoldItems.ToString();
            //textEdit2.Text = TotalSoldCost.ToString();
            //textEdit7.Text = TotalReturnItems.ToString();
            //textEdit8.Text = TotalReturnCost.ToString();

            //  var SalesProfite = ((TotalSoldItems - TotalReturnItems) - (TotalSoldCost - TotalReturnCost));
            // textEdit3.Text = SalesProfite.ToString();

            // var TotalExpenses = db.Expenses.Where(s => s.MyDate.Value.Date >= dateEdit1.DateTime.Date && s.MyDate.Value.Date <= dateEdit2.DateTime.Date).Sum(sd => sd.ExpenseValue) ?? 0;

            //  textEdit5.Text = TotalExpenses.ToString();
            //   var NetProfite = SalesProfite - TotalExpenses;
            //  textEdit6.Text = (Math.Abs(NetProfite)).ToString();
            //  textEdit4.Text = (NetProfite >= 0) ? "ربح" : "خساره";

            //  textEdit4.BackColor = (NetProfite >= 0) ? DXSkinColors.FillColors.Success : DXSkinColors.FillColors.Danger;

        }
        public class ProfitAndLosesAnalysis
        {
            public ProfitAndLosesAnalysis(string monthName)
            {
                MonthName = monthName;
            }
            [Display(Name = "الشهر")]
            public string MonthName { get; set; }
            [Display(Name = "اجمالي المبيعات")]
            public double TotalSoldItems { get; set; }
            [Display(Name = "اجمالي مردود المبيعات")]
            public double TotalReturnItems { get; set; }
            [Display(Name = "تكلفه البضاعه المباعه")]
            public double TotalSoldCost { get; set; }
            [Display(Name = "تكلفه البضاعه المردوده")]
            public double TotalReturnCost { get; set; }
            [Display(Name = "الربح من المبيعات")]
            public double SalesProfite
            {
                get
                {
                    return ((TotalSoldItems - TotalReturnItems) - (TotalSoldCost - TotalReturnCost));
                }
            }
            [Display(Name = "المصروفات")]
            public double TotalExpenses { get; set; }
           
            [Display(Name = "صافي الربح")]
            public double NetProfite
            {
                get
                {
                    return SalesProfite - TotalExpenses;

                }
            }
            [Display(Name = "الحالة")]
            public string NetProfiteState
            {
                get
                {
                    return (NetProfite >= 0) ? "ربح" : "خساره";

                }
            }
        }
    }
}
