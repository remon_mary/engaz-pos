﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.DataAccess.Excel;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraSplashScreen;
using DevExpress.SpreadsheetSource;
using System.Threading;
using DevExpress.XtraWaitForm;
using ByStro.UControle;
using DevExpress.CodeParser;
using static ByStro.Forms.frm_AddVendorCustomerRange;

namespace ByStro.Forms
{
    public partial class frm_ImportFromExcelVendorCustomer : XtraForm
    {

        bool IsCustomer = true;
        int GroupDefault = 0;
        BindingList<VendorCustomer> vendorCustomers;
        public frm_ImportFromExcelVendorCustomer(bool IsCustomer, ref BindingList<VendorCustomer>  vendorCustomers,int GroupDefault)
        {
            InitializeComponent();
            if (!IsCustomer)
            {
                lciTax.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                this.IsCustomer = IsCustomer;
                this.Text = "استيراد الموردين من ملف اكسيل";
            }
            this.vendorCustomers = vendorCustomers;
            this.GroupDefault = GroupDefault;
        }

        private void textEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            XtraOpenFileDialog dialog = new XtraOpenFileDialog();
            dialog.Filter = "Excel File(*.xls)|*.xls|Excel File(*.xlsx)|*.xlsx";
            if (dialog.ShowDialog() != DialogResult.OK)
                return;
            textEdit1.Text = dialog.FileName;
        }

        private void cb_Sheets_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cb_Sheets.SelectedIndex >= 0 && cb_Sheets.Text != string.Empty)
            {
                try
                {
                    gridControl1.DataSource = null;
                    gridView1.Columns.Clear();

                    ExcelDataSource ds = new ExcelDataSource();
                    ds.FileName = textEdit1.Text;
                    DevExpress.DataAccess.Excel.ExcelSourceOptions excelSourceOptions1 = new DevExpress.DataAccess.Excel.ExcelSourceOptions();
                    DevExpress.DataAccess.Excel.ExcelWorksheetSettings excelWorksheetSettings1 = new DevExpress.DataAccess.Excel.ExcelWorksheetSettings();
                    excelWorksheetSettings1.WorksheetName = cb_Sheets.Text;
                    excelSourceOptions1.ImportSettings = excelWorksheetSettings1;
                    ds.SourceOptions = excelSourceOptions1;
                    ds.Fill();

                    gridControl1.DataSource = ds;
                    gridView1.PopulateColumns();

                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message);
                }
            }
        }

        private void textEdit1_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                cb_Sheets.Properties.Items.Clear();
                cb_Sheets.Properties.Items.AddRange(GetExcelSheetNames(textEdit1.Text));
            }
            catch {}
        }

        public static string[] GetExcelSheetNames(string filelocation)
        {  
            using (ISpreadsheetSource spreadsheetSource = SpreadsheetSourceFactory.CreateSource(filelocation))
            {
                IWorksheetCollection worksheetCollection = spreadsheetSource.Worksheets;
                return worksheetCollection.Select(x => x.Name).ToArray();
            } 
        }
        private void gridControl1_DataSourceChanged(object sender, EventArgs e)
        {
            cb_Tax.Properties.Items.Clear();
            cb_City.Properties.Items.Clear();
            cb_Mobile .Properties.Items.Clear();
            cb_Phone.Properties.Items.Clear();
            cb_Name.Properties.Items.Clear();
            cb_Group.Properties.Items.Clear();
            cb_Address.Properties.Items.Clear();
            cb_Email.Properties.Items.Clear();
            cb_MaxCredit.Properties.Items.Clear();
            cb_Credit.Properties.Items.Clear();
            cb_Debit.Properties.Items.Clear();

            cb_Tax.Properties.Items.Add("");
            cb_City.Properties.Items.Add("");
            cb_Phone.Properties.Items.Add("");
            cb_Mobile.Properties.Items.Add("");
            cb_Name.Properties.Items.Add("");
            cb_Group.Properties.Items.Add("");
            cb_Address.Properties.Items.Add("");
            cb_Email.Properties.Items.Add("");
            cb_MaxCredit.Properties.Items.Add("");
            cb_Credit.Properties.Items.Add("");
            cb_Debit.Properties.Items.Add("");

            foreach (GridColumn column in gridView1.Columns)
            {
                cb_Tax.Properties.Items.Add(column.FieldName);
                cb_City.Properties.Items.Add(column.FieldName);
                cb_Phone.Properties.Items.Add(column.FieldName);
                cb_Mobile.Properties.Items.Add(column.FieldName);
                cb_Name.Properties.Items.Add(column.FieldName);
                cb_Group.Properties.Items.Add(column.FieldName);
                cb_Address.Properties.Items.Add(column.FieldName);
                cb_Email.Properties.Items.Add(column.FieldName);
                cb_MaxCredit.Properties.Items.Add(column.FieldName);
                cb_Credit.Properties.Items.Add(column.FieldName);
                cb_Debit.Properties.Items.Add(column.FieldName);
            }
        }
        void StartImporting()
        {
            SplashScreenManager.ShowForm(this, typeof(WaitForm1), true, true, false);
            if (cb_Name.SelectedIndex <= 0)
            {
                cb_Name.ErrorText = "هذا الحقل مطلوب";
                return;
            }

            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
            for (int i = Convert.ToInt32(spn_Start.EditValue) - 1; i < gridView1.RowCount; i++)
            {
                if (gridView1.GetRowCellValue(i, cb_Name.Text) != null && gridView1.GetRowCellValue(i, cb_Name.Text) != DBNull.Value &&
                        gridView1.GetRowCellValue(i, cb_Name.Text).ToString().Trim() != "")
                {
                    SplashScreenManager.Default.SetWaitFormDescription(i.ToString() + "%");

                    #region SelectedIndex

                    
                    
                    string tax = "";
 
                    if (cb_Tax.SelectedIndex > 0 && gridView1.GetRowCellValue(i, cb_Tax.Text) != null)
                        tax = gridView1.GetRowCellValue(i, cb_Tax.Text).ToString();

                    string City = "";
 
                    if (cb_City.SelectedIndex > 0 && gridView1.GetRowCellValue(i, cb_City.Text) != null)
                        City = gridView1.GetRowCellValue(i, cb_City.Text).ToString();
             
                    string Address = "";
 
                    if (cb_Address.SelectedIndex > 0 && gridView1.GetRowCellValue(i, cb_Address.Text) != null)
                        Address = gridView1.GetRowCellValue(i, cb_Address.Text).ToString();
             
                    string Phone = "";
 
                    if (cb_Phone.SelectedIndex > 0 && gridView1.GetRowCellValue(i, cb_Phone.Text) != null)
                        Phone = gridView1.GetRowCellValue(i, cb_Phone.Text).ToString();

                    string Mobile = "";
 
                    if (cb_Mobile.SelectedIndex > 0 && gridView1.GetRowCellValue(i, cb_Mobile.Text) != null)
                        Mobile = gridView1.GetRowCellValue(i, cb_Mobile.Text).ToString();

                    string Email = "";
 
                    if (cb_Email.SelectedIndex > 0 && gridView1.GetRowCellValue(i, cb_Email.Text) != null)
                        Email = gridView1.GetRowCellValue(i, cb_Email.Text).ToString();

                    int MaxCredit = 0;

                    if (cb_MaxCredit.SelectedIndex > 0 && gridView1.GetRowCellValue(i, cb_MaxCredit.Text) != null)
                        int.TryParse(gridView1.GetRowCellValue(i, cb_MaxCredit.Text).ToString(), out MaxCredit);
                    
                    double Credit = 0;

                    if (cb_Credit.SelectedIndex > 0 && gridView1.GetRowCellValue(i, cb_Credit.Text) != null)
                        double.TryParse(gridView1.GetRowCellValue(i, cb_Credit.Text).ToString(), out Credit);

                    double Debit = 0;

                    if (cb_Debit.SelectedIndex > 0 && gridView1.GetRowCellValue(i, cb_Debit.Text) != null)
                        double.TryParse(gridView1.GetRowCellValue(i, cb_Debit.Text).ToString(), out Debit);
                    #endregion

                    VendorCustomer vendorCustomer = new VendorCustomer()
                    {
                        Name = gridView1.GetRowCellValue(i, cb_Name.Text).ToString(),
                        Tax=tax,
                        City= City,
                        Address = Address,
                        Phone = Phone,
                        Email = Email,
                        MaxCredit = MaxCredit,
                        Credit = Credit,
                        Debit = Debit,
                    };
                    if (cb_Group.SelectedIndex > 0 && gridView1.GetRowCellValue(i, cb_Group.Text) != null)
                    {
                        int GroupID = 0;
                        if (IsCustomer)
                        {
                            var groups = db.Sl_CustomerGroups.Where(x => x.Name.Trim() == gridView1.GetRowCellValue(i, cb_Group.Text).ToString().Trim()).ToList();
                            if (groups.FirstOrDefault() != null)
                                GroupID = groups.FirstOrDefault().Number;
                            else
                            {
                                DAL.Sl_CustomerGroup Group = new DAL.Sl_CustomerGroup()
                                {
                                    Number = GetNewNumber(0),
                                    Name = gridView1.GetRowCellValue(i, cb_Group.Text).ToString(),
                                };
                                if (groups.Count() == 0)
                                {
                                    db.Sl_CustomerGroups.InsertOnSubmit(Group);
                                    db.SubmitChanges();
                                    GroupID = Group.Number;
                                }
                            }
                        }
                        else
                        {
                            var groups = db.Pr_VendorGroups.Where(x => x.Name.Trim() == gridView1.GetRowCellValue(i, cb_Group.Text).ToString().Trim()).ToList();
                            if (groups.FirstOrDefault() != null)
                                GroupID = groups.FirstOrDefault().Number;
                            else
                            {
                                DAL.Pr_VendorGroup Group = new DAL.Pr_VendorGroup()
                                {
                                    Number= GetNewNumber(0),
                                    Name = gridView1.GetRowCellValue(i, cb_Group.Text).ToString(),
                                    ParentID=0
                                };
                                if (groups.Count() == 0)
                                {
                                    db.Pr_VendorGroups.InsertOnSubmit(Group);
                                    db.SubmitChanges();
                                    GroupID = Group.Number;
                                }
                            }
                        }
                        vendorCustomer.Group = GroupID;
                    }
                    else
                        vendorCustomer.Group = GroupDefault;
                    
                    vendorCustomers.Add(vendorCustomer);
                }
            }
            SplashScreenManager.CloseForm(false);
            this.Close();
        }
        int GetNewNumber(int? parent)
        {
            try
            {
                DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
                if(IsCustomer)
                    return (int)db.Sl_CustomerGroups.Where(n => n.ParentID == parent).Max(n => n.Number) + 1;
                return (int)db.Pr_VendorGroups.Where(n => n.ParentID == parent).Max(n => n.Number) + 1;

            }
            catch
            {
                return (int)parent * 100 + 1;
            }

        }
        private void simpleButton1_Click(object sender, EventArgs e)
        {
             StartImporting();
        }

       
    }
  }