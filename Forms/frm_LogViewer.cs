﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms; 
using DevExpress.XtraEditors;
namespace ByStro.Forms
{
   
    public partial class frm_LogViewer : DevExpress.XtraEditors.XtraForm
    {
        public frm_LogViewer()
        {
            InitializeComponent();
            simpleButton1.Text ="اغلاق";
        }
        public static void ViewLog(string Caption, String Log)
        {
            frm_LogViewer frm = new frm_LogViewer();
            frm.Text = Caption;
            frm.memoEdit1.Text = Log;
            frm.ShowDialog();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            this.Close();

        }
    }
}