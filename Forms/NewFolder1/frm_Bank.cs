﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraLayout;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Linq.Expressions;
using DevExpress.XtraBars;
using ByStro. DAL;
using ByStro.Forms;
using ByStro.Clases;

namespace ByStro.Forms 
{
    public partial class frm_Bank : frm_Master
    {
        Acc_Bank bank;
        
        public frm_Bank( )
        {
            InitializeComponent();
            usc_OpenBalance1 = new GUI.Usc_OpenBalance();
            New();
            GetData(); 
        }
        public frm_Bank(int id)
        {
            InitializeComponent();
            usc_OpenBalance1 = new GUI.Usc_OpenBalance();
            GoTo(id);
        }
        private void frm_Bank_Load(object sender, EventArgs e)
        {
            btn_List.Visibility=btn_Print.Visibility=btn_SaveAndPrint.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
        }
        public  void GoTo(int id)
        {
            if (id.ToString() == TextEdit_ID.Text) return;
            if (ChangesMade && !SaveChangedData()) return;
            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
            bank = db.Acc_Banks.Where(x => x.ID == id).SingleOrDefault();
            if (bank != null) { 
                GetData();
                IsNew = false;
            }
        }
        public bool CheckIfNameIsUsed(string Name)
        {
            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
            var obj = from s in db.Acc_Banks where this.TextEdit_ID.Text != s.ID.ToString() && s.BankName == Name select s.ID;
            return (obj.Count() > 0);
        }
        public bool CheckIfCodeIsUsed(string Code)
        {
            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
            var obj = from s in db.Acc_Banks where this.TextEdit_ID.Text != s.ID.ToString() && s.BankAccountNumber == Code select s.ID;
            return (obj.Count() > 0);
        }
        private bool ValidData()
        {

           
            if (TextEdit_BankName.EditValue == null || String.IsNullOrEmpty(TextEdit_BankName.Text))
            {
                TextEdit_BankName.ErrorText = LangResource.ErrorCantBeEmpry;
                TextEdit_BankName.Focus();
                return false;
            }
            if (TextEdit_BankAccountNumber.EditValue == null || String.IsNullOrEmpty(TextEdit_BankAccountNumber.Text))
            {
                TextEdit_BankAccountNumber.ErrorText = LangResource.ErrorCantBeEmpry;
                TextEdit_BankAccountNumber.Focus();
                return false;
            }
             
            if (usc_OpenBalance1.Validate() == false)
                return false;
            return true;
        }
        public override void Save()
        {
            if (CanSave() == false) return;
            if (!ValidData()) { return; }
            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
            if (IsNew)
            {
                bank = new Acc_Bank();
                db.Acc_Banks.InsertOnSubmit(bank);
                GenrateAccounts();
            }
            else
            {
                bank = db.Acc_Banks.Where(s => s.ID == bank.ID).First();
                Master.UpdateAccount((int)bank.AccountID , TextEdit_BankName .Text);
            }
            SetData();
            db.SubmitChanges();
            usc_OpenBalance1.Save();
                RefreshData();
            CurrentSession.UserAccessibleBanks = db.Acc_Banks.ToList();

            base.Save();
        }
        public override void Delete()
        {
            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
            if (CanPerformDelete() == false ) return;
            if (IsNew) return;
            var acctevetylog = from i in db.Acc_ActivityDetials
                               join l in db.Acc_Activities on i.AcivityID equals l.ID
                               where i.ACCID == bank.AccountID && l.Type != "0"
                               select i.ID;
            if (acctevetylog.Count() > 0)
            {
                XtraMessageBox.Show(LangResource.CantDeleteDrawerHasTransactions, "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            var PayCards = db.Acc_PayCards.Where(x => x.BankID == bank.ID).Count();
            if (PayCards > 0)
            {
                XtraMessageBox.Show(LangResource.CantDeleteBankUsedByPayCards , "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            PartNumber = bank.ID.ToString();
            PartName = bank.BankAccountNumber;
            if (Master.AskForDelete(this, IsNew, PartName, PartNumber))
            {
                bank = db.Acc_Banks.Where(c => c.ID == bank.ID).First();
                db.Acc_Banks.DeleteOnSubmit(bank);
                db.Acc_Accounts.DeleteOnSubmit(db.Acc_Accounts.Where(a => a.ID == bank.AccountID ).First());
                db.SubmitChanges();
                base.Delete();
                New();
            }
        }
        void SetData()
        {
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);

            bank.BankName = TextEdit_BankName.Text ;
            bank.BankAccountNumber = TextEdit_BankAccountNumber.Text;
            bank.Address = TextEdit_Address.Text;
            bank.Phone1 = TextEdit_Phone1.Text;
            bank.Phone2 = TextEdit_Phone2.Text;
            bank.Phone3 = TextEdit_Phone3.Text;
            bank.Notes = TextEdit_Notes.Text; 
            usc_OpenBalance1.Account = bank.AccountID;

        }

        public override void New()
        {
            bank = new Acc_Bank() ;
            bank.ID = GetNextID();
            base.New();
            IsNew = true;
            GetData();
            ChangesMade = false;
        }
        public override void RefreshData()
        {
            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
            var banks = (from c in db.Acc_Banks
                           join a in db.Acc_Accounts
                           on c.AccountID  equals a.ID
                           select new { c.ID, c.BankName  }).ToList();

            var branches = db.Inv_Stores.Select(x => new { x.ID, x.Name }).ToList();
            branches.Add(new {ID= 0,Name =  LangResource.All });
       
          

            usc_OpenBalance1.RefreshData();



            base.RefreshData();

        }
        public override void frm_Load(object sender, EventArgs e)
        {
            base.frm_Load(sender, e);
            layoutControl1.AllowCustomization = false;
            btn_Refresh.Visibility = BarItemVisibility.Never;
            btn_Print.Visibility = BarItemVisibility.Never;
            btn_List.Visibility = BarItemVisibility.Never;

            #region DataChanged

            TextEdit_ID.EditValueChanged += DataChanged;
            TextEdit_BankName.EditValueChanged += DataChanged;
            TextEdit_BankAccountNumber.EditValueChanged += DataChanged;
            TextEdit_Address.EditValueChanged += DataChanged;
            TextEdit_Phone1.EditValueChanged += DataChanged;
            TextEdit_Phone2.EditValueChanged += DataChanged;
            TextEdit_Phone3.EditValueChanged += DataChanged;
            TextEdit_Notes.EditValueChanged += DataChanged;
            #endregion 
        }
        private void GetData()
        {
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
            TextEdit_ID.EditValue = bank.ID;
            TextEdit_BankName.EditValue = bank.BankName;
            TextEdit_BankAccountNumber.EditValue = bank.BankAccountNumber;
            TextEdit_Address.EditValue = bank.Address;
            TextEdit_Phone1.EditValue = bank.Phone1;
            TextEdit_Phone2.EditValue = bank.Phone2;
            TextEdit_Phone3.EditValue = bank.Phone3;
            TextEdit_Notes.EditValue = bank.Notes;
            usc_OpenBalance1.Account = bank.AccountID;
            usc_OpenBalance1.GetData();
 

        }
        int GetNextID()
        {
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
            try
            {
                return (int)db.Acc_Banks.Max(n => n.ID) + 1;
            }
            catch
            {
                return (int)1;
            }
        }
        void GenrateAccounts()
        {
            bank.AccountID = (int)Master.InsertNewAccount(TextEdit_BankName .Text, CurrentSession.Company.BanksAccount  );
            RefreshData();
        }

        
    }
}
