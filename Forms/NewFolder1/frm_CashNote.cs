﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraDiagram.Bars;
using ByStro. DAL;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Columns;
using System.Linq.Expressions;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using System.Collections.ObjectModel;
using System.Data.Linq;
using ByStro.Clases;
using static ByStro.Clases.Master;

namespace ByStro.Forms
{
    
    public partial class frm_CashNote : frm_Master 
    {
        private bool ISCashIn;
        private int AccountID;

        private object CustomersList;
        private object VendorsList;
       // private object InvoicesList;
        Acc_CashNote  CashNote = new Acc_CashNote();
        public override Master.SystemProssess ProcessType
        {
            get
            {
                if (CashNote.IsCashNote) return Master.SystemProssess.CashNoteIn;
                else return Master.SystemProssess.CashNoteOut;
            }
        }
        public frm_CashNote(bool _ISCashIn)
        {
            InitializeComponent();
            lkp_PayType.EditValueChanged += Lkp_PayType_EditValueChanged; 
            ISCashIn = _ISCashIn;
            this.Name = (ISCashIn) ? "frm_CashNoteIn" : "frm_CashNoteOut";

            New();

        }
        /// <summary>
        /// Open new CashNote  
        /// </summary>
        /// <param name="_ISCashIn"> Cash trans type </param>
        /// <param name="ID"> Id  </param>
        /// <param name="IdType">    </param>
        public frm_CashNote(bool _ISCashIn, int ID ,String IdType)
        {
            InitializeComponent();
            lkp_PayType.EditValueChanged += Lkp_PayType_EditValueChanged;
            ISCashIn = _ISCashIn;
            this.Name = (ISCashIn) ? "frm_CashNoteIn" : "frm_CashNoteOut";
            New();
            switch (IdType)
            {
                case "Customer": CashNote.PartType  = 1; CashNote.PartID  = ID;  break; 
                case "Vendor":  CashNote.PartType = 0; CashNote.PartID = ID; break;   
                default:
                    LoadItemByCode(ID,_ISCashIn ); 
                    break;
            } 
        }
        public frm_CashNote(SystemProssess prossess  , int id ,int partType,int partId, int sourceType,  int sourceID   )
        {
            InitializeComponent();
            lkp_PayType.EditValueChanged += Lkp_PayType_EditValueChanged;
            if (id > 0)
            {
                ISCashIn = (prossess == SystemProssess.CashNoteIn);
                LoadItemByID(id);
                ISCashIn = CashNote.IsCashNote;
                GetData();
                return;
            }

            ISCashIn = (prossess == SystemProssess.CashNoteIn);
            this.Name = (ISCashIn) ? "frm_CashNoteIn" : "frm_CashNoteOut";
            New();
            CashNote.PartType = Convert.ToByte( partType);
            CashNote.PartID = partId;
            CashNote.LinkType = Convert.ToByte(sourceType);
            CashNote.LinkID = sourceID;
            GetData();
             
        }
        public frm_CashNote( int ID )
        {
            InitializeComponent();
            lkp_PayType.EditValueChanged += Lkp_PayType_EditValueChanged;
            LoadItemByID(ID);
            ISCashIn = CashNote.IsCashNote;
            GetData();

           // ProccessType = ISCashIn ? Master.SystemProssess.CashNoteIn : Master.SystemProssess.CashNoteOut;


        }

        public override void frm_Load(object sender, EventArgs e)
        {
            base.frm_Load(sender, e);
            btn_Print.Visibility = btn_List.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            LookUpEdit_PartType.Properties.DataSource = new[]
                    {
                      new { ID = 1 ,Name =  LangResource.Vendor  },
                      new { ID = 2 ,Name =  LangResource.Customer  },
                      new { ID = 3 ,Name =  LangResource.Employee  },
                      new { ID = 4 ,Name =  LangResource.Account  },
                    };
            
            this.Text = layoutControlGroup5 .Text = (ISCashIn) ? LangResource.CashNoteIn  : LangResource.CashNoteOut;
            this.Name = (ISCashIn) ? "frm_CashNoteIn" : "frm_CashNoteOut";
        
            lkp_Store.Properties.ValueMember = "ID";
            lkp_Store.Properties.DisplayMember = "Name";

            

            LookUpEdit_PartID.Properties.ValueMember = "ID";
            LookUpEdit_PartID.Properties.DisplayMember = "Name";

            LookUpEdit_PartType.Properties.ValueMember = "ID";
            LookUpEdit_PartType.Properties.DisplayMember = "Name";


            LookUpEdit_SourceID.Properties.ValueMember = "ID";
            LookUpEdit_SourceID.Properties.DisplayMember = "Code";


            LookUpEdit_Source.Properties.ValueMember = "ID";
            LookUpEdit_Source.Properties.DisplayMember = "Name";


            glkp_Source_EditValueChanged(null, null);
            
            #region DataChangedEventHandlers 
            dt_Date.EditValueChanged += DataChanged;
            memoEdit1.EditValueChanged += DataChanged;
            lkp_Store.EditValueChanged += DataChanged;
            LookUpEdit_SourceID.EditValueChanged += DataChanged;
            LookUpEdit_PartID.EditValueChanged += DataChanged;
            txt_Code.EditValueChanged += DataChanged;
            #endregion
        }
        private void Lkp_PayType_EditValueChanged(object sender, EventArgs e)
        {
            DAL.DBDataContext dbc = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
       
                var drawers = CurrentSession.UserAccessibleDrawer.Select(x => new { ID = x.ACCID, Name = x.Name });
                var banks = CurrentSession.UserAccessibleBanks.Select(x => new { ID = x.AccountID, Name = x.BankName });
                var paycards = CurrentSession.UserAccessiblePayCards.Select(x => new { x.ID, Name = x.Number });
                var accounts = CurrentSession.UserAccessbileAccounts.Where(x => dbc.Acc_Accounts.Where(a => x.ParentID == x.ID).Count() == 0).Select(x => new { x.ID, Name = x.Name });

            int payType = -1;
            if (lkp_PayType.EditValue .IsNumber())
                payType = lkp_PayType.EditValue.ToInt();
            switch (payType)
                {
                    case 1:
                        lkp_PayAccount .Properties .DataSource = drawers.ToList();
                        break;
                    case 2:
                    lkp_PayAccount.Properties.DataSource = banks.ToList();
                        break;
                    case 3:
                    lkp_PayAccount.Properties.DataSource = paycards.ToList();
                        break;
                    case 4:
                    lkp_PayAccount.Properties.DataSource = accounts.ToList();
                        break;
                default:
                    lkp_PayAccount.Properties.DataSource = null;
                    return; 


            }
            lkp_PayAccount.Properties.DisplayMember = "Name";
            lkp_PayAccount.Properties.ValueMember = "ID";
           //lkp_PayAccount.Properties.PopulateColumns();
           //lkp_PayAccount.Properties.Columns[0].Visible = false;
              
          //  }
        }

        public override void RefreshData()
        {
            using (DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String))
            {
                var store = (from s in db.Inv_Stores select new { s.ID, s.Name }).ToList();
                lkp_Store.Properties.DataSource = store;
                CustomersList = (from c in CurrentSession.UserAccessbileCustomers select new { c.ID, c.Name, c.City, c.Address, c.Mobile, c.Phone }).ToList();
                VendorsList = (from v in CurrentSession.UserAccessbileVendors select new { v.ID, v.Name, v.City, v.Address, v.Mobile, v.Phone }).ToList();
                RefreshPaySources();

               
                List<Master.NameAndIDDataSource> payTypes = new List<Master.NameAndIDDataSource>()
                        {
                        new Master.NameAndIDDataSource(){ID = 1 ,Name = LangResource.CashPay  },
                        new Master.NameAndIDDataSource(){ID = 2 ,Name = LangResource.BankTransfer },
                        new Master.NameAndIDDataSource(){ID = 3 ,Name = LangResource.PayCards },
                        new Master.NameAndIDDataSource(){ID = 4 ,Name = LangResource.OnAccount },
                        };
                lkp_PayType.Properties.DataSource = payTypes;
                lkp_PayType.Properties.ValueMember = "ID";
                lkp_PayType.Properties.DisplayMember = "Name";


            }
            // AccountsRepo.View.PopulateColumns(AccountsRepo.DataSource);
            base.RefreshData();
        }
        void RefreshPaySources()
        {
            using (DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String))
            {
                var banks = CurrentSession.UserAccessibleBanks.Select(x => new { ID = x.AccountID, Name = x.BankName }).ToList();
                var drawers = CurrentSession.UserAccessibleDrawer.Select(x => new { ID = x.ACCID ?? 0, Name = x.Name }).ToList();
                var paycards = CurrentSession.UserAccessiblePayCards.Where(x => banks.Select(b => b.ID).Contains(x.BankID)).Select(x => new { x.ID, Name = x.Number }).ToList();
                var accounts = CurrentSession.UserAccessbileAccounts.Where(x => db.Acc_Accounts.Where(a => a.ParentID == x.ID).Count() == 0).Select(x => new { x.ID, Name = x.Name }).ToList();

                var dataSources = drawers;

                dataSources.AddRange(paycards);
                dataSources.AddRange(accounts);
                dataSources.AddRange(banks);
            }
        }


        private bool ValidData()
        {
            if ((Convert.ToDouble(spn_Paid .EditValue ) + Convert.ToDouble(spn_Discount .EditValue)) == 0)
            {
                spn_Paid.ErrorText = LangResource.ErrorValMustBeGreaterThan0;
                return false;
            }


            if (LookUpEdit_PartID .EditValue.ValidAsIntNonZero () == false )
            {
                LookUpEdit_PartID.ErrorText = LangResource.ErrorCantBeEmpry;
                return false;
            }


            if (lkp_Store.EditValue == null || lkp_Store.EditValue == DBNull.Value ||
                lkp_Store.EditValue.GetType() != typeof(int))
            {
                lkp_Store.ErrorText = LangResource.ErrorCantBeEmpry;
                return false;
            }
            if(LookUpEdit_Source.EditValue .ToInt ()!= (int)Master.SystemProssess.Without  && LookUpEdit_SourceID.EditValue .ValidAsIntNonZero() == false )
            {
                LookUpEdit_SourceID.ErrorText = LangResource.ErrorCantBeEmpry;
                return false;
            }

            
            return true;

        }
        public override void Save()
        {
           
            

            if (!ValidData())
            {
                return;
            }

            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);

            if (IsNew)
            {
                CashNote = new Acc_CashNote();
                db.Acc_CashNotes.InsertOnSubmit(CashNote);
                CashNote.UserID = CurrentSession.User.ID;
            }
            else
            {
                
                CashNote = db.Acc_CashNotes .Where(x => x.ID == CashNote.ID).First();
                if (CashNote == null)
                {
                    CashNote = new Acc_CashNote();
                    db.Acc_CashNotes.InsertOnSubmit(CashNote);
                    CashNote.UserID = CurrentSession.User.ID;
                }

                CashNote.LastUpdateUserID = CurrentSession.User.ID;
                CashNote.LastUpdateDate = db.GetSystemDate();
            } 
            SetData();
            db.SubmitChanges();
         
            Master.DeleteAccountAcctivity(((int)ProcessType).ToString(), CashNote.Code .ToString());

            // TODO  Insert Into Account Journal 
         
             
            
            
            var MainMessage = "";
            var DiscountAccount = 0;
            var IsPartCredit = true;
            var PartAccount = 0;



            if (ISCashIn)
            {
                IsPartCredit = true; 
                MainMessage = LangResource.CashNoteIn + " " + LangResource.Number + " " + CashNote.Code;
                DiscountAccount = CurrentSession.Company.SalesDiscountAccount;
            }
            else
            {
                IsPartCredit = false; 
                MainMessage = LangResource.CashNoteOut  + " " + LangResource.Number + " " + CashNote.Code;
                DiscountAccount = CurrentSession.Company.PurchaseDiscountAccount ;
            }
            switch (CashNote.PartType)
            {
                case (int)Master.PartTypes.Vendor:
                    PartAccount = db.Pr_Vendors.Where(x => x.ID == CashNote.PartID).SingleOrDefault().Account;
                    break;
                case (int)Master.PartTypes.Customer:
                    PartAccount = db.Sl_Customers.Where(x => x.ID == CashNote.PartID).SingleOrDefault().Account;
                    break;
                case (int)Master.PartTypes.Account:
                    PartAccount = CashNote.PartID;
                    break;
                //case (int)Master.PartTypes.Employee:
                //    PartAccount = db.HR_Employees.Where(x => x.ID == CashNote.PartID).SingleOrDefault().AccountId.Value;
                //    break;
            }

            Acc_Activity acctivity = new Acc_Activity()
            {

                Date = CashNote.Date,
                StoreID = CashNote.StoreID,
                Type = ((int)ProcessType).ToString(),
                TypeID = CashNote.Code.ToString(), 
                Note = MainMessage,
                InsertDate = CashNote.Date ,
                LastUpdateDate = CashNote.LastUpdateDate,
                LastUpdateUserID = CashNote.UserID,
                UserID = CashNote.UserID
            };
            db.Acc_Activities.InsertOnSubmit(acctivity);
            db.SubmitChanges();
            int payType = -1;
            int PayID = Convert.ToInt32(lkp_PayAccount.EditValue);
            double Amount = CashNote.Amount;
            DateTime PayDate = CashNote.Date;
         
                payType = lkp_PayType.EditValue.ToInt();
            switch (payType)
            {
                case 1:// Drawer
                case 2:// Bank
                case 4:// Account
                    db.Acc_ActivityDetials.InsertOnSubmit(new Acc_ActivityDetial()
                    {
                        ACCID = PayID,
                        Debit = IsPartCredit ? Amount : 0,
                        Credit = IsPartCredit ? 0 : Amount,
                        AcivityID = acctivity.ID,
                        DueDate = PayDate,
                        Note = string.Format("{0}-{1}", MainMessage, LangResource.Pay),
                    });
               
                    break;

                case 3://Pay Card 
                    var card = db.Acc_PayCards.Where(x => x.ID == PayID).SingleOrDefault();
                    var bank = db.Acc_Banks.Where(x => x.ID == card.BankID).SingleOrDefault();
                    db.Acc_ActivityDetials.InsertOnSubmit(new Acc_ActivityDetial()
                    {
                        ACCID = bank.AccountID,
                        Debit = IsPartCredit ? Amount - (Amount * card.Commission) : 0,
                        Credit = IsPartCredit ? 0 :Amount - (Amount * card.Commission),
                        AcivityID = acctivity.ID,
                        DueDate =PayDate,
                        Note = string.Format("{0}-{1}", MainMessage, LangResource.Pay),
                    });
                    if (card.Commission > 0) db.Acc_ActivityDetials.InsertOnSubmit(new Acc_ActivityDetial()
                    {
                        ACCID = card.CommissionAccount,
                        Debit = IsPartCredit ? (Amount * card.Commission) : 0,
                        Credit = IsPartCredit ? 0 : (Amount * card.Commission),
                        AcivityID = acctivity.ID,
                        DueDate =PayDate,
                        Note = string.Format("{0}-{2}-{1}", MainMessage, LangResource.Pay, LangResource.Commission),
                    });
               
                    break;

                default:
                    break;
            }


            // Part Account
            db.Acc_ActivityDetials.InsertOnSubmit(new Acc_ActivityDetial()
            {
                ACCID = PartAccount,
                Debit = IsPartCredit ? 0 : Amount,
                Credit = IsPartCredit ? Amount : 0,
                AcivityID = acctivity.ID,
                DueDate = PayDate,
                Note = string.Format("{0}-{1}", MainMessage, LangResource.Pay),
            });




            if (CashNote.DiscountValue > 0)
            {
                db.Acc_ActivityDetials.InsertOnSubmit(new Acc_ActivityDetial()
                {
                    ACCID = DiscountAccount,
                    Debit = IsPartCredit ? CashNote.DiscountValue : 0,
                    Credit = IsPartCredit ? 0 : CashNote.DiscountValue,
                    AcivityID = acctivity.ID,
                    Note = string.Format("{0}-{1}", MainMessage, LangResource.Discount),
                 

                });
                db.Acc_ActivityDetials.InsertOnSubmit(new Acc_ActivityDetial()
                {
                    ACCID = PartAccount,
                    Debit = IsPartCredit ? 0 : CashNote.DiscountValue,
                    Credit = IsPartCredit ? CashNote.DiscountValue : 0,
                    AcivityID = acctivity.ID,
                    Note = string.Format("{0}-{1}", MainMessage, LangResource.Discount), 
                });
            }
            ChangeSet cs = db.GetChangeSet();
            double debit = 0;
            double credit = 0;

            foreach (var item in cs.Inserts)
            {

                if (item.GetType() == typeof(Acc_ActivityDetial))
                {
                    debit += ((Acc_ActivityDetial)item).Debit;
                    credit += ((Acc_ActivityDetial)item).Credit;

                }
            }
            if (debit != credit)
            {
                XtraMessageBox.Show("Debit ={" + debit + "}   / Credit={" + credit + "} حدث خطأ ما , القيود غير متزنه ");
            }
            

            db.SubmitChanges(); 
            base.Save();
           //TODO Mark Invoices as 
            
        }
 
        int GetNextID()
        {
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);

            try
            {
                return  db.Acc_CashNotes .Where(x => x.IsCashNote  == ISCashIn).Max(n => n.Code) + 1;
            }
            catch
            {
                return (int)1;
            }
        }
        public override void New()
        {
            if (ChangesMade && !SaveChangedData()) return;
            IsNew = true;
            CashNote = new Acc_CashNote();
            CashNote.IsCashNote = ISCashIn;
            CashNote.Code   = GetNextID();
      
            CashNote.StoreID = CurrentSession.User.DefaultStore;
          
            CashNote.Date  = (new DAL.DBDataContext()).GetSystemDate();
            CashNote.StoreID = Convert.ToInt32(CurrentSession.User.DefaultRawStore);

            CashNote.PartType = ISCashIn ? (byte)Master.PartTypes.Customer : (byte)Master.PartTypes.Vendor;

            CashNote.LinkType  = (int)Master.SystemProssess.Without;

            GetData();
            
            base.New();
            IsNew = true;
            ChangesMade = false;
        }

        public override void Delete()
        {
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
           

            if (IsNew) return;
            PartNumber = txt_Code.Text.ToString();
            if (Master.AskForDelete(this, IsNew, PartName, PartNumber))
            {
                Delete(new List<int>() { CashNote.Code },ISCashIn, this.Name, (byte)ProcessType);
                New();
            }

        }

        void SetData()
        {
            CashNote.Code   = Convert.ToInt32(txt_Code.Text);
            CashNote.IsCashNote = ISCashIn; 
            CashNote.StoreID = Convert.ToInt32(lkp_Store.EditValue); 
            CashNote.Date   = dt_Date.DateTime;
            CashNote.PartType =(byte)LookUpEdit_PartType.EditValue .ToInt() ;
            CashNote.PartID = LookUpEdit_PartID.EditValue.ToInt();
            CashNote.LinkType = (byte)LookUpEdit_Source.EditValue.ToInt();
            CashNote.LinkID = (int?)LookUpEdit_SourceID.EditValue;
            CashNote.Notes = memoEdit1.Text;
            CashNote.Amount  = Convert.ToDouble(spn_Paid.EditValue  );
            CashNote.DiscountValue   = Convert.ToDouble(spn_Discount.EditValue  );
            PartNumber = CashNote.Code  .ToString();
            CashNote.PayType  = Convert.ToByte(lkp_PayType.EditValue);
            CashNote.PayAccount   = (int)lkp_PayAccount.EditValue;
        }

        void GetData()
        {
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
			 
           
        

            txt_Code.Text = CashNote.Code  .ToString();
            lkp_Store.EditValue = CashNote.StoreID; 
            LookUpEdit_PartType.EditValue= (int)  CashNote.PartType;
            LookUpEdit_PartID.EditValue=   CashNote.PartID;
            LookUpEdit_Source.EditValue= (int)CashNote.LinkType;
            LookUpEdit_SourceID.EditValue=CashNote.LinkID;
            dt_Date.DateTime = CashNote.Date ;
            memoEdit1.Text = CashNote.Notes;
            spn_Paid.EditValue = CashNote.Amount;
            spn_Discount.EditValue = CashNote.DiscountValue;


            PartNumber = CashNote.Code  .ToString();
            //txt_InsertUser.Text = db.St_Users.Where(x => x.ID == CashNote.UserID).Select(x => x.UserName).FirstOrDefault();
            //txt_UpdateUser.Text = db.St_Users.Where(x => x.ID == CashNote.LastUpdateUserID).Select(x => x.UserName)
             //   .FirstOrDefault();
            txt_LastUpdate.Text = (CashNote.LastUpdateDate != null)
                ? ((DateTime)CashNote.LastUpdateDate).ToString("yyyy-MM-dd dddd hh:mm tt")    : "";

          lkp_PayType.EditValue =  CashNote.PayType ;
          lkp_PayAccount.EditValue =  CashNote.PayAccount   ;

            ChangesMade = false;

        }
        public override void Print()
        {

            Print(new List<int>() { CashNote.Code }, this.Name, ISCashIn );

        }
        public static void Delete(List<int> Codes,Boolean IsCashIn , string CallerForm, byte ProccessID)

        {
            DAL.DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
            foreach (var item in Codes)
            {
               // Master.InsertUserLog(2, CallerForm, "", item.ToString());
                Master.DeleteAccountAcctivity(ProccessID.ToString(), item.ToString());
            }
            db.SubmitChanges();
            db.Acc_CashNotes .DeleteAllOnSubmit(db.Acc_CashNotes .Where(c => Codes.Contains(c.Code) && c.IsCashNote == IsCashIn ));
            db.SubmitChanges();

            Master.RefreshAllWindows(); 
        }
        public static object  PrintDataSource(List<int> ids, Master.SystemProssess proccessType)
        {
          
            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);


            //HeaderDT.Columns.Add("Code", typeof(string));
            //HeaderDT.Columns.Add("Store");
            //HeaderDT.Columns.Add("Drawer"); 
            //HeaderDT.Columns.Add("Date", typeof(DateTime));
            //HeaderDT.Columns.Add("Notes", typeof(string));
            //HeaderDT.Columns.Add("Amount");
            //HeaderDT.Columns.Add("Discount");
            //HeaderDT.Columns.Add("Total");
            //HeaderDT.Columns.Add("TotalInText");
            //HeaderDT.Columns.Add("PartType");
            //HeaderDT.Columns.Add("PartName");
            //HeaderDT.Columns.Add("Source");
            //HeaderDT.Columns.Add("SourceCode");
            //HeaderDT.Columns.Add("User");






            //var parts = db.Pr_Vendors.Select(x => new { PartID = x.ID, PartType = ((int)Master.PartTypes.Vendor), x.Name });
            //parts.Concat(db.Sl_Customers.Select(x => new { PartID = x.ID, PartType = ((int)Master.PartTypes.Customer), x.Name }));
            //parts.Concat(db.HR_Employees.Select(x => new { PartID = x.ID, PartType = ((int)Master.PartTypes.Employee), x.Name }));
            //parts.Concat(db.Acc_Accounts.Select(x => new { PartID = x.ID, PartType = ((int)Master.PartTypes.Account), x.Name }));

            var Invoices = db.Inv_Invoices.Select(i => i);
            
            var quiry =( from e in db.Acc_CashNotes 
                        join b in db.Inv_Stores on e.StoreID equals b.ID
                     //   from d in db.Acc_Drawers.Where(x => x.ID == e.DrawerACCID )
                      //  from u in db.St_Users.Where(x => x.ID == e.UserID).DefaultIfEmpty()
                        from p in(
                        db.Pr_Vendors.Select(x => new { PartID = x.ID, PartType = ((int)Master.PartTypes.Vendor), x.Name })
                                       .Concat(db.Sl_Customers.Select(x => new { PartID = x.ID, PartType = ((int)Master.PartTypes.Customer), x.Name }))
                                     //  .Concat(db.HR_Employees.Select(x => new { PartID = x.ID, PartType = ((int)Master.PartTypes.Employee), x.Name }))
                                       .Concat(db.Acc_Accounts.Select(x => new { PartID = x.ID, PartType = ((int)Master.PartTypes.Account), x.Name }))
                        
                        ).Where(x => x.PartID  == e.PartID  && x.PartType == e.PartType ).DefaultIfEmpty()
                        from i in Invoices .Where (x =>x.InvoiceType == e.LinkType && x.ID == e.LinkID  ).DefaultIfEmpty ()
                        where ids.Contains(e.Code )
                        select new
                        {
                            e.ID,
                            e.Code, 
                            Barcode = string.Format("#*{0}*{1}", ((int)proccessType).ToString() , e.Code ),
                            p.PartType  ,
                            PartTypeText = "",
                            p.Name ,
                            Store = b.Name,
                            //Drawer = d.Name, 
                            e.Date ,
                            e.Notes,
                            e.Amount ,
                            NetText = "", 
                            e.DiscountValue ,
                            Total = e.Amount+e.DiscountValue,
                            e.LinkType ,
                            SourceName = "" ,
                            SourceCode = i.ID.ToString() + "-"+  i.Code.ToString()  ,
                           // u.UserName,
                            //Pays = db.Acc_Pays .Where(x=>x.SourceType == ((int)proccessType) && x.SourceID == e.Code  ).Select (
                            // x=>   new {
                            //     x.Amount ,
                            //     x.Refrence ,
                            //     x.PayType,PayTypeText ="",
                            //     x.PayID,PayIDText="" , 
                            //     x.PayDate ,
                            //     Currancy = db.Acc_Currencies.Single(c=>c.ID == x.CurrancyID ).Name  , 
                            //     x.CurrancyRate ,
                            //     x.Notes     }).ToList()
                        }).ToList();

            //foreach (var h in quiry.ToList())
            //    HeaderDT.Rows.Add(h.Code , h.Store , h.Date , h.Notes
            //        , h.Amount , h.DiscountValue ,h.Total , Master.ConvertMoneyToText(h.Total.ToString() ,1)
            //       , h.PartType ,h.Name ,h.SourceName ,h.SourceCode  , h.UserName); 
            //ds.Tables.Add(HeaderDT); 
            List<Master.NameAndIDDataSource> payTypes = new List<Master.NameAndIDDataSource>()
                        {
                        new Master.NameAndIDDataSource(){ID = 1 ,Name = LangResource.CashPay  },
                        new Master.NameAndIDDataSource(){ID = 2 ,Name = LangResource.BankTransfer },
                        new Master.NameAndIDDataSource(){ID = 3 ,Name = LangResource.PayCards },
                        new Master.NameAndIDDataSource(){ID = 4 ,Name = LangResource.OnAccount },
                        };
            var banks = CurrentSession.UserAccessibleBanks.Select(x => new { ID = x.AccountID, Name = x.BankName ,Type =(int) Master.PayTypes.Bank  }).ToList();
            var drawers = CurrentSession.UserAccessibleDrawer.Select(x => new { ID = x.ACCID ?? 0, Name = x.Name, Type = (int)Master.PayTypes.Drawer }).ToList();
            var paycards = CurrentSession.UserAccessiblePayCards.Where(x => banks.Select(b => b.ID).Contains(x.BankID)).Select(x => new { x.ID, Name = x.Number, Type = (int)Master.PayTypes.PayCard }).ToList();
            var accounts = CurrentSession.UserAccessbileAccounts.Where(x => db.Acc_Accounts.Where(a => a.ParentID == x.ID).Count() == 0).Select(x => new { x.ID, Name = x.Name, Type = (int)Master.PayTypes.Account }).ToList();

            var PaySourcesName = drawers;

            PaySourcesName.AddRange(paycards);
            PaySourcesName.AddRange(accounts);
            PaySourcesName.AddRange(banks);

             var parts =   new[]
                   {
                      new { ID = 1 ,Name =  LangResource.Vendor  },
                      new { ID = 2 ,Name =  LangResource.Customer  },
                      new { ID = 4 ,Name =  LangResource.Account  },
                    };

            quiry.ForEach(x =>
            {
                x.Set(i => i.NetText, Master.ConvertMoneyToText(x.Amount.ToString()));
                x.Set(i => i.PartTypeText , parts.Single  (p=>p.ID == x.PartType).Name );
                if (x.LinkType > 0 && x.LinkType < Master.Prossess.Count()) 
                x.Set(i => i.SourceName , Master.Prossess[x.LinkType]);
               

            });
            return quiry;
        }
        public static void Print(List<int> ids, string CallerForm, bool IsCashIn)
        {

            foreach (var item in ids)
            {
            //    Master.InsertUserLog(3, CallerForm, "", item.ToString());
            }
           // Reporting.rpt_CashNote .Print(PrintDataSource(ids,IsCashIn? SystemProssess.CashNoteIn : SystemProssess.CashNoteOut ), IsCashIn);
            //base.Print();
        }

     
        void LoadItemByCode(int code ,bool isCashIn )
        {
            DAL.DBDataContext dbc = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
            CashNote = dbc.Acc_CashNotes.Where(x => x.Code  == code && x.IsCashNote == isCashIn).Single();// (from i in dbc.Acc_CashNotes where i.ID   == ID  select i).FirstOrDefault();
            ISCashIn = isCashIn;
            if (CashNote == null)
                New();
            this.Name = (ISCashIn) ? "frm_CashNoteIn" : "frm_CashNoteOut";
            IsNew = false;
        }
        void LoadItemByID(int id)
        {
            DAL.DBDataContext dbc = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
            CashNote = dbc.Acc_CashNotes.Where(x => x.ID == id ).SingleOrDefault();// (from i in dbc.Acc_CashNotes where i.ID   == ID  select i).FirstOrDefault();
            if(CashNote == null)
                CashNote = dbc.Acc_CashNotes.Where(x => x.Code  == id && x.IsCashNote == ISCashIn ).SingleOrDefault();// (from i in dbc.Acc_CashNotes where i.ID   == ID  select i).FirstOrDefault();
            if(CashNote ==null)
            {
                XtraMessageBox.Show(LangResource.DocumentNotFound);
                New();
                return;
            }
            this.Name = (ISCashIn) ? "frm_CashNoteIn" : "frm_CashNoteOut";
            IsNew = false;
        }

        private void LookUpEdit_PartType_EditValueChanged(object sender, EventArgs e)
        {
      
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);

            var sources = new List<Master.NameAndIDDataSource>();
            sources.Add(new Master.NameAndIDDataSource() { ID = (int)Master.SystemProssess.Without, Name = Master.Prossess[(int)Master.SystemProssess.Without] }); 
            switch (LookUpEdit_PartType.EditValue.ToInt())
            {
                case 0:
                    LookUpEdit_PartID.Properties.DataSource = null;
                    LookUpEdit_PartID.EditValue = 0;
                    break;
                case (int)Master.PartTypes.Vendor :

                    LookUpEdit_PartID.Properties.DataSource = CurrentSession.UserAccessbileVendors.Select(c => new { c.ID, c.Name, c.City, c.Address, c.Mobile, c.Phone }).ToList();
                    LookUpEdit_PartID.EditValue = null;
                    glkp_Fromitem.Text = LangResource.Vendor;

                    if (ISCashIn)
                        sources.Add(new Master.NameAndIDDataSource() { ID = (int)Master.SystemProssess.PurchaseReturn , Name = Master.Prossess[(int)Master.SystemProssess.PurchaseReturn] }); 
                    else
                        sources.Add(new Master.NameAndIDDataSource() { ID = (int)Master.SystemProssess.PurchaseInvoice , Name = Master.Prossess[(int)Master.SystemProssess.PurchaseInvoice] });  
                    break;

                case (int)Master.PartTypes.Customer :
                    LookUpEdit_PartID.Properties.DataSource = CurrentSession.UserAccessbileCustomers.Select(c => new { c.ID, c.Name, c.City, c.Address, c.Mobile, c.Phone }).ToList();
                    LookUpEdit_PartID.EditValue =null;
                    glkp_Fromitem.Text = LangResource.Customer;
                     
                    if (ISCashIn)
                        sources.Add(new Master.NameAndIDDataSource() { ID = (int)Master.SystemProssess.SalesInvoice , Name = Master.Prossess[(int)Master.SystemProssess.SalesInvoice ] });
                    else
                        sources.Add(new Master.NameAndIDDataSource() { ID = (int)Master.SystemProssess.SalesReturn , Name = Master.Prossess[(int)Master.SystemProssess.SalesReturn ] });
                    break;
                case (int)Master.PartTypes.Employee :
                    //LookUpEdit_PartID.Properties.DataSource = db.HR_Employees.Select(c => new { c.ID, c.Name }).ToList();
                    //LookUpEdit_PartID.EditValue = 0;
                    //glkp_Fromitem.Text = LangResource.Employee;

                    break;
                case (int)Master.PartTypes.Account:
                    LookUpEdit_PartID.Properties.DataSource = db.Acc_Accounts.Where(x => CurrentSession.UserAccessbileAccounts.Select(a => a.ID).Contains(x.ID) &&
                    db.Acc_Accounts.Where(a => a.ParentID == x.ID).Count() == 0).Select(c => new { c.ID, c.Name }).ToList();
                    LookUpEdit_PartID.EditValue = 0;
                    glkp_Fromitem.Text = LangResource.Account;
                    break;

                default:
                    break;
            }


           


            LookUpEdit_Source.Properties.DataSource = sources;
            LookUpEdit_Source.Properties.ValueMember = "ID";
            LookUpEdit_Source.Properties.DisplayMember = "Name";
            LookUpEdit_Source.Properties.ShowHeader = false;
            //LookUpEdit_Source.Properties.Columns[0].Visible = false; 
            
            LookUpEdit_PartID.Properties.PopulateViewColumns();
          //  Master.TranslateGridColumn(LookUpEdit_PartID.Properties.View);
           

        }
        private void cb_Source_SelectedIndexChanged(object sender, EventArgs e)
        {
 


        }

        private void spn_Paid_EditValueChanged(object sender, EventArgs e)
        {
              }

        private void glkp_From_EditValueChanged(object sender, EventArgs e)
        {
            cb_Sourceitem.Enabled = glkp_Sourceitem.Enabled =
               (LookUpEdit_PartID.EditValue != null && LookUpEdit_PartID.EditValue != DBNull.Value);
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);

            if (LookUpEdit_PartID.EditValue == null) return;
            switch (LookUpEdit_PartType.EditValue.ToInt())
            {
                case 0:
                    break;

                case (int)Master.PartTypes.Vendor:
                    AccountID = CurrentSession.UserAccessbileVendors
                        .Where(x => x.ID.ToString() == LookUpEdit_PartID.EditValue.ToString()).Select(x => x.Account)
                        .FirstOrDefault(); break;
                case (int)Master.PartTypes.Customer :
                    AccountID = CurrentSession.UserAccessbileCustomers
                        .Where(x => x.ID.ToString() == LookUpEdit_PartID.EditValue.ToString()).Select(x => x.Account)
                        .FirstOrDefault(); break;
                case (int)Master.PartTypes.Employee :
                    //AccountID = db.HR_Employees 
                    //    .Where(x => x.ID  ==Convert.ToInt32( LookUpEdit_PartID.EditValue)).Select(x => x.AccountId )
                    //    .FirstOrDefault()??0;
                    break;
                case (int)Master.PartTypes.Account:
                    AccountID = LookUpEdit_PartID.EditValue.ToInt();
                    break;
                default:
                    throw new NotImplementedException();
            }
            Master.AccountBalance CustomerBalance = Master.GetAccountBalance(AccountID);
            txt_BalanceBefore.Text = Math.Abs(CustomerBalance.Balance).ToString();
            txt_BBType.Text = (CustomerBalance.Balance >= 0) ? LangResource.Debit : LangResource.Credit;


        }

        private void glkp_Source_EditValueChanged(object sender, EventArgs e)
        {
            if (LookUpEdit_SourceID.EditValue == null || LookUpEdit_SourceID.EditValue == DBNull.Value || IsNew == false ) return;
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);

            var Invoice = (from i in db.Inv_Invoices
                           where i.ID == Convert.ToInt32(LookUpEdit_SourceID.EditValue)
                           select new
                           {
                               i.PartType ,
                               i.PartID , 
                               i.Net,
                               Paid = (double?)db.Acc_CashNotes .Where(x => x.IsCashNote  == ISCashIn  && x.LinkType ==Convert.ToInt32( LookUpEdit_Source.EditValue)
                               && x.LinkID == i.ID).Sum(x =>(Double?) x.Amount) ?? 0,
                           }

            ).FirstOrDefault();
            if (Invoice != null)
            {
                txt_PaiedFromInvoice.Text = Invoice.Paid.ToString();
                txt_InvoiceNet.Text = Invoice.Net.ToString(); 
                txt_Remains.Text = (Invoice.Net - Invoice.Paid ).ToString();
                //if (Convert.ToDouble(spn_Paid.EditValue) == 0)
                //{
                //    var firstPay = ((Collection<Acc_Pay>)GridView_Pays.DataSource).FirstOrDefault();
                //    if (firstPay != null)
                //        firstPay.Amount = Convert.ToDouble((Invoice.Net - Invoice.Paid)); 
                //    GridView_Pays_RowCountChanged(null, null); 
                //}
                groupControl3item.Expanded = true;
                LookUpEdit_PartID.EditValue  = Invoice.PartID;
                LookUpEdit_PartType.EditValue =(int) Invoice.PartType;


            }
            else
            {
                txt_PaiedFromInvoice.Text =
                txt_InvoiceNet.Text =
                txt_Remains.Text = "";
                groupControl3item.Expanded = false; 
            }


        }

        private void cb_Source_EditValueChanged(object sender, EventArgs e)
        {

            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
            var Invoices = db.Inv_Invoices.Select(x=>x);

            if(LookUpEdit_PartType.EditValue .ValidAsIntNonZero())
                Invoices = Invoices.Where(x => x.PartType == Convert.ToInt32(LookUpEdit_PartType.EditValue));
            if (LookUpEdit_Source.EditValue.ValidAsIntNonZero())
                Invoices = Invoices.Where(x => x.InvoiceType  == Convert.ToInt32(LookUpEdit_Source.EditValue));
            else
            {
                LookUpEdit_SourceID.Properties.DataSource = null;
                LookUpEdit_SourceID.EditValue = null;
                return; 
            }

          //  Invoices = Invoices.Where(i => (i.Net - ((double?)db.Acc_Pays.Where(x => x.SourceType == 6 && x.SourceID == i.ID).Sum(x => x.Amount) ?? 0)) > 0 || i.ID == CashNote.LinkID ).Select(i => i);
           
            var parts = db.Pr_Vendors.Select(x => new { PartID = x.ID, PartType = ((int)Master.PartTypes.Vendor), x.Name });
            parts.Concat(db.Sl_Customers .Select(x => new { PartID = x.ID, PartType = ((int)Master.PartTypes.Customer ), x.Name }));
            //parts.Concat(db.HR_Employees.Select(x => new { PartID = x.ID, PartType = ((int)Master.PartTypes.Employee), x.Name }));
            parts.Concat(db.Acc_Accounts.Select(x => new { PartID = x.ID, PartType = ((int)Master.PartTypes.Account), x.Name }));
            LookUpEdit_SourceID.Properties.DataSource = Invoices.Select(i => new
            {
                i.ID ,
                i.Code ,
                Part = parts.Where(x=>x.PartID == i.PartID && x.PartType == i.PartType  ).Single().Name ,
                i.Net,
                i.Date,
                //Paid = (double?)db.Acc_Pays.Where(x => x.SourceType == 6 && x.SourceID == i.ID).Sum(x => x.Amount) ?? 0, 
                //Remains = i.Net - ((double?)db.Acc_Pays.Where(x => x.SourceType == 6 && x.SourceID == i.ID).Sum(x => x.Amount) ?? 0), 
            });
            LookUpEdit_SourceID.Properties.PopulateViewColumns();
           // Master.TranslateGridColumn(LookUpEdit_SourceID.Properties.View);
        }

        private void glkp_Source_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.GetType() != typeof(EditorButton) || e.Button.Tag == null) return;
            if (LookUpEdit_SourceID.EditValue == null || LookUpEdit_SourceID.EditValue == DBNull.Value && Convert.ToInt32(LookUpEdit_SourceID.EditValue) == 0) return;
            if (e.Button.Tag.ToString() == "Open" && LookUpEdit_SourceID.EditValue != null )
            {
                //if (cb_Source.Text == LangResource.PurchaseInvoice)
                //    frm_Main.OpenForm(new frm_Pr_PrInvoice (Convert.ToInt32(glkp_Source.EditValue)), openNew: true, CloseIfOpen: false);
                //else if (cb_Source.Text == LangResource.purchasesReturn)
                //    frm_Main.OpenForm(new frm_Pr_PrReturnInvoice (Convert.ToInt32(glkp_Source.EditValue)), openNew: true, CloseIfOpen: false);
              //  else
                //if (LookUpEdit_Source.Text == LangResource.SalesInvoice)
                //    frm_Main.OpenForm(new frm_Inv_Invoice (Convert.ToInt32(LookUpEdit_SourceID.EditValue)), openNew: true, CloseIfOpen: false);
                //else if (LookUpEdit_Source.Text == LangResource.SalesReturnInvoice)
                //    frm_Main.OpenForm(new frm_Sl_SlReturnInvoice (Convert.ToInt32(LookUpEdit_SourceID.EditValue)), openNew: true, CloseIfOpen: false);

            } 
        }
        //private void GridView_Pays_ValidateRow(object sender, ValidateRowEventArgs e)
        //{
        //    var row = e.Row as Acc_Pay;
        //    var view = sender as GridView;
        //    if (row == null)
        //        return;
        //    if (row.Amount <= 0)
        //    {
        //        view.SetColumnError(view.Columns[nameof(row.Amount)], LangResource.ErrorValMustBeGreaterThan0);

        //        e.Valid = false;
        //    }
        //    if (row.PayType <= 0)
        //    {
        //        view.SetColumnError(view.Columns[nameof(row.PayType)], "");

        //        e.Valid = false;
        //    }
        //    if (row.PayID <= 0)
        //    {
        //        view.SetColumnError(view.Columns[nameof(row.PayID)], "");

        //        e.Valid = false;
        //    }
        //    if (row.PayDate.Year <= 1950)
        //    {

        //        view.SetColumnError(view.Columns[nameof(row.PayDate)], "");

        //        e.Valid = false;
        //    }
        //    if ((row.PayType == 2 || row.PayType == 3) && (row.Refrence == null || row.Refrence.Trim() == ""))
        //    {
        //        view.SetColumnError(view.Columns[nameof(row.Refrence)], LangResource.MustEnterTheBankTransferNumber);
        //        e.Valid = false;
        //    }
        //    if (row.PayType == 5 && (row.Refrence == null || row.Refrence.Trim() == ""))
        //    {

        //        view.SetColumnError(view.Columns[nameof(row.Refrence)], LangResource.MustEnterCashNoteNumber);
        //        e.Valid = false;
        //    }
        //    if (row.CurrancyRate <= 0)
        //    {
        //        view.SetColumnError(view.Columns[nameof(row.CurrancyRate)], LangResource.ErrorValMustBeGreaterThan0);
        //        e.Valid = false;
        //    }
        //}

        //private void GridView_Pays_RowCountChanged(object sender, EventArgs e)
        //{
        //    var sum = (GridView_Pays.DataSource as Collection<Acc_Pay>).Sum(x => x.Amount);
        //    spn_Paid.EditValue = sum;
        //}

        //private void GridView_Pays_CellValueChanged(object sender, CellValueChangedEventArgs e)
        //{
        //    var row = GridView_Pays.GetRow(e.RowHandle) as Acc_Pay;
        //    DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);

        //    if (e.Column.FieldName == nameof(row.CurrancyID))
        //    {
        //        var currency = db.Acc_Currencies.Where(x => x.ID == row.CurrancyID).Single();
        //        row.CurrancyRate = currency.LastRate;
        //    }
             

        //    row.UserID = CurrentSession.user.ID;
        //    row.InsertDate = db.GetSystemDate();
        //    GridView_Pays_RowCountChanged(sender, null);


        //}

        //private void GridView_Pays_InitNewRow(object sender, InitNewRowEventArgs e)
        //{
        //    DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
        //    var row = GridView_Pays.GetRow(e.RowHandle) as Acc_Pay;
        //    if (row.PayType == 0)
        //        row.PayType = 1;
        //    row.UserID = CurrentSession.user.ID;
        //    row.InsertDate = db.GetSystemDate();
        //    row.PayDate = CashNote .Date;
        //    row.CurrancyID = 1;
        //    row.CurrancyRate = 1;
        //    row.SourceType = (int)Master.SystemProssess.SalesInvoice;
        //    row.SourceID = CashNote.Code ;
        //    row.Refrence = "";
        //}

        //private void GridView_Pays_CustomRowCellEditForEditing(object sender, CustomRowCellEditEventArgs e)
        //{
        //    DAL.DBDataContext dbc = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
        //    GridView Senderview = sender as GridView;

        //    if (e.Column.FieldName == "PayID")
        //    {
        //        RepositoryItemLookUpEdit PayIDRepoEXP = new RepositoryItemLookUpEdit();
        //        PayIDRepoEXP.NullText = "";
        //        if (Senderview.GetRowCellValue(e.RowHandle, "PayType").ValidAsIntNonZero() == false)
        //        { e.RepositoryItem = new RepositoryItem(); return; }

        //        var drawers = CurrentSession.UserAccessibleDrawer.Where(x => x.LinkedToBranch == 0 || x.LinkedToBranch == lkp_Store.EditValue.ToInt()).Select(x => new { ID = x.ACCID, Name = x.Name });
        //        var banks = CurrentSession.UserAccessibleBanks.Where(x => x.LinkedToBranch == 0 || x.LinkedToBranch == lkp_Store.EditValue.ToInt()).Select(x => new { ID = x.AccountID, Name = x.BankName });
        //        var paycards = CurrentSession.UserAccessiblePayCards.Where(x => banks.Select(b => b.ID).Contains(x.BankID)).Select(x => new { x.ID, Name = x.Number });
        //        var accounts = CurrentSession.UserAccessbileAccounts.Where(x => dbc.Acc_Accounts.Where(a => x.ParentID == x.ID).Count() == 0).Select(x => new { x.ID, Name = x.Name });
        //        switch (Senderview.GetRowCellValue(e.RowHandle, "PayType").ToInt())
        //        {
        //            case 1:
        //                PayIDRepoEXP.DataSource = drawers.ToList();
        //                break;
        //            case 2:
        //                PayIDRepoEXP.DataSource = banks.ToList();
        //                break;
        //            case 3:
        //                PayIDRepoEXP.DataSource = paycards.ToList();
        //                break;
        //            case 4:
        //                PayIDRepoEXP.DataSource = accounts.ToList();
        //                break;
                  
        //        }
        //        PayIDRepoEXP.DisplayMember = "Name";
        //        PayIDRepoEXP.ValueMember = "ID";
        //        PayIDRepoEXP.PopulateColumns();
        //        PayIDRepoEXP.Columns[0].Visible = false;
        //        e.RepositoryItem = PayIDRepoEXP;
        //    }


        //}

        private void ButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            GridView view = ((GridControl)((ButtonEdit)sender).Parent).MainView as GridView;

            if (view.FocusedRowHandle >= 0)
            {
                view.DeleteSelectedRows();

            }

        }


        //private void GridView_Pays_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        //{
        //    GridView_Pays_FocusedColumnChanged(sender, new FocusedColumnChangedEventArgs(GridView_Pays.FocusedColumn, GridView_Pays.FocusedColumn));
        //}

        //private void GridView_Pays_FocusedColumnChanged(object sender, FocusedColumnChangedEventArgs e)
        //{
        //    var row = GridView_Pays.GetFocusedRow() as Acc_Pay;
        //    if (row == null) return;
           
        //    if (e.FocusedColumn.FieldName == nameof(row.CurrancyRate))
        //    {
        //        e.FocusedColumn.OptionsColumn.ReadOnly = row.CurrancyID == 1;
        //    }
        //    if (e.FocusedColumn.FieldName == nameof(row.Amount))
        //    {
        //        e.FocusedColumn.OptionsColumn.ReadOnly = row.PayType == 5;
        //    }

        //}
        //private void gridView2_InvalidRowException(object sender, InvalidRowExceptionEventArgs e)
        //{
        //    e.ExceptionMode = ExceptionMode.NoAction;
        //}

    }
}
