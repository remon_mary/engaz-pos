﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraEditors.Repository;
using ByStro.Clases;
using DevExpress.DataProcessing.InMemoryDataProcessor.GraphGenerator;

namespace ByStro.Forms
{
    public partial class frm_CustomerList : frm_Master
    {
        public frm_CustomerList()
        {
            InitializeComponent();
        }
        bool SelectCustomer;
        public int CustomerSelectedId;
        public frm_CustomerList(bool SelectCustomer)
        {
            InitializeComponent();
            this.SelectCustomer= SelectCustomer;
        }
        public override void frm_Load(object sender, EventArgs e)
        {
            base.frm_Load(sender, e);
            Master.RestoreGridLayout(CustomerListGridView, this);
            btn_Save.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            btn_Delete.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            btn_List.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            CustomerListGridView.Columns["ID"].Caption = LangResource.ID;
            CustomerListGridView.Columns["ID"].Visible = false;
            CustomerListGridView.Columns["ID"].OptionsColumn.ShowInCustomizationForm = false;
            CustomerListGridView.Columns["Name"].Caption = LangResource.Name;
            CustomerListGridView.Columns["Phone"].Caption = LangResource.Phone;
            CustomerListGridView.Columns["City"].Caption = LangResource.City;
            CustomerListGridView.Columns["Address"].Caption = LangResource.Address;
            CustomerListGridView.Columns["GroupID"].Caption = LangResource.Group;
            CustomerListGridView.Columns["GroupID"].ColumnEdit = customersrepo;
            gridControl1.RepositoryItems.AddRange(new RepositoryItem[] { customersrepo });
        }
        RepositoryItemLookUpEdit customersrepo = new RepositoryItemLookUpEdit { ValueMember = "Number", DisplayMember = "Name", NullText = "" };
        public override void RefreshData()
        {
            gridControl1.DataSource = (from s in CurrentSession.UserAccessbileCustomers select new { s.ID, s.Name, s.GroupID, s.Phone, s.City, s.Address, s.Mobile, s.MaxCredit }).ToList();

            DAL.DBDataContext objDataContext = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
            var customersgroup = from u in objDataContext.Sl_CustomerGroups select u;
            customersrepo.DataSource = customersgroup;

        }

        public override void New()
        {
            PL.Main_frm.OpenForm(new frm_Customer(), true);
        }
        public override void Print()
        {
            //if (CanPerformPrint() == false) return;
            CustomerListGridView.OptionsPrint.PrintFilterInfo = true;
            //Reporting.rpt_GridReport.Print(gridControl1, this.Text, CustomerListGridView.FilterPanelText, (this.RightToLeft == System.Windows.Forms.RightToLeft.Yes) ? true : false);
            base.Print();
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            if (CustomerListGridView.FocusedRowHandle >= 0)
            {
                CustomerSelectedId = Convert.ToInt32(CustomerListGridView.GetFocusedRowCellValue("ID"));
                if (SelectCustomer)
                {
                    Close();
                }
                else
                PL.Main_frm.OpenForm(new frm_Customer(CustomerSelectedId), true);
            }
        }

        private void gridView1_ColumnFilterChanged(object sender, EventArgs e)
        {
            GridView gridView = sender as GridView;
            //list = new List<int>();
            //for (int i = 0; i < gridView.RowCount; i++)
            //{
            //    list.Add(Convert.ToInt32(gridView.GetRowCellValue(i, "ID")));

            //}

        }

        private void frm_CustomerList_FormClosing(object sender, FormClosingEventArgs e)
        {
            Master.SaveGridLayout(CustomerListGridView, this);
        }

        private void accordionControlElement13_Click(object sender, EventArgs e)
        {
            //TODO OpenForm  Invoice
            //if (CustomerListGridView.FocusedRowHandle >= 0)
            //    frm_Main.OpenForm(new frm_Inv_Invoice(Convert.ToInt32(CustomerListGridView.GetFocusedRowCellValue("ID")), Master.PartTypes.Customer , Master.InvoiceType.SalesInvoice,list));
        }

        private void accordionControlElement6_Click(object sender, EventArgs e)
        {      
            //TODO OpenForm  Invoice List
            //if (CustomerListGridView.FocusedRowHandle >= 0)
            //    frm_Main.OpenForm(new frm_Inv_InvoiceList (Master.InvoiceType.SalesInvoice,Master.PartTypes.Customer  ,Convert.ToInt32(CustomerListGridView.GetFocusedRowCellValue("ID"))));
        }

        private void CustomerListGridView_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if(e.FocusedRowHandle >= 0)
            {
                int x = 0;
                int.TryParse(CustomerListGridView.GetFocusedRowCellValue("ID").ToString(), out x );
                uc_CustomerSideMenu1.CustomerID = x;
            }
        }
    }
}
