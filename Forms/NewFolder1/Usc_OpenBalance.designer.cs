﻿namespace ByStro.GUI
{
    partial class Usc_OpenBalance
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Usc_OpenBalance));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.lkp_Currancy = new DevExpress.XtraEditors.LookUpEdit();
            this.spn_Rate = new DevExpress.XtraEditors.SpinEdit();
            this.date_Balance = new DevExpress.XtraEditors.DateEdit();
            this.cb_Balance = new DevExpress.XtraEditors.ComboBoxEdit();
            this.spn_Balance = new DevExpress.XtraEditors.SpinEdit();
            this.lookUpEdit_OpenBalanceAccount = new DevExpress.XtraEditors.LookUpEdit();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.gb_OpenBalance = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.spn_Ratexxx = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lkp_Currancy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_Rate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date_Balance.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date_Balance.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Balance.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_Balance.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_OpenBalanceAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gb_OpenBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_Ratexxx)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            resources.ApplyResources(this.layoutControl1, "layoutControl1");
            this.layoutControl1.Controls.Add(this.lkp_Currancy);
            this.layoutControl1.Controls.Add(this.spn_Rate);
            this.layoutControl1.Controls.Add(this.date_Balance);
            this.layoutControl1.Controls.Add(this.cb_Balance);
            this.layoutControl1.Controls.Add(this.spn_Balance);
            this.layoutControl1.Controls.Add(this.lookUpEdit_OpenBalanceAccount);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsView.RightToLeftMirroringApplied = true;
            this.layoutControl1.Root = this.Root;
            // 
            // lkp_Currancy
            // 
            resources.ApplyResources(this.lkp_Currancy, "lkp_Currancy");
            this.lkp_Currancy.Name = "lkp_Currancy";
            this.lkp_Currancy.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("lkp_Currancy.Properties.Buttons"))))});
            this.lkp_Currancy.Properties.NullText = resources.GetString("lkp_Currancy.Properties.NullText");
            this.lkp_Currancy.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.lkp_Currancy.StyleController = this.layoutControl1;
            // 
            // spn_Rate
            // 
            resources.ApplyResources(this.spn_Rate, "spn_Rate");
            this.spn_Rate.Name = "spn_Rate";
            this.spn_Rate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("spn_Rate.Properties.Buttons"))))});
            this.spn_Rate.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.spn_Rate.StyleController = this.layoutControl1;
            // 
            // date_Balance
            // 
            resources.ApplyResources(this.date_Balance, "date_Balance");
            this.date_Balance.Name = "date_Balance";
            this.date_Balance.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("date_Balance.Properties.Buttons"))))});
            this.date_Balance.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("date_Balance.Properties.CalendarTimeProperties.Buttons"))))});
            this.date_Balance.Properties.CalendarTimeProperties.Mask.EditMask = resources.GetString("date_Balance.Properties.CalendarTimeProperties.Mask.EditMask");
            this.date_Balance.Properties.CalendarTimeProperties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.date_Balance.Properties.Mask.EditMask = resources.GetString("date_Balance.Properties.Mask.EditMask");
            this.date_Balance.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.date_Balance.StyleController = this.layoutControl1;
            // 
            // cb_Balance
            // 
            resources.ApplyResources(this.cb_Balance, "cb_Balance");
            this.cb_Balance.Name = "cb_Balance";
            this.cb_Balance.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("cb_Balance.Properties.Buttons"))))});
            this.cb_Balance.Properties.Items.AddRange(new object[] {
            resources.GetString("cb_Balance.Properties.Items"),
            resources.GetString("cb_Balance.Properties.Items1"),
            resources.GetString("cb_Balance.Properties.Items2")});
            this.cb_Balance.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.cb_Balance.StyleController = this.layoutControl1;
            // 
            // spn_Balance
            // 
            resources.ApplyResources(this.spn_Balance, "spn_Balance");
            this.spn_Balance.Name = "spn_Balance";
            this.spn_Balance.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("spn_Balance.Properties.Buttons"))))});
            this.spn_Balance.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.spn_Balance.StyleController = this.layoutControl1;
            // 
            // lookUpEdit_OpenBalanceAccount
            // 
            resources.ApplyResources(this.lookUpEdit_OpenBalanceAccount, "lookUpEdit_OpenBalanceAccount");
            this.lookUpEdit_OpenBalanceAccount.Name = "lookUpEdit_OpenBalanceAccount";
            this.lookUpEdit_OpenBalanceAccount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("lookUpEdit_OpenBalanceAccount.Properties.Buttons"))))});
            this.lookUpEdit_OpenBalanceAccount.Properties.NullText = resources.GetString("lookUpEdit_OpenBalanceAccount.Properties.NullText");
            this.lookUpEdit_OpenBalanceAccount.StyleController = this.layoutControl1;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.gb_OpenBalance});
            this.Root.Name = "Root";
            this.Root.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.Root.Size = new System.Drawing.Size(383, 124);
            this.Root.TextVisible = false;
            // 
            // gb_OpenBalance
            // 
            resources.ApplyResources(this.gb_OpenBalance, "gb_OpenBalance");
            this.gb_OpenBalance.ExpandButtonVisible = true;
            this.gb_OpenBalance.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem13,
            this.layoutControlItem12,
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.spn_Ratexxx,
            this.layoutControlItem1});
            this.gb_OpenBalance.Location = new System.Drawing.Point(0, 0);
            this.gb_OpenBalance.Name = "gb_OpenBalance";
            this.gb_OpenBalance.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.gb_OpenBalance.Size = new System.Drawing.Size(383, 124);
            this.gb_OpenBalance.Click += new System.EventHandler(this.gb_OpenBalance_Click);
            // 
            // layoutControlItem13
            // 
            resources.ApplyResources(this.layoutControlItem13, "layoutControlItem13");
            this.layoutControlItem13.Control = this.date_Balance;
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(377, 25);
            this.layoutControlItem13.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItem12
            // 
            resources.ApplyResources(this.layoutControlItem12, "layoutControlItem12");
            this.layoutControlItem12.Control = this.cb_Balance;
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(377, 24);
            this.layoutControlItem12.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItem16
            // 
            resources.ApplyResources(this.layoutControlItem16, "layoutControlItem16");
            this.layoutControlItem16.Control = this.spn_Balance;
            this.layoutControlItem16.Location = new System.Drawing.Point(213, 24);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(164, 24);
            this.layoutControlItem16.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItem17
            // 
            resources.ApplyResources(this.layoutControlItem17, "layoutControlItem17");
            this.layoutControlItem17.Control = this.lkp_Currancy;
            this.layoutControlItem17.Location = new System.Drawing.Point(114, 24);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(99, 24);
            this.layoutControlItem17.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem17.TextSize = new System.Drawing.Size(29, 13);
            this.layoutControlItem17.TextToControlDistance = 5;
            // 
            // spn_Ratexxx
            // 
            resources.ApplyResources(this.spn_Ratexxx, "spn_Ratexxx");
            this.spn_Ratexxx.Control = this.spn_Rate;
            this.spn_Ratexxx.Location = new System.Drawing.Point(0, 24);
            this.spn_Ratexxx.Name = "spn_Ratexxx";
            this.spn_Ratexxx.Size = new System.Drawing.Size(114, 24);
            this.spn_Ratexxx.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.spn_Ratexxx.TextSize = new System.Drawing.Size(55, 13);
            this.spn_Ratexxx.TextToControlDistance = 5;
            // 
            // layoutControlItem1
            // 
            resources.ApplyResources(this.layoutControlItem1, "layoutControlItem1");
            this.layoutControlItem1.Control = this.lookUpEdit_OpenBalanceAccount;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(377, 24);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(76, 13);
            // 
            // Usc_OpenBalance
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Name = "Usc_OpenBalance";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lkp_Currancy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_Rate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date_Balance.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date_Balance.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Balance.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_Balance.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_OpenBalanceAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gb_OpenBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_Ratexxx)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraEditors.LookUpEdit lkp_Currancy;
        private DevExpress.XtraEditors.SpinEdit spn_Rate;
        private DevExpress.XtraEditors.DateEdit date_Balance;
        private DevExpress.XtraEditors.ComboBoxEdit cb_Balance;
        private DevExpress.XtraEditors.SpinEdit spn_Balance;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit_OpenBalanceAccount;
        private DevExpress.XtraLayout.LayoutControlGroup gb_OpenBalance;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem spn_Ratexxx;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
    }
}
