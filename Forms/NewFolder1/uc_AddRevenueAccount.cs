﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ByStro.Clases;

namespace ByStro.Forms.UserControls
{
    public partial class uc_AddRevenueAccount : DevExpress.XtraEditors.XtraUserControl
    {
        public uc_AddRevenueAccount()
        {
            InitializeComponent();
            RefreshData();
        }
        void RefreshData()
        {
            lookUpEdit1.Properties.DataSource = CurrentSession.UserAccessbileAccounts.Where(x => x.Number.ToString().StartsWith(
            new DAL.DBDataContext(Properties.Settings.Default.Connection_String ) .Acc_Accounts.Where(xa => xa.ID == CurrentSession . Company.RevenueAccount).Single().Number)).Select(x => new { x.Number, x.Name }).ToList();
            lookUpEdit1.Properties.DisplayMember = "Name";
            lookUpEdit1.Properties.ValueMember  = "ID";
            lookUpEdit1.EditValue = CurrentSession . Company.RevenueAccount; 
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (txt_Name.Text.Trim() == string.Empty)
            {
                txt_Name.ErrorText = LangResource.ErrorCantBeEmpry;
                return;
            }
            DAL.DBDataContext dbc = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
            if (dbc.Acc_Accounts.Where(x => x.ParentID == CurrentSession . Company.RevenueAccount  && x.Name == txt_Name.Text.TrimEnd().TrimStart()).Count() > 0)
            {
                XtraMessageBox.Show("");
                return;
            }
            Master.InsertNewAccount(txt_Name.Text, Convert.ToInt32(lookUpEdit1.EditValue ), true, 2);
            txt_Name.Text = string.Empty;
            
            CurrentSession.UserAccessbileAccounts = dbc.Acc_Accounts.ToList();
            lookUpEdit1.Properties.DataSource = CurrentSession.UserAccessbileAccounts.Where(x => x.Number.ToString().StartsWith(
                 dbc.Acc_Accounts.Where(xa => xa.ID  == CurrentSession . Company.RevenueAccount).Single ().Number)).Select(x => new { x.Number, x.Name }).ToList();
        }
    }
}
