﻿using ByStro.PL;
using static ByStro.Forms.frm_Report;

namespace ByStro.Forms
{
    class MultipleForms
    {

    }
    public class InvoiceByCustomerChart : frm_InvoiceByCustomerVendorChart
    {
        public InvoiceByCustomerChart() : base( Clases.MasterClass.PartTypes.Customer)
        {
            this.Text = "مخطط حركات العملاء السنوي";
        }
    }
    public class InvoiceByVendorChart : frm_InvoiceByCustomerVendorChart
    {
        public InvoiceByVendorChart() : base( Clases.MasterClass.PartTypes.Vendor)
        {
            this.Text = "مخطط حركات موردين السنوي";
        }
    }
    public class RevenueEntry : frm_RevExpenEntry
    {
        public RevenueEntry() : base(true)
        {
            this.Text = "تسجيل ايردات";
        }
    } public class RevenueEntryList : frm_RevExpensEntryList
    {
        public RevenueEntryList() : base(true)
        {
            this.Text = "قائمة الايردات";
        }
    }
    public class ExpenceEntry : frm_RevExpenEntry
    {
        public ExpenceEntry() : base(false)
        {
            this.Text = "تسجيل مصروفات";

        }
    }
    public class ExpenceEntryList : frm_RevExpensEntryList
    {
        public ExpenceEntryList() : base(false)
        {
            this.Text = "قائمة المصروفات";

        }
    }
    public class CustomerInvoiceDetails : frm_EditReportFilters
    {
        public CustomerInvoiceDetails() : base(ReportType.CustomerInvoiceDetails)
        {
        }
    }    
    public class FullInvoiceByCustomer : frm_EditReportFilters
    {
        public FullInvoiceByCustomer() : base(ReportType.FullInvoiceBy,Clases.MasterClass.PartTypes.Customer)
        {
        }
    } public class FullInvoiceByVendor : frm_EditReportFilters
    {
        public FullInvoiceByVendor() : base(ReportType.FullInvoiceBy,Clases.MasterClass.PartTypes.Vendor)
        {
        }
    }
    public class PayInstallmentUnPaidList : frm_PayInstallmentList
    {
        public PayInstallmentUnPaidList() : base(false)
        {

        }
    }  
    public class PayInstallmentPaidList : frm_PayInstallmentList
    {
        public PayInstallmentPaidList() : base(true)
        {

        }
    }
    public class RbtCustomer_frm : RbtCustomerSuppliers_frm
    {
        public RbtCustomer_frm() : base(true)
        {

        }
    }
    public class RbtSuppliers_frm : RbtCustomerSuppliers_frm
    {
        public RbtSuppliers_frm() : base(false)
        {

        }
    }
    public class SalesInvoiceOther : frm_Inv_InvoiceOther
    {
        public SalesInvoiceOther() : base( Clases.MasterClass.InvoiceType.SalesInvoice)
        {

        }
    }
    public class SalesReturnInvoiceOther : frm_Inv_InvoiceOther
    {
        public SalesReturnInvoiceOther() : base(Clases.MasterClass.InvoiceType.SalesReturn)
        {

        }
    }
    public class SalesReturnInvoice : frm_Inv_Invoice
    {
        public SalesReturnInvoice() : base( Clases.MasterClass.InvoiceType.SalesReturn)
        {

        }
    }
    public class NewCashNoteIn : frm_CashNote
    {
        public NewCashNoteIn() : base(true)
        {

        }
    }
    public class NewCashNoteOut : frm_CashNote
    {
        public NewCashNoteOut() : base(false)
        {

        }
    }
    public class CashNoteOutList : frm_CashNoteList
    {
        public CashNoteOutList() : base(false)
        {

        }
    }
    public class CashNoteInList : frm_CashNoteList
    {
        public CashNoteInList() : base(true)
        {

        }
    }
    public class DiningTablesOrderDoneList : frm_DiningTablesOrderList
    {
        public DiningTablesOrderDoneList() : base(true)
        {
        }
    }   
    public class DiningTablesOrderNotDoneList : frm_DiningTablesOrderList
    {
        public DiningTablesOrderNotDoneList() : base(false)
        {
        }
    }
}


