﻿using ByStro.Clases;
using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ByStro.Forms
{
    public partial class frm_Drawer : frm_Master 
    {
        DAL.Acc_Drawer drawer = new DAL.Acc_Drawer();

        public frm_Drawer()
        {
            InitializeComponent();
            New();
        }
        public frm_Drawer(int id)
        {
            InitializeComponent();
            GoTo(id);
        }
        public override void frm_Load(object sender, EventArgs e)
    {
        base.frm_Load(sender, e);
        btn_Refresh.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
        btn_Print.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
 
        GetData();
        #region DataChangedEventHandlers
        txt_Name.EditValueChanged += DataChanged;
        spn_Balance.EditValueChanged += DataChanged;
        date_Balance.EditValueChanged += DataChanged;
        cb_Secrecy.SelectedIndexChanged += DataChanged;

            #endregion
     }
        public override void RefreshData()
        {
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
            var drawers = (from c in db.Acc_Drawers
                           join a in db.Acc_Accounts
                           on (int)c.ACCID equals a.ID
                           select new { c.ID, c.Name }).ToList();
            base.RefreshData();
        }
        public  void GoTo(int id)
        {
            if (id.ToString() == txt_ID.Text) return;
            if (ChangesMade && !SaveChangedData()) return;
            LoadObject(id);
            GetData(); 
        }
        public bool CheckIfNameIsUsed(string Name)
        {
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
            var obj = from s in db.Acc_Drawers  where this.txt_ID.Text != s.ID.ToString() && s.Name == Name select s.ID;
            return (obj.Count() > 0);
        }
        private bool ValidData()
        {
            if (string.IsNullOrEmpty(this.txt_Name.Text.Trim()))
            {
                this.txt_Name.ErrorText = LangResource.ErrorCantBeEmpry;
                this.txt_Name.Focus();
                return false;
            }
            if (CheckIfNameIsUsed(this.txt_Name.Text))
            {
                this.txt_Name.ErrorText = LangResource.ErorrThisNameIsUsedBefore;
                this.txt_Name.Focus();
                return false;
            }

            if (Convert.ToDouble(spn_Balance.EditValue) < 0)
            {
                this.spn_Balance.ErrorText = LangResource.ErrorValMustBeGreaterThan0;
                this.spn_Balance.Focus();
                return false;
            }
            if (date_Balance.EditValue == null)
            {
                this.date_Balance.ErrorText = LangResource.ErrorCantBeEmpry;
                this.date_Balance.Focus();
                return false;
            }


            return true;
        }
        int GetNextID()
        {
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
            try
            {
                return (int)db.Acc_Drawers.Max(n => n.ID) + 1;
            }
            catch
            {
                return (int)1;
            }
        }
        public override void Save()
        {
            if (CanSave() == false) return;
            if (!ValidData()) { return; }
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
            DAL.DBDataContext SaveDataContext = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);

            if (IsNew)
            {
                drawer = new DAL.Acc_Drawer ();
                drawer.ID = Convert.ToInt32(txt_ID.Text);
                GenrateAccounts();
                SaveDataContext.Acc_Drawers.InsertOnSubmit(drawer);
            }
            else
            {
                drawer = SaveDataContext.Acc_Drawers.Where(s => s.ID == Convert.ToInt32(drawer.ID)).First();
                
                Master.UpdateAccount((int)drawer.ACCID, txt_Name.Text, secrecy: cb_Secrecy.SelectedIndex);
                RefreshData();

            }
            SetData();
            Master.AccountOpenBalance OP = new Master.AccountOpenBalance();
            OP.IsDebit = true;
            OP.Amount = Convert.ToDouble(spn_Balance.EditValue);
            OP.Date = date_Balance.DateTime;
            Master.CreatAccOpenBalance((int)drawer.ACCID, drawer.Name, OP);
            SaveDataContext.SubmitChanges();
            DAL.DBDataContext objDataContext = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
            CurrentSession.UserAccessbileCustomers = (from c in db.Sl_Customers
                                                      join a in db.Acc_Accounts
                                                      on c.Account equals a.ID
                                                      join g in db.Sl_CustomerGroups on c.GroupID equals g.Number
                                                      select c).ToList();
            base.Save();


        }
        public override void New()
        {
            if (ChangesMade && !SaveChangedData()) return;
            drawer = new DAL.Acc_Drawer ();
            drawer.ID = GetNextID();
            spn_Balance.EditValue = 0;
            date_Balance.DateTime = DateTime.Now; 
            IsNew = true;
            GetData();
            base.New();
            ChangesMade = false;
        }
        public override void Delete()
        {
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
            if (!CanPerformDelete()) return;
            if (IsNew) return;
            var acctevetylog = from i in db.Acc_ActivityDetials
                               join l in db.Acc_Activities on i.AcivityID equals l.ID
                               where i.ACCID == drawer.ACCID && l.Type != "0"
                               select i.ID;
            if (acctevetylog.Count() > 0)
            {
                XtraMessageBox.Show(LangResource.CantDeleteDrawerHasTransactions, "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
          

            PartNumber = drawer.ID.ToString();
            PartName = txt_Name.Text;
            if (Master.AskForDelete(this, IsNew, PartName, PartNumber))
            {
                drawer = db.Acc_Drawers.Where(c => c.ID == drawer.ID).First();
                db.Acc_Drawers.DeleteOnSubmit(drawer);
                db.Acc_Accounts.DeleteOnSubmit(db.Acc_Accounts.Where(a => a.ID == drawer.ACCID ).First());
                Master.DeleteAccountAcctivity("0", drawer.ACCID.ToString());
               db.SubmitChanges();
                base.Delete();
                New();
            }
        }
        void SetData()
        {
            drawer.Name = txt_Name.Text;
            PartNumber = txt_ID.Text;
            PartName = txt_Name.Text;

        }
        void GetData()
        {
            txt_ID.Text = drawer.ID.ToString();
            txt_Name.Text = drawer.Name;
            PartNumber = txt_ID.Text;
            PartName = txt_Name.Text;
            if (!IsNew )
            {
                DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
                cb_Secrecy.SelectedIndex = (int)db.Acc_Accounts.Where(x => x.ID  == drawer.ACCID).First().Secrecy;
                Master.AccountOpenBalance OP = Master.GetAccountOpenBalance((int) drawer.ACCID );
                spn_Balance.EditValue = OP.Amount;
                date_Balance.EditValue = OP.Date;
                OP.Date = date_Balance.DateTime;

            }

            ChangesMade = false;


        }
        void GenrateAccounts()
        {
            drawer .ACCID  =(int) Master.InsertNewAccount(txt_Name.Text, CurrentSession.Company . DrawerAccount , secrecy: cb_Secrecy.SelectedIndex);
            RefreshData();
        }
        void LoadObject(int DrawerID)
        {
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
            drawer = (from i in db.Acc_Drawers where i.ID == DrawerID select i).First();
            IsNew = false;

        }

    }
}
