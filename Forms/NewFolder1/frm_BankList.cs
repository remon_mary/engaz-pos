﻿using ByStro.Clases;
using ByStro.PL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByStro.Forms
{
   public class frm_BankList : FormLayoutList
    {
        DAL.Acc_Bank  acc_Bank;
        public frm_BankList() : base("ID") { }
        public override void PopulateColumns()
        {
            base.PopulateColumns();
            gridView1.CustomColumn(nameof(acc_Bank.ID), "م", true, false);
            gridView1.CustomColumn(nameof(acc_Bank.BankName), "الاسم", true, false);
            gridView1.CustomColumn(nameof(acc_Bank.BankAccountNumber), "رقم الحساب", true, false);
            gridView1.CustomColumn(nameof(acc_Bank.Phone1), "هاتف1", true, false);
            gridView1.CustomColumn(nameof(acc_Bank.Phone2), "هاتف2", true, false);
            gridView1.CustomColumn(nameof(acc_Bank.Phone3), "هاتف3", true, false);
            gridView1.CustomColumn(nameof(acc_Bank.Notes), "ملاحظات", true, false);
        }
        public override void RefreshData()
        {
            using (DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String))
            {
                this.gridControl1.DataSource = db.Acc_Banks.ToList();
            }
            base.RefreshData();
        }
        public override void OpenForm(int id)
        {
            Main_frm.OpenForm(new frm_Bank(id), true);
            base.OpenForm(id);
            RefreshData();
        }
        public override void New()
        {
            Main_frm.OpenForm(new frm_Bank(), true);
            base.New();
            RefreshData();
        }
    }
}
