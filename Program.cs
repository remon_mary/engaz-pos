﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using ByStro.Clases;
using ByStro.DAL;
using ByStro.PL;
using ByStro.Properties;
using DevExpress.DataProcessing;
using DevExpress.LookAndFeel;
using DevExpress.XtraEditors;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;

namespace ByStro
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            bool result;
            var mutex = new System.Threading.Mutex(true,"1241024445555",out result);
            if (!result)
            {
                return; 
            }
            //try
            //{
            string Lang = "ar-EG";
            DevExpress.XtraEditors.WindowsFormsSettings.RightToLeft = DevExpress.Utils.DefaultBoolean.True;
            CultureInfo cultureInfo = new CultureInfo(Lang);
            Thread.CurrentThread.CurrentCulture = new CultureInfo(Lang);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(Lang);
            CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo(Lang);
            CultureInfo.DefaultThreadCurrentCulture = new CultureInfo(Lang); 
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);




            UserLookAndFeel.Default.SkinName = Settings.Default["ApplicationSkinName"].ToString();
            if (Debugger.IsAttached == false) {


                var countryCode = RegionInfo.CurrentRegion.TwoLetterISORegionName;
                AppCenter.SetCountryCode(countryCode);
                Analytics.TrackEvent("SessionStarted");
                AppCenter.Start("c62a9ab2-3f86-43fc-90d9-b4b367dbcb73",
                typeof(Analytics), typeof(Crashes));


                Application.ThreadException += (sender, args) =>
                {
                    Crashes.TrackError(args.Exception);
                    XtraMessageBox.Show(text: " لقد حدث خطأً ما في البرنامج وسيتم ارسال هذا الخطأ الي الدعم الفني " + Environment.NewLine

                        + args.Exception + Environment.NewLine + args.Exception.Message
                        , caption: "ThreadException was unhandeled",
                        icon: MessageBoxIcon.Error, buttons: MessageBoxButtons.OK);

                };

            }

            AppStart(DateTime.Now); 
            new Forms.SplashScreen1().ShowDialog();
            //    Application.Run(new Forms.frm_ProfitAndLosesAnalysis());
            Application.Run(new LoginUser_Form());
            if (Debugger.IsAttached == false)
                SessionEnd();

        }
        public static DateTime SessionStartDate;
        public static void AppStart(DateTime TimeStamp)
        {
            Analytics.TrackEvent("AppStart", new Dictionary<string, string> { { "TimeStamp", TimeStamp.ToString("hh") },{ "Distributer" , "Elhayani" } });
            var d = TimeStamp;
            SessionStartDate =new DateTime( d.Year , d.Month , d.Day , d.Hour ,d.Minute ,d.Second ,d.Kind ) ; 
        }
        public static void   SessionEnd()
        {
            var duration = ( DateTime.Now  -  SessionStartDate).TotalMinutes;
            var dic = GetDataBaseStatistecs();
                dic.Add("Duration", duration.ToString());
            Analytics.TrackEvent("Session Ending", dic);
            Thread.Sleep(2000);
        }
        public static Dictionary<string , string> GetDataBaseStatistecs()
        {
            var dic = new Dictionary<string, string>();
            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);

            try
            {
                var ProductsCount = db.Prodecuts.Count();

                var Customers = db.Customers.Count();
                var Vendors = db.Prodecuts.Count();
                var Stores = db.Inv_Stores.Count();
                var SalesInvoices = db.Inv_Invoices .Where(x=>x.InvoiceType == (byte)MasterClass.InvoiceType.SalesInvoice).Count();
                var PurchasesInvoices = db.Inv_Invoices.Where(x => x.InvoiceType == (byte)MasterClass.InvoiceType.PurchaseInvoice).Count();
                var OpenBanalceInvoice = db.Inv_Invoices.Where(x => x.InvoiceType == (byte)MasterClass.InvoiceType.ItemOpenBalance).Count();
                var StoreTransactions = db.Inv_StoreLogs.Count();

                dic.Add("Products", ProductsCount.ToString());
                dic.Add("Customers", Customers.ToString());
                dic.Add("Vendors", Vendors.ToString());
                dic.Add("Stores", Stores.ToString());
                dic.Add("Sales Invoices", SalesInvoices.ToString());
                dic.Add("Purchases Invoices", PurchasesInvoices.ToString());
                dic.Add("Open Banalce Bills", OpenBanalceInvoice.ToString());
                dic.Add("Store Trans", StoreTransactions.ToString());

            }
            catch (Exception)
            {
                 
            }





            if (dic.Count == 0)
                dic.Add("DataBase Is Empty ", "true");
            return dic;
        }

        #region DBUpdate
        public const int DBVersion = 3;
        public static void CheckForDBUpdate()
        {
            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
            if (db.DatabaseExists() == false) return;
            //try
            //{
            //    var x = db.Instalments.Count();
            //}
            //catch (Exception)
            //{
            //    string cmd = System.IO.File.ReadAllText(Environment.CurrentDirectory + "\\DAL\\DB Update\\AddInstalment.sql");
            //    db.ExecuteCommand(cmd, new object[0]);
            //}
            //try
            //{
            //    var x = db.BarcodeTemplates.Count();
            //}
            //catch (Exception)
            //{
            //    string cmd = System.IO.File.ReadAllText(Environment.CurrentDirectory + "\\DAL\\DB Update\\CreatBarcodeTemplateTable.sql");
            //    db.ExecuteCommand(cmd, new object[0]);
            //}
            //try
            //{
            //    var x = db.Drivers.Count();
            //}
            //catch (Exception)
            //{
            //    string cmd = System.IO.File.ReadAllText(Environment.CurrentDirectory + "\\DAL\\DB Update\\DeliverySystem.sql");
            //    db.ExecuteCommand(cmd, new object[0]);
            //}
            //try
            //{
            //    var x = db.DrawerInOuts.Count();
            //}
            //catch (Exception)
            //{
            //    string cmd = System.IO.File.ReadAllText(Environment.CurrentDirectory + "\\DAL\\DB Update\\DrawerInOut.sql");
            //    db.ExecuteCommand(cmd, new object[0]);
            //}
            //try
            //{
            //    var x = db.SystemSettings.Count();
            //}
            //catch (Exception)
            //{

            //    string cmd = System.IO.File.ReadAllText(Environment.CurrentDirectory + "\\DAL\\DB Update\\SystemSettings.sql");
            //    db.ExecuteCommand(cmd, new object[0]);
            //}

            try
            {
               
                var maxV = db.ST_Updates.Max(x =>(Int32?) x.DBVer)??0;
                if (maxV < DBVersion)
                {
                    var allFiles = System.IO.Directory.GetFiles(Environment.CurrentDirectory + "\\DAL\\DB Update", "Update*", System.IO.SearchOption.TopDirectoryOnly);
                    allFiles.ToList().ForEach(f =>
                    {
                        var FullName = System.IO.Path.GetFileName(f);
                        var num = Convert.ToInt32(FullName.Substring(6, FullName.Length - 10));
                        if (num > maxV)
                        {
                            string cmd = System.IO.File.ReadAllText(f);
                            db.ExecuteCommand(cmd, new object[0]);
                            db.ST_Updates.InsertOnSubmit(new ST_Update()
                            {
                                DBVer = num,
                                UpdateDate = DateTime.Now,
                            });
                            db.SubmitChanges();
                        }
                    });
                }

            }
            catch (Exception ex )
            {
                MessageBox.Show("UpdateError \n" + ex.Message);
            }
        }
        
        #endregion
    }
}
