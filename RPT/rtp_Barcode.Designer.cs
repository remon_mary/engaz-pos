﻿namespace ByStro.RPT
{
    partial class rtp_Barcode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraPrinting.BarCode.Code128Generator code128Generator1 = new DevExpress.XtraPrinting.BarCode.Code128Generator();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.lbl_ItemName = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Price = new DevExpress.XtraReports.UI.XRLabel();
            this.xrBarCode1 = new DevExpress.XtraReports.UI.XRBarCode();
            this.lbl_companyName = new DevExpress.XtraReports.UI.XRLabel();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.Detail.BorderWidth = 1F;
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel1});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 400F;
            this.Detail.KeepTogether = true;
            this.Detail.KeepTogetherWithDetailReports = true;
            this.Detail.MultiColumn.ColumnWidth = 600F;
            this.Detail.MultiColumn.Layout = DevExpress.XtraPrinting.ColumnLayout.AcrossThenDown;
            this.Detail.MultiColumn.Mode = DevExpress.XtraReports.UI.MultiColumnMode.UseColumnCount;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.StylePriority.UseBorders = false;
            this.Detail.StylePriority.UseBorderWidth = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPanel1
            // 
            this.xrPanel1.AnchorHorizontal = ((DevExpress.XtraReports.UI.HorizontalAnchorStyles)((DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left | DevExpress.XtraReports.UI.HorizontalAnchorStyles.Right)));
            this.xrPanel1.AnchorVertical = ((DevExpress.XtraReports.UI.VerticalAnchorStyles)((DevExpress.XtraReports.UI.VerticalAnchorStyles.Top | DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom)));
            this.xrPanel1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPanel1.BorderWidth = 1F;
            this.xrPanel1.CanGrow = false;
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lbl_ItemName,
            this.lbl_Price,
            this.xrBarCode1,
            this.lbl_companyName});
            this.xrPanel1.Dpi = 254F;
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.SizeF = new System.Drawing.SizeF(385.6665F, 388.9583F);
            this.xrPanel1.SnapLinePadding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel1.StylePriority.UseBorders = false;
            this.xrPanel1.StylePriority.UseBorderWidth = false;
            // 
            // lbl_ItemName
            // 
            this.lbl_ItemName.AnchorHorizontal = ((DevExpress.XtraReports.UI.HorizontalAnchorStyles)((DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left | DevExpress.XtraReports.UI.HorizontalAnchorStyles.Right)));
            this.lbl_ItemName.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.lbl_ItemName.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lbl_ItemName.BorderWidth = 2F;
            this.lbl_ItemName.CanShrink = true;
            this.lbl_ItemName.Dpi = 254F;
            this.lbl_ItemName.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Bold);
            this.lbl_ItemName.KeepTogether = true;
            this.lbl_ItemName.LocationFloat = new DevExpress.Utils.PointFloat(0F, 60.85407F);
            this.lbl_ItemName.Multiline = true;
            this.lbl_ItemName.Name = "lbl_ItemName";
            this.lbl_ItemName.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_ItemName.SizeF = new System.Drawing.SizeF(377.2503F, 55.56246F);
            this.lbl_ItemName.StylePriority.UseBorders = false;
            this.lbl_ItemName.StylePriority.UseBorderWidth = false;
            this.lbl_ItemName.StylePriority.UseFont = false;
            this.lbl_ItemName.StylePriority.UseTextAlignment = false;
            this.lbl_ItemName.Text = "اسم الصنف";
            this.lbl_ItemName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lbl_Price
            // 
            this.lbl_Price.AnchorHorizontal = ((DevExpress.XtraReports.UI.HorizontalAnchorStyles)((DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left | DevExpress.XtraReports.UI.HorizontalAnchorStyles.Right)));
            this.lbl_Price.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.lbl_Price.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lbl_Price.BorderWidth = 2F;
            this.lbl_Price.Dpi = 254F;
            this.lbl_Price.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Bold);
            this.lbl_Price.KeepTogether = true;
            this.lbl_Price.LocationFloat = new DevExpress.Utils.PointFloat(0F, 116.4165F);
            this.lbl_Price.Multiline = true;
            this.lbl_Price.Name = "lbl_Price";
            this.lbl_Price.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_Price.SizeF = new System.Drawing.SizeF(377.2502F, 50.27083F);
            this.lbl_Price.StylePriority.UseBorders = false;
            this.lbl_Price.StylePriority.UseBorderWidth = false;
            this.lbl_Price.StylePriority.UseFont = false;
            this.lbl_Price.StylePriority.UseTextAlignment = false;
            this.lbl_Price.Text = "السعر";
            this.lbl_Price.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lbl_Price.TextFormatString = "{0:£0.00}";
            // 
            // xrBarCode1
            // 
            this.xrBarCode1.Alignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrBarCode1.AnchorHorizontal = ((DevExpress.XtraReports.UI.HorizontalAnchorStyles)((DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left | DevExpress.XtraReports.UI.HorizontalAnchorStyles.Right)));
            this.xrBarCode1.AnchorVertical = ((DevExpress.XtraReports.UI.VerticalAnchorStyles)((DevExpress.XtraReports.UI.VerticalAnchorStyles.Top | DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom)));
            this.xrBarCode1.AutoModule = true;
            this.xrBarCode1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrBarCode1.BorderWidth = 1F;
            this.xrBarCode1.Dpi = 254F;
            this.xrBarCode1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrBarCode1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 166.6874F);
            this.xrBarCode1.Name = "xrBarCode1";
            this.xrBarCode1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrBarCode1.SizeF = new System.Drawing.SizeF(377.2502F, 129.2843F);
            this.xrBarCode1.StylePriority.UseBorders = false;
            this.xrBarCode1.StylePriority.UseBorderWidth = false;
            this.xrBarCode1.StylePriority.UseFont = false;
            this.xrBarCode1.StylePriority.UsePadding = false;
            this.xrBarCode1.StylePriority.UseTextAlignment = false;
            this.xrBarCode1.Symbology = code128Generator1;
            this.xrBarCode1.Text = "0123456789";
            this.xrBarCode1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lbl_companyName
            // 
            this.lbl_companyName.AnchorHorizontal = ((DevExpress.XtraReports.UI.HorizontalAnchorStyles)((DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left | DevExpress.XtraReports.UI.HorizontalAnchorStyles.Right)));
            this.lbl_companyName.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.lbl_companyName.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lbl_companyName.CanGrow = false;
            this.lbl_companyName.Dpi = 254F;
            this.lbl_companyName.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_companyName.KeepTogether = true;
            this.lbl_companyName.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.lbl_companyName.Name = "lbl_companyName";
            this.lbl_companyName.Padding = new DevExpress.XtraPrinting.PaddingInfo(9, 9, 9, 9, 254F);
            this.lbl_companyName.SizeF = new System.Drawing.SizeF(377.2503F, 58.41996F);
            this.lbl_companyName.StylePriority.UseBorders = false;
            this.lbl_companyName.StylePriority.UseFont = false;
            this.lbl_companyName.StylePriority.UsePadding = false;
            this.lbl_companyName.StylePriority.UseTextAlignment = false;
            this.lbl_companyName.Text = "اسم الشركه";
            this.lbl_companyName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 0F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 0F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // rtp_Barcode
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.BorderWidth = 0F;
            this.Dpi = 254F;
            this.Font = new System.Drawing.Font("Arial", 9.75F);
            this.Margins = new System.Drawing.Printing.Margins(0, 0, 0, 0);
            this.PageHeight = 300;
            this.PageWidth = 400;
            this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.ReportPrintOptions.DetailCountOnEmptyDataSource = 144;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.ShowPrintMarginsWarning = false;
            this.SnapGridSize = 31.75F;
            this.Version = "19.2";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion
        public DevExpress.XtraReports.UI.DetailBand Detail;
        public  DevExpress.XtraReports.UI.XRPanel xrPanel1;
        public DevExpress.XtraReports.UI.TopMarginBand topMarginBand1;
        public DevExpress.XtraReports.UI.BottomMarginBand bottomMarginBand1;
        public DevExpress.XtraReports.UI.XRLabel lbl_ItemName;
        public DevExpress.XtraReports.UI.XRLabel lbl_Price;
        public DevExpress.XtraReports.UI.XRBarCode xrBarCode1;
        public DevExpress.XtraReports.UI.XRLabel lbl_companyName;
    }
}
