﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace ByStro.RPT
{
    public partial class rtp_TotalGroupProductSells : RPT.rpt_Master
    {
        public rtp_TotalGroupProductSells()
        {
            InitializeComponent();
            objectDataSource1.DataSource = ReportModules.TotalGroupProductSells.GetTotalGroupProductSells();
        }

    }
}
