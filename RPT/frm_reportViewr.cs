﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace ByStro.RPT
{

    public class frm_reportStores : frm_reportViewr
    {
        public frm_reportStores()
        {
            documentViewer1.DocumentSource = new rpt_Stores();
        }

    }
    public class frm_reportTotalGroupProductSells : frm_reportViewr
    {
        public frm_reportTotalGroupProductSells()
        {
            documentViewer1.DocumentSource = new rtp_TotalGroupProductSells();
        }

    }  
    public class frm_InertProduct : frm_reportViewr
    {
        public frm_InertProduct()
        {
            documentViewer1.DocumentSource = new rpt_InertProduct();
        }

    }  
 

    public partial class frm_reportViewr : DevExpress.XtraEditors.XtraForm
    {
        
        public frm_reportViewr()
        {
            InitializeComponent();
            this.Text = "";
        }
    }
}