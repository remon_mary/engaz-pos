﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Linq;
using ByStro.Clases;
using static ByStro.Clases.MasterClass;

namespace ByStro.RPT
{
    public partial class rpt_InertProduct : DevExpress.XtraReports.UI.XtraReport
    {
        public rpt_InertProduct()
        {
            InitializeComponent();
            this.ParametersRequestSubmit += Rpt_InertProduct_ParametersRequestSubmit;
        }

        private void Rpt_InertProduct_ParametersRequestSubmit(object sender, DevExpress.XtraReports.Parameters.ParametersRequestEventArgs e)
        {
            using (DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String))
            {
                var Inv_Invoices = db.Inv_Invoices.AsQueryable();
                if (parameter1.Value is DateTime dateFrom)
                    Inv_Invoices= Inv_Invoices.Where(x => x.Date.Date >= dateFrom.Date);
                if (parameter2.Value is DateTime dateTo)
                    Inv_Invoices = Inv_Invoices.Where(x => x.Date.Date <= dateTo.Date);

                var InvoicesIds = Inv_Invoices.Where(x => x.InvoiceType == (byte)InvoiceType.SalesInvoice|| x.InvoiceType == (byte)InvoiceType.PurchaseInvoice).Select(x => x.ID).ToList();
                var Inv_InvoiceItemIDs = db.Inv_InvoiceDetails.Where(x => InvoicesIds.Contains(x.InvoiceID)).Select(x => x.ItemID).ToList();
                var Prodecuts = db.Prodecuts.Where(x => Inv_InvoiceItemIDs.Contains(x.ProdecutID) == false).ToList();
                objectDataSource1.DataSource = (from x in Prodecuts
                                                select new CustomItem
                                                {
                                                    ID = x.ProdecutID,
                                                    Code = x.FiestUnitBarcode,
                                                    Name = x.ProdecutName,
                                                    Units = CustomUnit.GetUnits(x),
                                                    CustomUnit = CustomUnit.GetCustomBalance(x)
                                                }).ToList();
            }
        }
    }
}
