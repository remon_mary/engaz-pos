﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using ByStro.Clases;
using DevExpress.XtraEditors;
using DevExpress.XtraReports.Parameters;
using System.Linq;
using System.Windows.Forms;
using System.Text;

namespace ByStro.RPT
{
    public partial class rpt_Stores : DevExpress.XtraReports.UI.XtraReport
    {
        public rpt_Stores()
        {
            InitializeComponent();
            this.DataSource = CustomItem.GetCustomItems();
            this.ParametersRequestBeforeShow += ReportReservationDetails_ParametersRequestBeforeShow;
            this.ParametersRequestSubmit += ReportReservationDetails_ParametersRequestSubmit;
        }

        private void ReportReservationDetails_ParametersRequestSubmit(object sender, ParametersRequestEventArgs e)
        {
            StringBuilder Filter = new StringBuilder();
            var CategoryIds = checkedComboBoxEdit.Properties.Items.Where(x => x.CheckState == CheckState.Checked).Select(x => Convert.ToInt32(x.Value)).ToList();
            if (CategoryIds.Count > 0)
            {
                Filter.Append("[CategoryID] = 0");
                foreach (var item in CategoryIds)
                    Filter.Append($" or [CategoryID] ={item}");
            }
            this.FilterString = Filter.ToString();

        }

        CheckedComboBoxEdit checkedComboBoxEdit = new CheckedComboBoxEdit();
        private void ReportReservationDetails_ParametersRequestBeforeShow(object sender, DevExpress.XtraReports.Parameters.ParametersRequestEventArgs e)
        {
            foreach (ParameterInfo info in e.ParametersInformation)
            {
                if (info.Parameter.Name == "parameter1")
                {
                    using (DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String))
                    {
                        checkedComboBoxEdit.Properties.DataSource = db.Categories.Select(x => new { x.CategoryID, x.CategoryName }).ToList();
                        checkedComboBoxEdit.Properties.DisplayMember = "CategoryName";
                        checkedComboBoxEdit.Properties.ValueMember = "CategoryID";
                        info.Editor = checkedComboBoxEdit;
                    }
                }
            }

        }


    }
}
