﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.IO;


namespace ByStro.RPT
{
    public partial class rpt_Master : DevExpress.XtraReports.UI.XtraReport
    {
        public static string ReportPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Bystro\\" + "\\PrintTemplets";
        public rpt_Master()
        {


        }
        public virtual void LoadData()
        {

        }

        private TopMarginBand topMarginBand1;
        private DetailBand detailBand1;
        private XRPictureBox xrPictureBox1;
        public bool UseDefaultPrinter = true;
        public void LoadTemplete()
        {
            this.Name = this.GetType().Name;
            PrintingSystem.Document.AutoFitToPagesWidth = 1;
            RightToLeftLayout = RightToLeftLayout.Yes;
            RightToLeft = RightToLeft.Yes;
            // ShowPrintMarginsWarning = false;
            if (System.IO.File.Exists(ReportPath + "\\" + this.Name + ".xml"))
                LoadLayout(ReportPath + "\\" + this.Name + ".xml");

            RightToLeftLayout = RightToLeftLayout.Yes;
            RightToLeft = RightToLeft.Yes;

            //if (UseDefaultPrinter)
            //{
            //    if (Program.setting.DefaultPrinter != string.Empty && this.PrinterName == string.Empty)
            //        PrinterName = Program.setting.DefaultPrinter;
            //}
            //else
            //{
            //    if (Program.setting.CasherPrinter != string.Empty && this.PrinterName == string.Empty)
            //        PrinterName = Program.setting.CasherPrinter;
            //}

        }

        private void InitializeComponent()
        {
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.detailBand1 = new DevExpress.XtraReports.UI.DetailBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBox1});
            this.topMarginBand1.HeightF = 117.75F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // detailBand1
            // 
            this.detailBand1.Name = "detailBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(484.2083F, 0F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(142.7083F, 117.75F);
            // 
            // rpt_Master
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.topMarginBand1,
            this.detailBand1,
            this.bottomMarginBand1});
            this.Margins = new System.Drawing.Printing.Margins(100, 100, 118, 100);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
            this.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
            this.Version = "19.2";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        private BottomMarginBand bottomMarginBand1;
    }
}
