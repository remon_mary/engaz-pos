﻿namespace ByStro.RPT
{
    partial class rpt_Inv_Invoice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_PrinDate = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.Cell_VATValue = new DevExpress.XtraReports.UI.XRLabel();
            this.cell_DiscountValue = new DevExpress.XtraReports.UI.XRLabel();
            this.cell_Expire = new DevExpress.XtraReports.UI.XRLabel();
            this.cell_Serial = new DevExpress.XtraReports.UI.XRLabel();
            this.cell_Color = new DevExpress.XtraReports.UI.XRLabel();
            this.cell_Size = new DevExpress.XtraReports.UI.XRLabel();
            this.cell_Total = new DevExpress.XtraReports.UI.XRLabel();
            this.cell_Price = new DevExpress.XtraReports.UI.XRLabel();
            this.cell_Qty = new DevExpress.XtraReports.UI.XRLabel();
            this.cell_Unit = new DevExpress.XtraReports.UI.XRLabel();
            this.cell_Item = new DevExpress.XtraReports.UI.XRLabel();
            this.cell_Index = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Index = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Item = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Unit = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Qty = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Price = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_CellTotalText = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Size = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Color = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Serial = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_ExpDate = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.lbl_QtyCount = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_ProductsCount = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.cell_Dicount = new DevExpress.XtraReports.UI.XRLabel();
            this.Cell_Vat = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_DiscountValueText = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_CellDicountText = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_ShipTo = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Driver = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_DriverText = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_NetAsString = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_RemainsText = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Remains = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_paidText = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Paid = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Net = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_NetText = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_OtherCharges = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_OtherChargesText = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_DicountValue = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_DicountText = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Discount = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_TotalText = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Total = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_UserName = new DevExpress.XtraReports.UI.XRLabel();
            this.calculatedField1 = new DevExpress.XtraReports.UI.CalculatedField();
            this.xrControlStyle1 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.xrControlStyle2 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.xrCrossBandBox1 = new DevExpress.XtraReports.UI.XRCrossBandBox();
            this.lbl_rptName = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_companyName = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.lbl_DateText = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_StoreText = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_IDText = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Store = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_ID = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Date = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Cpmpany_Phone = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_InsertUserText = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_InsertUser = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.lbl_PartName = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_PartType = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Code = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_AdressTxt = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Phonetxt = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_City = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Address = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Mobile = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Phone = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_AttinText = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Attintion = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Notes = new DevExpress.XtraReports.UI.XRLabel();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrCrossBandBox2 = new DevExpress.XtraReports.UI.XRCrossBandBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // lbl_PrinDate
            // 
            this.lbl_PrinDate.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Right;
            this.lbl_PrinDate.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.lbl_PrinDate.AutoWidth = true;
            this.lbl_PrinDate.CanShrink = true;
            this.lbl_PrinDate.Dpi = 254F;
            this.lbl_PrinDate.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.lbl_PrinDate.ForeColor = System.Drawing.Color.DarkGray;
            this.lbl_PrinDate.LocationFloat = new DevExpress.Utils.PointFloat(10.58469F, 7.170105F);
            this.lbl_PrinDate.Name = "lbl_PrinDate";
            this.lbl_PrinDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_PrinDate.SizeF = new System.Drawing.SizeF(205.9926F, 36.82996F);
            this.lbl_PrinDate.StylePriority.UseFont = false;
            this.lbl_PrinDate.StylePriority.UseForeColor = false;
            this.lbl_PrinDate.StylePriority.UseTextAlignment = false;
            this.lbl_PrinDate.Text = "lbl_PrinDate";
            this.lbl_PrinDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lbl_PrinDate.WordWrap = false;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 254F;
            this.TopMargin.HeightF = 55F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lbl_PrinDate,
            this.xrPageInfo1,
            this.lbl_UserName});
            this.BottomMargin.Dpi = 254F;
            this.BottomMargin.HeightF = 81F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.AnchorHorizontal = ((DevExpress.XtraReports.UI.HorizontalAnchorStyles)((DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left | DevExpress.XtraReports.UI.HorizontalAnchorStyles.Right)));
            this.xrPageInfo1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrPageInfo1.Dpi = 254F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrPageInfo1.ForeColor = System.Drawing.Color.DimGray;
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(601.136F, 7.170105F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPageInfo1.RunningBand = this.DetailReport;
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(127.8751F, 43.58003F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            this.xrPageInfo1.StylePriority.UseForeColor = false;
            this.xrPageInfo1.StylePriority.UseTextAlignment = false;
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.GroupHeader1,
            this.GroupFooter1});
            this.DetailReport.Dpi = 254F;
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            this.DetailReport.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand;
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.Cell_VATValue,
            this.cell_DiscountValue,
            this.cell_Expire,
            this.cell_Serial,
            this.cell_Color,
            this.cell_Size,
            this.cell_Total,
            this.cell_Price,
            this.cell_Qty,
            this.cell_Unit,
            this.cell_Item,
            this.cell_Index});
            this.Detail1.Dpi = 254F;
            this.Detail1.HeightF = 165F;
            this.Detail1.Name = "Detail1";
            // 
            // Cell_VATValue
            // 
            this.Cell_VATValue.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Cell_VATValue.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.Cell_VATValue.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Cell_VATValue.BorderWidth = 1F;
            this.Cell_VATValue.Dpi = 254F;
            this.Cell_VATValue.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.Cell_VATValue.LocationFloat = new DevExpress.Utils.PointFloat(69.7713F, 82.22916F);
            this.Cell_VATValue.Name = "Cell_VATValue";
            this.Cell_VATValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Cell_VATValue.SizeF = new System.Drawing.SizeF(129.9501F, 76.93749F);
            this.Cell_VATValue.StylePriority.UseBorderColor = false;
            this.Cell_VATValue.StylePriority.UseBorderDashStyle = false;
            this.Cell_VATValue.StylePriority.UseBorders = false;
            this.Cell_VATValue.StylePriority.UseBorderWidth = false;
            this.Cell_VATValue.StylePriority.UseFont = false;
            this.Cell_VATValue.StylePriority.UseTextAlignment = false;
            this.Cell_VATValue.Text = "cell_Index";
            this.Cell_VATValue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // cell_DiscountValue
            // 
            this.cell_DiscountValue.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cell_DiscountValue.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.cell_DiscountValue.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.cell_DiscountValue.BorderWidth = 1F;
            this.cell_DiscountValue.Dpi = 254F;
            this.cell_DiscountValue.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.cell_DiscountValue.LocationFloat = new DevExpress.Utils.PointFloat(199.7214F, 82.77079F);
            this.cell_DiscountValue.Name = "cell_DiscountValue";
            this.cell_DiscountValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.cell_DiscountValue.SizeF = new System.Drawing.SizeF(129.9498F, 76.93749F);
            this.cell_DiscountValue.StylePriority.UseBorderColor = false;
            this.cell_DiscountValue.StylePriority.UseBorderDashStyle = false;
            this.cell_DiscountValue.StylePriority.UseBorders = false;
            this.cell_DiscountValue.StylePriority.UseBorderWidth = false;
            this.cell_DiscountValue.StylePriority.UseFont = false;
            this.cell_DiscountValue.StylePriority.UseTextAlignment = false;
            this.cell_DiscountValue.Text = "cell_Index";
            this.cell_DiscountValue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // cell_Expire
            // 
            this.cell_Expire.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.cell_Expire.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cell_Expire.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.cell_Expire.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.cell_Expire.BorderWidth = 1F;
            this.cell_Expire.Dpi = 254F;
            this.cell_Expire.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.cell_Expire.LocationFloat = new DevExpress.Utils.PointFloat(329.6712F, 82.22916F);
            this.cell_Expire.Name = "cell_Expire";
            this.cell_Expire.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.cell_Expire.SizeF = new System.Drawing.SizeF(166.4436F, 76.9375F);
            this.cell_Expire.StylePriority.UseBackColor = false;
            this.cell_Expire.StylePriority.UseBorderColor = false;
            this.cell_Expire.StylePriority.UseBorderDashStyle = false;
            this.cell_Expire.StylePriority.UseBorders = false;
            this.cell_Expire.StylePriority.UseBorderWidth = false;
            this.cell_Expire.StylePriority.UseFont = false;
            this.cell_Expire.StylePriority.UseTextAlignment = false;
            this.cell_Expire.Text = "cell_Index";
            this.cell_Expire.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.cell_Expire.TextFormatString = "{0:yyyy-MM-dd dddd}";
            // 
            // cell_Serial
            // 
            this.cell_Serial.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.cell_Serial.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cell_Serial.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.cell_Serial.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.cell_Serial.BorderWidth = 1F;
            this.cell_Serial.Dpi = 254F;
            this.cell_Serial.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.cell_Serial.LocationFloat = new DevExpress.Utils.PointFloat(72.91801F, 5.291667F);
            this.cell_Serial.Name = "cell_Serial";
            this.cell_Serial.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.cell_Serial.SizeF = new System.Drawing.SizeF(84.77421F, 76.93749F);
            this.cell_Serial.StylePriority.UseBackColor = false;
            this.cell_Serial.StylePriority.UseBorderColor = false;
            this.cell_Serial.StylePriority.UseBorderDashStyle = false;
            this.cell_Serial.StylePriority.UseBorders = false;
            this.cell_Serial.StylePriority.UseBorderWidth = false;
            this.cell_Serial.StylePriority.UseFont = false;
            this.cell_Serial.StylePriority.UseTextAlignment = false;
            this.cell_Serial.Text = "cell_Index";
            this.cell_Serial.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // cell_Color
            // 
            this.cell_Color.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.cell_Color.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cell_Color.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.cell_Color.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.cell_Color.BorderWidth = 1F;
            this.cell_Color.Dpi = 254F;
            this.cell_Color.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.cell_Color.LocationFloat = new DevExpress.Utils.PointFloat(505.4984F, 82.22916F);
            this.cell_Color.Name = "cell_Color";
            this.cell_Color.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.cell_Color.SizeF = new System.Drawing.SizeF(121.2647F, 76.9375F);
            this.cell_Color.StylePriority.UseBackColor = false;
            this.cell_Color.StylePriority.UseBorderColor = false;
            this.cell_Color.StylePriority.UseBorderDashStyle = false;
            this.cell_Color.StylePriority.UseBorders = false;
            this.cell_Color.StylePriority.UseBorderWidth = false;
            this.cell_Color.StylePriority.UseFont = false;
            this.cell_Color.StylePriority.UseTextAlignment = false;
            this.cell_Color.Text = "cell_Index";
            this.cell_Color.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // cell_Size
            // 
            this.cell_Size.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.cell_Size.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cell_Size.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.cell_Size.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.cell_Size.BorderWidth = 1F;
            this.cell_Size.Dpi = 254F;
            this.cell_Size.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.cell_Size.LocationFloat = new DevExpress.Utils.PointFloat(626.7631F, 82.22916F);
            this.cell_Size.Name = "cell_Size";
            this.cell_Size.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.cell_Size.SizeF = new System.Drawing.SizeF(123.8003F, 76.9375F);
            this.cell_Size.StylePriority.UseBackColor = false;
            this.cell_Size.StylePriority.UseBorderColor = false;
            this.cell_Size.StylePriority.UseBorderDashStyle = false;
            this.cell_Size.StylePriority.UseBorders = false;
            this.cell_Size.StylePriority.UseBorderWidth = false;
            this.cell_Size.StylePriority.UseFont = false;
            this.cell_Size.StylePriority.UseTextAlignment = false;
            this.cell_Size.Text = "cell_Index";
            this.cell_Size.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // cell_Total
            // 
            this.cell_Total.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cell_Total.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.cell_Total.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.cell_Total.BorderWidth = 1F;
            this.cell_Total.Dpi = 254F;
            this.cell_Total.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.cell_Total.LocationFloat = new DevExpress.Utils.PointFloat(626.7631F, 5.291667F);
            this.cell_Total.Name = "cell_Total";
            this.cell_Total.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.cell_Total.SizeF = new System.Drawing.SizeF(123.8004F, 76.93749F);
            this.cell_Total.StylePriority.UseBorderColor = false;
            this.cell_Total.StylePriority.UseBorderDashStyle = false;
            this.cell_Total.StylePriority.UseBorders = false;
            this.cell_Total.StylePriority.UseBorderWidth = false;
            this.cell_Total.StylePriority.UseFont = false;
            this.cell_Total.StylePriority.UseTextAlignment = false;
            this.cell_Total.Text = "cell_Index";
            this.cell_Total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // cell_Price
            // 
            this.cell_Price.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cell_Price.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.cell_Price.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.cell_Price.BorderWidth = 1F;
            this.cell_Price.Dpi = 254F;
            this.cell_Price.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.cell_Price.LocationFloat = new DevExpress.Utils.PointFloat(545.4769F, 5.291667F);
            this.cell_Price.Name = "cell_Price";
            this.cell_Price.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.cell_Price.SizeF = new System.Drawing.SizeF(81.28619F, 76.93749F);
            this.cell_Price.StylePriority.UseBorderColor = false;
            this.cell_Price.StylePriority.UseBorderDashStyle = false;
            this.cell_Price.StylePriority.UseBorders = false;
            this.cell_Price.StylePriority.UseBorderWidth = false;
            this.cell_Price.StylePriority.UseFont = false;
            this.cell_Price.StylePriority.UseTextAlignment = false;
            this.cell_Price.Text = "cell_Index";
            this.cell_Price.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // cell_Qty
            // 
            this.cell_Qty.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cell_Qty.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.cell_Qty.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.cell_Qty.BorderWidth = 1F;
            this.cell_Qty.Dpi = 254F;
            this.cell_Qty.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.cell_Qty.LocationFloat = new DevExpress.Utils.PointFloat(427.7715F, 5.291667F);
            this.cell_Qty.Name = "cell_Qty";
            this.cell_Qty.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.cell_Qty.SizeF = new System.Drawing.SizeF(117.7053F, 76.93749F);
            this.cell_Qty.StylePriority.UseBorderColor = false;
            this.cell_Qty.StylePriority.UseBorderDashStyle = false;
            this.cell_Qty.StylePriority.UseBorders = false;
            this.cell_Qty.StylePriority.UseBorderWidth = false;
            this.cell_Qty.StylePriority.UseFont = false;
            this.cell_Qty.StylePriority.UseTextAlignment = false;
            this.cell_Qty.Text = "cell_Index";
            this.cell_Qty.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // cell_Unit
            // 
            this.cell_Unit.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cell_Unit.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.cell_Unit.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.cell_Unit.BorderWidth = 1F;
            this.cell_Unit.Dpi = 254F;
            this.cell_Unit.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.cell_Unit.LocationFloat = new DevExpress.Utils.PointFloat(339.913F, 5.291667F);
            this.cell_Unit.Name = "cell_Unit";
            this.cell_Unit.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.cell_Unit.SizeF = new System.Drawing.SizeF(87.85843F, 76.93749F);
            this.cell_Unit.StylePriority.UseBorderColor = false;
            this.cell_Unit.StylePriority.UseBorderDashStyle = false;
            this.cell_Unit.StylePriority.UseBorders = false;
            this.cell_Unit.StylePriority.UseBorderWidth = false;
            this.cell_Unit.StylePriority.UseFont = false;
            this.cell_Unit.StylePriority.UseTextAlignment = false;
            this.cell_Unit.Text = "cell_Index";
            this.cell_Unit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // cell_Item
            // 
            this.cell_Item.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cell_Item.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.cell_Item.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.cell_Item.BorderWidth = 1F;
            this.cell_Item.Dpi = 254F;
            this.cell_Item.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.cell_Item.LocationFloat = new DevExpress.Utils.PointFloat(157.6921F, 5.291667F);
            this.cell_Item.Name = "cell_Item";
            this.cell_Item.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.cell_Item.SizeF = new System.Drawing.SizeF(180.6905F, 76.93749F);
            this.cell_Item.StylePriority.UseBorderColor = false;
            this.cell_Item.StylePriority.UseBorderDashStyle = false;
            this.cell_Item.StylePriority.UseBorders = false;
            this.cell_Item.StylePriority.UseBorderWidth = false;
            this.cell_Item.StylePriority.UseFont = false;
            this.cell_Item.StylePriority.UseTextAlignment = false;
            this.cell_Item.Text = "cell_Index";
            this.cell_Item.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // cell_Index
            // 
            this.cell_Index.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cell_Index.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.cell_Index.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.cell_Index.BorderWidth = 1F;
            this.cell_Index.Dpi = 254F;
            this.cell_Index.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.cell_Index.LocationFloat = new DevExpress.Utils.PointFloat(30.58469F, 5.291667F);
            this.cell_Index.Name = "cell_Index";
            this.cell_Index.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.cell_Index.SizeF = new System.Drawing.SizeF(42.04031F, 76.93749F);
            this.cell_Index.StylePriority.UseBorderColor = false;
            this.cell_Index.StylePriority.UseBorderDashStyle = false;
            this.cell_Index.StylePriority.UseBorders = false;
            this.cell_Index.StylePriority.UseBorderWidth = false;
            this.cell_Index.StylePriority.UseFont = false;
            this.cell_Index.StylePriority.UseTextAlignment = false;
            this.cell_Index.Text = "cell_Index";
            this.cell_Index.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel8,
            this.xrLabel7,
            this.lbl_Index,
            this.lbl_Item,
            this.lbl_Unit,
            this.lbl_Qty,
            this.lbl_Price,
            this.lbl_CellTotalText,
            this.lbl_Size,
            this.lbl_Color,
            this.lbl_Serial,
            this.lbl_ExpDate});
            this.GroupHeader1.Dpi = 254F;
            this.GroupHeader1.HeightF = 166.9569F;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // xrLabel8
            // 
            this.xrLabel8.BackColor = System.Drawing.Color.DimGray;
            this.xrLabel8.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.xrLabel8.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrLabel8.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel8.BorderWidth = 1F;
            this.xrLabel8.CanGrow = false;
            this.xrLabel8.Dpi = 254F;
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel8.ForeColor = System.Drawing.Color.White;
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(76.65654F, 86.12427F);
            this.xrLabel8.Multiline = true;
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(123.0648F, 80.8326F);
            this.xrLabel8.StylePriority.UseBackColor = false;
            this.xrLabel8.StylePriority.UseBorderColor = false;
            this.xrLabel8.StylePriority.UseBorderDashStyle = false;
            this.xrLabel8.StylePriority.UseBorders = false;
            this.xrLabel8.StylePriority.UseBorderWidth = false;
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UseForeColor = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "الضريبه";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel7
            // 
            this.xrLabel7.BackColor = System.Drawing.Color.DimGray;
            this.xrLabel7.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.xrLabel7.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrLabel7.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel7.BorderWidth = 1F;
            this.xrLabel7.CanGrow = false;
            this.xrLabel7.Dpi = 254F;
            this.xrLabel7.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.ForeColor = System.Drawing.Color.White;
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(199.7214F, 86.12378F);
            this.xrLabel7.Multiline = true;
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(129.9498F, 80.8326F);
            this.xrLabel7.StylePriority.UseBackColor = false;
            this.xrLabel7.StylePriority.UseBorderColor = false;
            this.xrLabel7.StylePriority.UseBorderDashStyle = false;
            this.xrLabel7.StylePriority.UseBorders = false;
            this.xrLabel7.StylePriority.UseBorderWidth = false;
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UseForeColor = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "الخصم";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lbl_Index
            // 
            this.lbl_Index.BackColor = System.Drawing.Color.DimGray;
            this.lbl_Index.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbl_Index.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.lbl_Index.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_Index.BorderWidth = 1F;
            this.lbl_Index.CanGrow = false;
            this.lbl_Index.Dpi = 254F;
            this.lbl_Index.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Index.ForeColor = System.Drawing.Color.White;
            this.lbl_Index.LocationFloat = new DevExpress.Utils.PointFloat(30.58469F, 5.291667F);
            this.lbl_Index.Multiline = true;
            this.lbl_Index.Name = "lbl_Index";
            this.lbl_Index.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_Index.SizeF = new System.Drawing.SizeF(42.33333F, 80.83259F);
            this.lbl_Index.StylePriority.UseBackColor = false;
            this.lbl_Index.StylePriority.UseBorderColor = false;
            this.lbl_Index.StylePriority.UseBorderDashStyle = false;
            this.lbl_Index.StylePriority.UseBorders = false;
            this.lbl_Index.StylePriority.UseBorderWidth = false;
            this.lbl_Index.StylePriority.UseFont = false;
            this.lbl_Index.StylePriority.UseForeColor = false;
            this.lbl_Index.StylePriority.UseTextAlignment = false;
            this.lbl_Index.Text = "م";
            this.lbl_Index.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lbl_Item
            // 
            this.lbl_Item.BackColor = System.Drawing.Color.DimGray;
            this.lbl_Item.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbl_Item.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.lbl_Item.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_Item.BorderWidth = 1F;
            this.lbl_Item.CanGrow = false;
            this.lbl_Item.Dpi = 254F;
            this.lbl_Item.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Item.ForeColor = System.Drawing.Color.White;
            this.lbl_Item.LocationFloat = new DevExpress.Utils.PointFloat(157.6921F, 5.291424F);
            this.lbl_Item.Multiline = true;
            this.lbl_Item.Name = "lbl_Item";
            this.lbl_Item.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_Item.SizeF = new System.Drawing.SizeF(171.9787F, 80.8326F);
            this.lbl_Item.StylePriority.UseBackColor = false;
            this.lbl_Item.StylePriority.UseBorderColor = false;
            this.lbl_Item.StylePriority.UseBorderDashStyle = false;
            this.lbl_Item.StylePriority.UseBorders = false;
            this.lbl_Item.StylePriority.UseBorderWidth = false;
            this.lbl_Item.StylePriority.UseFont = false;
            this.lbl_Item.StylePriority.UseForeColor = false;
            this.lbl_Item.StylePriority.UseTextAlignment = false;
            this.lbl_Item.Text = "الصنف";
            this.lbl_Item.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lbl_Unit
            // 
            this.lbl_Unit.BackColor = System.Drawing.Color.DimGray;
            this.lbl_Unit.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbl_Unit.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.lbl_Unit.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_Unit.BorderWidth = 1F;
            this.lbl_Unit.CanGrow = false;
            this.lbl_Unit.Dpi = 254F;
            this.lbl_Unit.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Unit.ForeColor = System.Drawing.Color.White;
            this.lbl_Unit.LocationFloat = new DevExpress.Utils.PointFloat(329.6712F, 5.291182F);
            this.lbl_Unit.Multiline = true;
            this.lbl_Unit.Name = "lbl_Unit";
            this.lbl_Unit.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_Unit.SizeF = new System.Drawing.SizeF(98.10022F, 80.8326F);
            this.lbl_Unit.StylePriority.UseBackColor = false;
            this.lbl_Unit.StylePriority.UseBorderColor = false;
            this.lbl_Unit.StylePriority.UseBorderDashStyle = false;
            this.lbl_Unit.StylePriority.UseBorders = false;
            this.lbl_Unit.StylePriority.UseBorderWidth = false;
            this.lbl_Unit.StylePriority.UseFont = false;
            this.lbl_Unit.StylePriority.UseForeColor = false;
            this.lbl_Unit.StylePriority.UseTextAlignment = false;
            this.lbl_Unit.Text = "الوحده";
            this.lbl_Unit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lbl_Qty
            // 
            this.lbl_Qty.BackColor = System.Drawing.Color.DimGray;
            this.lbl_Qty.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbl_Qty.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.lbl_Qty.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_Qty.BorderWidth = 1F;
            this.lbl_Qty.CanGrow = false;
            this.lbl_Qty.Dpi = 254F;
            this.lbl_Qty.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Qty.ForeColor = System.Drawing.Color.White;
            this.lbl_Qty.LocationFloat = new DevExpress.Utils.PointFloat(427.7715F, 5.291182F);
            this.lbl_Qty.Multiline = true;
            this.lbl_Qty.Name = "lbl_Qty";
            this.lbl_Qty.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_Qty.SizeF = new System.Drawing.SizeF(117.7053F, 80.8326F);
            this.lbl_Qty.StylePriority.UseBackColor = false;
            this.lbl_Qty.StylePriority.UseBorderColor = false;
            this.lbl_Qty.StylePriority.UseBorderDashStyle = false;
            this.lbl_Qty.StylePriority.UseBorders = false;
            this.lbl_Qty.StylePriority.UseBorderWidth = false;
            this.lbl_Qty.StylePriority.UseFont = false;
            this.lbl_Qty.StylePriority.UseForeColor = false;
            this.lbl_Qty.StylePriority.UseTextAlignment = false;
            this.lbl_Qty.Text = "الكميه";
            this.lbl_Qty.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lbl_Price
            // 
            this.lbl_Price.BackColor = System.Drawing.Color.DimGray;
            this.lbl_Price.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbl_Price.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.lbl_Price.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_Price.BorderWidth = 1F;
            this.lbl_Price.CanGrow = false;
            this.lbl_Price.Dpi = 254F;
            this.lbl_Price.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Price.ForeColor = System.Drawing.Color.White;
            this.lbl_Price.LocationFloat = new DevExpress.Utils.PointFloat(545.4767F, 5.291182F);
            this.lbl_Price.Multiline = true;
            this.lbl_Price.Name = "lbl_Price";
            this.lbl_Price.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_Price.SizeF = new System.Drawing.SizeF(81.28632F, 80.8326F);
            this.lbl_Price.StylePriority.UseBackColor = false;
            this.lbl_Price.StylePriority.UseBorderColor = false;
            this.lbl_Price.StylePriority.UseBorderDashStyle = false;
            this.lbl_Price.StylePriority.UseBorders = false;
            this.lbl_Price.StylePriority.UseBorderWidth = false;
            this.lbl_Price.StylePriority.UseFont = false;
            this.lbl_Price.StylePriority.UseForeColor = false;
            this.lbl_Price.StylePriority.UseTextAlignment = false;
            this.lbl_Price.Text = "السعر";
            this.lbl_Price.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lbl_CellTotalText
            // 
            this.lbl_CellTotalText.BackColor = System.Drawing.Color.DimGray;
            this.lbl_CellTotalText.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbl_CellTotalText.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.lbl_CellTotalText.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_CellTotalText.BorderWidth = 1F;
            this.lbl_CellTotalText.CanGrow = false;
            this.lbl_CellTotalText.Dpi = 254F;
            this.lbl_CellTotalText.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_CellTotalText.ForeColor = System.Drawing.Color.White;
            this.lbl_CellTotalText.LocationFloat = new DevExpress.Utils.PointFloat(626.763F, 5.29199F);
            this.lbl_CellTotalText.Multiline = true;
            this.lbl_CellTotalText.Name = "lbl_CellTotalText";
            this.lbl_CellTotalText.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_CellTotalText.SizeF = new System.Drawing.SizeF(123.8004F, 80.8326F);
            this.lbl_CellTotalText.StylePriority.UseBackColor = false;
            this.lbl_CellTotalText.StylePriority.UseBorderColor = false;
            this.lbl_CellTotalText.StylePriority.UseBorderDashStyle = false;
            this.lbl_CellTotalText.StylePriority.UseBorders = false;
            this.lbl_CellTotalText.StylePriority.UseBorderWidth = false;
            this.lbl_CellTotalText.StylePriority.UseFont = false;
            this.lbl_CellTotalText.StylePriority.UseForeColor = false;
            this.lbl_CellTotalText.StylePriority.UseTextAlignment = false;
            this.lbl_CellTotalText.Text = "الاجمالي";
            this.lbl_CellTotalText.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lbl_Size
            // 
            this.lbl_Size.BackColor = System.Drawing.Color.Gray;
            this.lbl_Size.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbl_Size.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.lbl_Size.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_Size.BorderWidth = 1F;
            this.lbl_Size.CanGrow = false;
            this.lbl_Size.Dpi = 254F;
            this.lbl_Size.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Size.ForeColor = System.Drawing.Color.White;
            this.lbl_Size.LocationFloat = new DevExpress.Utils.PointFloat(626.763F, 86.12394F);
            this.lbl_Size.Multiline = true;
            this.lbl_Size.Name = "lbl_Size";
            this.lbl_Size.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_Size.SizeF = new System.Drawing.SizeF(123.8004F, 80.8326F);
            this.lbl_Size.StylePriority.UseBackColor = false;
            this.lbl_Size.StylePriority.UseBorderColor = false;
            this.lbl_Size.StylePriority.UseBorderDashStyle = false;
            this.lbl_Size.StylePriority.UseBorders = false;
            this.lbl_Size.StylePriority.UseBorderWidth = false;
            this.lbl_Size.StylePriority.UseFont = false;
            this.lbl_Size.StylePriority.UseForeColor = false;
            this.lbl_Size.StylePriority.UseTextAlignment = false;
            this.lbl_Size.Text = "الحجم";
            this.lbl_Size.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lbl_Color
            // 
            this.lbl_Color.BackColor = System.Drawing.Color.Gray;
            this.lbl_Color.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbl_Color.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.lbl_Color.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_Color.BorderWidth = 1F;
            this.lbl_Color.CanGrow = false;
            this.lbl_Color.Dpi = 254F;
            this.lbl_Color.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Color.ForeColor = System.Drawing.Color.White;
            this.lbl_Color.LocationFloat = new DevExpress.Utils.PointFloat(505.4984F, 86.12378F);
            this.lbl_Color.Multiline = true;
            this.lbl_Color.Name = "lbl_Color";
            this.lbl_Color.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_Color.SizeF = new System.Drawing.SizeF(121.2646F, 80.83261F);
            this.lbl_Color.StylePriority.UseBackColor = false;
            this.lbl_Color.StylePriority.UseBorderColor = false;
            this.lbl_Color.StylePriority.UseBorderDashStyle = false;
            this.lbl_Color.StylePriority.UseBorders = false;
            this.lbl_Color.StylePriority.UseBorderWidth = false;
            this.lbl_Color.StylePriority.UseFont = false;
            this.lbl_Color.StylePriority.UseForeColor = false;
            this.lbl_Color.StylePriority.UseTextAlignment = false;
            this.lbl_Color.Text = "اللون";
            this.lbl_Color.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lbl_Serial
            // 
            this.lbl_Serial.BackColor = System.Drawing.Color.Gray;
            this.lbl_Serial.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbl_Serial.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.lbl_Serial.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_Serial.BorderWidth = 1F;
            this.lbl_Serial.CanGrow = false;
            this.lbl_Serial.Dpi = 254F;
            this.lbl_Serial.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Serial.ForeColor = System.Drawing.Color.White;
            this.lbl_Serial.LocationFloat = new DevExpress.Utils.PointFloat(72.91801F, 5.291182F);
            this.lbl_Serial.Multiline = true;
            this.lbl_Serial.Name = "lbl_Serial";
            this.lbl_Serial.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_Serial.SizeF = new System.Drawing.SizeF(84.77421F, 80.8326F);
            this.lbl_Serial.StylePriority.UseBackColor = false;
            this.lbl_Serial.StylePriority.UseBorderColor = false;
            this.lbl_Serial.StylePriority.UseBorderDashStyle = false;
            this.lbl_Serial.StylePriority.UseBorders = false;
            this.lbl_Serial.StylePriority.UseBorderWidth = false;
            this.lbl_Serial.StylePriority.UseFont = false;
            this.lbl_Serial.StylePriority.UseForeColor = false;
            this.lbl_Serial.StylePriority.UseTextAlignment = false;
            this.lbl_Serial.Text = "كود الصنف";
            this.lbl_Serial.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lbl_ExpDate
            // 
            this.lbl_ExpDate.BackColor = System.Drawing.Color.Gray;
            this.lbl_ExpDate.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbl_ExpDate.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.lbl_ExpDate.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_ExpDate.BorderWidth = 1F;
            this.lbl_ExpDate.CanGrow = false;
            this.lbl_ExpDate.Dpi = 254F;
            this.lbl_ExpDate.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_ExpDate.ForeColor = System.Drawing.Color.White;
            this.lbl_ExpDate.LocationFloat = new DevExpress.Utils.PointFloat(329.6712F, 86.12402F);
            this.lbl_ExpDate.Multiline = true;
            this.lbl_ExpDate.Name = "lbl_ExpDate";
            this.lbl_ExpDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_ExpDate.SizeF = new System.Drawing.SizeF(175.8272F, 80.83261F);
            this.lbl_ExpDate.StylePriority.UseBackColor = false;
            this.lbl_ExpDate.StylePriority.UseBorderColor = false;
            this.lbl_ExpDate.StylePriority.UseBorderDashStyle = false;
            this.lbl_ExpDate.StylePriority.UseBorders = false;
            this.lbl_ExpDate.StylePriority.UseBorderWidth = false;
            this.lbl_ExpDate.StylePriority.UseFont = false;
            this.lbl_ExpDate.StylePriority.UseForeColor = false;
            this.lbl_ExpDate.StylePriority.UseTextAlignment = false;
            this.lbl_ExpDate.Text = "الصلاحيه";
            this.lbl_ExpDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lbl_QtyCount,
            this.lbl_ProductsCount,
            this.xrLabel6,
            this.xrLabel5,
            this.xrPanel1,
            this.lbl_ShipTo,
            this.lbl_Driver,
            this.lbl_DriverText,
            this.lbl_NetAsString,
            this.lbl_RemainsText,
            this.lbl_Remains,
            this.lbl_paidText,
            this.lbl_Paid,
            this.lbl_Net,
            this.lbl_NetText,
            this.lbl_OtherCharges,
            this.lbl_OtherChargesText,
            this.lbl_DicountValue,
            this.lbl_DicountText,
            this.lbl_Discount,
            this.lbl_TotalText,
            this.lbl_Total});
            this.GroupFooter1.Dpi = 254F;
            this.GroupFooter1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail;
            this.GroupFooter1.HeightF = 723.9465F;
            this.GroupFooter1.KeepTogether = true;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // lbl_QtyCount
            // 
            this.lbl_QtyCount.Dpi = 254F;
            this.lbl_QtyCount.LocationFloat = new DevExpress.Utils.PointFloat(533.1522F, 0F);
            this.lbl_QtyCount.Name = "lbl_QtyCount";
            this.lbl_QtyCount.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_QtyCount.SizeF = new System.Drawing.SizeF(174.2752F, 58.41996F);
            this.lbl_QtyCount.StylePriority.UseTextAlignment = false;
            this.lbl_QtyCount.Text = "اجمالي سعر الشراء";
            this.lbl_QtyCount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl_ProductsCount
            // 
            this.lbl_ProductsCount.Dpi = 254F;
            this.lbl_ProductsCount.LocationFloat = new DevExpress.Utils.PointFloat(197.3282F, 0F);
            this.lbl_ProductsCount.Name = "lbl_ProductsCount";
            this.lbl_ProductsCount.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_ProductsCount.SizeF = new System.Drawing.SizeF(171.6097F, 58.41996F);
            this.lbl_ProductsCount.StylePriority.UseTextAlignment = false;
            this.lbl_ProductsCount.Text = "اجمالي سعر الشراء";
            this.lbl_ProductsCount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel6
            // 
            this.xrLabel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.xrLabel6.BorderColor = System.Drawing.Color.Empty;
            this.xrLabel6.Dpi = 254F;
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel6.ForeColor = System.Drawing.Color.White;
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(368.9379F, 0F);
            this.xrLabel6.Multiline = true;
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(164.2144F, 58.42001F);
            this.xrLabel6.StylePriority.UseBackColor = false;
            this.xrLabel6.StylePriority.UseBorderColor = false;
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseForeColor = false;
            this.xrLabel6.StylePriority.UsePadding = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "عدد القطع";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel5
            // 
            this.xrLabel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.xrLabel5.BorderColor = System.Drawing.Color.Empty;
            this.xrLabel5.Dpi = 254F;
            this.xrLabel5.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel5.ForeColor = System.Drawing.Color.White;
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(67.37874F, 0F);
            this.xrLabel5.Multiline = true;
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(129.9494F, 58.42001F);
            this.xrLabel5.StylePriority.UseBackColor = false;
            this.xrLabel5.StylePriority.UseBorderColor = false;
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UseForeColor = false;
            this.xrLabel5.StylePriority.UsePadding = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "الاصناف";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPanel1
            // 
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.cell_Dicount,
            this.Cell_Vat,
            this.xrLabel4,
            this.lbl_DiscountValueText,
            this.lbl_CellDicountText,
            this.xrLabel3});
            this.xrPanel1.Dpi = 254F;
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(43.35184F, 639.2798F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.SizeF = new System.Drawing.SizeF(709.0833F, 84.66669F);
            this.xrPanel1.Visible = false;
            // 
            // cell_Dicount
            // 
            this.cell_Dicount.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cell_Dicount.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.cell_Dicount.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.cell_Dicount.BorderWidth = 1F;
            this.cell_Dicount.Dpi = 254F;
            this.cell_Dicount.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.cell_Dicount.LocationFloat = new DevExpress.Utils.PointFloat(24.02687F, 0F);
            this.cell_Dicount.Name = "cell_Dicount";
            this.cell_Dicount.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.cell_Dicount.SizeF = new System.Drawing.SizeF(129.9495F, 76.93749F);
            this.cell_Dicount.StylePriority.UseBorderColor = false;
            this.cell_Dicount.StylePriority.UseBorderDashStyle = false;
            this.cell_Dicount.StylePriority.UseBorders = false;
            this.cell_Dicount.StylePriority.UseBorderWidth = false;
            this.cell_Dicount.StylePriority.UseFont = false;
            this.cell_Dicount.StylePriority.UseTextAlignment = false;
            this.cell_Dicount.Text = "cell_Index";
            this.cell_Dicount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Cell_Vat
            // 
            this.Cell_Vat.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Cell_Vat.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.Cell_Vat.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Cell_Vat.BorderWidth = 1F;
            this.Cell_Vat.Dpi = 254F;
            this.Cell_Vat.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.Cell_Vat.LocationFloat = new DevExpress.Utils.PointFloat(283.9259F, 0F);
            this.Cell_Vat.Name = "Cell_Vat";
            this.Cell_Vat.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Cell_Vat.SizeF = new System.Drawing.SizeF(129.95F, 76.93749F);
            this.Cell_Vat.StylePriority.UseBorderColor = false;
            this.Cell_Vat.StylePriority.UseBorderDashStyle = false;
            this.Cell_Vat.StylePriority.UseBorders = false;
            this.Cell_Vat.StylePriority.UseBorderWidth = false;
            this.Cell_Vat.StylePriority.UseFont = false;
            this.Cell_Vat.StylePriority.UseTextAlignment = false;
            this.Cell_Vat.Text = "cell_Index";
            this.Cell_Vat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel4
            // 
            this.xrLabel4.BackColor = System.Drawing.Color.DimGray;
            this.xrLabel4.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.xrLabel4.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrLabel4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel4.BorderWidth = 1F;
            this.xrLabel4.CanGrow = false;
            this.xrLabel4.Dpi = 254F;
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel4.ForeColor = System.Drawing.Color.White;
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(270.4501F, 76.9375F);
            this.xrLabel4.Multiline = true;
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(100.8458F, 80.8326F);
            this.xrLabel4.StylePriority.UseBackColor = false;
            this.xrLabel4.StylePriority.UseBorderColor = false;
            this.xrLabel4.StylePriority.UseBorderDashStyle = false;
            this.xrLabel4.StylePriority.UseBorders = false;
            this.xrLabel4.StylePriority.UseBorderWidth = false;
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseForeColor = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "VAT";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lbl_DiscountValueText
            // 
            this.lbl_DiscountValueText.BackColor = System.Drawing.Color.DimGray;
            this.lbl_DiscountValueText.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbl_DiscountValueText.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.lbl_DiscountValueText.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_DiscountValueText.BorderWidth = 1F;
            this.lbl_DiscountValueText.CanGrow = false;
            this.lbl_DiscountValueText.Dpi = 254F;
            this.lbl_DiscountValueText.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lbl_DiscountValueText.ForeColor = System.Drawing.Color.White;
            this.lbl_DiscountValueText.LocationFloat = new DevExpress.Utils.PointFloat(193.4165F, 76.9375F);
            this.lbl_DiscountValueText.Multiline = true;
            this.lbl_DiscountValueText.Name = "lbl_DiscountValueText";
            this.lbl_DiscountValueText.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_DiscountValueText.SizeF = new System.Drawing.SizeF(77.03352F, 80.83261F);
            this.lbl_DiscountValueText.StylePriority.UseBackColor = false;
            this.lbl_DiscountValueText.StylePriority.UseBorderColor = false;
            this.lbl_DiscountValueText.StylePriority.UseBorderDashStyle = false;
            this.lbl_DiscountValueText.StylePriority.UseBorders = false;
            this.lbl_DiscountValueText.StylePriority.UseBorderWidth = false;
            this.lbl_DiscountValueText.StylePriority.UseFont = false;
            this.lbl_DiscountValueText.StylePriority.UseForeColor = false;
            this.lbl_DiscountValueText.StylePriority.UseTextAlignment = false;
            this.lbl_DiscountValueText.Text = "ق خصم";
            this.lbl_DiscountValueText.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lbl_CellDicountText
            // 
            this.lbl_CellDicountText.BackColor = System.Drawing.Color.DimGray;
            this.lbl_CellDicountText.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbl_CellDicountText.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.lbl_CellDicountText.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_CellDicountText.BorderWidth = 1F;
            this.lbl_CellDicountText.CanGrow = false;
            this.lbl_CellDicountText.Dpi = 254F;
            this.lbl_CellDicountText.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lbl_CellDicountText.ForeColor = System.Drawing.Color.White;
            this.lbl_CellDicountText.LocationFloat = new DevExpress.Utils.PointFloat(107.6923F, 76.9375F);
            this.lbl_CellDicountText.Multiline = true;
            this.lbl_CellDicountText.Name = "lbl_CellDicountText";
            this.lbl_CellDicountText.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_CellDicountText.SizeF = new System.Drawing.SizeF(85.72438F, 80.83261F);
            this.lbl_CellDicountText.StylePriority.UseBackColor = false;
            this.lbl_CellDicountText.StylePriority.UseBorderColor = false;
            this.lbl_CellDicountText.StylePriority.UseBorderDashStyle = false;
            this.lbl_CellDicountText.StylePriority.UseBorders = false;
            this.lbl_CellDicountText.StylePriority.UseBorderWidth = false;
            this.lbl_CellDicountText.StylePriority.UseFont = false;
            this.lbl_CellDicountText.StylePriority.UseForeColor = false;
            this.lbl_CellDicountText.StylePriority.UseTextAlignment = false;
            this.lbl_CellDicountText.Text = "خصم %";
            this.lbl_CellDicountText.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel3
            // 
            this.xrLabel3.BackColor = System.Drawing.Color.DimGray;
            this.xrLabel3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.xrLabel3.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrLabel3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel3.BorderWidth = 1F;
            this.xrLabel3.CanGrow = false;
            this.xrLabel3.Dpi = 254F;
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel3.ForeColor = System.Drawing.Color.White;
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(33.30469F, 76.9375F);
            this.xrLabel3.Multiline = true;
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(74.38751F, 80.83261F);
            this.xrLabel3.StylePriority.UseBackColor = false;
            this.xrLabel3.StylePriority.UseBorderColor = false;
            this.xrLabel3.StylePriority.UseBorderDashStyle = false;
            this.xrLabel3.StylePriority.UseBorders = false;
            this.xrLabel3.StylePriority.UseBorderWidth = false;
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseForeColor = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "VAT %";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lbl_ShipTo
            // 
            this.lbl_ShipTo.Dpi = 254F;
            this.lbl_ShipTo.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.lbl_ShipTo.LocationFloat = new DevExpress.Utils.PointFloat(68.86668F, 596.5117F);
            this.lbl_ShipTo.Name = "lbl_ShipTo";
            this.lbl_ShipTo.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_ShipTo.SizeF = new System.Drawing.SizeF(638.2233F, 77.16388F);
            this.lbl_ShipTo.StylePriority.UseFont = false;
            this.lbl_ShipTo.StylePriority.UseTextAlignment = false;
            this.lbl_ShipTo.Text = "عنوان الشحن";
            this.lbl_ShipTo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lbl_ShipTo.TextFormatString = "{0:yyyy-MM-dd dddd hh:mm tt}";
            // 
            // lbl_Driver
            // 
            this.lbl_Driver.Dpi = 254F;
            this.lbl_Driver.LocationFloat = new DevExpress.Utils.PointFloat(335.325F, 536.4332F);
            this.lbl_Driver.Name = "lbl_Driver";
            this.lbl_Driver.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_Driver.SizeF = new System.Drawing.SizeF(372.1024F, 58.41998F);
            this.lbl_Driver.StylePriority.UseTextAlignment = false;
            this.lbl_Driver.Text = "اجمالي سعر الشراء";
            this.lbl_Driver.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl_DriverText
            // 
            this.lbl_DriverText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.lbl_DriverText.BorderColor = System.Drawing.Color.Empty;
            this.lbl_DriverText.Dpi = 254F;
            this.lbl_DriverText.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lbl_DriverText.ForeColor = System.Drawing.Color.White;
            this.lbl_DriverText.LocationFloat = new DevExpress.Utils.PointFloat(67.37872F, 535.6461F);
            this.lbl_DriverText.Multiline = true;
            this.lbl_DriverText.Name = "lbl_DriverText";
            this.lbl_DriverText.Padding = new DevExpress.XtraPrinting.PaddingInfo(15, 15, 0, 0, 254F);
            this.lbl_DriverText.SizeF = new System.Drawing.SizeF(267.9449F, 58.42001F);
            this.lbl_DriverText.StylePriority.UseBackColor = false;
            this.lbl_DriverText.StylePriority.UseBorderColor = false;
            this.lbl_DriverText.StylePriority.UseFont = false;
            this.lbl_DriverText.StylePriority.UseForeColor = false;
            this.lbl_DriverText.StylePriority.UsePadding = false;
            this.lbl_DriverText.StylePriority.UseTextAlignment = false;
            this.lbl_DriverText.Text = "السائق";
            this.lbl_DriverText.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl_NetAsString
            // 
            this.lbl_NetAsString.BackColor = System.Drawing.Color.Gainsboro;
            this.lbl_NetAsString.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_NetAsString.Dpi = 254F;
            this.lbl_NetAsString.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.lbl_NetAsString.LocationFloat = new DevExpress.Utils.PointFloat(65.32095F, 322.9748F);
            this.lbl_NetAsString.Name = "lbl_NetAsString";
            this.lbl_NetAsString.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_NetAsString.SizeF = new System.Drawing.SizeF(642.1061F, 58.41986F);
            this.lbl_NetAsString.StylePriority.UseBackColor = false;
            this.lbl_NetAsString.StylePriority.UseBorders = false;
            this.lbl_NetAsString.StylePriority.UseFont = false;
            this.lbl_NetAsString.StylePriority.UseTextAlignment = false;
            this.lbl_NetAsString.Text = "اجمالي سعر الشراء";
            this.lbl_NetAsString.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lbl_RemainsText
            // 
            this.lbl_RemainsText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.lbl_RemainsText.BorderColor = System.Drawing.Color.Empty;
            this.lbl_RemainsText.Dpi = 254F;
            this.lbl_RemainsText.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lbl_RemainsText.ForeColor = System.Drawing.Color.White;
            this.lbl_RemainsText.LocationFloat = new DevExpress.Utils.PointFloat(65.3206F, 445.9225F);
            this.lbl_RemainsText.Multiline = true;
            this.lbl_RemainsText.Name = "lbl_RemainsText";
            this.lbl_RemainsText.Padding = new DevExpress.XtraPrinting.PaddingInfo(15, 15, 0, 0, 254F);
            this.lbl_RemainsText.SizeF = new System.Drawing.SizeF(267.9449F, 58.42001F);
            this.lbl_RemainsText.StylePriority.UseBackColor = false;
            this.lbl_RemainsText.StylePriority.UseBorderColor = false;
            this.lbl_RemainsText.StylePriority.UseFont = false;
            this.lbl_RemainsText.StylePriority.UseForeColor = false;
            this.lbl_RemainsText.StylePriority.UsePadding = false;
            this.lbl_RemainsText.StylePriority.UseTextAlignment = false;
            this.lbl_RemainsText.Text = "المتبقي";
            this.lbl_RemainsText.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl_Remains
            // 
            this.lbl_Remains.Dpi = 254F;
            this.lbl_Remains.LocationFloat = new DevExpress.Utils.PointFloat(335.325F, 448.3831F);
            this.lbl_Remains.Name = "lbl_Remains";
            this.lbl_Remains.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_Remains.SizeF = new System.Drawing.SizeF(370.002F, 58.41995F);
            this.lbl_Remains.StylePriority.UseTextAlignment = false;
            this.lbl_Remains.Text = "اجمالي سعر الشراء";
            this.lbl_Remains.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl_paidText
            // 
            this.lbl_paidText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.lbl_paidText.BorderColor = System.Drawing.Color.Empty;
            this.lbl_paidText.Dpi = 254F;
            this.lbl_paidText.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lbl_paidText.ForeColor = System.Drawing.Color.White;
            this.lbl_paidText.LocationFloat = new DevExpress.Utils.PointFloat(65.31963F, 385.0568F);
            this.lbl_paidText.Multiline = true;
            this.lbl_paidText.Name = "lbl_paidText";
            this.lbl_paidText.Padding = new DevExpress.XtraPrinting.PaddingInfo(15, 15, 0, 0, 254F);
            this.lbl_paidText.SizeF = new System.Drawing.SizeF(267.9449F, 58.42001F);
            this.lbl_paidText.StylePriority.UseBackColor = false;
            this.lbl_paidText.StylePriority.UseBorderColor = false;
            this.lbl_paidText.StylePriority.UseFont = false;
            this.lbl_paidText.StylePriority.UseForeColor = false;
            this.lbl_paidText.StylePriority.UsePadding = false;
            this.lbl_paidText.StylePriority.UseTextAlignment = false;
            this.lbl_paidText.Text = "المدفوع";
            this.lbl_paidText.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl_Paid
            // 
            this.lbl_Paid.Dpi = 254F;
            this.lbl_Paid.LocationFloat = new DevExpress.Utils.PointFloat(335.325F, 386.287F);
            this.lbl_Paid.Name = "lbl_Paid";
            this.lbl_Paid.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_Paid.SizeF = new System.Drawing.SizeF(370.002F, 58.41995F);
            this.lbl_Paid.StylePriority.UseTextAlignment = false;
            this.lbl_Paid.Text = "اجمالي سعر الشراء";
            this.lbl_Paid.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl_Net
            // 
            this.lbl_Net.Dpi = 254F;
            this.lbl_Net.LocationFloat = new DevExpress.Utils.PointFloat(333.2649F, 262.1087F);
            this.lbl_Net.Name = "lbl_Net";
            this.lbl_Net.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_Net.SizeF = new System.Drawing.SizeF(374.1624F, 58.41995F);
            this.lbl_Net.StylePriority.UseTextAlignment = false;
            this.lbl_Net.Text = "اجمالي سعر الشراء";
            this.lbl_Net.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl_NetText
            // 
            this.lbl_NetText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.lbl_NetText.BorderColor = System.Drawing.Color.Empty;
            this.lbl_NetText.Dpi = 254F;
            this.lbl_NetText.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lbl_NetText.ForeColor = System.Drawing.Color.White;
            this.lbl_NetText.LocationFloat = new DevExpress.Utils.PointFloat(63.78961F, 262.1087F);
            this.lbl_NetText.Multiline = true;
            this.lbl_NetText.Name = "lbl_NetText";
            this.lbl_NetText.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.lbl_NetText.SizeF = new System.Drawing.SizeF(267.9449F, 58.42001F);
            this.lbl_NetText.StylePriority.UseBackColor = false;
            this.lbl_NetText.StylePriority.UseBorderColor = false;
            this.lbl_NetText.StylePriority.UseFont = false;
            this.lbl_NetText.StylePriority.UseForeColor = false;
            this.lbl_NetText.StylePriority.UsePadding = false;
            this.lbl_NetText.StylePriority.UseTextAlignment = false;
            this.lbl_NetText.Text = "الصافي";
            this.lbl_NetText.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl_OtherCharges
            // 
            this.lbl_OtherCharges.Dpi = 254F;
            this.lbl_OtherCharges.LocationFloat = new DevExpress.Utils.PointFloat(334.7948F, 183.7733F);
            this.lbl_OtherCharges.Name = "lbl_OtherCharges";
            this.lbl_OtherCharges.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_OtherCharges.SizeF = new System.Drawing.SizeF(372.6326F, 58.41998F);
            this.lbl_OtherCharges.StylePriority.UseTextAlignment = false;
            this.lbl_OtherCharges.Text = "اجمالي سعر البيع";
            this.lbl_OtherCharges.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl_OtherChargesText
            // 
            this.lbl_OtherChargesText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.lbl_OtherChargesText.BorderColor = System.Drawing.Color.Empty;
            this.lbl_OtherChargesText.Dpi = 254F;
            this.lbl_OtherChargesText.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lbl_OtherChargesText.ForeColor = System.Drawing.Color.White;
            this.lbl_OtherChargesText.LocationFloat = new DevExpress.Utils.PointFloat(65.31966F, 183.7733F);
            this.lbl_OtherChargesText.Multiline = true;
            this.lbl_OtherChargesText.Name = "lbl_OtherChargesText";
            this.lbl_OtherChargesText.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.lbl_OtherChargesText.SizeF = new System.Drawing.SizeF(267.9449F, 58.42001F);
            this.lbl_OtherChargesText.StylePriority.UseBackColor = false;
            this.lbl_OtherChargesText.StylePriority.UseBorderColor = false;
            this.lbl_OtherChargesText.StylePriority.UseFont = false;
            this.lbl_OtherChargesText.StylePriority.UseForeColor = false;
            this.lbl_OtherChargesText.StylePriority.UsePadding = false;
            this.lbl_OtherChargesText.StylePriority.UseTextAlignment = false;
            this.lbl_OtherChargesText.Text = "مصاريف اخري";
            this.lbl_OtherChargesText.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl_DicountValue
            // 
            this.lbl_DicountValue.Dpi = 254F;
            this.lbl_DicountValue.LocationFloat = new DevExpress.Utils.PointFloat(333.2649F, 122.9074F);
            this.lbl_DicountValue.Name = "lbl_DicountValue";
            this.lbl_DicountValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_DicountValue.SizeF = new System.Drawing.SizeF(228.6765F, 58.41998F);
            this.lbl_DicountValue.StylePriority.UseTextAlignment = false;
            this.lbl_DicountValue.Text = "اجمالي سعر البيع";
            this.lbl_DicountValue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl_DicountText
            // 
            this.lbl_DicountText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.lbl_DicountText.BorderColor = System.Drawing.Color.Empty;
            this.lbl_DicountText.Dpi = 254F;
            this.lbl_DicountText.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lbl_DicountText.ForeColor = System.Drawing.Color.White;
            this.lbl_DicountText.LocationFloat = new DevExpress.Utils.PointFloat(65.31966F, 122.9074F);
            this.lbl_DicountText.Multiline = true;
            this.lbl_DicountText.Name = "lbl_DicountText";
            this.lbl_DicountText.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.lbl_DicountText.SizeF = new System.Drawing.SizeF(267.9449F, 58.42001F);
            this.lbl_DicountText.StylePriority.UseBackColor = false;
            this.lbl_DicountText.StylePriority.UseBorderColor = false;
            this.lbl_DicountText.StylePriority.UseFont = false;
            this.lbl_DicountText.StylePriority.UseForeColor = false;
            this.lbl_DicountText.StylePriority.UsePadding = false;
            this.lbl_DicountText.StylePriority.UseTextAlignment = false;
            this.lbl_DicountText.Text = "الخصم";
            this.lbl_DicountText.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl_Discount
            // 
            this.lbl_Discount.Dpi = 254F;
            this.lbl_Discount.LocationFloat = new DevExpress.Utils.PointFloat(566.5122F, 125.3533F);
            this.lbl_Discount.Name = "lbl_Discount";
            this.lbl_Discount.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_Discount.SizeF = new System.Drawing.SizeF(140.9152F, 58.42F);
            this.lbl_Discount.StylePriority.UseTextAlignment = false;
            this.lbl_Discount.Text = "اجمالي سعر البيع";
            this.lbl_Discount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lbl_Discount.TextFormatString = "{0:0.00%}";
            // 
            // lbl_TotalText
            // 
            this.lbl_TotalText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.lbl_TotalText.BorderColor = System.Drawing.Color.Empty;
            this.lbl_TotalText.Dpi = 254F;
            this.lbl_TotalText.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lbl_TotalText.ForeColor = System.Drawing.Color.White;
            this.lbl_TotalText.LocationFloat = new DevExpress.Utils.PointFloat(65.31966F, 62.0416F);
            this.lbl_TotalText.Multiline = true;
            this.lbl_TotalText.Name = "lbl_TotalText";
            this.lbl_TotalText.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.lbl_TotalText.SizeF = new System.Drawing.SizeF(267.9449F, 58.42001F);
            this.lbl_TotalText.StylePriority.UseBackColor = false;
            this.lbl_TotalText.StylePriority.UseBorderColor = false;
            this.lbl_TotalText.StylePriority.UseFont = false;
            this.lbl_TotalText.StylePriority.UseForeColor = false;
            this.lbl_TotalText.StylePriority.UsePadding = false;
            this.lbl_TotalText.StylePriority.UseTextAlignment = false;
            this.lbl_TotalText.Text = "الاجمالي";
            this.lbl_TotalText.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl_Total
            // 
            this.lbl_Total.Dpi = 254F;
            this.lbl_Total.LocationFloat = new DevExpress.Utils.PointFloat(333.2649F, 62.04176F);
            this.lbl_Total.Name = "lbl_Total";
            this.lbl_Total.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_Total.SizeF = new System.Drawing.SizeF(374.1626F, 58.41996F);
            this.lbl_Total.StylePriority.UseTextAlignment = false;
            this.lbl_Total.Text = "اجمالي سعر الشراء";
            this.lbl_Total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl_UserName
            // 
            this.lbl_UserName.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.lbl_UserName.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.lbl_UserName.AutoWidth = true;
            this.lbl_UserName.CanShrink = true;
            this.lbl_UserName.Dpi = 254F;
            this.lbl_UserName.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.lbl_UserName.ForeColor = System.Drawing.Color.DimGray;
            this.lbl_UserName.LocationFloat = new DevExpress.Utils.PointFloat(279.605F, 0F);
            this.lbl_UserName.Name = "lbl_UserName";
            this.lbl_UserName.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_UserName.SizeF = new System.Drawing.SizeF(254F, 44.00006F);
            this.lbl_UserName.StylePriority.UseFont = false;
            this.lbl_UserName.StylePriority.UseForeColor = false;
            this.lbl_UserName.Text = "xrLabel1";
            // 
            // calculatedField1
            // 
            this.calculatedField1.DataMember = "Inv_ItemSalesInvoice.Inv_ItemSalesInvoiceInv_ItemSalesInvoiceDetails";
            this.calculatedField1.Name = "calculatedField1";
            // 
            // xrControlStyle1
            // 
            this.xrControlStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.xrControlStyle1.Name = "xrControlStyle1";
            this.xrControlStyle1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            // 
            // xrControlStyle2
            // 
            this.xrControlStyle2.Name = "xrControlStyle2";
            this.xrControlStyle2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            // 
            // xrCrossBandBox1
            // 
            this.xrCrossBandBox1.AnchorVertical = ((DevExpress.XtraReports.UI.VerticalAnchorStyles)((DevExpress.XtraReports.UI.VerticalAnchorStyles.Top | DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom)));
            this.xrCrossBandBox1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrCrossBandBox1.Dpi = 254F;
            this.xrCrossBandBox1.EndBand = this.GroupFooter1;
            this.xrCrossBandBox1.EndPointFloat = new DevExpress.Utils.PointFloat(25F, 723.9465F);
            this.xrCrossBandBox1.Name = "xrCrossBandBox1";
            this.xrCrossBandBox1.StartBand = this.GroupHeader1;
            this.xrCrossBandBox1.StartPointFloat = new DevExpress.Utils.PointFloat(25F, 0F);
            this.xrCrossBandBox1.WidthF = 741.4381F;
            // 
            // lbl_rptName
            // 
            this.lbl_rptName.BackColor = System.Drawing.Color.Empty;
            this.lbl_rptName.CanGrow = false;
            this.lbl_rptName.Dpi = 254F;
            this.lbl_rptName.Font = new System.Drawing.Font("Times New Roman", 28F, System.Drawing.FontStyle.Bold);
            this.lbl_rptName.ForeColor = System.Drawing.Color.Navy;
            this.lbl_rptName.LocationFloat = new DevExpress.Utils.PointFloat(25F, 407.75F);
            this.lbl_rptName.Name = "lbl_rptName";
            this.lbl_rptName.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.lbl_rptName.SizeF = new System.Drawing.SizeF(741.4381F, 111.3367F);
            this.lbl_rptName.StylePriority.UseBackColor = false;
            this.lbl_rptName.StylePriority.UseFont = false;
            this.lbl_rptName.StylePriority.UseForeColor = false;
            this.lbl_rptName.StylePriority.UsePadding = false;
            this.lbl_rptName.StylePriority.UseTextAlignment = false;
            this.lbl_rptName.Text = "نوع الفاتوره";
            this.lbl_rptName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lbl_rptName.WordWrap = false;
            // 
            // lbl_companyName
            // 
            this.lbl_companyName.CanGrow = false;
            this.lbl_companyName.Dpi = 254F;
            this.lbl_companyName.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.lbl_companyName.LocationFloat = new DevExpress.Utils.PointFloat(25F, 519.0868F);
            this.lbl_companyName.Name = "lbl_companyName";
            this.lbl_companyName.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_companyName.SizeF = new System.Drawing.SizeF(740.6249F, 87.02084F);
            this.lbl_companyName.StylePriority.UseFont = false;
            this.lbl_companyName.StylePriority.UseTextAlignment = false;
            this.lbl_companyName.Text = "اسم الشركه";
            this.lbl_companyName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrPictureBox1.Dpi = 254F;
            this.xrPictureBox1.ImageAlignment = DevExpress.XtraPrinting.ImageAlignment.MiddleCenter;
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(126.0441F, 25.00001F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(496.8336F, 382.75F);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            // 
            // lbl_DateText
            // 
            this.lbl_DateText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.lbl_DateText.BorderColor = System.Drawing.Color.Empty;
            this.lbl_DateText.Dpi = 254F;
            this.lbl_DateText.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lbl_DateText.ForeColor = System.Drawing.Color.White;
            this.lbl_DateText.LocationFloat = new DevExpress.Utils.PointFloat(43.06875F, 751.9772F);
            this.lbl_DateText.Multiline = true;
            this.lbl_DateText.Name = "lbl_DateText";
            this.lbl_DateText.Padding = new DevExpress.XtraPrinting.PaddingInfo(15, 15, 0, 0, 254F);
            this.lbl_DateText.SizeF = new System.Drawing.SizeF(267.9449F, 58.42001F);
            this.lbl_DateText.StylePriority.UseBackColor = false;
            this.lbl_DateText.StylePriority.UseBorderColor = false;
            this.lbl_DateText.StylePriority.UseFont = false;
            this.lbl_DateText.StylePriority.UseForeColor = false;
            this.lbl_DateText.StylePriority.UsePadding = false;
            this.lbl_DateText.Text = "التاريخ";
            // 
            // lbl_StoreText
            // 
            this.lbl_StoreText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.lbl_StoreText.BorderColor = System.Drawing.Color.Empty;
            this.lbl_StoreText.Dpi = 254F;
            this.lbl_StoreText.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lbl_StoreText.ForeColor = System.Drawing.Color.White;
            this.lbl_StoreText.LocationFloat = new DevExpress.Utils.PointFloat(43.06875F, 818.0874F);
            this.lbl_StoreText.Multiline = true;
            this.lbl_StoreText.Name = "lbl_StoreText";
            this.lbl_StoreText.Padding = new DevExpress.XtraPrinting.PaddingInfo(15, 15, 0, 0, 254F);
            this.lbl_StoreText.SizeF = new System.Drawing.SizeF(267.9449F, 58.42001F);
            this.lbl_StoreText.StylePriority.UseBackColor = false;
            this.lbl_StoreText.StylePriority.UseBorderColor = false;
            this.lbl_StoreText.StylePriority.UseFont = false;
            this.lbl_StoreText.StylePriority.UseForeColor = false;
            this.lbl_StoreText.StylePriority.UsePadding = false;
            this.lbl_StoreText.Text = "الفرع";
            // 
            // lbl_IDText
            // 
            this.lbl_IDText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.lbl_IDText.BorderColor = System.Drawing.Color.Empty;
            this.lbl_IDText.Dpi = 254F;
            this.lbl_IDText.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lbl_IDText.ForeColor = System.Drawing.Color.White;
            this.lbl_IDText.LocationFloat = new DevExpress.Utils.PointFloat(43.06875F, 685.8668F);
            this.lbl_IDText.Multiline = true;
            this.lbl_IDText.Name = "lbl_IDText";
            this.lbl_IDText.Padding = new DevExpress.XtraPrinting.PaddingInfo(15, 15, 0, 0, 254F);
            this.lbl_IDText.SizeF = new System.Drawing.SizeF(267.9449F, 58.42F);
            this.lbl_IDText.StylePriority.UseBackColor = false;
            this.lbl_IDText.StylePriority.UseBorderColor = false;
            this.lbl_IDText.StylePriority.UseFont = false;
            this.lbl_IDText.StylePriority.UseForeColor = false;
            this.lbl_IDText.StylePriority.UsePadding = false;
            this.lbl_IDText.Text = "كود";
            // 
            // lbl_Store
            // 
            this.lbl_Store.Dpi = 254F;
            this.lbl_Store.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.lbl_Store.LocationFloat = new DevExpress.Utils.PointFloat(321.4857F, 818.0873F);
            this.lbl_Store.Name = "lbl_Store";
            this.lbl_Store.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_Store.SizeF = new System.Drawing.SizeF(435.0166F, 58.42001F);
            this.lbl_Store.StylePriority.UseFont = false;
            this.lbl_Store.StylePriority.UseTextAlignment = false;
            this.lbl_Store.Text = "المخزن";
            this.lbl_Store.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl_ID
            // 
            this.lbl_ID.Dpi = 254F;
            this.lbl_ID.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.lbl_ID.LocationFloat = new DevExpress.Utils.PointFloat(319.9561F, 685.8668F);
            this.lbl_ID.Name = "lbl_ID";
            this.lbl_ID.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_ID.SizeF = new System.Drawing.SizeF(194.2271F, 58.42F);
            this.lbl_ID.StylePriority.UseFont = false;
            this.lbl_ID.StylePriority.UseTextAlignment = false;
            this.lbl_ID.Text = "كود";
            this.lbl_ID.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl_Date
            // 
            this.lbl_Date.Dpi = 254F;
            this.lbl_Date.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.lbl_Date.LocationFloat = new DevExpress.Utils.PointFloat(321.4861F, 751.9772F);
            this.lbl_Date.Name = "lbl_Date";
            this.lbl_Date.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_Date.SizeF = new System.Drawing.SizeF(435.0166F, 58.41995F);
            this.lbl_Date.StylePriority.UseFont = false;
            this.lbl_Date.StylePriority.UseTextAlignment = false;
            this.lbl_Date.Text = "التاريخ";
            this.lbl_Date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lbl_Date.TextFormatString = "{0:yyyy-MM-dd dddd hh:mm tt}";
            // 
            // lbl_Cpmpany_Phone
            // 
            this.lbl_Cpmpany_Phone.Dpi = 254F;
            this.lbl_Cpmpany_Phone.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.lbl_Cpmpany_Phone.LocationFloat = new DevExpress.Utils.PointFloat(25F, 615.6633F);
            this.lbl_Cpmpany_Phone.Name = "lbl_Cpmpany_Phone";
            this.lbl_Cpmpany_Phone.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_Cpmpany_Phone.SizeF = new System.Drawing.SizeF(740.6249F, 58.41998F);
            this.lbl_Cpmpany_Phone.StylePriority.UseFont = false;
            this.lbl_Cpmpany_Phone.StylePriority.UseTextAlignment = false;
            this.lbl_Cpmpany_Phone.Text = "تلفون الشركه";
            this.lbl_Cpmpany_Phone.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lbl_InsertUserText
            // 
            this.lbl_InsertUserText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.lbl_InsertUserText.BorderColor = System.Drawing.Color.Empty;
            this.lbl_InsertUserText.Dpi = 254F;
            this.lbl_InsertUserText.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lbl_InsertUserText.ForeColor = System.Drawing.Color.White;
            this.lbl_InsertUserText.LocationFloat = new DevExpress.Utils.PointFloat(43.06875F, 884.1978F);
            this.lbl_InsertUserText.Multiline = true;
            this.lbl_InsertUserText.Name = "lbl_InsertUserText";
            this.lbl_InsertUserText.Padding = new DevExpress.XtraPrinting.PaddingInfo(15, 15, 0, 0, 254F);
            this.lbl_InsertUserText.SizeF = new System.Drawing.SizeF(267.9449F, 58.42004F);
            this.lbl_InsertUserText.StylePriority.UseBackColor = false;
            this.lbl_InsertUserText.StylePriority.UseBorderColor = false;
            this.lbl_InsertUserText.StylePriority.UseFont = false;
            this.lbl_InsertUserText.StylePriority.UseForeColor = false;
            this.lbl_InsertUserText.StylePriority.UsePadding = false;
            this.lbl_InsertUserText.Text = "المستخدم";
            // 
            // lbl_InsertUser
            // 
            this.lbl_InsertUser.Dpi = 254F;
            this.lbl_InsertUser.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.lbl_InsertUser.LocationFloat = new DevExpress.Utils.PointFloat(319.9561F, 884.1978F);
            this.lbl_InsertUser.Name = "lbl_InsertUser";
            this.lbl_InsertUser.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_InsertUser.SizeF = new System.Drawing.SizeF(435.0167F, 58.41998F);
            this.lbl_InsertUser.StylePriority.UseFont = false;
            this.lbl_InsertUser.StylePriority.UseTextAlignment = false;
            this.lbl_InsertUser.Text = "المستخدم";
            this.lbl_InsertUser.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine2
            // 
            this.xrLine2.Dpi = 254F;
            this.xrLine2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(0.2930215F, 674.0834F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(774.707F, 11.78326F);
            this.xrLine2.StylePriority.UseForeColor = false;
            // 
            // lbl_PartName
            // 
            this.lbl_PartName.Dpi = 254F;
            this.lbl_PartName.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.lbl_PartName.LocationFloat = new DevExpress.Utils.PointFloat(311.0136F, 942.6179F);
            this.lbl_PartName.Name = "lbl_PartName";
            this.lbl_PartName.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_PartName.SizeF = new System.Drawing.SizeF(445.489F, 58.42004F);
            this.lbl_PartName.StylePriority.UseFont = false;
            this.lbl_PartName.StylePriority.UseTextAlignment = false;
            this.lbl_PartName.Text = "كود";
            this.lbl_PartName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl_PartType
            // 
            this.lbl_PartType.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.lbl_PartType.BorderColor = System.Drawing.Color.Empty;
            this.lbl_PartType.Dpi = 254F;
            this.lbl_PartType.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lbl_PartType.ForeColor = System.Drawing.Color.White;
            this.lbl_PartType.LocationFloat = new DevExpress.Utils.PointFloat(43.06875F, 942.6177F);
            this.lbl_PartType.Multiline = true;
            this.lbl_PartType.Name = "lbl_PartType";
            this.lbl_PartType.Padding = new DevExpress.XtraPrinting.PaddingInfo(15, 15, 0, 0, 254F);
            this.lbl_PartType.SizeF = new System.Drawing.SizeF(267.9449F, 58.42F);
            this.lbl_PartType.StylePriority.UseBackColor = false;
            this.lbl_PartType.StylePriority.UseBorderColor = false;
            this.lbl_PartType.StylePriority.UseFont = false;
            this.lbl_PartType.StylePriority.UseForeColor = false;
            this.lbl_PartType.StylePriority.UsePadding = false;
            this.lbl_PartType.Text = "From";
            // 
            // lbl_Code
            // 
            this.lbl_Code.Dpi = 254F;
            this.lbl_Code.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.lbl_Code.LocationFloat = new DevExpress.Utils.PointFloat(533.1522F, 685.8668F);
            this.lbl_Code.Name = "lbl_Code";
            this.lbl_Code.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_Code.SizeF = new System.Drawing.SizeF(223.3504F, 58.42001F);
            this.lbl_Code.StylePriority.UseFont = false;
            this.lbl_Code.StylePriority.UseTextAlignment = false;
            this.lbl_Code.Text = "كود";
            this.lbl_Code.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(514.1832F, 685.8668F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(18.96906F, 58.42F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "-";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl_AdressTxt
            // 
            this.lbl_AdressTxt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.lbl_AdressTxt.BorderColor = System.Drawing.Color.Empty;
            this.lbl_AdressTxt.Dpi = 254F;
            this.lbl_AdressTxt.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lbl_AdressTxt.ForeColor = System.Drawing.Color.White;
            this.lbl_AdressTxt.LocationFloat = new DevExpress.Utils.PointFloat(43.06875F, 1074.838F);
            this.lbl_AdressTxt.Multiline = true;
            this.lbl_AdressTxt.Name = "lbl_AdressTxt";
            this.lbl_AdressTxt.Padding = new DevExpress.XtraPrinting.PaddingInfo(15, 15, 0, 0, 254F);
            this.lbl_AdressTxt.SizeF = new System.Drawing.SizeF(267.9449F, 58.42F);
            this.lbl_AdressTxt.StylePriority.UseBackColor = false;
            this.lbl_AdressTxt.StylePriority.UseBorderColor = false;
            this.lbl_AdressTxt.StylePriority.UseFont = false;
            this.lbl_AdressTxt.StylePriority.UseForeColor = false;
            this.lbl_AdressTxt.StylePriority.UsePadding = false;
            this.lbl_AdressTxt.Text = "العنوان";
            // 
            // lbl_Phonetxt
            // 
            this.lbl_Phonetxt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.lbl_Phonetxt.BorderColor = System.Drawing.Color.Empty;
            this.lbl_Phonetxt.Dpi = 254F;
            this.lbl_Phonetxt.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lbl_Phonetxt.ForeColor = System.Drawing.Color.White;
            this.lbl_Phonetxt.LocationFloat = new DevExpress.Utils.PointFloat(43.06875F, 1140.949F);
            this.lbl_Phonetxt.Multiline = true;
            this.lbl_Phonetxt.Name = "lbl_Phonetxt";
            this.lbl_Phonetxt.Padding = new DevExpress.XtraPrinting.PaddingInfo(15, 15, 0, 0, 254F);
            this.lbl_Phonetxt.SizeF = new System.Drawing.SizeF(267.9449F, 58.42F);
            this.lbl_Phonetxt.StylePriority.UseBackColor = false;
            this.lbl_Phonetxt.StylePriority.UseBorderColor = false;
            this.lbl_Phonetxt.StylePriority.UseFont = false;
            this.lbl_Phonetxt.StylePriority.UseForeColor = false;
            this.lbl_Phonetxt.StylePriority.UsePadding = false;
            this.lbl_Phonetxt.Text = "الهاتف";
            // 
            // lbl_City
            // 
            this.lbl_City.Dpi = 254F;
            this.lbl_City.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.lbl_City.LocationFloat = new DevExpress.Utils.PointFloat(311.0146F, 1074.838F);
            this.lbl_City.Name = "lbl_City";
            this.lbl_City.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_City.SizeF = new System.Drawing.SizeF(187.8727F, 58.42004F);
            this.lbl_City.StylePriority.UseFont = false;
            this.lbl_City.StylePriority.UseTextAlignment = false;
            this.lbl_City.Text = "كود";
            this.lbl_City.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl_Address
            // 
            this.lbl_Address.Dpi = 254F;
            this.lbl_Address.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.lbl_Address.LocationFloat = new DevExpress.Utils.PointFloat(500.4169F, 1074.838F);
            this.lbl_Address.Name = "lbl_Address";
            this.lbl_Address.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_Address.SizeF = new System.Drawing.SizeF(254.2628F, 58.42004F);
            this.lbl_Address.StylePriority.UseFont = false;
            this.lbl_Address.StylePriority.UseTextAlignment = false;
            this.lbl_Address.Text = "كود";
            this.lbl_Address.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl_Mobile
            // 
            this.lbl_Mobile.Dpi = 254F;
            this.lbl_Mobile.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.lbl_Mobile.LocationFloat = new DevExpress.Utils.PointFloat(564.9023F, 1140.948F);
            this.lbl_Mobile.Name = "lbl_Mobile";
            this.lbl_Mobile.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_Mobile.SizeF = new System.Drawing.SizeF(191.6003F, 58.4201F);
            this.lbl_Mobile.StylePriority.UseFont = false;
            this.lbl_Mobile.StylePriority.UseTextAlignment = false;
            this.lbl_Mobile.Text = "كود";
            this.lbl_Mobile.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl_Phone
            // 
            this.lbl_Phone.Dpi = 254F;
            this.lbl_Phone.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.lbl_Phone.LocationFloat = new DevExpress.Utils.PointFloat(311.0146F, 1140.949F);
            this.lbl_Phone.Name = "lbl_Phone";
            this.lbl_Phone.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_Phone.SizeF = new System.Drawing.SizeF(222.1377F, 58.42004F);
            this.lbl_Phone.StylePriority.UseFont = false;
            this.lbl_Phone.StylePriority.UseTextAlignment = false;
            this.lbl_Phone.Text = "كود";
            this.lbl_Phone.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Dpi = 254F;
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(533.1522F, 1140.949F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(31.75F, 58.41998F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "-";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl_AttinText
            // 
            this.lbl_AttinText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.lbl_AttinText.BorderColor = System.Drawing.Color.Empty;
            this.lbl_AttinText.Dpi = 254F;
            this.lbl_AttinText.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lbl_AttinText.ForeColor = System.Drawing.Color.White;
            this.lbl_AttinText.LocationFloat = new DevExpress.Utils.PointFloat(43.06875F, 1008.728F);
            this.lbl_AttinText.Multiline = true;
            this.lbl_AttinText.Name = "lbl_AttinText";
            this.lbl_AttinText.Padding = new DevExpress.XtraPrinting.PaddingInfo(15, 15, 0, 0, 254F);
            this.lbl_AttinText.SizeF = new System.Drawing.SizeF(267.9449F, 58.42F);
            this.lbl_AttinText.StylePriority.UseBackColor = false;
            this.lbl_AttinText.StylePriority.UseBorderColor = false;
            this.lbl_AttinText.StylePriority.UseFont = false;
            this.lbl_AttinText.StylePriority.UseForeColor = false;
            this.lbl_AttinText.StylePriority.UsePadding = false;
            this.lbl_AttinText.Text = "عنايه";
            // 
            // lbl_Attintion
            // 
            this.lbl_Attintion.Dpi = 254F;
            this.lbl_Attintion.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.lbl_Attintion.LocationFloat = new DevExpress.Utils.PointFloat(311.0136F, 1008.728F);
            this.lbl_Attintion.Name = "lbl_Attintion";
            this.lbl_Attintion.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_Attintion.SizeF = new System.Drawing.SizeF(445.489F, 58.41998F);
            this.lbl_Attintion.StylePriority.UseFont = false;
            this.lbl_Attintion.StylePriority.UseTextAlignment = false;
            this.lbl_Attintion.Text = "كود";
            this.lbl_Attintion.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl_Notes
            // 
            this.lbl_Notes.Dpi = 254F;
            this.lbl_Notes.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.lbl_Notes.LocationFloat = new DevExpress.Utils.PointFloat(43.06875F, 1199.369F);
            this.lbl_Notes.Name = "lbl_Notes";
            this.lbl_Notes.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_Notes.SizeF = new System.Drawing.SizeF(711.611F, 58.42004F);
            this.lbl_Notes.StylePriority.UseFont = false;
            this.lbl_Notes.StylePriority.UseTextAlignment = false;
            this.lbl_Notes.Text = "ملاحظات الفاتوره";
            this.lbl_Notes.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lbl_Notes.TextFormatString = "{0:yyyy-MM-dd dddd hh:mm tt}";
            // 
            // Detail
            // 
            this.Detail.BackColor = System.Drawing.Color.Empty;
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lbl_Notes,
            this.lbl_Attintion,
            this.lbl_AttinText,
            this.xrLabel2,
            this.lbl_Phone,
            this.lbl_Mobile,
            this.lbl_Address,
            this.lbl_City,
            this.lbl_Phonetxt,
            this.lbl_AdressTxt,
            this.xrLabel1,
            this.lbl_Code,
            this.lbl_PartType,
            this.lbl_PartName,
            this.xrLine2,
            this.lbl_InsertUser,
            this.lbl_InsertUserText,
            this.lbl_Cpmpany_Phone,
            this.lbl_Date,
            this.lbl_ID,
            this.lbl_Store,
            this.lbl_IDText,
            this.lbl_StoreText,
            this.lbl_DateText,
            this.xrPictureBox1,
            this.lbl_companyName,
            this.lbl_rptName});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 1257.789F;
            this.Detail.KeepTogether = true;
            this.Detail.KeepTogetherWithDetailReports = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.StylePriority.UseBackColor = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrCrossBandBox2
            // 
            this.xrCrossBandBox2.AnchorVertical = ((DevExpress.XtraReports.UI.VerticalAnchorStyles)((DevExpress.XtraReports.UI.VerticalAnchorStyles.Top | DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom)));
            this.xrCrossBandBox2.Dpi = 254F;
            this.xrCrossBandBox2.EndBand = this.Detail1;
            this.xrCrossBandBox2.EndPointFloat = new DevExpress.Utils.PointFloat(25.29302F, 165F);
            this.xrCrossBandBox2.Name = "xrCrossBandBox2";
            this.xrCrossBandBox2.StartBand = this.Detail1;
            this.xrCrossBandBox2.StartPointFloat = new DevExpress.Utils.PointFloat(25.29302F, 0F);
            this.xrCrossBandBox2.WidthF = 740.3318F;
            // 
            // rpt_Inv_Invoice
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.DetailReport});
            this.CalculatedFields.AddRange(new DevExpress.XtraReports.UI.CalculatedField[] {
            this.calculatedField1});
            this.CrossBandControls.AddRange(new DevExpress.XtraReports.UI.XRCrossBandControl[] {
            this.xrCrossBandBox2,
            this.xrCrossBandBox1});
            this.Dpi = 254F;
            this.Margins = new System.Drawing.Printing.Margins(10, 10, 55, 81);
            this.PageHeight = 2970;
            this.PageWidth = 800;
            this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.RollPaper = true;
            this.SnapGridSize = 25F;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.xrControlStyle1,
            this.xrControlStyle2});
            this.Version = "19.2";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.CalculatedField calculatedField1;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.XRLabel lbl_TotalText;
        private DevExpress.XtraReports.UI.XRLabel lbl_Total;
        private DevExpress.XtraReports.UI.XRLabel lbl_Item;
        private DevExpress.XtraReports.UI.XRLabel lbl_Unit;
        private DevExpress.XtraReports.UI.XRLabel lbl_Qty;
        private DevExpress.XtraReports.UI.XRLabel lbl_Price;
        private DevExpress.XtraReports.UI.XRLabel lbl_CellTotalText;
        private DevExpress.XtraReports.UI.XRLabel lbl_Size;
        private DevExpress.XtraReports.UI.XRLabel lbl_Color;
        private DevExpress.XtraReports.UI.XRLabel lbl_Serial;
        private DevExpress.XtraReports.UI.XRLabel lbl_ExpDate;
        private DevExpress.XtraReports.UI.XRControlStyle xrControlStyle1;
        private DevExpress.XtraReports.UI.XRControlStyle xrControlStyle2;
        private DevExpress.XtraReports.UI.XRLabel lbl_PrinDate;
        private DevExpress.XtraReports.UI.XRLabel lbl_UserName;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRLabel lbl_Index;
        private DevExpress.XtraReports.UI.XRLabel lbl_NetAsString;
        private DevExpress.XtraReports.UI.XRLabel lbl_RemainsText;
        private DevExpress.XtraReports.UI.XRLabel lbl_Remains;
        private DevExpress.XtraReports.UI.XRLabel lbl_paidText;
        private DevExpress.XtraReports.UI.XRLabel lbl_Paid;
        private DevExpress.XtraReports.UI.XRLabel lbl_Net;
        private DevExpress.XtraReports.UI.XRLabel lbl_NetText;
        private DevExpress.XtraReports.UI.XRLabel lbl_OtherCharges;
        private DevExpress.XtraReports.UI.XRLabel lbl_OtherChargesText;
        private DevExpress.XtraReports.UI.XRLabel lbl_DicountValue;
        private DevExpress.XtraReports.UI.XRLabel lbl_DicountText;
        private DevExpress.XtraReports.UI.XRLabel lbl_Discount;
        private DevExpress.XtraReports.UI.XRLabel cell_Price;
        private DevExpress.XtraReports.UI.XRLabel cell_Qty;
        private DevExpress.XtraReports.UI.XRLabel cell_Unit;
        private DevExpress.XtraReports.UI.XRLabel cell_Item;
        private DevExpress.XtraReports.UI.XRLabel cell_Index;
        private DevExpress.XtraReports.UI.XRLabel lbl_ShipTo;
        private DevExpress.XtraReports.UI.XRLabel lbl_Driver;
        private DevExpress.XtraReports.UI.XRLabel lbl_DriverText;
        private DevExpress.XtraReports.UI.XRLabel lbl_CellDicountText;
        private DevExpress.XtraReports.UI.XRLabel cell_Total;
        private DevExpress.XtraReports.UI.XRLabel cell_DiscountValue;
        private DevExpress.XtraReports.UI.XRLabel cell_Dicount;
        private DevExpress.XtraReports.UI.XRLabel lbl_DiscountValueText;
        private DevExpress.XtraReports.UI.XRLabel cell_Expire;
        private DevExpress.XtraReports.UI.XRLabel cell_Serial;
        private DevExpress.XtraReports.UI.XRLabel cell_Color;
        private DevExpress.XtraReports.UI.XRLabel cell_Size;
        private DevExpress.XtraReports.UI.XRCrossBandBox xrCrossBandBox1;
        private DevExpress.XtraReports.UI.XRLabel lbl_companyName;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        private DevExpress.XtraReports.UI.XRLabel lbl_DateText;
        private DevExpress.XtraReports.UI.XRLabel lbl_StoreText;
        private DevExpress.XtraReports.UI.XRLabel lbl_IDText;
        private DevExpress.XtraReports.UI.XRLabel lbl_Store;
        private DevExpress.XtraReports.UI.XRLabel lbl_ID;
        private DevExpress.XtraReports.UI.XRLabel lbl_Date;
        private DevExpress.XtraReports.UI.XRLabel lbl_Cpmpany_Phone;
        private DevExpress.XtraReports.UI.XRLabel lbl_InsertUserText;
        private DevExpress.XtraReports.UI.XRLabel lbl_InsertUser;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRLabel lbl_PartName;
        private DevExpress.XtraReports.UI.XRLabel lbl_PartType;
        private DevExpress.XtraReports.UI.XRLabel lbl_Code;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel lbl_AdressTxt;
        private DevExpress.XtraReports.UI.XRLabel lbl_Phonetxt;
        private DevExpress.XtraReports.UI.XRLabel lbl_City;
        private DevExpress.XtraReports.UI.XRLabel lbl_Address;
        private DevExpress.XtraReports.UI.XRLabel lbl_Mobile;
        private DevExpress.XtraReports.UI.XRLabel lbl_Phone;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel lbl_AttinText;
        private DevExpress.XtraReports.UI.XRLabel lbl_Attintion;
        private DevExpress.XtraReports.UI.XRLabel lbl_Notes;
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.XRCrossBandBox xrCrossBandBox2;
        protected internal DevExpress.XtraReports.UI.XRLabel lbl_rptName;
        private DevExpress.XtraReports.UI.XRLabel Cell_Vat;
        private DevExpress.XtraReports.UI.XRLabel Cell_VATValue;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRPanel xrPanel1;
        private DevExpress.XtraReports.UI.XRLabel lbl_QtyCount;
        private DevExpress.XtraReports.UI.XRLabel lbl_ProductsCount;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
    }
}
