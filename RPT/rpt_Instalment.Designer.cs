﻿namespace ByStro.RPT
{
    partial class rpt_Instalment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraPrinting.BarCode.Code128Generator code128Generator1 = new DevExpress.XtraPrinting.BarCode.Code128Generator();
            this.lbl_PrinDate = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_PrintDateText = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrCheckBox1 = new DevExpress.XtraReports.UI.XRCheckBox();
            this.cell_PayDate = new DevExpress.XtraReports.UI.XRLabel();
            this.cell_Amount = new DevExpress.XtraReports.UI.XRLabel();
            this.cell_DueDate = new DevExpress.XtraReports.UI.XRLabel();
            this.cell_Index = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.lbl_Index = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Item = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Unit = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Qty = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Price = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.lbl_usernameTxt = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_UserName = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Period = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Count = new DevExpress.XtraReports.UI.XRLabel();
            this.xrBarCode1 = new DevExpress.XtraReports.UI.XRBarCode();
            this.lbl_NetAsString = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_RemainsText = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Remains = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_paidText = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Paid = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Amount = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_NetText = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Benfite = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_DicountText = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_TotalText = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Total = new DevExpress.XtraReports.UI.XRLabel();
            this.calculatedField1 = new DevExpress.XtraReports.UI.CalculatedField();
            this.xrControlStyle1 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.xrControlStyle2 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.xrCrossBandBox1 = new DevExpress.XtraReports.UI.XRCrossBandBox();
            this.lbl_rptName = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_companyName = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.lbl_DateText = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_IDText = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_ID = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Date = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Cpmpany_Phone = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.lbl_PartName = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_PartType = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_AdressTxt = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Phonetxt = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Address = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Mobile = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Phone = new DevExpress.XtraReports.UI.XRLabel();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrCrossBandBox2 = new DevExpress.XtraReports.UI.XRCrossBandBox();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_AdvancedPay = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // lbl_PrinDate
            // 
            this.lbl_PrinDate.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Right;
            this.lbl_PrinDate.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.lbl_PrinDate.AutoWidth = true;
            this.lbl_PrinDate.CanShrink = true;
            this.lbl_PrinDate.Dpi = 254F;
            this.lbl_PrinDate.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.lbl_PrinDate.ForeColor = System.Drawing.Color.DarkGray;
            this.lbl_PrinDate.LocationFloat = new DevExpress.Utils.PointFloat(1586.567F, 46.49345F);
            this.lbl_PrinDate.Name = "lbl_PrinDate";
            this.lbl_PrinDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_PrinDate.SizeF = new System.Drawing.SizeF(325.1515F, 43.58001F);
            this.lbl_PrinDate.StylePriority.UseFont = false;
            this.lbl_PrinDate.StylePriority.UseForeColor = false;
            this.lbl_PrinDate.StylePriority.UseTextAlignment = false;
            this.lbl_PrinDate.Text = "lbl_PrinDate";
            this.lbl_PrinDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lbl_PrinDate.WordWrap = false;
            // 
            // lbl_PrintDateText
            // 
            this.lbl_PrintDateText.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Right;
            this.lbl_PrintDateText.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.lbl_PrintDateText.AutoWidth = true;
            this.lbl_PrintDateText.CanShrink = true;
            this.lbl_PrintDateText.Dpi = 254F;
            this.lbl_PrintDateText.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.lbl_PrintDateText.ForeColor = System.Drawing.Color.DarkGray;
            this.lbl_PrintDateText.LocationFloat = new DevExpress.Utils.PointFloat(1643.309F, 0F);
            this.lbl_PrintDateText.Name = "lbl_PrintDateText";
            this.lbl_PrintDateText.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_PrintDateText.SizeF = new System.Drawing.SizeF(211.6665F, 43.58003F);
            this.lbl_PrintDateText.StylePriority.UseFont = false;
            this.lbl_PrintDateText.StylePriority.UseForeColor = false;
            this.lbl_PrintDateText.StylePriority.UseTextAlignment = false;
            this.lbl_PrintDateText.Text = "تاريخ الطباعه";
            this.lbl_PrintDateText.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 254F;
            this.TopMargin.HeightF = 55F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lbl_PrintDateText,
            this.lbl_PrinDate,
            this.xrPageInfo1,
            this.lbl_usernameTxt,
            this.lbl_UserName});
            this.BottomMargin.Dpi = 254F;
            this.BottomMargin.HeightF = 99.00688F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 254F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrPageInfo1.ForeColor = System.Drawing.Color.DimGray;
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(856.8497F, 7.170105F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPageInfo1.RunningBand = this.DetailReport;
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(208.7292F, 43.58003F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            this.xrPageInfo1.StylePriority.UseForeColor = false;
            this.xrPageInfo1.StylePriority.UseTextAlignment = false;
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.GroupHeader1,
            this.GroupFooter1});
            this.DetailReport.Dpi = 254F;
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            this.DetailReport.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand;
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrCheckBox1,
            this.cell_PayDate,
            this.cell_Amount,
            this.cell_DueDate,
            this.cell_Index});
            this.Detail1.Dpi = 254F;
            this.Detail1.HeightF = 74.5158F;
            this.Detail1.Name = "Detail1";
            // 
            // xrCheckBox1
            // 
            this.xrCheckBox1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrCheckBox1.Dpi = 254F;
            this.xrCheckBox1.GlyphOptions.Alignment = DevExpress.Utils.HorzAlignment.Center;
            this.xrCheckBox1.LocationFloat = new DevExpress.Utils.PointFloat(1455.599F, 5.291667F);
            this.xrCheckBox1.Name = "xrCheckBox1";
            this.xrCheckBox1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrCheckBox1.SizeF = new System.Drawing.SizeF(504.4015F, 66.82668F);
            this.xrCheckBox1.StylePriority.UseBorders = false;
            this.xrCheckBox1.StylePriority.UseTextAlignment = false;
            this.xrCheckBox1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            // 
            // cell_PayDate
            // 
            this.cell_PayDate.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cell_PayDate.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.cell_PayDate.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.cell_PayDate.BorderWidth = 1F;
            this.cell_PayDate.Dpi = 254F;
            this.cell_PayDate.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.cell_PayDate.LocationFloat = new DevExpress.Utils.PointFloat(990.2414F, 5.291667F);
            this.cell_PayDate.Name = "cell_PayDate";
            this.cell_PayDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.cell_PayDate.SizeF = new System.Drawing.SizeF(465.357F, 66.82668F);
            this.cell_PayDate.StylePriority.UseBorderColor = false;
            this.cell_PayDate.StylePriority.UseBorderDashStyle = false;
            this.cell_PayDate.StylePriority.UseBorders = false;
            this.cell_PayDate.StylePriority.UseBorderWidth = false;
            this.cell_PayDate.StylePriority.UseFont = false;
            this.cell_PayDate.StylePriority.UseTextAlignment = false;
            this.cell_PayDate.Text = "cell_Index";
            this.cell_PayDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // cell_Amount
            // 
            this.cell_Amount.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cell_Amount.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.cell_Amount.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.cell_Amount.BorderWidth = 1F;
            this.cell_Amount.Dpi = 254F;
            this.cell_Amount.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.cell_Amount.LocationFloat = new DevExpress.Utils.PointFloat(653.8223F, 5.291667F);
            this.cell_Amount.Name = "cell_Amount";
            this.cell_Amount.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.cell_Amount.SizeF = new System.Drawing.SizeF(336.4191F, 66.82668F);
            this.cell_Amount.StylePriority.UseBorderColor = false;
            this.cell_Amount.StylePriority.UseBorderDashStyle = false;
            this.cell_Amount.StylePriority.UseBorders = false;
            this.cell_Amount.StylePriority.UseBorderWidth = false;
            this.cell_Amount.StylePriority.UseFont = false;
            this.cell_Amount.StylePriority.UseTextAlignment = false;
            this.cell_Amount.Text = "cell_Index";
            this.cell_Amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // cell_DueDate
            // 
            this.cell_DueDate.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cell_DueDate.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.cell_DueDate.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.cell_DueDate.BorderWidth = 1F;
            this.cell_DueDate.Dpi = 254F;
            this.cell_DueDate.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.cell_DueDate.LocationFloat = new DevExpress.Utils.PointFloat(66.14585F, 5.291667F);
            this.cell_DueDate.Name = "cell_DueDate";
            this.cell_DueDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.cell_DueDate.SizeF = new System.Drawing.SizeF(587.6765F, 66.82668F);
            this.cell_DueDate.StylePriority.UseBorderColor = false;
            this.cell_DueDate.StylePriority.UseBorderDashStyle = false;
            this.cell_DueDate.StylePriority.UseBorders = false;
            this.cell_DueDate.StylePriority.UseBorderWidth = false;
            this.cell_DueDate.StylePriority.UseFont = false;
            this.cell_DueDate.StylePriority.UseTextAlignment = false;
            this.cell_DueDate.Text = "cell_Index";
            this.cell_DueDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // cell_Index
            // 
            this.cell_Index.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cell_Index.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.cell_Index.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.cell_Index.BorderWidth = 1F;
            this.cell_Index.Dpi = 254F;
            this.cell_Index.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.cell_Index.LocationFloat = new DevExpress.Utils.PointFloat(5.584688F, 5.291667F);
            this.cell_Index.Name = "cell_Index";
            this.cell_Index.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.cell_Index.SizeF = new System.Drawing.SizeF(60.56115F, 66.82668F);
            this.cell_Index.StylePriority.UseBorderColor = false;
            this.cell_Index.StylePriority.UseBorderDashStyle = false;
            this.cell_Index.StylePriority.UseBorders = false;
            this.cell_Index.StylePriority.UseBorderWidth = false;
            this.cell_Index.StylePriority.UseFont = false;
            this.cell_Index.StylePriority.UseTextAlignment = false;
            this.cell_Index.Text = "cell_Index";
            this.cell_Index.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lbl_Index,
            this.lbl_Item,
            this.lbl_Unit,
            this.lbl_Qty,
            this.lbl_Price});
            this.GroupHeader1.Dpi = 254F;
            this.GroupHeader1.HeightF = 62.31257F;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // lbl_Index
            // 
            this.lbl_Index.BackColor = System.Drawing.Color.DimGray;
            this.lbl_Index.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbl_Index.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.lbl_Index.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_Index.BorderWidth = 1F;
            this.lbl_Index.CanGrow = false;
            this.lbl_Index.Dpi = 254F;
            this.lbl_Index.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold);
            this.lbl_Index.ForeColor = System.Drawing.Color.White;
            this.lbl_Index.LocationFloat = new DevExpress.Utils.PointFloat(5.291667F, 5.291667F);
            this.lbl_Index.Multiline = true;
            this.lbl_Index.Name = "lbl_Index";
            this.lbl_Index.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_Index.SizeF = new System.Drawing.SizeF(60.85417F, 57.0209F);
            this.lbl_Index.StylePriority.UseBackColor = false;
            this.lbl_Index.StylePriority.UseBorderColor = false;
            this.lbl_Index.StylePriority.UseBorderDashStyle = false;
            this.lbl_Index.StylePriority.UseBorders = false;
            this.lbl_Index.StylePriority.UseBorderWidth = false;
            this.lbl_Index.StylePriority.UseFont = false;
            this.lbl_Index.StylePriority.UseForeColor = false;
            this.lbl_Index.StylePriority.UseTextAlignment = false;
            this.lbl_Index.Text = "م";
            this.lbl_Index.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lbl_Item
            // 
            this.lbl_Item.BackColor = System.Drawing.Color.DimGray;
            this.lbl_Item.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbl_Item.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.lbl_Item.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_Item.BorderWidth = 1F;
            this.lbl_Item.CanGrow = false;
            this.lbl_Item.Dpi = 254F;
            this.lbl_Item.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold);
            this.lbl_Item.ForeColor = System.Drawing.Color.White;
            this.lbl_Item.LocationFloat = new DevExpress.Utils.PointFloat(66.14585F, 5.291667F);
            this.lbl_Item.Multiline = true;
            this.lbl_Item.Name = "lbl_Item";
            this.lbl_Item.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_Item.SizeF = new System.Drawing.SizeF(587.6765F, 57.0209F);
            this.lbl_Item.StylePriority.UseBackColor = false;
            this.lbl_Item.StylePriority.UseBorderColor = false;
            this.lbl_Item.StylePriority.UseBorderDashStyle = false;
            this.lbl_Item.StylePriority.UseBorders = false;
            this.lbl_Item.StylePriority.UseBorderWidth = false;
            this.lbl_Item.StylePriority.UseFont = false;
            this.lbl_Item.StylePriority.UseForeColor = false;
            this.lbl_Item.StylePriority.UseTextAlignment = false;
            this.lbl_Item.Text = "تاريخ الاستحقاق";
            this.lbl_Item.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lbl_Unit
            // 
            this.lbl_Unit.BackColor = System.Drawing.Color.DimGray;
            this.lbl_Unit.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbl_Unit.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.lbl_Unit.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_Unit.BorderWidth = 1F;
            this.lbl_Unit.CanGrow = false;
            this.lbl_Unit.Dpi = 254F;
            this.lbl_Unit.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold);
            this.lbl_Unit.ForeColor = System.Drawing.Color.White;
            this.lbl_Unit.LocationFloat = new DevExpress.Utils.PointFloat(653.8223F, 5.291667F);
            this.lbl_Unit.Multiline = true;
            this.lbl_Unit.Name = "lbl_Unit";
            this.lbl_Unit.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_Unit.SizeF = new System.Drawing.SizeF(336.4191F, 57.0209F);
            this.lbl_Unit.StylePriority.UseBackColor = false;
            this.lbl_Unit.StylePriority.UseBorderColor = false;
            this.lbl_Unit.StylePriority.UseBorderDashStyle = false;
            this.lbl_Unit.StylePriority.UseBorders = false;
            this.lbl_Unit.StylePriority.UseBorderWidth = false;
            this.lbl_Unit.StylePriority.UseFont = false;
            this.lbl_Unit.StylePriority.UseForeColor = false;
            this.lbl_Unit.StylePriority.UseTextAlignment = false;
            this.lbl_Unit.Text = "المبلغ";
            this.lbl_Unit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lbl_Qty
            // 
            this.lbl_Qty.BackColor = System.Drawing.Color.DimGray;
            this.lbl_Qty.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbl_Qty.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.lbl_Qty.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_Qty.BorderWidth = 1F;
            this.lbl_Qty.CanGrow = false;
            this.lbl_Qty.Dpi = 254F;
            this.lbl_Qty.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold);
            this.lbl_Qty.ForeColor = System.Drawing.Color.White;
            this.lbl_Qty.LocationFloat = new DevExpress.Utils.PointFloat(990.2414F, 5.291667F);
            this.lbl_Qty.Multiline = true;
            this.lbl_Qty.Name = "lbl_Qty";
            this.lbl_Qty.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_Qty.SizeF = new System.Drawing.SizeF(465.3571F, 57.0209F);
            this.lbl_Qty.StylePriority.UseBackColor = false;
            this.lbl_Qty.StylePriority.UseBorderColor = false;
            this.lbl_Qty.StylePriority.UseBorderDashStyle = false;
            this.lbl_Qty.StylePriority.UseBorders = false;
            this.lbl_Qty.StylePriority.UseBorderWidth = false;
            this.lbl_Qty.StylePriority.UseFont = false;
            this.lbl_Qty.StylePriority.UseForeColor = false;
            this.lbl_Qty.StylePriority.UseTextAlignment = false;
            this.lbl_Qty.Text = "تاريخ السداد";
            this.lbl_Qty.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lbl_Price
            // 
            this.lbl_Price.BackColor = System.Drawing.Color.DimGray;
            this.lbl_Price.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbl_Price.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.lbl_Price.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_Price.BorderWidth = 1F;
            this.lbl_Price.CanGrow = false;
            this.lbl_Price.Dpi = 254F;
            this.lbl_Price.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold);
            this.lbl_Price.ForeColor = System.Drawing.Color.White;
            this.lbl_Price.LocationFloat = new DevExpress.Utils.PointFloat(1455.599F, 5.291667F);
            this.lbl_Price.Multiline = true;
            this.lbl_Price.Name = "lbl_Price";
            this.lbl_Price.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_Price.SizeF = new System.Drawing.SizeF(504.4015F, 57.0209F);
            this.lbl_Price.StylePriority.UseBackColor = false;
            this.lbl_Price.StylePriority.UseBorderColor = false;
            this.lbl_Price.StylePriority.UseBorderDashStyle = false;
            this.lbl_Price.StylePriority.UseBorders = false;
            this.lbl_Price.StylePriority.UseBorderWidth = false;
            this.lbl_Price.StylePriority.UseFont = false;
            this.lbl_Price.StylePriority.UseForeColor = false;
            this.lbl_Price.StylePriority.UseTextAlignment = false;
            this.lbl_Price.Text = "مسدده";
            this.lbl_Price.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Dpi = 254F;
            this.GroupFooter1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail;
            this.GroupFooter1.HeightF = 5F;
            this.GroupFooter1.KeepTogether = true;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // lbl_usernameTxt
            // 
            this.lbl_usernameTxt.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.lbl_usernameTxt.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.lbl_usernameTxt.AutoWidth = true;
            this.lbl_usernameTxt.CanShrink = true;
            this.lbl_usernameTxt.Dpi = 254F;
            this.lbl_usernameTxt.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.lbl_usernameTxt.ForeColor = System.Drawing.Color.DimGray;
            this.lbl_usernameTxt.LocationFloat = new DevExpress.Utils.PointFloat(41.76405F, 7.170105F);
            this.lbl_usernameTxt.Name = "lbl_usernameTxt";
            this.lbl_usernameTxt.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_usernameTxt.SizeF = new System.Drawing.SizeF(254F, 44.00006F);
            this.lbl_usernameTxt.StylePriority.UseFont = false;
            this.lbl_usernameTxt.StylePriority.UseForeColor = false;
            this.lbl_usernameTxt.StylePriority.UseTextAlignment = false;
            this.lbl_usernameTxt.Text = "المستخدم";
            this.lbl_usernameTxt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lbl_UserName
            // 
            this.lbl_UserName.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.lbl_UserName.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.lbl_UserName.AutoWidth = true;
            this.lbl_UserName.CanShrink = true;
            this.lbl_UserName.Dpi = 254F;
            this.lbl_UserName.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.lbl_UserName.ForeColor = System.Drawing.Color.DimGray;
            this.lbl_UserName.LocationFloat = new DevExpress.Utils.PointFloat(5.879244F, 51.17017F);
            this.lbl_UserName.Name = "lbl_UserName";
            this.lbl_UserName.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_UserName.SizeF = new System.Drawing.SizeF(325.7695F, 47.83672F);
            this.lbl_UserName.StylePriority.UseFont = false;
            this.lbl_UserName.StylePriority.UseForeColor = false;
            this.lbl_UserName.StylePriority.UseTextAlignment = false;
            this.lbl_UserName.Text = "xrLabel1";
            this.lbl_UserName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel5
            // 
            this.xrLabel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.xrLabel5.BorderColor = System.Drawing.Color.Empty;
            this.xrLabel5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel5.Dpi = 254F;
            this.xrLabel5.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel5.ForeColor = System.Drawing.Color.White;
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(1384.752F, 678.2971F);
            this.xrLabel5.Multiline = true;
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(304.016F, 58.41992F);
            this.xrLabel5.StylePriority.UseBackColor = false;
            this.xrLabel5.StylePriority.UseBorderColor = false;
            this.xrLabel5.StylePriority.UseBorders = false;
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UseForeColor = false;
            this.xrLabel5.StylePriority.UsePadding = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "مده التقسيط";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.xrLabel4.BorderColor = System.Drawing.Color.Empty;
            this.xrLabel4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel4.Dpi = 254F;
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel4.ForeColor = System.Drawing.Color.White;
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(1384.752F, 619.8771F);
            this.xrLabel4.Multiline = true;
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(304.016F, 58.41992F);
            this.xrLabel4.StylePriority.UseBackColor = false;
            this.xrLabel4.StylePriority.UseBorderColor = false;
            this.xrLabel4.StylePriority.UseBorders = false;
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseForeColor = false;
            this.xrLabel4.StylePriority.UsePadding = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "عدد الاقساط";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl_Period
            // 
            this.lbl_Period.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_Period.Dpi = 254F;
            this.lbl_Period.LocationFloat = new DevExpress.Utils.PointFloat(1688.768F, 680.743F);
            this.lbl_Period.Name = "lbl_Period";
            this.lbl_Period.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_Period.SizeF = new System.Drawing.SizeF(265.5378F, 58.42004F);
            this.lbl_Period.StylePriority.UseBorders = false;
            this.lbl_Period.StylePriority.UseTextAlignment = false;
            this.lbl_Period.Text = "........";
            this.lbl_Period.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lbl_Count
            // 
            this.lbl_Count.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_Count.Dpi = 254F;
            this.lbl_Count.LocationFloat = new DevExpress.Utils.PointFloat(1688.768F, 619.8771F);
            this.lbl_Count.Name = "lbl_Count";
            this.lbl_Count.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_Count.SizeF = new System.Drawing.SizeF(265.5378F, 58.41992F);
            this.lbl_Count.StylePriority.UseBorders = false;
            this.lbl_Count.StylePriority.UseTextAlignment = false;
            this.lbl_Count.Text = ".......";
            this.lbl_Count.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrBarCode1
            // 
            this.xrBarCode1.AutoModule = true;
            this.xrBarCode1.Dpi = 254F;
            this.xrBarCode1.LocationFloat = new DevExpress.Utils.PointFloat(664.6918F, 291.3333F);
            this.xrBarCode1.Module = 5.08F;
            this.xrBarCode1.Name = "xrBarCode1";
            this.xrBarCode1.Padding = new DevExpress.XtraPrinting.PaddingInfo(26, 26, 0, 0, 254F);
            this.xrBarCode1.SizeF = new System.Drawing.SizeF(769.7291F, 99.53024F);
            this.xrBarCode1.Symbology = code128Generator1;
            this.xrBarCode1.Text = "123456";
            // 
            // lbl_NetAsString
            // 
            this.lbl_NetAsString.BackColor = System.Drawing.Color.Gainsboro;
            this.lbl_NetAsString.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_NetAsString.Dpi = 254F;
            this.lbl_NetAsString.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.lbl_NetAsString.LocationFloat = new DevExpress.Utils.PointFloat(822.8542F, 680.3889F);
            this.lbl_NetAsString.Name = "lbl_NetAsString";
            this.lbl_NetAsString.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_NetAsString.SizeF = new System.Drawing.SizeF(520.813F, 58.41992F);
            this.lbl_NetAsString.StylePriority.UseBackColor = false;
            this.lbl_NetAsString.StylePriority.UseBorders = false;
            this.lbl_NetAsString.StylePriority.UseFont = false;
            this.lbl_NetAsString.StylePriority.UseTextAlignment = false;
            this.lbl_NetAsString.Text = "التفقيط";
            this.lbl_NetAsString.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lbl_RemainsText
            // 
            this.lbl_RemainsText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.lbl_RemainsText.BorderColor = System.Drawing.Color.Empty;
            this.lbl_RemainsText.Dpi = 254F;
            this.lbl_RemainsText.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lbl_RemainsText.ForeColor = System.Drawing.Color.White;
            this.lbl_RemainsText.LocationFloat = new DevExpress.Utils.PointFloat(1383.222F, 493.7769F);
            this.lbl_RemainsText.Multiline = true;
            this.lbl_RemainsText.Name = "lbl_RemainsText";
            this.lbl_RemainsText.Padding = new DevExpress.XtraPrinting.PaddingInfo(9, 9, 9, 9, 254F);
            this.lbl_RemainsText.SizeF = new System.Drawing.SizeF(318.2157F, 58.41998F);
            this.lbl_RemainsText.StylePriority.UseBackColor = false;
            this.lbl_RemainsText.StylePriority.UseBorderColor = false;
            this.lbl_RemainsText.StylePriority.UseFont = false;
            this.lbl_RemainsText.StylePriority.UseForeColor = false;
            this.lbl_RemainsText.StylePriority.UsePadding = false;
            this.lbl_RemainsText.StylePriority.UseTextAlignment = false;
            this.lbl_RemainsText.Text = "المتبقي";
            this.lbl_RemainsText.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl_Remains
            // 
            this.lbl_Remains.Dpi = 254F;
            this.lbl_Remains.LocationFloat = new DevExpress.Utils.PointFloat(1702.967F, 496.2377F);
            this.lbl_Remains.Name = "lbl_Remains";
            this.lbl_Remains.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_Remains.SizeF = new System.Drawing.SizeF(251.3383F, 58.41994F);
            this.lbl_Remains.StylePriority.UseTextAlignment = false;
            this.lbl_Remains.Text = "........";
            this.lbl_Remains.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl_paidText
            // 
            this.lbl_paidText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.lbl_paidText.BorderColor = System.Drawing.Color.Empty;
            this.lbl_paidText.Dpi = 254F;
            this.lbl_paidText.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lbl_paidText.ForeColor = System.Drawing.Color.White;
            this.lbl_paidText.LocationFloat = new DevExpress.Utils.PointFloat(1383.222F, 433.5261F);
            this.lbl_paidText.Multiline = true;
            this.lbl_paidText.Name = "lbl_paidText";
            this.lbl_paidText.Padding = new DevExpress.XtraPrinting.PaddingInfo(9, 9, 9, 9, 254F);
            this.lbl_paidText.SizeF = new System.Drawing.SizeF(318.2157F, 58.42001F);
            this.lbl_paidText.StylePriority.UseBackColor = false;
            this.lbl_paidText.StylePriority.UseBorderColor = false;
            this.lbl_paidText.StylePriority.UseFont = false;
            this.lbl_paidText.StylePriority.UseForeColor = false;
            this.lbl_paidText.StylePriority.UsePadding = false;
            this.lbl_paidText.StylePriority.UseTextAlignment = false;
            this.lbl_paidText.Text = "قيمه الدفعات المسدده";
            this.lbl_paidText.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl_Paid
            // 
            this.lbl_Paid.Dpi = 254F;
            this.lbl_Paid.LocationFloat = new DevExpress.Utils.PointFloat(1702.967F, 435.3717F);
            this.lbl_Paid.Name = "lbl_Paid";
            this.lbl_Paid.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_Paid.SizeF = new System.Drawing.SizeF(251.3383F, 58.41994F);
            this.lbl_Paid.StylePriority.UseTextAlignment = false;
            this.lbl_Paid.Text = "........";
            this.lbl_Paid.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl_Amount
            // 
            this.lbl_Amount.Dpi = 254F;
            this.lbl_Amount.LocationFloat = new DevExpress.Utils.PointFloat(1092.329F, 619.8771F);
            this.lbl_Amount.Name = "lbl_Amount";
            this.lbl_Amount.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_Amount.SizeF = new System.Drawing.SizeF(251.3383F, 58.41994F);
            this.lbl_Amount.StylePriority.UseTextAlignment = false;
            this.lbl_Amount.Text = ".......";
            this.lbl_Amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl_NetText
            // 
            this.lbl_NetText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.lbl_NetText.BorderColor = System.Drawing.Color.Empty;
            this.lbl_NetText.Dpi = 254F;
            this.lbl_NetText.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lbl_NetText.ForeColor = System.Drawing.Color.White;
            this.lbl_NetText.LocationFloat = new DevExpress.Utils.PointFloat(822.8542F, 618.6469F);
            this.lbl_NetText.Multiline = true;
            this.lbl_NetText.Name = "lbl_NetText";
            this.lbl_NetText.Padding = new DevExpress.XtraPrinting.PaddingInfo(9, 9, 9, 9, 254F);
            this.lbl_NetText.SizeF = new System.Drawing.SizeF(267.9449F, 58.42001F);
            this.lbl_NetText.StylePriority.UseBackColor = false;
            this.lbl_NetText.StylePriority.UseBorderColor = false;
            this.lbl_NetText.StylePriority.UseFont = false;
            this.lbl_NetText.StylePriority.UseForeColor = false;
            this.lbl_NetText.StylePriority.UsePadding = false;
            this.lbl_NetText.StylePriority.UseTextAlignment = false;
            this.lbl_NetText.Text = "المبلغ المقسط";
            this.lbl_NetText.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl_Benfite
            // 
            this.lbl_Benfite.Dpi = 254F;
            this.lbl_Benfite.LocationFloat = new DevExpress.Utils.PointFloat(1092.329F, 494.3921F);
            this.lbl_Benfite.Name = "lbl_Benfite";
            this.lbl_Benfite.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_Benfite.SizeF = new System.Drawing.SizeF(251.3383F, 58.41994F);
            this.lbl_Benfite.StylePriority.UseTextAlignment = false;
            this.lbl_Benfite.Text = ".......";
            this.lbl_Benfite.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lbl_Benfite.TextFormatString = "{0:0.00%}";
            // 
            // lbl_DicountText
            // 
            this.lbl_DicountText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.lbl_DicountText.BorderColor = System.Drawing.Color.Empty;
            this.lbl_DicountText.Dpi = 254F;
            this.lbl_DicountText.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lbl_DicountText.ForeColor = System.Drawing.Color.White;
            this.lbl_DicountText.LocationFloat = new DevExpress.Utils.PointFloat(822.8542F, 493.7769F);
            this.lbl_DicountText.Multiline = true;
            this.lbl_DicountText.Name = "lbl_DicountText";
            this.lbl_DicountText.Padding = new DevExpress.XtraPrinting.PaddingInfo(9, 9, 9, 9, 254F);
            this.lbl_DicountText.SizeF = new System.Drawing.SizeF(267.9449F, 58.42001F);
            this.lbl_DicountText.StylePriority.UseBackColor = false;
            this.lbl_DicountText.StylePriority.UseBorderColor = false;
            this.lbl_DicountText.StylePriority.UseFont = false;
            this.lbl_DicountText.StylePriority.UseForeColor = false;
            this.lbl_DicountText.StylePriority.UsePadding = false;
            this.lbl_DicountText.StylePriority.UseTextAlignment = false;
            this.lbl_DicountText.Text = "الفايده";
            this.lbl_DicountText.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl_TotalText
            // 
            this.lbl_TotalText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.lbl_TotalText.BorderColor = System.Drawing.Color.Empty;
            this.lbl_TotalText.Dpi = 254F;
            this.lbl_TotalText.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lbl_TotalText.ForeColor = System.Drawing.Color.White;
            this.lbl_TotalText.LocationFloat = new DevExpress.Utils.PointFloat(822.8542F, 433.5261F);
            this.lbl_TotalText.Multiline = true;
            this.lbl_TotalText.Name = "lbl_TotalText";
            this.lbl_TotalText.Padding = new DevExpress.XtraPrinting.PaddingInfo(9, 9, 9, 9, 254F);
            this.lbl_TotalText.SizeF = new System.Drawing.SizeF(267.9449F, 58.42001F);
            this.lbl_TotalText.StylePriority.UseBackColor = false;
            this.lbl_TotalText.StylePriority.UseBorderColor = false;
            this.lbl_TotalText.StylePriority.UseFont = false;
            this.lbl_TotalText.StylePriority.UseForeColor = false;
            this.lbl_TotalText.StylePriority.UsePadding = false;
            this.lbl_TotalText.StylePriority.UseTextAlignment = false;
            this.lbl_TotalText.Text = "المبلغ";
            this.lbl_TotalText.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl_Total
            // 
            this.lbl_Total.Dpi = 254F;
            this.lbl_Total.LocationFloat = new DevExpress.Utils.PointFloat(1090.799F, 433.5261F);
            this.lbl_Total.Name = "lbl_Total";
            this.lbl_Total.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_Total.SizeF = new System.Drawing.SizeF(251.3383F, 58.41994F);
            this.lbl_Total.StylePriority.UseTextAlignment = false;
            this.lbl_Total.Text = ".......";
            this.lbl_Total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // calculatedField1
            // 
            this.calculatedField1.DataMember = "Inv_ItemSalesInvoice.Inv_ItemSalesInvoiceInv_ItemSalesInvoiceDetails";
            this.calculatedField1.Name = "calculatedField1";
            // 
            // xrControlStyle1
            // 
            this.xrControlStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.xrControlStyle1.Name = "xrControlStyle1";
            this.xrControlStyle1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            // 
            // xrControlStyle2
            // 
            this.xrControlStyle2.Name = "xrControlStyle2";
            this.xrControlStyle2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            // 
            // xrCrossBandBox1
            // 
            this.xrCrossBandBox1.AnchorVertical = ((DevExpress.XtraReports.UI.VerticalAnchorStyles)((DevExpress.XtraReports.UI.VerticalAnchorStyles.Top | DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom)));
            this.xrCrossBandBox1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrCrossBandBox1.Dpi = 254F;
            this.xrCrossBandBox1.EndBand = this.GroupFooter1;
            this.xrCrossBandBox1.EndPointFloat = new DevExpress.Utils.PointFloat(0F, 5F);
            this.xrCrossBandBox1.Name = "xrCrossBandBox1";
            this.xrCrossBandBox1.StartBand = this.GroupHeader1;
            this.xrCrossBandBox1.StartPointFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrCrossBandBox1.WidthF = 2031F;
            // 
            // lbl_rptName
            // 
            this.lbl_rptName.BackColor = System.Drawing.Color.Empty;
            this.lbl_rptName.CanGrow = false;
            this.lbl_rptName.Dpi = 254F;
            this.lbl_rptName.Font = new System.Drawing.Font("Times New Roman", 28F, System.Drawing.FontStyle.Bold);
            this.lbl_rptName.ForeColor = System.Drawing.Color.Navy;
            this.lbl_rptName.LocationFloat = new DevExpress.Utils.PointFloat(664.6918F, 25.00001F);
            this.lbl_rptName.Name = "lbl_rptName";
            this.lbl_rptName.Padding = new DevExpress.XtraPrinting.PaddingInfo(9, 9, 9, 9, 254F);
            this.lbl_rptName.SizeF = new System.Drawing.SizeF(770.5423F, 111.3367F);
            this.lbl_rptName.StylePriority.UseBackColor = false;
            this.lbl_rptName.StylePriority.UseFont = false;
            this.lbl_rptName.StylePriority.UseForeColor = false;
            this.lbl_rptName.StylePriority.UsePadding = false;
            this.lbl_rptName.StylePriority.UseTextAlignment = false;
            this.lbl_rptName.Text = "ملف تقسيط لعميل";
            this.lbl_rptName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lbl_rptName.WordWrap = false;
            // 
            // lbl_companyName
            // 
            this.lbl_companyName.CanGrow = false;
            this.lbl_companyName.Dpi = 254F;
            this.lbl_companyName.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.lbl_companyName.LocationFloat = new DevExpress.Utils.PointFloat(664.6918F, 136.3367F);
            this.lbl_companyName.Name = "lbl_companyName";
            this.lbl_companyName.Padding = new DevExpress.XtraPrinting.PaddingInfo(9, 9, 9, 9, 254F);
            this.lbl_companyName.SizeF = new System.Drawing.SizeF(769.729F, 87.02084F);
            this.lbl_companyName.StylePriority.UseFont = false;
            this.lbl_companyName.StylePriority.UsePadding = false;
            this.lbl_companyName.StylePriority.UseTextAlignment = false;
            this.lbl_companyName.Text = "اسم الشركه (يحمل تلقائيا)";
            this.lbl_companyName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrPictureBox1.Dpi = 254F;
            this.xrPictureBox1.ImageAlignment = DevExpress.XtraPrinting.ImageAlignment.MiddleCenter;
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(1509.509F, 48.81251F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(425.4912F, 303.8528F);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            // 
            // lbl_DateText
            // 
            this.lbl_DateText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.lbl_DateText.BorderColor = System.Drawing.Color.Empty;
            this.lbl_DateText.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_DateText.Dpi = 254F;
            this.lbl_DateText.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lbl_DateText.ForeColor = System.Drawing.Color.White;
            this.lbl_DateText.LocationFloat = new DevExpress.Utils.PointFloat(5.584688F, 487.8253F);
            this.lbl_DateText.Multiline = true;
            this.lbl_DateText.Name = "lbl_DateText";
            this.lbl_DateText.Padding = new DevExpress.XtraPrinting.PaddingInfo(15, 15, 0, 0, 254F);
            this.lbl_DateText.SizeF = new System.Drawing.SizeF(267.9449F, 58.42001F);
            this.lbl_DateText.StylePriority.UseBackColor = false;
            this.lbl_DateText.StylePriority.UseBorderColor = false;
            this.lbl_DateText.StylePriority.UseBorders = false;
            this.lbl_DateText.StylePriority.UseFont = false;
            this.lbl_DateText.StylePriority.UseForeColor = false;
            this.lbl_DateText.StylePriority.UsePadding = false;
            this.lbl_DateText.Text = "التاريخ";
            // 
            // lbl_IDText
            // 
            this.lbl_IDText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.lbl_IDText.BorderColor = System.Drawing.Color.Empty;
            this.lbl_IDText.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_IDText.Dpi = 254F;
            this.lbl_IDText.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lbl_IDText.ForeColor = System.Drawing.Color.White;
            this.lbl_IDText.LocationFloat = new DevExpress.Utils.PointFloat(5.584688F, 423.6376F);
            this.lbl_IDText.Multiline = true;
            this.lbl_IDText.Name = "lbl_IDText";
            this.lbl_IDText.Padding = new DevExpress.XtraPrinting.PaddingInfo(15, 15, 0, 0, 254F);
            this.lbl_IDText.SizeF = new System.Drawing.SizeF(267.9449F, 58.42F);
            this.lbl_IDText.StylePriority.UseBackColor = false;
            this.lbl_IDText.StylePriority.UseBorderColor = false;
            this.lbl_IDText.StylePriority.UseBorders = false;
            this.lbl_IDText.StylePriority.UseFont = false;
            this.lbl_IDText.StylePriority.UseForeColor = false;
            this.lbl_IDText.StylePriority.UsePadding = false;
            this.lbl_IDText.Text = "كود/رقم الملف";
            // 
            // lbl_ID
            // 
            this.lbl_ID.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_ID.Dpi = 254F;
            this.lbl_ID.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.lbl_ID.LocationFloat = new DevExpress.Utils.PointFloat(275.4501F, 423.6376F);
            this.lbl_ID.Name = "lbl_ID";
            this.lbl_ID.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_ID.SizeF = new System.Drawing.SizeF(513.7262F, 58.42001F);
            this.lbl_ID.StylePriority.UseBorders = false;
            this.lbl_ID.StylePriority.UseFont = false;
            this.lbl_ID.StylePriority.UseTextAlignment = false;
            this.lbl_ID.Text = "...........";
            this.lbl_ID.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl_Date
            // 
            this.lbl_Date.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_Date.Dpi = 254F;
            this.lbl_Date.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.lbl_Date.LocationFloat = new DevExpress.Utils.PointFloat(275.4501F, 489.748F);
            this.lbl_Date.Name = "lbl_Date";
            this.lbl_Date.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_Date.SizeF = new System.Drawing.SizeF(513.7262F, 58.42001F);
            this.lbl_Date.StylePriority.UseBorders = false;
            this.lbl_Date.StylePriority.UseFont = false;
            this.lbl_Date.StylePriority.UseTextAlignment = false;
            this.lbl_Date.Text = "...........";
            this.lbl_Date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lbl_Date.TextFormatString = "{0:yyyy-MM-dd dddd hh:mm tt}";
            // 
            // lbl_Cpmpany_Phone
            // 
            this.lbl_Cpmpany_Phone.Dpi = 254F;
            this.lbl_Cpmpany_Phone.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.lbl_Cpmpany_Phone.LocationFloat = new DevExpress.Utils.PointFloat(664.6918F, 232.9133F);
            this.lbl_Cpmpany_Phone.Name = "lbl_Cpmpany_Phone";
            this.lbl_Cpmpany_Phone.Padding = new DevExpress.XtraPrinting.PaddingInfo(9, 9, 9, 9, 254F);
            this.lbl_Cpmpany_Phone.SizeF = new System.Drawing.SizeF(769.729F, 58.41998F);
            this.lbl_Cpmpany_Phone.StylePriority.UseFont = false;
            this.lbl_Cpmpany_Phone.StylePriority.UsePadding = false;
            this.lbl_Cpmpany_Phone.StylePriority.UseTextAlignment = false;
            this.lbl_Cpmpany_Phone.Text = "lbl_Cpmpany_Phone";
            this.lbl_Cpmpany_Phone.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lbl_Cpmpany_Phone.Visible = false;
            // 
            // xrLine2
            // 
            this.xrLine2.Dpi = 254F;
            this.xrLine2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(3.00123F, 411.436F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(1956.999F, 12.2016F);
            this.xrLine2.StylePriority.UseForeColor = false;
            // 
            // lbl_PartName
            // 
            this.lbl_PartName.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_PartName.Dpi = 254F;
            this.lbl_PartName.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.lbl_PartName.LocationFloat = new DevExpress.Utils.PointFloat(275.4501F, 555.859F);
            this.lbl_PartName.Name = "lbl_PartName";
            this.lbl_PartName.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_PartName.SizeF = new System.Drawing.SizeF(513.7262F, 58.42001F);
            this.lbl_PartName.StylePriority.UseBorders = false;
            this.lbl_PartName.StylePriority.UseFont = false;
            this.lbl_PartName.StylePriority.UseTextAlignment = false;
            this.lbl_PartName.Text = "...........";
            this.lbl_PartName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl_PartType
            // 
            this.lbl_PartType.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.lbl_PartType.BorderColor = System.Drawing.Color.Empty;
            this.lbl_PartType.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_PartType.Dpi = 254F;
            this.lbl_PartType.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lbl_PartType.ForeColor = System.Drawing.Color.White;
            this.lbl_PartType.LocationFloat = new DevExpress.Utils.PointFloat(5.87771F, 552.0131F);
            this.lbl_PartType.Multiline = true;
            this.lbl_PartType.Name = "lbl_PartType";
            this.lbl_PartType.Padding = new DevExpress.XtraPrinting.PaddingInfo(15, 15, 0, 0, 254F);
            this.lbl_PartType.SizeF = new System.Drawing.SizeF(267.9449F, 58.42F);
            this.lbl_PartType.StylePriority.UseBackColor = false;
            this.lbl_PartType.StylePriority.UseBorderColor = false;
            this.lbl_PartType.StylePriority.UseBorders = false;
            this.lbl_PartType.StylePriority.UseFont = false;
            this.lbl_PartType.StylePriority.UseForeColor = false;
            this.lbl_PartType.StylePriority.UsePadding = false;
            this.lbl_PartType.Text = "اسم العميل";
            // 
            // lbl_AdressTxt
            // 
            this.lbl_AdressTxt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.lbl_AdressTxt.BorderColor = System.Drawing.Color.Empty;
            this.lbl_AdressTxt.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_AdressTxt.Dpi = 254F;
            this.lbl_AdressTxt.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lbl_AdressTxt.ForeColor = System.Drawing.Color.White;
            this.lbl_AdressTxt.LocationFloat = new DevExpress.Utils.PointFloat(5.878759F, 680.3885F);
            this.lbl_AdressTxt.Multiline = true;
            this.lbl_AdressTxt.Name = "lbl_AdressTxt";
            this.lbl_AdressTxt.Padding = new DevExpress.XtraPrinting.PaddingInfo(15, 15, 0, 0, 254F);
            this.lbl_AdressTxt.SizeF = new System.Drawing.SizeF(267.9449F, 58.42F);
            this.lbl_AdressTxt.StylePriority.UseBackColor = false;
            this.lbl_AdressTxt.StylePriority.UseBorderColor = false;
            this.lbl_AdressTxt.StylePriority.UseBorders = false;
            this.lbl_AdressTxt.StylePriority.UseFont = false;
            this.lbl_AdressTxt.StylePriority.UseForeColor = false;
            this.lbl_AdressTxt.StylePriority.UsePadding = false;
            this.lbl_AdressTxt.Text = "العنوان";
            // 
            // lbl_Phonetxt
            // 
            this.lbl_Phonetxt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.lbl_Phonetxt.BorderColor = System.Drawing.Color.Empty;
            this.lbl_Phonetxt.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_Phonetxt.Dpi = 254F;
            this.lbl_Phonetxt.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lbl_Phonetxt.ForeColor = System.Drawing.Color.White;
            this.lbl_Phonetxt.LocationFloat = new DevExpress.Utils.PointFloat(5.879244F, 616.2008F);
            this.lbl_Phonetxt.Multiline = true;
            this.lbl_Phonetxt.Name = "lbl_Phonetxt";
            this.lbl_Phonetxt.Padding = new DevExpress.XtraPrinting.PaddingInfo(15, 15, 0, 0, 254F);
            this.lbl_Phonetxt.SizeF = new System.Drawing.SizeF(267.9449F, 58.42F);
            this.lbl_Phonetxt.StylePriority.UseBackColor = false;
            this.lbl_Phonetxt.StylePriority.UseBorderColor = false;
            this.lbl_Phonetxt.StylePriority.UseBorders = false;
            this.lbl_Phonetxt.StylePriority.UseFont = false;
            this.lbl_Phonetxt.StylePriority.UseForeColor = false;
            this.lbl_Phonetxt.StylePriority.UsePadding = false;
            this.lbl_Phonetxt.Text = "الهاتف";
            // 
            // lbl_Address
            // 
            this.lbl_Address.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_Address.Dpi = 254F;
            this.lbl_Address.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.lbl_Address.LocationFloat = new DevExpress.Utils.PointFloat(275.4501F, 680.3885F);
            this.lbl_Address.Name = "lbl_Address";
            this.lbl_Address.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_Address.SizeF = new System.Drawing.SizeF(520.7482F, 58.4201F);
            this.lbl_Address.StylePriority.UseBorders = false;
            this.lbl_Address.StylePriority.UseFont = false;
            this.lbl_Address.StylePriority.UseTextAlignment = false;
            this.lbl_Address.Text = "...........";
            this.lbl_Address.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl_Mobile
            // 
            this.lbl_Mobile.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_Mobile.Dpi = 254F;
            this.lbl_Mobile.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.lbl_Mobile.LocationFloat = new DevExpress.Utils.PointFloat(519.2831F, 616.2008F);
            this.lbl_Mobile.Name = "lbl_Mobile";
            this.lbl_Mobile.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_Mobile.SizeF = new System.Drawing.SizeF(276.9149F, 58.4201F);
            this.lbl_Mobile.StylePriority.UseBorders = false;
            this.lbl_Mobile.StylePriority.UseFont = false;
            this.lbl_Mobile.StylePriority.UseTextAlignment = false;
            this.lbl_Mobile.Text = "...........";
            this.lbl_Mobile.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl_Phone
            // 
            this.lbl_Phone.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_Phone.Dpi = 254F;
            this.lbl_Phone.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.lbl_Phone.LocationFloat = new DevExpress.Utils.PointFloat(275.4501F, 616.2008F);
            this.lbl_Phone.Name = "lbl_Phone";
            this.lbl_Phone.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_Phone.SizeF = new System.Drawing.SizeF(243.833F, 58.42004F);
            this.lbl_Phone.StylePriority.UseBorders = false;
            this.lbl_Phone.StylePriority.UseFont = false;
            this.lbl_Phone.StylePriority.UseTextAlignment = false;
            this.lbl_Phone.Text = "...........";
            this.lbl_Phone.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // Detail
            // 
            this.Detail.BackColor = System.Drawing.Color.Empty;
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1,
            this.lbl_AdvancedPay,
            this.xrBarCode1,
            this.lbl_NetAsString,
            this.xrLabel5,
            this.xrLabel4,
            this.lbl_Period,
            this.lbl_Count,
            this.lbl_RemainsText,
            this.lbl_Remains,
            this.lbl_Total,
            this.lbl_Paid,
            this.lbl_Amount,
            this.lbl_NetText,
            this.lbl_Benfite,
            this.lbl_DicountText,
            this.lbl_TotalText,
            this.lbl_paidText,
            this.lbl_Phone,
            this.lbl_Mobile,
            this.lbl_Address,
            this.lbl_Phonetxt,
            this.lbl_AdressTxt,
            this.lbl_PartType,
            this.lbl_PartName,
            this.xrLine2,
            this.lbl_Cpmpany_Phone,
            this.lbl_Date,
            this.lbl_ID,
            this.lbl_IDText,
            this.lbl_DateText,
            this.xrPictureBox1,
            this.lbl_companyName,
            this.lbl_rptName});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 739.163F;
            this.Detail.KeepTogether = true;
            this.Detail.KeepTogetherWithDetailReports = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.StylePriority.UseBackColor = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrCrossBandBox2
            // 
            this.xrCrossBandBox2.AnchorVertical = ((DevExpress.XtraReports.UI.VerticalAnchorStyles)((DevExpress.XtraReports.UI.VerticalAnchorStyles.Top | DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom)));
            this.xrCrossBandBox2.Dpi = 254F;
            this.xrCrossBandBox2.EndBand = this.Detail1;
            this.xrCrossBandBox2.EndPointFloat = new DevExpress.Utils.PointFloat(0.2930215F, 74.5158F);
            this.xrCrossBandBox2.Name = "xrCrossBandBox2";
            this.xrCrossBandBox2.StartBand = this.Detail1;
            this.xrCrossBandBox2.StartPointFloat = new DevExpress.Utils.PointFloat(0.2930215F, 0F);
            this.xrCrossBandBox2.WidthF = 2030.707F;
            // 
            // xrLabel1
            // 
            this.xrLabel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.xrLabel1.BorderColor = System.Drawing.Color.Empty;
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.ForeColor = System.Drawing.Color.White;
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(822.8542F, 557.7809F);
            this.xrLabel1.Multiline = true;
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(9, 9, 9, 9, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(267.9449F, 58.42001F);
            this.xrLabel1.StylePriority.UseBackColor = false;
            this.xrLabel1.StylePriority.UseBorderColor = false;
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseForeColor = false;
            this.xrLabel1.StylePriority.UsePadding = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "المدفوع مقدما";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl_AdvancedPay
            // 
            this.lbl_AdvancedPay.Dpi = 254F;
            this.lbl_AdvancedPay.LocationFloat = new DevExpress.Utils.PointFloat(1092.329F, 559.0111F);
            this.lbl_AdvancedPay.Name = "lbl_AdvancedPay";
            this.lbl_AdvancedPay.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbl_AdvancedPay.SizeF = new System.Drawing.SizeF(251.3383F, 58.41994F);
            this.lbl_AdvancedPay.StylePriority.UseTextAlignment = false;
            this.lbl_AdvancedPay.Text = ".......";
            this.lbl_AdvancedPay.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // rpt_Instalment
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.DetailReport});
            this.CalculatedFields.AddRange(new DevExpress.XtraReports.UI.CalculatedField[] {
            this.calculatedField1});
            this.CrossBandControls.AddRange(new DevExpress.XtraReports.UI.XRCrossBandControl[] {
            this.xrCrossBandBox2,
            this.xrCrossBandBox1});
            this.Dpi = 254F;
            this.Margins = new System.Drawing.Printing.Margins(42, 98, 55, 99);
            this.PageHeight = 2970;
            this.PageWidth = 2100;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.SnapGridSize = 25F;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.xrControlStyle1,
            this.xrControlStyle2});
            this.Version = "19.2";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.CalculatedField calculatedField1;
        public  DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        public  DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.XRLabel lbl_TotalText;
        private DevExpress.XtraReports.UI.XRLabel lbl_Total;
        private DevExpress.XtraReports.UI.XRLabel lbl_Item;
        private DevExpress.XtraReports.UI.XRLabel lbl_Unit;
        private DevExpress.XtraReports.UI.XRLabel lbl_Qty;
        private DevExpress.XtraReports.UI.XRLabel lbl_Price;
        private DevExpress.XtraReports.UI.XRControlStyle xrControlStyle1;
        private DevExpress.XtraReports.UI.XRControlStyle xrControlStyle2;
        private DevExpress.XtraReports.UI.XRLabel lbl_PrinDate;
        private DevExpress.XtraReports.UI.XRLabel lbl_usernameTxt;
        private DevExpress.XtraReports.UI.XRLabel lbl_UserName;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRLabel lbl_PrintDateText;
        private DevExpress.XtraReports.UI.XRLabel lbl_Index;
        private DevExpress.XtraReports.UI.XRLabel lbl_NetAsString;
        private DevExpress.XtraReports.UI.XRLabel lbl_RemainsText;
        private DevExpress.XtraReports.UI.XRLabel lbl_Remains;
        private DevExpress.XtraReports.UI.XRLabel lbl_paidText;
        private DevExpress.XtraReports.UI.XRLabel lbl_Paid;
        private DevExpress.XtraReports.UI.XRLabel lbl_Amount;
        private DevExpress.XtraReports.UI.XRLabel lbl_NetText;
        private DevExpress.XtraReports.UI.XRLabel lbl_Benfite;
        private DevExpress.XtraReports.UI.XRLabel lbl_DicountText;
        private DevExpress.XtraReports.UI.XRLabel cell_PayDate;
        private DevExpress.XtraReports.UI.XRLabel cell_Amount;
        private DevExpress.XtraReports.UI.XRLabel cell_DueDate;
        private DevExpress.XtraReports.UI.XRLabel cell_Index;
        private DevExpress.XtraReports.UI.XRCrossBandBox xrCrossBandBox1;
        private DevExpress.XtraReports.UI.XRLabel lbl_companyName;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        private DevExpress.XtraReports.UI.XRLabel lbl_DateText;
        private DevExpress.XtraReports.UI.XRLabel lbl_IDText;
        private DevExpress.XtraReports.UI.XRLabel lbl_ID;
        private DevExpress.XtraReports.UI.XRLabel lbl_Date;
        private DevExpress.XtraReports.UI.XRLabel lbl_Cpmpany_Phone;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRLabel lbl_PartName;
        private DevExpress.XtraReports.UI.XRLabel lbl_PartType;
        private DevExpress.XtraReports.UI.XRLabel lbl_AdressTxt;
        private DevExpress.XtraReports.UI.XRLabel lbl_Phonetxt;
        private DevExpress.XtraReports.UI.XRLabel lbl_Address;
        private DevExpress.XtraReports.UI.XRLabel lbl_Mobile;
        private DevExpress.XtraReports.UI.XRLabel lbl_Phone;
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.XRCrossBandBox xrCrossBandBox2;
        protected internal DevExpress.XtraReports.UI.XRLabel lbl_rptName;
        private DevExpress.XtraReports.UI.XRBarCode xrBarCode1;
        private DevExpress.XtraReports.UI.XRCheckBox xrCheckBox1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel lbl_Period;
        private DevExpress.XtraReports.UI.XRLabel lbl_Count;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel lbl_AdvancedPay;
    }
}